# README #

Here are the steps to get the app running.

### What is this repository for? ###

* This repo is a revised framework for the Turbo project in preparation with the optimisation efforts.
* 0.0.1

### How do I get set up? ###
The repo requires the following dependencies so make sure they are installed.

* Node.js
* Gulp.js
* Bower.io
* Handlebars.js
* SASS
* [Compass](http://compass-style.org/)
* [Buorbon](http://bourbon.io/)

To get started simply type the following commands:
```javascript
npm install
bower install
```

To run the local server just type the command for your specific device:
```javascript
gulp serve -d desktop
gulp serve -d tablet
gulp serve -d mobile
gulp serve -d old
```

### Who do I talk to? ###

* For any questions please contact @wtfroland or ro@curiouslab.sg
