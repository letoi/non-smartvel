// generated on 2017-10-10 using generator-webapp 3.0.1
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');
const wait = require('gulp-wait2');

// requirements for compiling partials
// from https://github.com/lazd/gulp-handlebars
let path = require('path');
let wrap = require('gulp-wrap');
let concat = require('gulp-concat');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const stream = browserSync.stream;

let dev = true;

// add parameter handlers
let argv = require('yargs').argv;
let gulpif = require('gulp-if');

var options = {};
// assign the source folder
options.desktop = 'app/desktop';
options.tablet = 'app/tablet';
options.mobile = 'app/mobile';
options.old = 'app/old-browser';

gulp.task('styles', () => {
  return gulp.src(options[argv.d] + '/styles/*.scss')
    .pipe(wait(200))
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.if(dev, $.sourcemaps.write()))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src(options[argv.d] + '/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(dev, $.sourcemaps.init()))
    // .pipe($.babel())
    .pipe($.if(dev, $.sourcemaps.write('.')))
    .pipe(gulp.dest('.tmp/scripts'));
    // .pipe(wait(600))
    // .pipe(reload({stream: true}));
    // .pipe(stream({once: true}));
});

function lint(files) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js')
    .pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['styles', 'builtstyles', 'scripts', /*'hbstemplates', 'partials',*/ 'templates' ], () => {
  return gulp.src('app/*.html')
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('/**/*.js', $.uglify({compress: {drop_console: false}})))
    .pipe($.if('/**/*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if('/**/*.html', $.htmlmin({
      collapseWhitespace: false,
      minifyCSS: false,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', () => {
  return gulp.src(options[argv.d] + '/images/**/*')
    // .pipe($.cache($.imagemin()))
    // .pipe(gulp.dest('dist/images'));
    .pipe($.if(dev, gulp.dest('.tmp/images'), gulp.dest('dist/images')));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('app/_common/fonts/**/*'))
    .pipe($.if(dev, gulp.dest('.tmp/fonts'), gulp.dest('dist/fonts')));
});

gulp.task('pdf', () => {
  return gulp.src('app/_common/pdf/**/*', function (err) {})
    .pipe($.if(dev, gulp.dest('.tmp/pdf'), gulp.dest('dist/pdf')));
});

gulp.task('ajax', () => {
  return gulp.src('app/_common/ajax/**/*.json', function (err) {})
    .pipe($.if(dev, gulp.dest('.tmp/ajax'), gulp.dest('dist/ajax')));
});

gulp.task('templates', () => {
  return gulp.src(options[argv.d] + '/template/**/*.tpl', function (err) {})
    .pipe($.if(dev, gulp.dest('.tmp/template'), gulp.dest('dist/template')));
});
gulp.task('amp', () => {
  return gulp.src(options[argv.d] + '/amp-assets/**/*.*', function (err) {})

    .pipe($.if(dev, gulp.dest('.tmp/amp-assets'), gulp.dest('dist/amp-assets')));
});

gulp.task('builtstyles', () => {
  return gulp.src(options[argv.d] + '/styles/**/*.css', function (err) {})
    .pipe($.if(dev, gulp.dest('.tmp/styles'), gulp.dest('dist/styles')));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', () => {
  runSequence(['clean', 'wiredep'], ['styles', 'builtstyles', 'scripts', 'templates', /*'hbstemplates', 'partials',*/ 'fonts', 'pdf', 'ajax', 'images', 'amp'], () => {
    browserSync.init({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });

    gulp.watch([
      'app/*.html',
      options[argv.d] + '/images/**/*',
      // options[argv.d] + '/scripts/**/*.js',
      options[argv.d] + '/template/**/*.tpl',
      '.tmp/ajax/**/*.json',
      '.tmp/fonts/**/*'
    ]).on('change', reload);

    gulp.watch(options[argv.d] + '/styles/**/*.scss', ['styles']);
    // gulp.watch(options[argv.d] + '/scripts/**/*.js', ['scripts']);
    gulp.watch(options[argv.d] + '/scripts/**/*.js', ['scripts']).on('change', function(){
      setTimeout(function(){
        reload()
      }, 1500)
    });

    gulp.watch(options[argv.d] + '/template/**/*.tpl', ['templates']);
    gulp.watch(options[argv.d] + '/amp-assets/**/*.*', ['amp']);

    gulp.watch('app/_common/fonts/**/*', ['fonts']);
    gulp.watch('app/_common/ajax/**/*', ['ajax']);
    // gulp.watch('app/templates-hbs/*.hbs', ['hbstemplates']);
    // gulp.watch('app/templates-hbs/partials/*.hbs', ['partials']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', ['default'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']).on('change', function(){
    setTimeout(function(){
      reload()
    }, 1000)
  });
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src(options[argv.d] + '/styles/*.scss')
    .pipe($.filter(file => file.stat && file.stat.size))
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest(options[argv.d] + '/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      exclude: ['bootstrap'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

// TODO: Add multi pipe copy of files to desktop, tablet, mobile, and old-browser folders
gulp.task('build', ['lint', 'html', 'images', 'fonts', 'pdf', 'ajax', /*'hbstemplates', 'partials',*/ 'templates'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  return new Promise(resolve => {
    dev = false;
    runSequence(['clean', 'wiredep'], 'build', resolve);
  });
});
