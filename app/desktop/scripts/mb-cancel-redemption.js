SIA.MBCancelRedemption = function() {
    var passengerDetailsTpl = '<div class="passenger-details">\
        <span class="passenger-name">\
            <div class="custom-checkbox custom-checkbox--1">\
                <input name="booking-checkbox-<%- index %>" id="booking-checkbox-<%- index %>" aria-labelledby="booking-checkbox-<%- index %>-error" type="checkbox" aria-label="Eve Aw">\
                <label for="booking-checkbox-<%- index %>" class="show-fav-text"><%- index%>. <%-fullname %></label>\
            </div>\
        </span>\
        <%if (ticketNumber) { %>\
            <span class="passenger-ticket-no">Ticket number: <%- ticketNumber %></span>\
        <% } %>\
        <%if (membershipNumber) { %>\
            <span class="passenger-krisflyer-no"> KrisFlyer membership number: <%- membershipNumber %></span>\
        <% }  %>\
    </div>';
    
    var init = function() {
        onMount();
    };

    var attachEvents = function() {
        setAllCheckbox(true);
        termsAndConditionListerner();
        addValidations();
        var validator = $("#cancel-redemption-form").validate({
            onkeyup: function(element) {
                $(element).valid();
            },
            onfocusout: function(element) {
                $(element).valid();
            },
            errorPlacement: function(error, element) {
                var parent = element.closest(".validate-el");
                parent.find(".text-error").remove();
                parent.addClass("error");
                var errorMessage = $("<span></span>").append(error.text());
                var errorEL = $('<p class="text-error"></p>').append(errorMessage);
                parent.append(errorEL);
            },
            success: function(label, element) {
                var parent = $(element).closest(".validate-el");
                parent.removeClass("error");
            },
            submitHandler: function(form, evt) {
                evt.preventDefault();
                var checkboxes = $(form).find(".passenger-details").find(".custom-checkbox").find("[type='checkbox']");
                var isChecked = checkAllCheckbox(checkboxes);

                if (!isChecked) {
                    return;
                }
                
                form.submit();
            }
        });
        $(".ico-dropdown").each(function() {
            $(this).css("pointer-events", "none");
        });
        setTimeout(function() {
            SIA.initAutocompleteCity();
        }, 500);
    };

    var onMount = function() {
        $.get("ajax/JSONS/MB/OnlineRefundForm.json", function(response) {
            var data = response.passengerDetails;
            var tpls = _.map(data.passengerInfo, function(passenger, idx) {
                return $(_.template(passengerDetailsTpl, {
                    index: idx + 1,
                    fullname: (passenger.paxFirstName || "") + " " + (passenger.paxLastName || ""),
                    ticketNumber: passenger.ticketNumber || "",
                    membershipNumber: passenger.kfNumber || ""
                }));
            });
            
            $(".passenger-information").find(".passengers-info").append(tpls);
            $(".booking-information-payment").find("p").text(data.bookingInfo.paymentMethod || "");
            $(".booking-information-country").find("p").text(data.bookingInfo.ticketIssuingCountry || "");
            $(".information-firstname").find("p").text(data.kfInfo.firstName || "");
            $(".information-lastname").find("p").text(data.kfInfo.lastName || "");
            $(".information-membership").find("p").text(data.kfInfo.kfNumber || "");

            attachEvents();
        });
    };

    var setAllCheckbox = function(state) {
        var checkboxes = $(".passenger-details").find(".custom-checkbox").find("[type='checkbox']");
        checkboxes.each(function() {
            $(this).prop("checked", state);
        });
    };

    var addValidations = function() {
        var checkboxes = $(".passenger-details").find(".custom-checkbox").find("[type='checkbox']");
        $(".passenger-details")
            .find(".custom-checkbox")
            .find("[type='checkbox']")
            .off("change.checkBoxValue")
            .on("change.checkBoxValue", function() {
                var isChecked = checkAllCheckbox(checkboxes);
                $(".passengers-box-error")[isChecked ? "addClass" : "removeClass"]("hidden");
            });
    };

    var checkAllCheckbox = function(checkboxes) {
    return _.chain(checkboxes)
        .map(function(box) {
            box = $(box);
            return box.is(":checked");
        })
        .reduce(function(memory, bool) {
            return memory || bool;
        }, false)
        .value();
    };

    var termsAndConditionListerner = function() {
        var termsButton = $(".acknowledgement-details").find(".custom-checkbox").find("[type='checkbox']");
        termsButton.off().on("change", function() {
            var termsStatus = termsButton.is(":checked");
            $(".submit-request")[termsStatus ? "removeClass" : "addClass"]("disabled");
            if (termsStatus) {
                $(".submit-request").removeAttr("disabled");
            } else {
                $(".submit-request").attr("disabled", "true");
            }
        });
    };

    return {
        init: init
    }
}();

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof _ == "undefined" ||
            typeof $.validator == "undefined" ||
            typeof SIA.initAutocompleteCity == "undefined"
        ) {
            waitForLibraries(fn);
        } else {
            fn.init();
        }
    }, 100);
};

$(function() {
    waitForLibraries(SIA.MBCancelRedemption);
});