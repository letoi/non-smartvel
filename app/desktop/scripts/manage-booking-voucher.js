$(document).ready(function() {
	var hiddenClassName = 'hidden';
	var showAllClass = '.js-show-all';
	var showLightBoxClass = '.js-lightbox';

	var $vouchers = $('.js-mb-voucher');

	function getVoucherActive(data) {
		var vouchers = [];
		for (var i = 0; i < data.length; i++) {
			var keys = Object.keys(data[i]);

			for (var j = 0; j < keys.length; j++) {
				var voucherWithKey = data[i][keys[j]];

				for (var k = 0; k < voucherWithKey.length; k++) {
					voucherWithKey[k].type = keys[j];
				}

				vouchers = vouchers.concat(voucherWithKey);
			}
		}

		var voucherActive = vouchers.filter(function(obj) {
			return obj.rewardStatus === 'Available';
		});

		return voucherActive;
	}

	function addVoucherToDOM(tpl, data) {
		var template = window._.template(tpl, {
			data: data,
		});

		$vouchers.html(template);
	}

	function initialize() {
		return SIA.AjaxCaller.template('vouchers-active.tpl').then(function(tplContent) {
			return 	SIA.AjaxCaller.json('elite-reward.json').then(function(data) {
				var vouchersActive = getVoucherActive(data.SAARewardInfo || []);
				addVoucherToDOM(tplContent, vouchersActive);
			});
		}).fail(function(err) {
			console.log(err);
		});
	}

	$(document).on('click', showAllClass, function() {
		$('.js-voucher-item').removeClass(hiddenClassName);
		$(this).addClass(hiddenClassName);
	});

	$(document).on('click', showLightBoxClass, function() {
		var self = $(this);

		if (typeof self.data('trigger-popup') === 'boolean') {
			return;
		}

		var popup = $(self.data('trigger-popup'));
		if (!popup.data('Popup')) {
			popup.Popup({
				overlayBGTemplate: SIA.global.config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',

				afterHide: function(){
					$('#container').css('padding-right', '');
					$('body').css('overflow', '');
					var ua = navigator.userAgent.toLowerCase();
					if (ua.indexOf('safari') != -1) {
						if (ua.indexOf('chrome') > -1) { }
						else {
							$('body').attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
						}
					}
				}
			});
		}

		if (!self.hasClass('disabled')) {
			$(window).trigger('click.hideTooltip');
			popup.Popup('show');
		}
	});

	initialize();
});
