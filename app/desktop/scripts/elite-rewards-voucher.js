$(document).ready(function() {
	var $vouchers = $('.js-elite-rewards');
	var showLightBoxClass = '.js-lightbox';
	var $voucherActive = $('.js-mb-voucher-active');

	function flatVoucher(data) {
		var vouchers = [];
		for (var i = 0; i < data.length; i++) {
			var keys = Object.keys(data[i]);

			for (var j = 0; j < keys.length; j++) {
				var voucherWithKey = data[i][keys[j]];

				for (var k = 0; k < voucherWithKey.length; k++) {
					voucherWithKey[k].type = keys[j];
				}


				vouchers = vouchers.concat(voucherWithKey);
			}
		}

		return vouchers;
	}

	function getVoucherActived(vouchers) {
		var voucherActived = vouchers.filter(function(obj) {
			return obj.rewardStatus === 'Used' || obj.rewardStatus === 'Applied';
		});

		return voucherActived;
	}

	function getVoucherActive(vouchers) {
		var voucherActive = vouchers.filter(function(obj) {
			return obj.rewardStatus === 'Available';
		});

		return voucherActive;
	}

	function addVoucherToDOM(tpl, data) {
		var template = window._.template(tpl, {
			data: data,
		});
		$vouchers.html(template);
		}

	function addVouchersToDOM(tpl, data) {
		var template = window._.template(tpl, {
			data: data,
		});
		$voucherActive.html(template);
	}

	function initialize() {
		return 	SIA.AjaxCaller.json('elite-reward.json').then(function(data) {
			var vouchers = flatVoucher(data.SAARewardInfo || []);

			SIA.AjaxCaller.template('elite-reward-voucher-active.tpl').then(function(tplContent) {
				var voucherActive = getVoucherActive(vouchers);
				addVouchersToDOM(tplContent, voucherActive);
			});

			SIA.AjaxCaller.template('elite-reward-voucher-actived.tpl').then(function(tplContent) {
				var voucherActived = getVoucherActived(vouchers);
				addVoucherToDOM(tplContent, voucherActived);
			});
		}).fail(function(err) {
			console.error(err);
		});
	}

	$(document).on('click', showLightBoxClass, function() {
		var self = $(this);

		if (typeof self.data('trigger-popup') === 'boolean') {
			return;
		}

		var popup = $(self.data('trigger-popup'));
		if (!popup.data('Popup')) {
			popup.Popup({
				overlayBGTemplate: SIA.global.config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',

				afterHide: function(){
					$('#container').css('padding-right', '');
					$('body').css('overflow', '');
					var ua = navigator.userAgent.toLowerCase();
					if (ua.indexOf('safari') != -1) {
						if (ua.indexOf('chrome') > -1) { }
						else {
							$('body').attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
						}
					}
				}
			});
		}

		if (!self.hasClass('disabled')) {
			$(window).trigger('click.hideTooltip');
			popup.Popup('show');
		}
	});

	initialize();
});
