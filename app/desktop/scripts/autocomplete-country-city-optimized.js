/**
 * @name SIA
 * @description Define global initAutocompleteCity functions
 * @version 1.0
 */
SIA.autocompleteCountryCity = function() {
	var global = SIA.global;
	var vars = global.vars;
	var win = vars.win;
	var body = vars.body;
	var originSelected;
	var destinationSelected;
	var countryOriginSelected;
	var countryDestinationSelected;
	var _autoComplete = function(opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			autocompleteFields: '',
			autoCompleteAppendTo: '',
			airportData: [],
			open: function() {},
			change: function() {},
			select: function() {},
			close: function() {},
			search: function() {},
			response: function() {},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);
		that.autocompleteFields = that.options.autocompleteFields;
		that.airportData = that.options.airportData;
		that.cityData = that.options.cityData;
		that.timer = null;
		that.timerResize = null;
		that.winW = win.width();

		that.autocompleteFields.each(function(index, value) {
			var field = $(value);
			var wp = field.closest('[data-autocomplete]');
			var inputEl = wp.find('input');
			var optionEl = wp.find('select option');

			if (wp.data('origin') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						console.log(inputEl.val());
						countryOriginSelected = $(this).data('text');
						originSelected = $(this).data('country-exclusion');
					}
				});
			}

			if (wp.data('destination') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						countryDestinationSelected = $(this).data('text');
						destinationSelected = $(this).data('country-exclusion');
					}
				});
			}

			var countryAutoComplete = field.autocomplete({
				minLength: 1,
				open: that.options.open,
				change: that.options.change,
				select: that.options.select,
				close: that.options.close,
				search: that.options.search,
				response: that.options.response,

				source: function(request, response) {
					var listData = [],
						listCodeCheck = [];

					if (wp.data('origin')) {

						if (destinationSelected && destinationSelected !== '') {
							destinationSelected.indexOf(',') === -1 ? listCodeCheck.push(destinationSelected) : listCodeCheck = destinationSelected.split(',');
						}
						if (countryDestinationSelected && countryDestinationSelected !== '') {
							listCodeCheck.push(countryDestinationSelected);
						}
					}

					if (wp.data('destination')) {
						if (originSelected && originSelected !== '') {
							originSelected.indexOf(',') === -1 ? listCodeCheck.push(originSelected) : listCodeCheck = originSelected.split(',');
						}
						if (countryOriginSelected && countryOriginSelected !== '') {
							listCodeCheck.push(countryOriginSelected);
						}
					}

					if (!request.term) {
						listData = checkAvailableFlights(listCodeCheck, that.airportData);
						listData = checkGroupNoItem(listData);
						if (wp.closest('#singaporeAirlines, #australiaAirlines')) {
							response(listData.slice(0, 100));
						} else {
							response(listData);
						}
						return;
					}

					try {
						that.group = '';
						var matcher = new RegExp('(^' + $.ui.autocomplete.escapeRegex(request.term) + ')|(\\s' + $.ui.autocomplete.escapeRegex(request.term) + ')|([(]' + $.ui.autocomplete.escapeRegex(request.term) + ')', 'i');
						var match1 = [];
						if (that.cityData.length) {
							match1 = $.grep(that.cityData, function(airport) {
								var label = airport.label;
								var valueOption = airport.valueOption;
								return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
							});
						}

						var match = $.grep(that.airportData, function(airport) {
							var label = airport.label;
							var valueOption = airport.valueOption;
							var value = airport.countrySelectedcity;
							return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
						});

						listData = checkAvailableFlights(listCodeCheck, match);
						listData = checkGroupNoItem(listData);

						if (match1.length) {
							match1 = checkAvailableFlights(listCodeCheck, match1);
							response(match1.concat(listData));
						} else {
							if (wp.closest('#singaporeAirlines, #australiaAirlines')) {
								response(listData);
							} else {
								response(listData);
							}
						}

					} catch (err) {
						response(that.airportData);
					}
				},
				position: that.options.position,
				appendTo: that.options.autoCompleteAppendTo
			}).data('ui-autocomplete');

			countryAutoComplete._renderItem = function(ul, item) {
				if (item.city !== "No results found.") {
					var link = item.link ? ' data-link="' + item.link + '"' : '';
					if (item.parent === that.group) {
						return $('<li class="autocomplete-item"' + link + '>')
							.attr('data-value', item.city)
							.append('<a class="autocomplete-link">' + item.city + '</a>')
							.appendTo(ul);
					} else if (item.parent) {
						return $('<li class="autocomplete-item redundancy"' + link + '>')
							.attr('data-value', item.city)
							.append('<a class="autocomplete-link">' + item.city + '</a>')
							.prependTo(ul);
					}
					return $('<li class="autocomplete-item"' + link + '>')
						.attr('data-value', item.city)
						.append('<a class="autocomplete-link">' + item.city + '</a>')
						.appendTo(ul);
				} else if (item.city === "No results found.") {
					return $('<li class="autocomplete-item" data-value="No results found.">').append('<p style="padding-left: 10px;">No results found.</p>').appendTo(ul);
				}
			};

			/** Resize the menu base on the options width. **/
			countryAutoComplete._resizeMenu = function() {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function() {
				var self = $(this);

				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				win.off('resize.repositionAutocompleteCity').on('resize.repositionAutocompleteCity', function() {
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function() {
						if (that.winW !== win.width()) {
							field.trigger('blur.highlight');
							that.winW = win.width();
						}
					}, 100);
				});
			});

			field.off('blur.highlight').on('blur.highlight', function() {
				that.timer = setTimeout(function() {
					field.closest('.custom-select').removeClass('focus');
				}, 200);
				field.autocomplete('close');
				win.off('resize.repositionAutocompleteCity');
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e) {
				e.stopPropagation();
			});

			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function() {
				clearTimeout(that.timer);
			});

			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e) {
				if (e.which === 13) {
					field.autocomplete('widget').find('li.active').trigger('click');
					e.preventDefault();
				}
			});

			if (field.closest('#singaporeAirlines, #australiaAirlines').length >= 1) {
				field.next().remove();
			}

			field.on('keyup', function(e) {
				if (field.closest('#singaporeAirlines, #australiaAirlines').length >= 1) {
					if (field.val().length == 0) {
						field.closest('[data-autocomplete="true"]').find('.optim-close').addClass('hidden');
						field.autocomplete('close');
					} else {
						clearCloseBtn(field);
					}
				}
			});

			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e) {
				e.preventDefault();
				clearTimeout(that.timer);
				if (field.closest('.custom-select').hasClass('focus')) {
					field.trigger('blur.highlight');
				} else {
					field.trigger('focus.highlight');
				}
			});
		});
		resetAvailableFlight();
	};


	var clearCloseBtn = function(el) {
		var closeIconEl = $('<a href="#clear" class="ico-cancel-thin optim-close add-clear-text" tabindex="-1"><span class="ui-helper-hidden-accessible">clear</span></a>');
		var parent = el.closest('[data-autocomplete="true"]');

		var closeIcon = parent.find('.optim-close');
		if (closeIcon.length >= 1) {
			closeIcon.removeClass('hidden');
		} else {
			closeIconEl.off().on('click', function(e) {
				e.preventDefault();

				el.val('');
				$(this).addClass('hidden');
			});
			parent.find('.select__text').after(closeIconEl);
		}
	};

	var createList = function(data, type) {
		var str = '';
		if (type === 'list') {
			str = '<li class="autocomplete-item ui-menu-item" data-value="' + (data.airportCode + ' - ' + data.airportName) + '" role="presentation"><a class="autocomplete-link ui-corner-all" tabindex="-1">' + (data.airportName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.countryCode + ')') + '</a></li>';
		} else {
			str = '<option value="' + (data.airportName + ' - ' + data.airportCode) + '" data-text="' + (data.airportName + ' - ' + data.airportCode) +
				'" data-country="' + data.airportName + '" data-country-exclusion="' + data.airportName + '">' + (data.airportName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.airportCode + ')') + '</option>';
		}
		return $(str);
	};

	var autocompleteCountryCity = function() {
		// init autocomplete
		var autocompleteEl = body.find('[data-autocomplete]');
		var timerAutocomplete = null;

		var countriesList = globalJson.countriesList.countries;

		if (autocompleteEl.length) {
			autocompleteEl.each(function() {
				var self = $(this),
					inputEl = self.find('input:text');

				if (self.data('init-automcomplete')) {
					return;
				}

				if (self.find('input:text').is('[readonly]')) {
					self.find('input:text').off('focus').off('click');
					return;
				}

				var select = self.find('select');
				var data = [];
				var arrDataValue = [];
				var clearHideaddClear = null;

				self.data('minLength', 2);
				self.data('init-automcomplete', true);
				self.find('input:text').focus(function() {});

				function addData(els) {
					els.each(function() {
						var indexOfAirport = $(this).text().indexOf('(');
						var valueOption = $(this).text().slice(indexOfAirport);
						data.push({
							city: $(this).text(),
							label: $(this).text(),
							value: $(this).data('text'),
							valueOption: valueOption,
							parent: $(this).parent().attr('label'),
							country: $(this).data('country'),
							link: $(this).data('link') || '',
							countryExclusion: $(this).data('country-exclusion')
						});
						arrDataValue.push({
							city: $(this).text(),
							label: $(this).data('text'),
							value: $(this).data('text'),
							valueOption: valueOption,
							country: $(this).data('country'),
							link: $(this).data('link') || '',
							countryExclusion: $(this).data('country-exclusion')
						});
					});
				};

				function optionsAddData(select, options) {
					if (select.data('city-group')) {
						console.log(select.data())
						options.each(function(i, val) {
							var self = $(this);
							var countryItem = self.children();
							if (self.attr('label')) {
								data.push({
									city: self.attr('label'),
									label: self.attr('label'),
									group: self.attr('label'),
									link: self.data('link') || '',
									value: self.attr('label')
								});
							} else {
								data.push({
									city: self.text(),
									label: self.text(),
									value: self.data('text'),
									link: $(this).data('link') || ''
								});
							}

							if (countryItem.length) {
								console.log(":" + JSON.stringify(countryItem));
								addData(countryItem);
							}
						});
					} else {
						options.each(function() {
							data.push({
								city: $(this).text(),
								label: $(this).text(),
								value: $(this).data('text'),
								link: $(this).data('link') || ''
							});
						});
					}
				};

				if (select.closest('#singaporeAirlines, #australiaAirlines').length) {
					var selectEl = select;
					for (var i = 0; i < countriesList.length; i++) {
						selectEl.append(createList(countriesList[i], ''));
					}
					optionsAddData(select, select.children());
				} else {
					optionsAddData(select, select.children());
				}

				select.off('change.selectCity').on('change.selectCity', function() {
					select.closest('[data-autocomplete]').find('input').val(select.find(':selected').data('text'));
				});

				var isFixed = false;
				self.parents().each(function() {
					isFixed = $(this).css('position') === 'fixed';
					return !isFixed;
				});

				_autoComplete({
					autocompleteFields: self.find('input:text'),
					autoCompleteAppendTo: body,

					airportData: data,
					cityData: arrDataValue,
					position: isFixed ? {
						collision: 'flip'
					} : {
						my: 'left top',
						at: 'left bottom',
						collision: 'none'
					},
					response: function(event, ui) {
						if (ui.content.length === 1) {}
						if (!ui.content.length) {
							ui.content.push({
								city: L10n.globalSearch.noMatches,
								label: L10n.globalSearch.noMatches,
								value: null
							});
						}
					},
					close: function() {
						var self = $(this);
						self.closest('[data-autocomplete]').removeClass('focus');
						if (!self.val() || self.val() === self.attr('placeholder')) {
							self.closest('[data-autocomplete]').addClass('default');
						}

						// Fix bug for iPad
						if (/iPad/i.test(window.navigator.userAgent)) {
							setTimeout(function() {
								$(document).scrollTop(win.scrollTop() + 1);
							}, 100);
						}
						if (!self.closest('#singaporeAirlines, #australiaAirlines').length >= 1) {
							clearHideaddClear = setTimeout(function() {
								self.blur();
								self.addClear('hide');
							}, 200);
						}


						// add close icon for book redeem block
						if (self.closest('#singaporeAirlines, #australiaAirlines').length >= 1 && self.val().length >= 1) {
							clearCloseBtn(self);
						} else if (self.closest('#singaporeAirlines, #australiaAirlines').length >= 1 && self.val().length == 0) {
							self.closest('[data-autocomplete="true"]').find('.optim-close').addClass('hidden');
						}

						if (self.data('autopopulate')) {
							$(self.data('autopopulate')).val(self.val()).closest('[data-autocomplete]').removeClass('default');
							if (self.parents('.grid-col').hasClass('error')) {
								$(self.data('autopopulate')).valid();
							}
						}

						if (self.data('autopopulateholder')) {
							self.val(self.val() || self.data('autopopulateholder')).closest('[data-autocomplete]').removeClass('default');
							self.valid();
						}
					},
					select: function(event, ui) {
						// $(this).addClear('hide');
						var self = $(this),
							selfAutoComplete = self.parents('[data-autocomplete]'),
							searchBox = $('input', selfAutoComplete);

						if (ui.item.link) {
							searchBox.data('link', ui.item.link);
						}

						if (!ui.item.value) {
							window.setTimeout(function() {
								self.trigger('blur.triggerByGroupValidate');
							}, 400);
							return;
						} else {
							if (self.closest('#travel-widget').data('widget-v1') || self.closest('#travel-widget').data('widget-v2')) {
								$('#travel-widget .from-select, #travel-widget .to-select').not(selfAutoComplete).removeClass('default');
								if (selfAutoComplete.is('.from-select')) {
									$('#travel-widget .from-select').not(selfAutoComplete).find('input').val(ui.item.value);
								} else if (selfAutoComplete.is('.to-select')) {
									$('#travel-widget .to-select').not(selfAutoComplete).find('input').val(ui.item.value);
								}
							};
						}

						self.closest('[data-autocomplete]').removeClass('default');
						if (self.parents('.from-select').length) {
							originSelected = ui.item.countryExclusion;
							countryOriginSelected = ui.item.value;
							self.closest('.form-group').data('change', true);
							if (self.parents('.from-to-container').find('.to-select').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									}, 100);
								}
							} else {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										//self.blur();
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									}, 100);
								}
							}
						}
						if (self.parents('.to-select').length) {
							destinationSelected = ui.item.countryExclusion;
							countryDestinationSelected = ui.item.value;
							if ($('#travel-radio-4').length && $('#travel-radio-5').length && ($('#travel-radio-4').is(':visible') || $('#travel-radio-5').is(':visible'))) {
								if (self.is('#city-2')) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										if ($('#travel-radio-4').is(':checked')) {
											self.blur();
											setTimeout(function() {
												// $('#travel-start-day').closest('.input-3').trigger('click.showDatepicker');
												$('#travel-start-day').focus();
											}, 300);
										} else if ($('#travel-radio-5').is(':checked')) {
											self.blur();
											setTimeout(function() {
												// self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
												self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').focus();
											}, 300);
										}
									}, 100);
								}
							} else if (self.closest('form').find('[data-return-flight]').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
											self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
										} else {
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.blur();
										setTimeout(function() {
											// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
											if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
												self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
											} else {
												self.closest('form').find('.form-group').find('[data-oneway]').focus();
											}
										}, 300);
									}, 100);
								}
							} else if (self.closest('form').find('.form-group').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.blur();
										setTimeout(function() {
											// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
											if (!$('body').hasClass('multi-city-page')) {
												self.closest('form').find('.form-group').find('[data-oneway]').focus();
											} else {
												self.closest('.form-group').find('[data-oneway]').focus();
											}
										}, 300);
									}, 100);
								}
							} else if (self.parents('.from-to-container').children('[data-trigger-date]').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 100);
								}
							} else if (self.closest('[data-flight-schedule]').length) {
								var dtfs = self.closest('[data-flight-schedule]');
								if (dtfs.find('[data-return-flight]').length) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										dtfs.find('[data-start-date]').focus();
									}, 500);
								} else if (dtfs.find('[data-oneway]').length) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										dtfs.find('[data-oneway]').focus();
									}, 500);
								}
							}
						}
						setTimeout(function() {
							if (self.closest('form').data('validator')) {
								self.valid();
							}
							/*if(initLoading){
							 loadingStatus.hide();
							 }*/
							self.closest('[data-autocomplete]').siblings('.mobile').find('.select__text').text(ui.city).siblings('select').val(ui.value);
							self.trigger('change.programCode');
						}, 200);
					},
					setWidth: 0
				});

				if (inputEl.val() === '' || inputEl.val() === inputEl.attr('placeholder')) {
					self.closest('[data-autocomplete]').addClass('default');
				} else {
					self.closest('[data-autocomplete]').removeClass('default');
				}
			});

			win.off('touchmove.closeAutocomplete').on('touchmove.closeAutocomplete', function() {
				body.find('.focus[data-autocomplete] input').trigger('blur.highlight');
			});
		}
	};

	var checkAvailableFlights = function(listCode, match) {
		var listData = match;
		var filterDataOrigin = [];

		_.each(listCode, function(code, idx) {
			filterDataOrigin = $.grep(listData, function(item) {
				if (idx !== listCode.length - 1) {
					if (item.country) {
						return item.country !== code;
					}
				} else {
					if (item.value) {
						return item.value !== code;
					}
				}
				return item;
			});
			listData = filterDataOrigin;
		});

		return listData;
	};

	var checkGroupNoItem = function(listData) {
		var isCity = true;

		_.each(listData, function(data) {
			isCity = data.value ? true : false;
		})
		if (!isCity) listData = [];

		var newArr = [];
		_.each(listData, function(data) {
			!data.value ? newArr.push(data.label) : '';
			// data.value === data.label ? newArr.push(data.label) : '';
		})

		_.each(newArr, function(country) {
			var hasCity = false;
			for (var i = 0; i < listData.length; i++) {
				if (listData[i].parent === country) {
					hasCity = true;
					break;
				}
			}
			if (!hasCity) {
				_.each(listData, function(data, i) {
					if (data.label === country) {
						listData.splice(i, 1);
					}
				});
			}
		});
		return listData;
	};

	var resetAvailableFlight = function() {
		$('[data-autocomplete]').each(function() {
			var _self = $(this),
				inputEl = _self.find('input'),
				clearTextEl = _self.find('.add-clear-text'),
				timeout;
			if (timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function() {
				if (_self.data('origin')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							originSelected = '';
							countryOriginSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						originSelected = '';
						countryOriginSelected = '';
					});
				}
				if (_self.data('destination')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							destinationSelected = '';
							countryDestinationSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						destinationSelected = '';
						countryDestinationSelected = '';
					});
				}
			}, 1000);

		});
	};
	autocompleteCountryCity();
};
