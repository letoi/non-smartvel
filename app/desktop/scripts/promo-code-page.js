var SIA = SIA || {};

SIA.PromoCode = function () {
    var global = SIA.global;
    var jsonUrl = "";
    var listLimit = 3;

    var promoClass = [
        'first-code-info',
        'middle-code-info',
        'last-code-info'
    ];

    var promoCodeTpl = '<div class="static-details__content <%- className %>">\
        <div class="content content-promo">\
        <div class="inner-code">\
            <h3><%- promoCode.toUpperCase() %></h3>\
            <span class="bar-orange"></span>\
        </div>\
        <p class="promo-end">VALID TILL <%- validTill.toUpperCase() %></p>\
        <p class="promo-info"><%- promoDesc.toUpperCase() %></p>\
        <a href="#">APPLY</a>\
        </div>\
    </div>';

    var init = function () {
        jsonUrl = SIA.URLParamParser.getURLParams("json");
        promoValidator();
        renderTPL();
    };

    var renderTPL = function() {
        var contentBody = $(".promotion-content");
        getJSONData(function(promos) {
            _.each(promos, function(promo, idx) {
                var className = promoClass[idx % 3];
                var promoCodeTplRendered = $(_.template(promoCodeTpl, {
                    className: className,
                    promoCode: promo.promoCode,
                    validTill: promo.validTill,
                    promoDesc: promo.promoDesc
                }));
                contentBody.append(promoCodeTplRendered);
            });
        });
    };

    var getJSONData = function(cb) {
        $.get("ajax/" + jsonUrl, function(response) {
            cb(response.promoCodeDeals);
        });
    };

    var promoValidator = function () {
        var formPromo = $(".form--promo");
        formPromo.off().on("submit", function(evt) {
            evt.preventDefault();
            validatePromoCode(formPromo);
        });
        
        $(".form--promo").validate({
            onfocusout: false,
            onkeyup: false,
            errorPlacement: global.vars.validateErrorPlacement
        });
    };

    var onInputChanges = function(inputs, validator) {
        $(inputs.join(",")).on("change paste keyup", function() {
            validator.resetForm();
            $(this).off("change paste keyup");
        });
    };

    var validatePromoCode = function(formPromo) {
        var v = formPromo.validate();
        onInputChanges(["[name='promo-code']"], v);
        $.get("ajax/" + jsonUrl, function(response) {
            if (!$("[name='promo-code']").val()) {
                return;
            }
            
            if (response.promoErrorCode) {
                showError(v);
            } else {
                formPromo.off();
                formPromo.submit();
            }
        });
    };

    var showError = function(validator) {
        validator.showErrors({
            "promo-code": "This promo code is invalid."
        });
    };

    var codeVerifyListener = function () {
        $("#submit-3").off().on("click.verifyCode", function () {
            if ($('#input-4').val()) {
                $(".form--promo, .form-general").addClass("error-promo-code");
                $(".error-promo-msg").removeClass("hidden");
            }
        });
    };

    var pub = {
        init: init
    };

    return pub;
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.PromoCode.init();
        }
    }, 100);
};

$(function () {
    typeof _ == 'undefined' ? waitForUnderscore() : SIA.PromoCode.init();
});
