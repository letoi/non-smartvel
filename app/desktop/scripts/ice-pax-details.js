SIA.IcePaxDetails = function() {
    var passengerModel = {
        id: "",
        index: 0,
        title: "",
        birthday: {
            day: "",
            month: "",
            year: "",
        },
        passportExpiry: {
            day: "",
            month: "",
            year: "",
        },
        contact: {},
        flyerProgramme: {},
        emergencyContact: {},
        useApis: {},
        passportRegion: "",
        nationality: "",
        firstName: "",
        lastName: "",
        visaCitizenship: "",
        prArCard: {
            usePassportExpiry: true,
            expire: {}
        },
        visitor: {}
    };
    var monthFormat = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

    var templateInstance;
    var templateData = {
        passengers: [],
        flights: [],
        platform: "desktop",
        stickySidebar: true,
        passenger: Object.assign({}, passengerModel)
    };
    var bus;

    var getPassengerData = function() {
        return {
            titles: globalJson.titles,
            day: function() {
                return Array.from(new Array(31), function(val, index) { return (index + 1).toString() });
            }(),
            month: monthFormat,
            nationality: globalJson.nationality,
            year: function() {
                return Array.from(new Array(globalJson.birthdayYear.length), function(val, index) { return (index + globalJson.birthdayYear.yearStart).toString() }).reverse();
            }(),
            expireYear: function() {
                return Array.from(new Array(globalJson.expireYear.length), function(val, index) { return (index + globalJson.expireYear.yearStart).toString() });
            }(),
            cardType: globalJson.cardType,
            region: globalJson.region,
            countryCode: globalJson.countryCode,
            flyerProgramme: globalJson.flyerProgramme,
            usState: globalJson.usState
        }
    };

    var getTemplateMethods = function() {
        return {
            selectPassengerTab: selectPassengerTab,
            checkPassportExpiry: checkPassportExpiry,
            checkNoFirstname: checkNoFirstname,
            onNextPassenger: onNextPassenger,
            checkIfUSBound: checkIfUSBound
        };
    };

    var getTemplateFilters = function() {
        return {
            getPaxType: getPaxType
        };
    };

    var init = function() {
        bus = new Vue();
        Vue.use(VueScrollTo);
        Vue.use(VTooltip);
        Vue.mixin({
            data: function() {
                return {
                    bus: bus
                }
            }
        });
        initComponents();
        SIA.IcePaxValidation.init();
        templateInstance = new Vue({
            el: "#app-container",
            data: templateData,
            mounted: onMount
        });
    };

    var initComponents = function() {
        Vue.component("sidebar", {
            template: "#sidebar-template",
            props: {
                passengers: Object,
                stickySidebar: Boolean,
                value: Object,
                tabSelect: Object
            },
            methods: getTemplateMethods(),
            filters: getTemplateFilters(),
            mounted: onSidebarMounted
        });
        Vue.component("passenger-details", {
            template: "#passenger-details-template",
            props: {
                passengers: Object,
                flights: Object,
                value: Object
            },
            mounted: onPassengerComponentMount,
            watch: function() {
                return {
                    value: function(val) {
                        this.$emit("reset:validation");
                    }
                }
            }(),
            data: getPassengerData,
            methods: getTemplateMethods()
        });
        Vue.directive("clear", {
            bind: function(el, binding, node) {
                setTimeout(function() {
                    var closeIcon = el.querySelectorAll(".ico-cancel-thin")[0];
                    if (closeIcon) {
                        $(closeIcon).off().on("click", function() {
                            node.context.bus.$emit("clear:text", binding.value.key.split("."));
                        });
                    }
                }, 1000);
            },
        });
    };
    // Main Component
    var onMount = function() {
        var $this = this;
        $.get("ajax/JSONS/ICE/ICE_PaxDetailsJson.json", function(response) {
            var data = response.paxRecordVO;
            $this.passengers = data.passengers.map(function(passenger, idx) {
                passenger.active = (idx == 0) ? true : false;
                passenger.marked = (idx == 0) ? true : false;
                passenger.index = idx + 1;
                passenger.headerTitle = passenger.firstName + " " + passenger.lastName;
                return passenger;
            });
            $this.flights = data.flights;
            var passengerDetails = getPassengerDetails($this.passengers[0]);
            $this.bus.$emit("data:change", passengerDetails);
        });
    };
    // Sidebar Component
    var getPaxType = function(paxType) {
        var paxTypes = [
            {
                type: "A",
                description: "Adult"
            },
            {
                type: "C",
                description: "Child"
            },
            {
                type: "IN",
                description: "Infant"
            }
        ];

        return (_.findWhere(paxTypes, { type: paxType }) || {}).description;
    };

    var selectPassengerTab = function(passenger, moveToNext) {
        if (!passenger.isDone && !passenger.active && !moveToNext && !passenger.marked) {
            return;
        }
        resetActivePassenger(this.passengers);
        passenger.active = true;
        passenger.marked = true;
        var passengerDetails = getPassengerDetails(passenger);
        this.$scrollTo(".block-content-passenger-details");
        this.bus.$emit("data:change", passengerDetails);
    };

    var getPassengerDetails = function(passenger) {
        if (passenger.isParsed) {
            return passenger;
        }
        var birthday = passenger.dateOfBirth.split("-").map(function(num) { return parseInt(num) });
        var passport = passenger.passport.expiryDate.split("-").map(function(num) { return parseInt(num) });
        var contact = passenger.contactDetails;
        var redress = getApisType(passenger.prCard, "redress");
        var knownTraveller = getApisType(passenger.prCard, "knownTraveller");
        var details = {
            isParsed: true,
            id: passenger.passengerID,
            headerTitle: passenger.headerTitle,
            index: passenger.index,
            title: passenger.title,
            passengerType: passenger.passengerType,
            active: passenger.active,
            marked: passenger.marked,
            firstName: passenger.passport.firstName,
            lastName: passenger.passport.lastName,
            gender: passenger.gender,
            birthday: {
                day: birthday[2].toString(),
                month: monthFormat[birthday[1] - 1],
                year: birthday[0].toString(),
            },
            passportExpiry: {
                day: passport[2].toString(),
                month: monthFormat[passport[1] - 1],
                year: passport[0].toString()
            },
            nationality: passenger.passport.nationality,
            passportRegion: passenger.passport.issuingCountry,
            passportNumber: passenger.passport.number,
            contact: {
                code: (contact) ? passenger.contactDetails.mobile.mobileCountryCode : "",
                number: (contact) ? passenger.contactDetails.mobile.mobileNumber : ""
            },
            useApis: {
                redressNumber: redress ? redress.number : "",
                knownTravelerNumber: knownTraveller ? knownTraveller.number : "",
                countryOfResidence: passenger.countryOfResidence,
            },
            email: passenger.email,
            visaCitizenship: "US"
        };
        return Object.assign({}, passengerModel, details);
    };

    var onSidebarMounted = function() {
        var $this = this;
        this.bus.$on("sidebar:change", function(index) {
            selectPassengerTab.call($this, $this.passengers[index], true);
        });
    };
    // Passenger Form Component
    var checkPassportExpiry = function() {
        if (this.value.passportExpiry.day && 
            this.value.passportExpiry.month && 
            this.value.passportExpiry.year
        ) {
            var destinationLeg = this.flights[this.flights.length - 1];
            var flightSchedule = dateTimeSplit(destinationLeg.scheduledArrivalDateTime);
            var year = parseInt(this.value.passportExpiry.year);
            var day = parseInt(this.value.passportExpiry.day);
            var month = monthFormat.indexOf(this.value.passportExpiry.month) + 1;
            var addMonths = 0;
            var dayDifference = 0;
            var yearDifference = year - flightSchedule.year;
            var calcMonths = 0;
            if (yearDifference > 1) {
                return true
            } else if (year - flightSchedule.year < 0) {
                return false;
            }

            if (day - flightSchedule.day < 0) {
                dayDifference ++;
            }
            if (yearDifference == 1) {
                addMonths += month;
                calcMonths = 12 - flightSchedule.month;
                calcMonths += addMonths - dayDifference;
            } else {
                calcMonths = month - flightSchedule.month - dayDifference;
            }
            return calcMonths >= 6;
        } else {
            return true;
        }
    };

    var checkNoFirstname = function(state) {
        this.value.firstName = (!state) ? "" : this.value.firstName;
    };

    var onNextPassenger = function() {
        var $this = this;
        var index = this.value.index;
        this.value.headerTitle = this.value.firstName + " " + this.value.lastName;
        this.passengers[index - 1] = this.value;
        removePristine(this.$el);
        this.$forceUpdate();
        setTimeout(function() {
            var errorQuery = $this.$el.querySelectorAll(".error")[0];
            if (errorQuery && errorQuery.clientWidth) {
                $this.$scrollTo(errorQuery);
            } else {
                if (!$this.passengers[index]) return $this.$refs.formSubmit.submit();
                $this.value.isDone = true;
                $this.passengers[index - 1] = $this.value;
                $this.bus.$emit("sidebar:change", index);
            }
        }, 100);
    };

    var onPassengerComponentMount = function() {
        var $this = this
        this.bus.$on("data:change", function(data) {
            $this.$emit("input", data);
        });
        this.bus.$on("clear:text", function(key) {
            switch(key.length) {
                case 1:
                    $this.value[key[0]] = "";
                    break;
                case 2:
                    $this.value[key[0]][key[1]] = "";
                    break;
            }
        });
    };

    var checkIfUSBound = function() {
        var flightLength = this.flights.length;
        if (!flightLength) return false;

        return this.flights[flightLength - 1].destination.countryCode.toLowerCase() == "us";
    };
    // UTILITY
    var removePristine = function(parent) {
        _.each(parent.querySelectorAll(".pristine"), function(el) {
            if (el.clientWidth) {
                el.removeClass("pristine");
            }
        });
    };
    var resetActivePassenger = function(passengers) {
        _.chain(passengers)
            .filter(function(passenger) {
                if (passenger.active) return passenger;
            })
            .each(function(passenger) {
                passenger.active = false;
            });
    };
    var dateTimeSplit = function(date) {
        var dateTime = date.split("T");
        var dateArray = _.map(dateTime[0].split("-"), function(num) {
            return parseInt(num, 10);
        });
        var timeArray = _.map(dateTime[1].split(":"), function(num) {
            return parseInt(num, 10);
        });

        return {
            year: dateArray[0],
            month: dateArray[1],
            day: dateArray[2],
            hours: timeArray[0],
            minutes: timeArray[1]
        }
    };
    var getApisType = function(data, type) {
        return _.findWhere(data, {type: type});
    };

    return {
        init: init
    }
}();

SIA.IcePaxValidation = function() {

    var init = function() {
        Vue.directive("validate", {
            twoWay: true,
            bind: function(el, binding, node) {
                node.context.$on("reset:validation", function() {
                    var inputEl = el.querySelectorAll("input")[0];
                    el.addClass("pristine");
                    $(el).off().on("click", addClick.bind(el, binding));
                    $(inputEl).off().on("blur", onBlur.bind(el, binding));
                });
            }, 
            update: function(el, binding) {
                validate(el, binding);
            }
        });
    };

    var addClick = function() {
        $(this).off("click");
        this.removeClass("pristine");
    };

    var onBlur = function(binding) {
        var inputEl = this.querySelectorAll("input")[0];
        if (!inputEl.disabled && !inputEl.value) {
            binding.value.observe = inputEl.value;
            binding.oldValue = {};
            validate(this, binding);
        }
    };

    var resetValidate = function(el) {
        setTimeout(function() {
            el.querySelectorAll(".text-error")[0].addClass("hidden");
            el.removeClass("error");
        }, 500);
    };

    var validate = function (el, binding) {
        var isValid = true;
        if (binding.oldValue.observe == binding.value.observe &&
            el.hasClass("pristine")
        ) {
            return;
        }
        var observeValue = binding.value.observe;
        var validations = binding.value.rules;
        var errorClassDisplay = "";
        for (var i = 0; i < validations.length; i++) {
            var validationResult = getValidation(validations[i])(observeValue, binding.value.data);
            if (!validationResult) {
                errorClassDisplay = validations[i];
                isValid = false;
                break;
            }
        }
        
        if (binding.value.forceDisable) {
            isValid = true;
        }
        _.each(el.querySelectorAll(".text-error"), function (i) {
            i.addClass("hidden")
        });

        if (!isValid) {
            el.querySelectorAll(".text-error." + errorClassDisplay + "-error")[0].removeClass("hidden");
        }
        el[isValid ? "removeClass" : "addClass"]("error");
    };

    var getValidation = function(validation) {
        var validationFn = {
            "required": requiredValidation,
            "email": emailValidation,
            "minlength": minLengthValidation,
            "alphanumeric": alphanumericValidation,
            "digits": numericValidation
        };

        return validationFn[validation];
    };

    var requiredValidation = function(value) {
        return (value || "").length != 0;
    };

    var emailValidation = function(value) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
        return regex.test(value || "");
    };
    
    var minLengthValidation = function(value, data) {
        return value.length >= data.minlength;
    };

    var alphanumericValidation = function(value) {
        var regex = new RegExp("^[a-zA-Z0-9]*$", "i");
        return regex.test(value || "");
    };

    var numericValidation = function(value) {
        var regex = new RegExp("^[0-9]*$");
        return regex.test(value || "");
    };
    // UTILITY
    var escapeRegExp = function (str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };
    return {
        init: init,
        emailValidation: emailValidation
    }
}();

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof Vue == "undefined") {
            return waitForLibraries(fn);
        }

        fn.init();
    }, 100);
};

$(function() {
    waitForLibraries(SIA.IcePaxDetails);
});