SIA.kfFavouritesDestination = function() {


     var p = {};

       p.global = SIA.global;
         
     
       $(document).on('click', '#addDestinationBtn', function(){
        p.templateDivAddDestination = '<div class="add-destination-block">\
                <p class="title-5--blue">Add a destination to Favourites</p>\
                <div data-validate-by-group="false" class="form-group grid-row from-to-container">\
                  <div class="grid-col one-half">\
                    <div class="grid-inner">\
                      <div data-autocomplete="true" class="custom-select custom-select--2 to-select default" data-destination="true">\
                        <label for="city-2" class="select__label"></label><span class="select__text">\
                        <input type="text" name="city-2" id="city-2" placeholder="Select city" value="" aria-labelledby="city-2-error" class="destination-city" data-rule-required="true" data-msg-required="Select a destination." data-msg-notequalto="Select another origin or destination."/></span><span class="ico-dropdown">Select City</span>\
                        <label for="cib-flight1" class="hidden">&nbsp;\
                          <select id="cib-flight1" data-city-group="true">\
                  </select>\
                        </label>\
             </div>\
                    </div>\
                  </div>\
                </div>\
                 <div class="button-group-1">\
                  <input type="submit" class="btn-1 right" value="SAVE">\
                  <a href="#" class="btn-2 right">Cancel</a>\
                </div>\
              </div>'


              $('#divAddDestination').html(_.template(p.templateDivAddDestination))
              $('.add-destination-btn').addClass('hidden') 
       });

      //  var clickOnStar = function(){
      //    $(document).on('click', '.star-1', function(){
      //     var starStatus = $(this).attr('src');
      //     var starLoc = $(this).attr('data-starDiv');
      //     var divRemoveLoc = $(this).attr('data-divSize');
      //     console.log(starLoc);
      //     console.log('star status' + starStatus);

      //     if(starStatus === "desktop/images/star-icon1.png"){
      //     $(this)[0].setAttribute('src', 'images/star-icon2.png');  
      //     }else if(starStatus === "images/star-icon2.png"){
      //         p.templateRemoveDestination = '<p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
      //               </a><div class="button-group-1"><a href="#" data-accordion-trigger="1" class="promotion-item__content active" aria-expanded="true">\
      //                 <input type="submit" class="btn-1 right" value="CONFIRM">\
      //                 </a><a href="#" class="btn-2 right">Cancel</a>\
      //             </div>'
      //          if(starLoc == "upper"){
      //             if(divRemoveLoc == "left")
      //              $('.one-third').html(_.template(p.templateRemoveDestination))
      //          }
              
               
      //            $(this)[0].setAttribute('src', 'images/star-icon1.png');  

      //     }

      //   });
      //  }


      // clickOnStar();

      $(document).on('click','.star-1', function(){
        var imgType = $(this).data('type');

        var starStatus = $(this).attr('src');
         var starBlank = "desktop/images/star-icon1.png";
         var starYellow = "desktop/images/star-icon2.png"
        if(starStatus === starBlank){
          $(this)[0].setAttribute('src', 'desktop/images/star-icon2.png');
        }else{
        console.log("remove Favourites");
          p.removeDestinationModal = '<section data-accordion-wrapper-content="1" data-loading-view="3" class="promotion-list">\
            <article data-accordion="1" class="promotion-item promotion-item--2 fadeIn animated two-third">\
              <div class="promotion-item__inner">\
                <a href="#" aria-expanded="true" class="flight-item"><img src="images/image-4.png" alt="photo-1" longdesc="img-desc.html">\
                  <div class="country-name">\
                    <h3>Tokyo</h3>\
                  </div>\
                 <div class="ico-container"><img class="star-1" src="desktop/images/star-icon.png" data-type="two-third"></div>\
                </a>\
                <a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                  <div class="promotion-item__desc">\
                    <span class="avail-fare-deals">new fare deals available</span>\
                    <p>Economy return from</p>\
                    <h3 class="price">SGD 888<span>*</span></h3>\
                    <em class="ico-point-r">&nbsp;</em>\
                  </div>\
                  </div>\
              </div>\
            </article>\
           <div class="static-item-1 one-third">\
                <div class="static-item-1__info">\
                  <a href="#">\
                    <figure><img src="images/image-2.png" alt="photo-1" longdesc="img-desc.html">\
                    </figure>\
                     <div class="country-name">\
                      <h3>Tokyo</h3>\
                    </div>\
                    <div class="block-2 static-item-1__detail">\
                      <span class="avail-fare-deals">new fare deals available</span>\
                      <p>Economy return from</p>\
                      <h3 class="price">SGD 888<span>*</span></h3><span class="static-item-1__link"><em class="ico-point-r"></em></span>\
                    </div>\
                    </a><div class="overlay-delete two-thirds remove-favourites"><a href="#">\
                      <p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
                      </a><div class="button-group-1"><a href="#">\
                        </a><a href="#" class="btn-2 right">Cancel</a>\
                        <input type="submit" class="btn-1 right" value="CONFIRM">\
                      </div>\
                    </div>\
                </div>\
              </div>\
          </section>';

        $('#divDestinationLocationUpper').html(_.template(p.removeDestinationModal))
          $(this)[0].setAttribute('src', starBlank);

        }

      });


        $(document).on('click', '.btn-2', function(){
        initDivDestination();
        });


        var initDivDestination = function(){

          p.telmplateDestinationFavouritesUpper = '<div data-accordion-wrapper="1" class="promotion-result--enhance">\
             <section data-accordion-wrapper-content="1" data-loading-view="3" class="promotion-list"  id="divOneUpper">\
            <article data-accordion="1"" data-divLoc="upper" class="promotion-item promotion-item--2 fadeIn animated two-third" data-divSize="left">\
              <div class="promotion-item__inner">\
                <a href="#" aria-expanded="true" class="flight-item div-one"><img src="images/image-4.png" alt="photo-1" longdesc="img-desc.html">\
                  <div class="country-name">\
                    <h3>singapore</h3>\
                  </div>\
                 <div class="ico-container"><img class="star-1" data-starDiv="upper" src="desktop/images/star-icon1.png" data-type="two-third"></div>\
                </a>\
                <a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                  <div class="promotion-item__desc">\
                    <span class="avail-fare-deals">new fare deals available</span>\
                    <p>Economy return from</p>\
                    <h3 class="price">SGD 888<span>*</span></h3>\
                    <em class="ico-point-r">&nbsp;</em>\
                  </div>\
                  </a><div class="overlay-delete"><a href="#" data-accordion-trigger="1" class="promotion-item__content active" aria-expanded="true">\
                    <p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
                    </a><div class="button-group-1"><a href="#" data-accordion-trigger="1" class="promotion-item__content active" aria-expanded="true">\
                      <input type="submit" class="btn-1 right" value="CONFIRM">\
                      </a><a href="#" class="btn-2 right">Cancel</a>\
                    </div>\
                  </div>\
              </div>\
            </article>\
            <article data-accordion="1" data-divLoc="upper" class="promotion-item promotion-item--2 fadeIn animated one-third" data-divSize="right">\
              <div class="promotion-item__inner">\
                <a href="#" aria-expanded="false" class="flight-item"><img src="images/image-2.png" alt="photo-1" longdesc="img-desc.html">\
                  <div class="flight-item__vignette">&nbsp;</div>\
                  <div class="country-name">\
                    <h3>Japan</h3>\
                  </div>\
                 <div class="ico-container"><img class="star-1" data-starDiv="upper" src="desktop/images/star-icon1.png" data-type="one-third"></div>\
                </a>\
                <a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                  <div class="promotion-item__desc">\
                    <span class="avail-fare-deals">new fare deals available</span>\
                    <p>Economy return from</p>\
                    <h3 class="price">SGD 888<span>*</span></h3>\
                    <em class="ico-point-r">&nbsp;</em>\
                  </div>\
                  </a><div class="overlay-delete"><a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                    <p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
                    </a><div class="button-group-1"><a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                      <input type="submit" class="btn-1 right" value="CONFIRM">\
                      </a><a href="#" class="btn-2 right">Cancel</a>\
                    </div>\
                  </div>\
              </div>\
              <div data-accordion-content="1" class="promotion-item__detail" style="left: 0px; width: 944px; display: none;">\
                <h3 class="title">Mumbai to Singapore</h3>\
                <div class="editor">\
                  \
                  <table data-sort-content="true" class="fare-basic-table" data-room-table="true">\
                    <caption class="hidden">Promotion item descriptions</caption>\
                    <thead class="hidden-mb">\
                      <tr>\
                        <th class="th-5" scope="col">Prices from</th>\
                        <th class="th-5 advance-purchase" scope="col">Advanced purchase</th>\
                        <th class="th-5" scope="col">Book by</th>\
                        <th class="th-5" scope="col">Travel period</th>\
                        <th class="th-5" scope="col">Min. pax to go</th>\
                        <th class="th-5" scope="col"><span class="ui-helper-hidden-accessible">Table Heading</span></th>\
                      </tr>\
                    </thead>\
                    <tbody>\
                        <tr>\
                          <td data-th="Prices from" aria-describedby="wcag-currency-0-0"> INR 22,000<sup>*</sup><span class="info-trip-type">&nbsp;Return</span><span class="ui-helper-hidden-accessible" id="wcag-currency-0-0"> INR 22,000*</span></td>\
                         <td data-th="Advanced purchase" aria-describedby="wcag-advance-0-0">60 days<span class="ui-helper-hidden-accessible" id="wcag-advance-0-0">60 days</span></td>\
                          <td data-th="Book by" class="book-by" aria-describedby="wcag-bookby-0-0">31 Mar 2017<span class="ui-helper-hidden-accessible" id="wcag-bookby-0-0">31 Mar 2017</span></td>\
                          <td data-th="Travel period" aria-describedby="wcag-duration-0-0">Tue 01 Nov 2016 - Mon 30 Apr 2018<span class="ui-helper-hidden-accessible" id="wcag-duration-0-0">Tue 01 Nov 2016 - Mon 30 Apr 2018</span></td>\
                          <td data-th="Min pax to go" class="min-pax" aria-describedby="wcag-minpaxtogo-0-0">\
                            <span class="min-pax-hide">3</span>\
                            <ul class="min-to-go">\
                                <li><em class="ico-user"></em></li>\
                                <li><em class="ico-user"></em></li>\
                                <li><em class="ico-user"></em></li>\
                            </ul>\
                            <span class="ui-helper-hidden-accessible" id="wcag-minpaxtogo-0-0">3</span>\
                          </td>\
                          <td>\
                            <a href="/en_UK/in/special-offers/flight-from-Mumbai-to-Singapore_49701/" target="_blank" class="btn-1">Book Now</a>\
                          </td>\
                        </tr>\
                    </tbody>\
                  </table>\
                </div><a href="#" class="close-btn">Close<em class="ico-close"></em></a>\
              </div>\
            </article>\
          </section>\
          </div>'





          p.templateDestinationDown = '   <div data-accordion-wrapper="1" class="promotion-result--enhance">\
          <section data-accordion-wrapper-content="1" data-loading-view="3" class="promotion-list">\
            <article data-accordion="1" class="promotion-item promotion-item--2 fadeIn animated one-third" data-divSize="right">\
              <div class="promotion-item__inner">\
                <a href="#" aria-expanded="true" class="flight-item"><img src="images/image-2.png" alt="photo-1" longdesc="img-desc.html">\
                  <div class="country-name">\
                    <h3>Saudi</h3>\
                  </div>\
                  <div class="ico-container"><img class="star-1" data-starDiv="bottom" src="desktop/images/star-icon1.png" data-type="one-third"></div>\
                </a>\
                <a href="#" data-accordion-trigger="1" class="promotion-item__content active" aria-expanded="true">\
                  <div class="promotion-item__desc">\
                    <p>Economy return from</p>\
                    <h3 class="price">SGD 888<span>*</span></h3>\
                    <em class="ico-point-r">&nbsp;</em>\
                  </div>\
                  <div class="overlay-delete">\
                    <p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
                    <div class="button-group-1">\
                      <input type="submit" class="btn-1 right" value="CONFIRM">\
                      <a href="#" class="btn-2 right">Cancel</a>\
                    </div>\
                  </div>\
                </a>\
              </div>\
            </article>\
            <article data-accordion="1" class="promotion-item promotion-item--2 fadeIn animated two-third" data-divSize="left">\
              <div class="promotion-item__inner">\
                <a href="#" aria-expanded="false" class="flight-item"><img src="images/image-4.png" alt="photo-1" longdesc="img-desc.html">\
                  <div class="flight-item__vignette">&nbsp;</div>\
                  <div class="country-name">\
                    <h3>Malaysia</h3>\
                  </div>\
                  <div class="ico-container"><img class="star-1" data-starDiv="bottom" src="desktop/images/star-icon1.png" data-type="two-third"></div>\
                </a> \
                <div>\
                <div class="search-flight-container">\
                  <span class="no-fare">no fare available</span>\
                  <p class="flight-search"><a href="#" class="link-8"><em class="ico-point-r right-arrow"></em>Search flights</a></p>\
                </div>\
                <a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">\
                  <div class="promotion-item__desc">\
                  </div>\
                  <div class="overlay-delete">\
                    <p class="title-5--blue">Are you sure you want to remove Amsterdam from Favourites?</p>\
                    <div class="button-group-1">\
                      <input type="submit" class="btn-1 right" value="CONFIRM">\
                      <a href="#" class="btn-2 right">Cancel</a>\
                    </div>\
                  </div>\
                </a>\
              </div>\
            </article>\
          </section>\
          </div>'

          $('#divDestinationLocationUpper').html(_.template(p.telmplateDestinationFavouritesUpper))
          $('#divDestinationLocationDown').html(_.template(p.templateDestinationDown));
         
        };


     initDivDestination();
  }();