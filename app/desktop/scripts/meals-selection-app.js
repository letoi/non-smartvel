SIA.MealsSelection = function () {
	var undoMeal;
	var p = {};
	var mealCode = {
		SPML: "Special Meals",
		BTC: "Book the Cook"
	};

	p.global = SIA.global;
	p.body = p.global.vars.body;
	p.win = p.global.vars.win;
	p.htmlBody = $('html, body');
	p.stickySidebar = SIA.stickySidebar;

	// Declare templates
	p.removeButton = '<a class="meal-menu-remove-button" data-remove-trigger="1" href="#"> Remove</a>';
	p.accordionWrapperTpl = '<div id="accordionWrapper" class="meals-accordion-wrapper <%= initClass %>" />';
	p.fltBlkTpl = '<div class="flightmeal-control" />';
	p.fltTitleTpl = '<h3 class="title-4--blue" />';
	p.accordionWrapTpl = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content"></div></div>';
	p.accordionItemWrapTpl = '<div data-accordion="1" data-accordion-index="<%= id %>" class="block-2 accordion accordion-box" />';
	p.accordionTriggerTplOld = '<a href="#" data-accordion-trigger="1" class="accordion__control">\
          <div class="fm-accordion-label">\
            <ul class="meal-menu">\
              <li class="flight-ind"><%= origin %> - <%= destination %></li>\
              <li class="meal-service-name"><%= mealType %></li>\
              <li class="meal-sub-title-black" data-label="<%= menuSelectionLbl %>"><%= menuSelection %></li>\
            </ul>\
            <span class="fm-ico-text select-change-container"><%= label %></span>\
            <em class="ico-point-d"></em>\
          </div>\
		</a>\
		<div class="remove-prompt hidden">\
			<em class="ico-alert"></em>\
			<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod ternpor incididunt ut labore et dolore. </p> \
			<div class="button-group-1">\
				<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
				<a href="#" class="btn-2 btn-cancel">Cancel</a>\
			</div>\
		</div>';
	p.accordionTriggerTpl = '<a href="#" data-accordion-trigger="1" class="accordion__control <%= (isScoot) ? "bah-accordion-block" : "" %>">\
		<div class="<%= (!isScoot) ? "hidden" : "" %> bah-label-block">\
			<img class="bah-logo" src="images/svg/tr.svg" alt="tr Logo" longdesc="img-desc.html">\
			<span class="bah-text">Scoot-operated flight</span>\
		</div>\
	  <div class="fm-accordion-label">\
		<ul class="meal-menu">\
		  <li class="flight-ind"><%= origin %> - <%= destination %></li>\
		  <li class="meal-service-name"><%= mealType %></li>\
		  <li class="meal-sub-title-black" data-label="<%= menuSelectionLbl %>"><%= menuSelection %></li>\
		</ul>\
		<span class="fm-ico-text select-change-container"><%= label %></span>\
		<em class="ico-point-d"></em>\
	  </div>\
	</a>\
		  <div class="remove-prompt hidden">\
		 <p class="meal-sub-title-blue"> When you remove this meal selection, note that there will not be a refund for\
		 the meal which you&apos;ve previously paid for. You can select another meal to replace this. </p> \
		 <div class="button-group-1">\
			<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
			<a href="#" class="btn-2 btn-cancel">Cancel</a>\
		</div>\
	  </div>';
	p.accordionContentTpl = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
            <div class="meals-main-menu-wrap all-meals-menu">\
                <hr class="hr-line"/>\
                <h3 class="fm-title-4--blue">Select a meal</h3>\
                <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                    <article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %>">\
                        <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available">\
                                        </div>\
                                    </div>\
                                    <img src="<%= inflImg %>" alt="<%= inflDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span class="view-only-btn"> VIEW ONLY </span>\
                                        <span><%= inflLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                    <article class="promotion-item promotion-item--1">\
                        <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available">\
                                        </div>\
                                    </div>\
                                    <img src="<%= btcImg %>" alt="<%= btcDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span><%= btcLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                    <article class="promotion-item promotion-item--1">\
                        <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available"></div>\
                                    </div>\
                                    <img src="<%= spmlImg %>" alt="<%= spmlDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span><%= spmlLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                </div>\
                <hr>\
                <div class="not-eating-supper">\
                    <div class="custom-checkbox custom-checkbox--1">\
                        <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                        <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <%= mealType %></label>\
                        <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&apos;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                        Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
                    </div>\
                </div>\
            </div>\
            <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
                <hr>\
                <div class="fm-backtomeals-btn"></div>\
                <div class="menu-container"></div>\
                <div class="fm-backtomeals-btn floater">\
                    <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
                </div>\
            </div>\
            <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
                <hr>\
                <div class="fm-backtomeals-btn"></div>\
                <h2 class="fm-title-blue">Special Meals</h2>\
                <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
                before departure, or 48 hours for nut-free and Kosher meals.</p>\
                <div class="tab-wrapper">\
                    <div class="tab-content active">\
                        <div class="form-seatmap responsive special-meals"></div>\
                    </div>\
                </div>\
                <div class="fm-backtomeals-btn floater">\
                    <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
                </div>\
            </div>\
        </div>';

	p.accordionContentTplNoImage = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
            <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                <article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %> no-image">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label">\
                                    <span class="view-only-btn"> VIEW ONLY </span>\
                                    <span><%= inflLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= inflDesc %></p>\
                                    <p class="not-available-text">The menu will be available for selection from 7 September onwards</p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label">\
                                    <span><%= btcLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= btcDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label">\
                                    <span><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= spmlDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
            <hr>\
            <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <%= mealType %></label>\
                    <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&apos;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                    Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
                </div>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <h2 class="fm-title-blue">Special Meals</h2>\
            <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
            before departure, or 48 hours for nut-free and Kosher meals.</p>\
            <div class="tab-wrapper">\
                <div class="tab-content active">\
                    <div class="form-seatmap responsive special-meals"></div>\
                </div>\
            </div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
    </div>';

	//this is for the flights items outbound inbound
	p.accordionContentTplEco = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr class="hr-line"/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
			<div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
				<% if (false) { %>\
                <article class="promotion-item promotion-item--1">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item inflight-economy-hr">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label ml-inflight-container">\
                                    <span class="inflight-inner-container">\
                                        <%= inflLabel %> - Select your meal on board your flight\
                                        <span class="pull-right select-label">Select</span>\
                                        <span class="inflight-badge hidden">Selected</span>\
                                    </span>\
                                </div>\
                                <div>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
				</article>\
				<% } %>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label epe-meal-container ml-inflight-container">\
                                    <span class="special-meal-inner-container"><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
									<p class="meal-category-desc"><%= spmlDesc %></p>\
									<span class="inflight-badge hidden">Selected</span>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
            <hr class="hr-line">\
            <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <%= mealType %></label>\
                    <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&apos;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                    Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
                </div>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <h2 class="fm-title-blue">Special Meals</h2>\
            <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
            before departure, or 48 hours for nut-free and Kosher meals.</p>\
            <div class="tab-wrapper">\
                <div class="tab-content active">\
                    <div class="form-seatmap responsive special-meals"></div>\
                </div>\
            </div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
    </div>';

	p.accordionContentTplPey = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr class="hr-line"/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
			<div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
				<% if (false) { %>\
                <article class="promotion-item promotion-item--1">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item inflight-economy-hr">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label ml-inflight-container">\
                                    <span class="inflight-inner-container">\
                                        <%= inflLabel %> - Select your meal on board your flight\
                                        <span class="pull-right select-label">Select</span>\
                                        <span class="inflight-badge hidden">Selected</span>\
                                    </span>\
                                </div>\
                                <div>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
				</article>\
				<% } %>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label epe-meal-container ml-inflight-container">\
                                    <span class="book-the-cook-inner-container"><%= btcLabel %></span>\
                                    <em class="ico-point-r"></em>\
									<p class="meal-category-desc"><%= btcDesc %></p>\
									<span class="inflight-badge hidden">Selected</span>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label epe-meal-container ml-inflight-container">\
                                    <span class="special-meal-inner-container"><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
									<p class="meal-category-desc"><%= spmlDesc %></p>\
									<span class="inflight-badge hidden">Selected</span>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
            <hr class="hr-line">\
            <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <%= mealType %></label>\
                    <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&apos;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                    Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
                </div>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <h2 class="fm-title-blue">Special Meals</h2>\
            <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
            before departure, or 48 hours for nut-free and Kosher meals.</p>\
            <div class="tab-wrapper">\
                <div class="tab-content active">\
                    <div class="form-seatmap responsive special-meals"></div>\
                </div>\
            </div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
    </div>';
	p.accordionContentTplBah = '<div data-accordion-content="1" class="accordion__content all-meals-list bah-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr class="hr-line"/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
            <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner hot-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="meal-label">\
                                    <span class="meal-category-title"><%= htmLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= htmDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner light-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="meal-label">\
                                    <span class="meal-category-title"><%= lmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= lmlDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner premium-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <span class="<%= (isPrslcsPreOrder) ? "" : "hidden" %> pre-order-badge">PRE-ORDER ONLY</span>\
                                <div class="meal-label">\
                                    <span class="meal-category-title"><%= prslcsLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= prslcsDesc %></p>\
                                    <div class="price-range-block">\
                                        <span class="meal-price-label">FROM</span>\
                                        <span class="meal-price">SGD 8.00</span>\
                                    </div>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap hot-meals-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap light-meals-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
		</div>\
		<div class="meal-sub-menu-wrap premium-meals-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
	</div>';
	p.accordionContentTplBahLong = '<div data-accordion-content="1" class="accordion__content all-meals-list bah-meals-list" tabindex="-1">\
		<div class="meal-sub-menu-wrap long-bah-menu-wrap">\
			<hr>\
			<div class="menu-container"></div>\
		</div>\
	</div>';

	p.menuItemBah = '<div class="fm-inf-menu <%- !speciality.length ? "no-dish-icon" : "" %> meal-of-bah">\
            <span class="fm-inf-menu-select-badge">Selected</span>\
			<h4 class="fm-inf-menu-title" aria-hidden="true"><%- mealName %></h4>\
			<% if (price) { %>\
				<span class="meal-price"><%- currency %> <%- price %></span>\
			<% } %>\
            <p class="fm-inf-menu-desc" aria-hidden="true"><%- mealDescription %></p>\
            <div class="fm-footnote-txt">\
				<span class="food-icon-set">\
					<% _.each(speciality, function(special) {%>\
					<% if (icons[special.dishIconId]) { %>\
						<em class="<%- icons[special.dishIconId].className %> fm-footnote-logo"></em>\
					<% } %>\
                    <span class="fm-dish-text">\
                        <%- special.specialityFootNote %>\
					</span>\
					<% }) %>\
                </span>\
                <input type="button" name="select" value="Select" class="fm-select-btn right" data-paxid="<%- paxId %>" data-segment="<%- segmentId %>" data-meal-servicecode="<%- mealServiceCode %>" data-meal-category="<%- mealCategory %>" data-meal-mealcode="<%- mealCode %>" data-meal-menuname="<%- mealName %>" data-meal-price="<%- price %>">\
            </div>\
            <div data-prompt="initial" class="confirmation-prompt hidden">\
              <p class="meal-sub-title-blue"> You have an existing premium meal which will be replaced with this new meal selection. There will not be a refund for the meal which you&apos;ve previously paid for. </p>\
              <div class="button-group-1">\
                <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
                <a href="#" class="btn-2 btn-cancel">Cancel</a>\
              </div>\
            </div>\
        </div>';
	
	p.accordionInfantContentTpl = '<div data-accordion-content="1" class="accordion__content" tabindex="-1">\
            <div class="meal-sub-menu-wrap inflight-menu-wrap infant-meal-wrap">\
                <hr>\
                <div class="infant-meals all-meals-list">\
                    <h2 class="fm-title-blue">Infant Meals</h2>\
                </div>\
                <hr class="hr-bottom">\
                <div class="not-eating-supper">\
                  <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox" tabindex="0">Bring my own meal</label>\
                    <p class="not-eating-msg hidden">Thank you for indicating that you will be bringing your own meal. Our flight attendants will not approach you during mealtime. If you change your mind, however, you still select your meal by returning to this page at least 24 hours before your departure.</p>\
                  </div>\
                </div>\
            </div>\
        </div';
	p.checkinAlert = '<div class="alert-block checkin-alert">\
          <div class="inner">\
            <div class="alert__icon"><em class="ico-alert"> </em></div>\
            <div class="alert__message"><%= checknMsg %></div>\
          </div>\
        </div>';
	p.mealsLabelTpl = '<span class="meals-label btn-small btn-grey">VIEW ONLY</span>';
	p.inflightAlertPrompt = $('<div class="inflight-confirmation-prompt confirmation-prompt">\
        <em class="ico-alert inflight-ico-alert"></em>\
        <p class="meal-sub-title-black">You&apos;ve changed your meal choice to <span>Inflight Menu</span>. <a href="#" class="inflight-link-undo"> <em class="ico-point-r"></em> Undo</a></p>\
      </div>');

	// To be used to index each vertical accordion tab to reference when changing arrow text
	p.accordTabArr = [];
	p.accordionTabIndex = 0;

	p.accordionTabOpenTxt = 'Select / Change';
	p.accordionTabCloseTxt = 'Close';

	p.mealTitleTpl = '<% if (!hideRemove) { %>\
		<span class="title-selected">Selected:</span> \
		<%= mealName %> (<%= mealType %>)\
		<% } else { %>\
			<span><%= mealName %> (<%= mealType %>)</span>\
		<% } %>\
		<% if (!hideRemove) { %>\
		<a class="meal-menu-remove-button" data-remove-trigger="1" href="#">Remove</a>\
		<% } %>';

	p.paxNavCount = 0;
	p.paxArr = [];
	p.inflightMenuExist = [];

	p.ifm = [];
	p.spm = [];
	p.btc = [];
	p.infm = [];

	p.cabinClassCodes = {
		pey: ['P', 'T', 'S'],
		eco: ['N', 'Q', 'W', 'H', 'M', 'E', 'B', 'Y'],
		biz: ['C', 'D', 'J', 'Z', 'U'],
		first: ['R', 'A', 'F']
	};

	p.isPey = false;
	p.isEco = false;
	p.isBiz = false;
	p.isFirst = false;

	// declare dish icon for inflight menu scenario
	p.inflightDishIcons = {
		'ICP': 'ico-4-salmon',
		'DWH': 'ico-4-fork-1',
		'WHS': 'ico-4-fork',
		'EPG': 'ico-4-amet',
		'PLF': 'ico-4-cook',
		'MTL': 'ico-4-fork',
		'VGT': 'ico-4-leaf',
		'LCL': 'ico-4-heart',
		'LCA': 'ico-4-sandwich',
		'LCH': 'ico-4-fork',
		'CNY': 'ico-4-info',
		'XMAS': 'ico-4-pine',
		'DEP': 'ico-4-coffee',
		'TWG': 'ico-4-fork',
		'LFD': 'ico-4-cook'
	};

	// init pax data
	p.data = {};

	p.bspIsDisplayed = false;

	p.notEatingPastData = {};

	p.mealDictionary = {
		"SPML": "Special Meals",
		"BTC": "Book the Cook"
	}

	var init = function () {
		p.json = globalJson.mealsInfo;

		// Assign the initial states
		getInitialStates();

		p.mealSelectionForm = $('#mealSelection');
		p.paxListWrapper = $('#paxListWrapper');
		p.curWinWidth = p.win.width();

		// Add hidden input field in form
		p.mealSelectionForm.prepend(p.input);

		// Update the initial data for the pax
		setupData();
		
		// Paint meals menu
		paintMealsMenu();

		// add event to checkbox
		addCheckboxEvents();

		// Paint pax navigation
		paintPaxNav();

		if ($("body").hasClass("meals-selection-bah")) {
			SIA.clickActivity.init();
		}

		attachSelected();
	};

	var attachSelected = function() {
		$("body").on("update-selection", function(evt, el) {
			el = $(el);
			var wrapper = el.closest(".accordion-box");
			var segmentMeal = p.data.paxList[p.curPaxIndex].selectedMeals[el.data("segment")].selectedMeal[el.data("meal-servicecode")];
			
			wrapper.find(".meal-label").each(function() {
				$(this).removeClass("selected-border")
					.find(".inflight-badge")
					.addClass("hidden");
			});
			if (segmentMeal.mealCategoryCode == "SPML" && segmentMeal.isEating) {
				wrapper
					.find(".special-meals")
					.find(".meal-label")
					.addClass("selected-border")
					.find(".inflight-badge")
					.removeClass("hidden");
			} else if (segmentMeal.mealCategoryCode == "BTC" && segmentMeal.isEating) {
				wrapper
					.find(".book-the-cook-menu")
					.find(".meal-label")
					.addClass("selected-border")
					.find(".inflight-badge")
					.removeClass("hidden");
			}
		});
	};

	var removeButtonListener = function () {
		setTimeout(function () {
			var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
			var mealTitleTpl = SIA.MealsSelection.public.mealTitleTpl;
			var mealDictionary = SIA.MealsSelection.public.mealDictionary;
			var segmentMeals;

			$("[data-accordion-trigger]").find("[data-remove-trigger]").off()
				.on("click", function (evt) {
					evt.preventDefault();
					evt.stopPropagation();

					var self = $(this);
					var wrapper = self.closest(".accordion-box");
					var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
					var removeWrap = wrapper.find(".remove-prompt");
					removeWrap.eq(0).removeClass("hidden");
					self.addClass("hidden");

					// var segmentMealsValues = Object.values(segmentMeals.selectedMeal);
					// segmentMealsValues = _.findWhere(segmentMealsValues, { mealCategoryCode: "SPML", isEating: true });

					// removeWrap.find(".menu-label").text(segmentMealsValues ? segmentMealsValues.mealMenuName : "Inflight Meal");

					removeWrap.find(".btn-cancel")
						.off().on("click", function(evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							removeWrap.addClass("hidden");
							self.removeClass("hidden");
						});

					removeWrap.find(".btn-proceed")
						.off().on("click", function (evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							wrapper.find(".fm-inf-menu.selected").each(function () {
								var self = $(this);
								var mealButton = self.find(".fm-select-btn");
								segmentMeals = pax.selectedMeals[mealButton.data("segment")];
								if (mealButton.data("meal-category") == "SPML") {
									SIA.MealsSelection.resetToInflight(mealButton);
								} else {
									unSelectMeal(mealButton);
								}
								undoMeal = mealButton;
							});

							segmentMealsValues = Object.values(segmentMeals.selectedMeal);
							segmentMealsValues = _.findWhere(segmentMealsValues, { mealCategoryCode: "SPML", isEating: true });
							
							if (segmentMealsValues) {
								var mealBtn = wrapper.find(".fm-select-btn[data-meal-mealcode='" + segmentMealsValues.mealCode + "']");
								SIA.MealsSelection.selectMeal(mealBtn);
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html("");
								wrapper.find(".fm-accordion-label .meal-sub-title-black").append(
									$(_.template(mealTitleTpl, {
										mealName: segmentMealsValues.mealMenuName,
										mealType: mealDictionary.SPML,
										hideRemove: !(p.isEco || p.isPey)
									}))
								);
								SIA.MealsSelection.updateData(mealBtn);
								removeButtonListener();
							} else {
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
								SIA.MealsSelection.removeInputData(wrapper);
							}

							removeWrap.addClass("hidden");
						});
				});
		}, 500);
	};

	var resetToInflight = function(meal) {
		var pax = p.data.paxList[p.curPaxIndex];
		var curSegment = pax.selectedMeals[meal.data("segment")];

		for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
			var spml = SIA.MealsSelection.public.spm[i];

			if (p.curPaxIndex !== spml.p.curPaxId) continue;
			spml.p.specialMealWrap.find('.fm-inf-menu.selected').each(function () {
				var t = $(this).find('.fm-select-btn');
				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				
				var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];
				if (typeof curCatCode !== 'undefined' &&
					curCatCode.isEating &&
					t.attr('data-meal-category') === 'SPML' &&
					(curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {
					// Unselect other selected special meals in same sector
					if (t.attr('data-segment') === meal.attr('data-segment')) {
						var inflLbl = 'Inflight Menu';
						if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
						accordionTabMealTitle.html(inflLbl);
						unSelectMeal(t);
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var setupData = function () {
		// Add hidden input field in form
		var mealsPreselectedBSP = [];
		p.input = $('<input type="hidden" value="" />');
		p.mealSelectionForm.prepend(p.input);

		p.data = {};
		p.data.paxList = [];

		// Loop through the passenger list
		for (var i = 0; i < p.json.paxList.length; i++) {
			var d = p.json.paxList[i];
			var f = p.json.flightSegment;

			var pax = {};
			// Add base pax info
			pax.paxId = d.paxId;
			pax.paxType = d.paxType;
			pax.paxName = d.paxName;

			// Add selected meals array
			// to hold each flightSector's mealServiceCode
			pax.selectedMeals = [];
			pax.preselectedMeals = [];

			for (var j = 0; j < f.length; j++) {
				var segment = {
					'segmentId': f[j].segmentId,
					'selectedMeal': {}
				}

				// loop through the sectors
				for (var k = 0; k < f[j].flightSector.length; k++) {
					var paxServiceType = getServiceType(i);
					var serviceTypes = f[j].flightSector[k][paxServiceType];
					
					for (var l = 0; l < serviceTypes.length; l++) {
						var mealServiceCode = serviceTypes[l].mealServiceCode;
						var preselectedData = d.selectedMeal[j];
						var flightDetails = f[j].flightSector[k];
						var isPreselectedMeal = false;
						// setup initial values for each service code
						var mealSelection = {
							'isEating': true,
							'isALaCarte': false,
							'mealCategoryCode': null,
							'mealCode': null,
							'mealMenuName': null,
							'aLaCarteData': null
						};
						
						if (
							$("body").hasClass("meals-selection-bah") &&
							preselectedData &&
							preselectedData.mealServiceCode == mealServiceCode &&
							preselectedData.mealCategoryCode == "PRSLCS"
						) {
							if (
								preselectedData.segmentId == f[j].segmentId &&
								preselectedData.sectorId == f[j].flightSector[k].sectorId
							) {
								isPreselectedMeal = true;
								mealSelection.mealCategoryCode = preselectedData.mealCategoryCode;
								mealSelection.mealCode = preselectedData.mealCode;
								mealSelection.mealMenuName = preselectedData.mealMenuName;
								mealSelection.mealPrice = preselectedData.price;
								mealSelection.isPreselected = true;
							}
						}
						segment.selectedMeal[mealServiceCode] = mealSelection;
						if (isPreselectedMeal) {
							pax.preselectedMeals.push(JSON.parse(JSON.stringify((segment))));
						}
					}
				}

				pax.selectedMeals.push(segment);
			}

			if (p.isEco || p.isPey) {
				pax.selectedMeals = p.data.paxList[i].selectedMeals || pax.selectedMeals;
			}

			p.data.paxList.push(pax);
			p.input.val(JSON.stringify(p.data))
		}
	};

	var showBSP = function(bspDetails) {
		_.each(bspDetails, function(bsp) {
			SIA.BSPUtil.addBSPTpl(bsp.ids, bsp.pax);
		 });
	};

	var updateData = function (menu) {
		// Update data
		var pid = parseInt(menu.attr('data-paxid'));
		var sid = parseInt(menu.attr('data-segment'));
		var service = menu.attr('data-meal-servicecode');
		var categoryCode = menu.attr('data-meal-category');
		var mealCode = menu.attr('data-meal-mealcode');
		var mealMenuName = menu.attr('data-meal-menuname');
		var mealPrice = menu.attr('data-meal-price');
		var isALC = menu.attr('data-isalc');
		var aLaCarteData = null;

		if (typeof menu.attr('data-alc-json') != 'undefined') aLaCarteData = JSON.parse(menu.attr('data-alc-json'));

		var meal = p.data.paxList[pid].selectedMeals[sid].selectedMeal[service];
		delete meal.isPreselected;
		// Check if element clicked is a checkbox
		if (menu.hasClass('not-eating')) {
			meal.isEating = !menu.prop('checked');
			meal.mealCode = null;
			meal.mealMenuName = null;
		} else if (isALC && typeof aLaCarteData != 'undefined') {
			meal.isEating = true;
			meal.mealCode = mealCode;
			meal.mealCategoryCode = categoryCode;
			meal.mealMenuName = mealMenuName;
			meal.isALaCarte = isALC;
			meal.aLaCarteData = aLaCarteData;
		} else {
			if (menu.hasClass('selected')) {
				meal.isEating = true;
				meal.mealCode = mealCode;
				meal.mealCategoryCode = categoryCode;
				meal.mealMenuName = mealMenuName;
				meal.isALaCarte = isALC;
				meal.mealPrice = mealPrice;
			} else {
				meal.mealCode = null;
				meal.mealCategoryCode = null;
				meal.mealMenuName = null;
				meal.mealPrice = null;
			}
		}
		$("body").trigger("update-selection", menu);
		p.input.val(JSON.stringify(p.data));
	};

	var getInitialStates = function () {
		for (var i = 0; i < p.json.paxList.length; i++) {
			if (p.json.paxList[i].paxId === p.json.passengerStartingPoint) {
				p.curPaxIndex = i;
				p.passengerStartingPoint = i;
			}
		}

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			if (p.json.flightSegment[i].segmentId == p.json.startSegment) p.startSegment = i;
		}

		for (var i = 0; i < p.json.flightSegment[p.startSegment].flightSector.length; i++) {
			if (p.json.flightSegment[p.startSegment].flightSector[i].sectorId == p.json.startSector) p.startSector = i;
		}

		p.startMealService = p.json.startMealService;
	};

	var paintMealsMenu = function (initClass = '') {
		// Reset initial values
		p.accordionTabIndex = 0;
		var segmentId, sectorId, paxId, mealCategoryCode, mealServiceCode = null;

		// Create new accordion wrapper
		p.accordionWrapper = $(_.template(p.accordionWrapperTpl, {
			'initClass': initClass
		}));

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			// create new flight block
			var flSegment = $(p.fltBlkTpl);

			// create flight segment title
			var segmentTitle = $(p.fltTitleTpl);
			segmentTitle.html((i + 1) + '. ' + p.json.flightSegment[i].departureAirportName + ' to ' + p.json.flightSegment[i].arrivalAirportName);

			// Add title to segment block
			flSegment.append(segmentTitle);

			// check if second segment available for meal selection
			secSegmentAvailable(flSegment, i);

			// create accordion-wrapper block
			var accordionWrap = $(p.accordionWrapTpl);

			var accordionContentWrap = accordionWrap.find('.accordion-wrapper-content');

			// Check cabin class
			p.isPey = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.pey); // Check if PEY
			p.isEco = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.eco); // Check if Economy
			p.isBiz = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.biz); // Check if Bisuness
			p.isFirst = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.first); // Check if First/Suite
			
			if ($('body').hasClass('meals-selection-bah')) {
				p.isEco = true;
			}

			
			for (var j = 0; j < p.json.flightSegment[i].flightSector.length; j++) {
				var data = p.json.flightSegment[i].flightSector[j];
				// get current pax type if adult, child or infant
				p.serviceType = getServiceType(p.curPaxIndex);
				var mealService = data[p.serviceType];
				
				for (var k = 0; k < mealService.length; k++) {
					// Activate the accordion based on start states
					var accLabel = p.accordionTabOpenTxt;

					// Check if infant
					var mealCategoryCode, mealServiceCode;
					if (p.serviceType == 'mealServicesInfant') {
						mealCategoryCode = mealService[k].allMeals[0].mealCategoryInfant[0].mealCategoryCode;
						mealServiceCode = mealService[k].mealServiceCode;
					} else {
						if (mealService[k].allMeals[0].mealCategoryLongHaul) {
							mealCategoryCode = "LONGHAUL";
						} else if (mealService[k].allMeals[1].mealCategoryInflight) {
							mealCategoryCode = mealService[k].allMeals[1].mealCategoryInflight[0].mealCategoryCode;
						} else {
							mealCategoryCode = mealService[k].allMeals[1].mealCategoryLightMeals[0].mealCategoryCode;
						}
						mealServiceCode = mealService[k].mealServiceCode;
					}

					// Check if current pax selected is the starting passenger
					// If its not, open the first sector & segment instead
					if (p.curPaxIndex == p.passengerStartingPoint) {
						if (i == p.startSegment && j == p.startSector && mealService[k].mealServiceCode == p.startMealService) {
							accLabel = p.accordionTabCloseTxt;
						}
					} else {
						if (p.accordionTabIndex < 1) {
							accLabel = p.accordionTabCloseTxt;
						}
					}

					// Each meal service will need an accordian item wrapper
					var accItemWrap = $(_.template(p.accordionItemWrapTpl, {
						'id': (p.accordionTabIndex + 1)
					}));

					// add flag for inflight menu
					p.inflightMenuExist[(p.accordionTabIndex + 1)] = false;

					// check if is eating
					var curPaxData = p.data.paxList[p.curPaxIndex].selectedMeals[i].selectedMeal[mealService[k].mealServiceCode];
					
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					var menuLabel = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : inflLbl;

					var menuSelectionLbl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : 'Inflight Menu';
					
					if (curPaxData.mealMenuName && curPaxData.isEating) {
						// Add meal category indicator
						var mealTypeIndicator;
						if (curPaxData.mealCategoryCode === 'SPML') mealTypeIndicator = ' (Special Meal)';
						if (curPaxData.mealCategoryCode === 'INFM') mealTypeIndicator = ' (Inflight Meal)';
						if (curPaxData.mealCategoryCode === 'BTC') mealTypeIndicator = ' (Book the Cook)';
						if (curPaxData.mealCategoryCode === 'INF') mealTypeIndicator = ' (Infant Meal)';
						if (curPaxData.mealCategoryCode === 'HTM') mealTypeIndicator = ' (Hot Meal)';
						if (curPaxData.mealCategoryCode === 'LML') mealTypeIndicator = ' (Light Meal)';
						if (curPaxData.mealCategoryCode === 'PRSLCS') mealTypeIndicator = ' (Premium Selections)';

						var selectedLabel = '';
						if (p.isEco || p.isPey) selectedLabel = '<span class="title-selected">Selected:</span> ';

						menuLabel = selectedLabel + curPaxData.mealMenuName + mealTypeIndicator;
					} else if (mealService[k].mealServiceCode == "MEAL_115" && !curPaxData.mealCode) {
						if (mealService[k].allMeals[0].mealCategoryLongHaul) {
							menuLabel = '<span class="title-selected">Long Haul Meal Bundle</span>'
						} else {
							menuLabel = '<span class="title-selected">Hot Meal</span>';
						}
					}

					// Check if theres existing A la carte data
					if (curPaxData.isALaCarte === 'true') {
						var parentAccMealSummary = '';
						_.each(curPaxData.aLaCarteData, function (v, k, l) {
							if (v.mealMenuName != null) {
								// set the title info
								parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';
							}

						});
						menuLabel = parentAccMealSummary.substring(4);
					}

					if (!curPaxData.isEating) {
						menuLabel = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + mealService[k].mealServiceName;
					}
					
					// Create accordion trigger
					var triggerAcc = p.accordionTriggerTplOld
					if (data.isScoot) {
						triggerAcc = p.accordionTriggerTpl;
					}
					var accTrigger = $(_.template(triggerAcc, {
						'origin': data.departureAirportCode,
						'destination': data.arrivalAirportCode,
						'mealType': mealService[k].mealServiceName,
						'menuSelection': menuLabel,
						'menuSelectionLbl': menuSelectionLbl,
						'label': accLabel,
						'isScoot': data.isScoot
					}));

					if (curPaxData.mealCategoryCode == "SPML" || curPaxData.mealCategoryCode == "BTC") {
						// if ((p.isEco || p.isPey) && curPaxData.isEating) accTrigger.find("li:nth-child(3)").append(p.removeButton);
						if (curPaxData.isEating) accTrigger.find("li:nth-child(3)").append(p.removeButton);
						removeButtonListener();
					}

					accTrigger.on({
						'click': function () {
							resetAccordionLabels($(this));
						}
					});

					p.accordTabArr.push(accTrigger);

					if (accLabel == 'Close') {
						accTrigger.addClass('active').attr('aria-expanded', 'true');
					}

					// Check all meals availability attribute only shows up in inflight meal
					var mealAvailData = mealsAvailable(mealService, k);

					// get contentTpl option
					var allMeals = mealService[k].allMeals;
					var hasNoImg = false,
						btcImg, inflImg, spmlImg, htmImg, lmlImg, prslcsImg,
						btcLabel, inflLabel, spmlLabel, htmLabel, lmlLabel, prslcsLabel,
						btcDesc, inflDesc, spmlDesc, htmDesc, lmlDesc, prslcsDesc,
						hasBtcImg = true,
						hasInflImg = true,
						hasSpmlImg = true,
						hasHtmImg = true, 
						hasLmlImg = true, 
						hasPrslcsImg = true,
						isPrslcsPreOrder = false;

					// Check if any of the menu types have no image, if one is missing use the no Image tpl
					for (var m = 0; m < allMeals.length; m++) {
						if (typeof allMeals[m].mealCategoryBTC !== 'undefined') {
							btcImg = allMeals[m].mealCategoryBTC[0].mealCategoryImage;
							btcDesc = allMeals[m].mealCategoryBTC[0].mealCategoryDescription;
							btcLabel = allMeals[m].mealCategoryBTC[0].mealMenuCategoryName;

							if (typeof btcImg === 'undefined' || btcImg === '') hasBtcImg = false;
							if (typeof allMeals[m].mealCategoryBTC === 'undefined') hasBtcImg = false;
						}
						if (typeof allMeals[m].mealCategoryInflight !== 'undefined') {
							inflImg = allMeals[m].mealCategoryInflight[0].mealCategoryImage;
							inflDesc = allMeals[m].mealCategoryInflight[0].mealCategoryDescription;
							inflLabel = allMeals[m].mealCategoryInflight[0].mealMenuCategoryName;

							if (typeof inflImg === 'undefined' || inflImg === '') hasInflImg = false;
							if (typeof allMeals[m].mealCategoryInflight === 'undefined') hasInflImg = false;
						}
						if (typeof allMeals[m].mealCategorySPML !== 'undefined') {
							spmlImg = allMeals[m].mealCategorySPML[0].mealCategoryImage;
							spmlDesc = allMeals[m].mealCategorySPML[0].mealCategoryDescription;
							spmlLabel = allMeals[m].mealCategorySPML[0].mealMenuCategoryName;
							if (typeof spmlImg === 'undefined' || spmlImg === '') hasSpmlImg = false;
							if (typeof allMeals[m].mealCategorySPML === 'undefined') hasSpmlImg = false;
						}
						if (typeof allMeals[m].mealCategoryHotMeals !== 'undefined') {
							htmImg = allMeals[m].mealCategoryHotMeals[0].mealCategoryImage;
							htmDesc = allMeals[m].mealCategoryHotMeals[0].mealCategoryDescription;
							htmLabel = allMeals[m].mealCategoryHotMeals[0].mealMenuCategoryName;
							if (typeof htmImg === 'undefined' || htmImg === '') hasHtmImg = false;
							if (typeof allMeals[m].mealCategoryHotMeals === 'undefined') hasHtmImg = false;
						}
						if (typeof allMeals[m].mealCategoryLightMeals !== 'undefined') {
							lmlImg = allMeals[m].mealCategoryLightMeals[0].mealCategoryImage;
							lmlDesc = allMeals[m].mealCategoryLightMeals[0].mealCategoryDescription;
							lmlLabel = allMeals[m].mealCategoryLightMeals[0].mealMenuCategoryName;
							if (typeof lmlImg === 'undefined' || lmlImg === '') hasLmlImg = false;
							if (typeof allMeals[m].mealCategoryLightMeals === 'undefined') hasLmlImg = false;
						}
						if (typeof allMeals[m].mealCategoryPremiumSelections !== 'undefined') {
							prslcsImg = allMeals[m].mealCategoryPremiumSelections[0].mealCategoryImage;
							prslcsDesc = allMeals[m].mealCategoryPremiumSelections[0].mealCategoryDescription;
							prslcsLabel = allMeals[m].mealCategoryPremiumSelections[0].mealMenuCategoryName;
							if (typeof prslcsImg === 'undefined' || prslcsImg === '') hasPrslcsImg = false;
							if (typeof allMeals[m].mealCategoryPremiumSelections === 'undefined') hasPrslcsImg = false;
							isPrslcsPreOrder = (allMeals[m].mealCategoryPremiumSelections[0].isPreOrder == "true") ? true : false;
						}
					}

					var nContentTpl = (!hasBtcImg || !hasInflImg || !hasSpmlImg || !hasHtmImg || !hasLmlImg || !hasPrslcsImg) ? p.accordionContentTplNoImage : p.accordionContentTpl;
					// Change content based on cabin class
					if (p.isPey) nContentTpl = p.accordionContentTplPey;
					if (p.isEco) nContentTpl = p.accordionContentTplEco;

					// Create accordion content
					var contentTpl;
					var isBahMenu = getMealCategory('mealCategoryLightMeals', mealService[k].allMeals);
					var isBahLongMenu = getMealCategory('mealCategoryLongHaul', mealService[k].allMeals);
					
					if ($('body').hasClass('meals-selection-bah') && isBahMenu) {
						nContentTpl = p.accordionContentTplBah;
					} else if ($('body').hasClass('meals-selection-bah-long') && isBahLongMenu) {
						nContentTpl = p.accordionContentTplBahLong;
					} else if (p.isEco) {
						nContentTpl = p.accordionContentTplEco;
					}
					
					contentTpl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? p.accordionInfantContentTpl : nContentTpl;
					// check if meal is not available for order
					mealsSelectionAvail = p.curPaxObj.paxType.toLowerCase() != 'infant' ? checkMealsSelAvailable(mealService, k) : {
						'mealsLabel': '',
						'viewOnly': ''
					};

					var checked = '';
					var notEatingMsg = 'hidden';
					if (!curPaxData.isEating) {
						checked = 'checked';
						notEatingMsg = '';
					}

					var accContent = $(_.template(contentTpl, {
						'mealNotReady': mealAvailData.mealNotReady,
						'mealNotReadyMsg': mealAvailData.mealNotReadyMsg,
						'id': p.accordionTabIndex,
						'viewOnly': mealsSelectionAvail.viewOnly,
						'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
						'mealType': mealService[k].mealServiceName.toLowerCase(),
						'flightSegment': i,
						'flightSector': j,
						'curPax': p.curPaxIndex,
						'service': k,
						'targetTrigger': p.accordionTabIndex,
						'segmentId': i,
						'sectorId': j,
						'paxId': p.curPaxIndex,
						'mealCategoryCode': mealCategoryCode,
						'mealServiceCode': mealServiceCode,
						'checked': checked,
						'notEatingMsg': notEatingMsg,
						'mealServiceCode': mealService[k].mealServiceCode,
						'btcImg': btcImg,
						'inflImg': inflImg,
						'spmlImg': spmlImg,
						'htmImg': htmImg,
						'lmlImg': lmlImg,
						'prslcsImg': prslcsImg,
						'btcLabel': btcLabel,
						'inflLabel': inflLabel,
						'spmlLabel': spmlLabel,
						'htmLabel': htmLabel,
						'lmlLabel': lmlLabel,
						'prslcsLabel': prslcsLabel,
						'btcDesc': btcDesc,
						'inflDesc': inflDesc,
						'spmlDesc': spmlDesc,
						'htmDesc': htmDesc,
						'lmlDesc': lmlDesc,
						'prslcsDesc': prslcsDesc,
						'isPrslcsPreOrder': isPrslcsPreOrder
					}));

					if (p.serviceType == 'mealServicesInfant') {
						var infantMealCat = getMealCategory('mealCategoryInfant', mealService[k].allMeals);
						var infm = new SIA.InfantMeal(infantMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
						infm.init();
						p.infm.push(infm);
					} else {
						// Init Inflight Menu
						var inflightMealCat = getMealCategory('mealCategoryInflight', mealService[k].allMeals);
						if (inflightMealCat) {
							var ifm = new SIA.InflightMenu(inflightMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
							ifm.init();
							p.ifm.push(ifm);
						}
						
						var specialMealCat = getMealCategory('mealCategorySPML', mealService[k].allMeals);
						if (specialMealCat) {
							var spm = new SIA.SpecialMeal(specialMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
							spm.init();

							p.spm.push(spm);
						}
						// pain book the cook menu content
						var btcMealCat = getMealCategory('mealCategoryBTC', mealService[k].allMeals);
						var btcm = new SIA.BookTheCook();
						if (btcMealCat) {
							btcm.init(
								btcMealCat,
								p.inflightDishIcons,
								i, j, p.curPaxIndex,
								mealService[k].mealServiceCode,
								p.data,
								accContent,
								'pre-slide-in-left hidden');

							p.btc.push(btcm);
						}
						
						var hotMealsCat = getMealCategory('mealCategoryHotMeals', mealService[k].allMeals);
						if (hotMealsCat) {
							SIA.BahMealMenu(
								hotMealsCat, 
								accContent, i, j, k, 
								p.curPaxIndex, 
								mealService[k].mealServiceCode, 
								curPaxData,
								data.departureAirportCode,
								data.arrivalAirportCode,
								p.data.paxList[p.curPaxIndex].paxName
							);
						}

						var lightMealsCat = getMealCategory('mealCategoryLightMeals', mealService[k].allMeals);
						if (lightMealsCat) {
							SIA.BahMealMenu(
								lightMealsCat, 
								accContent, i, j, k, 
								p.curPaxIndex, 
								mealService[k].mealServiceCode, 
								curPaxData,
								data.departureAirportCode,
								data.arrivalAirportCode,
								p.data.paxList[p.curPaxIndex].paxName
							);
						}

						var premiumMealsCat = getMealCategory('mealCategoryPremiumSelections', mealService[k].allMeals);
						if (premiumMealsCat) {
							SIA.BahMealMenu(
								premiumMealsCat, 
								accContent, i, j, k, 
								p.curPaxIndex,
								mealService[k].mealServiceCode, 
								curPaxData,
								data.departureAirportCode,
								data.arrivalAirportCode,
								p.data.paxList[p.curPaxIndex].paxName
							);
						}
						var bahLongMenu = getMealCategory('mealCategoryLongHaul', mealService[k].allMeals);
						if (bahLongMenu) {
							SIA.BahMealMenuLong(bahLongMenu, accContent, i, j, k, p.curPaxIndex, mealService[k].mealServiceCode, curPaxData);
						}
					}

					// if meal is not available for order, disabled clickable event.
					$('.meal-not-ready a').removeAttr('href');

					// add the trigger and content in the wrapper
					accItemWrap.append(accTrigger);
					accItemWrap.append(accContent);

					// check for past meals
					if(typeof p.json.paxList[p.curPaxIndex].pastMeals !== 'undefined') {
						var pastMeals = p.json.paxList[p.curPaxIndex].pastMeals[mealService[k].mealServiceCode];
						if(p.json.paxList[p.curPaxIndex].paxType.toLowerCase() !== 'infant' && (p.isBiz || p.isFirst) && typeof pastMeals !== 'undefined') {
							var mealsO = {
								'SPML': p.spm,
								'INFM': p.infm,
								'BTC': p.btc
							};
							paintPastMeals(accContent, pastMeals, i, j, k, mealsO);
						}
					}
					

					// Add accordion item wrapper to accordion group wrapper
					accordionContentWrap.append(accItemWrap);

					p.accordionTabIndex++;

					defaultSelection(accItemWrap);
				}
			}

			// Add accordion-wrapper block
			flSegment.append(accordionWrap);

			// Add flight segment to accordion wrapper
			p.accordionWrapper.append(flSegment);

			// add accordion wrapper to mealSelection block
			$('#mealSelection .block-inner').prepend(p.accordionWrapper);
		}

		// Add checkbox listeners
		addCheckboxEvents();

		// add events to all meal menu types
		addMealsItemEvents();

		// Initialise accordion plugin
		waitForAccordion();
	};

	var defaultSelection = function (wrapper) {
		if (p.isEco || p.isPey) {
			var itemContainer = wrapper.find(".item-container");
			if (!itemContainer.length) return;
			var paxId = itemContainer.data("curpax");
			var segmentId = itemContainer.data("segment");
			var mealServiceCode = itemContainer.data("meal-service-code");
			if (!p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode].isEating) {
				return;
			}
		}
		var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
		if (!wrapper.find(".fm-inf-menu.selected").length) {
			wrapperInflightMenu.find(".select-label").addClass("hidden");
		}
	};

	var addCheckboxEvents = function () {
		// get all checkbox for not eating then add change listener
		var checkBox = $('[data-checktype="not-eating"]');
		checkBox.each(function () {
			var cb = $(this);
			var el = cb.closest("[data-accordion-content]").find(".inflight-menu");
			cb.off().on({
				'change': function () {
					var t = $(this);
					// var trigger = t.parent().parent().parent().parent().prev().find('.meal-sub-title-black');
					var trigger = t.closest(".accordion-box").find("[data-accordion-trigger]").find('.meal-sub-title-black');
					var parent = t.closest("[data-accordion]");
					if (t.prop('checked')) {
						t.next().next().removeClass('hidden');

						var msg = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + t.attr('data-meal-type');
						trigger.text(msg);

						var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
						var segmentMeal = pax.selectedMeals[t.data("segment")].selectedMeal[t.data("meal-servicecode")];
						var pastSelected = parent.find(".fm-inf-menu.selected").find(".fm-select-btn");
						if (pastSelected.length) {
							var objectKey = p.curPaxIndex + '-' + t.data("segment") + '-' + pastSelected.data("meal-servicecode");
							p.notEatingPastData[objectKey] = {
								mealCode: pastSelected.data("meal-mealcode"),
								mealCategory: pastSelected.data("meal-category")
							}
							segmentMeal.pastSelected = objectKey;
						}

						resetMeals(t, ['ifm', 'infm', 'spm', 'alcm', 'btcm']);
					} else {
						t.next().next().addClass('hidden');
						trigger.html('<span class="title-selected">' + trigger.attr('data-label') + '</span>');

						var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
						var segmentMeal = pax.selectedMeals[t.data("segment")].selectedMeal[t.data("meal-servicecode")];
						var pastSelectedMeal = (p.notEatingPastData[segmentMeal.pastSelected] || {});
						
						if (pastSelectedMeal.mealCategory == "BTC") {
							pastSelectedMeal = parent.find(".fm-select-btn[data-meal-mealcode='"+pastSelectedMeal.mealCode+"']");
						} else {
							var mealsSegments = Object.values(pax.selectedMeals[t.data("segment")].selectedMeal);
							var result = _.findWhere(mealsSegments, {mealCategoryCode: "SPML", isEating: true});
							if (!result) {
								SIA.MealsSelection.removeInputData(t.closest(".accordion-box"));
								return;
							}
							pastSelectedMeal = parent.find(".fm-select-btn[data-meal-mealcode='"+result.mealCode+"']");
						}
						var mealTitle = t.closest("[data-accordion]").find("[data-accordion-trigger]").find(".meal-sub-title-black");
						if (pastSelectedMeal) {
							var tpl = $(_.template(p.mealTitleTpl, {
								mealName: pastSelectedMeal.data("meal-menuname"),
								mealType: p.mealDictionary[pastSelectedMeal.data("meal-category")],
								hideRemove: !(p.isEco || p.isPey)
							}));
							selectMeal(pastSelectedMeal);
							mealTitle.html("");
							mealTitle.append(tpl);
							if (p.isEco || p.isPey) {
								removeButtonListener();
							}
							t = pastSelectedMeal;
							delete p.notEatingPastData[segmentMeal.pastSelected];
						}
					}

					updateData(t);
				}
			}).ready(function () {
				var t = cb;
				// if (p.isPey || p.isEco) {
					t.closest("[data-accordion-content]").off()
						.on("click.checkBoxSelect", function (e) {
							var targetEl = $(e.target);
							var clickables = [
								targetEl.hasClass("inflight-inner-container"),
								targetEl.hasClass("select-label"),
								targetEl.hasClass("ml-inflight-container")
							];
							if (clickables.indexOf(true) != -1) {
								if (t.is(":checked")) {
									t.trigger("click");
								}
							}
						})
						.on("specialMealSelect", function (e) {
							if (t.is(":checked")) {
								t.trigger("click");
							}
						});
				// }
			});
		});
	};

	var animateMealMenu = function (el, className, currAllMeals) {
		var curSubMenu = el.find(className);
		curSubMenu.removeClass('hidden');

		currAllMeals.addClass('sub-menu-on');

		setTimeout(function () {
			curSubMenu.removeClass('pre-slide-right');
		}, 100);
	};

	var addMealsItemEvents = function () {
		var mealMenuList = $('.meals-menu-item');
		for (var i = 0; i < mealMenuList.length; i++) {
			var mealMenuItemEl = $(mealMenuList[i]);

			// check if meal is available
			if (!mealMenuItemEl.parent().hasClass('meal-not-ready')) {
				mealMenuItemEl.on('click', function (e) {
					e.preventDefault();

					// get the current accordion tab wrapper
					var currAccordionTabWrapper = $(this).parent().parent().parent().parent().parent();

					// get the current accordion tab index
					var accordionIndex = currAccordionTabWrapper.attr('data-accordion-index');

					// get current accordion content wrapper
					var currAccordionContentWrapper = $(this).parent().parent().parent().parent();

					// get the current accordion content block
					var currAllMeals = $(this).parent().parent().parent();

					// hide the current all meals content
					currAllMeals.find('.meals-menu-item').attr('tabindex', '-1');
					currAllMeals.parent().find('[data-checktype="not-eating"]').attr('tabindex', '-1');

					// check what is the selected meal menu type
					if ($(this).hasClass('book-the-cook-menu')) {
						animateMealMenu(currAccordionContentWrapper, '.book-the-cook', currAllMeals);
					} else if ($(this).hasClass('inflight-menu')) {
						if (!p.isEco && !p.isPey) {
							animateMealMenu(currAccordionContentWrapper, '.inflight-menu-wrap', currAllMeals);
						} else {
							chooseInflightMenu($(this));
						}
					} else {
						animateMealMenu(currAccordionContentWrapper, '.special-meal-wrap', currAllMeals);
					}

					setTimeout(function () {
						currAccordionContentWrapper.find('[data-first-focus="true"]').focus();
					}, 500);
				});
			}
		}
	};

	var chooseInflightMenu = function (el, disableFlightAlert) {
		if (!inflightBadge.hasClass("badge-active")) {
			el.find(".ml-inflight-container").find(".select-label").addClass("hidden");
			removeAllSpecialMenu(el);
			if (!disableFlightAlert && undoMeal) {
				// p.inflightAlertPrompt.insertAfter(el);
			}
			removeInputData(el.closest(".accordion-box"));
			undoButtonListener();
		}
	};

	var unChooseInflightMenu = function (el) {
		var inflightBadge = el.find(".inflight-badge");
		inflightBadge.removeClass("badge-active");
		inflightBadge.addClass("hidden");
		el.find(".ml-inflight-container").find(".select-label").removeClass("hidden");
		el.parent().find(".inflight-confirmation-prompt").remove();
	};

	var removeInputData = function (parent) {
		var input = $("#mealSelection").find("[type='hidden']");
		var btnElem = $(parent.find(".fm-select-btn").get(0));
		var paxId = btnElem.data("paxid");
		var segment = btnElem.data("segment");
		var mealCode = btnElem.data("meal-servicecode");

		var pax = JSON.parse(input.val());
		pax.paxList[paxId].selectedMeals[segment].selectedMeal[mealCode] = {
			"isEating": true,
			"isALaCarte": false,
			"mealCategoryCode": null,
			"mealCode": null,
			"mealMenuName": null,
			"mealPrice": null,
			"aLaCarteData": null
		}
		input.val(JSON.stringify(pax));
		p.data.paxList = pax.paxList;
		$("body").trigger("update-selection", btnElem);
	};

	var undoButtonListener = function () {
		$(".inflight-link-undo").on("click", function (evt) {
			var parent = $(this).closest("[data-accordion-content]");
			var mealSelectText = "Selected: ";

			var selectedLabel = '';
			if (p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

			// var mealText = mealSelectText + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + ")";
			if (mealCode[undoMeal.data("meal-category")] == "Special Meals") {
				// var mealText = mealSelectText + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + "s" + ")";
				var mealText = selectedLabel + " " + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + "s" + ")";
			}
			if (mealCode[undoMeal.data("meal-category")] == "Book the Cook") {
				// var mealText = mealSelectText + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + ")";
				var mealText = selectedLabel + " " + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + ")";
			}

			// var mealText = '<span class="title-selected">Selected: </span>' + " " + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + ")";
			selectMeal(undoMeal);
			updateData(undoMeal);
			var mealSubTitle = parent.siblings("[data-accordion-trigger]").find(".meal-sub-title-black");
			mealSubTitle.html(mealText);
			// if (p.isEco || p.isPey) mealSubTitle.append($(p.removeButton));
			mealSubTitle.append($(p.removeButton));
			removeButtonListener();
			parent.find(".ml-inflight-container").find(".select-label").removeClass("hidden");
			undoMeal = null;

			evt.preventDefault();
			evt.stopPropagation();
		});

		$(document).off().on('click.promptUndoCheck', function (e) {
			var t = $(e.target);
			if (t.hasClass('passenger-info__text') ||
				t.hasClass('meal-tab-item') ||
				t.hasClass('accordion__control') ||
				t.hasClass('meal-sub-title-black') ||
				t.hasClass('ico-point-d') ||
				t.hasClass('fm-ico-text') ||
				t.hasClass('meal-service-name') ||
				t.hasClass('flight-ind') ||
				t.hasClass('btn-back') ||
				t.is('#not-eating-delectables-1')
			) {
				$(".inflight-link-undo").parent().parent().remove();
				$(document).off('click.promptUndoCheck');
			}
		});
	};

	var removeAllSpecialMenu = function (el) {
		var parent = el.closest("[data-accordion-content]");
		parent.find(".fm-inf-menu.selected").each(function () {
			var self = $(this);
			var mealButton = self.find(".fm-select-btn");
			unSelectMeal(mealButton);
			undoMeal = mealButton;
		});
		parent.siblings("[data-accordion-trigger]").find(".meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span> ');
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
		removeDefaultSelection(meal);
	};

	var unSelectMeal = function (meal, isPastMeal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');

		if(isPastMeal) {
			updateData(t);

			// Check if there are selected special meals then select same meal for this service code
			for (var i = 0; i < p.spm.length; i++) {
				var spml = p.spm[i];
				if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
					spml.selectSegmentMeal();
					p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
					if(!parentEl.hasClass('past-meal-menu')) parentEl.after(p.spmlAlertPrompt);

					$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
						var t = $(e.target);
						if (t.hasClass('passenger-info__text') ||
							t.hasClass('meal-tab-item') ||
							t.hasClass('accordion__control') ||
							t.hasClass('ico-point-d') ||
							t.hasClass('fm-ico-text') ||
							t.hasClass('meal-service-name') ||
							t.hasClass('flight-ind') ||
							t.hasClass('btn-back') ||
							t.hasClass('fm-inf-menu') ||
							t.hasClass('search-btn')
						) {
							p.spmlAlertPrompt.detach();

							$(document).off('click.spmlPromptCheck');
						}
					});

					break;
				}

			}
		}
	};

	var removeDefaultSelection = function (mealButton) {
		var parent = mealButton.closest("[data-accordion-content]");
		var alertPrompt = parent.find(".inflight-confirmation-prompt");
		alertPrompt.remove();
	};

	var resetAccordionLabels = function (obj) {
		for (var i = 0; i < p.accordTabArr.length; i++) {
			p.accordTabArr[i].find('.fm-ico-text').text(p.accordionTabOpenTxt);
		}

		if (!obj.hasClass('active')) obj.find('.fm-ico-text').text(p.accordionTabCloseTxt);
	};

	var paintPaxNav = function () {
		// cache paxlist
		var paxList = p.json.paxList;

		// Get dynamic pax width
		p.paxWidth = p.win.width() < 1024 ? p.paxListWrapper.parent().width() * 0.5 : 320;

		// Loop through pax list and add to DOM
		for (var i = 0; i < paxList.length; i++) {
			var pax;
			if (paxList[i].paxType.toLowerCase() === 'infant') {
				pax = $('<li class="pax-nav"><a href="#" data-id="' + i + '"><span>' + (i + 1) + '. ' + paxList[i].paxName + ' (Infant)</span></a></li>');
			} else {
				pax = $('<li class="pax-nav"><a href="#" data-id="' + i + '"><span>' + (i + 1) + '. ' + paxList[i].paxName + '</span></a></li>');
			}

			// Add active class to current pax
			if (i == p.curPaxIndex) {
				pax.addClass('active');
			}

			// add pax-start class if first pax is active pax
			if (p.curPaxIndex == 0) p.paxListWrapper.parent().addClass('pax-start');

			p.paxListWrapper.append(pax);
			p.paxArr.push(pax);

			if (p.win.width() < 1024 && paxList.length > 2) {
				pax.css('width', p.paxWidth);
			}
		}

		// Add conditionals for different pax counts
		if (paxList.length === 1) {
			p.paxListWrapper.parent().addClass('hidden');
			$('#main-inner').find('.main-heading').text('Meal selection');

		} else if (paxList.length === 2) {
			p.paxListWrapper.parent().addClass('double-pax');
			addPaxNavListeners();
		} else if (paxList.length === 3 && p.win.width() >= 1024) {
			p.paxListWrapper.parent().addClass('three-pax');
			setTimeout(function () {
				initPaxNav();
			}, 700);
		} else {
			// initialise pax nav event listeners
			initPaxNav();
		}
	};

	var initPaxNav = function () {
		// Get reference to the buttons
		p.paxLt = p.paxListWrapper.prev();
		p.paxRt = p.paxListWrapper.next();

		if (typeof p.paxListWrapper.slick == 'function') {
			// Get reference to the buttons
			p.paxLt = p.paxListWrapper.prev();
			p.paxRt = p.paxListWrapper.next();

			p.paxListWrapper.slick({
				dots: false,
				infinite: false,
				// the magic
				responsive: [{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 414,
					settings: {
						slidesToShow: 1
					}
				}],
				slidesToShow: 3,
				speed: 600,
				draggable: true,
				slidesToScroll: 1,
				accessibility: true,
				arrows: true,
				prevArrow: p.paxLt,
				nextArrow: p.paxRt,
				initialSlide: p.curPaxIndex,
				zIndex: 0
			}).on({
				'afterChange': function (event, slick, currentSlide, nextSlide) {
					if (!$(slick.$slides[0]).hasClass('slick-active')) {
						p.paxLt.show();
						p.paxListWrapper.parent().removeClass('pax-start');
					} else {
						p.paxLt.hide();
						p.paxListWrapper.parent().addClass('pax-start');
					}

					if (!$(slick.$slides[slick.$slides.length - 1]).hasClass('slick-active')) {
						p.paxRt.show();
						p.paxListWrapper.parent().removeClass('pax-end');
					} else {
						p.paxRt.hide();
						p.paxListWrapper.parent().addClass('pax-end');
					}
				},
				'breakpoint': function (event, slick, breakpoint) {
					if (breakpoint < 1024) {
						p.paxListWrapper.parent().removeClass('three-pax');
					} else {
						if (p.json.paxList.length === 3 && p.win.width() >= 1024) p.paxListWrapper.parent().addClass('three-pax');
					}
				}
			});

			addPaxNavListeners();
		} else {
			setTimeout(function () {
				initPaxNav();
			}, 100);
		}
	};

	var addPaxNavListeners = function () {
		// add listeners on each pax element
		for (var i = 0; i < p.paxArr.length; i++) {
			p.paxArr[i].find('a').off().on({
				'click': function (e) {
					e.preventDefault();
					e.stopImmediatePropagation();
					if (!$(this).parent().hasClass('active')) {
						p.curPaxIndex = parseInt($(this).attr('data-id'));


						// Reset other pax nav and enable the clicked pax
						resetAllPax($(this));

						// scroll the page up to focus on the new menu
						// after scroll show the next accordion wrapper
						var top = p.paxListWrapper.parent().offset().top;
						var bodyTop = p.htmlBody.prop('scrollTop');

						// index the current accordion wrapper
						p.exitWrapper = null;
						p.exitWrapper = p.accordionWrapper;

						// Add class to prepare curobj for transition
						p.exitWrapper.addClass('pre-slide-out');

						// After adding the exit wrapper reference, paint the next wrapper for the next pax
						paintMealsMenu('pre-slide-in-right');

						if (bodyTop >= top) {
							showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
						} else {
							p.htmlBody.stop().animate({
								scrollTop: top
							}, 400);

							setTimeout(function () {
								showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
							}, 450);
						}
					}
				}
			});
		}
	};

	var resetAllPax = function (obj) {
		for (var i = 0; i < p.paxArr.length; i++) {
			p.paxArr[i].removeClass('active');
		}

		obj.parent().addClass('active');
	};

	var paintPastMeals = function(container, pastMeals, segmentId, sectorId, serviceId, mealsO) {
		var pastMealsWrap = $('\
			<h3 class="fm-title-4--blue">Pick a favourite meal from a past flight</h3>\
			<div class="past-meals-wrap">\
				<button type="button" class="past-meal__btn slider-btn-left"><em class="ico-point-l"></em></button>\
				<div class="past-meals-slider">\
					<div class="past-meals-container">\
					</div>\
				</div>\
				<button type="button" class="past-meal__btn slider-btn-right"><em class="ico-point-r"></em></button>\
			</div>\
		');

		var pastMealContainer = pastMealsWrap.find('.past-meals-container');
		for(var i = 0; i < pastMeals.length; i++) {
			var pastMeal = pastMeals[i];
			pastMealContainer.append($(
				'<div class="fm-inf-menu past-meal-menu">\
					<span class="fm-inf-menu-select-badge">Selected</span>\
					<h4 class="fm-inf-menu-title">'+pastMeal.mealMenuName+'</h4>\
					<span class="fm-inf-menu-sub-title">'+pastMeal.mealMenuCategoryName+'</span>\
					<p class="fm-inf-menu-desc">'+pastMeal.mealMenuDescription+'</p>\
					<div class="fm-footnote-txt">\
						<input type="button" name="" class="fm-select-btn right past-meal" value="Select" data-mealcattype="speacial-meal-list" data-segment="'+segmentId+'" data-sector="'+sectorId+'" data-paxid="'+p.curPaxIndex+'" data-service-id="'+serviceId+'" data-meal-category="'+pastMeal.mealCategoryCode+'" data-meal-servicecode="'+pastMeal.mealServiceCode+'" data-meal-mealcode="'+pastMeal.mealCode+'" data-meal-menuname="'+pastMeal.mealMenuName+'">\
					</div>\
				</div>'
			))
		}

		var menuHeading = container.find('.fm-title-4--blue');
		if(menuHeading.text() === 'Select a meal') {
			menuHeading.text('Or discover new favourites').before($('<hr class="hr-line past-meal">'));
		}
		
		container.find('.fm-inf-menu.selected').each(function(){
			var t = $(this).find('.fm-select-btn');
			var btn = pastMealContainer.find('.past-meal-menu [data-meal-category="'+t.attr('data-meal-category')+'"][data-meal-mealcode="'+t.attr('data-meal-mealcode')+'"]');
			btn.val('Remove').addClass('selected').parent().parent().addClass('selected');
		});

		var firstHr = container.find('.hr-line').first();
		firstHr.after(pastMealsWrap);

		addPastMealsListener(pastMealsWrap.find('.past-meals-container'), pastMealsWrap, mealsO);
	};
	
	var addPastMealsListener = function(slider, container, mealsO){
		// Start slick slider
		var btnLeft = container.find('.slider-btn-left');
		var btnRight = container.find('.slider-btn-right');

		// var outsideBox = container.closest('.accordion__content');
		// outsideBox.before(btnLeft);
		// outsideBox.before(btnRight);

		var meals = slider.find('.fm-inf-menu');

		// Add hidden prompt
		var pastMealPrompt = $(
			'<div class="remove-prompt past-meal-prompt hidden" aria-expanded="true">\
				<em class="ico-alert"></em>\
				<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label"></span> sed do eiusmod ternpor incididunt ut labore et dolore. </p>\
				<div class="button-group-1">\
					<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>'
		);

		// Initialise slick slider based on meals length
		if(meals.length > 2) {
			slider.addClass('past-meals-slick');

			slider.slick({
				dots: true,
				infinite: false,
				slidesToShow: 2,
				slidesToScroll: 2,
				speed: 400,
				draggable: true,
				accessibility: true,
				arrows: true,
				prevArrow: btnLeft,
				nextArrow: btnRight,
				initialSlide: 0,
				variableWidth: true,
				zIndex: 0,
				adaptiveHeight: true
			}).on({
				'afterChange': function (event, slick, currentSlide, nextSlide) {
					if (!$(slick.$slides[0]).hasClass('slick-active')) {
						btnLeft.show();
					} else {
						btnLeft.hide();
					}

					if (!$(slick.$slides[slick.$slides.length - 1]).hasClass('slick-active')) {
						btnRight.show();
					} else {
						btnRight.hide();
					}
				},
				'breakpoint': function (event, slick, breakpoint) {
					
				}
			});

			// Hide prev button by default
			btnLeft.hide();

			// Add pastMealPrompt after slick initialisation
			container.find('.slick-dots').before(pastMealPrompt);
		}else {
			slider.addClass('past-meals-no-slick');
			btnLeft.addClass('hidden');
			btnRight.addClass('hidden');

			slider.parent().append(pastMealPrompt);
		}

		// Add click events for each past meal
		meals.each(function(){
			var t = $(this);
			var btn = t.find('.fm-select-btn');

			var curMeal = mealsO[btn.attr('data-meal-category')];
			if(curMeal.length > 0) {
				curMeal[curMeal.length-1].addInputListener(btn);
			}
		});
	};

	/* slide out the accordion wrapper
	 */
	var showNextPaxAccordion = function (curObj, nextObj) {

		// Add listener to exitwrapper
		curObj.one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd',
			function () {
				curObj.remove();
			});

		setTimeout(function () {
			// Add class to exit wrapper to start animating out
			curObj.addClass('slide-out-left');
			// Add class to current accordion wrapper to start animating in
			nextObj.removeClass('pre-slide-in-right');
		}, 10);
	};

	/* To set the accordion wrapper default position
	 */
	var waitForAccordion = function () {
		setTimeout(function () {
			if (typeof SIA.accordion.initAccordion == 'undefined') {
				waitForAccordion();
			} else {
				SIA.accordion.initAccordion();

				// check if second segment meal selection is not available
				// then remove any events for accordion tab
				disableSecSegAvailable();
			}
		}, 60);
	};

	/* check if second segment available for meal selection
	 */
	var secSegmentAvailable = function (flSegment, index) {
		var mealSelWaitListedData = p.json.flightSegment[index];
		var secSegMealSelectionStatus = mealSelWaitListedData.isSegmentAvailableForMealSelection;
		var secSegMealSelectionMsg = mealSelWaitListedData.segmentMealSelectionStatusMessage;

		if (secSegMealSelectionStatus == 'false') {
			var checkinAlert = $(_.template(p.checkinAlert, {
				checknMsg: secSegMealSelectionMsg
			}));

			// add checkin alert to second segment block
			flSegment.append(checkinAlert);

			// change accordion arrow text && icon to gray to disable
			flSegment.addClass('disable-accordion');
		}
	};

	/* disable events for all second segment meals selection
	 */
	var disableSecSegAvailable = function () {
		$('.disable-accordion [data-accordion-trigger="1"]').unbind();

		// remove href attrib to disable clickable
		$('.disable-accordion a').removeAttr('href');
	};

	/* Check all meals availability attribute only shows up in inflight meal
	 */
	var mealsAvailable = function (mealService, index) {
		var mealNotReady = '';
		var mealNotReadyMsg = '';

		if (((p.serviceType == 'mealServicesAdult') ||
				(p.serviceType == 'mealServicesChild'))) {
			var mealServiceData;
			if (mealService[index].allMeals[0].mealCategoryLongHaul) {
				mealServiceData = mealService[index].allMeals[0].mealCategoryLongHaul[0]
			} else if (mealService[index].allMeals[1].mealCategoryInflight) {
				mealServiceData = mealService[index].allMeals[1].mealCategoryInflight[0];
			} else if (mealService[index].allMeals[1].mealCategoryLightMeals) {
				mealServiceData = mealService[index].allMeals[1].mealCategoryLightMeals[0];
			}
			var isViewMenuEligible = mealServiceData.isViewMenuEligible;
			var isMealSelAvail = mealServiceData.isMealSelectionAvailable;

			mealNotReady = ((isViewMenuEligible == 'false') && (isMealSelAvail == 'false')) ? 'meal-not-ready' : '';
			mealNotReadyMsg = mealServiceData.isViewMenuDate;
		}

		return {
			mealNotReady: mealNotReady,
			mealNotReadyMsg: mealNotReadyMsg
		};
	};

	/* check if meal is not available for order
	 */
	var checkMealsSelAvailable = function (mealService, index) {
		var mealsAvailData;
		if (mealService[index].allMeals[0].mealCategoryLongHaul) {
			mealsAvailData = mealService[index].allMeals[0].mealCategoryLongHaul[0]
		} else if (mealService[index].allMeals[1].mealCategoryInflight) {
			mealsAvailData = mealService[index].allMeals[1].mealCategoryInflight[0];
		} else if (mealService[index].allMeals[1].mealCategoryLightMeals) {
			mealsAvailData = mealService[index].allMeals[1].mealCategoryLightMeals[0];
		}
		var isViewMenuEligible = mealsAvailData.isViewMenuEligible;
		var isMealSelAvail = mealsAvailData.isMealSelectionAvailable;

		if ((isViewMenuEligible == 'true') && (isMealSelAvail == 'false')) {
			return {
				'mealsLabel': p.mealsLabelTpl,
				'viewOnly': 'view-only'
			};
		} else {
			return {
				'mealsLabel': '',
				'viewOnly': ''
			};
		}
	};

	/* get current pax type if adult, child or infant
	 */
	var getServiceType = function (paxIndex) {
		p.curPaxObj = p.json.paxList[paxIndex];
		var serviceType = '';
		switch (p.curPaxObj.paxType.toLowerCase()) {
			case 'adult':
				serviceType = 'mealServicesAdult';
				break;
			case 'child':
				serviceType = 'mealServicesChild';
				break;
			case 'infant':
				serviceType = 'mealServicesInfant';
				break;
			default:
				serviceType = 'mealServicesAdult';
		}

		return serviceType;
	};

	var getMealCategory = function (type, arr) {
		for (var i = 0; i < arr.length; i++) {
			if (typeof arr[i][type] != 'undefined') {
				return arr[i][type];
			}
		}
	};

	var resetMeals = function (el, meals) {
		var mealList = {
			'btcm': '.book-the-cook .selected .fm-select-btn',
			'ifm': '.inflight-menu-wrap .selected .fm-select-btn',
			'infm': '.selected .fm-select-btn',
			'spm': '.special-meal-wrap .selected .fm-select-btn',
			'alcm': '.a-la-carte [style="display: block;"] .selected .fm-select-btn'
		};

		$.each(meals, function (i, meal) {
			var inputBtn = el.closest('.all-meals-list').find(mealList[meal]);
			inputBtn.closest('.fm-inf-menu').removeClass('selected');
			inputBtn.closest('.fm-inf-menu').removeClass('int-prompt');
			inputBtn.attr('value', 'Select');

			inputBtn.parent().parent().find('.confirmation-prompt').addClass('hidden');

			// for a-la-carte only to clear its accordion tab.
			if (meal == 'alcm') {
				inputBtn.closest('.fm-inf-menu').parent().prev().find('.alc-summary').html('');
			}
		});
	};

	var resetNotEating = function (el) {
		var notEatingEl = el.closest('.all-meals-list').find('.not-eating-supper');
		notEatingEl.find('input').prop('checked', false);
		notEatingEl.find('.not-eating-msg').addClass('hidden');
		notEatingEl.find('.fm-cbox').removeClass('checkbox_color');
	};

	var checkCabinClass = function (curCC, classArr) {
		var isMatch = false;
		for (var i = 0; i < classArr.length; i++) {
			var cc = classArr[i];
			if (curCC === cc) {
				isMatch = true;
				break;
			}
		}

		return isMatch;
	};

	var oPublic = {
		init: init,
		getServiceType: getServiceType,
		updateData: updateData,
		resetMeals: resetMeals,
		selectMeal: selectMeal,
		resetToInflight: resetToInflight,
		animateMealMenu: animateMealMenu,
		resetNotEating: resetNotEating,
		removeInputData: removeInputData,
		public: p
	};

	return oPublic;
}();

SIA.clickActivity = function() {
	var initClick = function() {
		var elList = [
			".btn-back"
		];
		$(elList.join(",")).on("click", function(evt) {
			hideAlertPrompt();
		});
	};

	var hideAlertPrompt = function() {
		$("body")
			.find('[data-accordion="1"]')
			.find(".confirmation-prompt:not(.hidden)")
			.each(function() {
				var $this = $(this);
				$this.addClass("hidden");
				$this.closest(".meal-of-bah:not(.selected)").find(".fm-select-btn").val("Select");
			});
	};

	var init = function() {
		initClick();
	};

	return {
		init: init,
		hideAlertPrompt: hideAlertPrompt
	}
}();

SIA.InflightMenu = function (jsonData,
	paxData,
	accContent,
	segmentId,
	sectorId,
	serviceId,
	paxId,
	mealServiceCode) {
	var p = {};

	// Declare templates for inflight menu
	p.h2TxtTpl = '<div class="alert-block info-box<%= viewOnlyAlert %>">\
          <div class="inner">\
            <div class="alert__icon"><em class="ico-tooltips"></em></div>\
            <div class="alert__message"><%= viewMenuData %></div>\
          </div>\
        </div>\
        <h2 class="fm-title-blue"><%= h2Txt %></h2>';
	p.descTxtTpl = '<p class="fm-inf-menu-desc">Choose a delicious main course from our inflight menu to complete your inflight dining experience.</p>\
        <h3 class="fm-title2-black">Your choice of main course</h3>';
	p.inflightMealMenuWrapperTpl = '<div class="fm-inf-menu <%= selectedClass %><%= viewOnlyWrapper %>">\
        <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
        <h4 class="fm-inf-menu-title"><%= title %></h4>\
        <p class="fm-inf-menu-desc"><%= desc %></p>\
        <div class="fm-footnote-txt">\
          <span class="food-icon-set"><%= foodIconSet %></span>\
          <input type="button" name="select" value="<%= inputVal %>" data-inflightMenuselect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-isALC="<%= isALC %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
          <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
            <span class="btn-small">VIEW ONLY</span>\
            <span class="onboard"> Order on board</span>\
          </div>\
        </div>\
      </div>';
	p.inflightEthinicMealMenuWrapperTpl = '<div class="fm-inf-menu <%= selectedClass %><%= viewOnlyWrapper %>">\
            <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
            <span class="fm-ysh-choice <%= badgeState %>"><%= badgeName %></span>\
            <h4 class="fm-inf-menu-title"><%= menuTitle %></h4>\
            <p class="fm-inf-menu-desc"><%= menuDesc %></p>\
            <%= promotionsConditionTpl %>\
            <div class="fm-footnote-txt">\
                <span class="food-icon-set"><%= foodIconSet %></span>\
                <input type="button" name="" value="<%= inputVal %>" data-inflightMenuselect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-isALC="<%= isALC %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
                <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
                  <span class="btn-small">VIEW ONLY</span>\
                  <span class="onboard"> Order on board</span>\
                </div>\
            </div>\
        </div>';
	p.inflightEthinicMealMenuTpl1 = '<div class="promotion-conditions" />';
	p.inflightEthinicMealMenuTpl2 = '<div class="promotion-conditions-text" />';
	p.inflightEthinicMealMenuTplDataTpl = '<dl><dt><%= foodName %></dt></dl>';
	p.inflightEthinicMealMenuFoodDescTpl = '<dd><span class="title-5--blue"><%= foodDesc %></span></dd>';

	// A la carte / noodle bar description
	p.noodleBarDescTxtTpl = '<p class="fm-inf-menu-desc">\
       You can choose to pre-order one option from each meal service now,\
       but there isn&apos;t a fixed time where they&apos;ll be served the meal(s). \
       Pre-ordering will help you guarantee that you&apos;ll get some things that you would like to eat.\
       You can also choose to order more from the various meal services when you are on the flight.</p>\
       <h3 class="fm-title2-black">Mid-flight noodle bar</h3>';
	p.alcarteDescTxtTpl = '<p class="fm-inf-menu-desc">\
        You can choose to pre-order one option from each meal service now,\
        but there isn&apos;t a fixed time where they&apos;ll be served the meal(s).\
        Pre-ordering will help you guarantee that you&apos;ll get some things that you would like to eat.\
        You can also choose to order more from the various meal services when you are on the flight.</p>';

	// A la carte
	p.alcAccMainWrap = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper">\
        <div data-accordion-wrapper-content="1" class="accordion-wrapper-content">\
        </div></div>';
	p.alcAccWrapper = '<div data-accordion="1" data-accordion-index="<%= id %>" data-mealsubcat-code="<%= mealSubCategoryCode %>" data-mealsubcat-name="<%= mealServiceName %>" class="block-2 accordion accordion-box alc-accordion">\
            <a href="#" data-accordion-trigger="1" class="accordion__control<%= isActive %>" aria-expanded="true" tabindex="0">\
                <div class="fm-accordion-label">\
                    <ul class="meal-menu">\
                        <li class="meal-service-name"><%= mealServiceName %></li>\
                    </ul>\
                    <span class="fm-ico-text selected alc-summary"></span>\
                    <em class="ico-point-d"></em>\
                </div>\
            </a>\
            <div data-accordion-content="1" class="accordion__content alc-acc-content"></div>\
        </div>';

	// applied special meal prompt
	p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
        <em class="ico-alert"></em>\
        <p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
      </div>');
	p.inflightAlertPrompt = $('<div class="inflight-confirmation-prompt confirmation-prompt">\
        <em class="ico-alert"></em>\
        <p class="meal-sub-title-black">You&apos;ve changed your meal choice to <span><%= menuName %></span>. <a href="#" class="inflight-link-undo"> > Undo</a></p>\
      </div>');

	p.selectedALC = [];
	p.isALC = false;

	var init = function () {
		p.mealCatName = jsonData[0].mealMenuCategoryName;

		p.mealWrap = accContent.find('.inflight-menu-wrap .menu-container');

		backToMainMenu();
		paintHeading();
		paintMealChoices();
	};

	var paintHeading = function () {
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = jsonData[0].isViewMenuDate;
		var inflMenuViewOnly = true;

		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
			inflMenuViewOnly = true;
		}

		p.mealMenuCatName = $(_.template(p.h2TxtTpl, {
			'h2Txt': p.mealCatName,
			'viewOnlyAlert': viewOnlyAlert,
			'viewMenuData': viewOnlyMsg
		}));

		p.mealWrap.append(p.mealMenuCatName);

		// add the meal menu category name description
		if (mealServiceCode == 'ACL_110') {
			p.mealWrap.append(p.alcarteDescTxtTpl);
		} else if (mealServiceCode == 'MNB_110') {
			p.mealWrap.append(p.noodleBarDescTxtTpl);
		} else {
			p.mealWrap.append(p.descTxtTpl);
		}
	};

	var paintMealChoices = function () {
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = jsonData[0].isViewMenuDate;
		var inflMenuViewOnly = false;

		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
			inflMenuViewOnly = true;
		}

		p.mealCardList = [];
		var mealCatCode = jsonData[0].mealCategoryCode;

		p.curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

		if (jsonData[0].isALaCarte == 'true') {
			p.isALC = true;
			p.alcInput = $('<input type="button" class="hidden selected" name="A La Carte Selection" value="A La Carte Selection" data-isEating="true" data-inflightMenuselect-id="" data-segment="" data-sector="" data-service-id="" data-isalc="true" data-paxid="" data-meal-category="" data-meal-servicecode="" data-meal-menuname="" data-meal-mealcode="" />');

			p.mealWrap.append(p.alcInput);

			// Setup a la carte data
			p.alcData = {};

			var alcAccMainWrap = $(p.alcAccMainWrap);
			for (var j = 0; j < jsonData[0].mealSubcategory.length; j++) {
				var curData = jsonData[0].mealSubcategory[j];

				var alcAccTitle = '';
				// Check if any ALC meals are selected
				if (p.curPaxData.isALaCarte === 'true') {
					p.alcData[curData.mealSubCategoryCode] = p.curPaxData.aLaCarteData[curData.mealSubCategoryCode];
					if (p.alcData[curData.mealSubCategoryCode].mealMenuName) alcAccTitle = 'Selected: ' + p.alcData[curData.mealSubCategoryCode].mealMenuName + ' (Inflight Menu)';
				} else {
					p.alcData[curData.mealSubCategoryCode] = {};
					p.alcData[curData.mealSubCategoryCode].mealCategoryCode = null;
					p.alcData[curData.mealSubCategoryCode].mealCode = null;
					p.alcData[curData.mealSubCategoryCode].mealMenuName = null;
					p.alcData[curData.mealSubCategoryCode].mealServiceCode = null;
					p.alcData[curData.mealSubCategoryCode].mealServiceName = null;
				}

				var isActive = '';
				if (j == 0) isActive = ' active';

				var alcAccordionWrap = $(_.template(p.alcAccWrapper, {
					'mealServiceName': jsonData[0].mealSubcategory[j].mealSubCategoryName,
					'mealSubCategoryCode': jsonData[0].mealSubcategory[j].mealSubCategoryCode,
					'id': j,
					'isActive': isActive
				}));

				alcAccMainWrap.find('.accordion-wrapper-content').append(alcAccordionWrap);
				p.mealWrap.addClass('a-la-carte');

				// Add individual alc title
				alcAccordionWrap.find('.alc-summary').html(alcAccTitle);

				var mealCardData = jsonData[0].mealSubcategory[j].Meals;
				addInflightMeals(mealCardData, mealCatCode, p.curPaxData, alcAccordionWrap.find('.alc-acc-content'), inflMenuViewOnly, jsonData[0].mealSubcategory[j].mealSubCategoryCode);
			}

			p.mealWrap.append(alcAccMainWrap);

		} else {
			var mealCardData = jsonData[0].mealSubcategory["0"].Meals;
			addInflightMeals(mealCardData, mealCatCode, p.curPaxData, p.mealWrap, inflMenuViewOnly);
		}
	};

	var addInflightMeals = function (mealCardData, mealCatCode, curPaxData, mealWrapper, inflMenuViewOnly, alcCat) {

		for (var i = 0; i < mealCardData.length; i++) {

			// check for any previous selected item
			if (curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == mealCardData[i].mealCode) {
				selectedClass = 'selected';
				inputVal = 'Remove';
				isSelected = '';
				p.lastTick = p.mealCardList.length;
			} else {
				selectedClass = '';
				inputVal = 'Select';
				isSelected = 'hidden';
			}

			var mealCard;

			// if its ethnic meal
			if (mealCardData[i].isEthnicMeal === 'true' && typeof mealCardData[i].ethinicMeallist != 'undefined') {
				mealCard = paintEthnicMeal(i, mealCardData, mealCatCode, inflMenuViewOnly);
			} else { 
				// generate the information for meal card
				mealCard = paintMeal(i, mealCardData, mealCatCode, inflMenuViewOnly, alcCat);
			}

			// check if menu is view ONLY, if true, hide input
			if (inflMenuViewOnly) {
				mealCard.find('input').hide();
				mealCard.find('.btn-wrap-view-only').hide();
				mealCard.addClass('view-only-meal');
			} else {
				// add meal menu select button event
				mealCard.find('input').each(function () {
					addInputListener($(this));
				});
			}

			// add the meal card
			p.mealCardList.push(mealCard);
			mealWrapper.append(mealCard);
		}
	};

	var paintEthnicMeal = function (i, mealCardData, mealCatCode, inflMenuViewOnly) {
		var mealCard;
		// create two wrapper to have two columns layout in ethinic meal menu list of names
		var ethinicMealListWrapper1 = $(p.inflightEthinicMealMenuTpl2);
		var ethinicMealListWrapper2 = $(p.inflightEthinicMealMenuTpl2);
		var ethinicMealMenuData = mealCardData[i].ethinicMeallist;

		for (var k = 0; k < ethinicMealMenuData.length; k++) {
			// prepare the ethinic meal menu name item wrapper
			var ethinicMealMenuDataWrapper = $(_.template(p.inflightEthinicMealMenuTplDataTpl, {
				'foodName': ethinicMealMenuData[k].ethinicMealMenuName
			}));

			// prepare the ethinic meal menu description block
			var ethinicMealMenuDescData = ethinicMealMenuData[k].itemInfo;
			var ethinicMealMenuDesc = null;

			for (var j = 0; j < ethinicMealMenuDescData.length; j++) {
				// generate ethinic meal menu description data
				ethinicMealMenuDesc = $(_.template(p.inflightEthinicMealMenuFoodDescTpl, {
					'foodDesc': ethinicMealMenuDescData[j].ethinicMealMenuDescription
				}));

				// add to ethinic meal menu name item wrapper
				ethinicMealMenuDataWrapper.append(ethinicMealMenuDesc);
			}

			// Add additional description
			var ethnicMealDesc = ethinicMealMenuData[k].ethnicMealMenuDescription;
			if (typeof ethnicMealDesc !== 'undefined' && ethnicMealDesc !== '') {
				ethinicMealMenuDataWrapper.append('<dd class="ethnic-meal-desc">' + ethinicMealMenuData[k].ethnicMealMenuDescription + '</dd>')
			}

			// check in which column to add then,
			// add to ethinic meal menu list column
			if (k % 2 == 0) {
				ethinicMealListWrapper1.append(ethinicMealMenuDataWrapper);
			} else {
				ethinicMealListWrapper2.append(ethinicMealMenuDataWrapper);
			}
		}

		// add to ethinic meal menu main wrapper
		var ethinicMealMenuMainWrapper = $(p.inflightEthinicMealMenuTpl1);
		ethinicMealMenuMainWrapper.append(ethinicMealListWrapper1);
		ethinicMealMenuMainWrapper.append(ethinicMealListWrapper2);

		var foodIconSet = '';
		if (typeof mealCardData[i].specialityInfo !== 'undefined') {
			for (var l = 0; l < mealCardData[i].specialityInfo.length; l++) {
				var br = l === mealCardData[i].specialityInfo.length - 1 ? '' : '<br>';
				foodIconSet += '<em class="' + SIA.MealsSelection.public.inflightDishIcons[mealCardData[i].specialityInfo[l].dishIconId] + ' fm-footnote-logo"></em>\
            <span class="fm-dish-text"> ' + mealCardData[i].specialityInfo[l].specialityFootNote + ' </span>' + br;
			}
		}

		// check if to add a badge style
		var badgeName = null;
		var badgeState = 'hidden';
		if (mealCardData[i].isSpecialMeal === 'true') {
			badgeName = mealCardData[i].specialMealName
			badgeState = '';
		}

		var selectClass = '';
		var viewOnlyClass = ' hidden';
		var viewOnlyWrapper = '';

		if (typeof mealCardData[i].isViewOnly != 'undefined' && mealCardData[i].isViewOnly) {
			selectClass = ' hidden';
			viewOnlyClass = '';
			viewOnlyWrapper = ' view-only-meal';
		}

		// generate ethinic meal menu wrapper data
		mealCard = $(_.template(p.inflightEthinicMealMenuWrapperTpl, {
			'badgeName': badgeName,
			'badgeState': badgeState,
			'menuTitle': mealCardData[i].mealMenuName,
			'menuDesc': mealCardData[i].mealMenuDescription,
			'menuSpecialityFootNote': mealCardData[i].specialityInfo.specialityFootNote,
			'promotionsConditionTpl': ethinicMealMenuMainWrapper["0"].outerHTML,
			'id': p.mealCardList.length,
			'selectClass': selectClass,
			'segmentId': segmentId,
			'sectorId': sectorId,
			'paxId': paxId,
			'mealServiceId': serviceId,
			'mealCategoryCode': mealCatCode,
			'mealServiceCode': mealServiceCode,
			'mealCode': mealCardData[i].mealCode,
			'mealMenuName': mealCardData[i].mealMenuName,
			'viewOnlyClass': viewOnlyClass,
			'selectedClass': selectedClass,
			'inputVal': inputVal,
			'isSelected': isSelected,
			'isALC': p.isALC,
			'viewOnlyWrapper': viewOnlyWrapper,
			'foodIconSet': foodIconSet
		}));

		// check if menu is view ONLY, if true, hide input
		if (inflMenuViewOnly) {
			mealCard.find('input').hide();
			mealCard.find('.btn-wrap-view-only').hide();
			mealCard.addClass('view-only-meal');
		} else {
			// add meal menu select button event
			mealCard.find('input').each(function () {
				addInputListener($(this));
			});
		}

		return mealCard;
	};

	var paintMeal = function (i, mealCardData, mealCatCode, inflMenuViewOnly, alcCat) {
		var mealCard;

		var foodDesc = '';
		if (typeof mealCardData[i].specialityInfo != 'undefined') foodDesc = mealCardData[i].specialityInfo.specialityFootNote;

		var selectClass = '';
		var viewOnlyClass = ' hidden';
		var viewOnlyWrapper = '';

		if (typeof mealCardData[i].isViewOnly != 'undefined' && mealCardData[i].isViewOnly) {
			selectClass = ' hidden';
			viewOnlyClass = '';
			viewOnlyWrapper = ' view-only-meal';
		}

		if (p.isALC && typeof alcCat !== 'undefined' && p.curPaxData.aLaCarteData) {
			if (p.curPaxData.aLaCarteData[alcCat].mealCode === mealCardData[i].mealCode) {
				selectClass = ' selected';
				selectedClass = 'selected';
				viewOnlyClass = ' hidden';
				inputVal = 'Remove';
				isSelected = '';
				p.lastTick = i;
			}
		}

		var foodIconSet = '';
		if (typeof mealCardData[i].specialityInfo !== 'undefined') {
			for (var l = 0; l < mealCardData[i].specialityInfo.length; l++) {
				var br = l === mealCardData[i].specialityInfo.length - 1 ? '' : '<br>';
				foodIconSet += '<em class="' + SIA.MealsSelection.public.inflightDishIcons[mealCardData[i].specialityInfo[l].dishIconId] + ' fm-footnote-logo"></em>\
            <span class="fm-dish-text"> ' + mealCardData[i].specialityInfo[l].specialityFootNote + ' </span>' + br;
			}
		}

		mealCard = $(_.template(p.inflightMealMenuWrapperTpl, {
			'title': mealCardData[i].mealMenuName,
			'desc': mealCardData[i].mealMenuDescription,
			'foodDesc': foodDesc,
			'id': p.mealCardList.length,
			'triggerId': (p.accordionTabIndex + 1),
			'selectClass': selectClass,
			'segmentId': segmentId,
			'sectorId': sectorId,
			'paxId': paxId,
			'mealServiceId': serviceId,
			'mealCategoryCode': mealCatCode,
			'mealServiceCode': mealServiceCode,
			'mealCode': mealCardData[i].mealCode,
			'mealMenuName': mealCardData[i].mealMenuName,
			'viewOnlyClass': viewOnlyClass,
			'selectedClass': selectedClass,
			'inputVal': inputVal,
			'isSelected': isSelected,
			'isALC': p.isALC,
			'viewOnlyWrapper': viewOnlyWrapper,
			'foodIconSet': foodIconSet
		}));

		if (i == 0) {
			mealCard.find('.fm-select-btn').attr('data-first-focus', 'true');
		}

		return mealCard;
	};

	var addInputListener = function (input) {
		input.parent().parent().off().on({
			'click': function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var selectedId = t.attr('data-inflightMenuselect-id');

				if (t.hasClass('hidden')) return;

				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle, alcParentAccWrap, alcParentAccWrapTrigger;

				if (p.isALC) {
					accordionTabMealTitle = accordionContent.closest(".accordion-box").find('.alc-summary');
					alcParentAccWrap = accordionContent.closest('.alc-accordion');
					alcParentAccWrapTriggerSummary = alcParentAccWrap.closest('.all-meals-list').prev().find('li:nth-child(3)');
				} else {
					accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				}
				var parentElList = parentEl.parent();

				// search and remove any previous selected meal
				var mealCardList = accordionContent.find('.fm-select-btn');
				for (var j = 0; j < mealCardList.length; j++) {
					var el = $(mealCardList[j]);

					// add hidden class to hide the selected badge
					el.closest('.fm-inf-menu').find('.fm-inf-menu-select-badge').addClass('hidden');

					// remove selected class
					el.closest('.fm-inf-menu').removeClass('selected');

					// reset the select button value to it's default text('Select').
					el.attr('value', 'Select');

					// change accordion tab previously selected meal to its default text('inflight menu')
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					accordionTabMealTitle.html(p.isALC ? '' : inflLbl);
				}

				// check if current meal card is selected again, then remove it
				var badgeEl = $(parentEl).children();

				// Assign A La Carte initial data
				if (p.isALC) {
					var curMealSubCatCode = alcParentAccWrap.data('mealsubcat-code');
					var curMealSubCatName = alcParentAccWrap.data('mealsubcat-name');
				}

				if (p.lastTick == selectedId) {
					// Add selected class to the input and to the container
					parentEl.removeClass('selected');
					t.removeClass('selected');

					// remove hidden class to show the selected badge
					$(badgeEl["0"]).addClass('hidden');

					t.attr('value', 'Select');

					p.lastTick = '';

					// change accordion tab previously selected mean to its default text('inflight menu')
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					accordionTabMealTitle.html(p.isALC ? '' : inflLbl);

					if (p.isALC) {
						p.alcData[curMealSubCatCode].mealCategoryCode = null;
						p.alcData[curMealSubCatCode].mealCode = null;
						p.alcData[curMealSubCatCode].mealMenuName = null;
						p.alcData[curMealSubCatCode].mealServiceCode = null;
						p.alcData[curMealSubCatCode].mealServiceName = null;
					}

					// Check if there are selected special meals then select same meal for this service code
					for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
						var spml = SIA.MealsSelection.public.spm[i];
						if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
							spml.selectSegmentMeal();
							p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
							if(!parentEl.hasClass('past-meal-menu')) parentEl.after(p.spmlAlertPrompt);

							$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
								var t = $(e.target);
								if (t.hasClass('passenger-info__text') ||
									t.hasClass('meal-tab-item') ||
									t.hasClass('accordion__control') ||
									t.hasClass('ico-point-d') ||
									t.hasClass('fm-ico-text') ||
									t.hasClass('meal-service-name') ||
									t.hasClass('flight-ind') ||
									t.hasClass('btn-back') ||
									t.hasClass('fm-inf-menu') ||
									t.hasClass('search-btn')
								) {
									if(!t.hasClass('past-meal')) p.spmlAlertPrompt.detach();

									$(document).off('click.spmlPromptCheck');
								}
							});

							break;
						}
					}
				} else {
					// Add selected class to the input and to the container
					parentEl.addClass('selected');
					t.addClass('selected');

					// remove hidden class to show the selected badge
					$(badgeEl["0"]).removeClass('hidden');

					t.attr('value', 'Remove');

					p.lastTick = selectedId;

					// get the selected mean name
					var selMealName = p.isALC ? 'Selected: ' + parentEl.find('.fm-inf-menu-title').text() : parentEl.find('.fm-inf-menu-title').text();

					// change accordion tab default text('inflight menu') to selected meal
					accordionTabMealTitle.text(selMealName + ' (Inflight Menu)');

					SIA.MealsSelection.resetNotEating(t);

					if (p.isALC) {
						p.alcData[curMealSubCatCode].mealCategoryCode = t.data().mealCategory;
						p.alcData[curMealSubCatCode].mealCode = t.data().mealMealcode;
						p.alcData[curMealSubCatCode].mealMenuName = t.data().mealMenuname;
						p.alcData[curMealSubCatCode].mealServiceCode = t.data().mealServicecode;
						p.alcData[curMealSubCatCode].mealServiceName = curMealSubCatName;
					}
				}

				if (p.isALC) {
					var parentAccMealSummary = '';
					var mealMenunames = '';
					var mealMealcodes = '';

					_.each(p.alcData, function (v, k, l) {
						if (v.mealMenuName != null) {
							// set the title info
							parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';

							mealMenunames = mealMenunames + ',' + v.mealMenuName;
							mealMealcodes = mealMealcodes + ',' + v.mealCode;
						}

					});

					p.alcInput.attr('data-inflightMenuselect-id', t.data().inflightmenuselectId);
					p.alcInput.attr('data-paxid', t.data('paxid'));
					p.alcInput.attr('data-sector', t.data().sector);
					p.alcInput.attr('data-segment', t.data().segment);
					p.alcInput.attr('data-service-id', t.data().serviceId);
					p.alcInput.attr('data-meal-category', t.data().mealCategory);
					p.alcInput.attr('data-meal-servicecode', t.data().mealServicecode);
					p.alcInput.attr('data-alc-json', JSON.stringify(p.alcData));

					alcParentAccWrapTriggerSummary.html(parentAccMealSummary.substring(4));

					// Update the user data here
					SIA.MealsSelection.updateData(p.alcInput);
				} else {
					SIA.MealsSelection.updateData(t);
				}

			}
		});
	};

	var backToMainMenu = function () {
		// add back to all meals button event
		p.mealWrap.parent().find('.fm-backtomeals-btn.floater .btn-back').off().on('click', function (e) {
			e.preventDefault();
			var subMenuWrapper = $(this).parent().parent();
			subMenuWrapper.off().one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd',
				function () {
					$(this).addClass('hidden');

					// reset position of main menu wrapper
					var allMeals = $(this).parent().find('.all-meals-menu');
					allMeals.removeClass('sub-menu-on');

					allMeals.find('.meals-menu-item').attr('tabindex', '0');
					allMeals.parent().find('[data-checktype="not-eating"]').attr('tabindex', '0');

					allMeals.find('.meals-menu-item.special-meals').focus();
				});

			// hide inflight menu wrapper
			subMenuWrapper.addClass('pre-slide-right');

			// show all menu wrapper
			var prevAllMeal = $(this).parent().parent().parent();

			p.mealCardList = null;
		});
	};

	var oPublic = {
		init: init,
		p: p,
		addInputListener: addInputListener
	};

	return oPublic;
};

SIA.BahMealMenu = function(
		jsonData, 
		accordionContent,
		segmentId,
		sectorId,
		serviceId,
		paxId,
		mealServiceCode,
		paxData,
		origin,
		destination,
		paxName
	) {
	var p = SIA.MealsSelection.public;
	var allMeals = [];
	var mealTriggerClasses = [
		{
			"mealCode": "HTM",
			"className": "hot-meals",
			"name": "Hot Meal"
		},
		{
			"mealCode": "PRSLCS",
			"className": "premium-meals",
			"name": "Premium Selection"
		},
		{
			"mealCode": "LML",
			"className": "light-meals",
			"name": "Light Meal"
		}
	];
	var dishIcons = {
		"HLL": {
			className: "ico-halal"
		},
		"SPC": {
			className: "ico-5-Spicy"
		},
		"VEG": {
			className: "ico-4-leaf"
		}
	};
	var currentPax = {};
	var loadLimit = 5;

	p.bahTitleMenu = '<h2 class="fm-title-blue"><%- title %></h2>';
	p.bahDescriptionMenu = '<p class="fm-inf-menu-desc"><%- description %></p>';
	p.loadMoreButton = '<div class="load-more hidden">\
		<input type="button" name="select" value="Load more" class="load-more-btn" />\
	</div>';

	var getAllMenuOnSubCategory = function(meals) {
		_.each(meals, function(meal) {
			if (meal.hasOwnProperty("mealSubCategoryCode")) {
				allMeals = allMeals.concat(meal.Meals);
			}
		});
	};

	var addTriggerListener = function(mealCode) {
		var triggerClass = _.findWhere(mealTriggerClasses, { mealCode: mealCode });
		
		accordionContent.find("." + triggerClass.className + ".meals-menu-item").off().on("click.bahMeals", function(e) {
			e.stopImmediatePropagation();
			e.preventDefault();
			$("[data-accordion-trigger]").trigger("click.removeHandleEvent");
			var className = "." + triggerClass.className + "-menu-wrap";
			SIA.MealsSelection.animateMealMenu(
				accordionContent, 
				className, 
				accordionContent.find(".all-meals-menu"));
		});
	};

	var paintHeader = function(meal) {
		var triggerClass = _.findWhere(mealTriggerClasses, { mealCode: meal.mealCategoryCode });
		var className = "." + triggerClass.className + "-menu-wrap";
		var container = accordionContent.find(className).find(".menu-container");
		var titleTpl = _.template(p.bahTitleMenu, {title: meal.mealMenuCategoryName});
		var descriptionTpl = _.template(p.bahDescriptionMenu, {description: meal.mealCategoryDescription});
		container.append($(titleTpl));
		container.append($(descriptionTpl));
	};

	var getCurrentPax = function() {
		if (p.data.paxList[paxId].preselectedMeals[segmentId]) {
			currentPax = _.clone(p.data.paxList[paxId].preselectedMeals[segmentId].selectedMeal[mealServiceCode]);
		} else {
			currentPax = _.clone(p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode]);
		}
	};

	var addMealsList = function(meal) {
		var elementList = [];
		var currencyCode = "";
		var triggerClass = _.findWhere(mealTriggerClasses, { mealCode: meal.mealCategoryCode });
		var className = "." + triggerClass.className + "-menu-wrap";
		var container = accordionContent.find(className).find(".menu-container");
		_.each(allMeals, function(mealItem, idx) {
			var mealTpl = _.template(p.menuItemBah, {
				mealName: mealItem.mealMenuName,
				mealDescription: mealItem.mealMenuDescription,
				price: mealItem.price,
				currency: mealItem.currency,
				speciality: (mealItem.specialityInfo || []),
				icons: dishIcons,
				paxId: paxId,
				segmentId: segmentId,
				mealServiceCode: mealServiceCode,
				mealCategory: meal.mealCategoryCode,
				mealCode: mealItem.mealCode
			});
			var templateLoaded = $(mealTpl);
			if (idx >= loadLimit) {
				templateLoaded.addClass("hidden");
			}
			addClickListener(templateLoaded);
			var selectedMeal = templateLoaded.find('[data-meal-mealcode="'+paxData.mealCode+'"]');
			if (selectedMeal.length) {
				var wrapper = selectedMeal.closest(".meal-of-bah");
				wrapper.addClass("selected");
				toggleButton(wrapper, false);
				setTimeout(function() {
					changeAccordionLabel(selectedMeal);
				}, 500);
			}
			elementList.push(templateLoaded);
			currencyCode = mealItem.currency;
		});
		if (meal.mealCategoryCode == "PRSLCS") {
			var lowestPrice = getLowestPrice();
			accordionContent.find(".price-range-block").find(".meal-price").text(currencyCode + " " + lowestPrice.toFixed(2));
		}
		var loadButtonEl = $(p.loadMoreButton);
		elementList.push(loadButtonEl);
		container.append(elementList);
		if (container.find(".fm-inf-menu.hidden").length) {
			container.find(".load-more").removeClass("hidden");
			loadMoreButtonListener(container, loadButtonEl);
		}
	};

	var loadMoreButtonListener = function(container, loadButtonEl) {
		loadButtonEl.find(".load-more-btn").off().on("click", function() {
			var elementList = container.find(".fm-inf-menu.hidden").splice(0, loadLimit);
			_.each(elementList, function(el) {
				el = $(el);
				el.removeClass("hidden");
			})
			if (!container.find(".fm-inf-menu.hidden").length) {
				loadButtonEl.addClass("hidden");
			}
		});
	};

	var addClickListener = function(el) {
		el.off().on("click.bahMenuList", function() {
			getCurrentPax();
			$("[data-accordion-trigger]").trigger("click.removeHandleEvent");

			var $this = $(this);
			var isSelected = $this.hasClass("selected");
			if (!isSelected) {
				var canSelect = forfeitSelection($this);
				if (!canSelect) return;
			}
			if (isSelected && currentPax.isPreselected) {
				return cancelSelection($this, $this.find(".fm-select-btn"));
			}

			var toggledButton = toggleButton($this, isSelected);
			clearAllSelected($this);
			if (!isSelected) {
				SIA.MealsSelection.updateData(toggledButton);
				changeAccordionLabel(toggledButton);
			} else {
				var wrapper = $this.closest(".accordion-box");
				SIA.clickActivity.hideAlertPrompt();
				SIA.MealsSelection.removeInputData(wrapper);
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				changeAccordionLabel(toggledButton, true);
			}
		});
	};

	var countPax = function() {
		var count = 0;
		_.each(p.data.paxList, function(data) {
			if (data.paxType.toLowerCase() == "adult") {
				count ++;
			}
		});

		$("[data-booking-summary-panel]").find(".number-passengers").text(count + " Adults");
	};

	var clearAllSelected = function($this) {
		accordionContent.find(".meal-of-bah.selected").each(function() {
			var $current = $(this);
			var isSelected = $current.hasClass("selected");
			if ($this.is($current)) return;
			
			toggleButton($current, isSelected);
		});
	};

	var forfeitSelection = function(el) {
		var buttonElement = el.find(".fm-select-btn");
		var selectedMealCode = buttonElement.data("meal-category");
		var selectedPrice = parseFloat(buttonElement.data("meal-price"));
		var paxId = buttonElement.attr('data-paxid');

		if (
			selectedMealCode == currentPax.mealCategoryCode &&
			selectedPrice == parseFloat(currentPax.mealPrice) &&
			selectedMealCode == "PRSLCS"
		) {
			return equalForfeitSelection(el, buttonElement);
		} else if (
			selectedMealCode != "PRSLCS" &&
			currentPax.mealCategoryCode == "PRSLCS"
		) {		
			return fullForfeitSelection(el, buttonElement);
		} else if (
			selectedMealCode == currentPax.mealCategoryCode &&
			selectedPrice < parseFloat(currentPax.mealPrice) &&
			selectedMealCode == "PRSLCS"
		) {
			return partialForfeitSelection(el, buttonElement);
		} else if (
			currentPax.mealCategoryCode &&
			selectedPrice > parseFloat((currentPax.mealPrice ? currentPax.mealPrice : "0")) &&
			selectedMealCode == "PRSLCS"
		) {
			return topUpForfeitSelection(el, buttonElement);
		}
		if (selectedMealCode == "PRSLCS" && !p.bspIsDisplayed) {
			p.bspIsDisplayed = true;
			changeCTAButton("PROCEED TO PAYMENT");
			SIA.BSPUtil.displayBSP(function() {
				countPax();
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: selectedPrice,
					paxName: paxName,
					origin: origin,
					destination: destination
				});
			});
		} else if (selectedMealCode != "PRSLCS") {
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
		} else {
			changeCTAButton("PROCEED TO PAYMENT");
			SIA.BSPUtil.addBSPTpl({
				segmentId: segmentId,
				sectorId: sectorId
			}, {
				paxId: paxId,
				price: selectedPrice,
				paxName: paxName,
				origin: origin,
				destination: destination
			});
		}

		return true;
	};

	var changeCTAButton = function(inputText) {
		$(".btn-cta-submit").val(inputText.toUpperCase());
	};

	var equalForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. No additional payment will be required as the price of your previous meal is the same as your new meal.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var fullForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. Note that there will not be a refund for the meal which you've previously paid for.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var partialForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal premium selection. Payment is not required for your new meal as the price is lower than you've paid for your previous meal. Note that there will not be any refund for your previous meal as well.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var topUpForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. To confirm, proceed to pay for this new meal selection.";
		if (currentPax.mealCategoryCode != "PRSLCS") {
			promptMessage = "You have an existing meal which will be replaced with this new meal selection. To confirm, proceed to pay for this new meal selection.";
		}
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var cancelSelection = function(el, buttonElement) {
		var promptMessage = "When you remove this meal selection, note that there will not be a refund for the meal which you've previously paid for. You can select another meal to replace this.";
		setButtonPreSelected(el, buttonElement, promptMessage, true);
		return false;
	};

	var setButtonPreSelected = function(el, buttonElement, promptMessage, isCancel) {
		SIA.clickActivity.hideAlertPrompt();
		var mealWrapper = buttonElement.closest(".meal-of-bah");
		var confirmPrompt = mealWrapper.find(".confirmation-prompt");
		
		if(
			segmentId === parseInt(buttonElement.attr('data-segment')) &&
			mealServiceCode === buttonElement.attr('data-meal-servicecode') &&
			!currentPax.isPreselected &&
			!isCancel
		){
			var price = buttonElement.data("meal-price");
			var paxId = buttonElement.attr('data-paxid');
			if (buttonElement.data("meal-category") == "PRSLCS" && !p.bspIsDisplayed) {
				p.bspIsDisplayed = true;
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.displayBSP(function() {
					countPax();
					SIA.BSPUtil.addBSPTpl({
						segmentId: segmentId,
						sectorId: sectorId
					}, {
						paxId: paxId,
						price: price,
						paxName: paxName,
						origin: origin,
						destination: destination
					});
				});
			} else if (buttonElement.data("meal-category") == "PRSLCS") {
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: price,
					paxName: paxName,
					origin: origin,
					destination: destination
				});
			} else if (buttonElement.data("meal-category") != "PRSLCS") {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
			}

			clearAllSelected(el);
			toggleButton(el, false);
			SIA.MealsSelection.updateData(buttonElement);
			changeAccordionLabel(buttonElement);
		} else {
			confirmPrompt.find(".meal-sub-title-blue").text(promptMessage);
			confirmPrompt.removeClass("hidden");
			if (!isCancel) {
				buttonElement.val("Selected");
			}
			addPromptListener(el, buttonElement, confirmPrompt, isCancel);
		}
		
	};

	var addPromptListener = function(el, buttonEl, prompt, isCancel) {
		var cancelBtn = prompt.find(".btn-cancel");
		var proceedBtn = prompt.find(".btn-proceed");
		var price = buttonEl.data("meal-price");
		var paxId = buttonEl.attr('data-paxid');

		// Make sure while prompt is shown, button is set to Remove
		if(buttonEl.val() == 'Select') buttonEl.val('Remove');
		
		proceedBtn.off().on("click", function(evt) {
			var canDisplayBSP = false;
			evt.preventDefault();
			evt.stopImmediatePropagation();
			var currentPrice = parseFloat(currentPax.mealPrice);
			
			prompt.addClass("hidden");
			clearAllSelected(el);
			if (isCancel) {
				SIA.MealsSelection.removeInputData(el);
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				changeAccordionLabel(buttonEl, true);
				proceedBtn.off();
				cancelBtn.off();
				return;
			} else {
				toggleButton(el, false);
				SIA.MealsSelection.updateData(buttonEl);
				changeAccordionLabel(buttonEl);
				proceedBtn.off();
				cancelBtn.off();
			}

			if (!currentPax.isPreselected) {
				canDisplayBSP = true;
			}
			if (price > currentPrice) {
				canDisplayBSP = true;
			} else {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				return;
			}

			if (buttonEl.data("meal-category") == "PRSLCS" && !p.bspIsDisplayed && canDisplayBSP) {
				p.bspIsDisplayed = true;
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.displayBSP(function() {
					countPax();
					if (!$(".bah-subtotal").length && price > currentPrice) { 
						SIA.BSPUtil.paintSubTotal(currentPax);
					}
					SIA.BSPUtil.addBSPTpl({
						segmentId: segmentId,
						sectorId: sectorId
					}, {
						paxId: paxId,
						price: price,
						paxName: paxName,
						origin: origin,
						destination: destination,
						currentPaxPrice: currentPax.mealPrice
					});
				});
			} else if (buttonEl.data("meal-category") == "PRSLCS" && canDisplayBSP) {
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: price,
					paxName: paxName,
					origin: origin,
					destination: destination,
					currentPaxPrice: currentPax.mealPrice
				});
			} else if (buttonEl.data("meal-category") != "PRSLCS") {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
			}
		});

		cancelBtn.off().on("click", function(evt) {
			evt.preventDefault();
			evt.stopImmediatePropagation();

			prompt.addClass("hidden");
			if (!isCancel) {
				buttonEl.val("Select");
			}
			proceedBtn.off();
			cancelBtn.off();
		});
	};

	var toggleButton = function($this, status) {
		var mealButton = $this.find(".fm-select-btn");
		$this[status ? "removeClass" : "addClass"]("selected");
		mealButton[status ? "removeClass" : "addClass"]("selected");
		mealButton.val(status ? "Select" : "Remove");
		return mealButton;
	};

	var getLowestPrice = function() {
		var lowestPrice = 0;
		_.each(allMeals, function(meal) {
			if (lowestPrice == 0) {
				return lowestPrice = parseFloat(meal.price);
			}

			lowestPrice = (parseFloat(meal.price) < lowestPrice) ? parseFloat(meal.price) : lowestPrice;
		});

		return lowestPrice;
	};

	var addBackButtonListener = function() {
		accordionContent.find(".meal-sub-menu-wrap").each(function() {
			var $this = $(this);
			$this.find(".btn-back")
				.off("click").on("click", function(evt) {
					evt.preventDefault();
					$("[data-accordion-trigger]").trigger("click.removeHandleEvent");
					$this.off().one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd',
					function () {
						$this.addClass('hidden');
						var allMeals = accordionContent.find('.all-meals-menu');
						allMeals.removeClass('sub-menu-on');
					});
					$this.addClass("pre-slide-right");
				});
		});
	};

	var changeAccordionLabel = function(el, isRemoving) {
		var accordionBoxWrapper = el.closest(".accordion-box").find(".fm-accordion-label").find("[data-label]");
		var triggerClass = _.findWhere(mealTriggerClasses, {mealCode: el.data("meal-category")});
		var price = el.data("meal-price");
		var labelTpl;
		if (!isRemoving) {
			labelTpl = '<span>\
							<span class="title-selected">Selected:</span> '
							+ el.data("meal-menuname") 
							+ " (" + triggerClass.name + ") "
							+ (price ? " - SGD " + price + " " : "")
							+ '<a class="remove-bah-menu">Remove</a>'
							+ "</span>";
		} else {
			labelTpl = '<span class="title-selected">Hot Meal</span>';
		}
		var labelRendered = $((el ? labelTpl : defaultTpl));
		accordionBoxWrapper.html(labelRendered);
		if (!isRemoving) {
			addRemoveBahListener(labelRendered, el);
		} else {
			toggleButton(el.closest(".meal-of-bah"), true);
		}
	};

	var addRemoveBahListener = function(el, buttonEl) {
		var meal = p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];
		el.find(".remove-bah-menu").on("click", function(evt) {
			evt.stopImmediatePropagation();
			evt.preventDefault();
			
			if (meal.isPreselected) {
				var prompt = el.closest(".accordion-box").find(".remove-prompt");
				removeBahPrompt(prompt, buttonEl, $(this).closest(".accordion-box"));
				return;
			}

			SIA.MealsSelection.removeInputData($(this).closest(".accordion-box"));
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
			changeAccordionLabel(buttonEl, true);
		});
	};

	var removeBahPrompt = function(prompt, buttonEl, inputData) {
		prompt.removeClass("hidden");
		var proceed = prompt.find(".btn-proceed");
		var cancel = prompt.find(".btn-cancel");
		var classes = "[data-accordion-trigger]";

		$(document).find(classes).on("click.removeHandleEvent", function(evt) {
			prompt.addClass("hidden");
			proceed.off();
			cancel.off();
			$(classes).off("click.removeHandleEvent");
		});

		inputData.find(".confirmation-prompt:not(.hidden)").each(function() {
			$(this).addClass("hidden");
		});

		proceed.on("click", function(evt) {
			evt.preventDefault();

			SIA.MealsSelection.removeInputData(inputData);
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
			changeAccordionLabel(buttonEl, true);
			prompt.addClass("hidden");

			proceed.off();
			cancel.off();
		});

		cancel.on("click", function(evt) {
			evt.preventDefault();
			prompt.addClass("hidden");
			proceed.off();
			cancel.off();
		});
	};

	getAllMenuOnSubCategory(jsonData[0].mealSubcategory);
	addTriggerListener(jsonData[0].mealCategoryCode);
	addBackButtonListener();
	paintHeader(jsonData[0]);
	addMealsList(jsonData[0]);
};

SIA.BahMealMenuLong = function(
		jsonData, 
		accordionContent,
		segmentId,
		sectorId,
		serviceId,
		paxId,
		mealServiceCode,
		paxData
	) {
	var allMeals = jsonData;
	var p = SIA.MealsSelection.public;
	var dishIcons = {
		"VEG": {
			className: "ico-4-leaf"
		}
	};

	var addMealsList = function() {
		var elementList = [];
		var container = accordionContent.find(".long-bah-menu-wrap").find(".menu-container");
		container.append($('<h2 class="fm-title-blue">Select a Meal</h2>'))
		_.each(allMeals, function(mealItem) {
			var concatDescription = descriptionConcatenate(mealItem.mealMenuDescription);
			var mealTpl = _.template(p.menuItemBah, {
				mealName: mealItem.mealMenuName,
				mealDescription: "",
				price: false,
				currency: mealItem.currency,
				speciality: (mealItem.specialityInfo || []),
				icons: dishIcons,
				paxId: paxId,
				segmentId: segmentId,
				mealServiceCode: mealServiceCode,
				mealCategory: "LONGHAUL",
				mealCode: mealItem.mealCode
			});
			var templateLoaded = $(mealTpl);
			addClickListener(templateLoaded);
			var selectedMeal = templateLoaded.find('[data-meal-mealcode="'+paxData.mealCode+'"]');
			if (selectedMeal.length) {
				var wrapper = selectedMeal.closest(".meal-of-bah");
				wrapper.addClass("selected");
				toggleButton(wrapper, false);
				setTimeout(function() {
					changeAccordionLabel(selectedMeal);
				}, 500);
			}
			templateLoaded.find(".fm-inf-menu-desc").append(concatDescription);
			elementList.push(templateLoaded);
		});
		container.append(elementList);
	};

	var descriptionConcatenate = function(mealDescription) {
		var elementList = [];
		_.each(mealDescription, function(description) {
			elementList.push($("<p>" + description.mealSubDescription + "</p>"));
		});

		return elementList;
	};

	var addClickListener = function(el) {
		el.off().on("click.bahMenuList", function() {
			var $this = $(this);
			var isSelected = $this.hasClass("selected");
			accordionContent.find(".meal-of-bah.selected").each(function() {
				var $current = $(this);
				var isSelected = $current.hasClass("selected");
				if ($this.is($current)) return;
				toggleButton($current, isSelected);
			});
			var toggledButton = toggleButton($this, isSelected);
			if (!isSelected) {
				SIA.MealsSelection.updateData(toggledButton);
				changeAccordionLabel(toggledButton);
			} else {
				var wrapper = $this.closest(".accordion-box");
				SIA.MealsSelection.removeInputData(wrapper);
				changeAccordionLabel(toggledButton, true);
			}
		});
	};

	var changeAccordionLabel = function(el, isRemoving) {
		var accordionBoxWrapper = el.closest(".accordion-box").find(".fm-accordion-label").find("[data-label]");
		var labelTpl;
		if (!isRemoving) {
			labelTpl = '<span>\
							<span class="title-selected">Selected:</span> ' 
							+ el.data("meal-menuname") + " "
							+ '<a class="remove-bah-menu">Remove</a>'
							+ "</span>";
		} else {
			labelTpl = '<span class="title-selected">Long Haul Meal Bundle</span>';
		}
		var labelRendered = $((el ? labelTpl : defaultTpl));
		accordionBoxWrapper.html(labelRendered);
		if (!isRemoving) {
			addRemoveBahListener(labelRendered, el);
		} else {
			toggleButton(el.closest(".meal-of-bah"), true);
		}
	};

	var toggleButton = function($this, status) {
		var mealButton = $this.find(".fm-select-btn");
		$this[status ? "removeClass" : "addClass"]("selected");
		mealButton[status ? "removeClass" : "addClass"]("selected");
		mealButton.val(status ? "Select" : "Remove");
		return mealButton;
	};

	var addRemoveBahListener = function(el, buttonEl) {
		el.find(".remove-bah-menu").on("click", function(evt) {
			evt.stopImmediatePropagation();
			evt.preventDefault();

			SIA.MealsSelection.removeInputData($(this).closest(".accordion-box"));
			changeAccordionLabel(buttonEl, true);
		});
	};

	addMealsList();
};

SIA.BookTheCook = function () {
	var p = {};

	// declare templates
	p.back2AllMealsLinkTpl = '<hr><div class="fm-backtomeals-btn"></div>';
	p.back2AllMealsLinkTpl2 = '<div class="fm-backtomeals-btn floater">\
            <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back</a>\
        </div>';
	p.BTCTitleTpl = '<h2 class="fm-title-blue"><%= txt %></h2>';
	p.BTCDescTpl = '<p class="fm-inf-menu-desc">Choose a Book the Cook main course at least 24 hours before departure to enjoy an exquisite meal crafted by our International Culinary Panel of chefs.</p>';
	p.BTCSearhControl = '<p class="search-heading"> Have a meal in mind? </p>\
          <div class="static-content">\
              <div class="static-details">\
                  <div class="btc-search">\
                      <label for="input-search-1" class="hidden"></label>\
                      <em class="ico-search"></em>\
                      <span class="input-1">\
                          <input type="text" data-first-focus="true" name="input-search-1" value="" placeholder="Search all our available Book the Cook main courses"/>\
                      </span>\
                      <button type="submit" name="btn-search" class="btn-1 search-btn">Search</button>\
                  </div>\
                  <a href="#" class="clear-search hidden"><em class="ico-point-r"></em>Clear search result</a> </div>\
              </div>\
          </div>';

	p.BTCCuisineCatMainWrapperTpl = '<div class="tab-wrapper">\
          <div class="tab-content active">\
              <p class="search-result-msg hidden"></div>\
              <div class="form-seatmap responsive btc-meals">\
                  <%= tabList %>\
                  <div data-customselect="true" class="btc-meal-select custom-select custom-select--2 custom-select--3 multi-select tab-select" data-select-tab="true">\
                      <label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span>\
                      <select name="calcTabSelect">\
                      </select>\
                  </div>\
                  <%= contentList %>\
              </div>\
          </div>\
      </div>';
	p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
	p.BTCCruisineCatListTpl = '<div data-fixed="true" class="sidebar">\
          <div class="inner" role="application" style="left: auto;">\
              <ul class="booking-nav btc-meal-nav" role="tablist">\
                  <%= catListItems %>\
                  <li class="more-item">\
                      <a href="#"><span>More</span><em class="ico-dropdown"></em></a>\
                  </li>\
              </ul>\
          </div>\
      </div>';
	p.BTCCruisineCatListItemTpl = '<li class="booking-nav__item tab-item <%= isActive %>" role="tab" data-tab-menu="<%= tabMenu %>">\
            <a href="#" role="tab">\
                <span class="passenger-info">\
                    <span class="passenger-info__text"><%= menuName %>\
                    </span>\
                </span>\
            </a>\
        </li>';

	p.BTCMealItem = '<div class="fm-inf-menu <%= isSel %>">\
        <span class="fm-inf-menu-select-badge">Selected</span>\
        <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
        <h4 class="fm-inf-menu-title" id="ariaLbl<%= index %>"><%= mealMenuName %></h4>\
		<p class="fm-inf-menu-desc" id="ariaDesc<%= index %>"><%= mealMenuDesc %></p>\
        <div class="fm-footnote-txt">\
            <span class="food-icon-set"><%= foodIconSet %></span>\
            <input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" aria-labelledby="ariaLbl<%= index %>" aria-describedby="ariaDesc<%= index %>">\
		</div>\
		<div class="confirmation-prompt confirmation-remove--blue hidden">\
			<em class="ico-alert"></em>\
			<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod ternpor incididunt ut labore et dolore. </p>\
			<div class="button-group-1">\
				<input type="button" value="Proceed" class="btn-1 btn-proceed">\
				<a href="#" class="btn-2 btn-cancel">Cancel</a>\
			</div>\
		</div>\
    </div>';

	p.BTCMealItemWithImage = '<div class="fm-inf-menu with-image <%= isSel %>">\
        <span class="fm-inf-menu-select-badge">Selected</span>\
        <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
        <div class="meal-selection-info">\
            <h4 class="fm-inf-menu-title" id="ariaLbl<%= index %>"><%= mealMenuName %></h4>\
			<p class="fm-inf-menu-desc" id="ariaDesc<%= index %>"><%= mealMenuDesc %></p>\
            <div class="fm-footnote-txt">\
                <span class="food-icon-set"><%= foodIconSet %></span>\
            </div>\
            <input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" aria-labelledby="ariaLbl<%= index %>" aria-describedby="ariaDesc<%= index %>">\
        </div>\
		<img src="<%= btcImg %>" />\
		<div class="confirmation-prompt confirmation-remove--blue hidden">\
			<em class="ico-alert"></em>\
			<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod ternpor incididunt ut labore et dolore. </p>\
			<div class="button-group-1">\
				<input type="button" value="Proceed" class="btn-1 btn-proceed">\
				<a href="#" class="btn-2 btn-cancel">Cancel</a>\
			</div>\
		</div>\
    </div>';

	p.BTCLoadMoreBtn = '<div class="load-more">\
        <input type="button" name="select" value="Load more" class="load-more-btn">\
      </div>';
	p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';

	// applied special meal prompt
	p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
        <em class="ico-alert"></em>\
        <p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
      </div>');

	var mealSelEvent = null;
	var bckAllMealsEvent = null;

	p.removeButton = "<a class='meal-menu-remove-button' data-remove-trigger='1' href='#'> Remove</a>";

	var init = function (
		data,
		mealIcons,
		segment,
		sector,
		paxid,
		mealServiceCode,
		jsonData,
		accContent,
		animateClss) {
		p.BTCJson = data;
		p.mealIcons = mealIcons;
		p.segment = segment;
		p.sector = sector;
		p.paxid = paxid;
		p.mealServiceCode = mealServiceCode;
		p.btcMealCatCode = data[0].mealCategoryCode;
		p.data = jsonData;
		p.isPey = SIA.MealsSelection.public.isPey;
		p.isEco = SIA.MealsSelection.public.isEco;
		p.isBiz = SIA.MealsSelection.public.isBiz;
		p.isFirst = SIA.MealsSelection.public.isFirst;
		p.paxId = paxid;

		var btcMeal = paintBTCMeal(animateClss);
		backAllMealsEvent(btcMeal);
		mealSelectionEvent(btcMeal);

		accContent.append(btcMeal);
	};

	var paintBTCMeal = function (animateClss) {
		var animateClss = (animateClss != '') ? ' pre-slide-right hidden' : '';
		var bookTheCookEl = $('<div class="meal-sub-menu-wrap book-the-cook' + animateClss + '" />');

		// add back the all meals link
		bookTheCookEl.append(p.back2AllMealsLinkTpl);

		// add meal menu category name
		var mealMenuCatName = $(_.template(p.BTCTitleTpl, {
			txt: p.BTCJson[0].mealMenuCategoryName
		}));
		bookTheCookEl.append(mealMenuCatName);

		// add description
		bookTheCookEl.append(p.BTCDescTpl);

		// add search control
		bookTheCookEl.append(p.BTCSearhControl);

		var mealSubcategoryJson = p.BTCJson[0].mealSubcategory;
		var cruisineCatListItems = '';
		var allMealListing = $('<div class="seatmap btc-meal-list" role="application"></div>');
		var mealListingMaxNum = 5;
		var otherTabs = '';
		var totalTabs = mealSubcategoryJson.length;
		for (var i = 0; i < totalTabs; i++) {
			// create the Cuisine Category list
			// add default active to first
			var isActive = '';
			if (i == 0) {
				isActive = 'active';
			}

			var el = $(_.template(p.BTCCruisineCatListItemTpl, {
				'isActive': isActive,
				'menuName': mealSubcategoryJson[i].mealSubCategoryName,
				'tabMenu': i
			}));

			cruisineCatListItems += el[0].outerHTML;

			if (totalTabs > 4 && i > 2) {
				var opt = _.template(p.selOptionItemTpl, {
					'tabMenu': i,
					'menuName': mealSubcategoryJson[i].mealSubCategoryName
				});

				otherTabs += opt;
			}

			// create the Cuisine Category - Meal listing
			var mealListingWrapper = (i == 0) ?
				$('<div data-tab-content="' + i + '"></div>') :
				$('<div class="hidden" data-tab-content="' + i + '"></div>');
			var cruisineCatMealListingJson = mealSubcategoryJson[i].Meals;
			for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
				if (j < mealListingMaxNum) {
					mealListingWrapper.append(
						paintBTCEachMealItem(cruisineCatMealListingJson[j], j, i)
					);
				}
			}

			// check if there is more than 5 items, then add a load more button
			if (cruisineCatMealListingJson.length > mealListingMaxNum) {
				mealListingWrapper.append(p.BTCLoadMoreBtn);
			}

			allMealListing.append(mealListingWrapper);
		}

		// add to Cuisine Category list
		var cruisineCatListEl = $(_.template(p.BTCCruisineCatListTpl, {
			'catListItems': cruisineCatListItems
		}));

		// add to cruisine category main wrapper
		var cruisineCatMainWrapper = $(_.template(p.BTCCuisineCatMainWrapperTpl, {
			'tabList': cruisineCatListEl[0].outerHTML,
			'contentList': allMealListing[0].outerHTML
		}));

		cruisineCatMainWrapper.find('[name="calcTabSelect"]').append(otherTabs);

		// add to book the cook menu main wrapper
		bookTheCookEl.append(cruisineCatMainWrapper);

		// attach events
		loadMoreBtnEvent(cruisineCatMainWrapper, mealSubcategoryJson, mealListingMaxNum);
		searchBtnEvent(bookTheCookEl);

		var simpleTab = new SIA.VerticalTab(
			bookTheCookEl.find('.btc-meal-nav'),
			bookTheCookEl.find('.btc-meal-list'),
			bookTheCookEl.find('btc-meal-select select'),
			p.BTCJson[0].mealSubcategory.length);
		simpleTab.init();

		var moreTab = new SIA.MoreTab(cruisineCatMainWrapper.find('.booking-nav__item'), cruisineCatMainWrapper.find('[name="calcTabSelect"]'));
		moreTab.init();

		bookTheCookEl.append(p.back2AllMealsLinkTpl2);

		return bookTheCookEl;
	};

	var paintBTCEachMealItem = function (data, btnIndex, tabIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var btcSpecialMealName;
		if (data.isSpecialMeal === 'true' && typeof data.specialMealName !== 'undefined') {
			btcSpecialMealName = data.specialMealName;
		} else {
			btcSpecialMealName = 'SPECIAL';
		}

		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			// footLogo = '';
			var foodIconSet = '';
			for (var l = 0; l < data.specialityInfo.length; l++) {
				var br = l === data.specialityInfo.length - 1 ? '' : '<br>';
				foodIconSet += '<em class="' + SIA.MealsSelection.public.inflightDishIcons[data.specialityInfo[l].dishIconId] + ' fm-footnote-logo"></em>\
          <span class="fm-dish-text"> ' + data.specialityInfo[l].specialityFootNote + ' </span>' + br;
			}

			// <em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
			// <span class="fm-dish-text"><%= specialityFootNote %></span>\
			// specialityFootNote = dselMealselata.specialityInfo.specialityFootNote;
			// footIcon = p.mealIcons[data.specialityInfo.dishIconId];
		}

		// check for selected menu in data
		var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
		selMeal = selMealsel[p.mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		var tpl;
		if (typeof data.mealImage !== 'undefined' && data.mealImage !== '') {
			tpl = p.BTCMealItemWithImage;
		} else {
			tpl = p.BTCMealItem;
		}

		return $(_.template(tpl, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			// 'footLogo': footLogo,
			// 'footIcon': footIcon,
			// 'specialityFootNote': specialityFootNote,
			'foodIconSet': foodIconSet,
			'index': btnIndex,
			'tabIndex': tabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': p.segment,
			'sector': p.sector,
			'paxid': p.paxid,
			'mealServiceCode': p.mealServiceCode,
			'mealCatCode': p.btcMealCatCode,
			'btcImg': data.mealImage,
			'btcSpecialMealName': btcSpecialMealName
		}));
	}

	var loadMoreBtnEvent = function (el, data, maxPerList) {
		var loadMoreBtns = el.find('.load-more-btn');
		var content = '';

		for (var i = 0; i < loadMoreBtns.length; i++) {
			$(loadMoreBtns[i]).on('click', function () {
				var t = $(this);

				// get parent tab content
				var tabContentIndex = t.parent().parent().attr('data-tab-content');

				// get parent load more button
				var loadMoreBtnMain = t.parent();

				// get current index data
				var currentData = data[tabContentIndex].Meals;

				// compute if there is more than 5 meals left
				var leftMealsLength = currentData.length - maxPerList;
				var limitResult = (leftMealsLength >= maxPerList) ? true : false;
				for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
					if (k == (maxPerList - 1) && limitResult) {
						break;
					}

					var el = paintBTCEachMealItem(currentData[j], j, tabContentIndex);
					mealSelectionEvent(el);
					el.insertBefore(loadMoreBtnMain);

					if (k === 0) el.find('.fm-select-btn').focus();
				}

				// check if there is more left data, then hide the load more button
				// get total list length on tab content
				var tabContentMain = t.parent().parent();
				var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
				if (tabContentLength == currentData.length) {
					loadMoreBtnMain.addClass('hidden');
				}
			});
		}
	};

	var searchJSON = function (data, val) {
		var result = [];
		$.each(data, function (i, value) {
			$.each(data[i].Meals, function (j, value) {
				var mealMenuName = value.mealMenuName.toLowerCase();
				var mealMenuDescription = value.mealMenuDescription.toLowerCase();

				val = val.toLowerCase().trim();
				if ((mealMenuName.indexOf(val) >= 0) || (mealMenuDescription.indexOf(val) >= 0)) {
					result.push({
						'cat': i,
						'item': j
					});
				}
			});
		});

		return result;
	};

	var updateAttrDatas = function (el) {
		p.segment = el.attr('data-segment');
		p.sector = el.attr('data-sector');
		p.paxid = el.attr('data-curpax');
		p.mealServiceCode = el.attr('data-meal-service-code');
	};

	var searchBtnEvent = function (el) {
		var searchBtns = el.find('.search-btn');
		var clrSearchBtns = el.find('.clear-search');
		var tabContent = el.find('.tab-content');
		var searchMsgEl = el.find('.search-result-msg');
		var searchListClss = 'btc-meal-list';

		$.each(searchBtns, function () {
			searchBtns.on('click', function (e) {
				e.preventDefault();

				var t = $(this);

				updateAttrDatas(t.closest('.book-the-cook').parent().find('.all-meals-menu .item-container'));

				t.closest('.static-details').parent().next().find('.btc-meal-list').remove();

				// remove the btc meal list
				el.find('.btc-meals').remove();
				t.closest('.btc-search').next().removeClass('hidden');

				var val = t.closest('.static-content').find('[name="input-search-1"]').val();
				var mealSubCat = p.BTCJson['0'].mealSubcategory;
				var result = searchJSON(mealSubCat, val);

				var searchMsg = '';
				if (result.length >= 1) {
					searchMsg = result.length + ' search results for \'' + val + '\'';

					// generate meals blocks
					tabContent.append('<div class="' + searchListClss + '"></div>');
					var searchList = tabContent.find('.' + searchListClss);
					var resCatIds = [];
					$.each(result, function (i, meal) {
						var res = result[i];
						if (resCatIds.indexOf(res.cat) <= -1) {
							resCatIds.push(res.cat);
							searchList.append(_.template(p.searchMealTitleTpl, {
								'mealName': mealSubCat[res.cat].mealSubCategoryName
							}));
						}

						searchList.append(paintBTCEachMealItem(mealSubCat[res.cat].Meals[res.item], res.item, 0));
					});

					mealSelectionEvent(tabContent.find('.' + searchListClss));
				} else {
					// if no search result found
					searchMsg = 'There are no meals matching \'' + val + '\'. There are no results. Amend your search criteria to try again.';
					searchMsgEl.parent().addClass('no-result');
				}

				// generate search result message
				searchMsgEl.html(searchMsg);
				searchMsgEl.removeClass('hidden');
			});
		});

		$.each(clrSearchBtns, function () {
			clrSearchBtns.on('click', function (e) {
				e.preventDefault();

				var t = $(this);

				// get selected meal data index
				var selItem = t.closest('.static-content').next().find('.btc-meal-list .fm-inf-menu');
				$.each(selItem, function (i, value) {
					var selInput = $(selItem[i]).find('.fm-select-btn');
					if (selInput.attr('value') == 'Remove') {
						p.doSearch = true;
						p.selIndex = selInput.attr('data-btn-index');
					}
				});

				updateAttrDatas(t.closest('.book-the-cook').parent().find('.all-meals-menu .item-container'));

				// repaint the btc meal list
				var accordionContent = t.closest('.accordion__content');
				accordionContent.find('.book-the-cook').remove();
				accordionContent.append(paintBTCMeal(''));

				// attached event
				mealSelectionEvent(accordionContent.find('.btc-meal-list'));
				backAllMealsEvent(accordionContent.find('.book-the-cook'));
			});
		});
	};

	var backAllMealsEvent = function (el) {
		var allBackMealsBtn = el.find('.btn-back');
		for (var i = 0; i < allBackMealsBtn.length; i++) {
			$(allBackMealsBtn[i]).off().on('click', function (e) {
				e.preventDefault();

				var t = $(this);

				// get the accordion tab main content
				var accordionTabMainContent = t.parent().parent().parent();

				// get the current tab index content parent
				var currentIndexTabContent = t.parent().parent();

				// wait for the transition to end before adding a hidden
				// class to have smooth animation
				currentIndexTabContent.off().one('transitionend' || 'webkitTransitionEnd' ||
					'mozTransitionEnd' || 'oTransitionEnd',
					function () {
						// hide it to remove the height
						currentIndexTabContent.addClass('hidden');

						// show the all meals content
						var allMeals = accordionTabMainContent.find('.all-meals-menu');
						allMeals.removeClass('sub-menu-on');

						allMeals.parent().find('[data-checktype="not-eating"]').attr('tabindex', '0').addClass('focus-outline');

						allMeals.find('.meals-menu-item').each(function () {
							var t = $(this);
							t.attr('tabindex', '0');
							if (t.hasClass('inflight-menu')) {
								if (t.parent().hasClass('meal-not-ready')) {
									allMeals.find('.meals-menu-item.special-meals').focus();
								} else {
									t.focus();
								}
							}
						});
					});

				// hide book the cook
				currentIndexTabContent.addClass('pre-slide-right');

				currentIndexTabContent.removeClass('hidden');
			});
		}
	};

	var mealSelectionEvent = function (el) {
		var allSelectionBtn = el.find('.fm-select-btn');

		for (var i = 0; i < allSelectionBtn.length; i++) {
			$(allSelectionBtn[i]).parent().parent().on('click', function (e) {
				e.preventDefault();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var parentElList = parentEl.parent();
				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				var parentMealsList = t.closest('.' + t.attr('data-mealCatType'));
				var currMealTabIndex = t.attr('data-meal-tab-index');
				var badgeEl = $(parentEl).children();
				var btnIndex = t.attr('data-btn-index');
				var segmentAccordion = parentEl.closest("[data-accordion-wrapper-content]");

				// check if there is selected meal in inflight
				if (p.lastTick != null) {
					p.lastTick = null;
					parentMealsList.data('mealTabLast', '');
				}

				// add default selected
				if (i == 0) {
					parentMealsList.data('mealLastSelect', 0);
					parentMealsList.data('mealTabLast', 0);
				}

				// check if different tab index
				if (parentMealsList.data('mealTabLast') != currMealTabIndex) {
					parentMealsList.data('mealLastSelect', '');
					parentMealsList.data('mealTabLast', currMealTabIndex);
				}

				// clear if do a search in btc meals
				if (p.doSearch) {
					parentMealsList.data('mealLastSelect', p.selIndex);
					p.selIndex = false;
				}
				var lastButtonSelected = accordionContent.find(".fm-inf-menu.selected").find(".fm-select-btn");
				// check if current meal card is selected again, then remove it
				if (lastButtonSelected.data("meal-mealcode") === t.data("meal-mealcode")) {
					var allPrompt = parentEl.find(".confirmation-prompt");
					var removePrompt = parentEl.find(".confirmation-remove--blue");
					
					if (parentEl.hasClass("selected")) {
						var defautLabel = "Inflight Meal";
						segmentAccordion.find(".fm-inf-menu.selected").find(".fm-select-btn")
							.each(function() {
								var _self = $(this);
								if (_self.data("meal-category") == "SPML") defautLabel = _self.data("meal-menuname");
							});
						allPrompt.each(function() { $(this).addClass("hidden") });
						removePrompt.removeClass("hidden");
						t.addClass("hidden");
						removePrompt.find(".menu-label").text(defautLabel);

						removePrompt.find(".btn-cancel")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								t.removeClass("hidden");
								allPrompt.each(function() { $(this).addClass("hidden") });
							});

						removePrompt.find(".btn-proceed")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								t.removeClass("hidden");
								removePrompt.addClass("hidden");
								removeButtonListenerMealSelection(t);
							});

						return;
					}
					// search and remove any previous selected meal
					SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
					// Add selected class to the input and to the container
					parentEl.removeClass('selected');
					t.removeClass('selected');

					// remove hidden class to show the selected badge
					$(badgeEl["0"]).addClass('hidden');

					t.attr('value', 'Select');

					parentMealsList.data('mealLastSelect', '');
					
					// change accordion tab previously selected mean to its default text('inflight menu')
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					accordionTabMealTitle.html(inflLbl);

					defaultSelection(t);

					SIA.MealsSelection.updateData(t);

					// Check if there are selected special meals then select same meal for this service code
					for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
						var spml = SIA.MealsSelection.public.spm[i];
						if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
							spml.selectSegmentMeal();
							p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
							if(!parentEl.hasClass('past-meal-menu')) parentEl.after(p.spmlAlertPrompt);

							$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
								var t = $(e.target);
								if (t.hasClass('passenger-info__text') ||
									t.hasClass('meal-tab-item') ||
									t.hasClass('accordion__control') ||
									t.hasClass('ico-point-d') ||
									t.hasClass('fm-ico-text') ||
									t.hasClass('meal-service-name') ||
									t.hasClass('flight-ind') ||
									t.hasClass('btn-back') ||
									t.hasClass('fm-inf-menu') ||
									t.hasClass('search-btn')
								) {
									if(!t.hasClass('past-meal')) p.spmlAlertPrompt.detach();

									$(document).off('click.spmlPromptCheck');
								}
							});

							break;
						}

					}

				} else {
					// search and remove any previous selected meal
					SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

					// Change past meal states without triggering click event
					resetPastMeals(t.closest('.all-meals-list'));

					// Select same btc meal in past meals list
					selectPastMeal(t, accordionContent);

					// Add selected class to the input and to the container
					parentEl.addClass('selected');
					t.addClass('selected');

					// remove hidden class to show the selected badge
					$(badgeEl["0"]).removeClass('hidden');

					t.attr('value', 'Remove');

					parentMealsList.data('mealLastSelect', btnIndex);

					// get the selected mean name
					var selMealName = parentEl.find('.fm-inf-menu-title').text();

					// change accordion tab default text('inflight menu') to selected meal
					var selectedLabel = '';
					// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
					selectedLabel = '<span class="title-selected">Selected:</span> ';

					accordionTabMealTitle.html(selectedLabel + selMealName + ' (Book the Cook)');
					// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
					accordionTabMealTitle.append($(p.removeButton));

					SIA.MealsSelection.resetNotEating(t);

					removeDefaultSelection(t);
					removeButtonListener();

					SIA.MealsSelection.updateData(t);
				}
			});
		}
	};

	var addInputListener = function(el){
		el.parent().parent().on('click', function (e) {
			e.preventDefault();

			var t = $(this).find('.fm-select-btn');
			var parentEl = t.parent().parent();
			var parentElList = parentEl.parent();
			var accordionContent = t.closest('.accordion__content');
			var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
			var parentMealsList = t.closest('.' + t.attr('data-mealCatType'));
			var currMealTabIndex = t.attr('data-meal-tab-index');
			var badgeEl = $(parentEl).children();
			var btnIndex = t.attr('data-btn-index');
			var segmentAccordion = parentEl.closest("[data-accordion-wrapper-content]");

			// check if there is selected meal in inflight
			if (p.lastTick != null) {
				p.lastTick = null;
				parentMealsList.data('mealTabLast', '');
			}

			var lastButtonSelected = accordionContent.find(".fm-inf-menu.selected").find(".fm-select-btn");
			// check if current meal card is selected again, then remove it
			if (lastButtonSelected.data("meal-mealcode") === t.data("meal-mealcode")) {
				var allPrompt = parentEl.find(".confirmation-prompt");
				var removePrompt = t.hasClass('past-meal') ? parentEl.closest('.past-meals-slider').find('.past-meal-prompt') : parentEl.find(".confirmation-remove--blue");
				
				if (parentEl.hasClass("selected")) {
					var defautLabel = "Inflight Meal";
					segmentAccordion.find(".fm-inf-menu.selected").find(".fm-select-btn")
						.each(function() {
							var _self = $(this);
							if (_self.data("meal-category") == "SPML") defautLabel = _self.data("meal-menuname");
						});
					allPrompt.each(function() { $(this).addClass("hidden") });
					removePrompt.removeClass("hidden");
					t.addClass("hidden");
					removePrompt.find(".menu-label").text(defautLabel);

					removePrompt.find(".btn-cancel")
						.off().on("click", function(evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							t.removeClass("hidden");
							allPrompt.each(function() { $(this).addClass("hidden") });

							if(t.hasClass('past-meal')) removePrompt.addClass('hidden');
						});

					removePrompt.find(".btn-proceed")
						.off().on("click", function(evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							t.removeClass("hidden");
							removePrompt.addClass("hidden");
							removeButtonListenerMealSelection(t);

							if(t.hasClass('past-meal')) removePrompt.addClass('hidden');
						});

					return;
				}
				// search and remove any previous selected meal
				SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
				// Add selected class to the input and to the container
				parentEl.removeClass('selected');
				t.removeClass('selected');

				// remove hidden class to show the selected badge
				$(badgeEl["0"]).addClass('hidden');

				t.attr('value', 'Select');

				parentMealsList.data('mealLastSelect', '');
				
				// change accordion tab previously selected mean to its default text('inflight menu')
				var inflLbl = 'Inflight Menu';
				if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
				accordionTabMealTitle.html(inflLbl);

				defaultSelection(t);

				// SIA.MealsSelection.updateData(t);

				// Check if there are selected special meals then select same meal for this service code
				for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
					var spml = SIA.MealsSelection.public.spm[i];
					if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
						spml.selectSegmentMeal();
						p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
						if(!parentEl.hasClass('past-meal-menu')) parentEl.after(p.spmlAlertPrompt);

						$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
							var t = $(e.target);
							if (t.hasClass('passenger-info__text') ||
								t.hasClass('meal-tab-item') ||
								t.hasClass('accordion__control') ||
								t.hasClass('ico-point-d') ||
								t.hasClass('fm-ico-text') ||
								t.hasClass('meal-service-name') ||
								t.hasClass('flight-ind') ||
								t.hasClass('btn-back') ||
								t.hasClass('fm-inf-menu') ||
								t.hasClass('search-btn')
							) {
								p.spmlAlertPrompt.detach();

								$(document).off('click.spmlPromptCheck');
							}
						});

						break;
					}

				}

			} else {
				// search and remove any previous selected meal
				SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

				// Change past meal states without triggering click event
				resetPastMeals(t.closest('.past-meals-wrap').parent(), t);

				accordionContent.find('.fm-select-btn[data-meal-category="'+t.attr('data-meal-category')+'"][data-meal-mealcode="'+t.attr('data-meal-mealcode')+'"]').each(function(){
					var meal = $(this);
					if(!meal.hasClass('past-meal')) {
						meal.parent().parent().trigger('click');
					}
				});

				// Set card to selected state
				// t.closest('.accordion-wrapper-content').find('.past-meals-wrap [data-meal-category="'+t.attr('data-meal-category')+'"][data-meal-mealcode="'+t.attr('data-meal-mealcode')+'"]').each(function(){
				// 	var t = $(this);
				// 	t.val('Remove').addClass('selected').parent().parent().addClass('selected');
				// });
				t.val('Remove').addClass('selected').parent().parent().addClass('selected');
			}
		});
	};

	var defaultSelection = function (el) {
		var wrapper = el.closest("[data-accordion-content]");
		var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
		if (!wrapper.find(".fm-inf-menu.selected").length) {
			wrapperInflightMenu.find(".select-label").addClass("hidden");
		}
	}

	var removeDefaultSelection = function (mealButton) {
		var parent = mealButton.closest("[data-accordion-content]");
		var borderHighlight = parent.find(".ml-inflight-container");
		var alertPrompt = parent.find(".inflight-confirmation-prompt");
		borderHighlight.find(".select-label").removeClass("hidden");
		alertPrompt.remove();
	};

	var removeButtonListener = function () {
		setTimeout(function () {
			var pax = SIA.MealsSelection.public.data.paxList[p.paxId];
			var segmentMeals = pax.selectedMeals[p.segment];
			var mealTitleTpl = SIA.MealsSelection.public.mealTitleTpl;
			var mealDictionary = SIA.MealsSelection.public.mealDictionary;

			$("[data-accordion-trigger]").find("[data-remove-trigger]").off()
				.on("click", function (evt) {
					evt.preventDefault();
					evt.stopPropagation();
					
					var self = $(this);
					var wrapper = self.closest(".accordion-box");
					var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
					var removeWrap = wrapper.find(".remove-prompt");
					removeWrap.eq(0).removeClass("hidden");
					self.addClass("hidden");

					var segmentMealsValues = Object.values(segmentMeals.selectedMeal);
					segmentMealsValues = _.findWhere(segmentMealsValues, { mealCategoryCode: "SPML", isEating: true });

					removeWrap.find(".menu-label").text(segmentMealsValues ? segmentMealsValues.mealMenuName : "Inflight Meal");

					removeWrap.find(".btn-cancel")
						.off().on("click", function(evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							removeWrap.addClass("hidden");
							self.removeClass("hidden");
						});

					removeWrap.find(".btn-proceed")
						.off().on("click", function (evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							wrapper.find(".fm-inf-menu.selected").each(function () {
								var self = $(this);
								var mealButton = self.find(".fm-select-btn");
								if (mealButton.data("meal-category") == "SPML") {
									// remove other selected past special meals
									if(mealButton.attr('data-meal-category') === 'SPML' && mealButton.hasClass('past-meal')) mealButton.closest('.accordion-wrapper-content').find('.past-meal[data-meal-category="SPML"]').each(function(){
										$(this).val('Select').parent().parent().removeClass('selected');
									});
									
									SIA.MealsSelection.resetToInflight(mealButton);
								} else {
									unSelectMeal(mealButton);
								}
								undoMeal = mealButton;
							});

							segmentMealsValues = Object.values(segmentMeals.selectedMeal);
							segmentMealsValues = _.findWhere(segmentMealsValues, { mealCategoryCode: "SPML", isEating: true });
							
							if (segmentMealsValues) {
								var mealBtn = wrapper.find(".fm-select-btn[data-meal-mealcode='" + segmentMealsValues.mealCode + "']");
								SIA.MealsSelection.selectMeal(mealBtn);
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html("");
								wrapper.find(".fm-accordion-label .meal-sub-title-black").append(
									$(_.template(mealTitleTpl, {
										mealName: segmentMealsValues.mealMenuName,
										mealType: mealDictionary.SPML,
										hideRemove: !(p.isEco || p.isPey || p.isFirst || p.isBiz)
									}))
								);
								SIA.MealsSelection.updateData(mealBtn);
								removeButtonListener();
							} else {
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
								SIA.MealsSelection.removeInputData(wrapper);
							}

							removeWrap.addClass("hidden");
						});
				});
		}, 500);
	};

	var removeButtonListenerMealSelection = function (el) {
		var pax = SIA.MealsSelection.public.data.paxList[p.paxId];
		var segmentMeals = pax.selectedMeals[p.segment];
		var mealTitleTpl = SIA.MealsSelection.public.mealTitleTpl;
		var mealDictionary = SIA.MealsSelection.public.mealDictionary;

		var self = el;
		var wrapper = self.closest(".accordion-box");
		var wrapperInflightMenu = wrapper.find(".ml-inflight-container");

		wrapper.find(".fm-inf-menu.selected").each(function () {
			var self = $(this);
			var mealButton = self.find(".fm-select-btn");
			if (mealButton.data("meal-category") == "SPML") {
				SIA.MealsSelection.resetToInflight(mealButton);
			} else {
				unSelectMeal(mealButton);
			}
			undoMeal = mealButton;
		});

		var segmentMealsValues = Object.values(segmentMeals.selectedMeal);
		segmentMealsValues = _.findWhere(segmentMealsValues, { mealCategoryCode: "SPML", isEating: true });

		if (segmentMealsValues) {
			var mealBtn = wrapper.find(".fm-select-btn[data-meal-mealcode='" + segmentMealsValues.mealCode + "']");
			SIA.MealsSelection.selectMeal(mealBtn);
			wrapper.find(".fm-accordion-label .meal-sub-title-black").html("");
			wrapper.find(".fm-accordion-label .meal-sub-title-black").append(
				$(_.template(mealTitleTpl, {
					mealName: segmentMealsValues.mealMenuName,
					mealType: mealDictionary.SPML,
					hideRemove: !(p.isEco || p.isPey || p.isBiz || p.isFirst)
				}))
			);
			SIA.MealsSelection.updateData(mealBtn);
			removeButtonListener();
		} else {
			wrapper.find(".fm-accordion-label .meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
			SIA.MealsSelection.removeInputData(wrapper);
		}
	};

	var unSelectMeal = function (t) {
		var parentEl = t.parent().parent();
		var parentElList = parentEl.parent();
		var accordionContent = t.closest('.accordion__content');
		var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
		var parentMealsList = t.closest('.' + t.attr('data-mealCatType'));
		var currMealTabIndex = t.attr('data-meal-tab-index');
		var badgeEl = $(parentEl).children();
		var btnIndex = t.attr('data-btn-index');

		parentEl.removeClass('selected');
		t.removeClass('selected');

		// remove hidden class to show the selected badge
		$(badgeEl["0"]).addClass('hidden');

		t.attr('value', 'Select');

		parentMealsList.data('mealLastSelect', '');

		// change accordion tab previously selected mean to its default text('inflight menu')
		var inflLbl = 'Inflight Menu';
		if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
		accordionTabMealTitle.html(inflLbl);

		defaultSelection(t);

		SIA.MealsSelection.updateData(t);

		// Check if there are selected special meals then select same meal for this service code
		for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
			var spml = SIA.MealsSelection.public.spm[i];
			if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
				spml.selectSegmentMeal();
				p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
				if(!parentEl.hasClass('past-meal-menu')) parentEl.after(p.spmlAlertPrompt);


				$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
					var t = $(e.target);
					if (t.hasClass('passenger-info__text') ||
						t.hasClass('meal-tab-item') ||
						t.hasClass('accordion__control') ||
						t.hasClass('ico-point-d') ||
						t.hasClass('fm-ico-text') ||
						t.hasClass('meal-service-name') ||
						t.hasClass('flight-ind') ||
						t.hasClass('btn-back') ||
						t.hasClass('fm-inf-menu') ||
						t.hasClass('search-btn')
					) {
						p.spmlAlertPrompt.detach();

						$(document).off('click.spmlPromptCheck');
					}
				});

				break;
			}

		}
	};

	var resetPastMeals = function(wrap) {
		wrap.parent().find('.past-meals-wrap [data-meal-category]').each(function(){
			var btc = $(this);
			btc.val('Select').removeClass('selected');
			btc.parent().parent().removeClass('selected');
		});
	};

	var selectPastMeal = function(meal, wrap) {
		var btn = wrap.find('.past-meal-menu [data-meal-category="'+meal.attr('data-meal-category')+'"][data-meal-mealcode="'+meal.attr('data-meal-mealcode')+'"]');
		btn.val('Remove').addClass('selected').parent().parent().addClass('selected');
	};

	var oPublic = {
		init: init,
		paintBTCMeal: paintBTCMeal,
		public: p,
		addInputListener: addInputListener
	};

	return oPublic;
};

SIA.SpecialMeal = function (jsonData,
	paxData,
	accContent,
	segmentId,
	sectorId,
	serviceId,
	paxId,
	mealServiceCode) {
	var p = {};

	p.vTab = '<div data-fixed="true" class="sidebar">\
            <div class="inner" role="application">\
                <ul class="booking-nav special-meal-nav" role="tablist"></ul>\
            </div>\
            <div data-customselect="true" class="custom-select custom-select--2 custom-select--3 multi-select tab-select special-meal-select" data-select-tab="true">\
                <label for="calcTabSelect<%= menuId %>" class="select__label">&nbsp;</label>\
                <span class="select__text"><%= mealCat %></span><span class="ico-dropdown"></span>\
                <select id="smTabSelect<%= menuId %>" name="smTabSelect<%= menuId %>">\
                </select>\
            </div>\
        </div>\
        <div class="seatmap special-meal-list" role="application"></div>';
	p.tabItem = '<li class="booking-nav__item tab-item<%= active %>" role="tab"  data-tab-menu="<%= tabId %>">\
            <a href="#" role="tab" class="meal-tab-item">\
                <span class="passenger-info">\
                    <span class="passenger-info__text"><%= label %></span>\
                </span>\
            </a>\
        </li>';

	p.menuItem = '<div class="fm-inf-menu <%= selectedClass %>">\
        <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
        <h4 class="fm-inf-menu-title"><%= menuName %></h4>\
        <p class="fm-inf-menu-desc"><%= menuDescription %></p>\
        <div class="fm-footnote-txt">\
            <input type="button" name="" value="<%= inputVal %>" data-mealcattype="speacial-meal-list" data-specialMealSelect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
        </div>\
        <div data-prompt="with-selection" class="confirmation-prompt confirmation-prompt--blue hidden">\
            <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change your other special meals selection from <%= origin %> to <%= dest %> to <span><%= menuName %></span> as well. </p>\
            <div class="button-group-1">\
            <input type="button" value="Proceed" class="btn-1 btn-proceed">\
            <a href="#" class="btn-2 btn-cancel">Cancel</a>\
            </div>\
		</div>\
		<div class="confirmation-prompt confirmation-remove--blue hidden">\
			<em class="ico-alert"></em>\
            <p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span>Inflight Meal</span> sed do eiusmod ternpor incididunt ut labore et dolore. </p>\
            <div class="button-group-1">\
				<input type="button" value="Proceed" class="btn-1 btn-proceed">\
				<a href="#" class="btn-2 btn-cancel">Cancel</a>\
            </div>\
        </div>\
        <div data-prompt="initial" class="confirmation-prompt hidden">\
            <em class="ico-alert"></em>\
            <p class="meal-sub-title-black">For your convenience, all your meal choices from <%= origin %> to <%= dest %> have been changed to <span><%= menuName %></span> as well. If you&apos; prefer other meals, make your selections below. </p>\
        </div>\
    </div>';

	// Array to store all meals selection
	p.mealsArr = [];

	p.curPrompt = null;

	p.curSMLName = '';

	p.removeButton = "<a class='meal-menu-remove-button' data-remove-trigger='1' href='#'> Remove</a>";

	var init = function () {
		p.data = paxData;
		p.jsonData = jsonData;

		p.curPaxId = paxId;
		p.segmentId = segmentId;
		p.sectorId = sectorId;
		p.serviceId = serviceId;
		p.mealCatCode = jsonData[0].mealCategoryCode;
		p.mealCatName = jsonData[0].mealMenuCategoryName;
		p.totalTabs = jsonData[0].mealSubcategory.length;
		p.mealServiceCode = mealServiceCode;
		p.isPey = SIA.MealsSelection.public.isPey;
		p.isEco = SIA.MealsSelection.public.isEco;
		p.isBiz = SIA.MealsSelection.public.isBiz;
		p.isFirst = SIA.MealsSelection.public.isFirst;

		p.specialMealWrap = accContent.find('.special-meal-wrap');
		p.contentWrap = p.specialMealWrap.find('.responsive.special-meals');

		p.specialVTab = $(_.template(p.vTab, {
			'menuId': '' + segmentId + sectorId + serviceId,
			'mealCat': p.mealCatName
		}));
		p.contentWrap.append(p.specialVTab);

		paintTabMenu();
		paintTabContent();
		backToMainMenu();

		// var firstInput = $(p.menuList.find('.fm-select-btn')[0]);
		// firstInput.attr('data-first-focus','true');

		p.simpleTab = new SIA.VerticalTab(p.tabMenu, p.menuList, p.tabSelect, p.totalTabs);
		p.simpleTab.init();

	};

	var paintTabMenu = function () {
		p.tabMenu = p.specialVTab.find('.special-meal-nav');
		p.tabSelect = p.specialVTab.find('.special-meal-select select');

		for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
			var mealCatArr = p.jsonData[0].mealSubcategory[i];
			var active = '';
			var selected = '';

			if (i == 0) {
				active = ' active';
				selected = ' selected="true"';
			}

			var tabItem = $(_.template(p.tabItem, {
				'tabId': i,
				'label': mealCatArr.mealSubCategoryName,
				'active': active
			}));

			// Since on tablet portrait you can fit 4 tabs, only add options greater than 3rd index
			if (i > 2 && p.totalTabs > 4) {
				var tabOption = $('<option data-id="' + i + '" value="' + mealCatArr.mealSubCategoryName + '"' + selected + ' />');

				p.tabSelect.append(tabOption);
			}

			p.tabMenu.append(tabItem);
		}
	};

	var paintTabContent = function () {
		p.menuList = p.contentWrap.find('.special-meal-list');
		var curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

		for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
			var mealCatArr = p.jsonData[0].mealSubcategory[i];
			// Class for the content tab wrapper
			var hidden = '';
			if (i > 0) hidden = ' class="hidden"';

			var menuItemsWrap = $('<div data-tab-content="' + i + '"' + hidden + ' />');
			for (var j = 0; j < mealCatArr.Meals.length; j++) {
				var menu = mealCatArr.Meals[j];

				var selectClass = '';
				var viewOnlyClass = ' hidden';
				var mealCatCode = p.jsonData[0].mealCategoryCode;

				// check for any previous selected item
				if (curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == menu.mealCode) {
					selectedClass = 'selected';
					inputVal = 'Remove';
					isSelected = '';

					p.lastTick = p.mealsArr.length;
				} else {
					selectedClass = '';
					inputVal = 'Select';
					isSelected = 'hidden';
				}

				if (typeof menu.isViewOnly != 'undefined' && menu.isViewOnly) {
					selectClass = ' hidden';
					viewOnlyClass = '';
				}

				var origin = SIA.MealsSelection.public.json.flightSegment[p.segmentId].departureAirportCode;
				var dest = SIA.MealsSelection.public.json.flightSegment[p.segmentId].arrivalAirportCode;

				var menuItem = $(_.template(p.menuItem, {
					'id': p.mealsArr.length,
					'menuId': i + '-' + j,
					'menuName': menu.mealMenuName,
					'menuDescription': menu.mealMenuDescription,
					'segmentId': p.segmentId,
					'sectorId': p.sectorId,
					'paxId': p.curPaxId,
					'mealCategoryCode': p.mealCatCode,
					'mealServiceCode': mealServiceCode,
					'selectClass': selectClass,
					'mealServiceId': serviceId,
					'mealCode': menu.mealCode,
					'mealMenuName': menu.mealMenuName,
					'origin': origin,
					'dest': dest
				}));

				menuItemsWrap.append(menuItem);

				p.mealsArr.push(menuItem);

				menuItem.find('input.fm-select-btn').each(function () {
					addInputListener($(this));
				});
			}

			p.menuList.append(menuItemsWrap);
		}
	};

	var backToMainMenu = function () {
		// add back to all meals button event
		p.specialMealWrap.find('.fm-backtomeals-btn .btn-back').off().on('click', function (e) {
			e.preventDefault();
			var subMenuWrapper = $(this).parent().parent();
			subMenuWrapper.off().one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd',
				function () {
					$(this).addClass('hidden');

					// reset position of main menu wrapper
					// show the all meals content
					var allMeals = $(this).parent().find('.all-meals-menu');
					allMeals.removeClass('sub-menu-on');

					allMeals.find('.meals-menu-item').attr('tabindex', '0');
					var cb = allMeals.find('[data-checktype="not-eating"]');
					cb.attr('tabindex', '0');
					cb.focus();
					cb.addClass('focus-outline')
				});

			// hide inflight menu wrapper
			subMenuWrapper.addClass('pre-slide-right');

			// show all menu wrapper
			var prevAllMeal = $(this).parent().parent().parent();

			p.mealCardList = null;
		});
	};

	var addInputListener = function (input) {
		input.parent().parent().off().on({
			'click': function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				var parentElList = parentEl.parent();

				var selectMealCode = t.data('meal-mealcode');
				var hasOtherSpecials = getOtherSpecials(t);
				var haveItemsToUpdate = getMoreToSelect(t) || $('body').hasClass('meals-selection-bah');

				var curAccordWrapper = SIA.MealsSelection.public.mealSelectionForm.find('.flightmeal-control')[p.segmentId];

				// check if current meal card is selected again, then remove it
				var badgeEl = $(parentEl).children();

				if (t.val() === 'Remove') {
					return unSelectMeal(t, true);
					
					// change accordion tab previously selected mean to its default text('inflight menu')
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					accordionTabMealTitle.html(inflLbl);

					SIA.MealsSelection.updateData(t);
				} else {
					if (haveItemsToUpdate) {
						if (hasOtherSpecials.length > 0) {
							var prompt = t.parent().parent().find('[data-prompt="with-selection"]');
						} else {
							var prompt = t.parent().parent().find('[data-prompt="initial"]');
						}

						// Remove any selected prompt
						if (p.curPrompt !== null) {
							p.curPrompt.prev().find('.fm-select-btn').val('Select');
							p.curPrompt.addClass('hidden');
							p.curPrompt.parent().removeClass('int-prompt');
							p.curPrompt.find('.btn-cancel').off();
							p.curPrompt.find('.btn-proceed').off();
						}

						prompt.removeClass('hidden');
						
						resetPastMeals(t.closest('.accordion-box'), t);

						// reset same row of past meals
						if(t.hasClass('past-meal')) t.closest('.past-meals-wrap').find('.past-meal').each(function(){
							$(this).val('Select').parent().parent().removeClass('selected');
						});

						if (hasOtherSpecials.length > 0 && !t.hasClass('past-meal')) {
							t.val('Selected');

							prompt.parent().addClass('int-prompt');

							prompt.find('.btn-cancel').off().on({
								'click': function (e) {
									e.preventDefault();
									e.stopImmediatePropagation();

									t.val('Select');
									prompt.addClass('hidden');
									prompt.parent().removeClass('int-prompt');

									p.curPrompt = null;
								}
							});

							prompt.find('.btn-proceed').off().on({
								'click': function (e) {
									e.preventDefault();
									e.stopImmediatePropagation();

									// remove listener to prevent double calls on otherSM
									$(this).off();

									// reset other meals before assigning selected class to current item
									SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

									selectMeal(t);

									// apply selection to other special meals in same leg
									selectSameMeal(t, hasOtherSpecials);

									// assign data
									var selectedLabel = '';
									// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
									selectedLabel = '<span class="title-selected">Selected:</span> ';

									accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
									// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
									accordionTabMealTitle.append($(p.removeButton));
									SIA.MealsSelection.updateData(t);
								}
							});
						} else {
							// reset other meals before assigning selected class to current item
							SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

							var sameSpecialsSelected = getSameSpecials(t);
							
							selectMeal(t);

							// apply selection to other special meals in same leg
							selectSameMeal(t, hasOtherSpecials);

							var selectedLabel = '';
							// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
							selectedLabel = '<span class="title-selected">Selected:</span> ';

							// assign data
							accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
							// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
							accordionTabMealTitle.append($(p.removeButton));
							removeButtonListener();

							SIA.MealsSelection.updateData(t);

							if (sameSpecialsSelected.items.length < sameSpecialsSelected.length - 1) {
								prompt.removeClass('hidden');
								$(document).off().on('click.promptCheck', function (e) {
									var t = $(e.target);
									if (t.hasClass('passenger-info__text') ||
										t.hasClass('meal-tab-item') ||
										t.hasClass('accordion__control') ||
										t.hasClass('meal-sub-title-black') ||
										t.hasClass('ico-point-d') ||
										t.hasClass('fm-ico-text') ||
										t.hasClass('meal-service-name') ||
										t.hasClass('flight-ind') ||
										t.hasClass('btn-back')
									) {
										p.curPrompt.addClass('hidden');
										p.curPrompt.parent().removeClass('int-prompt');

										$(document).off('click.promptCheck');
									}
								});
							}

						}

						p.curPrompt = prompt;

					} else {
						// reset other meals before assigning selected class to current item
						SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
						console.log('calling reset');
						resetPastMeals(t.closest('.accordion-box'));
						
						selectMeal(t);

						var selectedLabel = '';
						// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
						selectedLabel = '<span class="title-selected">Selected:</span> ';

						// assign data
						accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
						// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
						accordionTabMealTitle.append($(p.removeButton));
						removeButtonListener();

						SIA.MealsSelection.updateData(t);
					}

					// select same past meals
					selectSamePastMeal(t);
				}
			}
		});
	};

	var getOtherSpecials = function (meal) {
		var otherSelectedSM = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode !== meal.attr('data-meal-mealcode')) {
				otherSelectedSM.push({
					'k': k,
					'val': v
				});
			}
		});

		return otherSelectedSM;
	};

	var getSameSpecials = function (meal) {
		var sameSelectedSM = {};
		sameSelectedSM['length'] = 0;
		sameSelectedSM['items'] = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode === meal.attr('data-meal-mealcode')) {
				sameSelectedSM.items.push({
					'k': k,
					'val': v
				});
			}

			sameSelectedSM.length++;
		});

		return sameSelectedSM;
	};

	var getMoreToSelect = function (meal) {
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];

		var hasSPMLToChange = false;
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== meal.attr('data-meal-servicecode') && (v.mealCategoryCode === 'SPML' || v.mealCategoryCode === null) && v.isEating) {
				hasSPMLToChange = true;
			}
		});

		return hasSPMLToChange;
	};

	var selectSameMeal = function (meal) {
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];

		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
			var spml = SIA.MealsSelection.public.spm[i];

			if (p.curPaxId !== spml.p.curPaxId) continue;

			spml.p.specialMealWrap.find('.fm-select-btn').each(function () {
				var t = $(this);

				var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];
				if (typeof curCatCode !== 'undefined' &&
					curCatCode.isEating &&
					t.attr('data-meal-category') === 'SPML' &&
					(curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {

					// Unselect other selected special meals in same sector
					if (t.attr('data-segment') === meal.attr('data-segment')) {
						unSelectMeal(t);
					}

					// Find the selected special meal in current segment and select it
					if (t.attr('data-segment') === meal.attr('data-segment') &&
						t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')) {
						selectMeal(t);

						var parentEl = t.parent().parent();
						var accordionContent = t.closest('.accordion__content');
						var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");

						// SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
						var selectedLabel = '';
						// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
						selectedLabel = '<span class="title-selected">Selected:</span> ';

						accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
						// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
						accordionTabMealTitle.append($(p.removeButton));
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var getSelectedSpml = function () {
		var selectedSPML = '';
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (v.mealCategoryCode === 'SPML' && v.mealCode !== null) {
				selectedSPML = v.mealCode;
			}
		});

		return selectedSPML;
	};

	// Used to select special meals when a meal is unselected from BTC or INFLM
	var selectSegmentMeal = function (mealServiceCode, segmentId) {
		var selectedMealCode = getSelectedSpml();

		for (var i = 0; i < p.mealsArr.length; i++) {
			var meal = p.mealsArr[i].find('.fm-select-btn');
			if (meal.attr('data-meal-mealcode') === selectedMealCode) {

				var parentEl = meal.parent().parent();
				var accordionContent = meal.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");

				selectMeal(meal);

				var selectedLabel = '';
				// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
				selectedLabel = '<span class="title-selected">Selected:</span> ';

				p.curSMLName = meal.attr('data-meal-menuname');
				accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals) ');
				// if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
				accordionTabMealTitle.append($(p.removeButton));
				// assign data
				SIA.MealsSelection.updateData(meal);

				break;
			}
		}
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');
		
		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
		removeDefaultSelection(meal);
		
		meal.closest("[data-accordion-content]").trigger("specialMealSelect");
		removeButtonListener();
	};

	var unSelectMeal = function (meal, isRemoveSingleElement) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');
		var allPrompt = parentEl.find(".confirmation-prompt");
		var removePrompt = meal.hasClass('past-meal') ? parentEl.closest('.past-meals-slider').find('.past-meal-prompt') : parentEl.find(".confirmation-remove--blue");
		
		if (!isRemoveSingleElement) {
			return unSelectMealFn(meal, parentEl, badgeEl);
		}

		allPrompt.each(function() {
			$(this).addClass("hidden");
		});

		meal.addClass("hidden");

		removePrompt.removeClass("hidden");
		removePrompt
			.find(".btn-proceed")
			.off().on("click", function(evt) {
				evt.preventDefault();
				evt.stopImmediatePropagation();
				meal.removeClass("hidden");
				if(meal.hasClass('past-meal')) {
					removePrompt.addClass("hidden");
				}
				
				resetToInflight(meal);
				allPrompt.each(function() {
					$(this).addClass("hidden");
				});
				
				resetPastMeals(meal.closest('.accordion-box'), meal);
			});
		removePrompt
			.find(".btn-cancel")
			.off().on("click", function(evt) {
				evt.preventDefault();
				evt.stopImmediatePropagation();
				meal.removeClass("hidden");

				if(meal.hasClass('past-meal')) {
					removePrompt.addClass("hidden");
				}

				allPrompt.each(function() {
					$(this).addClass("hidden");
				});
			});
	};

	var unSelectMealFn = function(meal, parentEl, badgeEl) {
		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
		defaultSelection(meal);
	};

	var resetToInflight = function(meal) {
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[meal.data("segment")];

		for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
			var spml = SIA.MealsSelection.public.spm[i];

			if (p.curPaxId !== spml.p.curPaxId) continue;
			spml.p.specialMealWrap.find('.fm-inf-menu.selected').each(function () {
				var t = $(this).find('.fm-select-btn');
				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				
				var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];
				if (typeof curCatCode !== 'undefined' &&
					curCatCode.isEating &&
					t.attr('data-meal-category') === 'SPML' &&
					(curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {
					// Unselect other selected special meals in same sector
					if (t.attr('data-segment') === meal.attr('data-segment')) {
						var inflLbl = 'Inflight Menu';
						if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
						accordionTabMealTitle.html(inflLbl);
						unSelectMeal(t);
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var defaultSelection = function (el) {
		var wrapper = el.closest("[data-accordion-content]");
		var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
		if (!wrapper.find(".fm-inf-menu.selected").length) {
			wrapperInflightMenu.find(".select-label").addClass("hidden");
		}
	};

	var removeDefaultSelection = function (mealButton) {
		var parent = mealButton.closest("[data-accordion-content]");
		var borderHighlight = parent.find(".ml-inflight-container");
		var alertPrompt = parent.find(".inflight-confirmation-prompt");
		borderHighlight.find(".select-label").removeClass("hidden");
		alertPrompt.remove();
	};

	var removeButtonListener = function () {
		setTimeout(function () {
			var pax = SIA.MealsSelection.public.data.paxList[paxId];
			var segmentMeals = pax.selectedMeals[segmentId];
			var mealTitleTpl = SIA.MealsSelection.public.mealTitleTpl;
			var mealDictionary = SIA.MealsSelection.public.mealDictionary;

			$("[data-accordion-trigger]").find("[data-remove-trigger]").off()
				.on("click", function (evt) {
					evt.preventDefault();
					evt.stopPropagation();
					
					var self = $(this);
					var wrapper = self.closest(".accordion-box");
					var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
					var removeWrap = wrapper.find(".remove-prompt").first();
					
					if(!removeWrap.hasClass('past-meal-prompt')) removeWrap.removeClass("hidden");
					self.addClass("hidden");

					var segmentMealsValues = Object.values(segmentMeals.selectedMeal);
					segmentMealsValues = _.findWhere(segmentMealsValues, {mealCategoryCode: "SPML", isEating: true});
					
					removeWrap.find(".menu-label").text(segmentMealsValues ? segmentMealsValues.mealMenuName : "Inflight Meal");

					removeWrap.find(".btn-cancel")
						.off().on("click", function(evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							removeWrap.addClass("hidden");
							self.removeClass("hidden");
						});

					removeWrap.find(".btn-proceed")
						.off().on("click", function (evt) {
							evt.preventDefault();
							evt.stopImmediatePropagation();
							wrapper.find(".fm-inf-menu.selected").each(function () {
								var self = $(this);
								var mealButton = self.find(".fm-select-btn");
								if (mealButton.data("meal-category") == "SPML") {
									resetToInflight(mealButton);
								} else {
									unSelectMeal(mealButton);
								}
								undoMeal = mealButton;
							});

							segmentMealsValues = Object.values(segmentMeals.selectedMeal);
							segmentMealsValues = _.findWhere(segmentMealsValues, {mealCategoryCode: "SPML", isEating: true});
							if (segmentMealsValues) {
								var mealBtn = wrapper.find(".fm-select-btn[data-meal-mealcode='"+segmentMealsValues.mealCode+"']");
								selectMeal(mealBtn);
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html("");
								wrapper.find(".fm-accordion-label .meal-sub-title-black").append(
									$(_.template(mealTitleTpl, {
										mealName: segmentMealsValues.mealMenuName,
										mealType: mealDictionary.SPML,
										hideRemove: !(p.isEco || p.isPey || p.isFirst || p.isBiz)
									}))
								);
								SIA.MealsSelection.updateData(mealBtn);
								removeButtonListener();
							} else {
								wrapper.find(".fm-accordion-label .meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
								SIA.MealsSelection.removeInputData(wrapper);
							}
							console.log('calling reset');
							resetPastMeals(wrapper);

							removeWrap.addClass("hidden");
						});
				});
		}, 500);
	};

	var resetPastMeals = function(wrap, btn) {
		if(typeof btn !== 'undefined') {
			wrap.parent().find('.past-meals-wrap [data-meal-category]').each(function(){
				var t = $(this);
				if(t.attr('data-meal-category') === 'SPML') {
					t.val('Select').removeClass('selected');
					t.parent().parent().removeClass('selected');
				}
			});
		}else {
			wrap.parent().find('.past-meals-wrap [data-meal-category]').each(function(){
				var t = $(this);
				t.val('Select').removeClass('selected');
				t.parent().parent().removeClass('selected');
			});
		}
	};

	var selectSamePastMeal = function(btn){
		btn.closest('.accordion-wrapper-content').find('.past-meal[data-meal-category="'+btn.attr('data-meal-category')+'"][data-meal-mealcode="'+btn.attr('data-meal-mealcode')+'"]').each(function(){
			var t = $(this);
			if(t.closest('.past-meals-wrap').find('.past-meal.selected[data-meal-category="BTC"]').length < 1){
				t.val('Remove').addClass('selected').parent().parent().addClass('selected');
			}
		})
	};

	var oPublic = {
		init: init,
		selectSegmentMeal: selectSegmentMeal,
		getSelectedSpml: getSelectedSpml,
		p: p,
		resetToInflight: resetToInflight,
		addInputListener: addInputListener
	};

	return oPublic;
};

SIA.InfantMeal = function (jsonData,
	paxData,
	accContent,
	segmentId,
	sectorId,
	serviceId,
	paxId,
	mealServiceCode) {
	var p = {};

	p.menuItem = '<div class="fm-inf-menu <%= selectedClass %>">\
            <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
            <h4 class="fm-inf-menu-title" aria-hidden="true" id="ariaLbl<%= id %>"><%= menuName %></h4>\
            <p class="fm-inf-menu-desc" aria-hidden="true" id="ariaDesc<%= id %>"><%= menuDescription %></p>\
            <div class="fm-footnote-txt">\
                <input type="button" name="" value="<%= inputVal %>" data-mealcattype="speacial-meal-list" data-infantMealSelect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>" aria-labelledby="ariaLbl<%= id %>" aria-describedby="ariaDesc<%= id %>">\
            </div>\
            <div data-prompt="with-selection" class="confirmation-prompt hidden">\
              <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change your other infant meals selection from <%= origin %> to <%= dest %> to <span><%= menuName %></span> as well. </p>\
              <div class="button-group-1">\
                <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
                <a href="#" class="btn-2 btn-cancel">Cancel</a>\
              </div>\
            </div>\
            <div data-prompt="initial" class="confirmation-prompt hidden">\
              <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change all your meal selections from <%= origin %> to <%= dest %> to <span><%= menuName %></span>.</p>\
              <div class="button-group-1">\
                <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
                <a href="#" class="btn-2 btn-cancel">Cancel</a>\
              </div>\
            </div>\
        </div>';

	// Array to store all meals selection
	p.mealsArr = [];
	p.curPrompt = null;
	p.currentMeal = null;

	var init = function () {
		p.data = paxData;
		p.jsonData = jsonData;

		p.curPaxId = paxId;
		p.segmentId = segmentId;
		p.sectorId = sectorId;
		p.serviceId = serviceId;
		p.mealCatCode = jsonData[0].mealCategoryCode;
		p.mealCatName = jsonData[0].mealMenuCategoryName;
		p.totalTabs = jsonData[0].mealSubcategory.length;
		p.totalTabs = jsonData[0].mealSubcategory.length;
		p.accContent = accContent;
		p.infantlMealWrap = accContent.find('.infant-meals');
		p.isEco = SIA.MealsSelection.public.isEco;
		p.isPey = SIA.MealsSelection.public.isPey;

		paintInfantMenu();

		var firstInput = $(p.infantlMealWrap.find('.fm-select-btn')[0]);
		firstInput.attr('data-first-focus', 'true');

		p.infmlChkBox = p.accContent.find('[data-checktype="not-eating"]');
		setTimeout(function () {
			p.infmlChkBox.on({
				'change.infm': function () {
					var t = $(this);
					if (t.prop('checked') && p.currentMeal !== null) {
						unSelectMeal(p.currentMeal);

						var accordionContent = t.closest('.accordion__content');
						var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
						accordionTabMealTitle.text('I don’t need an infant meal');
					}

					SIA.MealsSelection.updateData(t);
				}
			});
		}, 600);

	};

	var paintInfantMenu = function () {
		var curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

		for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
			var mealCatArr = p.jsonData[0].mealSubcategory[i];
			// Class for the content tab wrapper
			var hidden = '';
			if (i > 0) hidden = ' class="hidden"';

			if (typeof mealCatArr.Meals != 'undefined') {
				for (var j = 0; j < mealCatArr.Meals.length; j++) {
					var menu = mealCatArr.Meals[j];

					var selectClass = '';
					var viewOnlyClass = ' hidden';
					var mealCatCode = p.jsonData[0].mealCategoryCode;

					// check for any previous selected item
					if (curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == menu.mealCode) {
						selectedClass = 'selected';
						inputVal = 'Remove';
						isSelected = '';

						p.lastTick = p.mealsArr.length;
					} else {
						selectedClass = '';
						inputVal = 'Select';
						isSelected = 'hidden';
					}

					if (typeof menu.isViewOnly != 'undefined' && menu.isViewOnly) {
						selectClass = ' hidden';
						viewOnlyClass = '';
					}

					var origin = SIA.MealsSelection.public.json.flightSegment[p.segmentId].departureAirportCode;
					var dest = SIA.MealsSelection.public.json.flightSegment[p.segmentId].arrivalAirportCode;

					var menuItem = $(_.template(p.menuItem, {
						'id': p.mealsArr.length,
						'menuId': i + '-' + j,
						'menuName': menu.mealMenuName,
						'menuDescription': menu.mealMenuDescription,
						'segmentId': p.segmentId,
						'sectorId': p.sectorId,
						'paxId': p.curPaxId,
						'mealCategoryCode': p.mealCatCode,
						'mealServiceCode': mealServiceCode,
						'selectClass': selectClass,
						'mealServiceId': serviceId,
						'mealCode': menu.mealCode,
						'mealMenuName': menu.mealMenuName,
						'origin': origin,
						'dest': dest
					}));

					p.infantlMealWrap.append(menuItem);
					p.mealsArr.push(menuItem);

					menuItem.find('input.fm-select-btn').each(function () {
						addInputListener($(this));
					});
				}
			}
		}
	};

	var addInputListener = function (input) {
		input.parent().parent().off().on({
			'click': function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var accordionContent = t.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");
				var parentElList = parentEl.parent();

				var selectMealCode = t.data('meal-mealcode');

				var curAccordWrapper = SIA.MealsSelection.public.mealSelectionForm.find('.flightmeal-control')[p.segmentId];
				var similarInput = $(curAccordWrapper).find('input[data-meal-category="INF"].selected');

				// check if current meal card is selected again, then remove it
				var badgeEl = $(parentEl).children();

				if (t.val() === 'Remove') {
					unSelectMeal(t);

					t.attr('value', 'Select');

					// change accordion tab previously selected meal to its default text('inflight menu')
					var inflLbl = 'Inflight Menu';
					if (p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					accordionTabMealTitle.html(inflLbl);

					SIA.MealsSelection.updateData(t);

					p.currentMeal = null;
				} else {
					var hasOtherINFM = getOtherINFM(t);
					if (hasOtherINFM.length > 0) {
						var prompt = t.parent().parent().find('[data-prompt="with-selection"]');
					} else {
						var prompt = t.parent().parent().find('[data-prompt="initial"]');
					}

					// Remove any selected prompts
					if (p.curPrompt !== null) {
						p.curPrompt.parent().find('.fm-select-btn').val('Select');
						p.curPrompt.addClass('hidden');
						p.curPrompt.parent().removeClass('int-prompt');
						p.curPrompt.find('.btn-cancel').off();
						p.curPrompt.find('.btn-proceed').off();
					}

					t.val('Selected');

					prompt.removeClass('hidden');
					prompt.parent().addClass('int-prompt');

					prompt.find('.btn-cancel').off().on({
						'click': function (e) {
							e.preventDefault();
							e.stopImmediatePropagation();

							t.val('Select');
							prompt.addClass('hidden');
							prompt.parent().removeClass('int-prompt');

							p.curPrompt = null;
						}
					});

					prompt.find('.btn-proceed').off().on({
						'click': function (e) {
							e.preventDefault();
							e.stopImmediatePropagation();

							// remove listener to prevent double calls on otherSM
							$(this).off();

							// search and remove any previous selected meal
							SIA.MealsSelection.resetMeals(t, ['infm']);

							selectMeal(t);
							p.currentMeal = t;

							// apply selection to other special meals in same leg
							selectSameMeal(t, hasOtherINFM);

							// assign data
							accordionTabMealTitle.text(parentEl.find('.fm-inf-menu-title').text() + ' (Infant Meal)');
							SIA.MealsSelection.updateData(t);

							unselectChkBox();
						}
					});

					p.curPrompt = prompt;
				}
			}
		});
	};

	var getOtherINFM = function (meal) {
		var otherSelectedSM = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];

		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'INF' && v.mealCode !== meal.attr('data-meal-mealcode')) {
				otherSelectedSM.push({
					'k': k,
					'val': v
				});
			}
		});

		return otherSelectedSM;
	};

	var selectSameMeal = function (meal, sameMeals) {
		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.infm.length; i++) {
			var infml = SIA.MealsSelection.public.infm[i];

			infml.p.accContent.find('.fm-select-btn').each(function () {
				var t = $(this);
				// Unselect other selected special meals in same sector
				if (t.attr('data-segment') === meal.attr('data-segment')) {
					unSelectMeal(t);
				}

				// Find the selected special meal in current segment and select it
				if (t.attr('data-segment') === meal.attr('data-segment') &&
					t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')) {
					selectMeal(t);

					var parentEl = t.parent().parent();
					var accordionContent = t.closest('.accordion__content');
					var accordionTabMealTitle = accordionContent.closest(".accordion-box").find("[data-accordion-trigger]").find(".meal-sub-title-black");

					// SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
					accordionTabMealTitle.text(parentEl.find('.fm-inf-menu-title').text() + ' (Infant Meal)');
					SIA.MealsSelection.updateData(t);
				}
			});
		}
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	var unSelectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
	};

	var unselectChkBox = function () {
		p.infmlChkBox.prop("checked", false);
		p.infmlChkBox.parent().find('.not-eating-msg').addClass('hidden');
	};

	var destroy = function () {
		accContent.remove();
	};

	var oPublic = {
		init: init,
		p: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.MoreTab = function (mainTabs, moreTabs) {
	var p = {};

	p.mainTabs = mainTabs;
	p.moreTabs = moreTabs;

	var init = function () {
		p.moreTabs.on('change', function (e) {
			e.preventDefault();

			var t = $(this);
			var tabIndex = t.val();
			var tabContentEl = p.moreTabs.parent().next();

			// hide all tabs
			$.each(tabContentEl.find('[data-tab-content]'), function (i, value) {
				$(value).addClass('hidden');
			});

			// show the current selected tab index content
			tabContentEl.find('[data-tab-content]').eq(tabIndex).removeClass('hidden');

			// swap the text from the other tab to the main tab
			var activeMainTab = p.moreTabs.parent().prev().find('.active .passenger-info__text');
			var moreTabs = p.moreTabs.find('[value=' + tabIndex + ']');
			var moreTabsTxt = moreTabs.html();
			var activeMainTxt = activeMainTab.html();

			activeMainTab.html(moreTabsTxt);
			moreTabs.html(activeMainTxt);

			// swap index number from the other tab to the main tab
			var mainTabActive = activeMainTab.closest('.active');
			var mainTabId = mainTabActive.attr('data-tab-menu');
			moreTabs.attr('value', mainTabId);
			mainTabActive.attr('data-tab-menu', tabIndex);

			// update custom scroll list
			var customScroll = p.moreTabs.next().attr('id');
			var csId = customScroll.split('-');
			var csEl = $('.custom-scroll').eq(parseInt(csId[1]));
			var csElAtt = '[data-value="' + tabIndex + '"]';

			csEl.find(csElAtt).html(activeMainTxt);
			csEl.find(csElAtt).attr('data-value', mainTabId);
		});
	};

	var oPublic = {
		init: init,
		p: p
	};

	return oPublic;
};

SIA.VerticalTab = function (tabControls, tabContent, tabSelect, totalTabs) {
	var p = {};
	p.tabControls = tabControls;
	p.tabContent = tabContent;
	p.tabSelect = tabSelect;
	p.totalTabs = totalTabs;
	p.curTabId = 0;

	var init = function () {
		p.tabControls.find('.tab-item').on({
			'click': function (e) {
				e.preventDefault();
				var t = $(this);

				if (!t.hasClass('active')) {
					var id = parseInt(t.data('tab-menu'));
					showContent(id);

					p.tabControls.find('.tab-item').removeClass('active');
					t.addClass('active');

					if (id < 3 && p.totalTabs > 4) {
						p.tabSelect.find('options').removeAttr('selected');
					}
				}

			}
		});

		if (p.totalTabs < 5) {
			p.tabSelect.parent().addClass('hidden');
		}

		arrangeTabs();
		resizeTabs();
	};

	var arrangeTabs = function () {
		if ($(window).width() <= 1023 && totalTabs > 4) {
			tabControls.find('.tab-item:gt(2)').addClass('hidden');
		} else {
			tabControls.find('.tab-item').removeClass('hidden');
		}
	};

	var resizeTabs = function () {
		$(window).resize(function () {
			arrangeTabs();
		});
	};

	var showContent = function (id) {
		p.tabContent.children('[data-tab-content]').addClass('hidden');
		var curTabContent = p.tabContent.children('[data-tab-content="' + id + '"]');
		curTabContent.removeClass('hidden');

		// var firstItem = curTabContent.find('.fm-select-btn')[0];
		// firstItem.focus();

		p.curTabId = id;
	};

	var oPublic = {
		init: init,
		p: p
	};

	return oPublic;
};

SIA.BSPUtil = function() {

	var p = SIA.MealsSelection.public;

	var addBSPTpl = function(ids, pax) {
		var segment = ids.segmentId + "" + ids.sectorId;
		var listTpl = '<ul class="bsp-flights-cost__details" data-bsp-mealid="<%- paxId %>">\
			<span><%-paxName.toUpperCase()%></span>\
			<li data-bsp-currentprice="<%- currentPrice ? currentPrice : 0 %>" data-bsp-segment="<%- segment %>">\
				<span class="bsp-addons__flightinfo"><%-origin%> - <%-destination%></span>\
				<br>\
				<span style="float:left; min-width: 50%" class="bsp-addons__details"> Premium Selection</span>\
				<span class="price" data-bsp-mealprice="<%-price.toFixed(2)%>" data-bsp-segment-paxid="<%- paxId %>"><%-price.toFixed(2)%></span>\
			</li>\
		</ul>'
		var segmentTpl = '<li data-bsp-currentprice="<%- currentPrice ? currentPrice : 0 %>" data-bsp-segment="<%- segment %>">\
			<span class="bsp-addons__flightinfo"><%-origin%> - <%-destination%></span>\
			<br>\
			<span style="float:left; min-width: 50%" class="bsp-addons__details"> Premium Selection</span>\
			<span class="price" data-bsp-mealprice="<%-price.toFixed(2)%>" data-bsp-segment-paxid="<%- paxId %>"><%-price.toFixed(2)%></span>\
		</li>';
		
		var listEl = $(_.template(listTpl, {
			paxId: pax.paxId,
			segment: segment,
			origin: pax.origin,
			destination: pax.destination,
			price: parseFloat(pax.price),
			paxName: pax.paxName,
			currentPrice: parseFloat(pax.currentPaxPrice)
		}));

		var mealEl = $("[data-bsp-mealid='" + pax.paxId + "']");
		var segmentEl = mealEl.find("[data-bsp-segment='" + segment + "']");
		
		if (!mealEl.length) {
			if (!$("[data-bsp-mealid]").length) {
				listEl.insertAfter($(".append-meal-list"));
			} else {
				listEl.insertAfter($("[data-bsp-mealid]").last());
			}
			
		}
		if (segmentEl.length) {
			segmentEl.find(".bsp-addons__flightinfo").text(pax.origin + " - " + pax.destination);
			segmentEl.find(".price").data("bsp-mealprice", pax.price);
			segmentEl.find(".price").text(pax.price);
		} else {
			var newSegment = $(_.template(segmentTpl, {
				segment: segment,
				origin: pax.origin,
				destination: pax.destination,
				price: parseFloat(pax.price),
				paxId: pax.paxId,
				currentPrice: parseFloat(pax.currentPaxPrice)
			}));
			mealEl.append(newSegment);
		}
		
		calculatePaid();
		calculateTotal();
	};

	var calculatePaid = function() {
		var total = 0;
		$("[data-bsp-currentprice]").each(function() {
			var $this = $(this);
			total += parseFloat($this.data("bsp-currentprice"));
		});
		setSubTotal(total);
	};

	var calculateTotal = function() {
		var total = 0;
		var currency = "SGD"
		$("[data-bsp-mealprice]").each(function() {
			var $this = $(this);
			total += parseFloat($this.data("bsp-mealprice"));
		});

		if (!total) {
			var bookingPanel = $("[data-booking-summary-panel]");
			bookingPanel.html("");
			bookingPanel.html($('<div class="bsp-animate"></div>'));
			bookingPanel.addClass('hidden');
			$('.sia-breadcrumb').addClass('hidden');
			p.bspIsDisplayed = false;
			changeCTAButton("SAVE AND EXIT");
			return;
		}
		total = total.toFixed(2);
		if ($(".sub-total-price").length) {
			$(".sub-total-price").find(".total-price").text(total);
			var current = parseFloat(total);
			var paid = parseFloat($(".previous-price").find(".total-price").text());
			var newTotal = current - paid;
			total = newTotal.toFixed(2);
		}
		$(".grand-total__price").text(currency + " " + total);
		$(".total-cost").find(".unit").text(currency + " " + total.split(".")[0]);
		$(".total-cost").find(".unit").append($('<span class="unit-small">.'+(total.split(".")[1] || "00")+'</span>'))
	};

	var removeBSP = function(ids, paxId) {
		var segment = ids.segmentId + "" + ids.sectorId;
		var bspMealWrapper = $("[data-bsp-mealid='"+paxId+"']");
		bspMealWrapper.find("[data-bsp-segment='"+segment+"']").remove();
		if (!bspMealWrapper.find("[data-bsp-segment]").length) {
			bspMealWrapper.remove();
		}
		calculatePaid();
		calculateTotal();
	};

	var displayBSP = function(cb) {
		SIA.bookingSummnaryBubblePrice(cb);
		$(".sia-breadcrumb").removeClass("hidden");
	};

	var changeCTAButton = function(inputText) {
		$(".btn-cta-submit").val(inputText.toUpperCase());
	};

	var paintSubTotal = function(currentPax) {
		var totalPrice = parseFloat(currentPax.mealPrice);
		var tpl = '<ul class="bsp-flights-cost__details bah-subtotal">\
			<li class="sub-total sub-total-price">\
				<span>Sub-total</span>\
				<span class="total-price">0.00</span>\
			</li>\
			<li class="sub-total previous-price">\
				<span>Previously paid</span>\
				<span class="total-price">'+totalPrice.toFixed(2)+'</span>\
			</li>\
		</ul>';

		$(".bsp-grand-total").prepend($(tpl));
	};

	var setSubTotal = function(total) {
		$(".bah-subtotal").find(".previous-price").find(".total-price").text(total.toFixed(2));
	};

	return {
		addBSPTpl: addBSPTpl,
		removeBSP: removeBSP,
		displayBSP: displayBSP,
		paintSubTotal: paintSubTotal
	};
}();

// check if underscore is loaded
var waitForUnderscore = function () {
	setTimeout(function () {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.MealsSelection.init();
		}
	}, 100);
};

$(function () {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.MealsSelection.init();
});
