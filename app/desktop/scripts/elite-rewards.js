/**
 * @name SIA
 * @description Define global donatemiles functions
 * @version 1.0
 */

SIA.manageProgressBar = function () {
  var progressBar = $('[data-progress-bar]');
  var currentPPS = parseInt(progressBar.data('kf-points'));
  var totalPPS = parseInt(progressBar.data('total-points'));

  var $milestones = progressBar.find('.js-milestones');
  var $eliteAnimate = progressBar.find('.js-elite-animate');
  var $eliteItem = $milestones.find('.js-elite-item');
  var $tooltip = progressBar.find('.tooltip-progress');

  var tab = $('.js-tab');
  var tabItem = tab.find('.tab-item');
  var tabContent = tab.find('.tab-content');

  var isIE = function () {
    var userAgent = navigator.userAgent;
    return userAgent.indexOf('MSIE ') > -1 || userAgent.indexOf('Trident/') > -1 || userAgent.indexOf('Edge/') > -1;
  }

  var offsetMe = function (ele, val) {
    var animationInterval = setInterval(function () {
      if (!val) {
        clearInterval(animationInterval);
        return;
      }

      ele.style.strokeDashoffset = val;
      val--;
    }, 20);
  }

  var resetAnimation = function () {
    $eliteItem.removeClass('active');
    $eliteAnimate.css('width', 0);
    $tooltip.css('display', 'none');
  }

  var animationProgressBar = function () {
    $eliteAnimate.addClass('elite-progress-bar');
    $tooltip.find('.current-number').text(currentPPS);

    var fixAnimationIE = function (idx) {
      var _selfCircle = document.querySelectorAll('.checkmark__circle'),
        _selfCheck = document.querySelectorAll('.checkmark__check');
      offsetMe(_selfCircle[idx], 166);
      offsetMe(_selfCheck[idx], 48);
    }

    var startAnimation = function () {
      var arr = [];
      var countAnimateEl;

      $eliteAnimate.each(function () {
        arr.push(parseInt($(this).data('point')));
      });

      for (var i = 0; i < arr.length; i++) {
        if (currentPPS <= arr[i]) {
          countAnimateEl = i + 1;

          break;
        }
      }

      var prePoint = 0;
      var startWidth = 0;
      var selfPoint;
      var point;
      var width;
      for (var i = 0; i < countAnimateEl; i++) {
        var self = $($eliteAnimate[i]);
        var delayTime = i * 700;

        selfPoint = parseInt(self.data('point'));

        if (i >= 1) {
          prePoint = parseInt($($eliteAnimate[i - 1]).data('point'));
        }

        point = selfPoint - prePoint;
        if (currentPPS > selfPoint) {
          width = 100;
          self.delay(delayTime).animate({
            width: startWidth + '%',
          }).animate({
            width: width + '%',
          }, {
              duration: 500,
              easing: 'linear',
              complete: function () {
                var index = parseInt($(this).data('index'));
                $($eliteItem[index]).addClass('active');

                if (isIE()) {
                  fixAnimationIE(index);
                }
              }
            });
        } else {
          width = (currentPPS / selfPoint) * 100;
          self.delay(delayTime).animate({
            width: startWidth + '%',
          }).animate({
            width: width + '%',
          }, {
            duration: 500,
            easing: 'linear',
            step: function (now, fx) {
              if (now === 100) {
                $($eliteItem[countAnimateEl - 1]).addClass('active');

                if (isIE()) {
                  fixAnimationIE(countAnimateEl - 1);
                }
              }

              if (now === fx.end) {
                $($tooltip[countAnimateEl - 1]).css('left', now + '%');
              }
            },
            complete: function () {
              setTimeout(function () {
                $($tooltip[countAnimateEl - 1]).fadeIn('fast').css('display', 'table');
              }, 500);
            },
          });
        }
      }
    }

    startAnimation();
  };

  var barWidth = (currentPPS / totalPPS) * 100;

  if (barWidth < 13) {
    $('.milestones-animate-wrapper').addClass('first-milestones-animate-wrapper');

    if (barWidth >=3) {
      $('.first-milestones-animate-wrapper').find('.tooltip-progress .tooltip__arrow').css('left', ( barWidth * 4) + '%');
    } else {
      $('.first-milestones-animate-wrapper').find('.tooltip-progress .tooltip__arrow').css('left', ( barWidth * 6) + '%');
    }
  }
  if (barWidth > 91) {
    $('.milestones-animate-wrapper').addClass('last-milestones-animate-wrapper');
    var currentWidth = barWidth - 91;

    if (barWidth >= 99) {
      $('.last-milestones-animate-wrapper').find('.tooltip-progress .tooltip__arrow').css('left', (50 + currentWidth * 4.9) + '%');
    } else if (barWidth >= 98) {
      $('.last-milestones-animate-wrapper').find('.tooltip-progress .tooltip__arrow').css('left', (50 + currentWidth * 5.6) + '%');
    } else if (barWidth < 98) {
      $('.last-milestones-animate-wrapper').find('.tooltip-progress .tooltip__arrow').css('left', (50 + currentWidth * 6) + '%');
    }
  }

  tabItem.each(function (idx) {
    $(this).off('click.initAnimation').on('click.initAnimation', function (e) {

      if ($(this).is('[data-tabprogressbar]') && !$(tabContent[idx]).is('.active')) {
        setTimeout(function () {
          animationProgressBar();
        }, 200);
      } else {
        $eliteAnimate.stop(true, true);
        resetAnimation();
      }

      $(this).attr('[data-tabprogressbar]');
      $(this).addClass('active');
      $(this).siblings().removeClass('active');

      $(tabContent[idx]).addClass('active');
      $(tabContent[idx]).siblings().removeClass('active');
    });
  });

  return {
    animationProgressBar: animationProgressBar,
    resetAnimation: resetAnimation,
    offsetAnimation: offsetMe
  }
};

$(document).ready(function () {
  var manageProgressBar;
  var $vouchers = $('.js-elite-progress-bar');

  function initialize() {
    return SIA.AjaxCaller.template('elite-rewards-progress-bar.tpl').then(function (tplContent) {
      var template = window._.template(tplContent, {
        data: {},
      });

      $vouchers.html(template);

      setTimeout(function () {
        manageProgressBar = SIA.manageProgressBar();
        manageProgressBar.animationProgressBar();
      }, 100);
    }).fail(function (err) {
      console.log(err);
    });
  }

  initialize();
});
