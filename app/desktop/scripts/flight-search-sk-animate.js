/**
 * @name SIA
 * @description Define global flight economy functions
 * @version 1.0
 */

SIA.flightSearchNewAnimate = function () {
    var global = SIA.global,
        config = global.config,
        body = $('body'),
        container = $('#container'),
        isChooseDate = null,
        priceDateOutbound = 0,
        clickedBtn,
        miniFareData;

    var slideshowpremiumeconomy = function () {
        if ($('body').hasClass('fs-economy')) {
            var totalDesktopSlide = 1,
                totalLandscapeSlide = 1;
            $('[data-slideshow-premium-economy]').each(function () {
                var slider = $(this);
                slider.find('img').each(function () {
                    var self = $(this),
                        newImg = new Image();
                    newImg.onload = function () {
                        slider.css('visibility', 'visible');
                        slider.find('.slides').slick({
                            siaCustomisations: true,
                            dots: false,
                            speed: 300,
                            draggable: true,
                            slidesToShow: totalDesktopSlide,
                            slidesToScroll: totalLandscapeSlide,
                            accessibility: false,
                            arrows: true,
                        });
                    };
                    newImg.src = self.attr('src');
                });
            });
        }
    };
    var initSlider = function (sliderEl, btnNext, btnPrev) {
        var wrapperSlides = $(sliderEl).find('.slides');

        if (wrapperSlides.is('.slick-initialized')) {
            wrapperSlides.slick('unslick');
        }

        wrapperSlides.slick({
            accessibility: true,
            dots: false,
            draggable: false,
            infinite: true,
            speed: 300,
            slidesToShow: 7,
            slidesToScroll: 7,
            prevArrow: btnPrev,
            nextArrow: btnNext,
        })

        setTimeout(function () {
            $('.slick-slide').attr("tabindex", -1);
            $('.slick-active,.slick-arrow').attr("tabindex", 0);
        }, 500);

        selectItemSlider(wrapperSlides);

        if (!sliderEl.data('slider-outbound')) {
            sliderEl.find('.slide-item').each(function () {
                var selfPrice = parseFloat($(this).find('.large-price').text()),
                    priceAfterSelect = selfPrice - priceDateOutbound;

                $(this).find('.large-price').text('+ ' + (priceAfterSelect > 0 ? priceAfterSelect : 0));
            })
        }
    }

    var selectItemSlider = function (wrapperSlides) {
        var slideItem = wrapperSlides.find('.slick-slide');
        slideItem.each(function () {
            if (isChooseDate) {
                $(this).data('date') === isChooseDate && $(this).addClass('selected');
            }
            $(this).off('click.selectSlideItem').on('click.selectSlideItem', function (e, isFirst) {
                e.preventDefault();
                if ($('body').hasClass('sk-ut-workflow') && !isFirst) {
                    return false;
                }
                var isOutBound = $(this).closest('[data-fs-slider]').data('slider-outbound');
                isOutBound && $(this).closest('[data-fs-slider]').attr('data-selected-date', $(this).data('date'));
                slideItem.removeClass('selected');
                $(this).addClass('selected');
                isChooseDate = $(this).data('date');

                if (isOutBound) {
                    priceDateOutbound = parseFloat($(this).find('.large-price').text());
                }
            });
        })
    }

    var renderPopupBenefit = function (popupEl, callback) {
        $.get(global.config.url.fsEconomyBenefitTemplate, function (data) {
            var content = popupEl.find('.popup__content');
            var template = window._.template(data, {
                data: data
            });
            $(template).prependTo(content.empty());
            // reinit js
            SIA.initTabMenu();
            SIA.multiTabsWithLongText()

            if (callback) {
                callback();
            }
        });
    }

    var renderPopupPremiumBenefit = function (popupEl, callback) {
        $.get(global.config.url.fsEconomyPremiumBenefitTemplate, function (data) {
            var content = popupEl.find('.popup__content');
            var template = window._.template(data, {
                data: data
            });
            $(template).prependTo(content.empty());
            // reinit js
            SIA.initTabMenu();
            SIA.multiTabsWithLongText()

            if (callback) {
                callback();
            }
        });
    }

    var initPopup = function (data) {
        var triggerPopup = $('[data-trigger-popup]');

        triggerPopup.each(function () {
            var self = $(this);

            if (typeof self.data('trigger-popup') === 'boolean') {
                return;
            }

            var popup = $(self.data('trigger-popup'));

            if (!popup.data('Popup')) {
                popup.Popup({
                    overlayBGTemplate: config.template.overlay,
                    modalShowClass: '',
                    triggerCloseModal: '.popup__close, [data-close], .cancel',

                    afterHide: function () {
                        container.css('padding-right', '');
                        body.css('overflow', '');
                        var ua = navigator.userAgent.toLowerCase();
                        if (ua.indexOf('safari') != -1) {
                            if (ua.indexOf('chrome') > -1) {
                            }
                            else {
                                body.attr('style', function (i, s) {
                                    return s.replace('overflow:hidden !important;', '');
                                });
                            }
                        }
                    }
                });
            }
            self.off('click.showPopup').on('click.showPopup', function (e) {
                e.preventDefault();
                var jsonURL = self.data('flight-json-url');
                if (!self.hasClass('disabled')) {
                    // render popup benefit detail
                    if (self.data('trigger-popup') === '.popup-view-benefit--krisflyer') {
                        renderPopupBenefit(popup, function () {
                            popup.Popup('show');
                            $(window).trigger('resize');
                        });
                    } else if (self.data('trigger-popup') === '.popup-view-premium-benefit--krisflyer') {
                        renderPopupPremiumBenefit(popup, function () {
                            popup.Popup('show');
                            $(window).trigger('resize');
                        });
                    } else if (self.data('trigger-popup') === '.popup-view-partner-airlines') {
                        popup.Popup('show');
                    }
                    else if (self.data('trigger-popup') === '.popup-view-our-partner-airlines') {
                        popup.Popup('show');
                    }
                }
            });

        });
    };

    var showHideFilters = function () {
        var fsFilters = $('.flight-search-filter-economy');

        fsFilters.each(function () {
            var linkShow = $(this).find('.link-show'),
                linkHide = $(this).find('.link-hide'),
                content = $(this).find('.content');

            linkShow.off('click').on('click', function (e) {
                e.preventDefault();
                if (!$(this).parent().is('.active')) {
                    $(this).parent().addClass('active')
                    linkShow.hide();
                    content.fadeIn();
                }
            });
            linkHide.off('click', 'a').on('click', 'a', function (e) {
                e.preventDefault();
                content.hide();
                linkShow.fadeIn();
                $(this).closest('.flight-search-filter-economy').removeClass('active');
            });
        })
    };

    var showMoreLessDetails = function () {
        var btnMore = $('[data-more-details-table]'),
            btnLess = $('[data-less-details-table]'),
            triggerAnimation = $('[data-trigger-animation]'),
            wrapFlights = $('[data-wrap-flight]'),
            fareBlocks = $('[data-hidden-recommended]');

        // set data-height for wrap flight
        wrapFlights.each(function () {
            var flightLegItem = $(this).find('.flight-result-leg'),
                wrapFlightHeight = 0;

            flightLegItem.each(function () {
                wrapFlightHeight += $(this).outerHeight();
            });

            $(this).attr('data-wrap-flight-height', wrapFlightHeight);
        });

        btnMore.each(function () {
            $(this).off('click.showMore').on('click.showMore', function (e) {
                e.preventDefault();
                var pad = window.innerWidth < 988 ? 40 : 0;
                var flightItem = $(this).closest('[data-flight-item]'),
                    controlFlight = flightItem.find('.control-flight-station'),
                    wrapFlight = flightItem.find('[data-wrap-flight]'),
                    controlFlightHeight = wrapFlight.siblings('.control-flight-station').outerHeight() || 84,
                    btnLess = wrapFlight.find('[data-less-details-table]');
                    wrapFlightHeight = wrapFlight.data('wrap-flight-height');

                if (wrapFlight) {
                    flightItem.addClass('active');
                    wrapFlight.css({
                        'height': wrapFlightHeight - controlFlightHeight + pad + 'px',
                        '-webkit-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                        '-moz-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                        '-ms-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                        '-o-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                        'transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)'
                    })
                    btnLess.attr('tabindex', 0);
                }
            });
        });

        btnLess.each(function () {
            $(this).off('click.showLess').on('click.showLess', function (e) {
                e.preventDefault();
                var flightItem = $(this).closest('[data-flight-item]'),
                    controlFlight = flightItem.find('.control-flight-station'),
                    wrapFlight = flightItem.find('[data-wrap-flight]');

                if (wrapFlight) {
                    flightItem.removeClass('active');
                    wrapFlight.css({
                        'height': '10px',
                        '-webkit-transform': 'translate3d(0)',
                        '-moz-transform': 'translate3d(0)',
                        '-ms-transform': 'translate3d(0)',
                        '-o-transform': 'translate3d(0)',
                        'transform': 'translate3d(0)'
                    })
                    $(this).attr('tabindex', -1);
                }
            })
        });
    }

    var selectFlightAnimation = function () {
        var triggerAnimation = $('[data-trigger-animation]');
        triggerAnimation.removeAttr('data-trigger-popup');

        triggerAnimation.each(function () {
            $(this).off('click.toggleAnimation, keydown.toggleAnimation').on('click.toggleAnimation, keydown.toggleAnimation', function (e, isOneAnimation) {
                var parentRecommended = $(this).parent(),
                    flightItem = parentRecommended.find('[data-flight-item]'),
                    wrapFlight = flightItem.find('[data-wrap-flight]'),
                    btnMore = flightItem.find('[data-more-details-table]'),
                    flightStation = flightItem.find('.flight-station'),
                    hiddenRecommended = parentRecommended.siblings('[data-hidden-recommended]'),
                    hiddenRecommended1 = parentRecommended.siblings('[data-hidden-recommended-1]'),
                    isOpen = $(this).is('.active'),
                    hiddenRecommendedHeight = hiddenRecommended.data('fare-block-height'),
                    colIdx = $(this).data('trigger-animation'),
                    selfTable = $('[data-col-index="' + colIdx + '"]'),
                    code = e.keyCode || e.which;

                if ($('body').hasClass('sk-ut-workflow') && $(this).hasClass('economy-flight--pey')) {
                    return false;
                }

                triggerAnimation.not($(this)).each(function () {
                    if ($(this).is('.active')) {
                        $(this).trigger('click', true);
                    }
                })

                if (isOneAnimation || code === 1 || code === 13) {
                    // reset animation
                    if (wrapFlight) {
                        flightItem.removeClass('active');
                        wrapFlight.css({
                            'height': '10px',
                            '-webkit-transform': 'translate3d(0)',
                            '-moz-transform': 'translate3d(0)',
                            '-ms-transform': 'translate3d(0)',
                            '-o-transform': 'translate3d(0)',
                            'transform': 'translate3d(0)'
                        })
                    }
                    $('[data-col-index]').find('[data-tooltip], a, input').attr('tabindex', -1);
                    if (!$(this).next().is('.not-available')) {
                        $(this).next().attr('tabindex', 0);
                    }
                    parentRecommended.find('[data-trigger-animation]').removeClass('active');
                    hiddenRecommended.removeClass('active economy-flight--green , business-flight--blue');
                    hiddenRecommended1.removeClass('active economy-flight--pey , business-flight--red');
                    var recommendedFlight = $(this).closest('.flight-list-item').find('.flight-station-item').find('.inner-info');
                    recommendedFlight.find('.cabin-color').addClass('hidden');
                    recommendedFlight.find('.cabin-color.cabin-class-selected').removeClass('hidden');

                    // start animation
                    if (!isOpen) {
                        if (!$(this).hasClass('not-available')) {
                            selfTable.find('[data-tooltip], input').attr('tabindex', 0);
                            selfTable.find('a').each(function () {
                                var hasDisabled = $(this).closest('.has-disabled').length;

                                !hasDisabled && $(this).attr('tabindex', 0);
                            })
                            $(this).next().attr('tabindex', -1);
                            btnMore.trigger('click.showMore');
                            $(this).addClass('active');

                            var selectedCabinClass = $(this).parent().parent().find('.bgd-white [data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]');
                            if ($(this).hasClass('column-trigger-animation')) {
                                var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
                                if (!$(body).hasClass('fs-business')) {
                                    hiddenRecommended.addClass('active economy-flight--green');
                                } else {
                                    hiddenRecommended.addClass('active business-flight--blue');
                                }
                                recommendedFlightItem.find('[data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]').removeClass('hidden');
                                recommendedFlightItem.find('[data-col-cabinclass="' + $(this).prev().data('target-cabinclass') + '"]').addClass('hidden');

                                selectedCabinClass.addClass('cabin-class-selected');
                                selectedCabinClass.next().removeClass('cabin-class-selected').addClass('hidden');
                            }
                            if ($(this).hasClass('column-trigger-animation-1')) {
                                var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
                                if (!$(body).hasClass('fs-business')) {
                                    hiddenRecommended1.addClass('active economy-flight--pey');
                                } else {
                                    if ($(body).hasClass('fs-economy-rbd')) {
                                        hiddenRecommended1.addClass('active economy-flight--pey');
                                    } else {
                                        hiddenRecommended1.addClass('active business-flight--red');
                                    }
                                }

                                recommendedFlightItem.find('[data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]').removeClass('hidden');
                                recommendedFlightItem.find('[data-col-cabinclass="' + $(this).next().data('target-cabinclass') + '"]').addClass('hidden');

                                selectedCabinClass.addClass('cabin-class-selected');
                                selectedCabinClass.prev().removeClass('cabin-class-selected').addClass('hidden');
                            }
                        }
                    }
                }
            });
        });
    }

    var getTemplateCarousel = function (sliderEl, btnNext, btnPrev, daysData, callback) {
        $.get(global.config.url.fsSevenDayFareTemplate, function (data) {
            var slides = sliderEl.find('.slides');
            var template = window._.template(data, {
                data: daysData
            });
            slides.empty().append($(template));
            initSlider(sliderEl, btnNext, btnPrev);

            if (callback) {
                callback();
            }
        });
    }

    var loadCarouselSevenDay = function (sliderEl, btnNext, btnPrev, isNext) {
        var urlSevenday = global.config.url.fsSevenDayFareJson;
        var selfWrapContent = sliderEl.closest('.wrap-content-fs').index();

        if ($('body').hasClass('sk-ut-flight-search-a')) {
            urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-a-1.json' : 'ajax/histogram-bydate-response-sk-a-2.json';
        } else if ($('body').hasClass('sk-ut-flight-search-b')) {
            urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-b-1.json' : 'ajax/histogram-bydate-response-sk-b-2.json';
        }
        $.get(urlSevenday, function (data) {
            var initDays = data.response.byDay,
                currDate = sliderEl.data('current-date'),
                currDateIdx = 0,
                slideToIdx = 0,
                currDateIdx1 = 7;

            _.map(initDays, function (day, idx) {
                if (day.month === currDate) {
                    currDateIdx = idx;
                }
                return false;
            })
            if (typeof isNext !== 'undefined') {
                currDateIdx = isNext ? currDateIdx + 7 : currDateIdx - 7;
            }

            currDateIdx = currDateIdx < 0 ? 0 : currDateIdx;

            currDateIdx1 = currDateIdx;

            if (initDays.length - currDateIdx < 7) {
                currDateIdx = currDateIdx - (7 - (initDays.length - currDateIdx));
            }

            days = initDays.slice(currDateIdx, currDateIdx + 7);
            getTemplateCarousel(sliderEl, btnNext, btnPrev, days, function () {
                if (typeof isNext !== 'undefined') {
                    sliderEl.data('current-date', initDays[currDateIdx].month);
                } else {
                    $('.slick-slide.slick-active').each(function () {
                        if ($(this).data('date') === sliderEl.attr('data-selected-date') && !$(this).is('.selected')) {
                            $(this).trigger('click.selectSlideItem', true);
                        }
                    })
                }
                btnNext && initDays.length - currDateIdx1 < 7 && currDateIdx >= 0 ? btnNext.attr('disabled', true) : btnNext.attr('disabled', false);
                btnPrev && currDateIdx === 0 ? btnPrev.attr('disabled', true) : btnPrev.attr('disabled', false);
            });
        })
    }

    var handleActionSlider = function () {
        var slider = $('[data-fs-slider]');
        // init
        slider.each(function () {
            var self = $(this);
            btnPrev = $(this).find('.btn-prev'),
                btnNext = $(this).find('.btn-next');

            loadCarouselSevenDay($(this), btnNext, btnPrev);

            btnPrev.off('click.slidePrev').on('click.slidePrev', function () {
                loadCarouselSevenDay(self, $(this).siblings('.btn-next'), $(this), false);
            });

            btnNext.off('click.slideNext').on('click.slideNext', function () {
                loadCarouselSevenDay(self, $(this), $(this).siblings('.btn-prev'), true);
            });

        });

    }

    var formatTimeToHour = function (seconds) {
        return parseFloat(seconds / 3600).toFixed(2) + 'hr';
    }

    var formatTimeToDate = function (seconds) {
        var dateObj = new Date(seconds),
            date = dateObj.getDate(),
            month = dateObj.getMonth() + 1,
            hour = dateObj.getHours(),
            minute = dateObj.getMinutes();

        return hour + ':' + minute;
    }

    var sliderRange = function () {
        var rangeSlider = $('[data-range-slider]');

        rangeSlider.each(function () {
            var min = $(this).data('min'),
                max = $(this).data('max'),
                step = $(this).data('step'),
                unit = $(this).data('unit'),
                type = $(this).data('range-slider');
            labelFrom = '<span class="slider-from ' + type + '"></span',
                labelTo = '<span class="slider-to ' + type + '"></span',

                $(this).slider({
                    range: true,
                    min: min,
                    max: max,
                    step: step,
                    values: [min, max],
                    create: function () {
                        var slider = $(this),
                            leftLabel,
                            rightLabel;

                        switch (type) {
                            case "tripDuration":
                            case "layover":
                                leftLabel = $(labelFrom).text(formatTimeToHour(min));
                                rightLabel = $(labelTo).text(formatTimeToHour(max));
                                break;
                            case "departure":
                            case "arrival":
                                leftLabel = $(labelFrom).text(formatTimeToDate(min));
                                rightLabel = $(labelTo).text(formatTimeToDate(max));
                                break;
                            default:
                                break;
                        }

                        $(this).append(leftLabel);
                        $(this).append(rightLabel);
                    },
                    slide: function (event, ui) {
                        var Label;

                        switch (type) {
                            case "tripDuration":
                            case "layover":
                                $(this).find('.slider-from').text(formatTimeToHour(ui.values[0]));
                                $(this).find('.slider-to').text(formatTimeToHour(ui.values[1]));
                                break;
                            case "departure":
                            case "arrival":
                                $(this).find('.slider-from').text(formatTimeToDate(ui.values[0]));
                                $(this).find('.slider-to').text(formatTimeToDate(ui.values[1]));
                                break;
                            default:
                                break;
                        }
                    }
                })

            if (type === 'tripDuration') {
                $(this).find('.ui-slider-handle').eq(0).remove();
                $(this).find('.slider-from').remove();
            }

        });
    }

    var countLayover = function (segment) {
        var countLayover = 0;

        _.map(segment.legs, function (leg) {
            leg.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;

            leg.stops.length && _.map(leg.stops, function (stop) {
                stop.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;
            })
        })

        return countLayover;
    }

    var getFilterData = function (flightsData) {
        var arr = [];

        _.map(flightsData, function (flight, flightIdx) {
            var obj = {
                "nonStop": false,
                "oneStop": false,
                "twoStop": false,
                "codeShare": false
            };


            var tripDuration = _.sortBy(flight.segments, function (segment) {
                return segment.tripDuration;
            });

            var departure = _.sortBy(flight.segments, function (segment) {
                return new Date(segment.departureDateTime.replace(/-/g, '/')).getTime();
            });

            var arrival = _.sortBy(flight.segments, function (segment) {
                return new Date(segment.arrivalDateTime.replace(/-/g, '/')).getTime();
            });

            var layover = _.sortBy(flight.segments, function (segment) {
                var totalLayover = 0;

                _.map(segment.legs, function (leg, legIdx) {
                    totalLayover += leg.layoverDuration;
                })
                segment['totalLayover'] = totalLayover
                return totalLayover
            });

            _.map(flight.segments, function (segment) {
                var count = countLayover(segment);

                !segment.legs.length && (obj.nonStop = true);
                switch (count) {
                    case 0:
                        !obj.nonStop && (obj.nonStop = true);
                        break;
                    case 1:
                        !obj.oneStop && (obj.oneStop = true);
                        break;
                    case 2:
                        !obj.twoStop && (obj.twoStop = true);
                        break;
                    default:
                        break;
                }

                segment.legs.length && _.map(segment.legs, function (leg) {
                    if (typeof leg.codeShareFlight === "boolean") {
                        leg.codeShareFlight && (obj.codeShare = true);
                    }
                })
            })


            obj['minTripDuration'] = tripDuration[0].tripDuration;
            obj['maxTripDuration'] = tripDuration[tripDuration.length - 1].tripDuration;
            obj['minDeparture'] = new Date(departure[0].departureDateTime.replace(/-/g, '/')).getTime();
            obj['maxDeparture'] = new Date(departure[departure.length - 1].departureDateTime.replace(/-/g, '/')).getTime();
            obj['minArrival'] = new Date(arrival[0].arrivalDateTime.replace(/-/g, '/')).getTime();
            obj['maxArrival'] = new Date(arrival[arrival.length - 1].arrivalDateTime.replace(/-/g, '/')).getTime();
            obj['minLayover'] = layover[0].totalLayover;
            obj['maxLayover'] = layover[layover.length - 1].totalLayover;

            arr.push(obj);
        })

        return arr;
    }

    var filterFlights = function (data) {
        var filterBlock = $('[data-flight-filter]'),
            flightsArr = data.flights;

        filterBlock.each(function () {
            var flightsBlock = $(this).siblings('.recommended-flight-block'),
                flightIdx = $(this).data('flightFilter'),
                nonStopCheckbox = $('input[name="non-stop-' + flightIdx + '"]'),
                oneStopCheckbox = $('input[name="one-stop-' + flightIdx + '"]'),
                twoStopCheckbox = $('input[name="two-stop-' + flightIdx + '"]'),
                codeShareCheckbox = $('input[name="codeshare-' + flightIdx + '"]'),
                saGroupCheckbox = $('input[name="sa-group-' + flightIdx + '"]'),
                sliderTripDuration = $(this).find('[data-range-slider="tripDuration"]'),
                sliderDeparture = $(this).find('[data-range-slider="departure"]'),
                sliderArrival = $(this).find('[data-range-slider="arrival"]'),
                filterObj = {
                    "stopover": {
                        "nonstopVal": true,
                        "onestopVal": true,
                        "twostopVal": true
                    },
                    "operating": {
                        "codeshareVal": true
                    },
                    "tripduration": sliderTripDuration.slider("values"),
                    "departure": sliderDeparture.slider("values"),
                    "arrival": sliderArrival.slider("values")
                };

            // get value from checkbox

            nonStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.nonstopVal = $(this).is(':checked');
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            oneStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.onestopVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            twoStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.twostopVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            codeShareCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.operating.codeshareVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            saGroupCheckbox.off('change.resetFilter').on('change.resetFilter', function () {
                filterObj.stopover.nonstopVal = false;
                filterObj.stopover.onestopVal = false;
                filterObj.stopover.twostopVal = false;
                filterObj.operating.codeshareVal = false;
                filterObj.tripduration = [sliderTripDuration.data('min'), sliderTripDuration.data('max')];
                filterObj.departure = [sliderDeparture.data('min'), sliderDeparture.data('max')];
                filterObj.arrival = [sliderArrival.data('min'), sliderArrival.data('max')];
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            // get value from range slider

            sliderTripDuration.slider({
                stop: function (event, ui) {
                    filterObj.tripduration = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            sliderDeparture.slider({
                stop: function (event, ui) {
                    filterObj.departure = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            sliderArrival.slider({
                stop: function (event, ui) {
                    filterObj.arrival = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            // handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);

        });
    }

    var handleFilterFlights = function (el, data, flightData, filterObj, flightIdx) {
        var selfData = [],
            filterDataStopover = [],
            filterDataOperating = [],
            obj = {};

        _.map(filterObj.stopover, function (value, key) {
            selfData = [];

            switch (key) {
                case 'nonstopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return segment.legs.length === 0 || count === 0;
                    }));
                    break;
                case 'onestopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return count === 1;
                    }));
                    break;
                case 'twostopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return count === 2;
                    }));
                    break;
                default:
                    break;
            }

            filterDataStopover = $.unique([].concat.apply([], [filterDataStopover, selfData]));
        });

        filterDataStopover.length && (flightData = filterDataStopover);

        _.map(filterObj.operating, function (value, key) {
            selfData = [];

            _.map(flightData, function (flight) {
                flight.legs.length && _.map(flight.legs, function (leg) {
                    if (key === 'codeshareVal') {
                        if (value) {
                            typeof leg.codeShareFlight !== 'undefined' && selfData.push(flight);
                        } else {
                            typeof leg.codeShareFlight === 'undefined' && selfData.push(flight);
                        }
                        return false;
                    }
                })
                return false;
            })

            filterDataOperating = $.unique([].concat.apply([], [filterDataOperating, selfData]));
        });

        filterDataOperating.length && (flightData = filterDataOperating);

        var tripDuration = filterObj.tripduration;

        selfData = flightData.filter(function (flight) {
            return flight.tripDuration >= tripDuration[0] && flight.tripDuration <= tripDuration[1];
        });

        flightData = selfData;

        var departureDateTime = filterObj.departure;

        selfData = flightData.filter(function (flight) {
            return new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() >= departureDateTime[0] && new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() <= departureDateTime[1];
        });

        flightData = selfData;

        var arrivalDateTime = filterObj.arrival;

        selfData = flightData.filter(function (flight) {
            return new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() >= arrivalDateTime[0] && new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() <= arrivalDateTime[1];
        });

        flightData = selfData;

        obj['segments'] = flightData;

        getTemplateFlightTable(el, data, obj, flightIdx, true);
    }

    var handleLoadmore = function (flightBlock, loadmoreBlock) {
        var flights = flightBlock.find('.flight-list-item'),
            flightsHidden = flightBlock.find('.flight-list-item.hidden');

        flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
        loadmoreBlock.find('[data-total-flight]').text(flights.length);
        loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
        loadmoreBlock.off('click.loadMore').on('click.loadMore', function (e) {
            e.preventDefault();
            var nextFlightIdx = flightBlock.find('.flight-list-item:not(".hidden")').length;
            flightBlock.find('.flight-list-item.hidden').slice(0, 5).removeClass('hidden');
            flightsHidden = flightBlock.find('.flight-list-item.hidden');
            loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
            flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');

            setTimeout(function () {
                flightBlock.find('.flight-list-item').eq(nextFlightIdx).focus().addClass('focus-outline');
            }, 500);

            selectFlightAnimation();
            showMoreLessDetails();
        });
    }

    var labelStatusCheapest = function () {
        var arrPriceCheapest = [];
        var blockWrapFirst = $('.wrap-content-fs').first();
        var colInfoLeft = blockWrapFirst.find('.col-info-left');
        colInfoLeft.each(function (idexLabel) {
            arrPriceCheapest.push($(this).find('.flight-price').find('.price-cheapest-colum').text());
            arrPriceCheapest.sort(function (a, b) {
                return parseFloat(a) - parseFloat(b);
            });
            if ($(this).find('.flight-price').find('.price-cheapest-colum').text() === arrPriceCheapest[0]) {
                $(this).find('.head-col').append('<span class="label-status anim-all"></span');
            }
        });
    }

    var tableLoadPage = function (isRenderTemplate) {
        !isRenderTemplate && setTimeout(function () {
            if ($('.main-inner').find('.wrap-content-fs').length == 2) {
                $('.wrap-content-fs').last().addClass('hidden');
            }
            ;
        }, 500);
        var flightBlockItem = $('.recommended-flight-block').find('.flight-list-item');
        flightBlockItem.each(function (idx) {
            var rowSelect = $(this).find('[data-hidden-recommended]').find('.multy-column').find('.row-head-select');
            var rowSelect1 = $(this).find('[data-hidden-recommended-1]').find('.multy-column').find('.row-head-select');
            var rowSelectOneColumn = $(this).find('[data-hidden-recommended]').find('.one-column').find('.row-head-select');
            var rowSelectOneColumn1 = $(this).find('[data-hidden-recommended-1]').find('.one-column').find('.row-head-select');
            colSelectColor = rowSelect.find('.col-select');
            colSelectColor1 = rowSelect1.find('.col-select');
            colSelectColorOneColumn = rowSelectOneColumn.find('.col-select');
            colSelectColorOneColumn1 = rowSelectOneColumn1.find('.col-select');
            $(this).find('.row-head-select').find('.col-select').removeClass('economy-fs--green-1 , economy-fs--green-2 , economy-fs--green-3 , economy-fs--pey-1');

            if ($('body').hasClass('fs-business')) {
                flightBlockItem.removeClass('economy-flight-bgd').addClass('business-flight-bgd');
                flightBlockItem.find('.column-trigger-animation').addClass('business-flight--blue');

                var cbType = flightBlockItem.find('.column-trigger-animation-1').find('.text-head').html();
                if (cbType == 'Premium Economy') {
                    flightBlockItem.addClass('economy-flight-bgd');
                    flightBlockItem.find('.column-trigger-animation-1').addClass('economy-flight--pey');
                } else {
                    flightBlockItem.find('.column-trigger-animation-1').addClass('business-flight--red');
                }

                colSelectColor.each(function (idx) {
                    $(this).addClass('business-fs--blue-' + idx);
                });
                colSelectColor1.each(function (idx) {
                    if (cbType == 'Premium Economy') {
                        $(this).addClass('economy-fs--pey-' + idx);
                    } else {
                        $(this).addClass('business-fs--red-' + idx);
                    }
                });

                colSelectColorOneColumn.each(function (idx) {
                    $(this).addClass('business-fs--blue-1');
                });
                colSelectColorOneColumn1.each(function (idx) {
                    if (cbType == 'Premium Economy') {
                        $(this).addClass('economy-fs--pey-1');
                    } else {
                        $(this).addClass('business-fs--red-1');
                    }
                });

            } else {
                colSelectColor.each(function (idx1) {
                    $(this).addClass('economy-fs--green-' + idx1);
                });
                colSelectColor1.each(function (idx1) {
                    $(this).addClass('economy-fs--pey-' + idx1);
                });

                colSelectColorOneColumn.each(function (idx1) {
                    $(this).addClass('economy-fs--green-1');
                });
                colSelectColorOneColumn1.each(function (idx1) {
                    $(this).addClass('economy-fs--pey-1');
                });
            }
        });

        var dataRecommended = $('[data-hidden-recommended]');
        var dataRecommended1 = $('[data-hidden-recommended-1]');
        dataRecommended.each(function (idxData) {
            var colSelectItem = $(this).find('.select-fare-table.hidden-mb-small').find('.col-select');
            var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
            var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
            var btnPrice = colSelectItem.find('.btn-price');
            if (btnPrice.length > 1 && !$('body').hasClass('fs-business')) {
                var buttonFirst = btnPrice.first();
                buttonFirst.addClass('btn-price-cheapest-select');
                buttonFirst.closest('.col-select').next().find('.btn-price').addClass('btn-price-cheapest-select');

                var buttonLast = btnPrice.last();
                buttonLast.removeClass('btn-price-cheapest-select');
            }

            if (lengthColSelectItem == 2) {
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
            }
        });

        dataRecommended1.each(function (idxData) {
            var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
            var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
            if (lengthColSelectItem == 2) {
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
            }
        });

        labelStatusCheapest();
    }

    var wrapContent, nextWrapContent, parentFlightList, el, blockParentWrap,
        blockParentColSelect, wrapContents, wrapContentList,
        nameHead, baggage, seatSelection, earnKrisFlyer, upgrade,
        cancellation1, cancellation2, bookingChange1, bookingChange2, noShow1, noShow2, priceCurrentSelected, idx1,
        idx2, hasCodeShare = false, originSelected, returnSelected;

    var hiddenBlockClick = function () {
        wrapContent = el.closest('.wrap-content-fs');
        if (el.closest('.wrap-content-fs').index() === wrapContents.length - 1) {
            nextWrapContent = wrapContent.prev();
        }else {
            nextWrapContent = wrapContent.next();
        }

        wrapContent.css({
            'opacity': 0
        });
        
        if (el.closest('.wrap-content-fs').index() !== wrapContents.length - 1) {
            nextWrapContent.css({
                'opacity': 0
            });
        }

        setTimeout(function(){
            wrapContent.find('.anim-all').addClass('no-transition');
            nextWrapContent.find('.anim-all').addClass('no-transition');

            $('html, body').scrollTop(wrapContent.offset().top - $('[data-booking-summary-panel]').innerHeight());

            processODBlocks();

            setTimeout(function(){
                wrapContent.css({
                    'opacity': 1
                });

                setTimeout(function(){
                    nextWrapContent.css({
                        'opacity': 1
                    });

                    setTimeout(function(){
                        wrapContent.find('.anim-all').removeClass('no-transition');
                        nextWrapContent.find('.anim-all').removeClass('no-transition');
                    }, 100);
                }, 500);
                
            }, 200);
        }, 200);
    }

    var processODBlocks = function(){
        var selfHeight = el.closest('.flight-list-item').find('.recommended-table [data-wrap-flight-height]').data('wrap-flight-height');

        if (el.closest('.wrap-content-fs').index() === wrapContents.length - 1) {
            el = $('.btn-price.active');
            var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
            var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text();
            var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text();
            var listRow = el.closest('.select-fare-table').find('.row-select, .row-head-select');
            var thisCol = el.closest('.col-select');
            var idx = thisCol.index();
            var listContents, colRight, colLeft;
            var noteFare = $('.has-note-fare');
            listRow.each(function () {
                $(this).find('.col-select').eq(idx).addClass('col-selected');
            });
            var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
            var colSelected = listRow.find('.col-select.col-selected');
            var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
            idx2 = colSelected.find('.index-of').text();
            nameHead = el.attr('data-header-class');

            baggage = colSelected.find('.baggage').text();
            seatSelection = colSelected.find('.seat-selection').html();
            earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
            upgrade = colSelected.find('.upgrade').text();
            cancellation2 = colSelected.find('.cancellation').text();
            bookingChange2 = colSelected.find('.booking-change').text();
            noShow2 = colSelected.find('.no-show').text();
            listContents = summaryGroup.find('.row-select .column-left span, .row-select .column-right span');
            colLeft = summaryGroup.find('.col-select.column-left');
            colRight = summaryGroup.find('.col-select.column-right');
            summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').attr('class', className);
            summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').addClass('border-fare-family');
            summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
            summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
            summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('value', eligibleRecommendationIds);
            colRight.find('.baggage').text(baggage);
            colRight.find('.seat-selection').html(seatSelection); 

            colRight.find('.earn-krisFlyer').text(earnKrisFlyer);

            var upgradeTextSummary = colRight.find('.upgrade');
            upgradeTextSummary.text(upgrade);
            if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
                var upgradeText = colSelected.find('.upgrade');
                var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
                if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
                    upgradeTextSummary.parent().append(upgradeTooltip.clone());
                    upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
                }
            } else {
                upgradeTextSummary.siblings('em').remove();
            }

            // --- Compare 2 fare conditions
            noteFare.show();
            $('.note-fare').removeClass('hidden');
            if (idx1 < idx2) {
                colLeft.find('.cancellation').text(cancellation1);
                colLeft.find('.booking-change').text(bookingChange1);
                colLeft.find('.no-show').text(noShow1);
                // colRight.find('.cancellation').text(cancellation1 + ' *');
                // colRight.find('.booking-change').text(bookingChange1 + ' *');
                // colRight.find('.no-show').text(noShow1 + ' *');
                colRight.find('.cancellation').text(cancellation1);
                colRight.find('.booking-change').text(bookingChange1);
                colRight.find('.no-show').text(noShow1);
            } else if (idx1 > idx2) {
                // colLeft.find('.cancellation').text(cancellation2 + ' *');
                // colLeft.find('.booking-change').text(bookingChange2 + ' *');
                // colLeft.find('.no-show').text(noShow2 + ' *');
                colLeft.find('.cancellation').text(cancellation2);
                colLeft.find('.booking-change').text(bookingChange2);
                colLeft.find('.no-show').text(noShow2);
                colRight.find('.cancellation').text(cancellation2);
                colRight.find('.booking-change').text(bookingChange2);
                colRight.find('.no-show').text(noShow2);
            } else {
                noteFare.hide();
                if(!hasCodeShare) $('.note-fare').addClass('hidden');
                colLeft.find('.cancellation').text(cancellation1);
                colLeft.find('.booking-change').text(bookingChange1);
                colLeft.find('.no-show').text(noShow1);
                colRight.find('.cancellation').text(cancellation2);
                colRight.find('.booking-change').text(bookingChange2);
                colRight.find('.no-show').text(noShow2);
            }
            // --- End

            var eachColSelect = summaryGroup.find('.row-head-select').find('.col-select');
            eachColSelect.each(function () {
                var valuettt = eachColSelect.find('.ttt').val();
                if (valuettt === "0") {
                    summaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
                    summaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
                    summaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
                } else {
                    summaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
                    summaryGroup.find('.button-group-1').find('.text').addClass('hidden');
                    summaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
                }
            });

            // --- Render color accordingly
            listContents.each(function () {
                var _this = $(this),
                    thisText = _this.text();
                _this.removeClass('complimentary not-allowed fare-price');
                if (thisText.indexOf('Complimentary') !== -1) {
                    _this.addClass('complimentary');
                } else if ((thisText.indexOf('Not allowed') !== -1) || (thisText.indexOf('Only available during online check-in') !== -1)) {
                    _this.addClass('not-allowed');
                } else {
                    _this.addClass('fare-price');
                }
            });
            // --- End

            var rcmidCorresponding = el.find('.rcmid-corresponding').text();
            var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
            var flightId = el.closest('.flight-list-item').find('.segment-id').text();
            summaryGroup.find('#recommendation-id').val(rcmidCorresponding);
            summaryGroup.find('#fare-family-inbound').val(fareFamilyId);
            summaryGroup.find('#flight-inbound').val(flightId.trim());

            summaryGroup.removeClass('hidden');
            slideshowpremiumeconomy();
            listRow.each(function () {
                $(this).find('.col-select').eq(idx).removeClass('col-selected');
            });

            var nameTtem2 = summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            $('.flight-search-summary-conditions').find('.tab-item-2').text(nameTtem2.text());
            $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');

            showCorrelativeTab();

            // scrollTop func
            // setTimeout(function () {
            //     $('html, body').animate({
            //         scrollTop: $('.wrap-content-fs:eq(1)').offset().top - $('[data-booking-summary-panel]').innerHeight() - 20
            //     }, 0);
            // }, 750);
        } else {
            el = $('.btn-price.active');
            var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
            var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text();
            var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text();
            var listRow1 = el.closest('.select-fare-table').find('.row-select, .row-head-select');
            var thisCol = el.closest('.col-select');
            var idx = thisCol.index();
            listRow1.each(function () {
                $(this).find('.col-select').eq(idx).addClass('col-selected');
            });
            var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
            var colSelected = listRow1.find('.col-select.col-selected');
            var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
            idx1 = colSelected.find('.index-of').text();
            nameHead = el.attr('data-header-class');
            baggage = colSelected.find('.baggage').text();
            seatSelection = colSelected.find('.seat-selection').html();
            earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
            upgrade = colSelected.find('.upgrade').text();
            cancellation1 = colSelected.find('.cancellation').text();
            bookingChange1 = colSelected.find('.booking-change').text();
            noShow1 = colSelected.find('.no-show').text();
            colLeft = summaryGroup.find('.col-select.column-left');
            summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').attr('class', className);
            summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').addClass('border-fare-family');
            summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
            summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
            summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('value', eligibleRecommendationIds);
            colLeft.find('.baggage').text(baggage);
            colLeft.find('.seat-selection').html(seatSelection);

            colLeft.find('.earn-krisFlyer').text(earnKrisFlyer);

            var upgradeTextSummary = colLeft.find('.upgrade');
            upgradeTextSummary.text(upgrade);
            if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
                var upgradeText = colSelected.find('.upgrade');
                var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
                if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
                    upgradeTextSummary.parent().append(upgradeTooltip.clone());
                    upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
                }
            } else {
                upgradeTextSummary.siblings('em').remove();
            }

            var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
            var flightId = el.closest('.flight-list-item').find('.segment-id').text();
            summaryGroup.find('#fare-family-outbound').val(fareFamilyId);
            summaryGroup.find('#flight-outbound').val(flightId.trim());

            summaryGroup.addClass('hidden');
            var nameTtem1 = summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            $('.flight-search-summary-conditions').find('.tab-item-1').text(nameTtem1.text());
            $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

            listRow1.each(function () {
                $(this).find('.col-select').eq(idx).removeClass('col-selected');
            });

            // scrollTop func
            // setTimeout(function () {
            //     $('html, body').animate({
            //         scrollTop: $('.wrap-content-fs:eq(0)').offset().top - $('[data-booking-summary-panel]').innerHeight() - 20
            //     }, 0);
            // }, 750);
        }

        el.closest('.upsell').addClass('hidden');
        parentFlightList = el.closest('.flight-list-item');
        // parentFlightList.find('.col-info-left').get(0).click();
        parentFlightList.find('.change-flight-item.bgd-white').find('[data-wrap-flight-height]').data('wrap-flight-height', selfHeight);
        parentFlightList.find('.change-flight-item.bgd-white').removeClass('hidden');
        wrapContent.find('.economy-slider , .fs-status-list , .sub-logo , .monthly-view , .flight-search-filter-economy , .recommended-table , .head-recommended, .loadmore-block').addClass('hidden');
        wrapContent.find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
        wrapContent.find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
        wrapContent.find('.col-info-select').removeClass('active');
        parentFlightList.find('[data-less-details-table]').removeClass('hidden').removeClass('premium-economy');

        if (blockParentColSelect.find('.btn-price').hasClass('active') && $('.main-inner').find('.wrap-content-fs').length == 2) {
            blockParentWrap.next().removeClass('hidden');

            var dataTriggercolumn = blockParentWrap.next().find('[data-trigger-animation]');
            if (dataTriggercolumn.hasClass('has-disabled')) {
                dataTriggercolumn.removeClass('active');
                // dataTriggercolumn.closest('.recommended-table').find('.airline-info').find('.less-detail').get(0).click();
                dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
                dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
            }

        } else {
            blockParentWrap.next().addClass('hidden');
        }

        el.closest('.wrap-content-fs').find('.flight-list-item').attr('tabindex', -1);
        handleLoadmore(blockParentWrap.next().find('.recommended-flight-block'), blockParentWrap.next().find('[data-loadmore]'));

        // Reset spacing
        blockParentWrap.next().find('.recommended-flight-block').removeClass('collapsed');

        handleActionSlider();
        showMoreLessDetails();
    };

    var upgradeAdditional = function () {
        var nextBtn = el.closest('.col-select').next().find('.btn-price');
        var nameHeaderClick = nextBtn.data('header-class');
        var priceClick = el.find('.btn-price-cheapest-colum').text();
        var priceUpgrade = nextBtn.find('.btn-price-cheapest-colum').text();
        el.closest('[data-hidden-recommended]').find('.upsell').find('.name-family').text(nameHeaderClick);

        var newPrice;
        if(isNaN(parseFloat(priceClick))) {
          newPrice = parseFloat(priceUpgrade).toLocaleString(undefined, {minimumFractionDigits: 2});
        }
        if(isNaN(parseFloat(priceUpgrade))) {
          newPrice = parseFloat(priceClick).toLocaleString(undefined, {minimumFractionDigits: 2});
        }
        if(!isNaN(parseFloat(priceUpgrade)) && !isNaN(parseFloat(priceClick))) {
          newPrice = (parseFloat(priceUpgrade) - parseFloat(priceClick)).toLocaleString(undefined, {minimumFractionDigits: 2});
        }
        el.closest('[data-hidden-recommended]').find('.upsell').find('.price-sgd').text(' ' + newPrice);
    }

    $(document).on('click.calculatePriceInbound', '.btn-price', function (e) {
        e.preventDefault();
        // prototype
        if ($('body').hasClass('sk-ut-flight-search-a')) {
            if ($(this).closest('[data-returntrip]').data('returntrip') === 0 && ($(this).data('header-class') !== 'Economy Super Saver' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
            } else if ($(this).closest('[data-returntrip]').data('returntrip') === 1 && ($(this).data('header-class') !== 'Economy Lite' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
            }
        }

        if ($('body').hasClass('sk-ut-flight-search-b')) {
            if ($(this).closest('[data-returntrip]').data('returntrip') === 0 && ($(this).data('header-class') !== 'Economy Standard' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 3)) {
                return false;
            } else if ($(this).closest('[data-returntrip]').data('returntrip') === 1 && ($(this).data('header-class') !== 'Economy Flexi' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
            }
        }
        //

        if (!$(this).hasClass('btn-price-cheapest-select')) {
            $(this).parents('.recommended-flight-block').addClass('collapsed');
            var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
            changeFlightItem.each(function () {
                $(this).addClass('has-choose');
            });
        }

        var arrayPrice1, arrayPrice2;
        wrapContentList = $('.wrap-content-list');
        wrapContents = wrapContentList.find('.wrap-content-fs');
        var wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');
        clickedBtn = $(this);

        el = $(this);
        blockParentWrap = el.closest('.wrap-content-fs');

        // 
        // --- Start Animation Fix
        //
        setTimeout(function(){
            var dataTriggerAnimation = $('.wrap-content-fs').find('[data-flight-item]');
            if (!$(this).hasClass('btn-price-cheapest-select')) {
                dataTriggerAnimation.each(function () {
                    if ($(this).hasClass('active')) {
                        $(this).closest('.flight-list-item').find('.col-info').removeClass('selected-item')
                        $(this).find('.less-detail').get(0).click();
                    }
                });
            }
        }, 850);
        // 
        // --- End Animation Fix
        //

        blockParentWrap.removeClass('selected-item');

        if (el.closest('.wrap-content-fs').index() === 0) {
            wrapContentNotFirst.find('.change-flight-item.bgd-white').addClass('hidden');
            wrapContentNotFirst.find('.economy-slider , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
            wrapContentNotFirst.find('.col-select').find('.btn-price').removeClass('active');
        }

        var listItems = wrapContentNotFirst.find('.flight-list-item');
        var listCol = listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
        var listPrice = listItems.find('.btn-price').find('.list-price');
        var oneCol = listItems.find('.one-column');
        var contentListPrice = $(this).find('.list-price').text();

        // el.closest('.select-fare-block').addClass('selected-item');
        if (el.hasClass('column-left')) {
            el.closest('.flight-list-item').find('.column-trigger-animation').addClass('selected-item');
        } else if (el.hasClass('column-right')) {
            el.closest('.flight-list-item').find('.column-trigger-animation-1').addClass('selected-item');
        }

        upgradeAdditional();
        blockParentWrap = el.closest('.wrap-content-fs');

        var selectedFareTypeBlock = blockParentWrap.find('.selected-fare-type');
        var selectedFareType = $(this).data('cabin-class');
        var selectedFareClass = '';
        if (selectedFareType == 'Business') {
            selectedFareClass = 'label-bus';
        } else if (selectedFareType == 'Premium Economy') {
            selectedFareClass = 'label-pey';
        } else if (selectedFareType == 'First') {
            selectedFareClass = 'label-first';
        } else {
            selectedFareClass = 'label-economy';
        }

        var span = selectedFareTypeBlock.find('span').text($(this).data('header-class')).removeClass().addClass(selectedFareClass);

        // Reset for each interaction
        $('.has-note-fare-oal').addClass('hidden');

        // Check for codeshare attribute
        var codeShare = $(this).data('codeshare');
        if(codeShare) hasCodeShare = true;

        if (hasCodeShare) {
            $('.summary-label').each(function(){
                $(this).text($(this).text().replace(/\^/g,'') + '^');
            });
            $('.has-note-fare-oal').removeClass('hidden');
        }else {
          $('.summary-label').each(function(){
              var label = $(this).text();
              $(this).text(label.replace('^', ''));
          });
        }

        blockParentColSelect = blockParentWrap.find('.col-select');
        blockParentColSelect.find('.btn-price').removeClass('active');
        $('.col-select').find('.btn-price').removeClass('active');
        $('.upsell').addClass('hidden');
        $(this).addClass('active');
        if ($(this).hasClass('btn-price-cheapest-select')) {
            var upsell = $(this).closest('[data-hidden-recommended]').find('.upsell');

            if (blockParentWrap.index() === 0) {
                upsell.removeClass('hidden');
            } else {
                hiddenBlockClick();
            }
            setTimeout(function () {
                upsell.find('input[name="btn-keep-selection"]').focus();
            }, 100);
        } else {
            hiddenBlockClick();
        }

        if (el.closest('.wrap-content-fs').index() !== 0) {
            return;
        }

        listCol.addClass('has-disabled').attr('tabindex', -1);
        oneCol.each(function () {
            if (!$(this).hasClass('hidden')) {
                var thisIdx = $(this).closest('.select-fare-block').data('col-index');
                var thisColSelect = $(this).closest('.flight-list-item').find('[data-trigger-animation]');

                thisColSelect.each(function () {
                    if ($(this).data('trigger-animation') === thisIdx) {
                        $(this).addClass('has-disabled').attr('tabindex', -1);
                    }
                });
            }
        });

        var listRcmID = listItems.find('.btn-price').find('.rcmid');
        var rcmIdListPrice = $(this).find('.rcmid-out').text();
        var rcmidListPriceTrim = rcmIdListPrice.trim();
        var arrayRcmId1 = rcmidListPriceTrim.split(" ");
        if (listRcmID.length) {
            listRcmID.each(function () {
                var self = $(this);
                var arrayList = self.text().split("-");
                for (var j = 0; j < arrayList.length; j++) { // 21 63
                    for (var z = 0; z < arrayList[j].length; z++) { //
                        for (var i = 0; i < arrayRcmId1.length; i++) { // 8
                            if (arrayList[z] !== null || arrayList[z] !== "undefined" || arrayList[z] !== undefined) {
                                if (arrayList[z]) {
                                    if (arrayRcmId1[i] === arrayList[z].slice(0, 4).trim()) {

                                        var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
                                        var thisCol = $(this).closest('.col-select');
                                        var idx = thisCol.index();
                                        var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
                                        listRow.each(function () {
                                            $(this).find('.col-select').eq(idx).removeClass('has-disabled');
                                        });
                                        thisColSelectOneCol.removeClass('has-disabled');

                                        $(this).closest('.btn-price').find('.btn-price-cheapest-colum').text(arrayList[z].slice(3).trim());
                                        $(this).closest('.btn-price').find('.rcmid-corresponding').text(arrayRcmId1[i]);
                                        $(this).closest('.btn-price').attr('data-outbound-totalamt', el.attr('data-totalamt'));

                                        if(typeof $(this).closest('.btn-price').attr('data-totalamt-obj') !== 'undefined') {
                                          var totalAmtObj = JSON.parse($(this).closest('.btn-price').attr('data-totalamt-obj'));
                                          var combinableTotalAmt = totalAmtObj[arrayRcmId1[i]+''];

                                          $(this).closest('.btn-price').attr('data-totalamt', combinableTotalAmt);
                                          $(this).closest('.btn-price').attr('data-cmbnble-rcmid', arrayRcmId1[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        listItems.each(function () {
            var that = $(this);
            var thisFareBlock = $(this).find('.select-fare-block');
            var listColInfoSel = $(this).find('.col-info-select');
            thisFareBlock.each(function () {
                var listRowHeadSel = $(this).find('.select-fare-table.multy-column.hidden-mb-small .row-head-select');
                var listColSelLength = listRowHeadSel.find('.col-select:not(:eq(0))').length,
                    listColSelDisabledLength = listRowHeadSel.find('.col-select.has-disabled').length;
                if (listColSelLength === listColSelDisabledLength) {
                    var idx = $(this).data('col-index');
                    listColInfoSel.each(function () {
                        if ($(this).data('trigger-animation') === idx) {
                            $(this).addClass('has-disabled').attr('tabindex', -1);
                        }
                    });
                }
            });
        });

        var flightColumnTrigger = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation');
        var flightColumnTrigger1 = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation-1');
        if (el.closest('.wrap-content-fs').index() === 0) {
            var arrPriceCheapest = [];
            var blockWrapFirst = $('.wrap-content-fs').last();
            var colInfoLeft = blockWrapFirst.find('.col-info-left');
            colInfoLeft.find('.head-col').find('.label-status').remove();

            priceCurrentSelected = el.find('.btn-price-cheapest-colum').text();

            flightColumnTrigger.each(function (idx) {
                $(this).find('.price-cheapest-outbound').text(priceCurrentSelected);
                $(this).closest('.flight-list-item').find('[data-hidden-recommended]').find('.price-cheapest-colum').text(priceCurrentSelected);
            });

            colInfoLeft.each(function (idexLabel) {
                arrPriceCheapest.push($(this).find('.price').find('.compare-number').eq(0).text());
            });

            arrPriceCheapest.sort(function (a, b) {
                return parseFloat(a) - parseFloat(b);
            });

            colInfoLeft.each(function () {
                if (parseFloat($(this).find('.flight-price').find('.compare-number').eq(0).text()) === parseFloat(arrPriceCheapest[0])) {
                    $(this).find('.head-col').append('<span class="label-status anim-all"></span');
                }
            })

            flightColumnTrigger1.each(function (idx1) {
                $(this).find('.price-cheapest-outbound-1').text(priceCurrentSelected);
                $(this).closest('.flight-list-item').find('[data-hidden-recommended-1]').find('.price-cheapest-colum').text(priceCurrentSelected);
            });

            var colSelect = $('.wrap-content-fs').last().find('[data-hidden-recommended]').find('.col-select').find('.btn-price');
            var colSelect1 = $('.wrap-content-fs').last().find('[data-hidden-recommended-1]').find('.col-select').find('.btn-price');

            colSelect.each(function (idxColSelect) {
                var priceCheapestColumn = $(this).closest('[data-hidden-recommended]').find('.price-cheapest-colum').text();
                var priceCheapestColumnCabin = $(this).find('.btn-price-cheapest-colum').text();

                var totalPriceBtnColumn = parseFloat(priceCheapestColumnCabin) - parseFloat(priceCheapestColumn);
                var toFixedPrice = totalPriceBtnColumn.toFixed(2);
                var sym = '+ ';
                if (totalPriceBtnColumn < 0) {
                    // totalPriceBtnColumn = 0;
                    sym = '- ';
                }

                var priceStyleUnitSmall = toFixedPrice.indexOf(".");
                if (priceCheapestColumnCabin !== '') {
                    var price = parseFloat(totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall));
                    $(this).find('.unit-small').text('');
                    $(this).find('.btn-price-cheapest').text('');
                    var dec = totalPriceBtnColumn.toFixed(2).slice(priceStyleUnitSmall) === '' ? '.00' : totalPriceBtnColumn.toFixed(2).slice(priceStyleUnitSmall);
                    $(this).prev().text(sym + $(this).attr('data-currency') + ' ' + Math.abs(price).toLocaleString() + dec);
                    $(this).find('.unit-small').text('');
                    $(this).attr('data-price-segment-after', totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall));
                    $(this).attr('data-computed-totalamt', ( parseFloat($(this).attr('data-totalamt')) - parseFloat($(this).attr('data-outbound-totalamt')) ));
                }
            });

            colSelect1.each(function (idxColSelect1) {
                var priceCheapestColumn1 = $(this).closest('[data-hidden-recommended-1]').find('.price-cheapest-colum').text();
                var priceCheapestColumnCabin1 = $(this).find('.btn-price-cheapest-colum').text();

                var totalPriceBtnColumn1 = parseFloat(priceCheapestColumnCabin1) - parseFloat(priceCheapestColumn1);
                var toFixedPrice1 = totalPriceBtnColumn1.toFixed(2);

                var sym = '+ ';
                if (totalPriceBtnColumn1 < 0) {
                    // totalPriceBtnColumn1 = 0;
                    sym = '- ';
                }

                var priceStyleUnitSmall1 = toFixedPrice1.indexOf(".");
                if (priceCheapestColumnCabin1 !== '') {
                    var price = parseFloat(totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
                    $(this).find('.unit-small').text('');
                    $(this).find('.btn-price-cheapest-1').text('');
                    var dec = totalPriceBtnColumn1.toFixed(2).slice(priceStyleUnitSmall1) === '' ? '.00' : totalPriceBtnColumn1.toFixed(2).slice(priceStyleUnitSmall1);
                    $(this).prev().text(sym + $(this).attr('data-currency') + ' ' + Math.abs(price).toLocaleString() + dec);
                    $(this).find('.unit-small').text('');
                    $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
                    $(this).attr('data-computed-totalamt', ( parseFloat($(this).attr('data-totalamt')) - parseFloat($(this).attr('data-outbound-totalamt')) ));
                }
            });

            flightColumnTrigger.each(function (idx) {
                // var priceCheapest = $(this).find('.price-cheapest-colum').text();
                var priceSelectedOutBound = $(this).find('.price-cheapest-outbound').text();
                var cheapestPriceBtn = $(this).parent().parent().find('[data-hidden-recommended]').find('.row-select.row-footer-select .col-select').find('.btn-price .btn-price-cheapest-colum');
                var cheapestPrice = [];

                cheapestPriceBtn.each(function(){
                  if(!$(this).parent().parent().hasClass('has-disabled')) cheapestPrice.push($(this).text());
                });

                cheapestPrice.sort(function (a, b) {
                    return parseFloat(a) - parseFloat(b);
                });

                var priceCheapest = cheapestPrice[0];

                if (priceCheapest && priceSelectedOutBound) {
                    var totalPriceCabin = parseFloat(priceCheapest) - parseFloat(priceSelectedOutBound);
                    var sym = '+ ';
                    if (totalPriceCabin < 0) {
                        sym = '- ';
                    }
                    var triggerAnimation = $(this);
                    var priceStyleSmall = totalPriceCabin.toFixed(2).indexOf(".");

                    triggerAnimation.find('.price').text(sym + Math.abs(parseFloat(totalPriceCabin.toFixed(2).slice(0, priceStyleSmall))).toLocaleString());
                    triggerAnimation.find('.price').append("<small></small>");
                    triggerAnimation.find('.price').find('small').text(totalPriceCabin.toFixed(2).slice(priceStyleSmall));

                    triggerAnimation.find('.price').append('<span class="compare-number hidden">' + totalPriceCabin + '</span>');
                }
                // }
            });

            flightColumnTrigger1.each(function (idx1) {
                // var priceCheapest1 = $(this).find('.price-cheapest-colum').text();
                var priceSelectedOutBound1 = priceCurrentSelected;

                var cheapestPriceBtn = $(this).parent().parent().find('[data-hidden-recommended-1]').find('.row-select.row-footer-select .col-select').find('.btn-price .btn-price-cheapest-colum');
                var cheapestPrice = [];
                cheapestPriceBtn.each(function(){
                  if(!$(this).parent().parent().hasClass('has-disabled')) cheapestPrice.push($(this).text());
                });

                cheapestPrice.sort(function (a, b) {
                    return parseFloat(a) - parseFloat(b);
                });

                var priceCheapest1;

                // Make sure disabled triggers are computed
                if(cheapestPrice.length === 0) {
                   priceCheapest1 = cheapestPriceBtn.eq(0).text();
                }else {
                   priceCheapest1 = cheapestPrice[0];
                }

                if (priceCheapest1 && priceSelectedOutBound1) {
                    var totalPriceCabin1 = parseFloat(priceCheapest1) - parseFloat(priceSelectedOutBound1);
                    var sym = '+ ';
                    if (totalPriceCabin1 < 0) {
                        // totalPriceCabin1 = 0;
                        sym = '- ';
                    }

                    var triggerAnimation1 = $(this);
                    var priceStyleSmall1 = totalPriceCabin1.toFixed(2).indexOf(".");
                    triggerAnimation1.find('.price').text(sym + Math.abs(parseFloat(totalPriceCabin1.toFixed(2).slice(0, priceStyleSmall1))).toLocaleString());
                    triggerAnimation1.find('.price').append("<small></small>");
                    triggerAnimation1.find('.price').find('small').text(totalPriceCabin1.toFixed(2).slice(priceStyleSmall1));
                }
                // }
            });
        }

        $('[data-trigger-animation]').not('.has-disabled').not('.not-available').attr('tabindex', 0);
    });

    $(document).on('click', '.group-btn > #btn-keep-selection', function (e) {
        e.preventDefault();
        el = $(this);

        $(this).parents('.recommended-flight-block').addClass('collapsed');

        if (!$(this).hasClass('btn-price-cheapest-select')) {
            var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
            changeFlightItem.each(function () {
                $(this).addClass('has-choose');
            });
        }
        hiddenBlockClick();
    });

    $(document).on('click', '.group-btn > #btn-upgrade', function (e) {
        e.preventDefault();
        if (!$('body').hasClass('fs-business')) {
            if ($('body').hasClass('sk-ut-workflow')) {
                return false;
            }
        }
        var btnPriceActive = $(this).closest('.flight-list-item').find('.col-select').find('.btn-price.active');
        var parentBtn = btnPriceActive.closest('.col-select');

        var prevUpsell = $(this).closest('.upsell').prev().find('.col-select');
        var btnEqActive = prevUpsell.find('.btn-price');
        // btnEqActive.eq(1).addClass('item-next-active');
        // var btnPriceNext = parentBtn.next().find('.btn-price.item-next-active');
        var btnPriceNext = parentBtn.next().find('.btn-price');
        if (btnPriceNext.length) {
            btnPriceActive.removeClass('active');
            btnPriceNext.addClass('active');
            btnPriceNext.trigger('click');
            var priceActive = $(this).closest('[data-hidden-recommended]').find('.btn-price.active');
            var textPriceActive = priceActive.find('.header-family').text();
            var priceNext = priceActive.find('.btn-price-cheapest-colum').text();
            var priceCurrent = $(this).closest('.upsell').find('.price-selected ').text();
            var minus = parseFloat(priceNext) - parseFloat(priceCurrent);
            $(this).closest('.upsell').find('.name-family').text(textPriceActive);
            if (btnPriceNext.length) {
                $(this).closest('.upsell').find('.price-sgd').text(" SGD " + minus.toLocaleString(undefined, {minimumFractionDigits: 2}));
            }
            $(this).closest('.upsell').find('.price-selected ').text(priceNext);
            hiddenBlockClick();
        }
    });
    
    $(document).on('click', '.button-group-1 > .button-change', function (e) {
        var btnChange = $(this);
        var wrapContentBlock = btnChange.closest('.wrap-content-fs');
        var buttonSelected = btnChange.closest('.recommended-flight-block').find('[data-trigger-animation].selected-item');
        
        wrapContentBlock.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
            if (buttonSelected.length) {
                buttonSelected.get(0).click();
            }
    
            wrapContentBlock.find('.select-fare-block.selected-item').addClass('active');
            if ($('.main-inner').find('.wrap-content-fs').length == 2) {
                wrapContentBlock.find('.upsell').addClass('hidden');
                wrapContentBlock.find('.change-flight-item.bgd-white').addClass('hidden');
                wrapContentBlock.find('.economy-slider , .fs-status-list , .monthly-view , .sub-logo , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
                wrapContentBlock.find('.col-select').find('.btn-price').removeClass('active');
    
                if (wrapContentBlock.index() === 0) {
                    wrapContentBlock.next().addClass('hidden');
                    wrapContentBlock.next().find('.col-select').find('.btn-price').removeClass('active');
    
                    // reset codeshare
                    hasCodeShare = false;
                } else {
                    $('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
                    $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
                }
                handleLoadmore(wrapContentBlock.find('.recommended-flight-block'), wrapContentBlock.find('[data-loadmore]'));
                showMoreLessDetails();
                handleActionSlider();
            }
            wrapContentBlock.find('.flight-list-item').attr('tabindex', 0);
            $('form.fare-summary-group').addClass('hidden');
    
            var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
            changeFlightItem.each(function () {
                if (!changeFlightItem.is(':visible')) {
                    $(this).removeClass('has-choose');
                } else {
                    $(this).addClass('has-choose');
                    $(this).parents('.recommended-flight-block').addClass('collapsed');
                }
            });
    
            if(btnChange.parents('.recommended-flight-block').hasClass('collapsed')) btnChange.parents('.recommended-flight-block').removeClass('collapsed');

            // $('html, body').scrollTop(wrapContent.offset().top - $('[data-booking-summary-panel]').innerHeight() - 20);

            setTimeout(function(){
                wrapContentBlock.off().css({
                    'opacity': 1
                });
            }, 400);
            
        });

        wrapContentBlock.css({
            'opacity': 0
        });

        if (wrapContentBlock.index() === 0) {
            wrapContentBlock.next().css({
                'opacity': 0
            });
        }
    });

    // show 2nd tab when click link right
    function showCorrelativeTab() {
        $('.table-fare-summary.hidden-mb-small').on('click', '.link-left', function () {
            $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
        });
        $('.table-fare-summary.hidden-mb-small').on('click', '.link-right', function () {
            $('.flight-search-summary-conditions').find('.tab-right').trigger('click');
        });
    }

    var workflowTable = function (isRenderTemplate) {
        tableLoadPage(isRenderTemplate);
        var priceTable = $('.recommended-table').find('.price');
        var flightListItem = $('.flight-list-item');
        var flightList = $('.wrap-content-fs').first().find('.flight-list-item');
        priceTable.each(function (idx) {
            var contentInner = $(this).text();
            if ($.trim(contentInner) === "") {
                var parentPrice = $(this).closest('.col-info-select');
                parentPrice.addClass('not-available');
                parentPrice.find('.flight-price').remove();
                parentPrice.find('span.not-available').empty().text('Not available');
            }
        });
        flightListItem.each(function (idx) {
            var flightStationItem = $(this).find('.flight-station-item');
            flightStationItem.each(function (idx) {
                if (!$(this).is(':last-child')) {
                    $(this).find('.less-detail').remove();
                }
            });
        });
    }
    var getTemplateFlightTable = function (el, data1, flightData, index, isFilter) {
        var loadmoreBlock = el.siblings('[data-loadmore]');
        var miniUrl = 'ajax/minifare-conditions.json';
        var upgradeUrl = 'ajax/fare-conditions-upgrade.json';

        if ($('body').hasClass('sk-ut-flight-search-a')) {
            miniUrl = 'ajax/minifare-conditions-a.json';
            upgradeUrl = 'ajax/fare-conditions-upgrade-a.json';
        } else if ($('body').hasClass('sk-ut-flight-search-b')) {
            miniUrl = 'ajax/minifare-conditions-b.json';
            upgradeUrl = 'ajax/fare-conditions-upgrade-b.json';
        }

        if ($('body').hasClass('fs-sk-mixed-rbd-biz')) {
            miniUrl = 'ajax/sk-minifare-conditions.json';
        }

        if ($('body').hasClass('fs-economy-rbd')) {
            miniUrl = 'ajax/sk-minifare-conditions-pey-biz-syd-maa-new.json';
        }
        if ($('body').hasClass('lcf-json')) {
            miniUrl = 'ajax/292-fareconditions.json';
        }
        if ($('body').hasClass('ccc-json')) {
            miniUrl = 'ajax/cheapest-cross-cabin-fare-conditions.json';
        }

        if ($('body').hasClass('mpxbbl-json')) {
            miniUrl = 'ajax/fs-sk-economy-multiple-pax-ff-conditions.json';
        }

        if (miniFareData) {
            $.get(global.config.url.fsEconomyFlightTable, function (data) {
                var template = window._.template(data, {
                    data: data1,
                    familyData: miniFareData,
                    flight: flightData ? flightData : data1.flights[index],
                    flightIdx: index
                });

                el.find('.flight-list-item').remove();
                el.append($(template));
                el.find('.flight-list-item:gt(4)').addClass('hidden');

                if (isFilter && index === 1) {
                    $(clickedBtn).trigger('click.calculatePriceInbound');
                }

                workflowTable(isFilter);

                selectFlightAnimation();
                showMoreLessDetails();
                initPopup();
                wcag();

                // init tooltip
                if ($('[data-tooltip]')) {
                    $('[data-tooltip]').kTooltip();
                }
                handleLoadmore(el, loadmoreBlock);

                var segmentsLength = el.find('.flight-list-item').length;

                if (!segmentsLength) {
                    el.addClass('hidden');
                    el.siblings('.no-result-filter').removeClass('hidden');
                } else {
                    el.siblings('.no-result-filter').addClass('hidden');
                    el.removeClass('hidden');
                }
            })
        } else {
            $.ajax({
                url: miniUrl,
                type: SIA.global.config.ajaxMethod,
                dataType: 'json',
                success: function (response) {
                    miniFareData = response
                    $.get(global.config.url.fsEconomyFlightTable, function (data) {
                        var template = window._.template(data, {
                            data: data1,
                            familyData: response,
                            flight: flightData ? flightData : data1.flights[index],
                            flightIdx: index
                        });

                        el.find('.flight-list-item').remove();
                        el.append($(template));
                        el.find('.flight-list-item:gt(4)').addClass('hidden');

                        if (isFilter && index === 1) {
                            $(clickedBtn).trigger('click.calculatePriceInbound');
                        }

                        workflowTable(isFilter);

                        selectFlightAnimation();
                        showMoreLessDetails();
                        initPopup();
                        wcag();

                        // init tooltip
                        if ($('[data-tooltip]')) {
                            $('[data-tooltip]').kTooltip();
                        }
                        handleLoadmore(el, loadmoreBlock);

                        var segmentsLength = el.find('.flight-list-item').length;

                        if (!segmentsLength) {
                            el.addClass('hidden');
                            el.siblings('.no-result-filter').removeClass('hidden');
                        } else {
                            el.siblings('.no-result-filter').addClass('hidden');
                            el.removeClass('hidden');
                        }
                    })
                }
            });
        }
    }

    var renderFlightTable = function (data1) {
        var flightBlock = $('.recommended-flight-block');

        flightBlock.each(function (index) {
            var self = $(this);
            if (index === 0) {
                getTemplateFlightTable(self, data1, null, index);
            } else {
                setTimeout(function () {
                    getTemplateFlightTable(self, data1, null, index);
                }, 2000);
            }
        });
    }
    var wcag = function () {
        var groupBtnFooter = $('.row-footer-select'),
            tableBlocks = $('.select-fare-block '),
            colFlightNotAvail = $('[data-trigger-animation].not-available'),
            btnUpgrade = $('input[name="btn-upgrade"]'),
            bookingSummaryPanel = $('.bsp-booking-summary');

        $(document).off('keydown.focusNext').on('keydown.focusNext', '.tooltip__close', function (e) {
            var code = e.keyCode || e.which;
            isSAGroup = $(this).siblings('.tooltip__content').find('[data-tooltip-sagroup]').length,
                tootipIdx = $('[data-tooltip-sagroup]').data('tooltip-sagroup');

            if (code === 13 && isSAGroup) {
                var input = $('input[name="codeshare-' + tootipIdx + '"]');
                setTimeout(function () {
                    input.focus();
                }, 100)
            }
        })

        colFlightNotAvail.attr('tabindex', -1);

        tableBlocks.each(function () {
            $(this).find('[data-tooltip], a').attr('tabindex', -1);
        })

        groupBtnFooter.each(function () {
            var btnPriceLast = $(this).find('.btn-price').last();

            btnPriceLast.off('keydown.tabToNextFlight').on('keydown.tabToNextFlight', function (e) {
                var code = e.keyCode || e.which,
                    colIdx = $(this).closest('.select-fare-block').data('col-index'),
                    nextCol = $('[data-trigger-animation="' + colIdx + '"]').next();

                if (nextCol.length && !nextCol.is('.not-available')) {
                    nextCol.attr('tabindex', 0);
                    setTimeout(function () {
                        nextCol.focus();
                    }, 100);
                }
            })
        });

        btnUpgrade.each(function () {
            $(this).off('keydown.tabToNextFlight').on('keydown.tabToNextFlight', function (e) {
                var code = e.keyCode || e.which,
                    colIdx = $(this).closest('.upsell').data('col-index'),
                    nextCol = $('[data-trigger-animation="' + colIdx + '"]').next();

                $(this).closest('.upsell').addClass('hidden');
                if (nextCol.length && !nextCol.is('.not-available')) {
                    nextCol.attr('tabindex', 0);
                    setTimeout(function () {
                        nextCol.focus();
                    }, 100);
                }
            })
        });

        var initTabBsp = function () {
            var btnBsp = bookingSummaryPanel.find('input.btn-1'),
                btnMoreDetails = bookingSummaryPanel.find('.more-detail'),
                btnLessDetails = bookingSummaryPanel.find('.less-detail'),
                linkTriggerPopup = bookingSummaryPanel.find('[data-trigger-popup]').eq(0),
                linkEditSearch = bookingSummaryPanel.find('.bsp-booking-summary__heading .search-link'),
                saysTableSummary = $('#table-summary__0');
            linkEditSearch.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function (e) {
                var code = e.keyCode || e.which || e.charCode;

                if (code === 9) {
                    e.preventDefault();
                    btnBsp.focus();
                }
            });
            btnBsp.each(function (index) {
                $(this).off('keydown.tabToNextContent').on('keydown.tabToNextContent', function (e) {
                    var code = e.keyCode || e.which || e.charCode;

                    if (code === 9) {
                        e.preventDefault();
                        if (index === 0) {
                            btnMoreDetails.focus();
                        } else {
                            linkTriggerPopup.focus();
                        }
                    }
                    if (e.shiftKey && code === 9) {
                        e.preventDefault();
                        linkEditSearch.addClass('focus-outline').focus();
                    }
                })
            });
            btnMoreDetails.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function (e) {
                var code = e.keyCode || e.which || e.charCode;

                if (code === 9) {
                    e.preventDefault();
                    saysTableSummary.addClass('focus-outline').focus();
                }
                if (e.shiftKey && code === 9) {
                    e.preventDefault();
                    btnBsp.eq(0).focus();
                }
            });
            linkTriggerPopup.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function (e) {
                var code = e.keyCode || e.which || e.charCode;

                if (e.shiftKey && code === 9) {
                    e.preventDefault();
                    btnBsp.eq(1).focus();
                }
            });
            btnLessDetails.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function (e) {
                var code = e.keyCode || e.which || e.charCode;

                if (code === 9) {
                    e.preventDefault();
                    saysTableSummary.addClass('focus-outline').focus();
                }
            });
        }

        ally.when.key({

            'ctrl+y': function () {

                $('input[name="select-inbound"]').focus();
                initTabBsp();

            },

        });
    }

    var resetFilter = function () {
        var btnReset = $('[data-reset-filter]');

        btnReset.each(function () {
            $(this).off('click.resetFilter').on('click.resetFilter', function (e) {
                e.preventDefault();
                var filterBlock = $(this).closest('.no-result-filter').siblings('[data-flight-filter]'),
                    listCheckbox = filterBlock.find('input[type="checkbox"]').not("[disabled]"),
                    listRangeSlider = filterBlock.find('[data-range-slider]'),
                    checkboxReset = filterBlock.find('input[type="checkbox"]:disabled');

                listCheckbox.each(function () {
                    $(this).prop('checked', false);
                })

                listRangeSlider.each(function () {
                    var min = $(this).data('min'),
                        max = $(this).data('max');
                    $(this).slider("option", "values", [min, max]);
                })

                checkboxReset.prop('checked', true).trigger('change.resetFilter');

            })
        })
    }

    var renderCombinationsJson = function () {
        var templateBookingPayment;
        var appendDiv;
        if (!$('body').hasClass('fs-economy')) {
            appendDiv = $('.combinations-json');
        } else {
            appendDiv = $('.fs-economy').find('.top-main-inner');
        }

        var combinationsJson = function (data1) {
            if (!$('body').hasClass('fs-economy')) {
                $.get(global.config.url.combinationsJsonTpl, function (data) {
                    var template = window._.template(data, {
                        data: data1
                    });
                    templateBookingPayment = $(template);
                    appendDiv.append(templateBookingPayment);
                });
            } else {
                $.get(global.config.url.fsEconomy, function (data) {
                    var filterArr = getFilterData(data1.flights);
                    var template = window._.template(data, {
                        data: data1,
                        filterArr: filterArr
                    });
                    templateBookingPayment = $(template);
                    appendDiv.append(templateBookingPayment);
                    renderFlightTable(data1);
                    showHideFilters();
                    handleActionSlider();
                    sliderRange();
                    filterFlights(data1);
                    resetFilter(data1.flights);
                    SIA.flightEconomySort();

                    var urlPage;
                    if ($('body').hasClass('sk-ut-flight-search-a')) {
                        urlPage = 'sk-ut-passenger-details-a.html';
                    } else if ($('body').hasClass('sk-ut-flight-search-b')) {
                        urlPage = 'sk-ut-passenger-details-b.html';
                    }

                    if (urlPage) {
                        $('form[name="flight-search-summary"]').attr('action', urlPage);
                    }
                });
            }
        };
        var url;
        if ($('body').hasClass('fs-economy-response-page')) {
            url = "ajax/flightsearch-response.json";
        }
        if ($('body').hasClass('fs-economy-response-new-page') || $('body').hasClass('fs-economy')) {
            url = "ajax/sin-sfo-30 JUNE-2017-Most-updated.json";
        }
        if ($('body').hasClass('sk-ut-flight-search-a')) {
            url = "ajax/sk-ut-flight-search-a.json";
        }
        if ($('body').hasClass('sk-ut-flight-search-b')) {
            url = "ajax/sk-ut-flight-search-b.json";
        }
        if ($('body').hasClass('fs-business')) {
            url = "ajax/sin-sfo-business-first.json";
        }
        if ($('body').hasClass('fs-economy-scoot')) {
            url = "ajax/sin-sfo-economy-scoot.json";
        }
        if ($('body').hasClass('fs-economy-two-column-premium-economy')) {
            url = "ajax/sin-sfo-tow-column-premium-economy.json";
        }
        if ($('body').hasClass('fs-economy-sin-maa-roundtrip-page')) {
            url = "ajax/flight-search-SIN-MAA-Roundtrip-2A1C1I.json";
        }
        if ($('body').hasClass('fs-economy-four-column')) {
            url = "ajax/sin-sfo-economy-four-column.json";
        }

        if ($('body').hasClass('fs-sk-mixed-rbd-biz')) {
            url = "ajax/sk-bus-first-syd-maa.json";
        }
        if ($('body').hasClass('fs-economy-rbd')) {
            url = "ajax/sk-pey-biz-syd-maa-new.json";
        }
        if ($('body').hasClass('lcf-json')) {
            url = "ajax/292.json";
        }
        if ($('body').hasClass('ccc-json')) {
            url = "ajax/cheapest-cross-cabin.json";
        }

        if ($('body').hasClass('mpxbbl-json')) {
            url = "ajax/fs-sk-economy-multiple-pax-results.json";
        }

        $.ajax({
            url: url,
            type: SIA.global.config.ajaxMethod,
            dataType: 'json',
            success: function (response) {
                var data1 = response.response;
                combinationsJson(data1);
            }
        });
    };

    var popup1 = $('.popup--flights-details-sf');
    var popup2 = $('.flight-search-summary-conditions');
    $(document).on('click', '.flights-details-sf', function (e) {
        e.preventDefault();
        // prototype
        if ($('body').hasClass('sk-ut-workflow')) {
            return false;
        }
        //
        popup1.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.trigger-summary-of-fare-condition', function (e) {
        e.preventDefault();
        if ($(this).hasClass('link-left')) {
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
        } else {
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').removeClass('active');
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').addClass('active');
        }
        popup2.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.popup__close', function (event) {
        popup1.Popup('hide');
        popup2.Popup('hide');
        event.preventDefault();
    });

    if ($('body').hasClass('fs-economy-page')) {
        renderCombinationsJson();
    }

    var urlPage;
    if ($('body').hasClass('sk-ut-flight-search-a')) {
        urlPage = 'sk-ut-passenger-details-a.html';
    } else if ($('body').hasClass('sk-ut-flight-search-b')) {
        urlPage = 'sk-ut-passenger-details-b.html';
    }

    if (urlPage) {
        document.cookie = 'priceData' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        $('body').hasClass('sk-ut-workflow') && $(document).off('submit.changePage').on('submit.changePage', 'form[name="flight-search-summary"]', function (e) {
            e.preventDefault()
            window.location.replace(urlPage);
        });
    }
};


SIA.flightEconomySort = function() {
    jQuery.fn.shift = [].shift;
    jQuery.fn.unshift = [].unshift;
    
    var global = SIA.global;
    var listEl =  [];

    var sortFunctionListener = function() {
        var flightSearchFilterEl = $(".flight-search-filter-economy");
        flightSearchFilterEl.each(function() {
            var self = $(this);
            var sortTriggers = self.find(".sort-filter").find("a");
            sortTriggers.on("click.sortTrigger", function(e) {
                e.preventDefault();
                changeActive(sortTriggers, $(this));
            });
        });
    };

    var cloneArray = function(clone) {
        return JSON.parse(JSON.stringify(clone));
    }

    var changeActive = function(triggers, triggered) {
        if (triggered.hasClass("sort-active")) {
            return;
        }
        var values = getListElements(triggered);
        var sortBySelected = triggered.text().toLowerCase();
        
        if (sortBySelected == "price") {
            values = sortPrice(values);
            values = sortDurationFn(values, ["price"])
            values = sortDepartureFn(values, ["price", "duration"]);
            values = sortArrivalFn(values, ["price", "duration", "departTimeString"]);
        } else if (sortBySelected == "travel duration") {
            values = sortDuration(values);
            values = sortPriceFn(values, ["duration"]);
            values = sortArrivalFn(values, ["duration", "price"]);
            values = sortDepartureFn(values, ["duration", "price", "arrivalTimeString"]);
        } else if (sortBySelected == "arrival time") {
            values = sortDepartureArrival(values, "arrivalTime", true);
            values = sortDurationFn(values, ["arrivalTimeString"]);
            values = sortPriceFn(values, ["arrivalTimeString", "duration"]);
            values = sortDepartureFn(values, ["arrivalTimeString", "duration", "departTime"]);
        } else if (sortBySelected == "departure time") {
            values = sortDepartureArrival(values, "departTime");
            values = sortDurationFn(values, ["departTimeString"]);
            values = sortPriceFn(values, ["departTimeString", "duration"]);
            values = sortArrivalFn(values, ["departTimeString", "duration", "price"]);
        }
        
        reArrangeElements(triggered, values);
        _.each(triggers, function(trigger) {
            trigger = $(trigger);
            if (trigger.is(triggered)) {
                trigger.addClass("sort-active");
            } else {
                if (trigger.hasClass("sort-active")) {
                    trigger.removeClass("sort-active");
                }
            }
        });
    };

    var findSameNext = function(list, criteria) {
        var idx = 0;
        var same = [list[idx]];
        while(list[++idx]) {
            var conditions = [];

            _.each(criteria, function(by) {
                conditions.push(list[idx][by] == same[0][by]);
            });

            if (conditions.indexOf(false) == -1) {
                same.push(list[idx]);
            } else {
                break;
            }
        }

        return same;
    };

    var checkIfElementHasHidden = function(elementList) {
        var hiddenCount = 0;
        elementList = $(elementList);
        elementList.each(function() {
            var $this = $(this);
            if ($this.hasClass("hidden")) {
                hiddenCount ++;
                $this.removeClass("hidden");
            }
        });
        return {
            count: hiddenCount
        }
    };

    var reArrangeElements = function(el, values) {
        var newElementArrangement = [];
        var parentWrapper = el.closest(".wrap-content-fs");
        var renderAfterChange = parentWrapper.find(".render-after");
        _.each(values, function(value) {
            newElementArrangement.push(listEl[value.$id]);
        });
        var result = checkIfElementHasHidden(newElementArrangement);
        newElementArrangement = $(newElementArrangement);
        for (var i = 1 ; i <= result.count ; i ++ ) {
            var idx = (newElementArrangement.length - i);
            $(newElementArrangement[idx]).addClass("hidden");
        }
        newElementArrangement.insertAfter(renderAfterChange);
    };

    var getListElements = function(el) {
        var parentWrapper = el.closest(".wrap-content-fs");
        listEl = parentWrapper.find(".flight-list-item");
        var data = [];
        listEl.each(function(idx) {
            var elData = scrapeValues($(this));
            elData.$id = idx;
            data.push(elData);
        });

        return data;
    };

    var scrapeValues = function(el) {
        var data = {
            prices: []
        };
        data.duration = el.find("[data-sort-duration]").data("sort-duration");
        el.find("[data-sort-price]").each(function() {
            var $this = $(this);
            var key = $this.data("sort-price").toLowerCase().replace(" ", "_");
            var priceValue = $this.text().trim().replace(",", "").replace(" ", "");
            data.prices.push(parseFloat(priceValue));
        });
        var legData = el.find("[data-sort-leg]");
        data.departTime = getDepartureArrivalTime(legData, "sort-departure");
        data.arrivalTime = getDepartureArrivalTime(legData, "sort-arrival");

        return data;
    };

    var sortPriceFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            multiSort.push(...sortPrice(batch));
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortDurationFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            multiSort.push(...sortDuration(batch));
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortDepartureFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            multiSort.push(...sortDepartureArrival(batch, "departTime"));
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortArrivalFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            multiSort.push(...sortDepartureArrival(batch, "arrivalTime", true));
            values.splice(0, batch.length);
        }

        return multiSort;
    } ;

    var sortPrice = function(list) {
        return _.chain(list)
                .map(function(item) {
                    item.price = _.sortBy(item.prices)[0];
                    return item;
                })
                .sortBy("price")
                .value();
    };

    var sortDuration = function(list) {
        return _.sortBy(list, "duration");
    };

    var sortDepartureArrival = function(list, by, isArrival) {
        var newKey = by + "String";
        
        return _.chain(list)
                .map(function(item) {
                    var timeHolder = item[by];
                    var index = isArrival ? timeHolder.length - 1 : 0;
                    // item[newKey] = [
                    //     ((timeHolder[index].hours <= 9) ? "0" : "") + timeHolder[index].hours, 
                    //     ((timeHolder[index].minutes <= 9) ? "0" : "") + timeHolder[index].minutes, 
                    //     ((timeHolder[index].seconds <= 9) ? "0" : "") + timeHolder[index].seconds
                    // ].join(":");
                    item[newKey] = new Date(timeHolder[index]);
                    return item
                })
                .sortBy(newKey)
                .value();
    };

    var getDepartureArrivalTime = function(el, by) {
        var timeArray = [];
        el.each(function() {
            var $this = $(this);
            var date = new Date($this.data(by));
            // timeArray.push({
            //     hours: date.getHours(), 
            //     minutes: date.getMinutes(), 
            //     seconds: date.getSeconds()
            // });
            timeArray.push(date);
        });
        return timeArray;
    };

    var init = function() {
        sortFunctionListener();
    };

    init();
};
//# sourceMappingURL=flight-search-sk.js.map
