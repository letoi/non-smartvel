var SIA = SIA || {};

SIA.whereWeFly = function () {
    var onHoverElements = [
        ".region-wrapper"
    ];
    var imageDefault = "images/map-land.png";
    var imageBGElement = $(".map-display");

    var init = function() {
        onHoverListener();
        backButtonListener();
        customTooltipListener();
    };

    var toggleBack = function(state) {
        var classIds = [
            ".map-back-button"
        ];

        $(classIds.join(","))[state ? "removeClass" : "addClass"]("hidden");
    };

    var customTooltipListener = function() {
        $(".custom-tooltip").find(".tooltip-header").off().on("click", function() {
            toggleTooltip(!$(".custom-tooltip").hasClass("active"));
        });
        toggleTooltip(false);
    };

    var openAccordion = function(code) {
        var accTrigger = $("[data-map-accordion='" + code + "']").find("[data-accordion-trigger]");
        if (!accTrigger.hasClass("active")) {
            accTrigger.trigger("click");
        }
    };

    var openDefaultAccordion = function(id) {
        var accTrigger = $("[data-map-accordion='"+ id +"']").find(".accordion__control");
        if (!id) {
            $("[data-map-accordion]").find(".accordion__control.active").trigger("click");
        } else if (!accTrigger.hasClass("active")) {
            accTrigger.trigger("click");
        }
    };

    var toggleTooltip = function(state) {
        $(".custom-tooltip").find(".content")[state ? "removeClass" : "addClass"]("hidden");
        $(".custom-tooltip")[state ? "addClass" : "removeClass"]("active");
    };
 
    var onHoverListener = function() {
        $(onHoverElements.join(",")).off()
            .on("mouseenter", function(evt) {
                imageBGElement.attr("src", $(this).data("map-image-hover"));
                $(this).find(".hover-span").removeClass("hidden");
            })
            .on("mouseleave", function() {
                imageBGElement.attr("src", imageDefault);
                hideAllHoverSpan();
            });
        onClickListener();
    };

    var hideAllHoverSpan = function() {
        $(".hover-span:not(.hidden)").each(function() {
            $(this).addClass("hidden");
        });
    };

    var backButtonListener = function() {
        $(".map-back-button").off().on("click", function(evt) {
            evt.preventDefault();
            imageBGElement.attr("src", imageDefault);
            onHoverListener();
            // toggleTooltip(false);
            toggleBack(false);
            openDefaultAccordion();
            $(".top-text").removeClass("hidden");
        });
    };

    var onClickListener = function() {
        $("[data-map-image-onclick]").on("click", function(evt) {
            var imageOnClickData = $(this).data("map-image-onclick");
            if (imageOnClickData) {
                imageBGElement.attr("src", imageOnClickData);
                $(".top-text").addClass("hidden");
                $(onHoverElements.join(",")).off("mouseenter").off("mouseleave");
                $("[data-map-image-onclick]").off("click");
                // toggleTooltip(true);
                openAccordion($(this).data("map-accordion-trigger"));
                toggleBack(true);
                hideAllHoverSpan();
            }
        });
    };

    return {
        init: init
    };
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.whereWeFly.init();
        }
    }, 100);
};

$(function() {
    waitForUnderscore();
});
