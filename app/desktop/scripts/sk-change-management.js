var SIA = SIA || {};

SIA.skChangeManagement = function() {
    var p = {};

    p.overlayChangeManagementEl = $('.overlay-change-management');
    p.skChangeManagementSliderEl = $('.sk-change-management-slider');
    p.bodySkChangeManagementEl = $('.sk-change-management');

    // device sizes
    p.tabletSize = 768;
    p.desktopSize = 1024;

    // tablet images
    p.tabletImages = {
        '2': 'images/T-img-2-slider.png',
        '3': 'images/T-img-3-slider.png',
        '4': 'images/T-img-4-slider.png',
        '5': 'images/T-img-5-slider.png'
    };

    // desktop images
    p.desktopImages = {
        '2': 'images/D-img-2-slider.png',
        '3': 'images/D-img-3-slider.png',
        '4': 'images/D-img-4-slider.png',
        '5': 'images/D-img-5-slider.png'
    };

    var init = function () {
        if (showOverlay()) {
             // remove scrolling on background
            setBackgroundScrolling('position', 'fixed');

            // show the overlay of change management
            p.overlayChangeManagementEl.removeClass('hidden');

            // change all images by default according to screen size
            if(window.innerWidth >= p.tabletSize && window.innerWidth <= (p.desktopSize - 1)){
                changeImagesPerSize(p.tabletSize);
            } else {
                changeImagesPerSize(p.desktopSize);
            }

            loadCloseBtnEvent();
            loadSlider();
            windowResizeEvent();
        }
    };

    var showOverlay = function() {
        var cookie = document.cookie.replace(/(?:(?:^|.*;\s*)showCoachMarks\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        var val = false;

        if (cookie === '') {
            val = true;
        }

        return val;
    };

    var disableOverlay = function () {
        // don't show this overlay again
        document.cookie = 'showCoachMarks=false';
    };

    var windowResizeEvent = function () {
        var windowEl = $(window);

        windowEl.resize(function(){
            changeImagesPerSize(windowEl.width());
        });
    };

    var changeImagesPerSize = function (size) {
        var imagesNum = 4;

        for (var i = 2; i <= imagesNum + 1; i++) {
            var el = $('.img-' + i + '-slider');
            var indexStr = i.toString();

            if (size == p.tabletSize) {
                el.attr('src', p.tabletImages[indexStr]);
            } else {
                el.attr('src', p.desktopImages[indexStr]);
            }
         }
    };

    var setBackgroundScrolling = function (properties, values) {
        p.bodySkChangeManagementEl.css(properties, values);
    };

    var loadCloseBtnEvent = function() {
        p.overlayChangeManagementEl.find('.close-btn').on('click', function(e) {
            e.preventDefault();
            p.overlayChangeManagementEl.addClass('hidden');

            // add scrolling on background
            setBackgroundScrolling('position', '');

            // don't show this overlay again
            disableOverlay();
        });
    };

    var loadSlider = function() {
        p.nextBtnEl = $('.next-btn');

        p.skChangeManagementSliderEl.slick({
            dots: true,
            speed: 600,
            prevArrow: '',
            nextArrow: '',
            draggable: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        mobileFirst: true,
                        variableWidth: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        mobileFirst: false,
                        variableWidth: true
                    }
                }
            ]
        });

        function changeElemetStyle(currentSlide, nextSlide, el, className) {
            el.removeClass(className + currentSlide);
            el.addClass(className + nextSlide);
        }

        function loadNextBtnEvents() {
            p.nextBtnEl.on('click', function() {
                p.skChangeManagementSliderEl.slick('slickNext');
            });
        }

		function hideSlickDots() {
			p.skChangeManagementSliderEl.find('.slick-dots').addClass('hide');
		}

        // add default style
        var firstSlide = p.skChangeManagementSliderEl.slick('slickCurrentSlide');
        if (firstSlide == 0) {
            // dots styles
            changeElemetStyle('', 0, $('.slick-dots'), 'slick-dots-');
        }

        // add on before change slider event
        p.skChangeManagementSliderEl.on({
            'beforeChange': function(event, slick, currentSlide, nextSlide){
                var currentSlideDot = p.skChangeManagementSliderEl.find('.slick-dots li');

    			// if (!currentSlideDot.hasClass('slick-active')) {
    			// 	hideSlickDots();
    			// }

                // change dots style
                changeElemetStyle(currentSlide, nextSlide, $('.sk-change-management-slider .slick-dots'), 'slick-dots-');
                $('.sk-change-management-slider .slick-dots').addClass('hide');

                // add last button "start booking"
                if (nextSlide == 4) {
                    $('.start-booking').off().on('click', function (e) {
                        e.preventDefault();
                        p.overlayChangeManagementEl.addClass('hidden');

                        // add scrolling on background
                        setBackgroundScrolling('position', '');

                        // don't show this overlay again
                        disableOverlay();
                    });
                }
            },
            'afterChange': function(event, slick, currentSlide){
                // setTimeout(function() {
            	// 	// show the slick dots
    	        //     // p.skChangeManagementSliderEl.find('.slick-dots').removeClass('hidden');
                // }, 100);
                $('.sk-change-management-slider .slick-dots').removeClass('hide');
            }
        });

		$(window).on('touchmove', function() {
			hideSlickDots();
		});

        // add the next button event
        loadNextBtnEvents();
    };

    var oPublic = {
        init: init,
        public: p
    };

    return oPublic;
}();

$(function() {
    // wait for overlay loading to be finish
    var waitForOverlayLoading = function() {
       setTimeout(function() {
           if (! $('.overlay-loading').hasClass('hidden')) {
               waitForOverlayLoading();
           } else {
                SIA.skChangeManagement.init();
           }
       }, 100);
    };

    $('.overlay-loading').hasClass('hidden') ? SIA.skChangeManagement.init() : waitForOverlayLoading();
});
