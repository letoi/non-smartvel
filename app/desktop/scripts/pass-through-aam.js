var SIA = SIA || {};
SIA.PassThroughAAM = function () {
    var global = SIA.global;
    var promotionObject = {};
    promotionObject.json = globalJson.promotionAamData;
    promotionObject.passThroughPromotionDetailTpl = '<div class="promotion-item__inner">\
    <div class="promotion-item__content">\
     <h4>Promotion details</h4>\
     <table class="promotion-details-table">\
      <tbody>\
       <tr>\
        <td data-th="">Travel duration</td>\
        <td data-th=""><%- travelDurationStart %> — <%- travelDurationEnd %></td>\
       </tr>\
       <tr>\
        <td data-th="">Book by</td>\
        <td data-th=""><%- bookBy %></td>\
       </tr>\
       <tr class="promo-detail border-bottom table-row-3">\
        <td data-th="">Advance purchase</td>\
        <td data-th="" class="promo-detail-advance-purchase"></td>\
       </tr>\
       <tr class="promo-detail table-row-4">\
        <td data-th="">Minimum stay</td>\
        <td data-th=""><%- minStay %> <%- minimumStayType %></td>\
       </tr>\
       <tr class="promo-detail border-bottom table-row-5">\
        <td data-th="">Maximum stay</td>\
        <td data-th=""><%- maxStay %> <%- maximumStayType %></td>\
       </tr>\
       <tr class="promo-detail table-row-6">\
        <td data-th="">Minimum passengers</td>\
        <td data-th=""><%- minPaxNo %> passengers</td>\
       </tr>\
       <tr>\
        <td data-th="">Cabin classes</td>\
        <td data-th=""><%- cabinClass %></td>\
       </tr>\
       <tr>\
        <td data-th="">Eligibility</td>\
        <td data-th=""><%- eligibility %></td>\
       </tr>\
       <tr>\
        <td data-th="">Destinations</td>\
        <td data-th=""><%- destinationCityName %></td>\
       </tr>\
       <tr class="view-full-fare-condition">\
        <td>\
         <a href="#accordion--fare-conditions" class="link-4 accordion--fare-conditions-trigger">\
          <em class="ico-point-r"></em>View full fare conditions</a>\
        </td>\
       </tr>\
      </tbody>\
     </table>\
    </div>\
   </div>';

    promotionObject.passThroughFareDealBannerTpl = '<div data-tablet-bg="30% top" class="full-banner--img fare-deal-banner-img"><img class="bannerImg" src="" alt="Faredeal Banner" longdesc="img-desc.html"/>\
   </div>\
   <div class="promo-code-top-desc-main">\
     <div class="applied-promo-code">\
       <div class="applied-promo-code-inner">\
       <div class="ico-check-container">\
         <span class="ico-check-thick"></span>\
       </div>\
         <span class="promo-code">‘<%- promoCode %>’</span>\
       <span>promo code applied</span>\
     </div>\
     </div>\
     <div class="promo-off-fares">\
       <h1><%- AAMbannerTitle %></h1>\
     </div>\
     <div class="promo-off-fares-description">\
       <p class="aam-banner-desc"><%- AAMbannerdesc %></p>\
     </div>\
   </div>';

    promotionObject.passThroughFareConditionsListTpl = '<li class="fare-condition-list"><div class="list-title">Valid flights</div>\
   <ul class="fare-conditions-unordered-list">Availability of this fare deal is based on flight departure times.\
     <li>\
       <span>Valid outbound flights:</span> <%- outboundFlights %></li>\
     <li>\
       <span>Valid inbound flights:</span> <%- inboundFlights %></li>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Travel period</div>\
   <ul class="fare-conditions-unordered-list">\
     <li>\
       <span>Outbound travel period:</span> <%- startDate %> to <%- endDate %></li>\
     <li>\
       <span>Travel completion date:</span> <%- travelCompletionDate %></li>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">KrisFlyer</div>\
   <ul>\
     <% _.each(mileAccruableDesc, function(item, idx) { %>\
      <li><%- item %></li>\
     <% }) %>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Itinerary change</div>\
   <p><%- itineraryChangeDesc %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Cancellation / Refund</div>\
   <p><%- cancellationOrRefundDesc %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Other conditions</div>\
  <p class="other-conditions"><%- otherconditions %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Special conditions</div>\
   <p><%- specialConditions %></p>\
  </li>';

    var init = function () {
        renderTPLPromoDetailFareDealBanner();
        renderTPLFareConditions();
        accordionAnchorTrigger();
        $('[name="search-fl-city-2"]').attr('disabled',true);
        renderNewOriginDestinationList($('#flight-1'),$('#flight-2'),promotionObject.json.fareDeals.origin);
        renderCabinClassList($('#cabin-1'),promotionObject.json.fareDeals.cabinClass.split(","));
        console.log($('[name="search-fl-city-1"]').val());
        $('[name="search-fl-city-1"]').off('blur').on('blur',function(){
            setTimeout(function(){
                if($('[name="search-fl-city-1"]').val().length){
                    $('[name="search-fl-city-2"]').attr('disabled',false);
                }else{
                    $('[name="search-fl-city-2"]').val('');
                    $('[name="search-fl-city-2"]').closest('.select__text').find('.select__text_styled').addClass('hidden');
                    $('[name="search-fl-city-2"]').focus();
                    $('[name="search-fl-city-2"]').attr('disabled',true);
                }
            },500);
        });

        initPassengerCount();
        $('.passenger_count_span').closest('div.custom-select--2').click(function(e){
            e.stopPropagation();
            var icon = $(this).closest('div.custom-select--2').find('span.up-down-point-trigger');
            $('.passengers-booking-stepper-container').hasClass('hidden') ? icon.removeClass('ico-point-d').addClass('ico-point-u') : icon.addClass('ico-point-d').removeClass('ico-point-u');
            $('.passengers-booking-stepper-container').hasClass('hidden') ? $('.passengers-booking-stepper-container').removeClass('hidden') : $('.passengers-booking-stepper-container').addClass('hidden');
            $('.passengers-booking-stepper-container').hasClass('hidden') ? $('.passenger_count_span').parent().removeClass('focus') : $('.passenger_count_span').parent().addClass('focus');
        });


        // $('.passenger_count_span').closest('div.custom-select--2').find('span.ico-point-d').click(function(e){
        //     e.stopPropagation();
        //     $('.passenger_count_span').trigger('click');
        // });
        
        $(window).click(function() {
            if(!$('.passengers-booking-stepper-container').hasClass('hidden')){
                $('.passenger_count_span').parent().removeClass('focus');
                $('.passengers-booking-stepper-container').addClass('hidden');
                $('.passenger_count_span').closest('div.custom-select--2').find('span.up-down-point-trigger').addClass('ico-point-d').removeClass('ico-point-u');
            }
        });
        $('[name="search-fl-city-1"]').on('focusin',function(){
            downArrowEvent($('[name="search-fl-city-1"]'),$('ul#ui-id-4'),0);
        });
        
        $('[name="search-fl-city-2"]').on('focusin',function(){
            downArrowEvent($('[name="search-fl-city-2"]'),$('ul#ui-id-5'),0);
        });

        

    };

    var downArrowEvent = function(element,ul,counter){
        element.on('keydown',function(e){
            if(e.keyCode == 40){
                if(counter >= ul.find('li').length){
                    $(ul.find('li.drop-down-hover-select')).removeClass('drop-down-hover-select');
                    counter = 0;
                }else{
                    $(ul.find('li.drop-down-hover-select')).removeClass('drop-down-hover-select');
                    $(ul.find('li')[counter]).addClass('drop-down-hover-select');
                    counter = counter + 1;
                }
            }else if(e.keyCode == 38){
                if(counter < 0){
                    $(ul.find('li.drop-down-hover-select')).removeClass('drop-down-hover-select');
                    counter = ul.find('li').length;
                }else if(counter == 0){
                    $(ul.find('li.drop-down-hover-select')).removeClass('drop-down-hover-select');
                    counter = ul.find('li').length;
                    $(ul.find('li')[counter-1]).addClass('drop-down-hover-select');
                }else{
                    counter = counter - 1;
                    $(ul.find('li.drop-down-hover-select')).removeClass('drop-down-hover-select');
                    $(ul.find('li')[counter-1]).addClass('drop-down-hover-select');
                }
            }
        });
    }

    var initPassengerCount = function(){

        var passengerCountContainer = $('.passengers-booking-stepper-container');
        var hiddenInput = $('.passengers-input-field');
        var passengerCountInputs = [];
        var inputs = [];
        var plus = [];
        var minus = [];
        setTimeout(function(){
            var passengerCount = getCabinClassPassengerCount();
            // console.log(passengerCount);
            $('.passenger_count_span').text(promotionObject.json.fareDeals.minPaxNo+" Adult");
            $('input.passengers-input-field').val($('.passenger_count_span').text());
            addEventHandler(passengerCountContainer.find('.adult-booking-stepper'),"Adult",passengerCountInputs, inputs,plus,minus, passengerCount);
            addEventHandler(passengerCountContainer.find('.children-booking-stepper'),"Children",passengerCountInputs, inputs,plus,minus, passengerCount);
            addEventHandler(passengerCountContainer.find('.infants-booking-stepper'),"Infant",passengerCountInputs, inputs,plus,minus, passengerCount);
            $('#cabin-1').off('blur').on('blur',function(){
                console.log('im here');
                initPassengerCount();
            })
        },800);
    };

    var getCabinClassPassengerCount = function(){
        var valueString = $('#cabin-1').closest('[data-customselect="true"]').find('span.select__text').text().trim();
        var countObject = [0,0,0];    
            if(valueString == "Economy"){
                countObject[0] = 9;
            }else if(valueString == "First/Suites"){
                countObject[0] = 4;
            }else{
                countObject[0] = 6;
            }
            countObject[1] = parseInt(promotionObject.json.fareDeals.minPaxNo)*5;
            countObject[2] = parseInt(promotionObject.json.fareDeals.minPaxNo)*1;
        return countObject;
    }

    var addEventHandler = function(container, generation,passengerCountInputs,inputs,pluses,minuses,passengerCount){
        var minus = container.find(".stepper-button-minus").length ? container.find(".stepper-button-minus") : container.find(".disable-minus-btn");
        var plus  = container.find(".stepper-button-plus").length ? container.find(".stepper-button-plus") : container.find(".disable-plus-btn") ;
        var input = container.find(".passenger-stepper-input");
        generation == "Adult" ? input.val(promotionObject.json.fareDeals.minPaxNo) : input.val(0);
        passengerCountInputs.push({age: generation, input: input});
        inputs.push(input);
        pluses.push(plus);
        minuses.push(minus);

        removeClassDisable(minus);
        removeClassDisable(plus);

        if(input.val() == 0){
            changeButtonClass(minus,'minus');
        }


        minus.off().on('click',function(e){
            e.stopPropagation();
            if(input.val() > 0){
                if(inputs.indexOf(input) == 0){
                    if(input.val() == 1){
                        return;
                    }else if(input.val() > 1){
                        updatePassengerCount(input,"minus");
                        adjustCount(input, inputs, passengerCount);
                        if(input.val() == 1 && $(minus).hasClass('stepper-button-minus')){
                            changeButtonClass($(minus),'minus');
                        }
                        if(getAllInputsValue(passengerCountInputs) < passengerCount[0]){
                            $(pluses).each(function(index){
                                if($(this).hasClass('disable-plus-btn') && $(inputs[index]).val() < passengerCount[index] ||
                                    $(this).hasClass('stepper-button-plus') && $(inputs[index]).val() == passengerCount[index] ){
                                    changeButtonClass($(this),'plus');
                                }
                            });
                        }
                        return;
                    }
                }else{
                    updatePassengerCount(input,"minus");
                    if(input.val() == 0 && $(minus).hasClass('stepper-button-minus')){
                        changeButtonClass($(minus),'minus');
                    }
                    if(getAllInputsValue(passengerCountInputs) < passengerCount[0]){
                        $(pluses).each(function(index){
                            if($(this).hasClass('disable-plus-btn') && $(inputs[index]).val() < passengerCount[index]){
                                changeButtonClass($(this),'plus');
                            }
                            
                        });
                    }
                }
            }
        });

        plus.off().on('click',function(e){
            e.stopPropagation();
            if(input.val() < passengerCount[inputs.indexOf(input)] && passengerCount[0] != getAllInputsValue(passengerCountInputs)){
                updatePassengerCount(input,"plus");
                if(input.val() == passengerCount[inputs.indexOf(input)]){
                    if(plus.hasClass('stepper-button-plus')){
                        changeButtonClass(plus,'plus');
                    }
                }
                if(input.val() > 0){
                    if(minus.hasClass('disable-minus-btn')){
                        changeButtonClass(minus,'minus');
                    }
                }
                if(inputs.indexOf(input) == 0){
                    passengerCount[1] = input.val() * 5;
                    passengerCount[2] = input.val() * 1;
                    if(getAllInputsValue(passengerCountInputs) < passengerCount[0]){
                        $(pluses).each(function(index){
                            if($(this).hasClass('disable-plus-btn') && $(inputs[index]).val() < passengerCount[index]){
                                changeButtonClass($(this),'plus');
                            }
                            
                        });
                    }
                }
                if(getAllInputsValue(passengerCountInputs) == passengerCount[0]){
                    $(pluses).each(function(){
                        if($(this).hasClass('stepper-button-plus')){
                            changeButtonClass($(this),'plus');
                        }
                    });
                }
            }
        });

        input.off('click').on('click',function(e){
            e.stopPropagation();
        });

        input.off('keyup').on('keyup',_.debounce(function(e){
            e.stopPropagation();
            if($(this).val() == "" || $(this).val() <= 0 || $(this).val() > passengerCount[inputs.indexOf(input)]){
                if(inputs.indexOf(input) == 0){
                    $(this).val(1);
                    adjustCount(input, inputs, passengerCount);
                    if($(plus).hasClass('disable-plus-btn')){
                        changeButtonClass($(plus),'plus');
                    }
                }else{
                    $(this).val(0);
                    $(pluses).each(function(index){
                        if($(this).hasClass('disable-plus-btn') && $(inputs[index]).val() < passengerCount[index]){
                            changeButtonClass($(this),'plus');
                        }
                    });
                }
                if($(minus).hasClass('stepper-button-minus')){
                    changeButtonClass($(minus),'minus');
                }
                getAllInputsValue(passengerCountInputs);
            }else{
                if(passengerCount[0] < getAllInputsValue(passengerCountInputs)){
                    if(inputs.indexOf(input) == 0){
                        $(this).val(1);
                        if($(minus).hasClass('disable-plus-btn')){
                            changeButtonClass($(minus),'plus');
                        }
                        if($(minus).hasClass('stepper-button-minus')){
                            changeButtonClass($(minus),'minus');
                        }
                        adjustCount(input, inputs, passengerCount);
                    }else{
                        $(this).val(0);
                        if($(plus).hasClass('disable-plus-btn')){
                            changeButtonClass($(plus),'plus');
                        }
                        if($(minus).hasClass('stepper-button-minus')){
                            changeButtonClass($(minus),'minus');
                        }
                    }
                }else{
                    console.log("im here");
                    if($(minus).hasClass('disable-minus-btn')){
                        changeButtonClass($(minus),'minus');
                    }
                    if(getAllInputsValue(passengerCountInputs) == passengerCount[0]){
                        $(pluses).each(function(){
                            if($(this).hasClass('stepper-button-plus')){
                                changeButtonClass($(this),'plus');
                            }
                        });
                    }else if(inputs.indexOf(input) == 0){
                        adjustCount(input, inputs, passengerCount);
                        if($(this).val() == passengerCount[inputs.indexOf(input)]){
                            if($(plus).hasClass('stepper-button-plus')){
                                changeButtonClass($(plus),'plus');
                            }
                        }else{
                            $(pluses).each(function(index){
                                if($(this).hasClass('disable-plus-btn') && $(inputs[index]).val() < passengerCount[index]){
                                    changeButtonClass($(this),'plus');
                                }
                            });
                        }
                    }else{
                        if($(plus).hasClass('disable-plus-btn') && $(this).val() < passengerCount[inputs.indexOf(input)]){
                            changeButtonClass($(plus),'plus');
                        }else if($(plus).hasClass('stepper-button-plus') && $(this).val() == passengerCount[inputs.indexOf(input)]){
                            changeButtonClass($(plus),'plus');
                        }
                    }
                }
                getAllInputsValue(passengerCountInputs);
            }
           
        },1000));
    }

    var removeClassDisable = function(item){
        $(item).hasClass('disable-plus-btn') ? $(item).removeClass('disable-plus-btn').addClass('stepper-button-plus') : "";
        $(item).hasClass('disable-minus-btn') ? $(item).removeClass('disable-minus-btn').addClass('stepper-button-minus') : "";
    }

    var changeButtonClass = function(btn,type){
        if(type == 'plus'){
            $(btn).hasClass('stepper-button-plus') ? $(btn).removeClass('stepper-button-plus').addClass('disable-plus-btn') : $(btn).removeClass('disable-plus-btn').addClass('stepper-button-plus');
        }else{
            $(btn).hasClass('stepper-button-minus') ? $(btn).removeClass('stepper-button-minus').addClass('disable-minus-btn') : $(btn).removeClass('disable-minus-btn').addClass('stepper-button-minus');
        }
        
    };

    var adjustCount = function(input, inputs, passengerCount){
        passengerCount[1] = input.val() * 5;
        passengerCount[2] = input.val() * 1;
        if(inputs[1].val() > passengerCount[1]){
            var value = parseInt(inputs[1].val());
            inputs[1].val(value - (value-passengerCount[1]));
        }
        if(inputs[2].val() > passengerCount[2]){
            var value = parseInt(inputs[2].val());
            inputs[2].val(value - (value-passengerCount[2]));
        }
    };

    var updatePassengerCount = function(input,operation){
        input.val(operation == "minus" ? parseInt(input.val())-1 : parseInt(input.val())+1);
    }

    var getAllInputsValue = function(collection){
        var stringToShow = "";
        var counter = 0;
        _.each(collection, function(item){
            if(parseInt(item.input.val()) > 0){
                counter = counter+parseInt(item.input.val());
                if(stringToShow == ""){
                    stringToShow = stringToShow+item.input.val()+" "+item.age;
                }else{
                    stringToShow = stringToShow+" , "+item.input.val()+" "+item.age;
                }
                
            }
        });
        if(counter < parseInt(promotionObject.json.fareDeals.minPaxNo)){
            console.log("you are below min pax");
        }
        $('.passenger_count_span').text(stringToShow);
        $('input.passengers-input-field').val(stringToShow);
        return counter;
    }

    var renderNewOriginDestinationList = function(originSelect, destinationSelect,collection){
        _.each(collection,function(locationFrom){ 
            originSelect.append('<option value="'+locationFrom.originCode+'" data-text="'+locationFrom.originAirportName+' - '+locationFrom.originCode+'" data-country="AT" data-country-exclusion="AU">'+locationFrom.originCityName+'*'+locationFrom.originCountryName+'*'+locationFrom.originAirportName+'*'+locationFrom.originCode+'</option>');
            renderNewDestinationList(destinationSelect,locationFrom.destination);
        });
    }

    var renderNewDestinationList = function(selectComponent, collection){
        _.each(collection,function(locationFrom){ 
            selectComponent.append('<option value="'+locationFrom.destinationCode+'" data-text="'+locationFrom.destinationAirportName+' - '+locationFrom.destinationCode+'" data-country="AT" data-country-exclusion="AU">'+locationFrom.destinationCityName+'*'+locationFrom.destinationCountryName+'*'+locationFrom.destinationAirportName+'*'+locationFrom.destinationCode+'</option>');
        });
    }

    var renderCabinClassList = function(selectComponent, collection){
        _.each(collection,function(cabin){ 
            selectComponent.append('<option value="'+cabin+'">'+cabin+'</option>');
        });
    }

    var renderTPLPromoDetailFareDealBanner = function () {
        var contentBodyPassThroughPromoDetail = $(".pass-through-promotion-detail-container");
        var contentBodyPassThroughFareDealBanner = $(".fare-deal-banner-container");
        var fareDeals = promotionObject.json.fareDeals;
        var promoDestinationArray = [];
        // var promoData;
        // _.each(promotionObject.json, function (promo) {
        // promoData = promo;
        _.each(fareDeals.origin, function (originName) {
            promoDestinationArray.push(_.map(originName.destination, function (destinationNameParam) {
                return destinationNameParam.destinationCityName;
            }));
        });
        // });
        var destinationNames = promoDestinationArray.toString().split(",").join(", ");
        var passThroughPromotionDetailTplRendered = $(_.template(promotionObject.passThroughPromotionDetailTpl, {
            'travelDurationStart': fareDeals.travelDurationStart,
            'travelDurationEnd': fareDeals.travelDurationEnd,
            'bookBy': fareDeals.bookBy,
            'advancePurchase': fareDeals.advancePurchase,
            'minStay': fareDeals.minStay,
            'minimumStayType': fareDeals.minimumStayType,
            'maxStay': fareDeals.maxStay,
            'maximumStayType': fareDeals.maximumStayType,
            'minPaxNo': fareDeals.minPaxNo,
            'cabinClass': fareDeals.cabinClass,
            'eligibility': fareDeals.eligibility,
            'destinationCityName': destinationNames
        }));
        contentBodyPassThroughPromoDetail.append(passThroughPromotionDetailTplRendered);

        var passThroughFareDealBannerTplRendered = $(_.template(promotionObject.passThroughFareDealBannerTpl, {
            'AAMbannerImage': fareDeals.AAMbannerImage,
            'promoCode': fareDeals.promoCode,
            'AAMbannerTitle': fareDeals.AAMbannerTitle,
            'AAMbannerdesc': fareDeals.AAMbannerdesc
        }));
        contentBodyPassThroughFareDealBanner.append(passThroughFareDealBannerTplRendered);
        $('img.bannerImg').attr('src',fareDeals.AAMbannerImage);
        $('p.aam-banner-desc').html(fareDeals.AAMbannerdesc.replace(/[0-9][0-9]%|[0-9]%/,'<span class="bold-text">' + fareDeals.AAMbannerdesc.match(/[0-9][0-9]%|[0-9]%/)[0] + '</span>'));
    };

    var renderTPLFareConditions = function () {
        var contentBodyPassThroughFareConditionsList = $('.fare-condition-ordered-list-main');
        var fareDeals = promotionObject.json.fareDeals;
        var outbound = fareDeals.outboundDetails;
        var inbound = fareDeals.inboundDetails;
        var fareDealsConditions = fareDeals.conditions;
        var isUpgradable = (fareDealsConditions.mileageUpgradable == "yes") ? true : false;
        var mileAccruableDesc = fareDealsConditions.mileAccruableDesc.split(",").map(function(i) { return i.trim() });
        var outboundDates = outbound.outboundDates;

        if (!isUpgradable) {
          mileAccruableDesc = [mileAccruableDesc.shift()];
        }

        var passThroughFareConditionsListTplTplRendered = $(_.template(promotionObject.passThroughFareConditionsListTpl, {
            'outboundFlights': outbound.outboundFlights,
            'inboundFlights': inbound.inboundFlights,
            'startDate': outboundDates.startDate,
            'endDate': outboundDates.endDate,
            'travelCompletionDate': fareDeals.travelCompletionDate,
            'mileAccruableDesc': mileAccruableDesc,
            'cancellationOrRefundDesc': fareDealsConditions.cancellationOrRefundDesc,
            'specialConditions': fareDealsConditions.specialConditions,
            'itineraryChangeDesc': fareDealsConditions.itineraryChangeDesc,
            'otherconditions': fareDealsConditions.otherconditions
        }));
        contentBodyPassThroughFareConditionsList.append(passThroughFareConditionsListTplTplRendered);
        $('p.other-conditions').html(fareDealsConditions.otherconditions.replace('Note:','<span class="bold-text">Note:</span>'));
        $('.promo-detail-advance-purchase').text(fareDeals.advancePurchase + " days");
    };

    var accordionAnchorTrigger = function() {
        $("a.accordion--fare-conditions-trigger").off().on("click", function(evt) {
            var accordion = $("#accordion--fare-conditions").find("[data-accordion-trigger]");
            if (!accordion.hasClass("active")) {
                evt.preventDefault();
                accordion.trigger("click");
            }
        });
    };

    var pub = {
        init: init
    };

    return pub;
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.PassThroughAAM.init();
        }
    }, 100);
};

$(function () {
    typeof _ == 'undefined' ? waitForUnderscore() : SIA.PassThroughAAM.init();
});
