var SIA = SIA || {};
/*
** To use, include before the head and in the call for jsmod and dashboard
    <script>
        var jsdashboard = [
                'seatMap'
            ];
        var jsmod = {
            common: ['underscore', 'url-param-parser.js', 'accounting', 'seatmap'],
            d: [],
            t: [],
            m: [],
            o: []
        };
    </script>

    Then check for the variable to replace in your code:
        if(SIA.URLParamParser && SIA.URLParamParser.getURLParams('miniUrl')) miniUrl = 'ajax/'+ SIA.URLParamParser.getURLParams('miniUrl') + '.json';


*/
SIA.URLParamParser = function() {
    var html = document.getElementsByTagName('html');
    var className = 'ie8';
    var urlParams = new URLSearchParams(window.location.search);
    
    function getURLParams(name) {
        return urlParams.get(name);
    };
    
    var pub = {
        getURLParams: getURLParams
	};
    
	return pub;
}();