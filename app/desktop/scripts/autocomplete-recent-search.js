
SIA.recentSearch = function() {
    var observeForms = [
        "#book-flight",
        "#book-redem",
        "#form-book-travel",
        "#form-book-travel-1"
    ];
    var excludeForms = [
        'form-book-travel-1',
        'book-redem',
    ];

    var saveRecentSearch = function(formEl) {
        var searches = formEl.find("[data-recent-search]");
        var validator = [
            !!$("[data-recent-search='from']").val(),
            !!$("[data-recent-search='to']").val(),
            !!$("[data-recent-search='depart']").val(),
            !!$("[data-recent-search='return']").val()
        ];

        var searchObject = {};
        _.each(searches, function(search) {
            var self = $(search);
            if (self.attr("type") == "checkbox" || self.attr("type") == "radio") {
                searchObject[self.data("recent-search")] = self.is(":checked");
            } else {
                searchObject[self.data("recent-search")] = self.val();
            }
            
        });
        
        var recentSearchResult = getLocalStorage("fs_history");
        if (searchObject.oneWay) validator.pop();
        if (validator.indexOf(false) != -1) return;        
        searchObject.id = (new Date().valueOf());
        if(searchObject.oneWay) searchObject["return"] = "";

        recentSearchResult.push(searchObject);
        if (recentSearchResult.length > 3) {
            recentSearchResult.shift();
        }
        
        setLocalStorage("fs_history", recentSearchResult);
        // location.href = "fs-sk-economy-lcf.html";
        $("[data-recent-search-trigger]").off();
        formEl.submit();
    };

    var getFormValues = function (searches) {
        var searchObject = {};
        $.each(searches, function(idx, search) {
            var self = $(search);
            if (self.attr("type") == "checkbox" || self.attr("type") == "radio") {
                searchObject[self.data("recent-search")] = self.is(":checked");
            } else {
                searchObject[self.data("recent-search")] = self.val();
            }
            
        });

        return searchObject;
    };

    var getFlightForms = function () {
        var copyFormValues;
        $.each(observeForms, function(idx, value) {
            var formEl = $(value);
            dataElements = formEl.find("[data-recent-search]");
            var dataValues = getFormValues(dataElements);
            if (!formEl.hasClass("active") && !copyFormValues) {
                copyFormValues = dataValues;
            }
        });
        
        $("[data-recent-search-trigger]").trigger("selectAutoSearch", copyFormValues);
    };

    $("[data-recent-search-trigger]")
        .on("submit", function(evt) {
            evt.preventDefault();
            var self = $(this);
            if (excludeForms.indexOf(self.attr("id")) != -1) {
                $("[data-recent-search-trigger]").off();
                self.submit();
                return;
            }

            saveRecentSearch(self);
        })
        .on("selectAutoSearch", function(evt, data) {
            delete data.id;
            for(var key in data) {
                var includeKey = [
                    "to"
                ];
                var recentSearchData = $(this).find("[data-recent-search='" + key + "']");
                var attrType = recentSearchData.attr("type");
                recentSearchData.val(data[key]);

                if (includeKey.indexOf(key) != -1) {
                    recentSearchData.closest("[data-autocomplete-od]").removeClass("default");
                    var appendTo = recentSearchData.closest(".select__text");
                    if (recentSearchData.val()) {
                        $(this).trigger("createClearCloseBtn", recentSearchData);
                    } else {
                        appendTo.siblings(".ico-cancel-thin.optim-close").remove();
                    }
                }

                if (recentSearchData.prop("tagName") == "SELECT") {
                    recentSearchData.siblings(".select__text").html(recentSearchData.find(":selected").text());
                    var comboBoxAttr = recentSearchData.siblings("input").attr("id").match(/\w+-[0-9]*/)[0];
                    var listAllBox = $("#" + comboBoxAttr + "-listbox").find("li");
                    var listBox = $("#" + comboBoxAttr + "-listbox").find("[data-value='" + data[key] + "']");
                    
                    $(listAllBox).each(function() {
                        var self = $(this);
                        if (!self.is(listBox)) {
                            self.removeClass("active");
                            self.attr("aria-selected", "false");
                        } else {
                            self.addClass("active");
                            self.attr("aria-selected", "true");
                        }
                    });
                } else if (attrType == "checkbox" || attrType == "radio") {
                    recentSearchData.prop("checked", data[key]);
                }
            }

            $("[data-flight-type='twoWay']")[(data.twoWay) ? "removeClass" : "addClass" ]("hidden");
            $("[data-flight-type='oneWay']")[(data.oneWay) ? "removeClass" : "addClass" ]("hidden");
        })
        .on("clearTo", function() {
            var inputField = $(this).find("[data-recent-search='to']");
            inputField.val("");
            inputField.closest(".select__text").siblings(".optim-close").addClass("hidden");
        });

    $(document).on("mousedown", ".remove-recent", function() {
        $("[data-recent-search]").trigger("isDeleteClicked");
        var id = $(this).closest(".autocomplete-item").data("value");
        removeLocalStorage("fs_history", id);
        $(this).closest(".autocomplete-item").remove();
        return false;
    });

    function removeLocalStorage(key, id) {
        var items = JSON.parse(window.localStorage.getItem(key));
        items = items.filter(function(i) {
            if (i.id != id) return i;
        });
        setLocalStorage(key, items);
    }

    function setLocalStorage(key, data) {
        window.localStorage.setItem(key, JSON.stringify(data));
    }

    function getLocalStorage(key) {
        var items = JSON.parse(window.localStorage.getItem(key));
        return items ? items : [];
    }

    function trimSearches() {
        var items = getLocalStorage("fs_history");
        items = items.filter(function(i) {
            var depart = i.depart.split("/");
            depart = new Date(depart[1] + "-" + depart[0] + "-" + depart[2]);
            var now = new Date();
            now.setHours(0,0,0,0);
            if (depart >= now) return i;
        });
        setLocalStorage("fs_history", items);
        
    }

    
    $(".form-selection [type='radio']").on("click", function() {
        setTimeout(function() {
            getFlightForms();
        }, 100);
    });
    
    trimSearches();
}();