SIA.popupFirstNameValidator = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
    var firstNameForm = $('#form--popup-firstname');
    var firstNameInput = $("#first-name-last");

	function init() {
		firstNameForm.validate({
            highlight: function(element, errorClass) {
                console.log(element, errorClass);
                $(element).closest('span.input-1').addClass(errorClass);
            },
            success: function(element) {
                $(element).closest('span.input-1').removeClass('error');
            }
        });

        firstNameInput.off().on('blur', function () {
            firstNameForm.valid();
        });
    }

	return {
        init: init
	};
}();

var waitForJquery = function () {
    setTimeout(function () {
        if (typeof $.validator == 'undefined') {
            waitForJquery();
        } else {
            SIA.popupFirstNameValidator.init();
        }
    }, 100);
};

$(function () {
    typeof $.validator == 'undefined' ? waitForJquery() : SIA.popupFirstNameValidator.init();
});