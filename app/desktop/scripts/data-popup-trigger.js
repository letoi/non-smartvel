SIA.popUpTrigger = function () {
    $("body").delegate("[data-trigger-popup]", "click", function(evt) {
        evt.preventDefault();
        var popupValue = $(this).attr("data-trigger-popup");
        $(popupValue).Popup("show");
        $(popupValue).Popup("show");
        $('body').find('.bg-modal').addClass('overlay');
    });

    $("body").delegate(".popup__close", "click", function () {
        $(".popup").Popup("hide");
    });
}();