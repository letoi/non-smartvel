/**
 * @name SIA
 * @description Define global newsTickerContent functions
 * @version 1.0
 */
SIA.newsTickerContent = function(){
	var config = SIA.global.config;
	var win = SIA.global.vars.win;
	var tickerEl = $('.news-ticker__content');
	var items = tickerEl.find('ul');
	var itemLen = items.children().length;
	var itemHeight = items.children().eq(0).height();
	var itemIdx = 0;
	var timerTicker;

	var calcPadding = function(el) {
		var padLeft = el.children('span').outerWidth(true),
				padRight = el.siblings('a').not(':hidden').outerWidth(true);

		el.css({
			paddingLeft: padLeft,
			paddingRight: padRight
		});
	};

	var auto = function(){
		setTimeout(function(){
			itemIdx = (itemIdx + 1) % itemLen;
			items.animate({
				'margin-top': - itemHeight * itemIdx
			}, config.duration.newsTicker.animate, function(){
				if(itemIdx === itemLen - 1){
					itemIdx = 0;
					items.css('margin-top', 0);
				}
				auto();
			});
		}, config.duration.newsTicker.auto);
	};

	if(itemLen > 1){
		items.append(items.children().eq(0).clone());
		itemLen += 1;
		auto();
	}

	calcPadding(tickerEl);

	win.off('resize.tickerMessage').on('resize.tickerMessage', function() {
		clearTimeout(timerTicker);
		timerTicker = setTimeout(function() {
			calcPadding(tickerEl);
		}, 400);
	});
};
