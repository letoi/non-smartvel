const json = globalJson.promotionCugData;
const defaultConfig = {}

const defaultI18n = 'EN'
const availableMonths = {
  EN: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November',
    'December'],
  ID: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November',
    'Desember'],
  SRT:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
}

const availableShortDays = {
  EN: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  ID: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab']
}

const presetRangeLabel = {
  EN: {
    today: 'Today',
    thisMonth: 'This Month',
    lastMonth: 'Last Month',
    lastSevenSays: 'Last 7 Days',
    lastThirtyDays: 'Last 30 Days'
  },
  ID: {
    today: 'Hari ini',
    thisMonth: 'Bulan ini',
    lastMonth: 'Bulan lalu',
    lastSevenDays: '7 Hari Terakhir',
    lastThirtyDays: '30 Hari Terakhir'
  }
}

const defaultCaptions = {
  'title': 'Choose Dates',
  'ok_button': 'Apply'
}

const defaultStyle = {
  daysWeeks: 'calendar_weeks',
  days: 'calendar_days',
  daysSelected: 'calendar_days_selected',
  daysInRange: 'calendar_days_in-range',
  firstDate: 'calendar_month_left',
  secondDate: 'calendar_month_right',
  presetRanges: 'calendar_preset-ranges',
  dateDisabled: 'calendar_days--disabled',
  disabled : 'dateDisable',
  addFocus:'focus'
}

const defaultPresets = function (i18n = defaultI18n) {
  return {
    today: function () {
      const n = new Date()
      const startToday = new Date(n.getFullYear(), n.getMonth(), n.getDate() + 1, 0, 0)
      const endToday = new Date(n.getFullYear(), n.getMonth(), n.getDate() + 1, 23, 59)
      return {
        label: presetRangeLabel[i18n].today,
        active: false,
        dateRange: {
          start: startToday,
          end: endToday
        }
      }
    },
    thisMonth: function () {
      const n = new Date()
      const startMonth = new Date(n.getFullYear(), n.getMonth(), 2)
      const endMonth = new Date(n.getFullYear(), n.getMonth() + 1, 1)
      return {
        label: presetRangeLabel[i18n].thisMonth,
        active: false,
        dateRange: {
          start: startMonth,
          end: endMonth
        }
      }
    },
    lastMonth: function () {
      const n = new Date()
      const startMonth = new Date(n.getFullYear(), n.getMonth() - 1, 2)
      const endMonth = new Date(n.getFullYear(), n.getMonth(), 1)
      return {
        label: presetRangeLabel[i18n].lastMonth,
        active: false,
        dateRange: {
          start: startMonth,
          end: endMonth
        }
      }
    },
    last7days: function () {
      const n = new Date()
      const start = new Date(n.getFullYear(), n.getMonth(), n.getDate() - 5)
      const end = new Date(n.getFullYear(), n.getMonth(), n.getDate() + 1)
      return {
        label: presetRangeLabel[i18n].lastSevenDays,
        active: false,
        dateRange: {
          start: start,
          end: end
        }
      }
    },
    last30days: function () {
      const n = new Date()
      const start = new Date(n.getFullYear(), n.getMonth(), n.getDate() - 29)
      const end = new Date(n.getFullYear(), n.getMonth(), n.getDate() + 1)
      return {
        label: presetRangeLabel[i18n].lastThirtyDays,
        active: false,
        dateRange: {
          start: start,
          end: end
        }
      }
    }
  }
}
Vue.component('date-picker', {
    props: {
        configs: {
          type: Object,
          default: () => defaultConfig
        },
        i18n: {
          type: String,
          default: defaultI18n
        },
        months: {
          type: Array,
          default: () => null
        },
        shortDays: {
          type: Array,
          default: () => null
        },
        // options for captions are: title, ok_button
        captions: {
          type: Object,
          default: () => defaultCaptions
        },
        format: {
          type: String,
          default: 'DD MMMM YYYY (dddd)'
        },
        styles: {
          type: Object,
          default: () => {}
        },
        initRange: {
          type: Object,
          default: () => null
        },
        startActiveMonth: {
          type: Number,
          default: parseInt(json.fareDeals.travelDurationStart.split("-")[1])-1
        },
        startActiveYear: {
          type: Number,
          default: parseInt(json.fareDeals.travelDurationStart.split("-")[2])
        },
        presetRanges: {
          type: Object,
          default: () => null
        },
        compact: {
          type: String,
          default: 'false'
        },
        righttoleft: {
          type: String,
          default: 'true'
        }
      },
    template:`<div class="calendar-root">
    <div class="grid-col one-half" ref="errorDiv1">
      <div class="grid-inner">
        <div class="input-3" ref="startDiv" :class="{'focus':key == 'first', 'has_selected':key == 'first' || startDate}" tabindex=0 @click.stop="toggleCalendar('first')">
          <label for="select-date-2" class="input-3__label">Depart date</label>
          
          <div class="input-date" ><input readonly=true type="text" name="start-date" id="start-date"  aria-labelledby="start-date-error" v-model="startDate" value="" class="input-date-picker ui-autocomplete-input" data-rule-required="true" data-msg-required="Select depart date" data-toggle-mb="true" autocomplete="off" aria-required="true"></div>
          <button type="button" name="btn-search-fl-depart" id="btn-search-fl-depart">
            <em v-if="!startDate" class="ico-date">
              <span class="ui-helper-hidden-accessible">Calendar</span>
            </em>
          </button>
        </div>
      </div>
    </div>
    <div class="grid-col one-half" ref="errorDiv2">
      <div class="grid-inner">
        <div class="input-3" :class="{'focus':key == 'second', 'has_selected':endDate}" @click.stop="toggleCalendar('second')">
          <label for="select-date-1" class="input-3__label">Return date</label>
          <div class="input-date" ><input readonly=true ref="endInput"  type="text" name="end-date" id="end-date"  aria-labelledby="start-end-error" v-model="endDate" value="" class="input-date-picker ui-autocomplete-input" data-rule-required="true" data-msg-required="Select return date" data-toggle-mb="true" autocomplete="off" aria-required="true"></div>
          <button type="button" name="btn-search-fl-return" id="btn-search-fl-return">
            <em v-if="!endDate" class="ico-date">
              <span class="ui-helper-hidden-accessible">Calendar</span>
            </em>
          </button>
        </div>
      </div>
    </div>




    <div class="calendar" :class="{'calendar-mobile ': isCompact, 'calendar-right-to-left': isRighttoLeft}" v-if="isOpen">
      <div class="calendar-head" v-if="!isCompact">
      </div>
      <div class="calendar-wrap">
        <div class="calendar_month_left" :class="{'calendar-left-mobile': isCompact}" v-if="showMonth">
          <div class="months-text">
            <span class="left ico-point-l" @click.stop="goPrevMonth"></span>
            <span class="right" @click.stop="goNextMonth" v-if="isCompact"></span>
            {{monthsLocale[activeMonthStart] +' '+ activeYearStart}}
            <span class="return-date-flight">{{numberOfDays}} days</span></div>
            <ul :class="s.daysWeeks">
              <li v-for="item in shortDaysLocale" :key="item">{{item}}</li>
            </ul>
            <ul v-for="r in 6" :class="[s.days]" :key="r">
              <li :class="[{[s.daysSelected]: isDateSelected(r, i, 'first', startMonthDay, endMonthDate),
              [s.daysInRange]: isDateInRange(r, i, 'first', startMonthDay, endMonthDate),
              [s.dateDisabled]: isDateDisabled(r, i, startMonthDay, endMonthDate),
              [s.disabled]: checkInvalidDate(r, i, 'first', startMonthDay, endMonthDate)}]"
               v-for="i in numOfDays" :key="i" v-html="getDayCell(r, i, startMonthDay, endMonthDate)"
                @click.stop="selectFirstItem(r, i, checkInvalidDate(r, i, 'first', startMonthDay, endMonthDate))"><a></a></li>
            </ul>
        </div>
        <div class="calendar_month_right" v-if="!isCompact">
          <div class="months-text">
            {{monthsLocale[startNextActiveMonth] +' '+ activeYearEnd}}
            <span class="right ico-point-r" @click.stop="goNextMonth"></span>
          </div>
          <ul :class="s.daysWeeks">
              <li v-for="item in shortDaysLocale" :key="item">{{item}}</li>
          </ul>
          <ul v-for="r in 6" :class="[s.days]" :key="r">
            <li :class="[{[s.daysSelected]: isDateSelected(r, i, 'second', startNextMonthDay, endNextMonthDate),
            [s.daysInRange]: isDateInRange(r, i, 'second', startNextMonthDay, endNextMonthDate),
            [s.dateDisabled]: isDateDisabled(r, i, startNextMonthDay, endNextMonthDate),
            [s.disabled]: checkInvalidDate(r, i, 'second', startNextMonthDay, endNextMonthDate)}]"
                v-for="i in numOfDays" :key="i" v-html="getDayCell(r, i, startNextMonthDay, endNextMonthDate)"
                  @click.stop="selectSecondItem(r, i, checkInvalidDate(r, i, 'second', startNextMonthDay, endNextMonthDate))">{{}}</li>
          </ul>
        </div>
      </div>
      
    </div>
  </div>`,
  data () {
    return {
      dateRange: {},
      numOfDays: 7,
      isFirstChoice: true,
      isOpen: false,
      presetActive: '',
      showMonth: false,
      activeMonthStart: this.startActiveMonth,
      activeYearStart: this.startActiveYear,
      activeYearEnd: this.startActiveYear,
      startDate:'',
      endDate:'',
      numberOfDays:'0',
      key:''
    }
  },
  created () {
    if (this.isCompact) {
      this.isOpen = true
    }
    if (this.activeMonthStart === 11) this.activeYearEnd = this.activeYearStart + 1
  },
  watch: {
    startNextActiveMonth: function (value) {
      if (value === 0) this.activeYearEnd = this.activeYearStart + 1
    }
  },
  computed: {
    getTravelDurationStart: function(){
      var result =  json.fareDeals.travelDurationStart.split("-");
      return new Date(result[2],parseInt(result[1])-1,result[0]);
    },
    getTravelDurationEnd: function(){
      var result =  json.fareDeals.travelDurationEnd.split("-");
      return new Date(result[2],parseInt(result[1])-1,result[0]);
    },
    addAdvancePurchaseday:function(){
      var startingDate = new Date();
      startingDate.setDate(startingDate.getDate()+parseInt(json.fareDeals.advancePurchase));
      return  this.getTravelDurationStart > startingDate ? this.getTravelDurationStart : startingDate;
    },
    monthsLocale: function () {
      return this.months || availableMonths[this.i18n]
    },
    shortDaysLocale: function () {
      return this.shortDays || availableShortDays[this.i18n]
    },
    s: function () {
      return Object.assign({}, defaultStyle, this.style)
    },
    startMonthDay: function () {
      return new Date(this.activeYearStart, this.activeMonthStart, 1).getDay()
    },
    startNextMonthDay: function () {
      if(this.activeMonthStart === 11){
        return new Date(this.activeYearStart, this.startNextActiveMonth, 1).getDay() + 1;
      }else{
        return new Date(this.activeYearStart, this.startNextActiveMonth, 1).getDay();
      }
    },
    endMonthDate: function () {
      return new Date(this.activeYearEnd, this.startNextActiveMonth, 0).getDate()
    },
    endNextMonthDate: function () {
      return new Date(this.activeYearEnd, this.activeMonthStart + 2, 0).getDate()
    },
    startNextActiveMonth: function () {
      return this.activeMonthStart >= 11 ? 0 : this.activeMonthStart + 1
    },
    finalPresetRanges: function () {
      const tmp = {}
      const presets = this.presetRanges || defaultPresets(this.i18n)
      for (const i in presets) {
        const item = presets[i]
        let plainItem = item
        if (typeof item === 'function') {
          plainItem = item()
        }
        tmp[i] = plainItem
      }
      return tmp
    },
    isCompact: function () {
      return this.compact === 'true'
    },
    isRighttoLeft: function () {
      return this.righttoleft === 'true'
    }
  },
  methods: {
    convertToDate: function(stringDate){
      return new Date(stringDate[2],availableMonths.SRT.indexOf(stringDate[1]),stringDate[0]);
    },
    getNumberOfDays: function(start,end){
      var oneDay = 24*60*60*1000;
      var diffDays = Math.round(Math.abs((start.getTime() - end.getTime())/(oneDay)));
      return diffDays+1;
    },
    toggleCalendar: function (key) {
      this.key = key;
      if (this.isCompact) {
        this.showMonth = !this.showMonth
        return
      }
      this.isOpen = !this.isOpen
      if(!this.isOpen){
        this.key = '';
      }
      this.showMonth = !this.showMonth
      return
    },
    getDateString: function (date, format = this.format) {
      if (!date) {
        return null
      }
      const dateparse = new Date(Date.parse(date))
      return fecha.format(new Date(dateparse.getFullYear(), dateparse.getMonth(), dateparse.getDate() - 1), format)
    },
    getDayIndexInMonth: function (r, i, startMonthDay) {
      const date = (this.numOfDays * (r - 1)) + i
      return date - startMonthDay
    },
    getDayCell (r, i, startMonthDay, endMonthDate) {
      const result = this.getDayIndexInMonth(r, i, startMonthDay)
      // bound by > 0 and < last day of month
      return result > 0 && result <= endMonthDate ? result : '&nbsp;'
    },
    getNewDateRange (result, activeMonth, activeYear) {
      const newData = {}
      let key = 'start'

      if (!this.isFirstChoice) {
        key = 'end'
      } else {
        newData['end'] = null
      }
      const resultDate = new Date(activeYear, activeMonth, result)
      if (!this.isFirstChoice && resultDate < this.dateRange.start) {
        this.isFirstChoice = false
        return { start: resultDate }
      }
      var max = json.fareDeals.maximumStayType.toLowerCase() == 'months' ? parseInt(json.fareDeals.maximumStay)*30 : parseInt(json.fareDeals.maximumStay);
      if(!this.isFirstChoice && this.getNumberOfDays(this.dateRange.start,resultDate) < parseInt(json.fareDeals.minimumStay)){
        console.log("number should be greater or equal to min stay");
        return { end: null };
      }
      else if(!this.isFirstChoice && this.getNumberOfDays(this.dateRange.start,resultDate) > max){
        return { end: null };
      }
      // toggle first choice
      this.isFirstChoice = !this.isFirstChoice
      newData[key] = resultDate
      return newData
    },
    checkInvalidDate(r, i, key, startMonthDay, endMonthDate) {
      
      const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
      if (result < 2 || result > endMonthDate + 1) return false

      let currDate = null
      if (key === 'first') {
        currDate = this.changeMonth(this.getDateString(new Date(this.activeYearStart, this.activeMonthStart, result)));
      } else {
        currDate = this.changeMonth(this.getDateString(new Date(this.activeYearEnd, this.startNextActiveMonth, result)));
      }
      if(this.getTravelDurationEnd <= new Date() ||
       this.getNumberOfDays(new Date(),this.getTravelDurationEnd) < parseInt(json.fareDeals.minimumStay) ||
       this.getNumberOfDays(this.addAdvancePurchaseday,this.getTravelDurationEnd) < parseInt(json.fareDeals.minimumStay)
      ){
        return true;
      }else{
        thisValue = !(this.convertToDate(currDate.split(" ")) >= this.addAdvancePurchaseday && this.convertToDate(currDate.split(" ")) <= this.getTravelDurationEnd);
        return thisValue;
      }
    },
    changeMonth(date){
      if(date){
        var splitDate = date.split(" ");
        splitDate[1] = availableMonths.SRT[availableMonths.EN.indexOf(splitDate[1])];
        return splitDate[0]+" "+splitDate[1]+" "+splitDate[2]+" "+splitDate[3];
      }else{
        return null;
      }
      
    },
    checkDate(){
      this.toggleCalendar('');
      if(!this.dateRange.end){
        this.$refs.endInput.focus();
        this.toggleCalendar('second');
      }
      this.startDate = this.changeMonth(this.getDateString(this.dateRange.start));
      this.endDate = this.changeMonth(this.getDateString(this.dateRange.end));
      setTimeout(function(){
        !this.endDate ? $('li.calendar_days_selected').addClass('single-date') : $('li.single-date').removeClass('single-date');
      },100);

      if(this.startDate && this.$refs.errorDiv1.classList.contains('error')){
        this.$refs.errorDiv1.classList.remove('error');
        $(this.$refs.errorDiv1).children('.text-error').remove();
      }else if(this.startDate && this.$refs.errorDiv2.classList.contains('error')){
        this.$refs.errorDiv2.classList.remove('error');
        $(this.$refs.errorDiv2).children('.text-error').remove();
      }
      if(this.dateRange.end){
        this.numberOfDays = this.getNumberOfDays(this.convertToDate(this.startDate.split(" ")),this.convertToDate(this.endDate.split(" ")))
      }
    },
    selectFirstItem (r, i, notValid) {
      if(notValid){
        return;
      }
      if(this.key == 'second'){
        this.dateRange.end = null;
        this.isFirstChoice = false;
      }
      const result = this.getDayIndexInMonth(r, i, this.startMonthDay) + 1
      this.dateRange = Object.assign({}, this.dateRange, this.getNewDateRange(result, this.activeMonthStart,
      this.activeYearStart))
      if (this.dateRange.start && this.dateRange.end) {
        this.presetActive = ''
        if (this.isCompact) {
          this.showMonth = false
        }
      }
    
      this.checkDate();
    },
    selectSecondItem (r, i,notValid) {
      if(notValid){
        return;
      }
      if(this.key == 'second'){
        this.dateRange.end = null;
        this.isFirstChoice = false;
      }
      const result = this.getDayIndexInMonth(r, i, this.startNextMonthDay) + 1
      this.dateRange = Object.assign({}, this.dateRange, this.getNewDateRange(result, this.startNextActiveMonth,
      this.activeYearEnd))
      if (this.dateRange.start && this.dateRange.end) {
        this.presetActive = ''
      }
      this.checkDate();
    },
    isDateSelected (r, i, key, startMonthDay, endMonthDate) {
      const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
      if (result < 2 || result > endMonthDate + 1) return false

      let currDate = null
      if (key === 'first') {
        currDate = new Date(this.activeYearStart, this.activeMonthStart, result)
      } else {
        currDate = new Date(this.activeYearEnd, this.startNextActiveMonth, result)
      }
      return (this.dateRange.start && this.dateRange.start.getTime() === currDate.getTime()) ||
        (this.dateRange.end && this.dateRange.end.getTime() === currDate.getTime())
    },
    isDateInRange (r, i, key, startMonthDay, endMonthDate) {
      const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
      if (result < 2 || result > endMonthDate + 1) return false

      let currDate = null
      if (key === 'first') {
        currDate = new Date(this.activeYearStart, this.activeMonthStart, result)
      } else {
        currDate = new Date(this.activeYearEnd, this.startNextActiveMonth, result)
      }
      return (this.dateRange.start && this.dateRange.start.getTime() < currDate.getTime()) &&
        (this.dateRange.end && this.dateRange.end.getTime() > currDate.getTime())
    },
    isDateDisabled (r, i, startMonthDay, endMonthDate) {
      const result = this.getDayIndexInMonth(r, i, startMonthDay)
      // bound by > 0 and < last day of month
      return !(result > 0 && result <= endMonthDate)
    },
    goPrevMonth () {
      const prevMonth = new Date(this.activeYearStart, this.activeMonthStart, 0)
      this.activeMonthStart = prevMonth.getMonth()
      this.activeYearStart = prevMonth.getFullYear()
      this.activeYearEnd = prevMonth.getFullYear()
    },
    goNextMonth () {
      const nextMonth = new Date(this.activeYearEnd, this.startNextActiveMonth, 1)
      this.activeMonthStart = nextMonth.getMonth()
      this.activeYearStart = nextMonth.getFullYear()
      this.activeYearEnd = nextMonth.getFullYear()
    },
    updatePreset (item) {
      this.presetActive = item.label
      this.dateRange = item.dateRange
      // update start active month
      this.activeMonthStart = this.dateRange.start.getMonth()
      this.activeYearStart = this.dateRange.start.getFullYear()
      this.activeYearEnd = this.dateRange.end.getFullYear()
    },
    setDateValue: function () {
      this.$emit('selected', this.dateRange)
      if (!this.isCompact) {
        this.toggleCalendar('')
      }
    }
  },
  mounted:function(){
    var self = this;
    document.onkeydown = function(e){
      if(e.keyCode == 9 || e.which == 9 && this.$refs.startDiv.classList.contains('focus-outline')){
        if(!self.startDate){
          self.$refs.startDiv.classList.remove('focus-outline');
          self.toggleCalendar('first');
        }else if(!self.endDate){
          self.$refs.startDiv.classList.remove('focus-outline');
          self.toggleCalendar('second');
        }else{
          self.$refs.startDiv.classList.remove('focus-outline');
          self.toggleCalendar('first');
        }
      }
    }

    $('#app2').remove();
    document.onclick = function(){
      setTimeout(function(){
        if(self.$refs.errorDiv1.classList.contains('error')){
          self.toggleCalendar('first');
        }else if(self.isOpen){
          self.toggleCalendar(self.key)
        }
      },100);
    }
  }
  })
  
  var app7 = new Vue({
    el: '#app',
    data: {
      
    }
  })

  