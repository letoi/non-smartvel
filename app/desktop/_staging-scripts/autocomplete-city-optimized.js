/**
 * @name SIA
 * @description Define global initAutocompleteCity functions
 * @version 1.0
 */
SIA.initAutocompleteCityNew = function() {
	var global = SIA.global;
	var vars = global.vars;
	var doc = vars.doc;
	var win = vars.win;
	var config = global.config;
	var body = vars.body;
	var isIETouch = vars.isIETouch();
	var originSelected;
	var destinationSelected;
	var countryOriginSelected;
	var countryDestinationSelected;

	var countryGroups = [];

	var _autoComplete = function(opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			// containerAutocomplete : '',
			autocompleteFields: '',
			autoCompleteAppendTo: '',
			airportData: [],
			open: function() {},
			change: function() {},
			select: function() {},
			close: function() {},
			search: function() {},
			response: function() {},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		// that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.autocompleteFields = that.options.autocompleteFields;
		that.airportData = that.options.airportData;
		that.cityData = that.options.cityData;
		that.timer = null;
		that.timerResize = null;
		that.winW = null;

		that.autocompleteFields.each(function(index, value) {
			var field = $(value);
			var wp = field.closest('[data-autocomplete]');
			var inputEl = wp.find('input');
			var optionEl = wp.find('select option');

			if (wp.data('origin') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						countryOriginSelected = $(this).data('text');
						originSelected = $(this).data('country-exclusion');
					}
				});
			}

			if (wp.data('destination') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						countryDestinationSelected = $(this).data('text');
						destinationSelected = $(this).data('country-exclusion');
					}
				});
			}

			var bookingAutoComplete = field.autocomplete({
				minLength: 0,
				open: that.options.open,
				change: that.options.change,
				select: that.options.select,
				close: that.options.close,
				search: that.options.search,
				response: that.options.response,
				// source: that.airportData,
				source: function(request, response) {
					/*// create a regex from ui.autocomplete for 'safe returns'
					var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
					// match the user's request against each destination's keys,

					var match = $.grep(that.airportData, function(airport) {

					  var label = airport.label;
					  var value = airport.value;

					  return (matcher.test(label) || matcher.test(value)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
					});

					// ... return if ANY of the keys are matched
					response(match);*/

					var listData = [],
						listCodeCheck = [];

					if (wp.data('origin')) {
						if (destinationSelected && destinationSelected !== '') {
							destinationSelected.indexOf(',') === -1 ? listCodeCheck.push(destinationSelected) : listCodeCheck = destinationSelected.split(',');
						}
						if (countryDestinationSelected && countryDestinationSelected !== '') {
							listCodeCheck.push(countryDestinationSelected);
						}
					}

					if (wp.data('destination')) {
						if (originSelected && originSelected !== '') {
							originSelected.indexOf(',') === -1 ? listCodeCheck.push(originSelected) : listCodeCheck = originSelected.split(',');
						}
						if (countryOriginSelected && countryOriginSelected !== '') {
							listCodeCheck.push(countryOriginSelected);
						}
					}

					if (!request.term) {
						listData = checkAvailableFlights(listCodeCheck, that.airportData);
						listData = checkGroupNoItem(listData);
						if  (wp.closest('#book-redem', '#book-flight')) {
							response(listData.slice(0, 100));
						} else {
							response(isIETouch ? listData.slice(0, 20) : listData);
						}
						return;
					}

					try {
						that.group = '';
						var matcher = new RegExp('(^' + $.ui.autocomplete.escapeRegex(request.term) + ')|(\\s' + $.ui.autocomplete.escapeRegex(request.term) + ')|([(]' + $.ui.autocomplete.escapeRegex(request.term) + ')', 'i');
						var match1 = [];
						if (that.cityData.length) {
							match1 = $.grep(that.cityData, function(airport) {
								var label = airport.label;
								var valueOption = airport.valueOption;

								return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
							});
						}


						var match = $.grep(that.airportData, function(airport) {
							var label = airport.label;
							var valueOption = airport.valueOption;
							var value = airport.countrySelectedcity;

							return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
						});

						listData = checkAvailableFlights(listCodeCheck, match);

						listData = checkGroupNoItem(listData);

						if (match1.length) {
							match1 = checkAvailableFlights(listCodeCheck, match1);
							response(isIETouch ? match1.concat(listData).slice(0, 20) : match1.concat(listData));
						} else {
							if  (wp.closest('#book-redem, #book-flight')) {
								response(listData.slice(0, 100));
							} else {
								response(isIETouch ? listData.slice(0, 20) : listData);
							}
						}

					} catch (err) {
						response(isIETouch ? that.airportData.slice(0, 20) : that.airportData);
					}
				},
				position: that.options.position,
				appendTo: that.options.autoCompleteAppendTo
			}).data('ui-autocomplete');


			bookingAutoComplete._renderItem = function(ul, item) {

				if (item.group) {
					that.group = item.group;
					return $('<li class="group-item">' + item.group + '</li>')
						.appendTo(ul);
				} else {
					var link = item.link ? ' data-link="' + item.link + '"' : '';
					// since all cities are under a country/group, if item.parent does not belong to that.options.group, you can just prepend to ul
					if (item.parent === that.group) {
						return $('<li class="autocomplete-item"' + link + '>')
							.attr('data-value', item.city)
							.append('<a class="autocomplete-link">' + item.city + '</a>')
							.appendTo(ul);
					} else if (item.parent) {
						return $('<li class="autocomplete-item redundancy"' + link + '>')
							.attr('data-value', item.city)
							.append('<a class="autocomplete-link">' + item.city + '</a>')
							.prependTo(ul);
					}
					return $('<li class="autocomplete-item"' + link + '>')
						.attr('data-value', item.city)
						.append('<a class="autocomplete-link">' + item.city + '</a>')
						.appendTo(ul);
				}

			};

			bookingAutoComplete._resizeMenu = function() {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			// bookingAutoComplete._renderMenu = function( ul ) {
			//  $(ul).find('.redundancy').remove();
			// };

			bookingAutoComplete._move = function(direction) {
				var item, previousItem,
					last = false,
					api = this.menu.element.data('jsp'),
					li = $(),
					minus = null,
					currentPosition = api.getContentPositionY();
				var listCity = this.menu.element.find('li.autocomplete-item');
				var preIdx = 0;
				switch (direction) {
					case 'next':
						if (this.element.val() === '') {
							api.scrollToY(0);
							li = listCity.first();
							item = li.addClass('active').data('ui-autocomplete-item');
						} else {
							previousItem = listCity.filter('li.autocomplete-item.active').removeClass('active');
							preIdx = listCity.index(previousItem);
							li = listCity.eq(preIdx + 1);
							item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
							// console.log(currentPosition, previousItem.position().top);
						}
						if (!item) {
							last = true;
							li = listCity.removeClass('active').first();
							item = li.addClass('active').data('ui-autocomplete-item');
						}
						this.term = item.value;
						that.group = '';
						this.element.val(this.term);

						if (last) {
							api.scrollToY(0);
							last = false;
						} else {
							currentPosition = api.getContentPositionY();
							minus = li.position().top + li.innerHeight();
							if (minus - this.menu.element.height() > currentPosition) {
								api.scrollToY(Math.max(0, minus - this.menu.element.height()));
							}
						}
						$('#wcag-custom-select').html(item.value);
						break;
					case 'previous':
						if (this.element.val() === '') {
							last = true;
							item = listCity.last().addClass('active').data('ui-autocomplete-item');
						} else {
							previousItem = listCity.filter('li.autocomplete-item.active').removeClass('active');
							preIdx = listCity.index(previousItem);
							if (preIdx - 1 < 0) {
								li = $();
							} else {
								li = listCity.eq(preIdx - 1);
							}
							item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
						}
						if (!item) {
							last = true;
							item = listCity.removeClass('active').last().addClass('active').data('ui-autocomplete-item');
						}
						this.term = item.value;
						that.group = '';
						this.element.val(this.term);
						if (last) {
							api.scrollToY(this.menu.element.find('.jspPane').height());
							last = false;
						} else {
							currentPosition = api.getContentPositionY();
							if (li.position().top <= currentPosition) {
								api.scrollToY(li.position().top);
							}
						}

						$('#wcag-custom-select').html(item.value);

						break;
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');
			// win.off('resize.blur'+index).on('resize.blur'+index, function(){
			//  clearTimeout(that.timerResize);
			//  that.timerResize = setTimeout(function(){
			//    field.blur();
			//  }, 100);
			// });

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function() {
				var self = $(this);
				self.val('');
				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				if (global.vars.isIE() && global.vars.isIE() < 9) {
					if ($('ul.ui-autocomplete:visible').length && $('ul.ui-autocomplete:visible').not(field.autocomplete('widget')).length) {
						$('ul.ui-autocomplete:visible').not(field.autocomplete('widget')).data('input').autocomplete('close');
					}
					doc.off('click.hideAutocompleteCity').on('click.hideAutocompleteCity', function(e) {
						if (!$(e.target).closest('.ui-autocomplete').length && !$(e.target).is('.ui-autocomplete-input')) {
							field.closest('.custom-select').removeClass('focus');
							field.autocomplete('close');
						}
					});
				}

				SIA.WcagGlobal.customSelectAriaLive(field.closest('.custom-select .select__text'));

				win.off('resize.blur' + index).on('resize.blur' + index, function() {
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function() {
						if (that.winW !== win.width()) {
							field.blur();
						}
					}, 100);
				});
			});
			// if(!global.vars.isIE()){
			field.off('blur.highlight').on('blur.highlight', function() {
				that.timer = setTimeout(function() {
					field.closest('.custom-select').removeClass('focus');
				}, 200);
				field.autocomplete('close');
				win.off('resize.blur' + index);
			});
			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function() {
				clearTimeout(that.timer);
			});
			// }
			// else{
			//  field.autocomplete('widget').data('input', field);
			// }

			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e) {
				if (e.which === 13) {
					e.preventDefault();
					if (field.autocomplete('widget').find('li').length === 1) {
						field.autocomplete('widget').find('li').trigger('click');
						return;
					}
					field.autocomplete('widget').find('li.active').trigger('click');
				}

				if (field.closest('#book-redem, #book-flight').length >= 1) {
					if (field.val().length == 0) {
						field.closest('[data-autocomplete="true"]').find('.optim-close').addClass('hidden');
					} else {
						clearCloseBtn(field);
					}
				}

			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e) {
				// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if (field.closest('.custom-select').hasClass('focus')) {
					field.trigger('blur.highlight');
				} else {
					field.trigger('focus.highlight');
				}
			});
		});
		resetAvailableFlight();
	};

	var createGroupList = function(data, type) {
		var str = '';

		if (type == 'list') {
			str += '<li class="group-item">' + data + '</li>';
		} else {
			str += '<optgroup label="' + data + '"></optgroup>';
		}

		return $(str);
	};

	var createList = function(data, type) {
		var str = '';

		if (type === 'list') {
			/*str = '<li class="autocomplete-item ui-menu-item" data-value="' + data.cities[0].value + '" role="presentation"><a class="autocomplete-link ui-corner-all" tabindex="-1">' + data.country.name + ' (' + data.cities[0].text + ')' + '</a></li>';*/

			str = '<li class="autocomplete-item ui-menu-item" data-value="' + (data.airportCode + ' - ' + data.airportName) + '" role="presentation"><a class="autocomplete-link ui-corner-all" tabindex="-1">' + (data.airportName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.countryCode + ')') + '</a></li>';
		} else {
			/*str = '<option value="' + data.cities[0].value + '" data-text="' + (data.cities[0].cityName + ' - ' + data.cities[0].cityCode) +
				'" data-country="' + data.country.code + '" data-country-exclusion="' + data.cities[0].countryExclusion + '">' +
				data.country.name + ' (' + data.cities[0].text + ')' + '</option>';*/

			str = '<option value="' + (data.airportCode + ' - ' + data.airportName) + '" data-text="' + (data.airportCode + ' - ' + data.airportName) +
				'" data-country="' + data.airportName + '" data-country-exclusion="' + data.airportName + '">' + (data.airportName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.countryCode + ')') + '</option>';
		}

		return $(str);
	};

	var clearCloseBtn = function(el) {
		var closeIconEl = $('<a href="#clear" class="ico-cancel-thin optim-close add-clear-text" tabindex="-1"><span class="ui-helper-hidden-accessible">clear</span></a>');
		var parent = el.closest('[data-autocomplete="true"]');

		var closeIcon = parent.find('.optim-close');
		if (closeIcon.length >= 1) {
			closeIcon.removeClass('hidden');
		} else {
			closeIconEl.off().on('click', function(e) {
				e.preventDefault();

				el.val('');
				$(this).addClass('hidden');
			});

			parent.find('.select__text').after(closeIconEl);
		}
	};


	var initAutocompleteCity = function() {
		// init autocomplete
		var autocompleteEl = body.find('[data-autocomplete]');
		var timerAutocomplete = null;
		// var timerAutocompleteSearch = null;
		// var timerAutocompleteOpen = null;

		//var countriesList = globalJson.countriesList.airportList;
		var countriesList = globalJson.countriesList.countries;

		var loadingStatus = config.template.loadingStatus;
		var initLoading = (global.vars.isIE() && global.vars.isIE() < 9);
		if (initLoading) {
			loadingStatus = $(loadingStatus).appendTo(body);
		}
		if (autocompleteEl.length) {
			autocompleteEl.each(function() {
				var self = $(this),
					inputEl = self.find('input:text');

				if (self.data('init-automcomplete')) {
					return;
				}
				if (self.find('input:text').is('[readonly]')) {
					self.find('input:text').off('focus').off('click');
					return;
				}
				var select = self.find('select');
				//var options = select.children();
				var data = [];
				var arrDataValue = [];
				var clearHideaddClear = null;
				// if(self.val() === '' || self.val() !== self.attr('placeholder')){
				//  self.closest('[data-autocomplete]').removeClass('default');
				// }

				self.data('minLength', 2);
				self.data('init-automcomplete', true);
				self.find('input:text').focus(function() {
					// $(this).val('');
					/*var dataAutocomplete = inputEl.data('uiAutocomplete');
					if(dataAutocomplete && inputEl.val()) {
					  var indexItem = $.inArray(inputEl.val(), arrDataValue);
					  clearTimeout(timerAutocompleteSearch);
					  timerAutocompleteSearch = setTimeout(function() {
					    if(indexItem === -1) {
					      dataAutocomplete.search();
					    } else {
					      inputEl.autocomplete('widget').find('li').eq(indexItem).addClass('active');
					    }
					  }, 200);
					}*/
				});

				function addData(els) {
					els.each(function() {
						var indexOfAirport = $(this).text().indexOf('(');
						var valueOption = $(this).text().slice(indexOfAirport);
						data.push({
							city: $(this).text(),
							label: $(this).text(),
							value: $(this).data('text'),
							valueOption: valueOption,
							parent: $(this).parent().attr('label'),
							country: $(this).data('country'),
							link: $(this).data('link') || '',
							countryExclusion: $(this).data('country-exclusion')
						});
						arrDataValue.push({
							city: $(this).text(),
							label: $(this).data('text'),
							value: $(this).data('text'),
							valueOption: valueOption,
							country: $(this).data('country'),
							link: $(this).data('link') || '',
							countryExclusion: $(this).data('country-exclusion')
						});
					});
				};

				function optionsAddData(select, options) {
					if (select.data('city-group')) {
						options.each(function(i, val) {
							var self = $(this);
							var countryItem = self.children();

							if (self.attr('label')) {
								data.push({
									city: self.attr('label'),
									label: self.attr('label'),
									group: self.attr('label'),
									link: self.data('link') || '',
									value: self.attr('label')
								});
							} else {
								data.push({
									city: self.text(),
									label: self.text(),
									value: self.data('text'),
									link: $(this).data('link') || ''
								});
							}

							if (countryItem.length) {
								addData(countryItem);
							}
						});
					} else {
						options.each(function() {
							data.push({
								city: $(this).text(),
								label: $(this).text(),
								value: $(this).data('text'),
								link: $(this).data('link') || ''
							});
						});
					}
				};

				if (select.closest('#book-redem, #book-flight').length) {
					var countryGroupList = [];
					var selectEl = select;
					for (var i = 0; i < countriesList.length; i++) {
						var countryName = countriesList[i].countryName;
						if (countryGroupList.indexOf(countryName) <= -1) {
							selectEl.append(createGroupList(countryName, ''));

							countryGroupList.push(countryName);
						}

						// get last appended country group
						var lastSelectTagEl = selectEl.children().last();
						lastSelectTagEl.append(createList(countriesList[i], ''));
					}

					optionsAddData(select, selectEl.children());
				} else {
					optionsAddData(select, select.children());
				}

				select.off('change.selectCity').on('change.selectCity', function() {
					select.closest('[data-autocomplete]').find('input').val(select.find(':selected').data('text'));
				});

				var isFixed = false;
				self.parents().each(function() {
					isFixed = $(this).css('position') === 'fixed';
					return !isFixed;
				});

				_autoComplete({
					autocompleteFields: self.find('input:text'),
					autoCompleteAppendTo: body,
					airportData: data,
					cityData: arrDataValue,
					position: isFixed ? {
						collision: 'flip'
					} : {
						my: 'left top',
						at: 'left bottom',
						collision: 'none'
					},
					open: function() {
						var self = $(this);

						self.autocomplete('widget').find('.redundancy').remove();
						clearTimeout(clearHideaddClear);
						// clearTimeout(timerAutocompleteOpen);
						if (isFixed) {
							self.autocomplete('widget').css({
								position: 'fixed'
							});
						}

						var selectTag = self.closest('[data-autocomplete]').find('[data-city-group]');

						// remove close icon in book-redem
						if (selectTag.closest('#book-redem, #book-flight').length){
							self.next().remove();
						}

						if (! selectTag.closest('#book-redem, #book-flight').length) {
							self.autocomplete('widget')
							.jScrollPane({
								scrollPagePercent: 10
							}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e) {
								e.preventDefault();
							});
						}


						if (selectTag.closest('#book-redem, #book-flight').length) {
							var scrollBarPos = 0;
							var subMenu = self.autocomplete('widget');

							self.data('nextList', 100);

							subMenu.scroll(function() {
								if (self.val().length == 0) {
									var subMenuPos = subMenu.scrollTop();
									if (subMenuPos > scrollBarPos) {
										var nextList = parseInt(self.data('nextList'));

										if (nextList != (countriesList.length - 100)) {
											var countryName = countriesList[nextList].countryName;
											if (countryGroups.indexOf(countryName) <= -1) {
												subMenu.append(createGroupList(countryName, 'list'));
												selectTag.append(createGroupList(countryName, ''));

												countryGroups.push(countryName);
											}

											// get last appended country group
											var lastSelectTagEl = selectTag.children().last();
											lastSelectTagEl.append(createList(countriesList[nextList], ''));

											subMenu.append(createList(countriesList[nextList], 'list'));

											self.data('nextList', nextList + 1);
										}
									}

									scrollBarPos = subMenuPos;
								}
							});
						}
					},
					response: function(event, ui) {
						if (!ui.content.length) {
							ui.content.push({
								city: L10n.globalSearch.noMatches,
								label: L10n.globalSearch.noMatches,
								value: null
							});
						}
					},
					search: function() {
						var self = $(this);
						self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
						// clearTimeout(timerAutocompleteSearch);
						// timerAutocompleteSearch = setTimeout(function(){
						// if(self.autocomplete('widget').find('li').length === 1){
						//  self.autocomplete('widget').find('li').addClass('active');
						// }
						// }, 100);
					},
					close: function() {
						var self = $(this);
						self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
						self.closest('[data-autocomplete]').removeClass('focus');

						// if(self.rules().required && this.defaultValue && !this.value) {
						//  self.val(this.defaultValue).valid();
						// }

						if (global.vars.isIE() && global.vars.isIE() < 9) {
							doc.off('click.hideAutocompleteCity');
						}

						if (!self.val() || self.val() === self.attr('placeholder')) {
							self.closest('[data-autocomplete]').addClass('default');
						}

						clearHideaddClear = setTimeout(function() {
							self.blur();
							self.addClear('hide');
						}, 200);

						// add close icon for book redeem block
						if (self.closest('#book-redem, #book-flight').length >= 1 && self.val().length >= 1) {
							clearCloseBtn(self);
						} else if (self.closest('#book-redem, #book-flight').length >= 1 && self.val().length == 0) {
							self.closest('[data-autocomplete="true"]').find('.optim-close').addClass('hidden');
						}

						if (self.data('autopopulate')) {
							$(self.data('autopopulate')).val(self.val()).closest('[data-autocomplete]').removeClass('default');
							if (self.parents('.grid-col').hasClass('error')) {
								$(self.data('autopopulate')).valid();
							}
						}

						$('#wcag-custom-select').html(self.val());

						if (self.data('autopopulateholder')) {
							self.val(self.val() || self.data('autopopulateholder')).closest('[data-autocomplete]').removeClass('default');
							self.valid();
						}
					},
					select: function(event, ui) {
						// $(this).addClear('hide');
						var self = $(this),
							selfAutoComplete = self.parents('[data-autocomplete]'),
							searchBox = $('input', selfAutoComplete);

						if (ui.item.link) {
							searchBox.data('link', ui.item.link);
						}

						if (!ui.item.value) {
							window.setTimeout(function() {
								self.trigger('blur.triggerByGroupValidate');
							}, 400);
							return;
						} else {
							if (self.closest('#travel-widget').data('widget-v1') || self.closest('#travel-widget').data('widget-v2')) {
								$('#travel-widget .from-select, #travel-widget .to-select').not(selfAutoComplete).removeClass('default');
								if (selfAutoComplete.is('.from-select')) {
									$('#travel-widget .from-select').not(selfAutoComplete).find('input').val(ui.item.value);
								} else if (selfAutoComplete.is('.to-select')) {
									$('#travel-widget .to-select').not(selfAutoComplete).find('input').val(ui.item.value);
								}
							};
						}


						var wrapper = self.closest('div');
						self.closest('[data-autocomplete]').removeClass('default');
						if (initLoading) {
							loadingStatus.css({
								'width': wrapper.outerWidth(true),
								'height': wrapper.outerHeight(true),
								'top': wrapper.offset().top,
								'left': wrapper.offset().left,
								'display': 'block'
							});
						}
						if (self.parents('.from-select').length) {
							originSelected = ui.item.countryExclusion;
							countryOriginSelected = ui.item.value;
							self.closest('.form-group').data('change', true);
							if (self.parents('.from-to-container').find('.to-select').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									}, 100);
								}
							} else {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										//self.blur();
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
									}, 100);
								}
							}
						}
						if (self.parents('.to-select').length) {
							destinationSelected = ui.item.countryExclusion;
							countryDestinationSelected = ui.item.value;
							if ($('#travel-radio-4').length && $('#travel-radio-5').length && ($('#travel-radio-4').is(':visible') || $('#travel-radio-5').is(':visible'))) {
								if (self.is('#city-2')) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										if ($('#travel-radio-4').is(':checked')) {
											//self.blur();
											// $('#travel-start-day').closest('.input-3').trigger('click.showDatepicker');
											$('#travel-start-day').focus();
										} else if ($('#travel-radio-5').is(':checked')) {
											//self.blur();
											// self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
											self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').focus();
										}
										// self.closest('.form-group').find('input').valid();
									}, 100);
								}
							} else if (self.closest('form').find('[data-return-flight]').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
											self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
										} else {
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										//self.blur();
										// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
											self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
										} else {
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}
									}, 100);
								}
							} else if (self.closest('form').find('.form-group').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										//self.blur();
										// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										if (!$('body').hasClass('multi-city-page')) {
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										} else {
											self.closest('.form-group').find('[data-oneway]').focus();
										}
									}, 100);
								}
							} else if (self.parents('.from-to-container').children('[data-trigger-date]').length) {
								if (window.navigator.msMaxTouchPoints) {
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 500);
								} else {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 100);
								}
							} else if (self.closest('[data-flight-schedule]').length) {
								var dtfs = self.closest('[data-flight-schedule]');
								if (dtfs.find('[data-return-flight]').length) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										dtfs.find('[data-start-date]').focus();
									}, 500);
								} else if (dtfs.find('[data-oneway]').length) {
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function() {
										dtfs.find('[data-oneway]').focus();
									}, 500);
								}
							}
						}
						setTimeout(function() {
							if (self.closest('form').data('validator')) {
								self.valid();
							}
							// if(initLoading){
							//  loadingStatus.hide();
							// }
							self.closest('[data-autocomplete]').siblings('.mobile').find('.select__text').text(ui.city).siblings('select').val(ui.value);
							self.trigger('change.programCode');
						}, 200);
					},
					setWidth: 0
				});

				if (inputEl.val() === '' || inputEl.val() === inputEl.attr('placeholder')) {
					self.closest('[data-autocomplete]').addClass('default');
				} else {
					self.closest('[data-autocomplete]').removeClass('default');
				}
			});
		}
	};


	var checkAvailableFlights = function(listCode, match) {
		var listData = match;
		var filterDataOrigin = [];

		_.each(listCode, function(code, idx) {
			filterDataOrigin = $.grep(listData, function(item) {
				if (idx !== listCode.length - 1) {
					if (item.country) {
						return item.country !== code;
					}
				} else {
					if (item.value) {
						return item.value !== code;
					}
				}
				return item;
			});
			listData = filterDataOrigin;
		});

		return listData;
	};

	var checkGroupNoItem = function(listData) {
		var isCity = true;

		_.each(listData, function(data) {
			isCity = data.value ? true : false;
		})
		if (!isCity) listData = [];

		var newArr = [];
		_.each(listData, function(data) {
			!data.value ? newArr.push(data.label) : '';
		})

		_.each(newArr, function(country) {
			var hasCity = false;
			for (var i = 0; i < listData.length; i++) {
				if (listData[i].parent === country) {
					hasCity = true;
					break;
				}
			}
			if (!hasCity) {
				_.each(listData, function(data, i) {
					if (data.label === country) {
						listData.splice(i, 1);
					}
				});
			}
		});

		return listData;

	}

	var resetAvailableFlight = function() {

		$('[data-autocomplete]').each(function() {
			var _self = $(this),
				inputEl = _self.find('input'),
				clearTextEl = _self.find('.add-clear-text'),
				timeout = null;

			if (timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function() {
				if (_self.data('origin')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							originSelected = '';
							countryOriginSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						originSelected = '';
						countryOriginSelected = '';
					});
				}
				if (_self.data('destination')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							destinationSelected = '';
							countryDestinationSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						destinationSelected = '';
						countryDestinationSelected = '';
					});
				}
			}, 1000);

		});
	};

	// init
	initAutocompleteCity();
};
