/**
 * @name SIA
 * @description Define global KFMileHowToUse functions
 * @version 1.0
 */

SIA.KFMileHowToUse = function(){
  var global = SIA.global;
  var config = global.config;
  var flightsMiles = $('.block--flights-miles');
  var results = $('[data-results]', flightsMiles);
  var seemore = $('[data-see-more]', results);
  var lastShowItemIdx = 0;
  var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;

  var ajaxShowRedemptionCalculator = function(){
    var form = $('.form-calculate-miles-2');
    var calculateBtn = $('#calculate');
    var accrualProcessClass = '.accrual-calculator-process';
    var accrualResultClass = '.accrual-calculator-result';
    var process = calculateBtn.closest(accrualProcessClass);

    var renderTemplate = function(data){
      $.get(config.url.kfTableTemplate, function (html) {
        if(process.siblings(accrualResultClass).length){
          process.siblings(accrualResultClass).remove();
        }
        if(data){
          var template = window._.template(html, {
            data: data
          });
          $(template).insertAfter(process);

        }
      });
    };

    form.validate({
      focusInvalid: true,
      errorPlacement: global.vars.validateErrorPlacement,
      success: global.vars.validateSuccess,
      ignore: ':hidden, [data-ignore]',
      onfocusout: function(el){
        global.vars.validateOnfocusout.call(this, el);
      },
      submitHandler: function(){
        $.ajax({
          url: config.url.kfUseTableJSON,
          dataType: 'json',
          type: global.config.ajaxMethod,
          success: function(data) {
            renderTemplate(data);
            seemore.removeClass('hidden').removeAttr('style');
          },
          error: function(xhr, status) {
            if(status !== 'abort') {
              window.alert(L10n.flightSelect.errorGettingData);
            }
          }
        });
        return false;
      }
    });
  };

  var crossFadeConverLightbox = function(){
    var convertLightbox = $('.popup--convert');
    var convertTab = $('.popup__content > div', convertLightbox);
    var btn = $('.button-group-1 :input', convertLightbox);
    var btnSubmit = $('[data-button-submit]', convertLightbox);
    var formBtnSubmit = btn.eq(0).closest('form');

    var category = convertLightbox.find('[data-category]');
    var partner = convertLightbox.find('[data-partner]');
    var membership = convertLightbox.find('[data-membership]');
    var kfMile = convertLightbox.find('[data-kf-mile]');
    var vffpoint = convertLightbox.find('[data-vff-points]');
    var upCategory = convertLightbox.find('[data-category-update]');
    var upCartner = convertLightbox.find('[data-partner-update]');
    var upMembership = convertLightbox.find('[data-membership-update]');
    var upConvert = convertLightbox.find('[data-update-convert]');
    var rule = convertLightbox.find('[data-rule-kff]');
    var allcustomSelect = convertLightbox.find('.custom-select');

    var show = function(popupShow, popupHide){
      popupShow.removeClass('hidden').css({
        'position': 'absolute',
        'top': 40,
        'left': 40,
        'opacity': 0,
        'width': 570
      }).animate({
        'opacity': 1
      }, 300, function(){
        popupShow.removeAttr('style');
        var popupContent = convertLightbox.find('.popup__content');
        popupHide.hide();
        popupContent.removeAttr('style').css({
          'boxSizing': 'border-box',
          'height' : popupShow.outerHeight() + 80
        });
        convertLightbox.Popup('reposition');
      });

      popupHide.css('visibility', 'hidden').animate({
        'opacity': 0
      },200);
    };

    formBtnSubmit.validate({
      focusInvalid: true,
      errorPlacement: global.vars.validateErrorPlacement,
      success: global.vars.validateSuccess
    });

    btn.eq(0).off('click.crossFade').on('click.crossFade', function(e){
      e.preventDefault();
      formBtnSubmit.data('validatedOnce', true);
      if(formBtnSubmit.valid()){
        show(convertTab.eq(1), convertTab.eq(0));
        upCategory.text(category.find(':selected').text());
        upCartner.text(partner.find(':selected').text());
        upMembership.text(membership.val());
        // console.log(vffpoint.val(), kfMile.val(), L10n.convert.template.format(kfMile.val(), vffpoint.val()));
        upConvert.text(L10n.convert.template.format(kfMile.val(), vffpoint.val()));
      }
    });

    convertLightbox.off('afterHide.reset').on('afterHide.reset', function(){
      convertTab.eq(1).removeAttr('style').addClass('hidden');
      convertTab.eq(0).removeAttr('style');
      convertLightbox.find('.popup__inner').removeAttr('style');
      convertLightbox.find('form').get(0).reset();
      allcustomSelect.customSelect('refresh');
      category.closest('[data-customselect]').trigger('afterSelect.updateVFFPoint');
    });

    btnSubmit.off('click.submitComplete').on('click.submitComplete', function() {
      show(convertTab.eq(2), convertTab.eq(1));
    });

    convertLightbox.off('afterHide.backToUseMile').on('afterHide.backToUseMile', function() {
      convertTab.eq(2).addClass('hidden');
    });

    var convertVFF = function(){
      var rawData = {
        'standard': 10,
        'data':[
          {
            'category': [10, 9, 8, 6, 1]
          },
          {
            'category': [7, 9, 6, 7, 1]
          },
          {
            'category': [10, 6, 7, 8, 1]
          }
        ]
      };
      var doConvert = function(standardKF, standardP, arbitrary){
        var value = isNaN(Number(arbitrary)) ? 0 : (arbitrary*standardP)/standardKF;
        return value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
      };

      // category.closest('[data-customselect]').off('afterSelect.updateVFFPoint').on('afterSelect.updateVFFPoint', function(){
      //  vffpoint.val(doConvert(rawData.standard,  rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()], (kfMile.val().replace(/,|_/g, '') * 1)));
      //  rule.text(L10n.convert.template.format(rawData.standard, rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()]));
      // });
      // partner.closest('[data-customselect]').off('afterSelect.updateVFFPoint').on('afterSelect.updateVFFPoint', function(){
      //  vffpoint.val(doConvert(rawData.standard,  rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()], (kfMile.val().replace(/,|_/g, '') * 1)));
      //  rule.text(L10n.convert.template.format(rawData.standard, rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()]));
      // });

      // kfMile.off('change.updateVFFPoint').on('change.updateVFFPoint', function(){
      //  vffpoint.val(doConvert(rawData.standard,  rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()], (kfMile.val().replace(/,|_/g, '') * 1)));
      //  rule.text(L10n.convert.template.format(rawData.standard, rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()]));
      // }).off('keyup.updateVFFPoint').on('keyup.updateVFFPoint', function(){
      //  kfMile.val(kfMile.val().replace(/,/g, ''));
      //  kfMile.val(kfMile.val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
      //  vffpoint.val(doConvert(rawData.standard,  rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()], (kfMile.val().replace(/,|_/g, '') * 1)));
      //  rule.text(L10n.convert.template.format(rawData.standard, rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()]));
      // });
      // rule.text(L10n.convert.template.format(rawData.standard, rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()]));
      var getConvertResult = function(){
        var cPartner = rawData.data[category.find(':selected').index()].category[partner.find(':selected').index()];
        var plural = cPartner > 1 ? L10n.convert.templatePoints : L10n.convert.template;
        vffpoint.val(doConvert(rawData.standard,  cPartner, (kfMile.val().replace(/,|_/g, '') * 1)));
        rule.text(plural.format(rawData.standard, cPartner));
      };

      category.closest('[data-customselect]').off('afterSelect.updateVFFPoint').on('afterSelect.updateVFFPoint', function(){
        getConvertResult();
      });

      partner.closest('[data-customselect]').off('afterSelect.updateVFFPoint').on('afterSelect.updateVFFPoint', function(){
        getConvertResult();
      });

      kfMile.off('change.updateVFFPoint').on('change.updateVFFPoint', function(){
        getConvertResult();
      }).off('keyup.updateVFFPoint').on('keyup.updateVFFPoint', function(){
        kfMile.val(kfMile.val().replace(/,/g, ''));
        kfMile.val(kfMile.val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
        getConvertResult();
      });

      getConvertResult();
    };

    convertVFF();
  };

  var milesFilter = function(){
    var flight = $('[data-city] select', flightsMiles);
    var award = $('[data-award] select', flightsMiles);
    var journey = $('[data-journey] select', flightsMiles);
    var classEl = $('[data-class] select', flightsMiles);
    var title = $('.sub-heading-1--dark', flightsMiles);
    var allData = [];
    var dataHolder = [];
    var page = 1;
    var limitItem = 6;
    var itemsLength;
    var currentItem = 0;

    var slide = $('#slider-range');
    var cMin = slide.data('current-min');
    var cMax = slide.data('current-max');
    var labelMin = slide.find('.ui-slider_from');
    var labelMax = slide.find('.ui-slider_to');
    var rangeUI;

    var detectOverlap = function(to, range, handle){
      return Math.abs(handle*to/range);
    };

    slide.slider({
      range: true,
      values: [1600, 2000],
      min: cMin,
      max: cMax,
      slide: function(event, ui){

        // if((ui.values[0] + detectOverlap(dataHolder.maxFare, slide.width(), $(ui.handle).width())) > ui.values[1]) {
        //  return false;
        // }

        labelMin.text(accounting.formatMoney(ui.values[0], '', 0, ',', '.'));
        labelMax.text(accounting.formatMoney(ui.values[1], '', 0, ',', '.'));
      },
      change: function(event, ui){
        rangeUI = ui.values;
      },
      stop: function(event, ui){
        labelMin.text(accounting.formatMoney(ui.values[0], '', 0, ',', '.'));
        labelMax.text(accounting.formatMoney(ui.values[1], '', 0, ',', '.'));
        rangeUI = ui.values;
        page = 1;
        currentItem = 0;
        sliderFilter(dataHolder, ui.values, true, true, false, {rangeUI : rangeUI, event : event});
      }
    });

    var getIndex = function(value){
      var cl = {
        idx: 0,
        con: false
      };
      for(var i = 0; i < allData.promoVO.length; i ++){
        if(allData.promoVO[i].city === value){
          cl = {
            idx: i,
            con: true
          };
        }
      }
      return cl;
    };

    var sliderFilter = function(data, ui, isReset, isSeeall, isSeemore, sliderValue){
      var dataFilter = {
        cityVO:[]
      };

      for(var i = 0; i < data.cityVO.length; i++){
        var cityVO = data.cityVO[i];
        if(Number(cityVO.miles) <= ui[1] && Number(cityVO.miles) >= ui[0]){
          if(award.val().toLowerCase() === 'all' && journey.val().toLowerCase() === 'all' &&
            classEl.val().toLowerCase() === 'all'){
            dataFilter.cityVO.push(cityVO);
          }
          else {
            if(award.val().toLowerCase() === 'all' && classEl.val().toLowerCase() === 'all' &&
              cityVO.tripType === journey.val()){
              dataFilter.cityVO.push(cityVO);
            }
            else if(journey.val().toLowerCase() === 'all' && classEl.val().toLowerCase() === 'all' &&
              cityVO.awardType === award.val()){
              dataFilter.cityVO.push(cityVO);
            }
            else if(award.val().toLowerCase() === 'all' && journey.val().toLowerCase() === 'all' &&
              cityVO.flighttype === classEl.val()) {
              dataFilter.cityVO.push(cityVO);
            }
            else if (award.val().toLowerCase() === 'all' && cityVO.tripType === journey.val() &&
              cityVO.flighttype === classEl.val()) {
              dataFilter.cityVO.push(cityVO);
            }
            else if (journey.val().toLowerCase() === 'all' && cityVO.awardType === award.val() &&
              cityVO.flighttype === classEl.val()) {
              dataFilter.cityVO.push(cityVO);
            }
            else if (classEl.val().toLowerCase() === 'all' && cityVO.awardType === award.val() &&
              cityVO.tripType === journey.val()) {
              dataFilter.cityVO.push(cityVO);
            }
            else if(cityVO.awardType === award.val() && cityVO.tripType === journey.val() &&
              cityVO.flighttype === classEl.val()){
              dataFilter.cityVO.push(data.cityVO[i]);
            }
          }
        }
      }


      // if (dataFilter.cityVO.length > limitItem) {
      //  seemore.removeClass('hidden').removeAttr('style');
      // };

      buildHtml(isReset, dataFilter, isSeemore, sliderValue);


      if(seemore.css('display') === 'inline'){
        seemore.removeAttr('style');
      }

    };

    var renderCityTemplate = function(el, json){
      el.empty();
      var options = [];
      for(var i = 0; i < json.promos.city.length; i++){
        options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.promos.city[i].description +'"' + '>' + json.promos.city[i].description + '</option>');
      }
      el.html(options.join(''));
      if(el.closest('[data-customSelect]').data('customSelect')){
        el.closest('[data-customSelect]').customSelect('_createTemplate').customSelect('refresh');
      }
    };

    var renderAwardTemplate = function(el, json){
      el.empty();
      var options = [];
      for(var i = 0; i < json.promos.award.length; i++){
        options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.promos.award[i] +'"' + '>' + json.promos.award[i] + '</option>');
      }
      el.html(options.join(''));
      if(el.closest('[data-customSelect]').data('customSelect')){
        el.closest('[data-customSelect]').customSelect('_createTemplate').customSelect('refresh');
      }
    };

    var renderClassTemplate = function(el, json){
      el.empty();
      var options = [];
      for(var i = 0; i < json.promos.class.length; i++){
        options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' +
          json.promos.class[i] +'"' + '>' + json.promos.class[i] + '</option>');
      }
      el.html(options.join(''));
      if(el.closest('[data-customSelect]').data('customSelect')){
        el.closest('[data-customSelect]').customSelect('_createTemplate').customSelect('refresh');
      }
    };

    var renderJourneyTemplate = function(el, json){
      el.empty();
      var options = [];
      for(var i = 0; i < json.promos.journey.length; i++){
        options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.promos.journey[i] +'"' + '>' + json.promos.journey[i] + '</option>');
      }
      el.html(options.join(''));
      if(el.closest('[data-customSelect]').data('customSelect')){
        el.closest('[data-customSelect]').customSelect('_createTemplate').customSelect('refresh');
      }
    };

    var renderTemplate = function(data, isReset){
      if(data.cityVO.length){
        // slide.slider( 'option', 'min', data.minFare );
        // slide.slider( 'option', 'max', data.maxFare );
        // slide.slider( 'option', 'values', data.initValue );
        // labelMin.text(accounting.formatMoney(data.minFare, '', 0, ',', '.') + ' ' + L10n.unit.miles);
        // labelMax.text(accounting.formatMoney(data.maxFare, '', 0, ',', '.') + ' ' + L10n.unit.miles);
        labelMin.text(accounting.formatMoney(data.initValue[0], '', 0, ',', '.'));
        labelMax.text(accounting.formatMoney(data.initValue[1], '', 0, ',', '.'));
        slide.slider( 'option', {
          min: data.minFare,
          max: data.maxFare,
          values: data.initValue
        });
      }
      sliderFilter(data, rangeUI, isReset);
    };

    var buildHtml = function(isReset, items, isSeemore, sliderValue){

      var sliderV = $('#slider-range').slider('option', 'values');

      var limit = page * limitItem;
      itemsLength = items.cityVO.length;
      var dataFilter = {
        cityVO:[]
      };
      var wrapper = results.find('.items');

      if(itemsLength < limit){
        limit = itemsLength;
      }

      if(itemsLength <= (page * limitItem)){
        seemore.addClass('hidden');
        setTimeout(function() {
          seemore.text('see more');
        }, 200);
      }
      else{
        seemore.removeClass('hidden');
      }

      for(currentItem; currentItem < limit; currentItem++){
        dataFilter.cityVO.push(items.cityVO[currentItem]);
        if(currentItem === limit - 1){
          page++;
        }
      }
      for (var k in dataFilter.cityVO) {
        dataFilter.cityVO[k].milesText = accounting.formatMoney(dataFilter.cityVO[k].miles, '', 0, ',', '.');
      }

      $.get(config.url.kfMilesTemplate, function (html) {
        if(results.find('.empty-alert')){
          results.find('.empty-alert').remove();
        }

        if(isReset){
          if(wrapper.length){
            wrapper.remove();
            wrapper = results.find('.items');
          }
        }

        if(items.cityVO.length){
          var template = window._.template(html, {
            data: dataFilter
          });

          if(wrapper.length){
            $(template).children().appendTo(wrapper);
          }
          else{
            $(template).prependTo(results);
          }

          flightsMiles.removeClass('block--shadow');
        }
        else{
          flightsMiles.removeClass('block--shadow').addClass('block--shadow');
          $('<p class="empty-alert">'+ L10n.kfMiles.nodata +'</p>').prependTo(results);
        }

        wcag();

        if(isSeemore == undefined || !isSeemore) {

          if(sliderValue != undefined) {
            $('#aria-use-filter').text('');
            $('#aria-use-filter').text(L10n.wcag.sliderLabel.format(sliderV[0], sliderV[1])+ ', '+ L10n.wcag.foundLabel.format(itemsLength));
          }
          else {
            $('#aria-use-filter-dropdown').text('');
            $('#aria-use-filter-dropdown').text(L10n.wcag.foundLabel.format(itemsLength));
          }
        } else if(isSeemore) {
          $('#wcag-seemore').text('');
          wcagSeeMore(dataFilter.cityVO.length);
        }


        if(lastShowItemIdx > 0) {
          results.find('.flight-item').eq(lastShowItemIdx).focus();
        }
      });
    };

    seemore.off('click.seemore').on('click.seemore', function(e){
      var isSeeAll = false;
      lastShowItemIdx = results.find('.flight-item').length;
      if(page === 3){
        setTimeout(function() {
          $(this).text('See All');
        }.bind(this), 200);
      }
      if(page > 3){
        isSeeAll = true;
        page = dataHolder.cityVO.length;
      }
      e.preventDefault();
      sliderFilter(dataHolder, rangeUI, false, isSeeAll, true);
    });

    flight.closest('[data-customSelect]').off('afterSelect.changeCity').on('afterSelect.changeCity', function(){
      page = 1;
      currentItem = 0;
      if(getIndex(flight.val()).con){
        dataHolder = allData.promoVO[getIndex(flight.val()).idx];
      }
      else{
        dataHolder = {
          cityVO: []
        };
      }
      // title.html(allData.promos.description[flight.prop('selectedIndex')]);
      // title.html('');
      renderTemplate(dataHolder, true);
    });

    award.closest('[data-customSelect]').off('afterSelect.changeCity').on('afterSelect.changeCity', function(){
      page = 1;
      currentItem = 0;
      sliderFilter(dataHolder, rangeUI, true);
    });

    journey.closest('[data-customSelect]').off('afterSelect.changeCity').on('afterSelect.changeCity', function(){
      page = 1;
      currentItem = 0;
      sliderFilter(dataHolder, rangeUI, true);
    });

    classEl.closest('[data-customSelect]').off('afterSelect.changeCity').on('afterSelect.changeCity', function(){
      page = 1;
      currentItem = 0;
      sliderFilter(dataHolder, rangeUI, true);
    });

    results.on('click.favourite', '.flight-item__favourite', function(e){
      e.preventDefault();
      setTimeout(function(){
        $('#aria-favourite-star').text('');
      }, 100);
      var self = $(this);
      $.ajax({
        url: 'ajax/success.json',
        dataType: 'json',
        type: global.config.ajaxMethod,
        beforeSend: function() {
          self.find('.ico-star').addClass('hidden');
          self.find('.loading').removeClass('hidden');
        },
        success: function(data) {
          if(data.success){
            if(self.hasClass('favourited')){
              self.removeClass('favourited');
              self.closest('.items').find('.ui-helper-hidden-accessible').text('');
              wcagStar(self, false);
              self.attr({
                'aria-label': L10n.wcag.unfavouriteLable
              });
            }
            else{
              self.addClass('favourited');
              self.closest('.items').find('.ui-helper-hidden-accessible').text('');
              wcagStar(self, true);
              self.attr({
                'aria-label': L10n.wcag.favouriteLable
              });
            }

            self.find('.ico-star').removeClass('hidden');
            self.find('.loading').addClass('hidden');
          }
        },
        error: function(xhr, status) {
          if(status !== 'abort') {
            window.alert(L10n.flightSelect.errorGettingData);
          }
        }
      });
    });

    $.ajax({
      url: 'ajax/kf-miles.json',
      dataType: 'json',
      type: global.config.ajaxMethod,
      success: function(data) {
        results.find('.items').remove();
        allData = data;
        dataHolder = allData.promoVO[0];
        title.html(allData.promos.description[0]);
        renderCityTemplate(flight, data);
        renderAwardTemplate(award, data);
        renderClassTemplate(classEl, data);
        renderJourneyTemplate(journey, data);
        renderTemplate(dataHolder, true);
        // seemore.removeClass('hidden').removeAttr('style');
      },
      error: function(xhr, status) {
        if(status !== 'abort') {
          window.alert(L10n.flightSelect.errorGettingData);
        }
      }
    });

  };

  var resetCustomeSelect = function(){
    var calculateMilesForm = $('.form-calculate-miles-2');
    calculateMilesForm.find('.custom-select input:text').val('');
  };

  var initVoucherPopup = function(){

    var btnVoucher = $('[data-trigger-popup=".popup--voucher-details"]');
    var popupVoucher = $(btnVoucher.data('trigger-popup'));
    var voucherDetailsLightbox = $('.popup--voucher-details');
    var firstName = $('.text-intro__first-name', voucherDetailsLightbox);
    var lastName  = $('.text-intro__last-name', voucherDetailsLightbox);
    var requestId = $('.text-intro__request-id', voucherDetailsLightbox);
    var partnerAirlines = $('.text-intro__partner', voucherDetailsLightbox);
    var downloadLink = $('.hidden-tb li:nth-child(1) a', voucherDetailsLightbox);
    var totalMilesRedeemed = $('.prefer-result sub + span', voucherDetailsLightbox);
    var partnerAirlinesLink = $('.prefer-result + p a', voucherDetailsLightbox);
    var voucherReceiptInfoLst = $('.voucher-detail-table tbody', voucherDetailsLightbox);
    var voucherAllData = [];
    var voucherDataHolder = [];

    var ajaxSuccess = function(res){
      if(res) {
        var renderVoucherTable = function(el, json){
          el.empty();
          var row = [];
          for(var i = 0; i < json.length; i++){
            var tdValue = '<td><span>$' + json[i].SGDvalue + ' x ' + json[i].count + '</span></td>';
            var tdNumber = '<td class="table-row" data-th="Numbers"><span>' + json[i].number + '</span></td>';
            var tdExpiryDate = '<td class="table-row" data-th="Expiry Date"><span>' + json[i].expiryDate + '</span></td>';
            var tdMilesRedeemed = '<td class="align-right"><span>' + json[i].milesRedeemed +'</span></td>';
            row.push('<tr>'+ tdValue + tdNumber + tdExpiryDate + tdMilesRedeemed + '</tr>');
          }
          el.html(row.join(''));
        };
        voucherAllData = res;
        voucherDataHolder = voucherAllData.KFRedemptionVoucherJsonVO;
        firstName.html(voucherDataHolder.firstname);
        lastName.html(voucherDataHolder.lastname);
        requestId.html(voucherDataHolder.requestId);
        partnerAirlines.html(voucherDataHolder.partnerAirlines);
        downloadLink.attr('href', voucherDataHolder.downLoadLink);
        totalMilesRedeemed.html(voucherDataHolder.totalMilesRedeemed);
        partnerAirlinesLink.attr('href', voucherDataHolder.partnerAirlinesLink);
        renderVoucherTable(voucherReceiptInfoLst, voucherDataHolder.voucherReceiptInfoLst);
        popupVoucher.Popup('show');
      }
    };

    var ajaxFail = function(jqXHR, textStatus) {
      console.log(textStatus);
    };

    var initAjax = function(url, data, type, successFunc, errorFunc) {
      type = type || 'json';
      successFunc = successFunc || ajaxSuccess;
      errorFunc = errorFunc || ajaxFail;
      $.ajax({
        url: url,
        type: global.config.ajaxMethod,
        dataType: type,
        data: data,
        success: successFunc,
        error: errorFunc
      });
    };

    if(popupVoucher.length) {
      popupVoucher.data('Popup').options.beforeShow = function() {
        initAjax(global.config.url.kfVoucherSummaryJSON);
      };
    }

  };

  var wcagStar = function(starEl, favourite) {
   var wrapper = starEl.closest('.items'),
       wcagStarEl = starEl.find('.ui-helper-hidden-accessible'),
       wcagStarText = favourite === true ?
                     L10n.wcag.favouriteLable :
                     L10n.wcag.unfavouriteLable;

     wcagStarEl.text(wcagStarText);
  };

  var wcagSeeMore = function(count) {
    var status = seemore.parent().find('#wcag-seemore');
    status.text(L10n.wcag.seemoreLabel.format(count));

  };

  var wcag = function() {
    var wrapper = results.find('.items'),
        wcagStarEl = wrapper.find('#wcag-star'),
        flightList = wrapper.find('.flight-item');
    if(seemore.parent().find('#wcag-seemore').length <= 0) {
      seemore.parent().append('<span id="wcag-seemore" role="status" aria-atomic="true" aria-live="assertive" class="ui-helper-hidden-accessible"></span>');
    }

    if($('#aria-use-filter').length === 0) {
      $('.flights-list').prepend('<span class="ui-helper-hidden-accessible" id="aria-use-filter" aria-atomic="true" aria-live="assertive"></span>')
    };

    if($('#aria-use-filter-dropdown').length === 0) {
      $('.flights-list').prepend('<span class="ui-helper-hidden-accessible" id="aria-use-filter-dropdown" aria-atomic="true" aria-live="assertive"></span>')
    };

    flightList.each(function(flightIdx, e){
     var flight = $(e),
         star = flight.find('.flight-item__favourite'),
         favouriteLable = '',
         spanEl = $('<span role="status" aria-live="polite" class="ui-helper-hidden-accessible">'+ favouriteLable +'</span>');

     flight.attr({
       'tabindex' : 0
     });

     if(star.is('.favourited')) {
       favouriteLable = L10n.wcag.favouriteLable;
     } else {
       favouriteLable = L10n.wcag.unfavouriteLable;
     }
     spanEl.appendTo(star);
     spanEl.text(favouriteLable);

    });
  };


  var initModule = function() {
    // init function
    ajaxShowRedemptionCalculator();
    crossFadeConverLightbox();
    milesFilter();
    resetCustomeSelect();
    initVoucherPopup();
  };

  initModule();
};
