/**
 * @name SIA
 * @description Define global flightSchedule functions
 * @version 1.0
 */
SIA.flightSchedule = function(){
	var global = SIA.global;
	var config = global.config;
	// var win = global.vars.win;
	var noJsonTemplate = '<div class="alert-block checkin-alert error-alert">' +
		'<div class="inner">' +
		'<div class="alert__icon"><em class="ico-close-round-fill"></em></div>' +
		'<div class="alert__message">' + L10n.flightStatus.nodata + '</div>'+
		'</div>' +
		'</div>';
	var flightSchedulesPage = $('.flight-schedules-page');

	if(!flightSchedulesPage.length){
		return;
	}

	var tabContents = flightSchedulesPage.find('.flight-schedules .tab-content');

	// var flightScheduleTabPopup = function(hdlers, ppup, closePpup, pContent){
	// 	var handlers = hdlers,
	// 		popup = ppup;

	// 	if(handlers.length && popup.length){
	// 		handlers.each(function(index, el){
	// 			global.vars.popupGesture(popup, $(el), closePpup, pContent, true);
	// 		});
	// 	}
	// };

	// var activeSchedule = flightSchedulesPage.find('.flight-schedules .tab-content.active');
	// var deactiveSchedule = flightSchedulesPage.find('.flight-schedules .tab-content:not(".active")');
	// var activeTab = flightSchedulesPage.find('.flight-schedules .tab [data-outbound]');
	// var tabInbound = flightSchedulesPage.find('.flight-schedules .tab [data-inbound]');

	// var fillContent = function(res){
	// 	$.get(config.url.flightScheduleTemplate, function (data) {
	// 		var template = window._.template(data, {
	// 			data: res
	// 		});
	// 		contentSchedule.html(template);

	// 		// init after render html template
	// 		var popupAvailable = $('.popup--check-available');
	// 		var triggerCheckAvailable = contentSchedule.find('.schedule-check-availability');
	// 		// var tableDate = contentSchedule.find('.tablet-mobile table.table-date');
	// 		// var timerSetHeightFlightSchedule = 0;
	// 		// var next = contentSchedule.find('.button-group .btn--next');
	// 		// var prev = contentSchedule.find('.button-group .btn--prev');
	// 		var flyingFocus = $('#flying-focus');

	// 		if(!popupAvailable.data('Popup')){
	// 			popupAvailable.Popup({
	// 				overlayBGTemplate: config.template.overlay,
	// 				modalShowClass: '',
	// 				triggerCloseModal: '.popup__close',
	// 				afterShow: function(){
	// 					flyingFocus = $('#flying-focus');
	// 					if(flyingFocus.length){
	// 						flyingFocus.remove();
	// 					}
	// 				}
	// 			});
	// 		}

	// 		// next.off('click.nextflight').on('click.nextflight', function(e){
	// 		// 	e.preventDefault();
	// 		// 	fillContent(globalJson.flightSchedulePrev);
	// 		// 	setTimeout(function(){
	// 		// 		$('html,body').animate({
	// 		// 			'scrollTop': 0
	// 		// 		});
	// 		// 	}, 200);
	// 		// });
	// 		// prev.off('click.nextflight').on('click.nextflight', function(e){
	// 		// 	e.preventDefault();
	// 		// 	fillContent(globalJson.flightScheduleNext);
	// 		// 	setTimeout(function(){
	// 		// 		$('html,body').animate({
	// 		// 			'scrollTop': 0
	// 		// 		});
	// 		// 	}, 200);
	// 		// });

	// 		// win.off('resize.setHeightFlightSchedule').on('resize.setHeightFlightSchedule', function(){
	// 		// 	clearTimeout(timerSetHeightFlightSchedule);
	// 		// 	timerSetHeightFlightSchedule = setTimeout(function(){
	// 		// 		tableDate.each(function(idx){
	// 		// 			var tr = $(this).find('tr');
	// 		// 			if(win.width() < config.tablet - config.width.docScroll && win.width() >= config.mobile - config.width.docScroll){
	// 		// 				tr.eq(1).height($(this).prev().outerHeight() - (idx ? 0 : tr.eq(0).height()));
	// 		// 			}
	// 		// 			else{
	// 		// 				tr.eq(1).removeAttr('style');
	// 		// 			}
	// 		// 		});
	// 		// 	},150);
	// 		// }).trigger('resize.setHeightFlightSchedule');


	// 		triggerCheckAvailable.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
	// 			e.preventDefault();
	// 			popupAvailable.Popup('show');
	// 		});

	// 	}, 'html');
	// };

	var fillContent = function(res, contentSchedule){
		contentSchedule.html(config.template.loadingMedium);
		$.get(config.url.flightScheduleTemplate, function (data) {
			var template = window._.template(data, {
				data: res
			});
			contentSchedule.html(template);

			contentSchedule.off('click.showInformation').on('click.showInformation', '.flights--detail span',function(){
				var infFlightDetail = $(this).next();
				var self = $(this);
				// var flightsDetail = self.closest('.flights--detail');

				if(infFlightDetail.is('.hidden')) {
					infFlightDetail.removeClass('hidden').hide();
				}

				self.toggleClass('active');
				// if(flightsDetail.data('infor')){
				infFlightDetail.stop().slideToggle(400);
				// }
				// else{
				// 	if(!infFlightDetail.is(':visible')) {
				// 		self.children('em').addClass('hidden');
				// 		self.children('.loading').removeClass('hidden');
				// 		$.ajax({
				// 			url: global.config.url.flightSearchFareFlightInfoJSON,
				// 			dataType: 'json',
				// 			type: global.config.ajaxMethod,
				// 			data: {
				// 				flightNumber: self.parent().data('flight-number'),
				// 				carrierCode: self.parent().data('carrier-code'),
				// 				date: self.parent().data('date'),
				// 				origin: self.parent().data('origin')
				// 			},
				// 			success: function(data) {
				// 				var flyingTime = '';
				// 				for(var ft in data.flyingTimes){
				// 					flyingTime = data.flyingTimes[ft];
				// 				}
				// 				var textAircraftType = L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType;
				// 				var textFlyingTime = L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime;
				// 				var rowAircraftType = $('<p>' + textAircraftType + '</p>');
				// 				var rowFlyingTime = $('<p>' + textFlyingTime + '</p>');
				// 				infFlightDetail.empty();
				// 				infFlightDetail.append(rowAircraftType, rowFlyingTime);
				// 				infFlightDetail.stop().slideToggle(400);
				// 			},
				// 			error: function(xhr, status) {
				// 				if(status !== 'abort'){
				// 					window.alert(L10n.flightSelect.errorGettingData);
				// 				}
				// 			},
				// 			complete: function() {
				// 				self.children('em').removeClass('hidden');
				// 				self.children('.loading').addClass('hidden');
				// 			}
				// 		});
				// 	}
				// 	else {
				// 		infFlightDetail.stop().slideToggle(400);
				// 	}
				// }
			});

			// init after render html template
			var popupAvailable = $('.popup--check-available');
			var triggerCheckAvailable = contentSchedule.find('.schedule-check-availability');
			var flyingFocus = $('#flying-focus');

			if(!popupAvailable.data('Popup')){
				popupAvailable.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close',
					afterShow: function(){
						flyingFocus = $('#flying-focus');
						if(flyingFocus.length){
							flyingFocus.remove();
						}
					},
					beforeHide: function() {
						popupAvailable.find('[data-customselect]').customSelect('hide');
					}
					// closeViaOverlay: false
				});
			}

			triggerCheckAvailable.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
				e.preventDefault();
				popupAvailable.Popup('show');
			});
		});
	};

	var moreSevenTabs = function(){
		var wrapperTab = $('[data-wrapper-tab]', flightSchedulesPage),
			selectTab = wrapperTab.find('[data-select-tab]');
		if(wrapperTab.length){
			var customSelectEl = wrapperTab.find('[data-customselect]');
			if(customSelectEl.length){
				var	selectEl = customSelectEl.find('select'),
					wrapCust = customSelectEl.parent(),
					tabs = customSelectEl.closest('.tab').find('> li'),
					dataCustomSel = customSelectEl.data('customSelect'),
					indexOfFakeTab = customSelectEl.closest('.limit-item').index(),
					curIndex = dataCustomSel.element.curIndex;

				var changeIcon = function(){
					var displayTxtEl = customSelectEl.find(dataCustomSel.options.customText),
						txt = selectEl.find('option:selected').text(),
						txtReplace = customSelectEl.data('replaceTextByPlane'),
						regx = new RegExp(txtReplace, 'gi');
					displayTxtEl.html(txt.replace(regx, '<em class="ico-plane"></em>'));
					customSelectEl.siblings('.mark-desktop').html(txt.replace(regx, '<em class="ico-plane"></em>') + '<em class="ico-dropdown"></em>');
				};

				var showTab = function(){
					curIndex = dataCustomSel.element.curIndex;
					tabs.eq(curIndex + indexOfFakeTab).trigger('click.show');
					wrapCust
						.siblings('.active').removeClass('active')
						.end()
						.addClass('active');
					changeIcon();
				};
				customSelectEl.off('beforeSelect.triggerTab').on('beforeSelect.triggerTab', function(){
					showTab();
				});
				customSelectEl.off('afterSelect.triggerTab').on('afterSelect.triggerTab', function(){
					showTab();
				});
				wrapperTab.off('afterChange.openTab').on('afterChange.openTab', function(){
					if(selectTab.prop('selectedIndex') >= indexOfFakeTab){
						selectEl.prop('selectedIndex', selectTab.prop('selectedIndex') - indexOfFakeTab);
						customSelectEl.customSelect('refresh');
						wrapCust
							.siblings('.active').removeClass('active')
							.end()
							.addClass('active');
					}
					changeIcon();
				});
				changeIcon();
			}
		}
	};

	if(globalJson.flightSchedule) {
		// if(globalJson.flightSchedule.outBoundFlight) {
		// 	fillContent(globalJson.flightSchedule.outBoundFlight, activeSchedule, 'outBoundFlight');
		// }
		// if(globalJson.flightSchedule.inBoundFlight) {
		// 	fillContent(globalJson.flightSchedule.inBoundFlight, deactiveSchedule, 'inBoundFlight');
		// }
		flightSchedulesPage.find('.flight-schedules .tab-content').each(function(idx){
			var tabContent = $(this);
			var res = globalJson.flightSchedule.boundList[idx];
			if(res){
				fillContent(res, tabContent);
			}
			else {
				tabContent.html(noJsonTemplate);
			}
		});
		moreSevenTabs();
	}
	else {
		window.noJsonHandler = true;
		tabContents.html(noJsonTemplate);
	}
};
