/**
 * @name SIA
 * @description Define function to carousel fade
 * @version 1.0
 */
SIA.carouselFade = function() {
	// var vars = SIA.global.vars;
	// var win = vars.win;
	// var global = SIA.global;
	// var config = global.config;
	var carouselFade = $('[data-carousel-fade]');
	// var componentCarouselBGD = $('.component-carousel__background');

	if(carouselFade.length){
		// var resizeFullBanner = function(prImg){
		// 	if(vars.detectDevice.isTablet() && prImg.data('tablet-bg')){
		// 		prImg.css({
		// 			'backgroundPosition': prImg.data('tablet-bg')
		// 		});
		// 	}
		// 	else if(window.innerWidth > config.tablet){
		// 		if(prImg.data('desktop')){
		// 			prImg.css('backgroundPosition', prImg.data('desktop'));
		// 		}
		// 		else{
		// 			prImg.css('backgroundPosition', '');
		// 		}
		// 	}
		// };

		// componentCarouselBGD.each(function() {
		// 	var prImg = $(this);

		// 	prImg.css({
		// 		'backgroundImage': 'url(' + prImg.find('img').attr('src') + ')'
		// 	});

		// 	prImg.find('img').attr('src', config.imgSrc.transparent);

		// 	if (prImg.is('.component-carousel__background')) {
		// 		prImg.removeClass('visibility-hidden');
		// 	}
		// 	resizeFullBanner(prImg);
		// });

		// win.off('resize.fullBannerImg').on('resize.fullBannerImg', function(){
		// 	componentCarouselBGD.each(function() {
		// 		resizeFullBanner($(this));
		// 	});
		// });

		carouselFade.each(function(){
			var carousel = $(this);
			var option = carousel.data('option') ? $.parseJSON(carousel.data('option').replace(/\'/gi, '"')) : {};
			option.siaCustomisations = true;
			// start wcag opts
			option.accessibility = true;
			option.assistiveTechnology = true;
			option.customPaging = function(slider, i) {
				var index = (i+1);
				return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.carouselFade.customPagingTextPre + index + L10n.sliders.carouselFade.customPagingTextPost);
			};
			option.pauseOnFocus = true;
			option.focusOnSelect = true;
			option.pauseOnHover = true;
			// end wcag opts

			var imgPromotionLength = carousel.find('img').length - 1;

			var loadBackground = function(self, parentSelt, idx) {
				if (idx === imgPromotionLength) {
					carousel.css('visibility', 'visible');
					carousel.find('.slides')
						.slick(option);
				}
			};

			option.pauseOnHover = false;
			carousel.find('img').each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				nI.onload = function() {
					loadBackground(self, parentSelt, idx);
				};

				nI.src = self.attr('src');
			});
		});
	}
};
