/**
 * @name SIA
 * @description Define global autocomplete search functions
 * @version 1.0
 */
SIA.autocompleteSearch = function(){
	var global = SIA.global;
	var vars = global.vars;
	var win = vars.win;
	var doc = vars.doc;
	var searchEl = $('[data-autocomplete-search]');
	var timer = null;

	// Get version of Internet explorer
	var getInternetExplorerVersion = function() {
		var rv = -1;
		if (navigator.appName === 'Microsoft Internet Explorer') {
			var ua = navigator.userAgent;
			var re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})');
			if (re.exec(ua) !== null) {
				rv = parseFloat(RegExp.$1);
			}
		} else if (navigator.appName === 'Netscape') {
			var ua = navigator.userAgent;
			var re = new RegExp('Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})');
			if (re.exec(ua) !== null) {
				rv = parseFloat(RegExp.$1);
			}
		}
		return rv;
	};

	// Init autocomplele search
	var initAutocompleteSearch = function() {
		if (getInternetExplorerVersion() === 11) {
			vars.html.addClass('ie11');
		}

		searchEl.each(function(index, el) {
			var field = $(el);
			var term = '';
			var sourceUrl = field.data('autocomplete-source') || 'ajax/autocomplete-search.json';
			var searchAutoComplete = field.autocomplete({
				open: function() {
					$(this).autocomplete('widget').jScrollPane({
						scrollPagePercent: 10
					}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
				},
				source: function(request, response) {
					term = request.term;
					$.get(sourceUrl + '?term=' + term, function(data) {
						response(data.searchSuggests);
					});
				},
				search: function() {
					var self = $(this);
					self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				},
				close: function() {
					var self = $(this);
					self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				},
				select: function(evt, ui) {
					$(this).val(ui.item.value).closest('form').trigger('submit');
				}
			}).data('ui-autocomplete');

			searchAutoComplete._renderItem = function(ul, item) {
				if (!ul.hasClass('auto-suggest')) {
					ul.addClass('auto-suggest');
				}
				return $('<li class="autocomplete-item" data-value="' + (item.value ? item.value : item.label) + '"><a href="javascript:void(0);">' + (item.label ? item.label : '') + '</a></li>')
					.appendTo(ul);
			};

			searchAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(field.outerWidth());
			};

			searchAutoComplete._move = function(direction) {
				var item, previousItem,
					last = false,
					api = this.menu.element.data('jsp'),
					li = $(),
					minus = null;

				if (!api) {
					if (!this.menu.element.is(':visible')) {
						this.search( null, event );
						return;
					}
					if (this.menu.isFirstItem() && /^previous/.test(direction) ||
							this.menu.isLastItem() && /^next/.test(direction)) {
						this._value( this.term );
						this.menu.blur();
						return;
					}
					this.menu[direction](event);
				}
				else {
					var	currentPosition = api.getContentPositionY();

					switch(direction){
						case 'next':
							if(this.element.val() === ''){
								api.scrollToY(0);
								li = this.menu.element.find('li:first');
								item = li.addClass('active').data('ui-autocomplete-item');
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.next();
								item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
							}
							if(!item){
								last = true;
								li = this.menu.element.find('li').removeClass('active').first();
								item = li.addClass('active').data('ui-autocomplete-item');
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(0);
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								minus = li.position().top + li.innerHeight();
								if(minus - this.menu.element.height() > currentPosition){
									api.scrollToY(Math.max(0, minus - this.menu.element.height()));
								}
							}
							break;
						case 'previous':
							if(this.element.val() === ''){
								last = true;
								item = this.menu.element.find('li:last').addClass('active').data('ui-autocomplete-item');
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.prev();
								item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
							}
							if(!item){
								last = true;
								item = this.menu.element.find('li').removeClass('active').last().addClass('active').data('ui-autocomplete-item');
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(this.menu.element.find('.jspPane').height());
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								if(li.position().top <= currentPosition){
									api.scrollToY(li.position().top);
								}
							}
							break;
					}
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');

			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				// var self = $(this);

				if(global.vars.isIE()){
					doc.off('mousedown.hideAutocompleteSearch').on('mousedown.hideAutocompleteSearch', function(e){
						if(!$(e.target).closest('.ui-autocomplete').length){
							field.autocomplete('close');
						}
					});
				}
			});

			if(!global.vars.isIE()){
				field.off('blur.highlight').on('blur.highlight', function(){
					timer = setTimeout(function(){
						field.autocomplete('close');
					}, 200);
					// if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
					// 	win.off('resize.reposition' + namespace);
					// }
				});
				field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
					clearTimeout(timer);
				});
			}

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
				e.stopPropagation();
			});

			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});

			// field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
			// 	if(e.which === 13){
			// 		e.preventDefault();
			// 		if(field.autocomplete('widget').find('li').length === 1){
			// 			field.autocomplete('widget').find('li').trigger('click');
			// 			return;
			// 		}
			// 		field.autocomplete('widget').find('li.active').trigger('click');
			// 	}
			// });

			win.off('resize.blurSearch' + index).on('resize.blurSearch' + index, function() {
				var currentEl = searchEl.not(':hidden');
				if (currentEl.length) {
					var currentMenu = currentEl.autocomplete('widget');
					if (currentMenu.length && currentMenu.is(':visible')) {
						currentEl.autocomplete('close');
					}
				}
			});
		});
	};

	var initModule = function() {
		initAutocompleteSearch();
	};

	initModule();
};
