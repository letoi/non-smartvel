/**
 * @name SIA
 * @description Define feed back global feedBack and functions
 * @version 1.0
 */
SIA.feedBack = function() {
	// var global = SIA.global;

	// feedback form
	var form = $('#form-feedback');

	// all feedback group fields
	var feedBackFieldGrps = form.find('.feedback, .button-group-1, .terms-conditions');

	// feedback attachment group
	var feedBackAttachment = feedBackFieldGrps.not('.feedback--details, .feedback--contact');

	// button group
	var buttonGroup = form.find('.button-group-1');

	// term and conditions
	var termsConditionsEl = form.find('.terms-conditions');

	//captcha
	var captchaEl = form.find('.captcha-wrap');

	// feedback type
	var feedBackType = $('[data-feedback-type]');

	// feedback category
	var feedBackCategory = $('[data-feedback-category]');

	// feedback topic
	var feedBackTopic = $('[data-feedback-topic]');

	// feedback sub topic
	var feedBackSubTopic = $('[data-feedback-sub-topic]');

	// feedback question
	var feedBackQuestion = $('[data-feedback-question]');

	var type = feedBackType.find('input:radio');
	var category = feedBackCategory.find('[data-customselect]');
	var topic = feedBackTopic.find('[data-customselect]');
	var subTopic = feedBackSubTopic.find('[data-customselect]');

	var feedBackquestion1 = $('[data-feedback-question="1"]');
	var feedBackQuestion2 = $('[data-feedback-question="2"]');

	var typeIndex = -1;
	var scIdx = -1;
	var stIdx = -1;
	var sstIdx = -1;
	var scVlue = -1;
	var stVlue = -1;
	var sstVlue = -1;
	var validateIndex = -1;

	// feedback dashbord for all cases
	var dashboard = {
		results: {
			result: {
				'1': ['1', '2', '3', '10*', '11*', '12*', '13', '14*'],
				'2': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*'],
				'3': ['1*', '2', '3', '10*', '11*', '12*', '13', '14*'],
				'4': ['1*', '2', '10*', '11*', '12*', '13', '14*'],
				'5': ['17*', '18', '3', '10*', '11*', '12*', '13', '14*'],
				'6': ['1*', '2', '3', '21*', '10*', '11*', '12*', '13', '14*'],
				'7': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '14*'],
				'8': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*'],
				'9': ['1*', '2', '3', '21', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '14*'],
				'11': ['1*', '2', '3', '21*', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*'],
				'10': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'12': ['17*', '18', '3', '5*', '6*', '7*', '8*', '10*', '15', '11*', '12*', '13*', '14*'],
				'13': ['17*', '18', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'14': ['1*', '2', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'15': ['17*', '18', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'16': ['17*', '18', '3', '5*', '6*', '7*', '8*', '9', '20', '10*', '15', '11*', '12*', '13*', '14*'],
				'17': ['17*', '18', '3', '19', '22', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'18': ['17*', '18', '3', '19*', '22', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'19': ['1*', '18', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'20': ['1*', '18', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'21': ['1*', '2', '3', '21', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*']
			},
			getResult: function(r){
				return this.result[r];
			}
		},
		category: [0, 1, 0, 2],
		subTopics: {
			subTopic: {
				'0': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
				'1': [-1, 0, 1],
				'2': [-1, 2, 3],
				'3': [-1, 4, -1, 5, -1],
				'4': [-1, -1, -1, -1, -1],
				'5': [-1, -1, -1, -1, 4, -1, 6, -1, 5, -1],
				'6': [-1, -1, -1, -1, -1]
			},
			getSubTopic: function(st) {
				return this.subTopic[st];
			}
		},
		topics: {
			'0': [-1, -1, -1, -1, -1, -1],
			'1': [-1, 0, 1, 2, 3, -1, -1],
			'2': [-1, -1, -1, -1, -1, -1],
			'3': [-1, 4, 2, 5, 6, -1, -1]
		},
		questions: {
			question: [
				// begin compliment category

				{
					type: 0,
					category: 1,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 2,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 3,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 4,
					topic: -1,
					subTopic: -1,
					validateIndex: 9
				},
				{
					type: 0,
					category: 5,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},

				// end compliment category

				// -------------------------------------------------------------

				// begin concern category

					// begin category 1

				{
					type: 1,
					category: 1,
					topic: 1,
					subTopic: -1,
					// quesIdx: 0
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 2,
					subTopic: -1,
					// quesIdx: 1
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 3,
					subTopic: -1,
					// quesIdx: 2
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 4,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 5,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 6,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 7,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 8,
					subTopic: -1,
					// quesIdx: 3
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 9,
					subTopic: -1,
					validateIndex: 13
				},
				{
					type: 1,
					category: 1,
					topic: 10,
					subTopic: -1,
					validateIndex: 10
				},

					// end category 1

				// -------------------------------------------------------------

					// begin category 2

				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 1,
					// quesIdx: 4
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 2,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 3,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 4,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 5,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 6,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 2,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 3,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 4,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 5,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 6,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 7,
					validateIndex: 15
				},

					// end category 2

				// ------------------------------------------------------------

					// begin category 3

				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 1,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 2,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 1,
					quesIdx: 5
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 2,
					quesIdx: 5
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 4,
					validateIndex: 10
				},

					// end category 3

				// --------------------------------------------------------------

					// begin category 4

				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 2,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 3,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 4,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 5,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 6,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 7,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 2,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 3,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 3,
					subTopic: 2,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 4,
					subTopic: -1,
					validateIndex: 6
				},

					// end category 4

				// ----------------------------------------------------------

					// begin category 5

				{
					type: 1,
					category: 5,
					topic: -1,
					subTopic: -1,
					quesIdx: 6
				},


					// end category 5

				// -----------------------------------------------------------

					// begin category 6

				{
					type: 1,
					category: 6,
					topic: -1,
					subTopic: -1,
					validateIndex: 3
				},

					// end category 6

				// end concern category

				// ----------------------------------------------------------

				// begin suggestion category

				{
					type: 2,
					category: 1,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 2,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 3,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 4,
					topic: -1,
					subTopic: -1,
					validateIndex: 11
				},
				{
					type: 2,
					category: 5,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},

				// end suggestion category

				// --------------------------------------------------------

				// begin question category

					// begin category 1

				{
					type: 3,
					category: 1,
					topic: 1,
					subTopic: -1,
					// quesIdx: 0
					validateIndex: 15
				},
				{
					type: 3,
					category: 1,
					topic: 2,
					subTopic: -1,
					validateIndex: 14
				},
				{
					type: 3,
					category: 1,
					topic: 3,
					subTopic: -1,
					validateIndex: 14
				},
				{
					type: 3,
					category: 1,
					topic: 4,
					subTopic: -1,
					validateIndex: 19
				},

					// end category 1

				// ---------------------------------------------------------

					// begin category 2

				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 1,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 2,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 1,
					quesIdx: 5
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 2,
					quesIdx: 5
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 4,
					validateIndex: 10
				},

					// end category 2

				// ---------------------------------------------------------

					// begin category 3

				{
					type: 3,
					category: 3,
					topic: 1,
					subTopic: -1,
					validateIndex: 4
				},
				{
					type: 3,
					category: 3,
					topic: 2,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 3,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 1,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 2,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 3,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 4,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 5,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 6,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 7,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 5,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 6,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 6,
					subTopic: 2,
					validateIndex: 6
				},
				// {
				// 	type: 3,
				// 	category: 3,
				// 	topic: 6,
				// 	subTopic: 3,
				// 	validateIndex: 6
				// },
				{
					type: 3,
					category: 3,
					topic: 7,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 8,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 8,
					subTopic: 2,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 9,
					subTopic: -1,
					validateIndex: 6
				},

					// end category 3

				// -----------------------------------------------------------

					// begin category 4

				{
					type: 3,
					category: 4,
					topic: 1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 2,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 3,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 4,
					subTopic: -1,
					validateIndex: 3
				},

					// end category 4

				// ---------------------------------------------------------

					// begin category 5

				{
					type: 3,
					category: 5,
					topic: -1,
					subTopic: -1,
					quesIdx: 6
				},

					// end category 5

				// -----------------------------------------------------------

					// begin category 6

				{
					type: 3,
					category: 6,
					topic: -1,
					subTopic: -1,
					validateIndex: 3
				}

					// end category 6

				// end question category
			],
			answer: {
				'0': [
					{
						typeIdx: 1,
						results: [15, 1]
					},
					{
						typeIdx: 3,
						results: [15, 3]
					}
				],
				'1': [{	results: [15, 10]	} ],
				'2': [{ results: [15, 13] }],
				'3': [{ results: [15, 20] }],
				'4': [{ results: [5, 13] }],
				'5': [{ results: [18, 17] }],
				'6': [{ results: [-1, 16, 12] }]
			},
			getQuestion: function(type, c, t, s) {
				var idx = 0;
				var ques = this.question;
				c = parseInt(c);
				t = parseInt(t);
				s = parseInt(s);
				for (idx in ques) {
					if (ques[idx].type === type && ques[idx].category === c && ques[idx].topic === t && ques[idx].subTopic === s) {
						return ques[idx];
					}
				}
				return null;
			},
			getAnswer: function(quesIdx, quesAns, typeIdx) {
				var ans = this.answer[quesIdx];
				if (ans && ans.length > 1) {
					for (var i = ans.length - 1; i >= 0; i--) {
						if (ans[i].typeIdx === typeIdx) {
							return ans[i].results[quesAns];
						}
					}
				}
				else {
					return ans[0].results[quesAns];
				}
			}
		}
	};


	// show, hide and set validate fields
	var render = function(orders, fields){
		orders.addClass('hidden');
		orders.find('select, input').addClass('ignore-validate');

		for(var i = 0; i < fields.length; i ++){
			// var f = orders.eq(parseInt(fields[i]) - 1);
			var fieldOrder = fields[i];
			var f = $();

			if (fieldOrder.indexOf('c') !== -1) {
				f = orders.filter('[data-order="' + parseInt(fieldOrder) + '"]').filter('[data-order-contact]');
			}
			else {
				f = orders.filter('[data-order="' + parseInt(fieldOrder) + '"]').not('[data-order-contact]');
			}

			if (f.length) {
				f.removeClass('hidden');
				f.closest('.feedback').removeClass('hidden');
				f.find('label').eq(0).text(f.find('label').eq(0).text().replace('*', ''));

				if(fields[i].indexOf('*') !== -1){
					f.find('select, input').removeClass('ignore-validate');
					f.find('label').eq(0).text(f.find('label').eq(0).text() + '*');
				}
			}
		}

		feedBackAttachment.removeClass('hidden');
		termsConditionsEl.removeClass('hidden');
		captchaEl.removeClass('hidden');
		buttonGroup.removeClass('hidden');

		if (form.resetForm) {
			form.resetForm();
		}
	};

	// render form elementss
	var renderFeedbackForm = function(fields){
		var orders = $($('.feedback [data-order]').toArray().sort(function(a, b){
			var aVal = $(a).data('order'),
			bVal = $(b).data('order');
			return aVal - bVal;
		}));

		render(orders, fields);
	};

	// set validate according to feedback dashboard
	var setValidate = function(typeIndex, category, topic, subTopic) {
		var ques = dashboard.questions.getQuestion(typeIndex, category, topic, subTopic);
		if (ques) {
			var qIdx = ques.quesIdx;
			if (typeof(qIdx) !== 'undefined' && feedBackquestion1.eq(qIdx).length) {
				qIdx = parseInt(qIdx);
				var question = feedBackquestion1.eq(qIdx);
				feedBackquestion1.addClass('hidden');
				feedBackQuestion2.addClass('hidden');
				question.removeClass('hidden');

				var rdos = question.find('input:radio');
				if (rdos.length) {
					rdos.prop('checked', false);
				}

				var selectEl = question.find('select');
				if (selectEl.length) {
					selectEl.prop('selectedIndex', 0).closest('[data-customselect]').customSelect('refresh');
				}

				// question.find('input:radio:checked, select').filter(function() {
				// 	var qEl = $(this);
				// 	return qEl.is('input') || (qEl.is('select') && qEl.val());
				// }).trigger('change.answer');
			}
			else {
				feedBackquestion1.addClass('hidden');
				feedBackQuestion2.addClass('hidden');
				validateIndex = ques.validateIndex;
				if (validateIndex) {
					renderFeedbackForm(dashboard.results.getResult(validateIndex));
				}
			}
		}
		else {
			feedBackquestion1.addClass('hidden');
			feedBackQuestion2.addClass('hidden');
		}
	};

	// set event handler for select and radio button in feedback type, question, topic, subtoic, category
	var setEventHandler = function() {
		type.off('change.chooseType').on('change.chooseType', function(){
			if ($(this).prop('checked')) {
				feedBackTopic.addClass('hidden');
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');
				feedBackCategory.addClass('hidden');
				category.addClass('hidden');

				typeIndex = type.index(this);
				scIdx = dashboard.category[typeIndex];

				var curCategory = category.eq(scIdx);
				curCategory.find('select').prop('selectedIndex', 0);
				curCategory.customSelect('refresh');
				curCategory.removeClass('hidden');
				feedBackCategory.removeClass('hidden');
			}
		});

		category
			.off('afterSelect.chooseCategory')
			.on('afterSelect.chooseCategory', function() {
				feedBackTopic.addClass('hidden');
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var catVlue = self.find('select').val();

				if (catVlue) {
					scVlue = parseInt(catVlue);
					stIdx = dashboard.topics[typeIndex][catVlue];

					if (stIdx !== -1) {
						var curTopic = topic.eq(stIdx);
						topic.addClass('hidden');
						curTopic.removeClass('hidden');
						curTopic.find('select').prop('selectedIndex', 0);
						curTopic.customSelect('refresh');
						feedBackTopic.removeClass('hidden');
					}
					else {
						stVlue = stIdx;
						sstVlue = stIdx;
						setValidate(typeIndex, scVlue, stVlue, sstVlue);
					}
				}
			});

		topic
			.off('afterSelect.chooseTopic')
			.on('afterSelect.chooseTopic', function() {
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var topicVlue = self.find('select').val();

				if (topicVlue) {
					stVlue = parseInt(topicVlue);
					sstIdx = dashboard.subTopics.getSubTopic(stIdx)[stVlue];

					if (sstIdx !== -1) {
						var curSubTopic = subTopic.eq(sstIdx);
						subTopic.addClass('hidden');
						curSubTopic.removeClass('hidden');
						curSubTopic.find('select').prop('selectedIndex', 0);
						curSubTopic.customSelect('refresh');
						feedBackSubTopic.removeClass('hidden');
					}
					else {
						sstVlue = sstIdx;
						setValidate(typeIndex, scVlue, stVlue, sstVlue);
					}
				}
			});

		subTopic
			.off('afterSelect.chooseSubTopic')
			.on('afterSelect.chooseSubTopic', function() {
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var subTopicVlue = self.find('select').val();

				if (subTopicVlue) {
					sstVlue = parseInt(subTopicVlue);
					setValidate(typeIndex, scVlue, stVlue, sstVlue);
				}
			});

		feedBackquestion1
			.off('change.answer')
			.on('change.answer', 'input:radio, select', function() {
				feedBackFieldGrps.addClass('hidden');
				var self = $(this);
				var quesIdx = feedBackquestion1.index(self.closest('[data-feedback-question="1"]'));
				var answerVlue = '-1';
				if (self.is('select')) {
					answerVlue = self.val() || '-1';
				}
				else {
					if (self.prop('checked')) {
						answerVlue = self.closest('[data-feedback-question="1"]').find('input:radio').index(self);
					}
				}
				validateIndex = dashboard.questions.getAnswer(quesIdx, answerVlue, typeIndex);
				if (validateIndex) {
					renderFeedbackForm(dashboard.results.getResult(validateIndex));
				}
			});

		// form.validate({
		// 	focusInvalid: true,
		// 	errorPlacement: global.vars.validateErrorPlacement,
		// 	success: global.vars.validateSuccess
		// });

		// form.off('submit.feedBack').on('submit.feedBack', function() {
		// 	if (!form.valid()) {
		// 		var orderEl = $('input.error').closest('[data-order]').eq(0);
		// 		if (orderEl.length) {
		// 			$('html,body').animate({
		// 				scrollTop: orderEl.offset().top
		// 			}, 400);
		// 		}
		// 	}
		// });
	};

	setEventHandler();

	/* jshint ignore:start */

	var vars = SIA.global.vars;
	var doc = $(document);

	var disabledChb = $('[data-order]').find('input:checkbox');
	var tellUsMore = $('[data-order="10"]').find('textarea');
	var maxLength = parseInt(tellUsMore.attr('maxlength'));

	var feedbackTypes = $("input[name='feedbackType']");
	var rCompliment = $("#compliment");
	var rConcern = $("#concern");
	var rSuggestion = $("#suggestion");
	var rQuestion = $("#question");
	var dCategory = $('#category');
	var dCategoryCencern = $('#category-concern');
	var dCategoryQuestion = $('#category-question');
	var categoryOfFeedback = $('#feedbackCategory');
	var dTopicBooking = $('#topic-booking');
	var dTopicTravel = $('#topic-travel');
	var dTopicBaggage = $("#topic-baggage");
	var dTopicMembership = $('#topic-membership');
	var dTopicPackage = $('#topic-packages');
	var dTopicQbooking = $("#topic-qbooking");
	var dTopicQmembership = $("#topic-qmembership");
	var topicOfFeedback = $('#feedbackTopic');
	var dSubtopicBooking = $('#sub-topic-booking');
	var dSubtopicDuring = $('#sub-topic-during');
	var dSubtopicBaggageBefore = $("#sub-topic-baggage-before");
	var dSubtopicBaggageAfter = $("#sub-topic-baggage-after");
	var dSubtopicKrisflyer = $('#sub-topic-krisflyer');
	var dSubtopicServices = $('#sub-topic-services');
	var dSubtopicOther = $('#sub-topic-other');
	var dSubtopicQualification = $("#sub-topic-qualification");
	var sTopicOfFeedback = $('#feedbackSubTopic');

	var lnSblock = $('[data-order="17"]');
	var lnFblock = $('[data-order="1"]');
	var fnSblock = $('[data-order="18"]');
	var fnFblock = $('[data-order="2"]');
	var userTitle = $("#title");
	var userSbLastName = $("#last-name-2");
	var userFbLastNAme = $("#last-name-1");
	var userFamilyName = $("#familyName");
	var userSbFirstName = $("#first-name-2");
	var userFbFirstName = $("#first-name-1");
	var userGivenName = $("#givenName");
	var fBgender = $("#gender-1");
	var sBgender = $("#gender-2");

	$(document).ready(function(){
		var ele = document.querySelectorAll('.number-only')[0];
		var preventpaste = document.querySelectorAll('.preventpaste')[0];

		if (!ele || !preventpaste) {
			return;
		}

		ele.onkeypress = function(e) {
			if(isNaN(this.value+""+String.fromCharCode(e.charCode)))
			return false;
		}
		ele.onpaste = function(e){
			e.preventDefault();
		}
		preventpaste.onpaste = function(e){
			e.preventDefault();
		}
	});

	$(".feedbackLoginButton").click(function(){
		$('#corporateSelect').addClass('hidden');
	});
	$(".login").click(function(){
		$('#corporateSelect').removeClass('hidden');
	});

	$("#checkbox-passenger").click(function(){

		if($(this).is(':checked')){
			$(this).prop('checked', true);
			$("#thirdPartyFlag").val("true");
		}else{
			$(this).prop('checked', false);
			$("#thirdPartyFlag").val("false");
		}
	});

	$("#feedbackAgreement").click(function(){

		if($(this).is(':checked')){
			$("#feedback-submit").removeClass("disabled");
			$("#feedback-submit").prop("disabled",false);
		}else{
			$("#feedback-submit").addClass("disabled");
			$("#feedback-submit").prop("disabled",true);
		}
	});

	$("#file-reference").off('blur.validatefileReference').on('change.validatefileReference',function(){
		var fileref = $.trim($("#file-reference").val());
		var self=$(this);
		var wrap = self.closest('[data-order]');
		if( fileref.length > 0 ){
			var letters = /^[A-Za-z]+$/;
			var freffirst = fileref.substring(0, 5);
			var freflast = fileref.substring(5);
			if(!freffirst.match(letters) || isNaN(freflast)){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validfileReferencenumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validfileReferencenumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			if(wrap.find('.grid-col').hasClass('error')){
				var errorText = self.attr('data-msg-required');
				wrap.addClass("error");
				wrap.find('.grid-col').find('.text-error').text(errorText);
			}else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
	});

	$("#flight-number").off('blur.validateflightnumber').on('change.validateflightnumber',function(){
		var flightNo = $.trim($("#flight-number").val());
		var self=$(this);
		var wrap = self.closest('[data-order]');
		if( flightNo.length > 0 ){
			var letters = /^[A-Za-z]+$/;
			var fNofirst = flightNo.substring(0, 2);
			var fNolast = flightNo.substring(2);
			if(!fNofirst.match(letters) || isNaN(fNolast)){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validflightnumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validflightnumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			if(wrap.find('.grid-col').hasClass('error')){
				var errorText = self.attr('data-msg-required');
				wrap.addClass("error");
				wrap.find('.grid-col').find('.text-error').text(errorText);
			}else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
	});

	var mandatoryErrors = function(){

		var flightNumberfield = $('[data-order="5"]');
		if(flightNumberfield.find('.grid-col').hasClass('error')){
			var errorText = $("#flight-number").attr('data-msg-required');
			flightNumberfield.find('.grid-col').find('.text-error').text(errorText);
		}else {
			$("input[name='flightNo']").trigger('change.validateflightnumber');
		}

		var fileReffield = $('[data-order="19"]');
		if(fileReffield.find('.grid-col').hasClass('error')){
			var errorText = $("#file-reference").attr('data-msg-required');
			fileReffield.find('.grid-col').find('.text-error').text(errorText);
		}else {
			$("input[name='fileReference']").trigger('change.validatefileReference');
		}
	}

	$("#membership-number").off('blur.validatekfnumber').on('change.validatekfnumber',function(){
		var value = $.trim($("#membership-number").val());
		var self = $(this);
		var wrap = self.closest('[data-order]');
		if( value.length > 0 ){
			if( value.charAt(0)!= "8" || value.length != 10){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validkfnumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validkfnumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				//wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			$(this).parents('.grid-col').find(".text-error").empty();
			wrap.removeClass("error");
			//wrap.find('.grid-col').removeClass('error');
		}
	});

	$('#destinationCity').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].getAttribute('data-text')){
				$(parentDiv).find("#destinationCityFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

	$('#departureCity').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].getAttribute('data-text')){
				$(parentDiv).find("#departureCityFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

	$('#countryCode').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].text){
				$(parentDiv).find("#countryCodeFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

// ------------------------ BEGIN UPLOAD FILE MODULE ---------------------------

	var totalSize = 5;
	var totalFileSize = 0;
	var leftSize = totalSize;
	var imageArray = [];
	var mbRate = 1048576;
	var attachList = $('.list-attachment');
	var attachEl = $('.attachment');
	var attachError = attachEl.find('.text-error-1');
	var chooseFileEl = $('.choose-file');
	var cloneChooseFileEl = chooseFileEl.clone();
	var isSupportHTML5 = !!window.FileReader;
	var imgFileTpl = '<img style="width: 84px; height: 84px;" class="attached-image" src="{0}" alt="" />';
	var otherFileTpl = '<div class="icon-wrapper"><em class="{0}">&nbsp;</em></div>';
	var unsupportHtml5Tpl = '<li>{0}<a href="#"> delete</a></li>';
	var errorTpl = '<p class="text-error">{0}</p>';
	var pattern = /(jpg|jpeg|png|gif|tif|tiff|doc|docx|pdf|txt|zip)$/i;

	var attachTpl = '<li>{4}<a href="#" class="attachment-close"><input type="hidden" class="ret-file-size" value="{0}"/><input type="hidden" class="ret-file-name" value="{1}"/><em class="ico-close-rounded"></em></a>{2}<p class="chose-img-name">{3}</p></li>';

	var uploadError = {
		'notSupport': 'Not supported file format.',
		'fileExceed': 'File size exceeded.',
		'totalExceed': '17MB exceeded.',
		'fileExists': 'File name already exists.',
		'notSupportIe10': 'Not supported file format on ie 10 and below.'
	};

	var removeFileName = function(imageArray, removeItem) {
		return jQuery.grep(imageArray, function(value) {
			return value !== removeItem;
		});
	};

	var checkDuplicate = function(imageArray, files) {
		var duplicate = '';
		for(var i = 0, len = imageArray.length; i < len - 1; i++){
			if(imageArray[i] === files[0].name){
				duplicate = 'yes';
				break;
			}
		}
		return duplicate;
	};

	var renderFileSuccess = function(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl) {
		attachList.prepend($(attachTpl.format(self.files[j].size, self.files[j].name, imgTpl, self.files[j].name, fileIcon)).append(currentChooseFileEl.find('input:file').addClass('hidden').data('choosed-file', true)));

		chooseFileEl.remove();
		attachList.append(cloneChooseFileEl);

		totalFileSize = totalFileSize + filesize;
		leftSize = totalSize - totalFileSize;

		chooseFileEl = $('.choose-file:not([data-choosed-file])').find('.chose-img-name').text(Math.round(leftSize) + ' MB left').end();
	};

	var renderFileError = function(fileName, currentChooseFileEl, err) {
		imageArray = removeFileName(imageArray, fileName);
		currentChooseFileEl.val('');
		attachError = $(errorTpl.format(err)).insertAfter(attachList);
	};

	chooseFileEl.find('.chose-img-name').text(Math.round(leftSize) + 'MB left');
	// $('[data-support-html5="' + isSupportHTML5 + '"]').removeClass('hidden');
	attachError.empty().removeClass('hidden');

	attachEl
		.off('click.removeFile')
		.on('click.removeFile', 'a, .attachment-close', function(e) {
			e.preventDefault();
			$(this).parent().remove();
			attachError.remove();

			if (isSupportHTML5) {
				var removeItem = $(this).find('.ret-file-name').val();
				var thisSize = $(this).find('.ret-file-size').val()/1048576;

				imageArray = removeFileName(imageArray, removeItem);
				totalFileSize = totalFileSize - thisSize;
				leftSize = totalSize - totalFileSize;

				chooseFileEl.find('.chose-img-name').text(Math.round(leftSize) + 'MB left');
				$(this).find('.ret-file-size').val(0);
			}
			else {
				chooseFileEl.find('input:file').val('');
				chooseFileEl.removeClass('hidden');
			}
		})
		.off('change.chooseFile')
		.on('change.chooseFile', 'input:file', function() {
			var currentChooseFileEl = $(this);
			var files = this.files;

			if (files && files.length) {
				attachError.remove();

				if (isSupportHTML5) {
					imageArray.push(files[0].name);
					var duplicate = checkDuplicate(imageArray, files);

					for (var i = 0, len = files.length; i < len; i++) {
						var file = files[i];
						var fileName = file.name;
						var fileExtension = fileName.split('.').pop().toLowerCase();
						var filesize = 0;

						if(!duplicate){
							filesize = file.size/mbRate;

							if(leftSize >= filesize){
								if (!pattern.test(fileExtension)) {
									renderFileError(fileName, currentChooseFileEl, uploadError.notSupport);
								}
								else if (filesize > 3) {
									renderFileError(fileName, currentChooseFileEl, uploadError.fileExceed);
								}
								else {
									(function (j, self, extension) {
										var reader = new FileReader();

										reader.onload = function (e) {
											cloneChooseFileEl = chooseFileEl.clone();

											var fileIcon = '';
											var imgTpl = '';

											if (fileExtension !== 'tiff' && fileExtension !== 'tif') {
												fileIcon = extension === 'pdf' ?
													otherFileTpl.format('ico-pdf-2') :
													((extension === 'doc' || extension === 'docx' || extension === 'txt') ?
														otherFileTpl.format('ico-document') : ((extension === 'zip') ? otherFileTpl.format('ico-zip') : ''));

												imgTpl = (extension === 'png' || extension === 'jpg' || extension === 'gif' || extension === 'jpeg') ? imgFileTpl.format(e.target.result) : '';

												renderFileSuccess(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl);
											}
											else {
												if (SIA.global.vars.ieVersion() !== -1 && SIA.global.vars.ieVersion() <= 10) {
													renderFileError(fileName, currentChooseFileEl, uploadError.notSupportIe10);
												}
												else {
													Tiff.initialize({
														TOTAL_MEMORY: 300 * 1024 * 1024
													});

													var tiff = new Tiff({
														buffer: e.target.result
													});

													fileIcon = '';
													imgTpl = imgFileTpl.format(tiff.toDataURL('image/png'));

													renderFileSuccess(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl);
												}
											}
										};

										if (fileExtension === 'tiff' || fileExtension === 'tif') {
											reader.readAsArrayBuffer(file);
										}
										else {
											reader.readAsDataURL(file);
										}

									})(i, this, fileExtension);
								}
							}
							else {
								renderFileError(fileName, currentChooseFileEl, uploadError.totalExceed);
							}
						}
						else {
							renderFileError(fileName, currentChooseFileEl, uploadError.fileExists);
						}
					}
				}
				else {
					var fileName = files[0].name;
					var fileExtension = fileName.split('.').pop().toLowerCase();

					if (pattern.test(fileExtension)) {
						attachList.prepend(unsupportHtml5Tpl.format(fileName));
						chooseFileEl.addClass('hidden');
					}
					else {
						attachError = $(errorTpl.format('Not supported file format.')).insertAfter(attachList);
						this.value = '';
					}
				}
			}
		})
		.off('mousedown.chooseFile')
		.on('mousedown.chooseFile', 'input:file', function() {
			if (SIA.global.vars.ieVersion() === 10) {
				$(this).trigger('click');
			}
		});

// ------------------------ END UPLOAD FILE MODULE -----------------------------

	feedbackTypes.click(function(){
		switch($(this).val())
		{
			case "compliment" :
				rCompliment.prop('checked', true);
				break;
			case "concern" :
				rConcern.prop('checked', true);
				break;
			case "suggestion" :
				rSuggestion.prop('checked', true);
				break;
			case "question" :
				rQuestion.prop('checked', true);
				break;
		}
		$("input[name='fileReference']").trigger('change.validatefileReference');
		$("input[name='flightNo']").trigger('change.validateflightnumber');
		$("input[name='krisFlyerMembershipNo']").trigger('change.validatekfnumber');
	});

	var typeOfFeedBack = function() {

		if(rCompliment.is(':checked')){

			var fcategory = dCategory .find("option:selected").text();
			dCategory.off('change').on('change',function(){
				fcategory = dCategory .find("option:selected").text();
			});
			var fCategoryValue = $("#category option:contains('"+fcategory+"')").attr('value');
			//alert(fCategoryValue);
			categoryOfFeedback.val(fcategory);

		}else if(rConcern.is(':checked')){

			var fcategory = dCategoryCencern .find("option:selected").text();
			dCategoryCencern.off('change').on('change',function(){
				fcategory = dCategoryCencern .find("option:selected").text();
			});
			var fCategoryValue = $("#category-concern option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

			switch(fCategoryValue)
			{
				case "1" :

					var ftopic = dTopicBooking .find("option:selected").text();
					dTopicBooking.off('change').on('change',function(){
						ftopic = dTopicBooking .find("option:selected").text();
					});
					var ftopicValue = $("#topic-booking option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;

				case "2" :

					var ftopic = dTopicTravel .find("option:selected").text();
					dTopicTravel.off('change').on('change',function(){
						ftopic = dTopicTravel .find("option:selected").text();
					});
					var ftopicValue = $("#topic-travel option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBooking .find("option:selected").text();
							dSubtopicBooking.off('change').on('change',function(){
								 fsubtopic = dSubtopicBooking .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-booking option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicDuring .find("option:selected").text();
							dSubtopicDuring.off('change').on('change',function(){
								fsubtopic = dSubtopicDuring .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-during option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

							}
					break;

				case "3" :

					var ftopic = dTopicBaggage.find("option:selected").text();
					dTopicBaggage.off('change').on('change',function(){
						ftopic = dTopicBaggage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-baggage option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							dSubtopicBaggageBefore.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-before option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							dSubtopicBaggageAfter.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-after option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;

				case "4" :

					var ftopic = dTopicMembership .find("option:selected").text();
					dTopicMembership.off('change').on('change',function(){
						ftopic = dTopicMembership .find("option:selected").text();
					});
					var ftopicValue = $("#topic-membership option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							dSubtopicKrisflyer.off('change').on('change',function(){
								fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-krisflyer option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "4" :

							var fsubtopic = dSubtopicServices .find("option:selected").text();
							dSubtopicServices.off('change').on('change',function(){
								fsubtopic = dSubtopicServices .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-services option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;
			}

		}else if(rSuggestion.is(':checked')){

			var fcategory = dCategory .find("option:selected").text();
			dCategory.off('change').on('change',function(){
				fcategory = dCategory .find("option:selected").text();
			});
			var fCategoryValue = $("#category option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

		}else if(rQuestion.is(':checked')){

			var fcategory = dCategoryQuestion .find("option:selected").text();
			dCategoryQuestion.off('change').on('change',function(){
				fcategory = dCategoryQuestion .find("option:selected").text();
			});
			var fCategoryValue = $("#category-question option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

			switch(fCategoryValue)
			{
				case "1" :

					var ftopic = dTopicQbooking .find("option:selected").text();
					dTopicQbooking.off('change').on('change',function(){
						 ftopic = dTopicQbooking .find("option:selected").text();
					});
					var ftopicValue = $("#topic-qbooking option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;

				case "2" :

					var ftopic = dTopicBaggage.find("option:selected").text();
					dTopicBaggage.off('change').on('change',function(){
						ftopic = dTopicBaggage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-baggage option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							dSubtopicBaggageBefore.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-before option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							dSubtopicBaggageAfter.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-after option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;

				case "3" :

					var ftopic = dTopicQmembership .find("option:selected").text();
					dTopicQmembership.off('change').on('change',function(){
						 ftopic = dTopicQmembership .find("option:selected").text();
					});
					var ftopicValue = $("#topic-qmembership option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "4" :

							var fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							dSubtopicKrisflyer.off('change').on('change',function(){
								fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-krisflyer option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "6" :

							var fsubtopic = dSubtopicQualification .find("option:selected").text();
							dSubtopicQualification.off('change').on('change',function(){
								fsubtopic = dSubtopicQualification .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-qualification option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "8" :

							var fsubtopic = dSubtopicServices .find("option:selected").text();
							dSubtopicServices.off('change').on('change',function(){
								fsubtopic = dSubtopicServices .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-services option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
						}
					break;

				case "4" :

					var ftopic = dTopicPackage .find("option:selected").text();
					dTopicPackage.off('change').on('change',function(){
						 ftopic = dTopicPackage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-packages option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;
				}
		}
		var question1 = $('[data-feedback-question="1"]').find(":visible").closest('label').text();
		var sixthques = feedBackquestion1.eq(6).find('label').text();
		var i = question1.indexOf("?");
		if(i > 0){
			var feedbackQuestion1 = question1.slice(0, i+1);
		 }
		if(question1 == sixthques){
			var feedbackans1 = $("#question-lose-item").find("option:selected").text();
		}else{
			var feedbackans1 = $('[data-feedback-question="1"]').find(":visible").find('input:radio:checked').attr('value');
		}
		$("#feedbackQuestion1").val(feedbackQuestion1);
		$("#ans1").val(feedbackans1);


		var question2 = $('[data-feedback-question="2"]').find(":visible").closest('label').text();
		var thirdques = feedBackQuestion2 .eq(3).find('label').text();
		var fifthques = feedBackQuestion2 .eq(5).find('label').text();
		var j = question2.indexOf("?");
		var k = question2.indexOf(".");
		if(j > 0){
			var feedbackQuestion2 = question2.slice(0, j+1);
		 }
		if(k > 0){
			var feedbackQuestion2 = question2.slice(0, k+1);
		 }
		if(question2 == thirdques){
			var feedbackans2 = $("#request").find("option:selected").text();
		}else if(question2 == fifthques){
			var feedbackans2 = $("#airports").find(':selected').val();
		}else{
			var feedbackans2 = $('[data-feedback-question="2"]').find(":visible").find('input:text').val();
		}
		$("#feedbackQuestion2").val(feedbackQuestion2);
		$("#ans2").val(feedbackans2);
	}

	var basicDetails = function() {

		if(lnSblock.hasClass("hidden")){

			var title = fBgender .find("option:selected").text();
			var lastName = userFbLastNAme.val();
			userTitle.val(title);
			userFamilyName.val(lastName);

		}else if(lnFblock.hasClass("hidden")){

			var title = sBgender .find("option:selected").text();
			var lastName = userSbLastName.val();
			userTitle.val(title);
			userFamilyName.val(lastName);
		}

		if(fnSblock.hasClass("hidden")){

			var firstName = userFbFirstName.val();
			userGivenName.val(firstName);

		}else if(fnFblock.hasClass("hidden")){

			var firstName = userSbFirstName.val();
			userGivenName.val(firstName);
		}
	}

	var initForm = function() {
		var limitCharEl = tellUsMore.parent('.textarea-2').siblings('.limit-character');
		var limitText = limitCharEl.text();
		var llimitText = $("#cleft").val();

		var countCharLeft = function() {
			var vlue = tellUsMore.val();
			var breakLine = (vlue.match(/\n/g)||[]).length;
			var length = vlue.length;
			var charLeft = maxLength - (length + breakLine);
			var charText = '';
			charText = charLeft <= 0 ?
				'0' : charLeft === 1 ?
				'1' : window.accounting.formatNumber(charLeft, false);

			if(length + breakLine >= maxLength) {
				tellUsMore.val(vlue.slice(0, maxLength - breakLine));
				limitCharEl.empty();
				limitCharEl.text('0'+' '+llimitText);
			}
			else {
				if(charLeft <=1){
					limitCharEl.empty();
					limitCharEl.text(charText+' '+llimitText);
				}else{
					limitCharEl.empty();
					limitCharEl.text(charText+' '+limitText);
				}
			}
		};

		var loggedUser = $("#loggedUser").val();
		if(loggedUser == "true")
		{
			var fnAvailable = $("#fnAvailable").val();
			if(fnAvailable == '')
			{
				disabledChb.prop('checked', true);
				disabledChb.prop('disabled', true);
			}
			else
			{
				disabledChb.prop('checked', false);
				disabledChb.prop('disabled', true);
			}

			var phNoCountryCode = $("#phNoCountry").val();
			var phNoCountry=$('#pre-country-code-label').val(phNoCountryCode).find(':selected').data('text');
			$("#pre-country-code").val(phNoCountry);

			var countryOfResidenceCode = $("#countryOfRes").val();
			var countryOfResidence=$('#country-label').val(countryOfResidenceCode).find(':selected').data('text');
			$("#country").val(countryOfResidence);

		}
		else
		{
			disabledChb
				.off('change.disableName')
				.on('change.disableName', function() {
					var self = $(this);
					var wrap = self.closest('[data-order]');
					var sibl = wrap.find('input:text');

					if(self.is(':checked')){
						sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
						wrap.find('.text-error').remove();
						wrap.find('.error').removeClass('error');
					}
					else{
						sibl.prop('disabled', false).closest('span').removeClass('disabled');
					}
				});
		}

		tellUsMore
			.off('keyup.feedBack')
			.on('keyup.feedBack', function(e) {
				var key = e.which || e.keyCode;
				var vlue = tellUsMore.val();
				var breakLine = (vlue.match(/\n/g)||[]).length;
				var length = vlue.length;
				var charLeft = maxLength - (length + breakLine);
				var charText = '';
				charText = charLeft <= 0 ?
				'0' : charLeft === 1 ?
				'1' : window.accounting.formatNumber(charLeft, false);

				if(length + breakLine > maxLength) {
					if (e.ctrlKey || key === 13 || key === 32 || (key >= 48 && key <= 90) ||
						(key >= 96 && key <= 111) || key >= 186) {

						tellUsMore.val(vlue.slice(0, maxLength - breakLine));

						if(key === 13 && charLeft === -1) {
							limitCharEl.empty();
							limitCharEl.text('1'+' '+llimitText);
						}
						else {
							limitCharEl.empty();
							limitCharEl.text('0'+' '+llimitText);
						}
					}
				}
				else {
					if(charLeft <=1){
						limitCharEl.empty();
						limitCharEl.text(charText+' '+llimitText);
					}else{
						limitCharEl.empty();
						limitCharEl.text(charText+' '+limitText);
					}
				}
			})
			.off('keydown.feedBack')
			.on('keydown.feedBack', function(e) {
				var key = e.keyCode || e.which;
				if (e.ctrlKey || key === 13 || key === 32 || (key >= 48 && key <= 90) ||
					(key >= 96 && key <= 111) || key >= 186) {
					var vlue = tellUsMore.val();
					var breakLine = (vlue.match(/\n/g)||[]).length;
					var length = vlue.length;

					if(!e.ctrlKey && length + breakLine >= maxLength) {
						e.preventDefault();
					}
				}
			})
			.off('change.feedBack blur.feedBack focus.feedBack')
			.on('change.feedBack blur.feedBack focus.feedBack', function() {
				countCharLeft();
			})
			.off('paste.feedBack')
			.on('paste.feedBack', function() {
				setTimeout(function() {
					countCharLeft();
				}, 200);
			})
			.trigger('keyup.feedBack');

		doc.off('contextmenu.feedBack').on('contextmenu.feedBack', function(e){
			if(e.target.nodeName === 'TEXTAREA') {
				e.preventDefault();
			}
		});

		/*form.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});*/

		form.off('submit.feedBack').on('submit.feedBack', function() {

			if (!form.valid() ) {
				var orderEl = $('input.error').closest('[data-order]').eq(0);
				if (orderEl.length) {
					$('html,body').animate({
						scrollTop: orderEl.offset().top
					}, 400);
				}
			}
			var countryText = $("#country").val();
			var countryResCode = $("#country-label option:contains('"+countryText+"')").attr('value');
			$("#countryOfResCode").val(countryResCode);
			var language = $('input[name=locale]:checked').val();
			$("#language").val(language);
			basicDetails();
			typeOfFeedBack();
			mandatoryErrors();

		});

	};

	var initModule = function() {
		initForm();
		// SIA.initAutocompleteCity();
	};

	initModule();

	/* jshint ignore:end */
};
