SIA.sqcSavedTrips = function() {
	var config = SIA.global.config;
	var container = $('[data-fill-flights]');
	var upcomingInfoTemplate;
	var btnMore = $('[data-see-more]');
	var btnDelete = $('[data-message-delete]');
	var listTrips = $('.block--bookings-list');
	var lastItemIdx = 0;

	function getFlightInto(block) {
		block.find('.flights--detail > span')
		.off('click.getFlightInfo')
		.on('click.getFlightInfo', function() {
			var self = $(this);
			var details = self.siblings('.details');
			if(details.is('.hidden')) {
				$.ajax({
					url: config.url.orbFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						self.children('em').toggleClass('ico-point-d ico-point-u');
						details.toggleClass('hidden');
						var html = '';
						html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						for(var ft in res.flyingTimes) {
							html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
						}
						details.html(html);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						if(textStatus !== 'abort') {
							window.alert(errorThrown);
						}
					},
					beforeSend: function() {
						self.children('em').addClass('hidden');
						self.children('.loading').removeClass('hidden');
					},
					complete: function() {
						self.children('.loading').addClass('hidden');
						self.children('em').removeClass('hidden');
					}
				});
			}
			else {
				self.children('em').toggleClass('ico-point-d ico-point-u');
				details.toggleClass('hidden');
			}
		});
	}

	function showListPassengers(block) {
		block.find('[data-toggle-passengers-list]')
		.off('click.toggle-list-passengers')
		.on('click.toggle-list-passengers', function(e) {
			e.preventDefault();
			var listPassengers = $(this).siblings('[data-passengers]');
			$(this).toggleClass('active');
			var showLess = $(this).is('.active');
			var passengers = listPassengers.data('passengers').split(',');
			var lis = '';
			for(var i = 0; i < (showLess ? passengers.length : 3); i++) {
				lis += '<li>' + passengers[i];
				if(i < (showLess ? passengers.length : 3) - 1) {
					lis += ',';
				}
				lis += '</li>';
			}
			listPassengers.html(lis);
			listPassengers.children().last().addClass('last');
		});
	}

	function getUpcomingInfo(block) {
		var url = block.data('url');
		$.ajax({
			url: url,
			type: config.ajaxMethod,
			dataType: 'json',
			data: {},
			success: function(res) {
				var template = window._.template(upcomingInfoTemplate, {
					data: res
				});
				block.find('.loading').addClass('hidden');
				if(res.btnLabel) {
					block.find('.booking-form').removeClass('hidden').find('.btn-1').text(res.btnLabel);
				}
				block.find('.ico-point-d').removeClass('hidden');
				block.find('.ico-point-d').attr('tabindex', 0);
				block.find('.ico-point-d').attr('aria-expanded', false);
				block.find('.ico-point-d').attr('aria-controls', 'data-saved-trip-' + block.data('saved-trip') );
				block.find('.accordion__content').attr('id', 'data-saved-trip-' + block.data('saved-trip'));
				block.find('.ico-point-d').attr('role', 'tab');
				block.find('.accordion__content').attr('role', 'tabpanel');
				// block.find('.ico-point-d').append('<span />').text('')
				// block.find('.ico-point-d').attr('aria-label', 'Expand');

				block.find('.ico-point-d').on('keydown', function(e){
					var keyCode = e.keyCode || e.which || e.charCode;

					if( keyCode == 13 ){
						if(block.find('.accordion__control').attr('aria-expanded') === 'false'){
							block.find('.accordion__control').addClass('active');
							block.find('.accordion__control').attr('aria-expanded', true);
							block.find('.accordion__content').slideDown(400);
							block.find('.ico-point-d').attr('aria-expanded', true);
						}else{
							block.find('.accordion__control').removeClass('active');
							block.find('.accordion__control').attr('aria-expanded', false);
							block.find('.accordion__content').slideUp(400);
							block.find('.ico-point-d').attr('aria-expanded', false);
						}
						
					}

				});

				block.find('.booking-info-group').html(template);

				getFlightInto(block);
				showListPassengers(block);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if(jqXHR && textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			}
		});
	}

	function fillContent() {
		$.get(config.url.sqcUpcomingFlightDetailTemplate, function(html) {
			upcomingInfoTemplate = html;
		});
		$.get(config.url.sqcSavedTripsTemplate, function(html) {
			var template = window._.template(html, {
				data: globalJson.sqcSavedTrips
			});
			container.html(template);

			container.find('[data-saved-trip]').each(function() {
				var item = $(this);

				item.find('[tabindex]').removeAttr('tabindex')
				getUpcomingInfo(item);
				item.attr({
					'tabindex':0
				});

			});

			container.find('.accordion__control').each(function(){
				var self = $(this);
				self.off('keydown').on('keydown', function(e){
					var keyCode = e.keyCode || e.which || e.charCode;
					if(e.ctrlKey && keyCode === 13){
						self.trigger('click');
					}
				});
			});

			SIA.accordion.initAccordion();
			btnDelete.attr('disabled', 'disabled');

			listTrips
			.checkboxAll()
			.off('change.checkbox')
			.on('change.checkbox', '[data-checkbox-master], [data-checkbox-item]', function() {
				var checkedBoxes = listTrips.find('[data-checkbox-item]:checked');
				if(checkedBoxes.length) {
					btnDelete.addClass('active').removeAttr('disabled');
				}
				else {
					btnDelete.removeClass('active').attr('disabled', 'disabled');
				}
			});
		});
		if(globalJson.sqcSavedTrips.length > 3) {
			btnMore.show();
		}
		else {
			btnMore.hide();
		}
		btnMore.off('click.show-more').on('click.show-more', function(e) {
			e.preventDefault();

			lastItemIdx = listTrips.find('[data-saved-trip]').not('.hidden').length;

			$('.hidden[data-saved-trip]').removeClass('hidden');
			listTrips.find('[data-checkbox-master]').prop('checked', false);
			$(this).hide(0);

			listTrips.find('[data-saved-trip]').eq(lastItemIdx).focus();
			$('#aria-sqc-saved-seemore').text('');
			$('#aria-sqc-saved-seemore').text(L10n.wcag.seemoreLabel.format(listTrips.find('[data-saved-trip]').not('hidden').length - lastItemIdx));

		});
	}

	fillContent();

	function popupDeleteTrips() {
		var popup = $('.popup--saved-trips-delete').Popup({
			triggerCloseModal: '.popup__close, [data-close]',
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			container: '#container'
		});
		var renderDeletedTrip = function(){
			var lis = '';
			$('[data-fill-flights] [data-checkbox-item]:checked').not(':hidden').each(function() {
				lis += '<li>' + $(this).next('label').text() + '</li>';
			});
			return lis;
		};
		btnDelete.off('click.open-popup').on('click.open-popup', function(e) {
			e.preventDefault();
			popup.find('[data-trip-list]').html(renderDeletedTrip());
			popup.Popup('show');
		});
	}

	popupDeleteTrips();

	var wcag = function() {
		if($('#aria-sqc-saved-seemore').length === 0) {
			listTrips.prepend('<span class="ui-helper-hidden-accessible" id="aria-sqc-saved-seemore" aria-atomic="true" aria-live="assertive"></span>')
		};
	}

	wcag();

};
