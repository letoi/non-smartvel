/**
 * @name SIA
 * @description Define global flightSchedule functions
 * @version 1.0
 */
SIA.KFFavourite = function(){
	var global = SIA.global;

	var initFavourite = function(){
		var favouritesList = $('.favourites-list');
		var flightsList = $('[data-list-favourites]', favouritesList);
		var wrapper = $('.items', flightsList);
		var filterSelect = $('[data-filter] select', favouritesList);
		var seemore = $('[data-see-more]', flightsList);
		var allData = [];
		var dataHolder = [];
		var page = 0;
		var initItem = 9;
		var limitItem = 6;
		var itemsLength;
		var currentItem = 0;
		var htmlTpl = '';
		var lastItemIdx = null;

		var renderFilterTemplate = function(el, json){
			el.empty();
			var options = [];
			for(var i = 0; i < json.filter.length; i++){
				options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.filter[i].value +'"' + '>' + json.filter[i].text + '</option>');
			}
			el.html(options.join(''));
			if(el.closest('[data-customSelect]').data('customSelect')){
				el.closest('[data-customSelect]').customSelect('_createTemplate');
			}
		};

		var renderItems = function(data){
			wrapper.empty();
			buildHtml(false, data);
		};

		var buildHtml = function(isReset, items){
			// var limit = (page * limitItem) + initItem;

			var limit = page === 0 ? initItem : $('.flight-item').length + limitItem;
			var dataFilter = {
				favouriteItems:[]
			};

			itemsLength = items.favouriteItems.length;

			if(itemsLength <= limit){
				limit = itemsLength;
				seemore.addClass('hidden');
				seemore.text(L10n.kfSeemore.seeMore);
			}
			else {
				if (page < 2) {
					seemore.text(L10n.kfSeemore.seeMore);
					seemore.removeClass('hidden');
				}
				else if (page === 2) {
					seemore.text(L10n.kfSeemore.seeAll);
				}
				else {
					limit = itemsLength;
					seemore.addClass('hidden');
					seemore.text(L10n.kfSeemore.seeMore);
				}
			}

			for(currentItem; currentItem < limit; currentItem++){
				dataFilter.favouriteItems.push(items.favouriteItems[currentItem]);
				if(currentItem === limit - 1){
					page++;
				}
			}

			for (var k in dataFilter.favouriteItems) {
				dataFilter.favouriteItems[k].priceText = accounting.formatMoney(dataFilter.favouriteItems[k].price, '', 0, ',', '.');
			}

			if (htmlTpl) {
				renderTemplate(htmlTpl, dataFilter, isReset);
			}
			else {
				$.get(SIA.global.config.url.kfFavorite, function (html) {
					htmlTpl = html;
					renderTemplate(htmlTpl, dataFilter, isReset);
				});
			}

			if(lastItemIdx !== null) {
				$('.flight-item').eq(lastItemIdx).focus();
			}

		};

		var renderTemplate = function(htmlTpl, dataFilter, isReset) {
			var template = window._.template(htmlTpl, {
				data: dataFilter
			});

			if(isReset){
				wrapper.empty();
			}

			$(template).appendTo(wrapper);

			wcag();

		};

		var filterData = function(data){

			var newData = {
				favouriteItems: []
			};

			for(var i = 0; i < data.favouriteItems.length; i++){
				if(filterSelect.val() === 'All'){
					newData.favouriteItems.push(data.favouriteItems[i]);
				}
				else if(filterSelect.val() === 'promo' && data.favouriteItems[i].type === 'promo'){
					newData.favouriteItems.push(data.favouriteItems[i]);
				}
				else if(filterSelect.val() === 'expired' && data.favouriteItems[i].expire){
					newData.favouriteItems.push(data.favouriteItems[i]);
				}
			}

			dataHolder = newData;

			$('#aria-filter-item').text('');
			$('#aria-filter-item').text(L10n.wcag.foundLabel.format(dataHolder.favouriteItems.length));

			buildHtml(true, newData);
		};

		var delectFavourite = function(id, el){
			var indexArray = -1;

			for(var i = 0; i < allData.favouriteItems.length ; i ++){
				if(allData.favouriteItems[i].promoFareId === id){
					indexArray = i;
					break;
				}
			}

			if(indexArray > -1){
				if(window.Modernizr.cssanimations){
					el.closest('.flight-item').addClass('animated fadeOut');
					el.closest('.flight-item')
						.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend')
						.on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							allData.favouriteItems.splice(indexArray, 1);
							el.closest('.flight-item').remove();
						});
				}
				else{
					el.closest('.flight-item').fadeOut(function(){
						allData.favouriteItems.splice(indexArray, 1);
						el.closest('.flight-item').remove();
					});
				}
				currentItem--;
			}

			dataHolder = allData;
		};

		seemore.off('click.seemore').on('click.seemore', function(e){
			e.preventDefault();
			lastItemIdx = $('.flight-item').length;
			buildHtml(false, dataHolder);
			$('#aria-loaded-item').text('')
			wcagSeeMore($('#aria-loaded-item'), $('.flight-item').length - lastItemIdx);
		});

		filterSelect.closest('[data-customSelect]')
			.off('afterSelect.filter')
			.on('afterSelect.filter', function(){
				page = 0;
				currentItem = 0;
				filterData(allData);
			});

		wrapper.on('click.favourite', '.flight-item__favourite', function(e){
			e.preventDefault();
			var self = $(this);
			$.ajax({
				url: 'ajax/success.json',
				dataType: 'json',
				type: global.config.ajaxMethod,
				beforeSend: function() {
					self.find('.ico-star').addClass('hidden');
					self.find('.loading').removeClass('hidden');
				},
				success: function(data) {
					if(data.success){
						if(self.hasClass('favourited')){
							self.removeClass('favourited');
							$('#aria-favourite-star').text('');
							setTimeout(function(){
								wcagStar($('#aria-favourite-star'));
							},100);

						}
						else{
							self.addClass('favourited');
						}
						delectFavourite(self.data('id'), self);
						self.find('.ico-star').removeClass('hidden');
						self.find('.loading').addClass('hidden');
					}
				},
				error: function(xhr, status) {
					if(status !== 'abort') {
						window.alert(L10n.flightSelect.errorGettingData);
					}
				}
			});
		});

		$.ajax({
			url: 'ajax/kf-favourites.json',
			dataType: 'json',
			type: global.config.ajaxMethod,
			success: function(data) {
				allData = data;
				allData.favouriteItems.sort(function(a){
					return a.expire ? 1 : -1;
				});
				dataHolder = allData;
				renderFilterTemplate(filterSelect, allData);
				renderItems(allData);
			},
			error: function(xhr, status) {
				if(status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	};

	var addNewCityModal = function(){
		var modal = $('.popup--add-cities');
		var form = $('.form--phone-number', modal);
		var addMore = $('.button-group-1 input:last', modal);
		var tableDefault = $('.table-default .table-content--1', modal);
		var clone = $('.table-row:first', tableDefault).clone();
		var renderOrder = function(){
			tableDefault.find('.table-col:first-child span').each(function(idx){
				$(this).text((idx + 1)+'.');
			});
		};
		var idClone = 0;
		var doClone = function(){
			idClone ++;
			var tmp = clone.removeAttr('data-validate-row').find('[data-validate-col]').removeAttr('data-validate-col').end().clone().appendTo(tableDefault);
			var tmpLabel = tmp.find('label');
			tmpLabel.eq(0).attr('for', 'clone-' + idClone);
			tmp.find('input').attr('id', 'clone-' + idClone);
			tmpLabel.eq(1).attr('for', 'clone-' + idClone + '-label');
			tmpLabel.eq(1).children('select').attr('id', 'clone-' + idClone + '-label');
		};

		clone.find('input').removeAttr('id');
		renderOrder();

		addMore.off('click.addMore').on('click.addMore', function(e){
			e.preventDefault();
			doClone();
			SIA.initAutocompleteCity();
			renderOrder();
			tableDefault.scrollTop(tableDefault.prop('scrollHeight') - tableDefault.height());
		});

		form.validate({
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			onfocusout: global.vars.validateOnfocusout
		});
	};

	// Trigger close popup and custom select when opened menu
	$('.menu .ico-nav').click(function() {
		global.vars.body.trigger('click');
	});

	var wcagStar = function(starEl) {
		var wcagStarEl = starEl,
				starText = L10n.wcag.unfavouriteLable;
			wcagStarEl.text(starText);
	};
	var wcagSeeMore = function(seemoreEl, count) {
		var wcagSeeMoreEl = seemoreEl,
				seemoreText = L10n.wcag.seemoreLabel.format(count);
			wcagSeeMoreEl.text(seemoreText);
	};

	var wcag = function() {

		var flightList = $('.flights-list').find('.flight-item');

		flightList.attr({
			'tabindex' : 0
		})

		if($('#aria-favourite-star').length === 0) {
			$('.favourites-list .items').prepend('<span class="ui-helper-hidden-accessible" id="aria-favourite-star" aria-atomic="true" aria-live="assertive"></span>')
		};

		if($('#aria-loaded-item').length === 0) {
			$('.favourites-list .items').prepend('<span class="ui-helper-hidden-accessible" id="aria-loaded-item" aria-atomic="true" aria-live="assertive"></span>')
		};

		if($('#aria-filter-item').length === 0) {
			$('.favourites-list').prepend('<span class="ui-helper-hidden-accessible" id="aria-filter-item" aria-atomic="true" aria-live="assertive"></span>')
		};

		$('.flight-item__favourite').each(function(){
				if($(this).is('.favourited')) {
					$(this).attr({
						'aria-label': L10n.wcag.favouriteLable
					});
				}
			});

	};

	var initModule = function() {
		initFavourite();
		addNewCityModal();
	};

	initModule();
};
