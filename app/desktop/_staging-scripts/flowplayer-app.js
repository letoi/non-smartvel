/**
 * @name SIA
 * @description Define global flowPlayerApp functions
 * @version 1.0
 */
SIA.flowPlayerApp = function() {
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var win = global.vars.win;
	var flyingFocus = $();
	var dataVideo = globalJson.dataVideo;

	// if(dataVideo){
	//  dataVideo = {
	//    url: {
	//      webm: '//stream.flowplayer.org/night7.webm',
	//      mp4: '//stream.flowplayer.org/night7.mp4',
	//      m3u8: '//stream.flowplayer.org/night7.m3u8'
	//    },
	//    image: [{
	//      url: 'images/img-1.jpg',
	//      description: 'Lorem ipsum dolor sit dipiscing elit.'
	//    },{
	//      url:'images/img-2.jpg',
	//      description: 'Curabitur congue tortor vitae era.'
	//    }],
	//    playlist: [
	//      {
	//        sources: [
	//          {
	//            type: 'video/webm',
	//            src: '//stream.flowplayer.org/night7.webm'
	//          },
	//          {
	//            type: 'video/mp4',
	//            src: '//stream.flowplayer.org/night7.mp4'
	//          },
	//          {
	//            type: 'video/flash',
	//            src: 'mp4:night7'
	//          }
	//        ]
	//      },
	//      {
	//        sources: [
	//          {
	//            type: 'video/webm',
	//            src:'//stream.flowplayer.org/night5.webm'
	//          },
	//          {
	//            type: 'video/mp4',
	//            src: '//stream.flowplayer.org/night5.mp4'
	//          },
	//          {
	//            type: 'video/flash',
	//            src: 'mp4:night5'
	//          }
	//        ]
	//      }
	//    ]
	//  };
	// }

	// var allVideos = [
	//  {
	//    sources: [
	//      {
	//        type: 'video/webm',
	//        src: '//stream.flowplayer.org/night7.webm'
	//      },
	//      {
	//        type: 'video/mp4',
	//        src: '//stream.flowplayer.org/night7.mp4'
	//      },
	//      {
	//        type: 'video/flash',
	//        src: 'mp4:night7'
	//      }
	//    ]
	//  },
	//  {
	//    sources: [
	//      {
	//        type: 'video/webm',
	//        src:'//stream.flowplayer.org/night5.webm'
	//      },
	//      {
	//        type: 'video/mp4',
	//        src: '//stream.flowplayer.org/night5.mp4'
	//      },
	//      {
	//        type: 'video/flash',
	//        src: 'mp4:night5'
	//      }
	//    ]
	//  },
	// ];

	var initFlowPlayer = function() {
		var player;
		var url;

		var applyFlowPlayer = function(temp) {
			var videoCollect = $('[data-flow-url]');
			var videoLightbox = $(temp).appendTo(body);
			var thumbnail = videoLightbox.find('.info-watch');

			videoLightbox.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',
				afterShow: function() {
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
				},
				afterHide: function() {
					player.off('progress').on('progress', function() {
						if ($('.popup--video-lightbox').hasClass('hidden')) {
							player.stop();
						}
					});

					win.trigger('resize.openMenuT');
					win.trigger('resize.resetTabMenu');
				}
			});

			// player = $('#flowplayer').flowplayer({
			//  splash: true,
			//  ratio: 9/16,
			//  embed: {
			//    // include the whole playlist when embedding
			//    playlist: true,
			//    // embed with same skin
			//    skin: '//releases.flowplayer.org/6.0.1/skin/minimalist.css'
			//  }
			// });

			thumbnail.each(function(idx) {
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e) {
					e.preventDefault();
					// url = dataVideo.url[idx];
					// player.load(url);
					if (player) {
						// player.load(url);
						player.setPlaylist(dataVideo.playlist);
						player.play(idx);
						// player.load(dataVideo.playlist[idx]);
					} else {
						player = window.flowplayer($('#flowplayer'), {
							// splash: true,
							ratio: 9 / 16,
							playlist: idx ? dataVideo.playlist.slice(idx) : dataVideo.playlist
						});
						// player.load(dataVideo.playlist[idx]);
					}
				});
			});

			videoCollect.each(function() {
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e) {
					e.preventDefault();
					// url = dataVideo.url[idx];
					url = self.data('flow-url');
					if (player) {
						player.setPlaylist(dataVideo.playlist);
						player.play(url);
						// player.load(dataVideo.playlist[url]);
					} else {
						player = window.flowplayer($('#flowplayer'), {
							// splash: true,
							ratio: 9 / 16,
							playlist: url ? dataVideo.playlist.slice(url) : dataVideo.playlist
						});
					}
					videoLightbox.Popup('show');
				});
			});
		};
		$.get(config.url.videoLightbox.flowplayer, function(temp) {
			var template = window._.template(temp, {
				'data': dataVideo
			});
			applyFlowPlayer(template);
		}, 'html');
	};

	var initModule = function() {
		initFlowPlayer();
	};
	initModule();
};
