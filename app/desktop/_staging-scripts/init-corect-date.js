/**
 * @name SIA
 * @description Define global initCorrectDate functions
 * @version 1.0
 */
SIA.initCorrectDate = function(){
	// var target = $('[data-rule-validatedate]');
	// var detectDate = function(d, m, y, el){
	// 	var nd = new Date();
	// 	var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
	// 	if(d > getLastDate.getDate()){
	// 		el.val(getLastDate.getDate());
	// 		el.closest('[data-customselect]').customSelect('refresh');
	// 	}
	// };
	// target.each(function(){
	// 	var self = $(this);
	// 	var data = self.data('rule-validatedate');
	// 	var date = $(data[0]);
	// 	var month = $(data[1]);
	// 	var year = $(data[2]);

	// 	date.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
	// 		detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 	});
	// 	month.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
	// 		detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 	});
	// 	year.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
	// 		detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 	});
	// });

	var convertMonth = function(n, isGetIndex){
		var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return isGetIndex ? m.indexOf(n ? n.slice(0, 3) : 'Jan') : m[n];
	};

	var correctDate = function(){
		var target = $('[data-rule-validatedate]');
		var detectDate = function(d, m, y, el){
			var nd = new Date();
			var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
			if(parseInt(d) > getLastDate.getDate()){
				el.val(getLastDate.getDate());
				el.closest('[data-customselect]').customSelect('refresh');
			} else if(parseInt(d) < 1) {
				el.val(1);
				el.closest('[data-customselect]').customSelect('refresh');
			}
		};
		target.each(function(){
			var self = $(this);
			var data = self.data('rule-validatedate');
			var date = $(data[0]);
			var month = $(data[1]);
			var year = $(data[2]);

			date.off('blur.correctDate').on('blur.correctDate', function() {
				detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
			});
			month.off('blur.correctDate').on('blur.correctDate', function() {
				detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
			});
			year.off('blur.correctDate').on('blur.correctDate', function() {
				detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
			});
		});
	};

	correctDate();

	var correctDMY = function(){
		$('[data-month],[data-date],[data-year]').each(function(){
			var self  = $(this);
			var input = self.find('input');
			var select = self.find('select');

			var hasValue = function(val, el){
				var valid = false;
				el.each(function() {
					if($(this).data('value') + '' === val) {
						valid = true;
					}
				});
				return valid;
			};

			input.off('blur.correctMonth').on('blur.correctMonth', function(){
				
				if(input.data('autocomplete') || typeof(input.attr('autocomplete')) !== 'undefined' ){
					var li = input.autocomplete( 'widget' ).find('li');
				}else{
					var li = input.find('li');
				}
				
				if(li.length === 1 && li.data('value') === L10n.globalSearch.noMatches){
					input.val(select.find('option:first').text()).valid();
					self.removeClass('default');
				}
				else{
					// if(li.length !== select.children().length){ // has bug when double click text box
					if(input.val()) {
						if(!hasValue(input.val(), li)){
							input.val(li.first().data('value')).valid();
							self.removeClass('default');
						}
					}
				}
			});
		});
	};

	correctDMY();
};
