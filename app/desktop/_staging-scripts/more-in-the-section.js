
/**
 * @name SIA
 * @description Define global moreInTheSection functions
 * @version 1.0
 */
SIA.moreInTheSection = function(){
	var global = SIA.global;
	var	config = global.config;
	var	win = global.vars.win;
	var	totalTabletSlide = 3;
	var	timerDetectSlider;

	var initSliderMoreIn = function(){
		var blockMoreIn = $('[data-tablet-slider] .slides');

		win.off('resize.tabletSlider').on('resize.tabletSlider',function() {
			clearTimeout(timerDetectSlider);
			timerDetectSlider = setTimeout(function() {
				if(window.innerWidth < config.tablet) {
					blockMoreIn.each(function() {
						var slider = $(this);
						if(!slider.hasClass('slick-initialized')) {
							// slider.slick('unslick');
							slider
								.slick({
									siaCustomisations: true,
									dots: true,
									speed: 300,
									draggable: true,
									slidesToShow: totalTabletSlide,
									slidesToScroll: totalTabletSlide,
									arrows: false,
									// start wcag opts
									accessibility: true,
									assistiveTechnology: true,
									customPaging: function(slider, i) {
										var index = (i+1);
										return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.moreInTheSection.customPagingTextPre + index + L10n.sliders.moreInTheSection.customPagingTextPost);
									},
									pauseOnFocus: true,
									focusOnSelect: true,
									pauseOnHover: true,
									// end wcag opts
								});
						}
					});
				}
				else{
					blockMoreIn.each(function(){
						var slider = $(this);
						if(slider.hasClass('slick-initialized')) {
							slider.slick('unslick');
						}
					});
				}
			}, 50);
			// Correct timeout from 400 into 50 for resize window
		}).trigger('resize.tabletSlider');
	};

	var initModule = function(){
		initSliderMoreIn();
	};

	initModule();
};
