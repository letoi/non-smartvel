$(document).ready(function() {
	var $vouchers = $('.js-travel-agents');
	var global = SIA.global;

	function initialize() {
		return SIA.AjaxCaller.template('sqc-travel-agents.tpl').then(function(tplContent) {
			return 	SIA.AjaxCaller.json('sqc-travel-agents-land.json').then(function(data) {
					var template = window._.template(tplContent, {
						data: data,
					});
					$vouchers.html(template);
				});
			}).fail(function(err) {
				console.log(err);
		});
	}

	initialize();
});
