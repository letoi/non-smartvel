<div class="blk-heading">
  <h3 class="sub-heading-1--dark"><%- data.depart %> to <%- data.destination %></h3>
</div>
<p class="text-note"><%- data.textNote %></p>
<table class="table--1 desktop">
  <tbody>
    <tr class="table--1__header">
      <td><%- data.heading.flight %></td>
      <td>&nbsp;</td>
      <td><%- data.heading.scheduled %></td>
      <td><%- data.heading.estimated %></td>
      <td><%- data.heading.actual %></td>
      <td><%- data.heading.status %></td>
    </tr>
    <% _.each(data.results, function(result, resultIdx) { %>
      <% _.each( result.flight, function( flight, idx ){ %>
        <% _.each(flight.departs, function(depart, departIdx) { %>
          <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
            <% if(depart.flightNumber && departIdx == 0) {%>
              <td rowspan="<%= flight.departs.length * 2 %>" class="info--flight-number">
                <div class="info info--1">
                  <h3 class="info-title"><%- depart.flightNumber %></h3>
                </div>
              </td>
            <% } %>
            <td>
              <div class="info info--2">
                <div class="to">Departs</div>
                <p class="trip">
                  <%- depart.place %>
                  <!-- <span><%- depart.trip %></span> -->
                  <% if(depart.terminal && depart.terminal != 'Not available') {%>
                      <span>- Terminal </span>
                      <span><%- depart.terminal %></span>
                  <% } %>
                </p>
              </div>
            </td>
            <td>
              <div class="info info--1">
                <div class="time"><%- depart.scheduled.time %></div>
                <p><%- depart.scheduled.date %></p>
              </div>
            </td>
            <td>
              <% if(depart.estimated && depart.estimated.time) { %>
                <div class="info info--1"><div class="time"><%- depart.estimated.time %></div><p><%- depart.estimated.date %></p></div>
              <% } else { %>
                <div class="dash">–</div>
              <% } %>
            </td>
            <td>
              <% if(depart.actual && depart.actual.time) { %>
                <div class="info info--1"><div class="time"><%- depart.actual.time %></div><p><%- depart.actual.date %></p></div>
              <% } else { %>
                <div class="dash">–</div>
              <% } %>
            </td>
            <td rowspan="2" class="status <%= ((flight.departs.length - 1) == departIdx) ? "no-border-bottom" : "" %>">
              <% if(depart.status) {%>
                <div class="info"><p><%- depart.status.arrived %></p></div>
              <% } %>
            </td>
          </tr>
          <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %> <%- departIdx == (flight.departs.length - 1) ? 'no-border-bottom' : '' %>">
            <td>
              <div class="info info--2">
                <div class="to">Arrives</div>
                <p class="trip">
                  <%- flight.arrives[departIdx].place %>
                  <!-- <span><%- flight.arrives[departIdx].trip %></span> -->
                  <% if(flight.arrives[departIdx].terminal && flight.arrives[departIdx].terminal != 'flight.arrives[departIdx].terminal') {%>
                      <span>- Terminal </span>
                      <span><%- flight.arrives[departIdx].terminal %></span>
                  <% } %>
                </p>
              </div>
            </td>
            <td>
              <div class="info info--1">
                <div class="time"><%- flight.arrives[departIdx].scheduled.time %></div>
                <p><%- flight.arrives[departIdx].scheduled.date %></p>
              </div>
            </td>
            <td>
              <% if(flight.arrives[departIdx].estimated && flight.arrives[departIdx].estimated.time) { %>
                <div class="info info--1"><div class="time"><%- flight.arrives[departIdx].estimated.time %></div><p><%- flight.arrives[departIdx].estimated.date %></p></div>
              <% } else { %>
                <div class="dash">–</div>
              <% } %>
            </td>
            <td>
              <% if(flight.arrives[departIdx].actual && flight.arrives[departIdx].actual.time) { %>
                <div class="info info--1"><div class="time"><%- flight.arrives[departIdx].actual.time %></div><p><%- flight.arrives[departIdx].actual.date %></p></div>
              <% } else { %>
                <div class="dash">–</div>
              <% } %>
            </td>
          </tr>
        <% }); %>
        <% if(flight.alerts) {%>
          <tr>
            <td colspan="5" class="flight-alert">
              <div class="wrapper">
                <div class="alert-block checkin-alert">
                  <div class="inner">
                    <div class="alert__icon"><em class="ico-alert">&nbsp;</em></div>
                    <div class="alert__message"><%- flight.alerts %></div>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        <% } %>
        <% if(flight.LayoverTime) {%>
          <tr>
            <td colspan="5" class="layover-time">
              <div class="info info--1">
                <p><%- flight.LayoverTime.language %><%- flight.LayoverTime.time %></p>
              </div>
            </td>
          </tr>
        <% } %>
      <% }); %>
    <% }); %>
  </tbody>
</table>
<div class="table-mobile__popup">
  <table class="table--1">
    <tbody>
      <% _.each( data.results, function( result, resultIdx ){ %>
        <% _.each( result.flight, function( flight, idx ){ %>
          <% _.each(flight.departs, function(depart, departIdx) { %>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.flight %></div>
                  <p><%- depart.flightNumber %></p>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to">Departs</div>
                  <p class="trip">
                    <%- depart.place %>
                    <!-- <span><%- depart.trip %></span> -->
                    <% if(depart.terminal && depart.terminal != 'flight.arrives[departIdx].terminal') {%>
                        <span>- Terminal </span>
                        <span><%- depart.terminal %></span>
                    <% } %>
                  </p>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.scheduled %></div>
                  <p><%- depart.scheduled.time %> - <%- depart.scheduled.date %></p>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.estimated %></div>
                  <% if(depart.estimated && depart.estimated.time) { %>
                    <p><%- depart.estimated.time %> - <%- depart.estimated.date %></p>
                  <% } else { %>
                    <p class="dash">-</p>
                </div>
                <% } %>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.actual %></div>
                  <% if(depart.actual && depart.actual.time) { %>
                    <p><%- depart.actual.time %> - <%- depart.actual.date %></p>
                  <% } else { %>
                    <p class="dash">-</p>
                  <% } %>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to">Arrives</div>
                  <p class="trip">
                    <%- flight.arrives[departIdx].place %>
                    <!-- <span><%- flight.arrives[departIdx].trip %></span> -->
                    <% if(flight.arrives[departIdx].terminal && flight.arrives[departIdx].terminal != 'Not available') {%>
                        <span>- Terminal </span>
                        <span><%- flight.arrives[departIdx].terminal %></span>
                    <% } %>
                  </p>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.scheduled %></div>
                  <p><%- flight.arrives[departIdx].scheduled.time %> - <%- flight.arrives[departIdx].scheduled.date %></p>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.estimated %></div>
                  <% if(flight.arrives[departIdx].estimated && flight.arrives[departIdx].estimated.time) { %>
                    <p><%- flight.arrives[departIdx].estimated.time %> - <%- flight.arrives[departIdx].estimated.date %></p>
                  <% } else { %>
                    <p class="dash">-</p>
                  <% } %>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td>
                <div class="info info--2">
                  <div class="to"><%- data.heading.actual %></div>
                  <% if(flight.arrives[departIdx].actual && flight.arrives[departIdx].actual.time) { %>
                    <p><%- flight.arrives[departIdx].actual.time %> - <%- flight.arrives[departIdx].actual.date %></p>
                  <% } else { %>
                    <p class="dash">-</p>
                  <% } %>
                </div>
              </td>
            </tr>
            <tr class="<%- resultIdx % 2 == 1 ? 'bgd-1' : '' %>">
              <td class="status">
                <div class="info">
                  <div class="to"><%- data.heading.status %></div>
                  <p><%- depart.status.arrived %></p>
                </div>
              </td>
            </tr>
          <% }); %>
          <% if(flight.alerts) {%>
            <tr>
              <td class="flight-alert">
                <div class="wrapper">
                  <div class="alert-block checkin-alert">
                    <div class="inner">
                      <div class="alert__icon"><em class="ico-alert">&nbsp;</em></div>
                      <div class="alert__message"><%- flight.alerts %></div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          <% } %>
          <% if(flight.LayoverTime) {%>
            <tr class="layover-time">
              <td>
                <div class="info info--1">
                  <p><%- flight.LayoverTime.language %><%- flight.LayoverTime.time %></p>
                </div>
              </td>
            </tr>
          <% } %>
        <% }); %>
      <% }); %>
    </tbody>
  </table>
</div>
