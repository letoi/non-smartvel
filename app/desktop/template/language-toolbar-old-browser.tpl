<aside class="toolbar toolbar--language">

<div class="toolbar__content lang__content">
		<a class="toolbar__close language__close" href="#"></a>
		<p class="toolbar__text">This page is in&nbsp;<span class="toolbar__language">Simplified Chinese</span>. Would you like to display it in&nbsp;<span class="toolbar__language">English</span>?</p>
		<ul class="toolbar__control">
			<li><a href="#" data-lang-toolbar="true">Yes</a><span>|</span>
			</li>
			<li><a href="#" data-lang-toolbar="false">No</a>
			</li>
		</ul>
	</div>

	<div class="toolbar__content alert__content">
		<a class="toolbar__close alert__close" href="#"></a>
		<p class="toolbar__text">Some features and designs of the new singaporeair.com may not display properly on this version of your browser. For the best experience, update to the latest version. <span class="toolbar__language">New website FAQs</span></p>
	</div>

</aside>
