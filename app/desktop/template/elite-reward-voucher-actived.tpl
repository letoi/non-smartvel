<% if(data){ %>
  <div class="voucher">
    <span class="voucher__heading">
      Applied / Used Vouchers
    </span>
    <% _.each(data, function(data, dataIdx){ %>
      <div class="voucher__group">
        <div class="voucher__group__detail">
          <div class="voucher__group__detail__voucher">
            <div class="voucher__group__detail__title">
              <span class="voucher__icon voucher__icon--<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>double<%} else { %>seat<% } %>"></span>
              <span>
                <%= data.rewardType %>
              </span>
            </div>
            <span class="voucher__group__detail__voucher-code">
              voucher code
              <%- data.rewardNumber%>
            </span>
          </div>
          <div class="voucher__group__detail__voucher-disable">
            <a class="btn-2 voucher__btn--disabled">
             <% if(data.rewardStatus == "Applied") { %> Applied <% } else { %>used<% } %>
                <span data-tooltip="true" data-type="2" data-max-width="188" data-content="&lt;p class=tooltip__text-2&gt;You may recently used this &lt;br&gt; vouchers,and it's being&lt;br&gt; processed.&lt;/p&gt;"
                class="<% if (data.rewardStatus == "Applied") { %> ico-info-round-fill <% } %>"> </span>
              </a>
          </div>
           <div class="voucher__group__detail__link">
              <a href="javascript:void(0);" class="ico-point-r js-lightbox" data-trigger-popup=".popup--term-and-condition">Terms and conditions</a>
            </div>
        </div>
      </div>
    <% }) %>
  </div>
 <% } %>

 <aside class="popup popup--term-and-condition hidden">
  <div class="popup__inner">
    <div class="popup__content">
      <a href="#" class="popup__close">
        <span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>
      <h2 class="popup__heading">Terms and conditions</h2>
      <ul class="popup__heading__list">
        <li>
          <span>Only one Double miles acctual voucher may be used for one flight segment.</span>
        </li>
        <li>
          <span>The voucher can only be user by the principal PPS Club member.</span>
        </li>
        <li>
          <span>The voucher can only be user for flights operated by Singapore Airlines.</span>
        </li>
        <li>
          <span>The coucher can be combined with upgrade voucher. However, double miles will only be awarded based on the original
            (i.e., pre-upgraded) booking.</span>
        </li>
        <li>
          <span>Miles will be awarded upon the completion of your flight.</span>
        </li>
        <li>
          <span>Vouchers ara valid for 12 months from the date of issue.</span>
        </li>
      </ul>
    </div>
  </div>
</aside>
