<div class="form-group">
  <h3 class="title-5--blue">Your saved credit / debit card</h3>
  <div class="grid-col one-half">
    <div class="grid-inner">
      <div data-customselect="true" class="custom-select custom-select--2 get-value-card default">
        <label for="customSelect-2-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Label</span>
        </label><span class="select__text" aria-hidden="true">Select</span><span class="ico-dropdown" aria-hidden="true">Select</span>
        <select id="save-credit-debit" name="save-credit-debit" class="save-credit-debit">
          <option value="" class=" select-card-empty">Select card</option>
          <% _.each(data.digitalWalletCardVO, function(digitalWalletCard, digitalWalletCardIdx) { %>
            <%
              var expiryDate = digitalWalletCard.expiryDate;
              var indexOfDate = expiryDate.indexOf('-');
              var month = expiryDate.slice(0, indexOfDate);
              var year = expiryDate.slice(indexOfDate + 1);
              var creditCard = digitalWalletCard.cardTokenNumber;
              var sliceStringCard = creditCard.slice(-4); 
            %>
            <option value="<%- digitalWalletCard.cardType.toLowerCase() %>.png" data-id="customSelect-4-option-<%- digitalWalletCardIdx + 1 %>" data-month="<%- month %>" data-year="<%- year %>" <% if(digitalWalletCard.expiryStatus =="expired" || digitalWalletCard.cardEligibleForTransaction == 'false'){ %> class="has-disabled disabled-card" disabled = "true" <% } %> data-name="<%- digitalWalletCard.cardHolderName %>" data-card="<%- sliceStringCard %>">&bull;&bull;&bull;&bull;<%- sliceStringCard %><% if(digitalWalletCard.expiryStatus =="expired") { %> - Expired<% } %><% if(digitalWalletCard.defaultCard ==="true") { %> - Default card<% } %></option>
          <% }); %>
        </select>
        <img src="" alt="" longdesc="img-desc.html" class="img-card-selected" data-format-card="visa">
      </div>
    </div>
  </div>
  <div class="grid-col one-half">
    <a href="#" class="pay-another-card"><em class="ico ico-another-card"></em>Pay with another card</a>
  </div>
</div>
