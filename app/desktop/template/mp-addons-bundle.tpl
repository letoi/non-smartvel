<% if(data) {
  if(data.bundle.length > 0) { %>
    <div class="your-flight-item block-2">
      <div class="addons-your-flight-content">
        <div data-accordion-wrapper="2">
          <div class="description">
            <figure>
              <% if(data.bundle[0].isRecommended == "true") {
                if(data.bundle[0].packImageUrl !== "") { %>
                  <img src="<%- data.bundle[0].packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                <% } else { %>
                  <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                <% }
              } %>
            </figure>
            <div class="content">
              <div class="detail-content">
                <% if(data.bundle[0].isRecommended == "true") { %>
                  <span class="selected-orange">Recommended</span>
                <% } %>
                <h4 class="title-5--blue">
                  <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                    Preferred Seat + Additional baggage
                  <% } %>
                </h4>
                <% if(data.bundle[0].packDescription !== "") { %>
                  <p><% data.bundle[0].packDescription %></p>
                <% } %>
              </div>
              <div class="from-price-flight"><span class="note">From</span>
                <span class="sgd-price">
                  <% arrBundleId = [];
                    arrBundle = [];
                    arrSegmentPacks = [];
                    _.each(data.bundle, function(bundleData, bundleIdx) {
                      arrBundleId[bundleData.packId] = bundleData.packId;
                      arrBundle[bundleData.packId] = {
                        packId : bundleData.packId,
                        ancillaries : bundleData.ancillaries
                      }
                    });
                    _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                      _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                        _.each(segmentsData.packs, function (packsData, packsIdx) {
                          if (arrBundleId[packsData.packId]) {
                            arrSegmentPacks.push(packsData);
                          }
                        });
                      });
                    });
                    arrSegmentPacks.sort(function(a, b) {
                      return parseFloat(a.price) - parseFloat(b.price);
                    }); %>
                    <%- arrSegmentPacks[0].currency %> <%- Number(arrSegmentPacks[0].price).toFixed(2) %>
                </span>
                <span class="miles">Per passenger per flight</span><span class="selected-orange">save up to <%- data.bundle[0].percentageSaved + "%" %></span>
                <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                  <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="Select" class="btn-8" data-select-item="false"/>
                  <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Selected" class="hidden btn-1" data-select-item="false"/><a href="#"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Selected</span></em></a>
                </div>
              </div>
            </div>
          </div>
          <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
            <div class="block-flight-details">
              <div class="title-popup-mb">
                <span class="sub-heading-2--blue">
                  <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                    Preferred Seat + Additional baggage
                  <% } %>
                </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
              </div>
              <% _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                _.each(paxData.segments, function(segmentsData, segmentsIdx) { %>
                  <div class="block-flight-details--inner">
                    <div class="addons-landing--inner">
                      <div data-accordion="2" class="addons-landing-content data-pss-xbag">
                        <% if (segmentsIdx == 0) { %>
                        <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize active">
                        <% } else { %>
                        <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                        <% } %>
                          <span class="title-5--blue"><%- segmentsData.segmentDescription %></span>
                          <span class="bundle-selected hidden"><span></span>bundle selected</span><em class="ico-point-d"></em>
                        </a>
                        <div data-accordion-content="2" class="accordion__content">
                          <div class="bundle-flight-block"><span class="title-5--blue"><%- paxData.paxName %></span>
                            <div class="bundle-flight--inner">
                              <% _.each(segmentsData.packs, function(packsData, packsIdx) {
                                if (arrBundle[packsData.packId]) { %>
                                  <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                    </div>
                                    <div class="bundle-flight--inner">
                                  <% } %>
                                  <div class="bundle-flight-item">
                                    <div class="bundle-flight-item--inner"><span class="title-5--blue">Bundle <%- packsIdx + 1 %></span>
                                      <ul class="bundle-baggage two-bundle-baggage">
                                        <li><em class="ico-1-preferred"></em>Preferred Seat</li>
                                        <li>
                                          <% _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                            if (ancillariesData.ancillaryType == "xBag") { %>
                                              <em class="ico-business-1"></em>Additional baggage <%- ancillariesData.units.numberOfUnits%><%- ancillariesData.units.unitOfMeasurement.toLowerCase() %>
                                            <% }
                                          }); %>
                                        </li>
                                      </ul>
                                      <div class="select-price"><span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span>
                                        <input type="button" name="select-bundle-<%- segmentsData.segmentId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- segmentsData.segmentId %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false" data-total-fare/>
                                        <input type="button" name="selected-bundle-<%- segmentsData.segmentId %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- segmentsData.segmentId %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false"/>
                                      </div>
                                    </div>
                                  </div>
                                <% }
                              }); %>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <% });
              }); %>
              <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
            </div>
          </div>
        </div>
      </div>
    </div>
  <% }
} %>
