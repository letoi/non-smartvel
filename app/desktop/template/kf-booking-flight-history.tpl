<% _.each(data, function(flight, idx) { %>
	<tr class="<%- idx % 2 == 0 ? 'odd' : 'even' %> <%- showNumber && idx < showNumber ? '' : 'hidden' %>">
		<td>
			<div class="data-title">Departure date</div>
			<span><%- flight.longDate %></span>
		</td>
		<td>
			<div class="data-title">Details</div>
			<%- flight.number %>
			<%- flight.from %> to <%- flight.to %>
		</td>
		<td class="">
			<div class="data-title">Cabin Class</div>
			<%- flight.classType %>
		</td>
		<td class="">
			<div class="data-title">Tickets and receipts</div>
			<% if(flight.viewTktandRcpt == "true") { %>
				<a href="#" class="view-tickets" data-target-tpl="" data-trigger-popup=".popup--view-tickets" tabindex="0" data-src="<%= flight.url %>"><em class="ico-point-r"></em> <span>View</span> </a>
			<% } else { %>
				-
			<% } %>	
		</td>
	</tr>
<% }); %>
