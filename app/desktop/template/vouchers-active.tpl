<% if(data !== void 0){ %>
	<div class="manage-booking manage-booking--desktop hidden">
		<span class="manage-booking__heading">You may apply these vouchers to this booking:</span>
		<div class="manage-booking__content">
			<% _.each(data, function(data, dataIdx) { %>
			<div class="manage-booking__group js-voucher-item<% if(dataIdx > 2 ){ %> hidden<% } %>">
				<div class="manage-booking__group__detail">
					<% if (data.type == "ComplimentarySeatUpgrade") { %>
						<div class="alert-block manage-booking__group__error">
							<div class="inner">
								<div class="alert__icon"><em class="ico-close-round-fill"></em></div>
								<div class="alert__message">Lorem ipsum dolor, sit amet consectetur adipisicing elit, sed do elusmod tempor incididunt ut labore et dolore magna aliqua.</div>
							</div>
						</div>
						<% } %>
						<div class="manage-booking__group__detail__voucher manage-booking__icon manage-booking__icon--<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>double<%} else { %>seat<% } %>">
						<span class="manage-booking__group__detail__title"><%= data.rewardType %></span>
							<span class="manage-booking__group__detail__voucher-code">voucher code <%- data.rewardNumber%></span>
							<div class="manage-booking__group__detail__link">
								<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>
									<a href="#" class="ico-point-r js-lightbox" data-trigger-popup=".popup--how-to-use">How to use</a>
								<% } %>
								<a href="#" class="ico-point-r js-lightbox" data-trigger-popup=".popup--term-and-condition">Terms and conditions</a>
							</div>
						</div>
						<div class="manage-booking__group__detail__<% if (data.type == "ComplimentarySeatUpgrade") { %>voucher-disable<%} else { %>use-voucher<% }%>">
							<p class="manage-booking__group__detail__time"><%- data.rewardExpiryDate %></p>
							<a href="#" class="btn-<% if (data.type == "ComplimentarySeatUpgrade"){%>2 manage-booking__btn--disabled<% } else { %>1<% }%>">use voucher</a>
						</div>
					</div>
				</div>
				<% if(dataIdx > 2 ) { %>
					<a href="#" class="js-show-all manage-booking__show-all"  data-see-more="true">
						Show all vouchers
					</a>
				<% } %>
			<% }) %>
		</div>
	</div>
  <div class="elite-rewards__vouchers-box  manage-booking--mobile hidden">
    <span class="elite-rewards__vouchers__heading">
       You may apply these vouchers to this booking:
    </span>
    <% _.each(data, function(data, dataIdx){ %>
			<div class="manage-booking__group js-voucher-item<% if(dataIdx > 2 ){ %> hidden<% } %>">
				<div class="manage-booking__group__detail">
					<% if (data.type == "ComplimentarySeatUpgrade") { %>
						<div class="alert-block manage-booking__group__error">
							<div class="inner">
								<div class="alert__icon"><em class="ico-close-round-fill"></em></div>
								<div class="alert__message">Lorem ipsum dolor, sit amet consectetur adipisicing elit.</div>
							</div>
						</div>
					<% } %>
					<div class="manage-booking__group__detail__title">
						<span class="manage-booking__group__icon manage-booking__group__icon--<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>double<%} else { %>seat<% } %>"></span>
						<span>
							<%= data.rewardType %>
						</span>
					</div>
					<p class="manage-booking__group__detail__voucher-code">
						 voucher code <%- data.rewardNumber%>
					</p>
					<p class="manage-booking__group__detail__voucher-time">
						 <%- data.rewardExpiryDate %>
					</p>
					<div class="manage-booking__group__detail__user-voucher">
						<a href="#" class="btn-<% if (data.type == "ComplimentarySeatUpgrade"){%>2 manage-booking__btn--disabled<% } else { %>1<% }%>">
							use voucher
						</a>
					</div>
					<div class="manage-booking__group__detail__link">
						<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>
							<p class="manage-booking__group-how">
								<a href="#" class="ico-point-r js-lightbox" data-trigger-popup=".popup--how-to-use">How to use</a>
							</p>
						<% } %>

						<p class="manage-booking__group-term <% if(data.type != "EarnDoubleKrisFlyerMiles") { %>no-float<% } %>">
							<a href="#" class="ico-point-r js-lightbox" data-trigger-popup=".popup--term-and-condition">Terms and conditions</a>
						</p>
					</div>
				</div>
				<div class="manage-booking__group__detail__<% if (data.type == "ComplimentarySeatUpgrade") { %>voucher-disable<%} else { %>use-voucher<% }%>">
					<p class="manage-booking__group__detail__time"><%- data.rewardExpiryDate %></p>
					<a href="#" class="btn-<% if (data.type == "ComplimentarySeatUpgrade"){%>2<% } else { %>1<% }%>">use vouchers</a>
				</div>
			</div>

			<% if(dataIdx > 2 ) { %>
				<a href="#" class="js-show-all manage-booking__show-all">
						Show all vouchers
				</a>
			<% } %>
		<% }) %>
	</div>
<% } %>

<aside class="popup popup--term-and-condition hidden">
	<div class="popup__inner">
		<div class="popup__content"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>
			<h2 class="popup__heading">Terms and conditions</h2>
			<ul class="popup__heading__list">
				<li><span>Only one Double miles acctual voucher may be used for one flight segment.</span></li>
				<li><span>The voucher can only be user by the principal PPS Club member.</span></li>
			  <li><span>The voucher can only be user for flights operated by Singapore Airlines.</span></li>
				<li><span>The coucher can be combined with upgrade voucher. However, double miles will only be awarded based on the original (i.e., pre-upgraded) booking.</span></li>
				<li><span>Miles will be awarded upon the completion of your flight.</span></li>
				<li><span>Vouchers ara valid for 12 months from the date of issue.</span></li>
			</ul>
		</div>
	</div>
</aside>

<aside class="popup popup--how-to-use hidden">
	<div class="popup__inner">
		<div class="popup__content"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>
			<h2 class="popup__heading">How to use double KrisFlyer miles voucher</h2>
			<div class="popup__description">
				Simply select this Elite Reward and apply it to the flight segment of your choice in your bookings. This Elite Reward can only be used by the principal Elite Gold member. Please allow 2-4 weeks for the KrisFlyer miles to be reflected in your account.
			</div>

			<div class="popup__sub-heading">How miles are calculated</div>
			<div class="popup__description">As an example, if a PPS Club member travels on Business Class from Singapore to London:</d>
			<table class="popup__table">
				<tr>
					<td class="popup__table-text">Base miles earned</td>
					<td class="popup__table-value">6,761 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Cabin bonus (1.25X)</td>
					<td class="popup__table-value">6.761 x 1.25 = 8,452 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Double miles accrual voucher</td>
					<td class="popup__table-value">8,452 x 2 = 16,904 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Tier bonus (0.25X)</td>
					<td class="popup__table-value">16,904 x 0.25 = 18,595 miles</td>
				</tr>
			</table>
			<div class="popup__description">The double miles accrual bonus is applied after the cabin bonus and before the tier bonus.</div>
		</div>
	</div>
</aside>
