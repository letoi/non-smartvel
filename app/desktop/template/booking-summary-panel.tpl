<% if(data) { %>
	<div class="bsp-booking-summary__heading">
		<div class="bsp-booking-summary__heading-wrap">
			<% if(data.flight.length > 2) { %>
				<% _.each(data.flight, function(itemFly, idx){ %>
					<span class="bsp-flight hidden-mb-small hidden-tb"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
				<% }) %>
				<% _.each(data.flight.slice(0 , 4), function(itemFly, idx4){ %>
					<span class="bsp-flight hidden-mb-small hidden-dt"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
				<% }) %>
				<span class="plus-more-flight hidden-mb-small hidden-dt">
					<% if(data.flight.length > 4) { %>
						<% var numberTablet = data.flight.length - 4 %>
					+ <%- numberTablet %> more flights</span>
					<% } %>
				<span class="plus-more-flight hidden-tb-dt"><% var numberApplyMobile = data.flight.length %> <%- numberApplyMobile %> more flights</span>
				<span class="adults">
					<% if(data.adultCount) { %>
						· <%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %>
					<% } %>,
				</span>
				<% if(!$('body').hasClass('hide-edit-search')) { %>
					<a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
				<% } %>
			<% } else { %>
				<% if(data.flight.length < 2) { %>
						<span class="bsp-flight"><%- data.flight[0].originAirportCode %> - <%- data.flight[0].destinationAirportCode %></span>
						<span class="adults">
							<% if(data.adultCount) { %>
								· <%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %>
							<% } %>
						</span>
						<% if(!$('body').hasClass('hide-edit-search')) { %>
							<a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
						<% } %>
				<% } else { %>
					<% if(data.flight.length = 2) { %>
						<span class="bsp-flight hidden-mb-small"><%- data.flight[0].originAirportCode %> - <%- data.flight[0].destinationAirportCode %><em class="ico-return-arrows"></em><%- data.flight[1].originAirportCode %> - <%- data.flight[1].destinationAirportCode %></span>
						<span class="bsp-flight hidden-tb-dt"><%- data.flight[0].originAirportCode %><em class="ico-return-arrows"></em><%- data.flight[1].originAirportCode %></span>
						<span class="adults">
							<% if(data.adultCount) { %>
							· <%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %>
							<% } %>
						</span>
						<% if(!$('body').hasClass('hide-edit-search')) { %>
							<a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
						<% } %>
					<% } %>
				<% } %>
			<% } %>
		</div>
	</div>
	<div class="wrap-content-bsp">
		<div class="bsp-booking-summary__content all-transition">
			<div class="bsp-booking-summary__content-wrap">
				<div class="bsp-booking-summary__content-control">
					<div class="bsp-total-fare">
						<div class="total-fare--inner">
							<% if($('body').hasClass('review-refund-page')) { %>
								<span class="flight-date-title">Total to be refunded</span>
							<% } else if($('body').hasClass('review-atc-pay') || $('body').hasClass('atc-payment-page')) { %>
								<span class="flight-date-title">Total to be paid</span>
							<% } else { %>
								<span class="flight-date-title">Total fare</span>
							<% } %>
							<div class="flights__info">
									<% if(data.adultCount) { %>
										<span class="number-passengers" data-travel-party="<%- data.childCount > 0 ? true : false %>">
										<%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %>
									<% } %>
									</span>
									<p class="total-cost">
										<% if($('body').hasClass('review-refund-page')) { %>
											<span class="unit">
												<%
													var priceStyleSmall = data.cashToBeRefunded.toLocaleString(undefined, { minimumFractionDigits: 2 });
													var slicePrice = priceStyleSmall.indexOf(".");
													var priceUnit = priceStyleSmall.slice(0, slicePrice);
													var priceUnitSmall = priceStyleSmall.slice(slicePrice);
												%>
												<%- data.currency %> <%- priceUnit %><span class="unit-small"><%- priceUnitSmall %></span>
												<% if(data.milesToBeRefunded > 0) { %>
													+ <b data-replace-miles>
														<% if(data.milesToBeRefunded < 1000) { %>
														<%- data.milesToBeRefunded %>
														<% } else { %>
														<%- data.milesToBeRefunded.toLocaleString() %>
														<% } %>
														</b> miles
												<% } %>
											</span>
										<% } else if($('body').hasClass('review-atc-pay') || $('body').hasClass('atc-payment-page')) { %>
											<%
												var priceStyleSmall = data.totalToBePaidCash.toLocaleString(undefined, { minimumFractionDigits: 2 });
												var slicePrice = priceStyleSmall.indexOf(".");
												var priceUnit = priceStyleSmall.slice(0, slicePrice);
												var priceUnitSmall = priceStyleSmall.slice(slicePrice);
											%>
											<span class="unit">
												<%- data.currency %> <%- priceUnit %><span class="unit-small"><%- priceUnitSmall %></span>
												<% if(data.totalToBePaidMiles.toLocaleString() > 0) { %>
													+ <b data-replace-miles>
														<%- data.totalToBePaidMiles %>
														</b> miles
												<% } %>
											</span>
										<% } else if($('.payments-group').hasClass('payments-group__charge')) { %>
											<%
												var priceStyleSmall = data.costPayableByCash.toLocaleString(undefined, { minimumFractionDigits: 2 });
												var slicePrice = priceStyleSmall.indexOf(".");
												var priceUnit = priceStyleSmall.slice(0, slicePrice);
												var priceUnitSmall = priceStyleSmall.slice(slicePrice);
											%>
											<span class="unit payment-sk">
												<%- data.currency %> <%- priceUnit %><span class="unit-small refont-cash" data-miles-refont = <%- data.costPayableByMiles %> data-refont-cash = <%- data.costPayableByCash %>><%- priceUnitSmall %></span>

													<span class="miles-payment-sk hidden <% if(data.costPayableByMiles == 0) { %> miles-no-append <% } %>">
														+ <b data-replace-miles>
															<% if(data.costPayableByMiles < 1000) { %>
															<%- data.costPayableByMiles %>
															<% } else { %>
															<%- data.costPayableByMiles.toLocaleString() %>
															<% } %>
															</b> miles
													 </span>
											</span>
										<% } else { %>
											<%
												var priceStyleSmall = data.costPayableByCash.toLocaleString(undefined, { minimumFractionDigits: 2 });
												var slicePrice = priceStyleSmall.indexOf(".");
												var priceUnit = priceStyleSmall.slice(0, slicePrice);
												var priceUnitSmall = priceStyleSmall.slice(slicePrice);
											%>
											<span class="unit">
												<%- data.currency %> <%- priceUnit %><span class="unit-small"><%- priceUnitSmall %></span>
											</span>
										<% } %>
									</p>
									<span class="fare-notice">Total fare includes discounts, taxes and surcharges</span>
							</div>
							<div class="bsp-flights-cost"><span class="bsp-flights-cost-title"><span class="text-left">FLIGHTS</span><span class="text-right"><%- data.currency %></span></span>
								<ul class="bsp-flights-cost__details">
									<li><span>Fare</span><span class="price"><%- data.fareTotal.toFixed(2) %></span></li>
									<li><span>Airport/Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
									<li><span>Carrier surcharges</span><span class="price">
									<%- data.surchargeTotal.toFixed(2) %> </span></li>
									<% if($('body').hasClass('cib-payment-sk-page')) { %>
										<li class="credit-card-service"><span>Credit card service fee</span><span class="price">
										10.00 </span></li>
									<% } %>
									<% if($('body').hasClass('review-refund-page') || $('body').hasClass('review-atc-pay') || $('body').hasClass('atc-payment-page')) { %>
										<li><span>Rebooking fee</span><span class="price">
									<%- data.rebookingFee.toFixed(2) %> </span></li>
										<li class="sub-total"><span>Subtotal</span><span class="price"><%- data.fareSubTotal.toFixed(2) %></span></li> <br>
										<li class="sub-total"><span>Prevlously paid</span><span class="price"><%- data.currency %> <%- data.oldItinerary.costPaidByCash.toFixed(2) %> + <div data-replace-miles><%- data.oldItinerary.costPaidByMiles.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",") %> miles</div></span></li>
									<% } else { %>
										<li class="sub-total"><span>Subtotal</span><span class="price"><%- data.fareSubTotal.toFixed(2) %></span></li>
									<% } %>
									<% if($('.payments-group').hasClass('payments-group__charge')) { %>
										<% if(data.costPayableByCash !== undefined) { %>
											<li class="sub-total miles-grand-total hidden">
												<span class="total-paid-now">Total to be paid now</span>
												<span class="unit miles-hidden price hidden"></span>
												<span class=" price bsp-flights-cost-title payment-currency-text hidden">
													(Converted from <%- data.currency %> <%- data.costPayableByCash.toLocaleString(undefined, { minimumFractionDigits: 2 }) %>)
												</span>
											</li>
										<% } %>
									<% } %>
								</ul>
							</div>
							<% if($('body').hasClass('cib-add-ons-sk-page')) { %>
								<div class="bsp-flights-cost"><span class="bsp-flights-cost-title"><span class="text-left">Add-ons</span><span class="text-right"><%- data.currency %></span></span>
									<ul class="bsp-flights-cost__details">
										<li>
											<span>Travel Insurance</span>
											<span class="price">
												<%
													var addonsInsurance;
													_.each(data.commonAddons, function(commonInsurance, travelInsurance) {
														if(commonInsurance.type === 'Travel Insurance') {
															addonsInsurance = commonInsurance.amount;
														}
													});
												%>
												<%= addonsInsurance.toFixed(2) %>
											</span>
										</li>
										<li class="sub-total"><span>Subtotal</span><span class="price"><%- data.addonSubTotal.toFixed(2) %></span></li>
									</ul>
								</div>
							<% } %>
							<div class="cta-group">
								<li><a href="#" class="search-link trigger-popup-flights-details" data-trigger-popup=".popup--flights-details"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Cost breakdown by passengers</a>
								</li>
								<li><a href="#" class="search-link trigger-popup-add-ons-baggage" data-trigger-popup=".popup--add-ons-summary"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Full fare condition</a>
								</li>
								<li><a href="#" class="search-link trigger-popup-add-ons-summary" data-trigger-popup=".popup--add-ons-baggage"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Baggage allowance</a>
								</li>
							</div><a href="#" class="link-4 more-detail" data-more-details="true">More details<em class="ico-point-d"><span class="ui-helper-hidden-accessible"></span></em></a><a href="#" class="link-4 less-detail" data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a>
						</div>
					</div>
					<div class="bsp-booking-summary--group none-line-flight <% if(data.flight.length >= 2) { %> multi-city <% } else { %> one-way <% } %> hidden-mb-small">
							<% if(data.flight.length >= 2) { %>
								<% _.each(data.flight, function(itemFly, idx){ %>
									<div class="bsp-flights__info--group <% if(idx % 2 == 0) {%> position-even <% } %> <% if(idx >= 2) {%> hidden <% } %> flight-result-leg-wrap all-transition">
										<div class="bsp-flights__info--inner"><span class="flight-date-title">
										<% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
											<% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
										<% } %>
										<% if(data.flight.length > 2) { %>
											Flight <%- idx + 1 %>
										<% } %>
										</span>
											<div class="flight-station-info">
												<div class="station-stop">
													<span class="station-stop-detail">
														<em class="ico-airplane-2"></em>
														<span class="time"></span>
													</span>
												</div>
												<div class="flights-station__info--detail"><span class="time">
														<%- itemFly.flightSegments[0].deparure.date %></span><span class="hour"><%- itemFly.originAirportCode %> <%- itemFly.flightSegments[0].deparure.time %></span><span class="country-name"><%- itemFly.origin %></span>
												</div>
												<div class="flights-station__info--detail return-flight"><span class="time">
														<%- itemFly.flightSegments[0].arrival.date %></span><span class="hour"><%- itemFly.destinationAirportCode %> <%- itemFly.flightSegments[0].arrival.time %></span><span class="country-name"><%- itemFly.destination %></span>
												</div>
											</div>
										</div>
									</div>
									<% }) %>
								<% } else { %>
										<% if(data.flight.length < 2) { %>
											<div class="bsp-flights__info--group one-way flight-result-leg-wrap all-transition">
											<div class="bsp-flights__info--inner"><span class="flight-date-title">One way</span>
												<div class="flight-station-info">
													<div class="station-stop">
														<span class="station-stop-detail">
															<em class="ico-airplane-2"></em>
															<span class="time"></span>
														</span>
													</div>
													<div class="flights-station__info--detail"><span class="time">
															<%- data.flight[0].flightSegments[0].deparure.date %></span><span class="hour"><%- data.flight[0].originAirportCode %> <%- data.flight[0].flightSegments[0].deparure.time %></span><span class="country-name"><%- data.flight[0].origin %></span>
													</div>
													<div class="flights-station__info--detail return-flight"><span class="time">
															<%- data.flight[0].flightSegments[0].arrival.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- data.flight[0].destinationAirportCode %> <%- data.flight[0].flightSegments[0].arrival.time %></span><span class="country-name"><%- data.flight[0].destination %></span>
													</div>
												</div>
											</div>
										</div>
										<% } %>
								<% } %>
						<% if(data.flight.length > 2) { %>
							<% var numberLength = data.flight.length - 2 %>
							<div class="plus-more-detail"><a href="#" class="link-4 hidden-mb-small"><span class="text">+ <%- numberLength %> more flights</span></a>
							</div>
						<% } %>
					</div>
					<div class="bsp-booking-summary__content-detail ">
						<div class="bsp-booking-summary--group <% if(data.flight.length >= 2) { %> multi-city <% } %>">
							<% _.each(data.flight, function(itemFly, idx){ %>
								<div class="bsp-flights__info--group <% if(idx % 2 == 0 && data.flight.length >= 2) {%> position-even <% } %> flight-result-leg-wrap all-transition">
									<div class="bsp-flights__info--inner"><span class="flight-date-title">
									<% if(data.flight.length < 2) { %>
										One way
									<% } %>
									<% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
										<% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
									<% } %>
									<% if(data.flight.length > 2) { %>
										Flight <%- idx + 1 %>
									<% } %>
										</span>
										<% _.each(itemFly.flightSegments, function(itemSegments, idx){ %>
											<div class="flight-station-info">
												<div class="station-stop">
													<span class="station-stop-detail">
														<em class="ico-airplane-2"></em>
														<span class="time"></span>
													</span>
												</div>
												<div class="flights-station__info--detail"><span class="time">
														<%- itemSegments.deparure.date %></span><span class="hour"><%- itemSegments.deparure.airportCode %> <%- itemSegments.deparure.time %></span><span class="country-name"><%- itemSegments.deparure.cityName %></span><span class="date"><%- itemSegments.deparure.airportName %></span><span class="terminal"> <% if(itemSegments.deparure.terminal !== "" && itemSegments.deparure.terminal) { %> Terminal <% } %> <%- itemSegments.deparure.terminal %></span>
												</div>
												<div class="flights-station__info--detail return-flight"><span class="time">
														<%- itemSegments.arrival.date %></span><span class="hour"><%- itemSegments.arrival.airportCode %> <%- itemSegments.arrival.time %></span><span class="country-name"><%- itemSegments.arrival.cityName %></span><span class="date"><%- itemSegments.arrival.airportName %></span><span class="terminal"> <% if(itemSegments.arrival.terminal !== "" && itemSegments.arrival.terminal) { %> Terminal <% } %> <%- itemSegments.arrival.terminal %></span>
												</div>
											</div>
											<p class="flying-details"><strong><%- itemSegments.carrierName %></strong><span class="hidden-tb show-mb-inline">·</span> <%- itemSegments.carrierCode %> <%- itemSegments.flightNumber %> · <%- itemSegments.airCraftType %><span class="economy"><%- itemSegments.cabinClassDesc %></span></p>
											<% if(itemSegments.layoverTime) { %>
												<div class="flights__info border"><span>Layover time: <%- itemSegments.layoverTime %></span>
												</div>
											<% } %>
										<% }) %>
										<div class="flights__info border"><span>Total travel time: <%- itemFly.totalTravelTime %></span></div>
									</div>
								</div>
							<% }) %>
						</div>
						<div class="edit-search-mb-tb">
						<a href="#" class="link-4 less-detail hidden-tb-dt" data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<% } %>
