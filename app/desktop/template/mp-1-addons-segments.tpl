<%
if(data) {
    // console.log(data);
    var getPackByancillarySubType = function(subType) {
      for(var i = 0; i < data.bundle.length; i ++) {
        var find = _.findWhere(data.bundle[i].ancillaryList, {"ancillarySubType": subType});
        if (find) {
          return data.bundle[i];
        }
      }
    };

    var getXBagAlacarte = function() {
      for(var key in data.alacarte) {
        return data.alacarte[key];
      }
    };

    var getAllPacksOnPaxElementList = function(packPax) {
      var packs = [];
      _.each(packPax.elementList, function(list) {
        packs = packs.concat(list.packs);
      });

      return packs;
    };

    var filterPackByBundleType = function(packs, bundles) {
      return _.filter(packs, function(pack) {
        var packData = bundles[pack.packId];
        if (packData) {
          return pack;
        }
      });
    };

    var extraLegRoomSeats = {};
    var forwardZoneSeats = {};
    var xBagAlacarte = {};

    _.each(data.bundle, function(bundleData) {
        if (_.findWhere(bundleData.ancillaryList, {"ancillarySubType": "EXTRA LEGROOM SEAT"})) {
          extraLegRoomSeats[bundleData.packId] = bundleData;
        } else if (_.findWhere(bundleData.ancillaryList, {"ancillarySubType": "FRONT ZONE SEAT"})) {
          forwardZoneSeats[bundleData.packId] = bundleData;
        }
    });

    _.each(data.alacarte, function(alacarteData) {
      xBagAlacarte[alacarteData.packId] = alacarteData;
    });
    
    var extraLegRoomPaxAssociated = [];
    var extraLegRoomSegment = [];
    var forwardZoneSeatPaxAssociated = [];
    var forwardZoneSeatSegment = [];
    var xBagAlacartePaxAssociated = [];
    var xBagAlacartePaxSegment = [];

    var addPackDetails = function(segmentPack, bundles) {
      return _.map(segmentPack, function(pack) {
        if (bundles[pack.packId].ancillaryList) {
          pack.ancillaryList = bundles[pack.packId].ancillaryList;
        }
        if (bundles[pack.packId].ancillaryType) {
          pack.ancillaryUnit = {
            numberOfUnits: bundles[pack.packId].numberOfUnits,
            unitOfMeasurement: bundles[pack.packId].unitOfMeasurement
          }
        }
        return pack;
      });
    };

    //filter Pax Associations to the bundle included
    var filterPaxToBundle = function(bundles) {
      var paxAssoc = JSON.parse(JSON.stringify(data.packPaxAssociations));
      return _.filter(paxAssoc, function(pax) {
        var packs = getAllPacksOnPaxElementList(pax);
        packs = filterPackByBundleType(packs, bundles);
        if (packs.length) {
          pax.elementList = _.map(pax.elementList, function(list) {
            var segmentPack = filterPackByBundleType(list.packs, bundles);
            list.packs = addPackDetails(segmentPack, bundles);
            return list;
          });
          return pax;
        }

        if (!pax.elementList.length) {
          return pax;
        }
      });
    };

    var getSegments = function(packPaxAssociations) {
      var segmentValue = [];
      if (!packPaxAssociations.length) return segmentValue;

      _.each(packPaxAssociations[0].elementList, function(list) {
        segmentValue[list.elementId] = list;
      });

      return segmentValue;
    };

    extraLegRoomPaxAssociated = filterPaxToBundle(extraLegRoomSeats);
    extraLegRoomSegment = getSegments(extraLegRoomPaxAssociated);
    forwardZoneSeatPaxAssociated = filterPaxToBundle(forwardZoneSeats);
    forwardZoneSeatSegment = getSegments(forwardZoneSeatPaxAssociated);
    xBagAlacartePaxAssociated = filterPaxToBundle(xBagAlacarte);
    xBagAlacartePaxSegment = getSegments(xBagAlacartePaxAssociated);

    var getLowestPriceHighestDiscount = function(bundles) {
      var lowestPrice = 0;
      var highestDiscount = 0;
      _.each(data.packPaxAssociations, function(pax) {
        var packs = getAllPacksOnPaxElementList(pax);
        packs = filterPackByBundleType(packs, bundles);
        _.each(packs, function(pack) {
          var price = parseFloat(pack.price);
          if (!lowestPrice) {
            lowestPrice = price;
          } else {
            lowestPrice = (price < lowestPrice) ? price : lowestPrice;
          }

          if (!pack.percentageSaved) return;

          var percentage = parseInt(pack.percentageSaved);
          if (!highestDiscount) {
            highestDiscount = percentage;
          } else {
            highestDiscount = (percentage > highestDiscount) ? percentage : highestDiscount;
          }
        });
      });
      
      return {
        lowestPrice: lowestPrice,
        highestDiscount: highestDiscount
      }
    };

    var getCurrency = function () {
      return data.packPaxAssociations[0].currency;
    };

    var findOnPacks = function (packs, findBy) {
      var regExp = new RegExp(findBy, "i");
      var found;
      _.each(packs, function(eachPack) {
        if (regExp.exec(eachPack.packId)) {
          found = eachPack;
        }
      });

      return found;
    };
%>

<div data-accordion-wrapper="1" data-bundle>
    <div data-accordion-wrapper-content="1">
      <div data-accordion="1">
        <div class="addons-your-flight">
          <% if (data.bundle) { %>
            <% var firstBundle = getPackByancillarySubType("EXTRA LEGROOM SEAT"); %>

            <% if (extraLegRoomPaxAssociated.length > 0) { %>
              <div data-custom-accordion="true" data-custom-accordion-offset="50" class="your-flight-item block-2">
                <div class="addons-your-flight-content check-preferred">
                  <div data-accordion-wrapper="2">
                    <div class="description">
                      <figure>
                        <% if(firstBundle.packImageUrl !== "") { %>
                          <img src="<%- firstBundle.packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } else { %>
                          <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } %>
                      </figure>
                      <div class="content">
                        <div class="detail-content">
                          <% if(firstBundle.isRecommended.toLowerCase() == "true") { %>
                            <span class="selected-orange">Recommended Deal</span>
                          <% } %>
                          <h4 class="title-5--blue">
                              Extra Legroom Seat and additional baggage deal
                          </h4>
                          <% if(firstBundle.packDescription) { %>
                            <p><%= firstBundle.packDescription %></p>
                          <% } %>
                          <% if (firstBundle.isMixMilesCashAllowed) { %>
                          <div class="mix-miles-wrapper"><span class="btn-small btn-mix-miles">mix miles and cash</span></div>
                          <% } %>
                        </div>
                        <div class="from-price-flight"><span class="note">From</span>
                          <span class="sgd-price">
                            <%
                              var currency = getCurrency();
                              var discounted = getLowestPriceHighestDiscount(extraLegRoomSeats);
                            %>
                            <%- currency.code %> <%- discounted.lowestPrice.toFixed(currency.precision) %>
                          </span>
                          <span class="miles">Per passenger per flight</span>
                          <% if(discounted.highestDiscount > 0) { %>
                          <span class="selected-orange">save up to <%- discounted.highestDiscount %>% off</span>
                          <% } %>
                          <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                            <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="View" class="accordion_button_control btn-8" data-select-item="false"/>
                            <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Close" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                            <a href="#" class="icon-down chevron-ico" tabindex="-1"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
                      <div class="block-flight-details">
                        <div class="title-popup-mb">
                          <span class="sub-heading-2--blue">
                            <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                              Extra Legroom Seat and additional baggage deal
                            <% } %>
                          </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                        </div>
                        <% _.each(extraLegRoomSegment, function(segment, segmentIdx) { %>
                          <div class="block-flight-details--inner">
                            <div class="addons-landing--inner">
                              <div data-accordion="2" class="addons-landing-content bundle-accordion">
                                <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                  <span class="title-5--blue"><%- segment.segmentDescription %></span>
                                  <span class="bundle-selected hidden"><span></span></span><em class="ico-point-d"></em>
                                </a>
                                <input type="hidden" class="input__origin" value="<%= segment.originAirportCode%>"/>
                                <input type="hidden" class="input__destination" value="<%= segment.destinationAirportCode%>" />
                                <input type="hidden" class="add_ons_type" value="ELS-<%- segmentIdx%>" />
                                <div data-accordion-content="2" class="accordion__content" style="display: none;">
                                  <% _.each(extraLegRoomPaxAssociated, function(paxData) { %>
                                  <div class="bundle-flight-block data-pss-xbag">
                                    <span class="title-5--blue"><%- paxData.paxName %></span>
                                    <%if (!paxData.elementList.length) {%>
                                      <br>
                                      <span>This passenger enjoys complimentary Extra Legroom Seat selection.</span>
                                    <% } else { %>
                                    <div class="bundle-flight--inner">
                                      <% _.each(paxData.elementList[segment.elementId].packs, function(packsData, packsIdx) { 
                                        var numBaggage = "";
                                        %>
                                        <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                            </div>
                                            <div class="bundle-flight--inner">
                                        <% } %>
                                          <div class="bundle-flight-item">
                                            <div class="bundle-flight-item--inner"><span class="title-5--blue">Deal <%- packsIdx + 1 %></span>
                                              <ul class="bundle-baggage two-bundle-baggage">
                                                <li><em class="ico-1-preferred"></em>Extra Legroom Seat</li>
                                                <li>
                                                  <% _.each(packsData.ancillaryList, function(ancillariesData) {
                                                    numBaggage = ancillariesData.numberOfUnits + ancillariesData.unitOfMeasurement;
                                                    if (ancillariesData.ancillaryType == "XBAG") { %>
                                                      <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                    <% } else if (ancillariesData.ancillaryType == "PIEC") { %>
                                                      <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                    <% }
                                                  }); %>
                                                </li>
                                              </ul>
                                              <div class="select-price"><span class="sgd-price"><%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %></span>
                                                <% if (packsData.percentageSaved) {%>
                                                <div class="discount-wrapper">
                                                  <span class="selected-orange"><%= packsData.percentageSaved %>% OFF</span>
                                                </div>
                                                <% } %>
                                                <input type="button" name="select-bundle-<%- segment.elementId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false"  aria-label="Select Deal <%- packsIdx + 1 %> <%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                                <input type="button" name="selected-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false" aria-label="De-select Deal <%- packsIdx + 1 %> <%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                              </div>
                                            </div>
                                          </div>
                                        <% }); %>
                                    </div>
                                    <% } %>
                                  </div>
                                  <% }); %>
                                </div>
                              </div>
                            </div>
                          </div>
                          <% }); %>
                        <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <% } %>

            <% var secondBundle = getPackByancillarySubType("FRONT ZONE SEAT"); %>

            <% if (forwardZoneSeatPaxAssociated.length > 0) { %>
              <div data-custom-accordion="true" data-custom-accordion-offset="50" class="your-flight-item block-2">
                <div class="addons-your-flight-content check-preferred">
                  <div data-accordion-wrapper="2">
                    <div class="description">
                      <figure>
                        <% if(secondBundle.packImageUrl !== "") { %>
                          <img src="<%- secondBundle.packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } else { %>
                          <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } %>
                      </figure>
                      <div class="content">
                        <div class="detail-content">
                          <% if(secondBundle.isRecommended.toLowerCase() == "true") { %>
                            <span class="selected-orange">Recommended Deal</span>
                          <% } %>
                          <h4 class="title-5--blue">
                              Forward Zone Seat and additional baggage deal
                          </h4>
                          <% if(secondBundle.packDescription) { %>
                            <p><%= secondBundle.packDescription %></p>
                          <% } %>
                          <% if (secondBundle.isMixMilesCashAllowed) { %>
                          <div class="mix-miles-wrapper"><span class="btn-small btn-mix-miles">mix miles and cash</span></div>
                          <% } %>
                        </div>
                        <div class="from-price-flight"><span class="note">From</span>
                          <span class="sgd-price">
                            <%
                              var currency = getCurrency();
                              var discounted = getLowestPriceHighestDiscount(forwardZoneSeats); 
                            %>
                            <%- currency.code %> <%- discounted.lowestPrice.toFixed(currency.precision) %>
                          </span>
                          <span class="miles">Per passenger per flight</span>
                          <% if(discounted.highestDiscount > 0) { %>
                          <span class="selected-orange">save up to <%- discounted.highestDiscount %>% off</span>
                          <% } %>
                          <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                            <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="View" class="accordion_button_control btn-8" data-select-item="false"/>
                            <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Close" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                            <a href="#" class="icon-down chevron-ico" tabindex="-1"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
                      <div class="block-flight-details">
                        <div class="title-popup-mb">
                          <span class="sub-heading-2--blue">
                            <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                              Extra Legroom Seat and additional baggage deal
                            <% } %>
                          </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                        </div>
                        <% _.each(forwardZoneSeatSegment, function(segment, segmentIdx) { %>
                          <div class="block-flight-details--inner">
                            <div class="addons-landing--inner">
                              <div data-accordion="2" class="addons-landing-content bundle-accordion">
                                <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                  <span class="title-5--blue"><%- segment.segmentDescription %></span>
                                  <span class="bundle-selected hidden"><span></span></span><em class="ico-point-d"></em>
                                </a>
                                <input type="hidden" class="input__origin" value="<%= segment.originAirportCode%>"/>
                                <input type="hidden" class="input__destination" value="<%= segment.destinationAirportCode%>" />
                                <input type="hidden" class="add_ons_type" value="FS-<%- segmentIdx%>" />
                                <div data-accordion-content="2" class="accordion__content" style="display: none;">
                                  <% _.each(forwardZoneSeatPaxAssociated, function(paxData) { %>
                                  <div class="bundle-flight-block data-pss-xbag">
                                    <span class="title-5--blue" data-paxname="<%- paxData.paxName %>"><%- paxData.paxName %></span>
                                    <%if (!paxData.elementList.length) {%>
                                      <br>
                                      <span>This passenger enjoys complimentary Forward Seat selection.</span>
                                    <% } else { %>
                                    <div class="bundle-flight--inner">
                                      <% _.each(paxData.elementList[segment.elementId].packs, function(packsData, packsIdx) { 
                                        var numBaggage = "";
                                        %>
                                        <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                            </div>
                                            <div class="bundle-flight--inner">
                                        <% } %>
                                          <div class="bundle-flight-item">
                                            <div class="bundle-flight-item--inner"><span class="title-5--blue">Deal <%- packsIdx + 1 %></span>
                                              <ul class="bundle-baggage two-bundle-baggage">
                                                <li><em class="ico-change-seat"></em>Forward Seat</li>
                                                <li>
                                                  <% _.each(packsData.ancillaryList, function(ancillariesData) {
                                                    numBaggage = ancillariesData.numberOfUnits + ancillariesData.unitOfMeasurement;
                                                    if (ancillariesData.ancillaryType == "XBAG") { %>
                                                      <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                    <% } else if (ancillariesData.ancillaryType == "PIEC") { %>
                                                      <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                    <% }
                                                  }); %>
                                                </li>
                                              </ul>
                                              <div class="select-price"><span class="sgd-price"><%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %></span>
                                                <% if (packsData.percentageSaved) {%>
                                                <div class="discount-wrapper">
                                                  <span class="selected-orange"><%= packsData.percentageSaved %>% OFF</span>
                                                </div>
                                                <% } %>
                                                <input type="button" name="select-bundle-<%- segment.elementId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false"  aria-label="Select Deal <%- packsIdx + 1 %> <%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                                <input type="button" name="selected-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- segment.elementId  %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false" aria-label="De-select Deal <%- packsIdx + 1 %> <%- paxData.currency.code %> <%- Number(packsData.price).toFixed(paxData.currency.precision) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                              </div>
                                            </div>
                                          </div>
                                        <% }); %>
                                    </div>
                                    <% } %>
                                  </div>
                                  <% }); %>
                                </div>
                              </div>
                            </div>
                          </div>
                          <% }); %>
                        <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <% } %>
          <% } %>

          <% if (data.alacarte && !data.isAddOnsPiece) { %>
            <% var thirdBundle = getXBagAlacarte(); %>

            <% if (xBagAlacartePaxAssociated.length > 0 && data.alacarte.length > 0) { %>
              <div data-custom-accordion="true" data-custom-accordion-offset="50" class="your-flight-item block-2">
                <div class="addons-your-flight-content">
                  <div data-accordion-wrapper="2">
                    <div class="description">
                      <figure>
                          <% if(thirdBundle.packImageUrl) { %>
                            <img src="<%- thirdBundle.packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                          <% } else { %>
                            <img src="images/baggage-add-ons.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                          <% } %>
                      </figure>
                      <div class="content">
                        <div class="detail-content">
                          <h4 class="title-5--blue">Additional Baggage</h4>
                          <% if(thirdBundle.packDescription) { %>
                            <p><%= thirdBundle.packDescription %></p>
                          <% } %>
                          <% if (thirdBundle.isMixMilesCashAllowed) { %>
                            <div class="mix-miles-wrapper"><span class="btn-small btn-mix-miles">mix miles and cash</span></div>
                          <% } %>
                        </div>
                        <div class="from-price-flight"><span class="note">From</span>
                          <span class="sgd-price">
                            <%
                              var currency = getCurrency();
                              var discounted = getLowestPriceHighestDiscount(xBagAlacarte); 
                            %>
                            <%- currency.code %> <%- discounted.lowestPrice.toFixed(currency.precision) %>
                          </span>
                          <span class="miles">Per passenger per flight</span>
                          <% if(discounted.highestDiscount > 0) { %>
                            <span class="selected-orange">save up to <%- discounted.highestDiscount %>% off</span>
                          <% } %>
                          <div t="50" data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                            <input type="button" name="select-add-heavy-btn" id="select-add-heavy-btn" value="View" class="accordion_button_control btn-8" data-select-item="false"/>
                            <input type="button" name="selected-add-heavy-btn" id="selected-add-heavy-btn" value="Close" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                            <a href="#" class="icon-down chevron-ico" tabindex="-1"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true">
                        <div class="block-flight-details">
                          <div class="title-popup-mb"><span class="sub-heading-2--blue">Additional baggage</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                          </div>
                          <% _.each(xBagAlacartePaxSegment, function(segment, itemIdx) { %>
                              <div class="block-flight-details--inner xbag-accordion">
                                <div class="addons-landing--inner">
                                  <div data-accordion="2" class="addons-landing-content"><a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                    <span class="title-5--blue"><%- segment.segmentDescription %></span>
                                    <span class="bundle-selected segment-<%- itemIdx%> hidden additional-baggages"><span></span>Additional baggage selected</span><em class="ico-point-d"></em></a>
                                    <input type="hidden" class="input__origin" value="<%= segment.originAirportCode%>"/>
                                    <input type="hidden" class="input__destination" value="<%= segment.destinationAirportCode%>" />
                                    <input type="hidden" class="add_ons_type" value="XBAG-<%- itemIdx%>" />
                                    <div data-accordion-content="2" class="accordion__content" style="display: none;">
                                      <% _.each(xBagAlacartePaxAssociated, function(paxData, paxIdx) {
                                        var packsData = paxData.elementList[segment.elementId];%>
                                          <div class="preferred-flight-block preferred-flight-block-changes">
                                            <div class="preferred-flight-item weight-flight-item">
                                              <div class="title-baggage-item">
                                                <span class="title-5--blue"><%- paxData.paxName %></span>
                                                <span class="free-bagage">Your free baggage:
                                                  <% if (packsData) { %>
                                                    <strong><%- packsData.baggageAllowance%><%- packsData.packs[0].ancillaryUnit.unitOfMeasurement%></strong>
                                                  <% } else { %>
                                                    <strong>30kg</strong>
                                                  <% } %>
                                                </span>
                                              </div>
                                              <div class="weight-baggage bundle-flight-block">
                                                  <span class="title-5--blue hidden"><%- paxData.paxName%></span>
                                                  <div class="wrap-baggage"><span class="text-item"><em class="ico-business-1"></em><strong>Add weight</strong></span>
                                                    <div class="full-flight">
                                                        <% if (data.isDesktop == "true") { %>
                                                          <div data-customselect="true" class="custom-select custom-select--2">
                                                            <label for="add-weight-<%- itemIdx %>-<%- paxIdx + 1 %>" class="select__label">
                                                              <span class="ui-helper-hidden-accessible">Label</span>
                                                            </label>
                                                        <% } else { %>
                                                          <div class="custom-select custom-select--2">
                                                        <% } %>
                                                            <span class="select__text">5kg ($15.00)</span>
                                                            <span class="ico-dropdown"></span>
                                                            <select class="add-weight-select" id="<%- itemIdx %>-<%- paxIdx + 1 %>" name="add-weight-<%- itemIdx %>-<%- paxIdx + 1 %>" data-dropdpown-selected="true">
                                                              <option value="0">No Additional Baggage</option>
                                                              <option value="1">5kg ($15.00)</option>
                                                              <option value="2">10kg ($30.00)</option>
                                                              <option value="3">15kg ($45.00)</option>
                                                              <option value="4">20kg ($60.00)</option>
                                                              <option value="5">20kg ($75.00)</option>
                                                              <option value="6">25kg ($90.00)</option>
                                                              <option value="7">30kg ($105.00)</option>
                                                            </select>
                                                          </div>
                                                    </div>
                                                    <% 
                                                      if (packsData) {
                                                        if (packsData.packs[0].percentageSaved) {
                                                    %>
                                                      <div class="discount-wrapper-xbag">
                                                        <span class="selected-orange"><%- packsData.packs[0].percentageSaved%>% off</span>
                                                      </div>
                                                    <% 
                                                        }
                                                      } 
                                                    %>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="preferred-flight--info hidden">
                                              <div class="review-baggage--item">
                                                <span class="review-baggage-text" data-unit="80"><em class="ico-business-1"></em><span><strong>Add 9kg to free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 80.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span></span>
                                                <ul data-plus-or-minus-number="2" class="add-baggage-list">
                                                  <li>
                                                    <button type="button" class="btn-minus">-</button>
                                                  </li>
                                                  <li>
                                                    <input type="tel" name="number-baggage" value="2" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-1" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                  </li>
                                                  <li>
                                                    <button type="button" class="btn-plus">+</button>
                                                  </li>
                                                </ul>
                                                <span class="sgd-price">SGD <span>160.00</span></span>
                                              </div>
                                              <div class="review-baggage--item">
                                                <span class="review-baggage-text" data-unit="60"><em class="ico-business-1"></em><span><strong>Oversized free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 60.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small"  data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                              </span>
                                                <ul data-plus-or-minus-number="2" class="add-baggage-list">
                                                  <li>
                                                    <button type="button" class="btn-minus">-</button>
                                                  </li>
                                                  <li>
                                                    <input type="tel" name="number-baggage" value="" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-2" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                  </li>
                                                  <li>
                                                    <button type="button" class="btn-plus">+</button>
                                                  </li>
                                                </ul>
                                                <span class="sgd-price" style="display:none;">SGD <span>0.00</span></span>
                                              </div>
                                              <div class="review-baggage--item">
                                                <span class="review-baggage-text" data-unit="80"><em class="ico-business-1"></em><span><strong>Add extra bag, 32kg <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 80.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                                </span>
                                                <ul data-plus-or-minus-number="2" class="add-baggage-list">
                                                  <li>
                                                    <button type="button" class="btn-minus">-</button>
                                                  </li>
                                                  <li>
                                                    <input type="tel" name="number-baggage" value="" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-3" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                  </li>
                                                  <li>
                                                    <button type="button" class="btn-plus">+</button>
                                                  </li>
                                                </ul>
                                                <span class="sgd-price" style="display:none;">SGD <span>0.00</span></span>
                                              </div>
                                            </div>
                                        <% }); %>
                                        <span class="bundle-disclaimer"><i>Original price per kg: SGD 50. Displayed prices are rounded to the nearest dollar and may vary slightly from the final price shown at the package page.</i></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <%
                          }); %>
                          <input type="button" name="confirm-baggage-piece-btn" id="confirm-baggage-piece-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                        </div>
                        </div>
                      </div>
                </div>
              </div>
            <% } %>
          <% } %>

          <%if (data.alacarte && data.isAddOnsPiece) {%>
            <% var thirdBundle = getXBagAlacarte(); %>

            <% if (xBagAlacartePaxAssociated.length > 0 && data.alacarte.length > 0) { %>
              <div data-custom-accordion="true" data-custom-accordion-offset="50" class="your-flight-item block-2">
                <div class="addons-your-flight-content">
                  <div data-accordion-wrapper="2">
                    <div class="description">
                      <figure>
                          <% if(thirdBundle.packImageUrl) { %>
                            <img src="<%- thirdBundle.packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                          <% } else { %>
                            <img src="images/baggage-add-ons.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                          <% } %>
                      </figure>
                      <div class="content">
                        <div class="detail-content">
                          <h4 class="title-5--blue">Additional Baggage</h4>
                          <% if(thirdBundle.packDescription) { %>
                            <p><%= thirdBundle.packDescription %></p>
                          <% } %>
                          <% if (thirdBundle.isMixMilesCashAllowed) { %>
                            <div class="mix-miles-wrapper"><span class="btn-small btn-mix-miles">mix miles and cash</span></div>
                          <% } %>
                        </div>
                        <div class="from-price-flight"><span class="note">From</span>
                          <span class="sgd-price">
                            <%
                              var currency = getCurrency();
                              var discounted = getLowestPriceHighestDiscount(xBagAlacarte); 
                            %>
                            <%- currency.code %> <%- discounted.lowestPrice.toFixed(currency.precision) %>
                          </span>
                          <span class="miles">Per passenger per flight</span>
                          <% if(discounted.highestDiscount > 0) { %>
                            <span class="selected-orange">save up to <%- discounted.highestDiscount %>% off</span>
                          <% } %>
                          <div t="50" data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                            <input type="button" name="select-add-heavy-btn" id="select-add-heavy-btn" value="View" class="accordion_button_control btn-8" data-select-item="false"/>
                            <input type="button" name="selected-add-heavy-btn" id="selected-add-heavy-btn" value="Close" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                            <a href="#" class="icon-down chevron-ico" tabindex="-1"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true">
                        <div class="block-flight-details">
                          <div class="title-popup-mb"><span class="sub-heading-2--blue">Additional baggage</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                          </div>
                          <% _.each(xBagAlacartePaxSegment, function(segment, itemIdx) { %>
                              <div class="block-flight-details--inner xbag-accordion">
                                <div class="addons-landing--inner">
                                  <div data-accordion="2" class="addons-landing-content"><a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                    <span class="title-5--blue"><%- segment.segmentDescription %></span>
                                    <span class="bundle-selected segment-<%- itemIdx%> hidden additional-baggages"><span></span>Additional baggage selected</span><em class="ico-point-d"></em></a>
                                    <input type="hidden" class="input__origin" value="<%= segment.originAirportCode%>"/>
                                    <input type="hidden" class="input__destination" value="<%= segment.destinationAirportCode%>" />
                                    <input type="hidden" class="add_ons_type" value="XBAG-<%- itemIdx%>" />
                                    <div data-accordion-content="2" class="accordion__content" style="display: none;">
                                      <% _.each(xBagAlacartePaxAssociated, function(paxData, paxIdx) {
                                        var packsData = paxData.elementList[segment.elementId];
                                        var packPrice = findOnPacks(packsData.packs, "PIEC");
                                      %>
                                          <div data-parent-accordion="1" class="preferred-flight-block preferred-flight-block-changes">
                                            <div class="title-baggage-item">
                                                <div class="content-baggage content-baggage-title">
                                                  <span class="title-5--blue"><%- paxData.paxName %></span>
                                                  <span class="free-bagage">
                                                    <span> Your free baggage: </span>
                                                      <% if (packsData) { %>
                                                      <strong><%- packsData.baggageAllowance%>  bags  </strong>
                                                       (<%- packsData.weightAllowed%>kg each) 
                                                      <% } %>
                                                  </span>
                                                </div>
                                                <div class="content-baggage content-baggage-body pull-right">
                                                    <div class="from-price-flight">
                                                      <div class="from--label">
                                                        <span class="note">From</span>
                                                      </div>
                                                      <div class="price--label">
                                                        <span class="sgd-price">
                                                          <%- currency.code %> <%- parseInt(packPrice.price).toFixed(currency.precision) %>
                                                        </span>
                                                        <%if (packPrice.percentageSaved) {%>
                                                        <span class="selected-orange"><%- packPrice.percentageSaved %>% off</span>
                                                        <% } %>
                                                      </div>
                                                      <div data-child-accordion-click="1" class="button-group-3">
                                                          <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="Select" class="btn-8 btn-bag-select" data-select-item="false"/>
                                                      </div>
                                                    </div>
                                                  </div>
                                            </div>
                                            <div data-child-accordion-content="1" class="preferred-flight--info">
                                                <% 
                                                  var heavyPack = findOnPacks(packsData.packs, "HEAV");
                                                  var bulkPack = findOnPacks(packsData.packs, "BULK");
                                                  var piecePack = findOnPacks(packsData.packs, "PIEC");
                                                %>
                                                <% if (heavyPack) { %>
                                                <div class="review-baggage--item">
                                                    <span class="review-baggage-text"><em class="ico-business-1"></em><span><strong>Add <%- heavyPack.ancillaryUnit.numberOfUnits %><%- heavyPack.ancillaryUnit.unitOfMeasurement %> to free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (<%- currency.code %> <%- parseFloat(heavyPack.price).toFixed(currency.precision) %> each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span></span>
                                                    <ul data-plus-or-minus-number="2" data-unit="<%- heavyPack.price %>" data-packid="<%- heavyPack.packId %>-heavyPack" data-currency="<%- currency.code %>" data-paxname="<%- paxData.paxName %>" data-bagtype="<%- heavyPack.ancillaryUnit.numberOfUnits %><%- heavyPack.ancillaryUnit.unitOfMeasurement %>" data-od-segments="<%- segment.originAirportCode %> - <%- segment.destinationAirportCode %>" class="add-baggage-list">
                                                    <li>
                                                        <button type="button" data-stepperbtn="-" class="btn-minus btn-stepper">-</button>
                                                    </li>
                                                    <li>
                                                        <input type="tel" name="number-baggage" value="0" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-1" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                    </li>
                                                    <li>
                                                        <button type="button" data-stepperbtn="+" class="btn-plus btn-stepper">+</button>
                                                    </li>
                                                    </ul>
                                                    <span class="sgd-price hidden"><%- currency.code %> <span>0.00</span></span>
                                                </div>
                                                <% } %>
                                                <% if (bulkPack) { %>
                                                <div class="review-baggage--item">
                                                    <span class="review-baggage-text"><em class="ico-business-1"></em><span><strong>Oversized free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (<%- currency.code %> <%- parseFloat(bulkPack.price).toFixed(currency.precision) %> each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small"  data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                                </span>
                                                    <ul data-plus-or-minus-number="2" data-unit="<%- bulkPack.price %>" data-packid="<%- heavyPack.packId %>-bulkPack" data-currency="<%- currency.code %>" data-paxname="<%- paxData.paxName %>" data-bagtype="Oversized bag" data-od-segments="<%- segment.originAirportCode %> - <%- segment.destinationAirportCode %>" class="add-baggage-list">
                                                    <li>
                                                        <button type="button" data-stepperbtn="-" class="btn-minus btn-stepper">-</button>
                                                    </li>
                                                    <li>
                                                        <input type="tel" name="number-baggage" value="0" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-2" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                    </li>
                                                    <li>
                                                        <button type="button" data-stepperbtn="+" class="btn-plus btn-stepper">+</button>
                                                    </li>
                                                    </ul>
                                                    <span class="sgd-price hidden"><%- currency.code %> <span>0.00</span></span>
                                                </div>
                                                <% } %>
                                                <% if (piecePack) { %>
                                                <div class="review-baggage--item">
                                                    <span class="review-baggage-text"><em class="ico-business-1"></em><span><strong>Add extra bag, <%- piecePack.ancillaryUnit.numberOfUnits %><%- piecePack.ancillaryUnit.unitOfMeasurement %> <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (<%- currency.code %> <%- parseFloat(piecePack.price).toFixed(currency.precision) %> each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                                    </span>
                                                    <ul data-plus-or-minus-number="2" data-unit="<%- piecePack.price %>" data-packid="<%- heavyPack.packId %>-piecePack" data-currency="<%- currency.code %>" data-paxname="<%- paxData.paxName %>" data-bagtype="<%- piecePack.ancillaryUnit.numberOfUnits %><%- piecePack.ancillaryUnit.unitOfMeasurement %>" data-od-segments="<%- segment.originAirportCode %> - <%- segment.destinationAirportCode %>" class="add-baggage-list">
                                                    <li>
                                                        <button type="button" data-stepperbtn="-" class="btn-minus btn-stepper">-</button>
                                                    </li>
                                                    <li>
                                                        <input type="tel" name="number-baggage" value="0" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-3" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                    </li>
                                                    <li>
                                                        <button type="button" data-stepperbtn="+" class="btn-plus btn-stepper">+</button>
                                                    </li>
                                                    </ul>
                                                    <span class="sgd-price hidden"><%- currency.code %> <span>0.00</span></span>
                                                </div>
                                                <% } %>
                                            </div>
                                          </div>
                                          <p class="baggage-piece-text"> Displayed prices are rounded to the nearest dollar and may vary slightly from the final price shown at the payment page. </p>

                                        <% }); %>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <%
                          }); %>
                          <input type="button" name="confirm-baggage-piece-btn" id="confirm-baggage-piece-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                        </div>
                        </div>
                      </div>
                </div>
              </div>
            <% } %>
          <% } %>
          </div>
        </div>
      </div>
    </div>
<% } %>
