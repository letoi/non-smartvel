<% if(data) { %>

  <% _.each(data.flights, function(flightsData, flightidx) { %>
    <%  var originFlight = $.grep(data.airports, function(airport){
      return flightsData.originAirportCode === airport.airportCode
    })[0];
    var desinationFlight = $.grep(data.airports, function(airport){
      return flightsData.destinationAirportCode === airport.airportCode
    })[0]; %>
    <div class="head-flight-info">
      <h3 class="sub-heading-1--dark"><span><%- originFlight.cityName %> to <%- desinationFlight.cityName %></span>
        <a href="#" class="search-link" data-trigger-popup="popup--edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
        <a href="cib-fare-calendar-1.html" class="calendar-link"><em class="ico-date"><span class="ui-helper-hidden-accessible"></span></em>See 7-day fares</a>
      </h3>
      <div class="form-group grid-row">
        <div class="grid-col two-five">
          <div class="grid-inner">
            <div data-customselect="true" class="custom-select custom-select--2 sortby__select" data-table-filter="data-table-filter">
              <label for="customSelect-0-combobox" class="select__label">Sort by</label><span class="select__text" aria-hidden="true">Recommended</span><span class="ico-dropdown" aria-hidden="true">Recommended</span>
              <select id="custom-select--sort" name="custom-select--sort">
                <option value="recommended" selected="selected">Recommended</option>
                <option value="price-asc">Price (Lowest to highest)</option>
                <option value="travel-asc">Total travel time (Shortest to longest)</option>
                <option value="departure-asc">Departure time (Earliest to latest)</option>
                <option value="departure-desc">Departure time (Latest to earliest)</option>
                <option value="arrival-asc">Arrival time (Earliest to latest)</option>
                <option value="arrival-desc">Arrival time (Latest to earliest)</option>
              </select>
            </div>
          </div>
        </div><a href="#" class="btn-compare" data-trigger-popup="true"><em class="ico-download"><span class="ui-helper-hidden-accessible"></span></em>Compare all fare types</a>
      </div>
      <div data-flight="1" class="bgd-active-green-1">
        <p id="table-summary__<%- flightidx %>" class="says">, You are now on a fare table. To navigate through the fare selection, press tab. To quickly move to the other available flights, use the up or down arrow keys. To select a fare, press enter. To go directly to the Booking Summary Panel, press control Y. </p>
        <%
          if(data.flights.length > 1){
            var originFlight1 = $.grep(data.airports, function(airport){
              return data.flights[0].originAirportCode === airport.airportCode
            })[0];
            var desinationFlight2 = $.grep(data.airports, function(airport){
              return data.flights[1].destinationAirportCode === airport.airportCode
            })[0];
          }
          var blockCaption = "Outbound fares";
          if(flightidx > 0){
            if(originFlight1 === desinationFlight2){
              blockCaption = "Inbound fares";
            }
          }
        %>
        <% if(data.fareFamilies.length === 6) { %>
          <table class="flights__table flights__table--select six-column" aria-describedby="table-caption__1 table-summary__1 table-caption__1 table-summary__1">
        <% } else { %>
          <table class="flights__table flights__table--select" aria-describedby="table-caption__1 table-summary__1 table-caption__1 table-summary__1">
        <% } %>
          <caption id="table-caption__<%- flightidx %>" class="says">
              <%- blockCaption %>
          </caption>
          <thead>
            <tr>
              <th class="title-head bgd-economy-1">Flight information</th>
              <% _.each(data.fareFamilies, function(fare, fareidx){ %>
                <th class="title-head bgd-economy-<%-fare.fareFamilyCode%> hidden-mb package-<%- fareidx + 1 %>"><%- fare.fareFamilyName %></th>
              <% }) %>
            </tr>
          </thead>
          <tbody>
            <% _.each(flightsData.segments, function(segmentsItem, segidx){ %>
              <%
                var departureTime = segmentsItem.departureDateTime,
                    arrivalDateTime = segmentsItem.arrivalDateTime,
                    newDepartureTime = departureTime.slice(11, 16),
                    newArrivalDateTime = arrivalDateTime.slice(11, 16),
                    departureSplit = departureTime.split(' '),
                    departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0])),
                    arrivalDateTimeSplit = arrivalDateTime.split(' '),
                    arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0]));
                    var secondsTotal = parseInt(segmentsItem.tripDuration);
                    var hh2 = Math.floor(secondsTotal / 3600);
                    var mm2 = Math.floor((secondsTotal - (hh2 * 3600)) / 60);
                    if (hh2 < 10) {hh2 = "0"+hh2;}
                    if (mm2 < 10) {mm2 = "0"+mm2;}
                    var timeTotal = hh2+"hr "+mm2+ "mins";
              %>
                <tr data-price="0" data-travel-time="<%- segmentsItem.tripDuration %>" data-arrival-time="<%- segmentsItem.arrivalDateTime %>" data-departure-time="<%- segmentsItem.departureDateTime %>" data-recommended="<%-segidx%>" class="even-item">
                  <td colspan="7" class="flight-part" data-package-disabled="false">
                    <table class="flights__table--1__inner type-economy-select" aria-describedby="table-0__caption">
                      <caption class="says-1"><% _.each(segmentsItem.legs, function(segmentsLegs, legidx){ %><%
                          var departureLegs1 = segmentsLegs.departureDateTime,
                            arrivalDateLegs1 = segmentsLegs.arrivalDateTime,
                            newDepartureLegs1 = departureLegs1.slice(11, 16),
                            newArrivalDateLegs1 = arrivalDateLegs1.slice(11, 16),
                            departureSplit1 = departureLegs1.split(' '),
                            departureDatepicker1 = $.datepicker.formatDate('dd M (D)', new Date(departureSplit1[0])),
                            arrivalDateTimeSplit1 = arrivalDateLegs1.split(' '),
                            arrivalDateTimeDatepicker1 = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit1[0]));
                          var origin = $.grep(data.airports, function(airport){
                            return segmentsLegs.originAirportCode === airport.airportCode
                          })[0];
                          var desination = $.grep(data.airports, function(airport){
                            return segmentsLegs.destinationAirportCode === airport.airportCode
                          })[0];
                          var secondsLayover = parseInt(segmentsLegs.layoverDuration);
                          var hh1 = Math.floor(secondsLayover / 3600);
                          var mm1 = Math.floor((secondsLayover - (hh1 * 3600)) / 60);
                          if (hh1 < 10) {hh1 = "0"+hh1;}
                          if (mm1 < 10) {mm1 = "0"+mm1;}
                          var timeLayover = hh1+"hr "+mm1+ "mins";%>
                        Flight <%- segmentsLegs.marketingAirline.code %> <%- segmentsLegs.flightNumber %>. Departing from <%= origin.countryName %> at <%- origin.airportName %> Terminal <%- segmentsLegs.departureTerminal %> on <%- departureDatepicker1 %> at <%- newDepartureLegs1 %>. Arriving in <%- desination.countryName %> at <%- desination.airportName %> Terminal <%- segmentsLegs.arrivalTerminal %> on <%- arrivalDateTimeDatepicker1 %> at <%- newArrivalDateLegs1 %>.<% if(segmentsLegs.layoverDuration) { %>Layover time: <%- timeLayover %><% } %><% }) %>
                        <% if(segmentsItem.tripDuration){ %>Total flight duration: <%- timeTotal %><% } %>
                      </caption>
                      <tbody>
                        <tr>
                            <td class="first flights__info--group" data-package-disabled="false">
                              <% if(segmentsItem.legs.length) {%>
                                <% _.each(segmentsItem.legs, function(segmentsLegs, legidx){ %>
                                  <%
                                      var departureLegs1 = segmentsLegs.departureDateTime,
                                        arrivalDateLegs1 = segmentsLegs.arrivalDateTime,
                                        newDepartureLegs1 = departureLegs1.slice(11, 16),
                                        newArrivalDateLegs1 = arrivalDateLegs1.slice(11, 16),
                                        departureSplit1 = departureLegs1.split(' '),
                                        departureDatepicker1 = $.datepicker.formatDate('dd M (D)', new Date(departureSplit1[0])),
                                        arrivalDateTimeSplit1 = arrivalDateLegs1.split(' '),
                                        arrivalDateTimeDatepicker1 = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit1[0]));
                                      var origin = $.grep(data.airports, function(airport){
                                        return segmentsLegs.originAirportCode === airport.airportCode
                                      })[0];
                                      var desination = $.grep(data.airports, function(airport){
                                        return segmentsLegs.destinationAirportCode === airport.airportCode
                                      })[0];
                                  %>
                                    <div class="flights__info none-border">
                                    <% if( segmentsLegs.flightNumber !== 'undefined') { %>
                                      <div data-flight-number="<%- segmentsLegs.flightNumber %>" data-carrier-code="SQ" data-date="<%- segmentsLegs.departureDateTime %>" data-origin="<%- segmentsLegs.originAirportCode %>" class="flights--detail-1 left">
                                        <span class="flight-info" tabindex="0">
                                        FLIGHT <%- segmentsLegs.marketingAirline.code %> <%- segmentsLegs.flightNumber %> <em class="ico-point-d"></em><span class="loading loading--small hidden" aria-hidden="true">Loading...</span></span>
                                        <div class="details">
                                          <p>Aircraft type: <%- segmentsLegs.aircraft.name %>
                                          </p>
                                          <%
                                            var seconds = parseInt(segmentsLegs.flightDuration);
                                            var hh = Math.floor(seconds / 3600);
                                            var mm = Math.floor((seconds - (hh * 3600)) / 60);
                                            if (hh < 10) {hh = "0"+hh;}
                                            if (mm < 10) {mm = "0"+mm;}
                                            var time = hh+"hr "+mm+ "mins";
                                          %>
                                            <p>Flying time: <%- time %></p>
                                        </div>
                                      </div>
                                      <span class="class-flight"></span>
                                    <% } %>
                                    </div>
                                    <div class="flights__info">
                                      <div class="flights__info--detail"><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- segmentsLegs.originAirportCode %> <%- newDepartureLegs1 %></span><span class="country-name"><%= origin.countryName %></span><span class="date"><%- departureDatepicker1 %>, <br><%- origin.airportName %><br>
                                      <% if( segmentsLegs.departureTerminal !== null) { %>
                                        <span class="terminal">Terminal <%- segmentsLegs.departureTerminal %></span>
                                      <% } %>
                                      </span></div>
                                      <div class="flights__info--detail"><span class="hour"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateLegs1 %></span><span class="country-name"><%- desination.countryName %></span><span class="date"><%- arrivalDateTimeDatepicker1 %>, <br>
                                      <%- desination.airportName %><br>
                                      <% if( segmentsLegs.arrivalTerminal !== null) { %>
                                        <span class="terminal">Terminal <%- segmentsLegs.arrivalTerminal %></span>
                                      <% } %>
                                      </span></div>
                                      <p class="operated">Operated by <%- segmentsLegs.operatingAirline.name %></p>
                                    </div>
                                    <% if( segmentsLegs.layoverDuration !== 'undefined' && segmentsLegs.layoverDuration !== 0) { %>
                                      <%
                                        var secondsLayover = parseInt(segmentsLegs.layoverDuration);
                                        var hh1 = Math.floor(secondsLayover / 3600);
                                        var mm1 = Math.floor((secondsLayover - (hh1 * 3600)) / 60);
                                        if (hh1 < 10) {hh1 = "0"+hh1;}
                                        if (mm1 < 10) {mm1 = "0"+mm1;}
                                        var timeLayover = hh1+"hr "+mm1+ "mins";
                                      %>
                                      <div class="flights__info"><span>Layover time: <%- timeLayover %></span><a href="javascript:;" data-toggle-link="true" class="link-5 right active">Fares<em class="ico-point-d"><span class="ui-helper-hidden-accessible">See aircraft type</span></em>
                                      </a>
                                      </div>
                                    <% } %>
                                  <% }); %>
                                  <% if( segmentsItem.tripDuration !== 'undefined') { %>
                                    <%
                                      var secondsTotal = parseInt(segmentsItem.tripDuration);
                                      var hh2 = Math.floor(secondsTotal / 3600);
                                      var mm2 = Math.floor((secondsTotal - (hh2 * 3600)) / 60);
                                      if (hh2 < 10) {hh2 = "0"+hh2;}
                                      if (mm2 < 10) {mm2 = "0"+mm2;}
                                      var timeTotal = hh2+"hr "+mm2+ "mins";
                                    %>
                                    <div class="flights__info last"><span>Total travel time: <%- timeTotal %></span><a href="javascript:;" data-toggle-link="true" class="link-5 right active">Fares<em class="ico-point-d"></em></a>
                                    </div>
                                   <% } %>
                              <% }%>
                                <div data-toggle-content="true" class="flights__info flights__info--mb visible-mb">
                                  <% _.each(data.fareFamilies, function(fare, fareidx){ %>
                                    <%
                                    var totalPrice = [],
                                        recommendations = data.recommendations,
                                        segmentBound,
                                        segments,
                                        displayLastSeat,
                                        numberOfLastSeats;
                                    for(var i = 0; i < recommendations.length; i++) {
                                      segmentBound = flightidx === 0 ? recommendations[i].segmentBounds[0] : recommendations[i].segmentBounds[1];
                                      segments = segmentBound.segments;
                                      for(var j = 0; j < segments.length; j++) {
                                        if(segments[j].segmentID === segmentsItem.segmentID && fare.fareFamily === recommendations[i].fareFamily) {
                                          totalPrice.push(recommendations[i].fareSummary.fareDetailsPerAdult.totalAmount);
                                          displayLastSeat = segmentBound.segments[j].displayLastSeat;
                                          numberOfLastSeats = segmentBound.segments[j].numOfLastSeats;
                                            break;
                                        }
                                      }
                                    }
                                    %>
                                    <div class="flights__info--price bgd-economy-<%-fare.fareFamilyCode%>">
                                        <div class="package--name"></div>
                                        <div class="package--price">
                                          <div class="custom-radio custom-radio--1" tabindex="0">
                                            <% if (totalPrice.length > 0) { %>
                                              <input name="<%= flightidx %>-flight-select-mb-1" id="flight-select-mb-1-<%-flightidx%>-<%- segmentsItem.segmentID%>-<%-fareidx%>" type="radio" value="O<%-segmentsItem.segmentID + fare.fareFamily%>" data-related="<%-fareidx%>" data-flight-class="<%-fare.fareFamilyName %>" data-waitlisted="false" aria-hidden="true" tabindex="-1">
                                              <label for="flight-select-mb-1-<%-flightidx%>-<%- segmentsItem.segmentID%>-<%-fareidx%>"><span class="fare-label"><%= fare.fareFamilyName %></span>
                                                <% if( displayLastSeat === true) { %>
                                                <div class="seat-left seat-left-xs hidden">
                                                  <span>
                                                    <%= numberOfLastSeats %> seats left
                                                  </span>
                                                </div>
                                              <% } %>
                                              <div class="block-content">
                                                From <strong class="package--price-number" data-price-number='<%- totalPrice[0]%>'><%= Number(totalPrice[0]).toLocaleString('en', { minimumFractionDigits: 2 }) %></strong><a href="javascript: void(0)" data-type="3" data-tooltip="true" data-max-width="310" data-content="<div class=&quot;summary-fare&quot;> <h3 class=&quot;title-conditions&quot; id=&quot;tooltip-title&quot;>Summary of fare conditions</h3> <ul class=&quot;summary-fare__conditions&quot;> <li><em class=&quot;ico-check-thick&quot;></em>Booking change (USD 50)</li> <li><em class=&quot;ico-check-thick&quot;></em>Earn KrisFlyer miles (50%)</li> <li><em class=&quot;ico-close&quot;></em>Refund/Cancellation</li> <li><em class=&quot;ico-close&quot;></em>Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;<a href=&quot;cib-blank-page.html&quot;>terms and conditions</a>) </li> </ul> </div>" class="ico-info-round-fill" aria-label="View more information">&nbsp;</a>
                                                <% if( displayLastSeat === true) { %>
                                                  <div class="seat-left-tl hidden">
                                                    <span>
                                                      <%= numberOfLastSeats %> seats left
                                                    </span>
                                                  </div>
                                                <% } %>
                                              </div>
                                              <span class="radio-total-fare">Total fare, </span><span class="radio-adult">1 adult </span>
                                              </label>
                                            <% } else { %>
                                              <label for="flight-select-mb-1-<%-flightidx%>-<%- segmentsItem.segmentID%>-<%-fareidx%>">
                                              <span class="fare-label"><%= fare.fareFamilyName %></span>
                                              <span>Not available</span>
                                              </label>
                                            <% } %>
                                          </div>
                                        </div>
                                    </div>
                                  <% }) %>
                                </div>


                              </td>
                            <% _.each(data.fareFamilies, function(fare, fareidx) { %>
                              <%
                              var totalPrice = [],
                                  recommendations = data.recommendations,
                                  segmentBound,
                                  segments,
                                  displayLastSeat,
                                  numberOfLastSeats;
                              for(var i = 0; i < recommendations.length; i++) {
                                segmentBound = flightidx === 0 ? recommendations[i].segmentBounds[0] : recommendations[i].segmentBounds[1];
                                segments = segmentBound.segments;
                                for(var j = 0; j < segments.length; j++) {
                                  if(segments[j].segmentID === segmentsItem.segmentID && fare.fareFamily === recommendations[i].fareFamily) {
                                    totalPrice.push(recommendations[i].fareSummary.fareDetailsPerAdult.totalAmount);
                                    displayLastSeat = segmentBound.segments[j].displayLastSeat;
                                    numberOfLastSeats = segmentBound.segments[j].numOfLastSeats;
                                      break;
                                  }
                                }
                              }
                              %>
                              <td class="hidden-mb package-<%-fareidx + 1%>" data-package-disabled="false"><span class="flights__active-border"></span>
                                <% if (totalPrice.length > 0) { %>
                                  <% if( displayLastSeat === true) { %>
                                    <div class="seat-left">
                                      <span>
                                        <%= numberOfLastSeats %> seats left
                                      </span>
                                    </div>
                                  <% } %>
                                <div class="custom-radio custom-radio--1" tabindex="0" aria-labelledby="flight-select-<%- segmentsItem.segmentID%>-<%-fareidx%>-label" role="radio" aria-checked="false" data-radiogroup="flight-select-1">
                                  <input name="<%= flightidx %>-flight-select-1" id="flight-select-<%-flightidx%>-<%- segmentsItem.segmentID%>-<%-fareidx%>" type="radio" value="O<%-segmentsItem.segmentID + fare.fareFamily%>" data-related="<%-fareidx%>" data-flight-class="<%-fare.fareFamilyName %>" data-waitlisted="false" aria-hidden="true" tabindex="-1">
                                  <label for="flight-select-<%-flightidx%>-<%- segmentsItem.segmentID%>-<%-fareidx%>" id="flight-select-<%-flightidx%>-<%-segmentsItem.segmentID%>-<%-fareidx%>-label"><span class="says-1"><%= fare.fareFamilyName %></span>
                                  <% if( displayLastSeat === true) { %>
                                    <span class="says-1"><%= numberOfLastSeats %> seats left</span>
                                  <% } %>From <strong class="package--price-number" data-price-number='<%- totalPrice[0]%>'><%= Number(totalPrice[0]).toLocaleString('en', { minimumFractionDigits: 2 }) %></strong><span class="radio-total-fare">Total fare, </span><span class="radio-adult">1 adult </span>
                                  <span class="says-1">To select this fare press enter.</span></label>
                                </div>
                                <div class="tooltip-wrapper" aria-label="View More Information" role="button" data-aria="wcag-tooltip-0-0-1">
                                  <span class="flights__tooltip-background"></span><a href="javascript: void(0)" data-type="3" data-tooltip="true" data-max-width="310" data-content="<div class=&quot;summary-fare&quot;> <h3 class=&quot;title-conditions&quot; id=&quot;tooltip-title&quot;>Summary of fare conditions</h3> <ul class=&quot;summary-fare__conditions&quot;> <li><em class=&quot;ico-check-thick&quot;></em>Booking change (USD 50)</li> <li><em class=&quot;ico-check-thick&quot;></em>Earn KrisFlyer miles (50%)</li> <li><em class=&quot;ico-close&quot;></em>Refund/Cancellation</li> <li><em class=&quot;ico-close&quot;></em>Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;<a href=&quot;cib-blank-page.html&quot;>terms and conditions</a>) </li> </ul> </div>" class="ico-info-round-fill" aria-label="View more information" aria-hidden="true" tabindex="0"></a>
                                </div>
                                <% } else {%>
                                  <span>Not available</span>
                                <%}%>
                              </td>
                            <% }) %>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
              <% }); %>
            </tbody>
        </table>
      </div>
      <div class="flight__control">
      <a href="#" class="link-7 left"><em class="ico-point-l"></em>Previous day</a>
      <a href="#" class="link-7 right">Next day<em class="ico-point-r"></em></a>
      </div>
    </div>
  <% }); %>
<% } %>
