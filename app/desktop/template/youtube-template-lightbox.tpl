<aside id="youtube-lightbox" class="popup popup--video-lightbox hidden">
  <div class="popup__inner">
    <div class="popup__content">
      <a href="#" class="popup__close" data-close="true"></a>
      <div class="wrap-video">
        <div id="youtube-player">
          <img src="images/photo-preview-video.jpg" alt="" longdesc="img-desc.html">
        </div>
      </div>
      <ul class="watch-list">
        <% _.each(data.image, function(album, idx) { %>
          <li>
            <a href="#" class="info-watch">
              <img src="<%- album.url %>" alt="" longdesc="img-desc.html">
              <div class="desc-thumb">
                <p><%- album.description %></p>
              </div>
            </a>
          </li>
        <% }); %>
      </ul>
    </div>
  </div>
</aside>
