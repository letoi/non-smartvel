<a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
<h3 class="popup__heading">KrisFlyer member benefits for Premium Economy</h3>
<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">
  <ul class="tab" role="tablist">
    <li class="tab-item active" aria-selected="true" role="tab"><a href="#">PPS Club</a>
    </li>
    <li class="tab-item" role="tab"><a href="#">KrisFlyer Elite Gold</a>
    </li>
    <li class="tab-item" role="tab"><a href="#">KrisFlyer Elite Silver</a>
    </li>
    <li class="more-item"><a href="#">More<em class="ico-dropdown"></em></a>
    </li>
  </ul>
  <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">
    <label for="multi-select" class="select__label">&nbsp;</label><span class="select__text">Rooms and rates</span><span class="ico-dropdown"></span>
    <select id="multi-select" name="multi-select">
      <option value="0" selected="selected">PPS Club</option>
      <option value="1">KrisFlyer Elite Gold</option>
      <option value="2">KrisFlyer Elite Silver</option>
    </select>
  </div>
  <div class="tab-wrapper">
    <div class="tab-content active", role="tabpanel">
      <div class="tab-heading">
        PPS Club member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Premium Economy <br>Better & Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-business-1"></em>
              <span>Baggage*</span>
            </td>
            <td>
              <div class="data-title">Premium Economy <br>Better & Best</div><span>Additional 35kg</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Premium Economy <br>Better & Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
      <p> * This privilege is extended to your travelling partner</p>
      <p> ^ Your travelling partners will be allowed to select seats from SGD 18</p>
    </div>
    <div class="tab-content" role="tabpanel">
      <div class="tab-heading">
        KrisFlyer Elite Gold member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Premium Economy <br>Better & Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-business-1"></em>
              <span>Baggage*</span>
            </td>
            <td>
              <div class="data-title">Premium Economy <br>Better & Best</div><span>Additional 35kg</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Premium Economy <br>Better & Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="tab-content" role="tabpanel">
      <div class="tab-heading">
        KrisFlyer Elite Silver member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Premium Economy <br>Better & Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Premium Economy <br>Better & Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
