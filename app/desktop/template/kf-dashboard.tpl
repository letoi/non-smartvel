<% if (data) { %>
  <% _.each(data.digitalWalletCardVO, function(
  digitalWalletCard, digitalWalletCardIdx) { %>
    <div class="alert-block checkin-alert <% if(digitalWalletCard.expiryStatus !=="expired") { %> hidden <% } %>">
      <div class="inner">
        <div class="alert__icon"><em class="ico-alert"></em></div>
        <div class="alert__message">One or more of your saved cards have expired. Update your card details to continue using them.</div>
      </div>
    </div>
  <% }); %>
  <div class="blk-heading">
    <h2 class="main-heading">Save your credit / debit card details</h2>
    <p class="desc">Store your credit / debit cards in your Digital Wallet, and enjoy greater convenience the next time you pay for your fights.</p>
    <div class="button-group-1"><a href="kf-dashboard-add.html" class="btn-1 btn-add-new-card">Add new card<span class="text"></span></a>
    </div>
    <div class="kf-list-card">
      <% _.each(data.digitalWalletCardVO, function(digitalWalletCard, digitalWalletCardIdx) { %>
        <%
          var creditCard = digitalWalletCard.cardTokenNumber;
          var sliceStringCard = creditCard.slice(-4);

          var expiryDate = digitalWalletCard.expiryDate;
          var indexOfDate = expiryDate.indexOf('-');
          var month = expiryDate.slice(0, indexOfDate);
          var year = expiryDate.slice(indexOfDate + 1);
        %>
        <div class="item <% if(digitalWalletCard.defaultCard ==="true") { %> is-default <% } %>">
          <form name="form-edit-card-<%- digitalWalletCardIdx %>" action="#" class="form-global form-kf form-edit-card" novalidate="novalidate" id="form-edit-card-<%- digitalWalletCardIdx %>">
              <div class="group-name"><img src="images/<%- digitalWalletCard.cardType.toLowerCase() %>.png" alt="credit card" data-format-card="master" class="type-card">
                <h3 class="title-4--blue">&bull;&bull;&bull;&bull;<%- sliceStringCard %></h3>
                <span class="text-default">Default card</span>
              </div>
              <div class="content-infor">
                <ul class="kf-infor">
                  <li><span class="name">Name on credit / debit card</span><span class="value"><%- digitalWalletCard.cardHolderName %></span></li>
                  <li><span class="name">Expiry date</span><% if(digitalWalletCard.expiryStatus !=="expired") { %><span class="value"><%- month %> <%- year %></span><% } else { %> <span class="value has-expired"><%- month %> <%- year %> - Expired</span> <% } %></li>
                  <li><span class="name">Card currency</span><span class="value"><%- digitalWalletCard.cardCurrency %></span></li>
                </ul>
                <div class="group-link"><a href="#" class="link-4 link-delete" data-trigger-popup=".popup--delete-card"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Delete</a><a href="#" class="link-4 link-edit"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Edit</a>
                </div>
              </div>
              <div class="content-edit">
                <fieldset>
                  <div class="form-group grid-row">
                    <div class="grid-col kf-grid-col">
                      <label for="credit-debit-<%- digitalWalletCardIdx %>">Name on credit / debit card*</label>
                      <div class="grid-inner"><span class="input-1 capitalize-input">
                        <input id="credit-debit-<%- digitalWalletCardIdx %>" name="credit-debit-<%- digitalWalletCardIdx %>" placeholder="" value="<%- digitalWalletCard.cardHolderName %>" aria-labelledby="credit-debit-error" data-rule-required="true" data-msg-required="Enter first/given name (as in passport)." data-rule-onlycharacter="true" data-msg-onlycharacter="Enter letters of the English alphabet, and if required, any of these symbols /’@()”-" aria-required="true" aria-invalid="false" type="text" class="valid"><a href="#clear" class="ico-cancel-thin add-clear-text"><span class="ui-helper-hidden-accessible">clear</span></a><a href="#clear" class="ico-cancel-thin add-clear-text"><span class="ui-helper-hidden-accessible">clear</span></a></span></div>
                    </div>
                    <div class="grid-col kf-grid-col">
                      <div class="grid-inner">
                        <div class="form-group grid-row">
                          <label for="expiry-date-<%- digitalWalletCardIdx %>">Expiry date*</label>
                          <div class="grid-col kf-one-half">
                            <div class="grid-inner">
                              <div data-customselect="true" class="custom-select custom-select--2">
                                <label for="customSelect-<%- digitalWalletCardIdx %>-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Expiry date month</span>
                                </label><span class="select__text month" aria-hidden="true"><%- month %></span><span class="ico-dropdown month-edit" aria-hidden="true"><%- month %></span>
                                <select id="expiry-date-<%- digitalWalletCardIdx %>" name="expiry-date-<%- digitalWalletCardIdx %>" data-rule-required="true" data-msg-required="Select month." aria-required="true">
                                  <option value="">MM</option>
                                  <option value="01">01</option>
                                  <option value="02">02</option>
                                  <option value="03">03</option>
                                  <option value="04">04</option>
                                  <option value="05">05</option>
                                  <option value="06">06</option>
                                  <option value="07">07</option>
                                  <option value="08">08</option>
                                  <option value="09">09</option>
                                  <option value="10">10</option>
                                  <option value="11">11</option>
                                  <option value="12">12</option>
                                </select>
                              <p class="select__tips ui-helper-hidden-accessible" id="customSelect-0-customSelectUsage" data-description-host="#customSelect-<%- digitalWalletCardIdx %>-combobox" data-description-category="tip">Press space to open combobox, and up, down, and enter keys, to select an option</p></div>
                            </div>
                          </div>
                          <div class="grid-col kf-one-half">
                            <div class="grid-inner">
                              <div data-customselect="true" class="custom-select custom-select--2">
                                <label for="customSelect-1-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Expiry date year</span>
                                </label><span class="select__text year" aria-hidden="true"><%- year %></span><span class="ico-dropdown year-edit" aria-hidden="true"><%- year %></span>
                                <select id="input-year" name="input-year" data-rule-required="true" data-msg-required="Select year." aria-required="true">
                                  <option value="">YYYY</option>
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                  <option value="2021">2021</option>
                                  <option value="2022">2022</option>
                                  <option value="2023">2023</option>
                                  <option value="2024">2024</option>
                                  <option value="2025">2025</option>
                                  <option value="2026">2026</option>
                                  <option value="2027">2027</option>
                                  <option value="2028">2028</option>
                                  <option value="2029">2029</option>
                                  <option value="2030">2030</option>
                                  <option value="2031">2031</option>
                                  <option value="2032">2032</option>
                                  <option value="2033">2033</option>
                                  <option value="2034">2034</option>
                                  <option value="2035">2035</option>
                                </select>
                              <p class="select__tips ui-helper-hidden-accessible" id="customSelect-<%- digitalWalletCardIdx %>-customSelectUsage" data-description-host="#customSelect-<%- digitalWalletCardIdx %>-combobox" data-description-category="tip">Press space to open combobox, and up, down, and enter keys, to select an option</p></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="grid-col kf-grid-col">
                      <label for="card-currency-<%- digitalWalletCardIdx %>">Card currency</label>
                      <div class="grid-inner">
                        <div data-customselect="true" class="custom-select custom-select--2 default">
                          <label for="customSelect-2-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Label</span>
                          </label><span class="select__text card-currency" aria-hidden="true"><%- digitalWalletCard.cardCurrency %></span><span class="ico-dropdown card-currency" aria-hidden="true"><%- digitalWalletCard.cardCurrency %></span>
                          <select id="card-currency-<%- digitalWalletCardIdx %>" name="card-currency-<%- digitalWalletCardIdx %>">
                            <option value="">Select</option>
                            <option value="SGD">SGD</option>
                            <option value="USD">USD</option>
                            <option value="GBP">GBP</option>
                            <option value="GBP">JPY</option>
                            <option value="GBP">EUR</option>
                          </select>
                        <p class="select__tips ui-helper-hidden-accessible" id="customSelect-2-customSelectUsage" data-description-host="#customSelect-2-combobox" data-description-category="tip">Press space to open combobox, and up, down, and enter keys, to select an option</p></div>
                      </div>
                    </div>
                  </div>
                  <div class="custom-checkbox custom-checkbox--1 label-bold" <% if(digitalWalletCard.defaultCard ==="true") { %> data-disabled-checkbox = "true" <% } %> >
                    <input name="set-default-card-<%- digitalWalletCardIdx %>" id="set-default-card-<%- digitalWalletCardIdx %>" aria-labelledby="set-default-card-<%- digitalWalletCardIdx %>-error" type="checkbox" aria-label="Set this as your default payment method">
                    <label for="set-default-card-<%- digitalWalletCardIdx %>">Set this as your default payment method</label>
                  </div>
                </fieldset>
                <div class="button-group-1 has-btn-4">
                  <input type="submit" name="btn-save" id="btn-save" value="Save" class="btn-1 btn-save">
                  <input type="submit" name="btn-delete" id="btn-delete" value="Delete" class="btn-4 btn-delete" data-trigger-popup=".popup--delete-card">
                </div>
              </div>
          </form>
        </div>
      <% }); %>
    </div>
  </div>
<% } %>
