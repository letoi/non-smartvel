<% if(data) { %>
  <% var showBlock = false; %>
  <% _.each(data.passenger, function(passengers, passengerIdx){ %>
    <% 
      if(passengers.infant) {
        showBlock = true;
      }
    %>
  <% }); %>
	<div class="travel-party <% if(showBlock) { %> show <% } %>">
    <div class="travel-thumb"><span class="ico ico-preferred-group"></span></div>
    <div class="travel-content">
      <div class="title">Your travel party enjoys complimentary seat selection at booking, as you're travelling with a child</div>
      <span class="says" tabindex="0">Your travel party enjoys complimentary seat selection at booking, as you're travelling with a child</span>
    </div>
  </div>
<% } %>
