<% if(data) { %>
	<div class="bsp-booking-summary__heading">
		<div class="bsp-booking-summary__heading-wrap">
			<% if(data.flight.length > 2) { %>
				<% _.each(data.flight, function(itemFly, idx){ %>
					<span class="bsp-flight hidden-mb-small hidden-tb"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
					<% }) %>
						<% _.each(data.flight.slice(0 , 4), function(itemFly, idx4){ %>
							<span class="bsp-flight hidden-mb-small hidden-dt"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
							<% }) %>
								<span class="plus-more-flight hidden-mb-small hidden-dt">
                            <% if(data.flight.length > 4) { %>
                            <% var numberTablet = data.flight.length - 4 %>
                            + <%- numberTablet %> more flights</span>
								<% } %>
									<span class="plus-more-flight hidden-tb-dt"><% var numberApplyMobile = data.flight.length %> <%- numberApplyMobile %> more flights</span><span class="adults">· <%- data.adultCount %> Adults</span><a href="#" class="search-link hidden-mb-small trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
									<% } else { %>
										<% if(data.flight.length < 2) { %>
											<span class="bsp-flight"><%- data.flight[0].originAirportCode %> - <%- data.flight[0].destinationAirportCode %></span><span class="adults">· <%- data.adultCount %> Adults</span><a href="#" class="search-link hidden-mb-small trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
											<% } else { %>
												<% if(data.flight.length = 2) { %>
													<span class="bsp-flight hidden-mb-small"><%- data.flight[0].originAirportCode %> - <%- data.flight[0].destinationAirportCode %><em class="ico-return-arrows"></em><%- data.flight[1].originAirportCode %> - <%- data.flight[1].destinationAirportCode %></span>
													<span class="bsp-flight hidden-tb-dt"><%- data.flight[0].originAirportCode %><em class="ico-return-arrows"></em><%- data.flight[1].originAirportCode %></span>
													<span class="adults">· <%- data.adultCount %> Adults</span><a href="#" class="search-link hidden-mb-small trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
													<% } %>
														<% } %>
															<% } %>
		</div>
	</div>
	<div class="bsp-booking-summary__content all-transition">
		<div class="bsp-booking-summary__content-wrap">
			<div class="bsp-booking-summary__content-control">
				<div class="bsp-total-fare">
					<div class="total-fare--inner"><span class="flight-date-title">Total fare</span>
						<div class="flights__info"><span class="number-passengers"><%- data.adultCount %> Adults</span>
							<p class="total-cost"><span class="unit"><%- data.currency %> <%- data.costPayableByCash %></span></p><span class="fare-notice">Total fare includes discounts, taxes and surcharges</span>
						</div>
						<div class="bsp-flights-cost"><span class="bsp-flights-cost-title"><span class="text-left">FLIGHTS</span><span class="text-right"><%- data.currency %></span></span>
							<ul class="bsp-flights-cost__details">
								<li><span>Fare</span><span class="price"><%- data.fareTotal.toFixed(2) %></span></li>
								<li><span>Airport/Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
								<li><span>Carrier surcharges</span><span class="price"><% _.each(data.paxDetails, function(paxDetails, idx){ %>
                <%- paxDetails.fareDetails.surchargeAmount.toFixed(2) %> <% }) %></span></li>
								<li class="sub-total"><span>Subtotal</span><span class="price"><%- data.currency %> <%- data.fareSubTotal.toFixed(2) %></span></li>
							</ul>
						</div>
						<div class="cta-group">
							<li><a href="#" class="search-link trigger-popup-flights-details" data-trigger-popup=".popup--flights-details"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Cost breakdown by passengers</a>
							</li>
							<li><a href="#" class="search-link trigger-popup-add-ons-baggage" data-trigger-popup=".popup--add-ons-summary"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Full fare condition</a>
							</li>
							<li><a href="#" class="search-link trigger-popup-add-ons-summary" data-trigger-popup=".popup--add-ons-baggage"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Baggage allowance</a>
							</li>
						</div><a href="#" class="link-4 more-detail" data-more-details="true">More details<em class="ico-point-d"><span class="ui-helper-hidden-accessible"></span></em></a><a href="#" class="link-4 less-detail hidden-mb-small " data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a>
					</div>
				</div>
				<div class="bsp-booking-summary--group <% if(data.flight.length >= 2) { %> multi-city <% } %> hidden-mb-small">
					<% if(data.flight.length >= 2) { %>
						<% _.each(data.flight, function(itemFly, idx){ %>
							<div class="bsp-flights__info--group <% if(idx % 2 == 0) {%> position-even <% } %> <% if(idx >= 2) {%> hidden <% } %> flight-result-leg-wrap all-transition">
								<div class="bsp-flights__info--inner"><span class="flight-date-title">
                  <% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
                    <% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
                  <% } %>
                  <% if(data.flight.length > 2) { %>
                    Flight <%- idx + 1 %>
                  <% } %>
                  </span>
									<div class="flights__info">
										<% _.each(itemFly.flightSegments, function(flightSegments, idx){ %>
											<div class="bsp-flights__info--detail"><span class="time">
                            <%- flightSegments.deparure.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- itemFly.originAirportCode %> <%- flightSegments.deparure.time %></span>
												<span class="country-name">
													<%- itemFly.origin %>
														</span>
											</div>
											<div class="bsp-flights__info--detail"><span class="time">
                            <%- flightSegments.arrival.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- itemFly.destinationAirportCode %> <%- flightSegments.arrival.time %></span>
												<span class="country-name">
													<%- itemFly.destination %>
														</span>
											</div>
											<% }) %>
									</div>
								</div>
							</div>
							<% }) %>
								<% } else { %>
									<% if(data.flight.length < 2) { %>
										<div class="bsp-flights__info--group flight-result-leg-wrap all-transition">
											<div class="bsp-flights__info--inner"><span class="flight-date-title">One way</span>
												<div class="flights__info">
													<div class="bsp-flights__info--detail"><span class="time">
                            <%- data.flight[0].flightSegments[0].deparure.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- data.flight[0].originAirportCode %> <%- data.flight[0].flightSegments[0].deparure.time %></span>
														<span class="country-name">
															<%- data.flight[0].origin %>
																</span>
													</div>
													<div class="bsp-flights__info--detail"><span class="time">
                            <%- data.flight[0].flightSegments[0].arrival.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- data.flight[0].destinationAirportCode %> <%- data.flight[0].flightSegments[0].arrival.time %></span>
														<span class="country-name">
															<%- data.flight[0].destination %>
																</span>
													</div>
												</div>
											</div>
										</div>
										<% } %>
											<% } %>
												<% if(data.flight.length > 2) { %>
													<% var numberLength = data.flight.length - 2 %>
														<div class="plus-more-detail"><a href="#" class="link-4 hidden-mb-small"><span class="text">+ <%- numberLength %> more flights</span></a>
														</div>
														<% } %>
				</div>
				<div class="bsp-booking-summary__content-detail hidden ">
					<div class="bsp-booking-summary--group <% if(data.flight.length >= 2) { %> multi-city <% } %>">
						<% _.each(data.flight, function(itemFly, idx){ %>
							<div class="bsp-flights__info--group <% if(idx % 2 == 0 && data.flight.length >= 2) {%> position-even <% } %> flight-result-leg-wrap all-transition">
								<div class="bsp-flights__info--inner"><span class="flight-date-title">
                <% if(data.flight.length < 2) { %>
                  One way
                <% } %>
                <% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
                  <% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
                <% } %>
                <% if(data.flight.length > 2) { %>
                  Flight <%- idx + 1 %>
                <% } %>
                  </span>
									<div class="flights__info">
										<div class="bsp-flights__info--detail"><span class="time">
                     <% _.each(itemFly.flightSegments, function(itemSegments, idx){ %>
                        <%- itemSegments.deparure.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- itemFly.originAirportCode %> <%- itemSegments.deparure.time %></span>
											<span class="country-name">
												<%- itemFly.origin %>
													</span><span class="date"><%- itemSegments.deparure.airportName %></span><span class="terminal"> <% if(itemSegments.deparure.terminal !== null) { %> Terminal <% } %> <%- itemSegments.deparure.terminal %></span>
											<% }) %>
										</div>
										<div class="bsp-flights__info--detail"><span class="time">
                     <% _.each(itemFly.flightSegments, function(itemSegments, idx){ %>
                        <%- itemSegments.arrival.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- itemFly.destinationAirportCode %> <%- itemSegments.arrival.time %></span>
											<span class="country-name">
												<%- itemFly.destination %>
													</span><span class="date"><%- itemSegments.arrival.airportName %></span><span class="terminal"> <% if(itemSegments.arrival.terminal !== "") { %> Terminal <% } %> <%- itemSegments.arrival.terminal %></span>
											<% }) %>
										</div>
									</div>
									<% _.each(itemFly.flightSegments, function(itemSegments, idx){ %>
										<p class="flying-details"><strong><%- itemSegments.carrierName %></strong><span class="hidden-tb show-mb-inline">·</span>
											<%- itemSegments.carrierCode %>
												<%- itemSegments.flightNumber %> ·
													<%- itemSegments.airCraftType %><span class="economy"><%- itemSegments.cabinClassDesc %></span></p>
										<% }) %>
											<div class="flights__info border"><span>Total travel time: <%- itemFly.totalTravelTime %></span></div>
								</div>
							</div>
							<% }) %>
					</div>
					<div class="edit-search-mb-tb"><a href="#" class="btn-4 hidden-tb-dt trigger-popup-edit-search"><span class="text">Edit search</span></a></a><a href="#" class="link-4 less-detail hidden-tb-dt" data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a></div>
				</div>
			</div>
		</div>
	</div>
	<% } %>
