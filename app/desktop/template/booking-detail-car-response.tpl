<% if(data) { %>
    <div class="block-2 add-ons-item block-car">
        <h5 class="sub-heading-3--dark sub-title-car">Car rental</h5>
        <div class="car-infor">
            <div class="item-content-head">
                <div class="item-col-1">
                    <figure>
                        <img src="<%- data.vehicleDetails.imageURL %>" data-car alt="Car rental" longdesc="img-desc.html">
                    </figure>
                </div>
                <div class="item-col-2">
                    <div class="content">
                        <h6 class="sub-heading-2--blue name-car"><%- data.vehicleDetails.vehicleName %></h6>
                        <div class="desc">
                            <p class="sub-head">Premium</p>
                        </div>
                        <ul class="list-furnished">
                            <li><%- data.vehicleDetails.seats %> Seats</li>
                            <li><%- data.vehicleDetails.doors %> Doors</li>
                            <li><%- data.vehicleDetails.bigSuitcase %> Large bags</li>
                        </ul>
                        <ul class="list-furnished">
                            <li><% if(data.vehicleDetails.aircon === "Yes"){ %>Air Conditioning<% } else { %>
                            <% } %></li>
                            <li><%- data.vehicleDetails.automatic %> Tranmission</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="add-ons-item__content item-row">
            <div class="item">
                <ul class="info-details-1 style">
                    <li><span>Pick up:</span><p class="pick-up"><%- data.pickUp.dateAndTime %></p></li>
                    <li><span>Location:</span><%- data.pickUp.locationDetails.locationName %>, <%- data.pickUp.locationDetails.country %></li>
                </ul>
                <ul class="info-details-1 style">
                    <li><span>Drop off:</span><p class="drop-off"><%- data.dropOff.dateAndTime %></p></li>
                    <li><span>Location:</span><%- data.dropOff.locationDetails.locationName %>, <%- data.dropOff.locationDetails.country %></li>
                </ul>
            </div>
            <div class="item item--1">
                <div class="price-room">
                    <div class="total-title">Amount charged by Rentalcars.com</div>
                    <div class="total-cost"><%- data.price.currencyCode %> <%- data.price.amount %></div>
                </div>
                <ul class="info-details-2">
                    <li><span>Transaction ID:</span><%- data.bookingId %></li>
                    <li><span>Booking status:</span><%- data.statusMsg %></li>
                </ul>
            </div>
        </div>
        <div class="add-on-cantact">
            <figure class="img-block"><img src="images/addon-rentalcars-logo.png" alt="photo-car" longdesc="img-desc.html">
            </figure>
            <div class="add-on__content">
                <div class="desc">
                    <p>To make changes to your booking, go to Rentalcars.com.</p>
                    <span><a href="#" class="link-4"><em class="ico-point-r"></em>Manage booking</a></span>
                </div>
            </div>
        </div>
    </div>
<% } %>
