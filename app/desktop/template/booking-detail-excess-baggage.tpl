<%if (data && data.segmentDetail && data.segmentDetail.baggageDetails && data.segmentDetail.baggageDetails.length) {%>
    <%if (data.segmentDetail.baggageDetails.length === 1) {%>
      <div class="single-line booking-details booking-details--2 booking-details__baggage">
    <%} else {%>
      <div class="booking-details booking-details--2 booking-details__baggage">
    <% } %>
    <div class="booking-col col-1">
      <em class="ico-business-1"></em>
    </div>
    <div class="booking-col col-2">
      Baggage
    </div>
    <div class="booking-col col-3">
      <div class="align-wrapper">
        <div class="align-inner">
          <%_.each(data.segmentDetail.baggageDetails, function(baggage, bagIdx) {%>
            <p>
              <span class="booking-details__baggage-left"><%-baggage.baggageType%>: </span>
              <span class="booking-details__baggage-right">
                <% if (baggage.name && baggage.name.length) { %>
                  <% _.each(baggage.name, function(name, nameIdx) { %>
                    <%-name%> <%=nameIdx !== baggage.name.length - 1 ? '<br>' : ''%>
                  <% }); %>
                <% } %>
              </span>
            </p>
          <% }); %>
        </div>
      </div>
    </div>
    <div class="booking-col col-4">
      <%if (data.paxId && data.segmentDetail.segmentID) {%>
        <a href="mb-add-baggage.html?paxId=<%-data.paxId%>&amp;segmentId=<%-data.segmentDetail.segmentID%>"><em class="ico-point-r"></em><%-L10n.manageBooking.addLink%></a>
      <%}%>
    </div>
  </div>
<% } %>
