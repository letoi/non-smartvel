<% if(data.ticketStatus == 0) { %>
  <form method="post" class="booking-form" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1" data-checkin="0">Manage booking</button>
  </form>
<% } else if(data.ticketStatus == 1) { %>
  <form method="post" class="booking-form" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1" data-checkin="0">Check-in</button>
  </form>
<% } else if(data.ticketStatus == 2) { %>
  <form method="post" class="booking-form" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1" data-checkin="0">Manage Check in</button>
  </form>
<% } else if(data.ticketStatus == 3) { %>
  <div class="booking-form">
    <a href="<%-data.bookingConfirmUrl%>" class="btn-1" data-checkin="0">Confirm Booking</a>
  </div>
<% } %>
