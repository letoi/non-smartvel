<aside class="popup krisflyer-earn-rates hidden">
  <div class="popup__inner">
    <div>
      <div class="popup__content"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
        <h2 class="popup__heading">KrisFlyer earn rates for flights on Singapore Airlines and SilkAir</h2>
        <p>Before 20 January 2018</p>

        <table class="table-1 table-responsive">
          <thead class="hidden-mb">
            <tr>
              <th>Cabin Class</th>
              <th>Booking Class</th>
              <th>Earn Rate</th>
            </tr>
          </thead>
          <tbody>
            <td>
              <div class="data-title">Cabin Class</div><span>Suites/First Class</span>
            </td>
            <td>
              <div class="data-title">Booking Class</div>A,F
            </td>
            <td>
              <div class="data-title">Earn Rate</div>150%
            </td>
            </tr>
            <tr class="even">
              <td>
                <div class="data-title">Date</div><span>Business</span>
              </td>
              <td>
                <div class="data-title">Transaction type</div>C,D,J,U,Z
              </td>
              <td>
                <div class="data-title">Detail</div>125%
              </td>
            </tr>
            <tr class="odd">
              <td>
                <div class="data-title">Date</div><span>Premium Economy</span>
              </td>
              <td>
                <div class="data-title">Transaction type</div>P,S,T
              </td>
              <td>
                <div class="data-title">Detail</div>110%
              </td>
            </tr>
            <tr class="even">
              <td>
                <div class="data-title">Date</div><span>Economy</span>
              </td>
              <td>
                <div class="data-title">Transaction type</div>
                <p>B,E,H,L,M,W,Y
                  <p>
                    <p>N,Q</p>
                    <p>K,V</p>
              </td>
              <td>
                <div class="data-title">Detail</div>
                <p>100%</p>
                <p>50%</p>
                <p>10%</p>
              </td>
            </tr>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
</aside>
