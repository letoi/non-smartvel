<% _.each(data, function(flight, index) { %>
  <section class="booking booking--style-1 <%- index > 2 ? 'hidden' : '' %>" data-booking-item="true" data-url="<%- flight.url %>">
    <div class="blk-heading">
      <h3 class="sub-heading-1--dark">
        Booking Reference <%- flight.referenceNumber %>
      </h3>
      <a href="#" class="link-5"><em class="ico-gear"></em>Manage booking</a>
    </div>
    <h4 class="sub-heading-2--dark">
      <%- flight.from.name %> to <%- flight.to.name %>
    </h4>
    <% if(flight.delay) { %>
      <div class="alert-block checkin-alert hidden">
        <div class="inner">
          <div class="alert__icon"><em class="ico-alert">&nbsp;</em></div>
          <div class="alert__message"><%- flight.delay %></div>
        </div>
      </div>
    <% } %>

    <div class="block-2 booking-item">
      <div class="booking-info">
        <div class="booking-info-item booking-info--width-1">
          <div class="booking-desc">
            <span class="hour"><%- flight.flightNumber %></span>
            <div class="booking-content">
              <div class="preloader hidden">
                <img src="images/preloader-generic.jpg" alt="" longdesc="img-desc.html" width="16px" height="16px" />
              </div>
              <span data-aircraft="true"></span>
            </div>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-2">
          <div class="booking-desc">
            <span class="hour">
              <%- flight.from.code %> <span data-departure-time="true"></span>
            </span>
            <div class="booking-content">
              <div class="preloader hidden">
                <img src="images/preloader-generic.jpg" alt="" longdesc="img-desc.html" width="16px" height="16px" />
              </div>
              <span data-date-port-departure="true"></span>
            </div><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-2">
          <div class="booking-desc">
            <span class="hour">
              <%- flight.to.code %> <span data-arrival-time="true"></span>
            </span>
            <div class="booking-content">
              <div class="preloader hidden">
                <img src="images/preloader-generic.jpg" alt="" longdesc="img-desc.html" width="16px" height="16px" />
              </div>
              <span data-date-port-arrival="true"></span>
            </div>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-3 booking-info__button">
          <div class="booking-desc">
            <div class="booking-content" data-booking-button="true">
              <span class="loading loading--small">Loading...</span>
              <!--Spinner-->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<% }); %>
