<% var sa, san, ps, psn, psp, fz, fzn, fzp, ss, ssn, ssp, bs, bsn, na, nan; if(data) { %>
	<span class="title-legend hidden-tb show-mb">Legend</span>
	<% _.each(data.legends, function(legends, legendsIdx){ %>
		<%
			var priceLegends = legends.price;
			if(priceLegends !== "NA"){
				var indexOfPrice = priceLegends.indexOf(" ");
				var priceNote = legends.price.slice(0, indexOfPrice);
				var replacePrice = Number(priceNote.replace(',', ''));
				var newPriceFormat = replacePrice.toLocaleString(undefined, {
		          minimumFractionDigits: 2,
		          maximumFractionDigits: 2
		        });
			}
		%>
		<%
		if(legends.name === "Selected/Assigned") {
			sa = legends.code;
			san = legends.name;
		}
		if(legends.name === "Preferred Seat") {
			ps = legends.code;
			psp = priceLegends;
			psn = legends.name;
		}
		if(legends.name === "Forward Zone") {
			fz = legends.code;
			fzn = legends.name;
			fzp = priceLegends;
		}
		if(legends.name === "Standard Seat") {
			ss = legends.code;
			ssn = legends.name;
			ssp = priceLegends;
		}
		if(legends.name === "Not Available") {
			nan = legends.name;
		}
		if(legends.name === "Bassinet") {
			bsn = legends.name;
		}
		%>
	<% }); %>
	<% if(typeof san !== 'undefined') %><li><em class="seat seat-selected-border" data-code = "<%- sa %>"></em><span class="icon-annotation">Selected / Assigned</span></li>
	<% if(typeof psn !== 'undefined') %><li><em class="seat preferred-seat" data-code = "<%- ps %>"></em><span class="icon-annotation">Extra Legroom Seat<span class="icon-amount"><% if(typeof psp !== "undefined") { %> <%- psp %><% } %></span></span></li>
	<% if(typeof fzn !== 'undefined') %><li><span class="forward-zone-bg"><em class="seat forward-zone" data-code = "<%- fz %>"></em></span><span class="icon-annotation">Forward Zone Seat<span class="icon-amount"><% if(typeof fzp !== "undefined") { %><%- fzp %><% } %></span></span></li>
	<% if(typeof ssn !== 'undefined') %><li><em class="seat standard-seat" data-code = "<%- ss %>"></em><span class="icon-annotation">Standard Seat<span class="icon-amount"><% if(typeof ssp !== "undefined") { %> <%- ssp %><% } %></span></span></li>
	<% if(typeof bsn !== 'undefined') %><li><em class="ico-1-bassinet"></em><span class="icon-annotation">Bassinet</span><span data-tooltip="true" data-type="2" data-content="<p class=&quot;tooltip__text-2&quot;>768mm (L) x 298mm (W) x 158mm (H), and supports a maximum of 14kg.</p>" class="ico-1-info-round-fill" aria-label="View more information"></span></li>
	<% if(typeof nan !== 'undefined') %><li><em class="seat seat-not-available"></em><span class="icon-annotation">Not available</span></li>
<% } %>
