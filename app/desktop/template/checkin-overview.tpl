<%

 var toDate = function(epoch, format, locale) {
		var date = new Date(epoch),
		format = format || 'dd/mmm/YY',
		locale = locale || 'en'
		dow = {};

		dow.en = ['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];

		var monthNames = ["Jan","Feb", "Mar", "Apr", "May", "June ","July", "Aug", "Sept", "Oct", "Nov", "Dec"];

		var formatted = format
		.replace('D', dow[locale][date.getDay()])
		.replace('d', ("0" + date.getDate()).slice(-2))
		.replace('mmm', ((monthNames[date.getMonth()])))
		.replace('yyyy', date.getFullYear())
		.replace('yy', (''+date.getFullYear()).slice(-2))
		.replace('hh',  ("0" + date.getHours()).slice(-2))
		.replace('mn', date.getMinutes())

		return formatted
 };

%>

<form id="form-booking-1" method="post" action="" data-check-change="true" class="form-booking-1">
	<section class="booking booking--style-2">
		<% if(showBoardingPassError){ %>
		<div class="alert-block checkin-alert">
			<div class="inner">
				<div class="alert__icon"><em class="ico-alert"></em></div>
				<div class="alert__message">
				Self-printed and mobile boarding passes are not available for the following passengers(s).

				<br /><br />
				<%
					if(showBoardingPassError) {
						_.each(allPax, function(pax){
							_.each(pax.services, function(service){
								if(service.dcsStatus.boardingPassIssued == false && service.dcsStatus.checkedIn == true){ %>
									- <%- pax.paxDetails.firstName %> <%- pax.paxDetails.lastName %> <br>
								<% }
							});
						});
					}
				%>
				<br>
				<p>Please proceed to the airport for credit/debit card verfication and further assistance. The cardholder must be present to verify his/her card in person.</p>
				<p><a href="#">> View airport check-in counter opening hours</a></p>
				</div>
			</div>
		</div>
		<% } %>

		<div class="blk-heading">
	      <h3 class="sub-heading-1--dark">Booking Reference <%- recordLocator %></h3>
	      <% if(showCancelAllCheckin) { %>
	      <ul class="blk-heading-control">
	        <li class="hidden-tablet"><a href="#" class="print-btn--grey" onclick="javascript:window.print();"><em class="ico-print"><span class="ui-helper-hidden-accessible">Print</span></em></a>
	        </li>
	        <li><a href="#" class="cancel-all-flight"><em class="ico-cancel-all"><span class="ui-helper-hidden-accessible"></span></em>Cancel all check-ins</a>
	        </li>
	      </ul>
	      <% } %>
	    </div>

	  <%
	  var legCount = 0;
	  _.each(flights, function(flight) {
	    var checkedInLeg = [];
	  %>

	 <div class="block-2 booking-item" data-leg="<%- flying_info %>">
	   	<h4 class="sub-heading-3--dark<% if(flying_info > 1){ %> disabled<% } %>"><%- flying_info %>. <%= flight.origin.cityName %> to <%= flight.destination.cityName %><% if(checkinSummary) { %><em class="ico-check-thick"></em><% } %></h4>
	      <div class="booking-info-group">
	      	<div class="flights__info--group">
	  			<div data-flight-number="<%= flight.operatingAirline.flightNumber %>" data-carrier-code="<%= flight.operatingAirline.airlineCode %>" data-date="14/02/2014" data-origin="<%= flight.origin.airportCode %>" class="flights--detail">
	  				<span>Flight <%= flight.operatingAirline.airlineCode %> <%= flight.operatingAirline.flightNumber %>
	      			<em class="ico-point-d"></em><span class="loading loading--small hidden">Loading...</span></span>
	        		<div class="details hidden">
		          		<p>Aircraft type: <%= flight.aircraftType %></p>
		          		<p>Flying time: <%= flight.flyingTime %></p>
	        		</div>
		     			<% if(flight.operatedBy != null) { %>
		     			<br>
		       			<span>(OPERATED BY: <%= flight.operatedBy %>)</span>
		    			<% } %>
	    		</div>
	    		<span class="flights-type"><%= flight.cabinClass %> </span>
	  		</div>

	    	<div class="booking-info no-border">
	      		<div class="booking-info-item ">
		        	<div class="booking-desc"><span class="hour"><%= flight.origin.airportCode %>  <%= toDate(flight.scheduledDepartureDateTime, 'hh:mn') %></span><span class="country-name"><%= flight.origin.cityName %></span>
		          		<div class="booking-content"><span><%= toDate(flight.scheduledDepartureDateTime, "d mmm (D)") %>, <%= flight.origin.airportName %> Terminal <%= flight.origin.airportTerminal %></span></div>
		          		<em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
		        	</div>
	      		</div>
		      	<div class="booking-info-item ">
			        <div class="booking-desc">
			        	<span class="hour"><%= flight.destination.airportCode %>  <%= toDate(flight.scheduledArrivalDateTime, 'hh:mn') %></span>
			        	<span class="country-name"><%= flight.destination.cityName %></span>
			          	<div class="booking-content">
			          		<span><%= toDate(flight.scheduledArrivalDateTime, "d mmm (D)") %>, <%= flight.destination.airportName %> Terminal <%= flight.destination.airportTerminal %></span>

			          	</div>
			        </div>
		      	</div>
	    	</div>

		    <div class="booking-info booking-info-row">
		    	<div class="booking-content"><span>Total travel time <%= flight.totalTravelTime %></span></div>
		    </div>
			</div>

			<div class="booking-passenger">
				<a href="#" class="booking-passenger__control<% if(flying_info < 2){ %> active<% } %>">Select passengers<em class="ico-point-d"><span class="ui-helper-hidden-accessible"></span></em></a>
		        <div class="booking-passenger-content">
					<div class="booking-passenger-heading">
						<div class="custom-checkbox custom-checkbox--1">
						  <input name="booking-checkbox-all-<%- flying_info %>" id="booking-checkbox-all-<%- flying_info %>" aria-labelledby="booking-checkbox-all-<%- flying_info %>-error" type="checkbox" data-checkbox-all="true" aria-label="Select all">
						  <label for="booking-checkbox-all-<%- flying_info %>">Select all</label>
						</div>
					</div>
			    	<ul class="booking-passenger-list">
			    		<%
	  						// loop through all flights
	  						_.each(allPax, function(pax) {
	  							// loop through services of each pax
	  							_.each(pax.services, function(service){
	  								_.each(flight.flightIDs, function(flID){
	  									if(service.flightID == flID){
					                      // assign new obj for checkedin pax
					                      if(service.dcsStatus.checkedIn) {
					                        var checkedIn = {};
					                        checkedIn.paxDetails = pax.paxDetails;
					                        checkedIn.service = service;
					                        checkedInLeg.push(checkedIn);
					                      }

						   %>
				    	<li>
				          <div class="custom-checkbox custom-checkbox--1">
				            <input name="<%= service.flightID %>" id="<%= service.flightID %>" aria-labelledby="<%= service.flightID %>-error" type="checkbox" <% if(service.dcsStatus.checkedIn){ %> checked <% } %> aria-label="<%= pax.paxDetails.firstName %> <%= pax.paxDetails.lastName %>">
				            <label for="<%= service.flightID %>"><%- pax.paxDetails.title  %> <%- pax.paxDetails.firstName %> <%- pax.paxDetails.lastName %></label>
				          </div>
				      </li>
			        	<%
			        						}
										});
									});
			       		});
			       		%>
			     	</ul>
		    	</div>
			</div>

	    <%

	    // check if for the current flight there is someone checked in
			var i = 0;
	    if(checkedInLeg.length > 0) {
		%>

		<div class="booking-passenger">
			<a href="#" class="booking-passenger__control active" aria-expanded="true">Passengers checked-in (<%- checkedInLeg.length %>/<%- allPax.length %>) <em class="ico-point-d"><span class="ui-helper-hidden-accessible"></span></em></a>
			<div class="booking-passenger-content">
				<div class="booking-confirm">
				    <div class="booking-confirm-heading">
				      <p class="booking-confirm__desc">
				      Self-printed boarding passes are not available for this itinerary. Please proceed to the airport
				      to verify your credit/debit card and for further assistance.
				      <br /><br /> <a href="#">&gt; View airport check-in counter opening hours</a></p>

				      <ul class="booking-confirm-list">
				        <li><a href="./seat-map.html"><em class="ico-change-seat"><span class="ui-helper-hidden-accessible"></span></em>Change seat</a>
				        </li>
				        <li><a href="#" class="cancel-flight first-flight"><em class="ico-cancel-all"><span class="ui-helper-hidden-accessible"></span></em>Cancel check-in</a>
				        </li>
	              <%
	                // check if anyone is eligible to print boarding pass
	                var hasBoardingPass = false;
	                _.each(checkedInLeg, function(pax) {
	                  // console.log('checkedInLeg loop:', pax);
	                  _.each(pax.services, function(service){
	                    if(!hasBoardingPass && service.dcsStatus.boardingPassIssued) {
	                      hasBoardingPass = true;
	                    }
	                  });
	                });
	              %>
				        <li><a href="#" class="boarding-pass<% if(hasBoardingPass) { %> disable<% } %>"><em class="ico-get-boarding-pass"><span class="ui-helper-hidden-accessible"></span></em>Get boarding passes</a>
				        </li>
				      </ul>
				    </div>
				    <div class="booking-confirm-content">
				      <div class="table-default table--booking">
				        <div class="table-row table-row--heading">
				          <div class="table-col table-col-1">Passenger(s)</div>
				          <div class="table-col table-col-2">Selected seat(s)</div>
				          <div class="table-col table-col-3">Selected meal(s)</div>
				          <div class="table-col table-col-4">Additional information</div>
				        </div>
				        <%
                  var i = 0;
                  _.each(checkedInLeg, function(p) { %>
    						<div class="table-row">
    							<div class="table-col table-col-1">
    								  <% if((checkedInLeg[i].paxDetails.passengerType == "A") || (checkedInLeg[i].paxDetails.passengerType == "C")){ %>
    								  		<p class="text-dark"><%= checkedInLeg[i].paxDetails.title %> <%= checkedInLeg[i].paxDetails.firstName %> <%= checkedInLeg[i].paxDetails.lastName %></p>
    								  <% } else { %>

    										<% if(checkedInLeg[i].paxDetails.passengerType == "INF") { %>
    									  		<p class="text-dark"><%= checkedInLeg[i].paxDetails.firstName %> <%= checkedInLeg[i].paxDetails.lastName %><span>&nbsp;(INFANT)</span></p>
    								   	<%
    								   		 }
                         }
    								  %>
    							</div>

    							<div class="table-col table-col-2">
    								<div class="data-title">Selected seat(s)</div><span><%= checkedInLeg[i].service.seatSelected %></span>
    							</div>

							    <div class="table-col table-col-3">
  				    			<div class="data-title">Selected meal(s)</div>

  				    			<% if(checkedInLeg[i].service.mealSelected != null) { %>
  				    				<a href="#" class="link-2" data-trigger-popup=".popup--view-meal"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>View meals</a>
  				    			<% } %>
				        </div>

				  			<div class="table-col table-col-4">
				    			<div class="data-title">Additional information</div>Proceed to the check-in counter to verify your travel document.
				  			</div>
						  </div>
    						 <%
                    i++;
    							});
    						  %>
				    </div>
  				</div>
				</div>
			</div>
		</div>

	  <%
	      }
	      flying_info++;
	  %>
	</div>
    <% }); %>
    <div class="button-group-1">
      <input type="submit" name="checkin-btn" id="checkin-btn" value="Next" class="btn-1<%  if(!showCancelAllCheckin) { %> disabled<% } %>" <% if(!showCancelAllCheckin) { %>disabled=""<% } %>>
    </div>
	</section>
</form>
