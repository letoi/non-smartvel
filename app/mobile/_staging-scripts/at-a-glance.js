/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.atAGlance = function(){
	if(!$('.at-a-glance-page').length){
		return;
	}
	var global = SIA.global;
	var config = global.config;
	// var body = global.vars.body;

	// var getDrawData = function(jsonUrl,templateUrl,appendToElement){
	// 	var drawContent = function(res,apElement) {
	// 		var appendElement = $(apElement);
	// 		if(appendElement.length){
	// 			$.get(templateUrl, function (data) {
	// 				appendElement.empty();
	// 				var template = window._.template(data, {
	// 					data: res
	// 				});
	// 				$(template).appendTo(appendElement);
	// 				$('[data-accordion-wrapper]').accordion('refresh');
	// 			}, 'html');
	// 		}
	// 	};


	// 	var ajaxSuccess = function(res) {
	// 		drawContent(res,appendToElement);
	// 	};
	// 	$.ajax({
	// 			url: jsonUrl,
	// 			type: global.config.ajaxMethod,
	// 			dataType: 'json',
	// 			success: ajaxSuccess,
	// 			error: function(xhr, status) {
	// 				if(status !== 'abort') {
	// 					window.alert(L10n.flightSelect.errorGettingData);
	// 				}
	// 			}
	// 		});
	// };

	//window.alert(Modernizr.cssanimations);
	var tabWrapper = $('.tab-wrapper');
	var dialsChart = $('.dials-chart__item');
	var customiseLinkForm = $('.popup--customise-link form');
	var customiseLinkChkbs = customiseLinkForm.find('input:checkbox');
	var totalCustome = customiseLinkForm.data('totalCustome');
	var flexslider2 = $('.flexslider-2');
	var highlightSlider = $('#highlight-slider');

	// getDrawData(config.url.kfAtAGlanceJSON ,config.url.kfAtAGlanceTemplate,'[data-accordion-wrapper-content=1]');

	dialsChart.removeClass('visibility-hidden');
	customiseLinkChkbs.off('change.customiseLinkChkbs').on('change.customiseLinkChkbs',function(){
		var unCheckedChkbxs = customiseLinkForm.find('input:checkbox:not(:checked)');
		if(customiseLinkForm.find('input:checkbox:checked').length >= totalCustome){
			unCheckedChkbxs.prop('disabled', true);
			unCheckedChkbxs.parent('.custom-checkbox').addClass('disabled');
		}
		else {
			unCheckedChkbxs.prop('disabled', false);
			unCheckedChkbxs.parent('.custom-checkbox').removeClass('disabled');
		}
	}).trigger('change.customiseLinkChkbs');

	$('.dials-tab').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active',
		beforeChange: function(tab) {
			var sld = tab.filter(':not(.active)').find('.flexslider-2 .slides');
			var items = sld.find('.slide-item');
			if(sld.length){
				if(!items.width()){
					sld.find('.slick-track').width(100000);
					items.width(tabWrapper.width()).eq(0).css('display', 'none');
					sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));
				}
			}
		},
	});

	// Show all dials
	var showAllDials = function(self){
		self.addClass('animated fadeIn');
		self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn').on('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn' , function(){
			setTimeout(function(){
				dialsChart = $('.dials-chart__item', flexslider2);
				dialsChart.chart({
					startVal: 0,
					endVal: 4000,
					increment: 180 / 100,
					incrementVal: 70
				}).chart('show');
				self.removeClass('animated fadeIn');
				self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn');
			},0);
		});
	};
	// var delegateEventForToolbar = function(self){
	// 	var init = function(){
	// 		body.on('transitionend.fadeIn oTransitionEnd.fadeIn webkitTransitionEnd.fadeIn MSTransitionEnd.fadeIn', '.toolbar--language', function(){
	// 			showAllDials(self);
	// 			body.off('transitionend.fadeIn oTransitionEnd.fadeIn webkitTransitionEnd.fadeIn MSTransitionEnd.fadeIn', '.toolbar--language');
	// 		});
	// 	};
	// 	init();
	// };

	flexslider2.each(function(idx){
		var self = $(this),
				slideEl = self.find('.slides');

		slideEl
			.on('init', function() {
				if(!idx){
					showAllDials(self);
				}
			})
			.on('afterChange', function() {
				self.find('.slide-item').eq(0).css('display', '');
			});

		self.find('.slides').slick({
			siaCustomisations: true,
			dots: true,
			speed: 300,
			draggable: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			accessibility: false,
			arrows: false,
			useCSS: false/*,
			onInit: function(){
				if(!idx){
					// if(body.data('showTBar')){
					// 	delegateEventForToolbar(self);
					// }
					// else{
					showAllDials(self);
					// }
				}
			},
			onAfterChange: function(){
				self.find('.slide-item').eq(0).css('display', '');
			}*/
		});
	});

	// Init slick slide
	highlightSlider.find('.slides').slick({
		siaCustomisations: true,
		dots: true,
		speed: 300,
		draggable: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		accessibility: false,
		arrows: false
	});

	// This function uses render for the Your bookings block.
	var renderYourBookings = function(){
		var dataAccordionWrapperContent = $('[data-accordion-wrapper-content="1"]');
		if (dataAccordionWrapperContent.length > 1) {
			dataAccordionWrapperContent = $(dataAccordionWrapperContent[1]);
		}
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.children('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfUpcomingFlights[idx];
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			dataAccordionWrapperContent.children('[data-accordion]').remove();
			$.get(config.url.kfAtAGlanceTemplate, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfUpcomingFlights
				});
				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};

	//this function used render for the Booking to be confirm block.
	var renderYourConfirmBookings = function(){
		var dataAccordionWrapperContent = $('.block--confirm-booking [data-accordion-wrapper-content="1"]');
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.find('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfBookingConfirm[idx];
									res.bookingConfirmUrl = dataAccordionWrapperContent.data('payment-url');
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			$.get(config.url.kfAtAGlanceTemplateSf, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfBookingConfirm
				});

				dataAccordionWrapperContent.children().remove();

				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};
	renderYourBookings();
	renderYourConfirmBookings();

	var renderInfoBox = function(){
		var infoBox = $('.info-box');

		infoBox.each(function(){
			var self = $(this);
			var infoBoxButton = $('.info__button', self);

			infoBoxButton.off('click').on('click', function(){
				self.remove();
			});
		});
	};

	renderInfoBox();
	
};
