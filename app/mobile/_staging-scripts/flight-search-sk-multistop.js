/**
 * @name SIA
 * @description Define global flight economy functions
 * @version 1.0
 */
$('#selectflightJson').val(JSON.stringify(globalJson.threeStops));

SIA.flightEconomy = function() {
    var saar5 = {
	   l4: {
		  sk: {
			 chooseflight: {
				add25kg: "Additional 25kg",
				add40kg: "Additional 40kg",
				add50kg: "Additional 50kg",
				additional: "for an additional",
				additional1to23: "Additional 1 pieces (up to 23kg each)",
				additional1to32: "???jslocalisation.saar5.l.sk.chooseflight.additional1to32???",
				additional2to23: "Additional 2 pieces (up to 23kg each)                                                   ",
				additional2to32: "Additional 2 pieces (up to 32kg each)",
				additional20: "Additional 20kg",
				additional25: "Additional 25%",
				additional30: "Additional 30kg",
				additional35: "Additional 35kg",
				additionalBaggagePerPax: "???jslocalisation.saar5.l.sk.chooseflight.additionalBaggagePerPax???",
				adlt: "Adult",
				adtnl: "Additional",
				advantageUpgradeFrom: "???jslocalisation.saar5.l.sk.chooseflight.advantageUpgradeFrom???",
				airportTax: "Airport/Government taxes",
				arrTime: "Arrival time",
				asCharged: "As charged",
				baggage: "Baggage",
				baggageAllowance: "Check-in baggage allowance",
				baggageOne: "Baggage^",
				bagggeAllwnce: "Baggage allowance",
				benefits: "PPS Club member benefits",
				blanket: "A warm woven fleece blanket",
				book: "Book",
				booking: "Booking change",
				business: "Business",
				businessCondition: "As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each Business Class fare family.",
				businessFlexi: "Business Flexi",
				businessLite: "Business Lite",
				businessStandard: "Business Standard",
				calendar: "calendar",
				cancellation: "Cancellation",
				carrier: "Operating carrier",
				carrierSurcharge: "Carrier surcharges",
				checkedinbaggageallowance: "Checked-in baggage allowance",
				checkin: "Enjoy priority check-in, boarding and baggage handling.",
				chld: "Child",
				choice: "Greater choices",
				chrgble: "Chargeable",
				close: "Close",
				comfort: "Greater comfort",
				complimentary: "Complimentary",
				complimentarySeatMsg1: "???jslocalisation.saar5.l.sk.chooseflight.complimentarySeatMsg1???",
				complimentarySeatMsg2: "???jslocalisation.saar5.l.sk.chooseflight.complimentarySeatMsg2???",
				complimentarySeatMsg3: "???jslocalisation.saar5.l.sk.chooseflight.complimentarySeatMsg3???",
				conditions: "Fare conditions",
				cook: "Savour more meal choices from our Premium Economy Book the Cook menu.",
				cookOne: "Cook",
				costBreakdown: "Cost breakdown by passengers",
				dayFares: "-day fares",
				depFlight: "Selected departure flight",
				depTime: "Departure time",
				departing: "Departing",
				disabled: "disabled",
				discounted: "Discounted",
				duration: "Travel duration",
				earnFiftyMiles: "???jslocalisation.saar5.l.sk.chooseflight.earnFiftyMiles???",
				earnMiles: "Earn KrisFlyer miles",
				earnMilesOne: "Earn KrisFlyer miles^",
				earnMoreMiles: "???jslocalisation.saar5.l.sk.chooseflight.earnMoreMiles???",
				economy: "Economy",
				economyGood: "Economy good",
				economySuperSaver: "Economy Super Saver",
				economyp1: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy seat selection and baggage allowance privileges across all our fare types on Economy Class, according to your membership tier.",
				economyp2: "Complimentary advance seat selection privileges                                          ",
				economyp3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
				economyp4: "Complimentary additional checked-in baggage allowance privileges",
				economyp5: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				economyp6: "Complimentary advance seat selection privileges",
				economyp7: "Seat selection privileges apply to the principal member only. The selected seat will be yours even if your Elite Gold membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
				economyp8: "Complimentary additional checked-in baggage allowance privileges",
				economyp9: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				economyp10: "Complimentary advance seat selection privileges",
				economyp11: "Seat selection privileges apply to the principal member only. The selected seat will be yours even if your Elite Silver membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
				economyp12: "Complimentary additional checked-in baggage allowance privileges",
				economyp13: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				economyp14: "Complimentary additional checked-in baggage allowance privileges",
				economyp15: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				economypCommon: "???jslocalisation.saar5.l.sk.chooseflight.economypCommon???",
				editSearch: "Edit search",
				eliteGold: "KrisFlyer Elite Gold",
				eliteGoldBenefits: "",
				eliteSilver: "KrisFlyer Elite Silver",
				eliteSilverBenefits: "KrisFlyer Elite Silver member benefits",
				enabled: "enabled",
				enjoy: ", and you'll enjoy:",
				extralegroomseat: "Extra Legroom Seat selection                                           ",
				fare: "Fare",
				fareCondition: "Fare condition",
				fareCondtns: "fare conditions",
				fareMsg1: "You are now on a fare table. To navigate through the fare selection, press tab. To quickly move to the other available flights, use the up or down arrow keys. To select a fare, press enter. To go directly to the Booking Summary Panel, press control Y. ",
				fareMsg2: "Each of the flight segments you've selected comes with its own fare conditions. When you mix fare types, whether within the same cabin class or across different cabin classes, fare conditions for cancellation, booking change and no show will follow the more restrictive fare type.",
				fareMsg3: "^ These fare conditions are only applicable to Singapore Airlines and SilkAir flights. Refer to the full fare rules and conditions for more information when flying with partner airlines.",
				fareMsg4: "Fares are not guaranteed until payment is completed",
				fareMsg5: "View fare conditions when flying with our partner airlines",
				fareMsg6: "View fare conditions when flying with Scoot",
				fareMsg7: "SQ 2627 is a flight operated by Scandinavian Airlines. Fare conditions for baggage allowance, seat selection, earning of miles and upgrading with miles indicated below are only for Singapore Airlines and SilkAir flights.",
				fareMsg8: "View conditions for flights operated by partner airlines",
				fareMsg9: "Total fare includes discounts, taxes and surcharges",
				fiftyPcnt: "???jslocalisation.saar5.l.sk.chooseflight.fiftyPcnt???",
				filters: "Filters",
				find: "Find out more",
				first: "First",
				firstCondition: "As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each First Class fare family.",
				firstFlexi: "First Flexi",
				five: "5",
				fiveKg: "???jslocalisation.saar5.l.sk.chooseflight.fiveKg???",
				flexi: "Flexi",
				flexibility: "???jslocalisation.saar5.l.sk.chooseflight.flexibility???",
				flightMsg1: "Flight",
				flightMsg2: ", Departing from",
				flightMsg3: "on ",
				flightMsg4: "at ",
				flightMsg5: ". Arriving in ",
				flightMsg6: "Terminal",
				flightMsg7: ", with layover time: ",
				flightMsg8: ", total flight duration: ",
				flightMsg9: "Flights",
				flights: "flights",
				flyng: "Flying",
				forallodpairs: "For all OD pairs",
				forwardZone: "Forward Zone selection",
				frwdzoneseatselection: "Forward Zone Seat selection",
				fullFare: "Full fare conditions",
				fullFareCondition: "Full fare condition",
				fullFareRules: "???jslocalisation.saar5.l.sk.chooseflight.fullFareRules???",
				fxi: "Flexi",
				handling: "More privileges, with priority check-in, boarding and baggage handling",
				hideFilter: "Hide filters",
				hold: "Hold this fare for",
				hotMeal: "Complimentary hot meal on board",
				inbound: "Inbound fares",
				inflight: "Your Premium Economy Class inflight experience",
				infnt: "Infant",
				international: "Int'l",
				isNotOffered: "???jslocalisation.saar5.l.sk.chooseflight.isNotOffered???",
				kfBusiness: "PPS Club / KrisFlyer member privileges on Business Class",
				kfFirst: "PPS Club / KrisFlyer member privileges on First Class",
				kfMember: "PPS Club / KrisFlyer member privileges on Economy Class",
				kfPremium: "KrisFlyer member benefits for Premium Economy",
				kfbenefitseconomy: "Economy",
				kfgold: "KRISFLYER GOLD",
				kfsilver: "KRISFLYER SILVER",
				kilos: "???jslocalisation.saar5.l.sk.chooseflight.kilos???",
				krisflyer: "KrisFlyer",
				krisflyerBenefits: "KrisFlyer member benefits",
				layOver: "Layover time: ",
				lessDetails: "Less details",
				lite: "Lite",
				load: "Load more flights",
				logo: "Singapore Airlines Group",
				lowestFare: "Lowest available fare",
				lte: "Lite",
				menu: "menu",
				miles: "miles",
				milesRequired: "???jslocalisation.saar5.l.sk.chooseflight.milesRequired???",
				milesRequiredMsg: "???jslocalisation.saar5.l.sk.chooseflight.milesRequiredMsg???",
				more: "More",
				moreChoice: "More choices, as you can pre-order from our",
				moreComfort: "More comfort, with a greater seat width and more legroom",
				moreDetails: "More details",
				moreFlights: "more flights",
				moreInfo: "???saar5.l.kf.digital.wallet.moreinfo???",
				newItinerary: "New Itinerary",
				noResult: "We are unable to find recommendations for your search. Please change your search criteria and resubmit the search.",
				noShow: "No show",
				non_Stop: "Non-stop",
				notAllowed: "Not allowed",
				offset: "Offset by previously paid amount",
				oldItinerary: "Old Itinerary",
				oneAdult: "???jslocalisation.saar5.l.sk.chooseflight.oneAdult???",
				oneStop: "1-stop",
				oneWay: "One way",
				one_Stop: "One-stop",
				operatedBy: "???jslocalisation.saar5.l.sk.chooseflight.operatedBy???",
				outbound: "Outbound fares",
				partner: "Partner / codeshare airlines",
				paySeat: "???jslocalisation.saar5.l.sk.chooseflight.paySeat???",
				paySeatSelection: "???jslocalisation.saar5.l.sk.chooseflight.paySeatSelection???",
				perPassenger: "Per passenger",
				peyp1: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy seat selection and baggage allowance privileges across our fare types on Premium Economy Class, according to your membership tier. ",
				peyp2: "Complimentary advance seat selection privileges",
				peyp3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
				peyp4: "Complimentary additional checked-in baggage allowance privileges",
				peyp5: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				peyp6: "Complimentary additional checked-in baggage allowance privileges",
				peyp7: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				peyp8: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy privileges on First Class / Suites, according to your membership tier.",
				peyp9: "Complimentary additional checked-in baggage allowance privileges",
				peyp10: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				peyp11: "Complimentary additional checked-in baggage allowance privileges",
				peyp12: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
				ppsClub: "PPS Club",
				ppsCondition: "As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each Economy Class fare family.",
				preferredSeat: "Preferred Seat selection",
				premiumEconomy: "Premium Economy",
				previouslyPaid: "Previously paid",
				priority: "Priority treatment",
				privileges: "View PPS Club / KrisFlyer privileges",
				promo: "???jslocalisation.saar5.l.sk.chooseflight.promo???",
				rebookingFee: "Rebooking fee",
				recFlight: "Recommended flight for you",
				reset: "Reset filter",
				returning: "Returning",
				rooms: "Rooms and rates",
				saveBooking: "???jslocalisation.saar5.l.sk.chooseflight.saveBooking???",
				saveUsd: "???jslocalisation.saar5.l.sk.chooseflight.saveUsd???",
				saverUpgradeFrom: "???jslocalisation.saar5.l.sk.chooseflight.saverUpgradeFrom???",
				sc: "SC",
				scoot: "You'll be flying from Singapore to Seoul on Scoot, the low-cost carrier of the Singapore Airlines Group. On your flight, you'll enjoy:",
				scootBenefits: "About scoot benefits",
				scootConditions: "You'll also enjoy the same fare conditions on Scoot that you've selected for your Singapore Airlines flight. Other services on Scoot are not included in your fare, and may be chargeable. When you fly on Scoot, their Conditions of Carriage apply.",
				seat: "Seat selection at booking",
				seatLeft: "seats left",
				seatOne: "Seat selection at booking^",
				seats: "???jslocalisation.saar5.l.sk.chooseflight.seats???",
				secureFare: "Secure Fare",
				secureOne: "Fare secure is not applicable to one ",
				secureTwo: "or more of your selected flights",
				see: "See",
				selected: "Selected",
				seven: "7",
				sgd: "???jslocalisation.saar5.l.sk.chooseflight.sgd???",
				sgd305: "???jslocalisation.saar5.l.sk.chooseflight.sgd305???",
				show: "Show all flights",
				showFilter: "Show filters",
				showing: "Showing",
				si: "SI",
				sit: "Sit back. All the way back, with a generous 38' seat pitch and 19.5' seat width.",
				snooze: "Snooze Kit",
				sq: "SQ",
				standard: "Standard",
				standardSeat: "Standard Seat selection",
				std: "Standard",
				stdseatselection: "Standard Seat selection",
				stopOver: "Stopover",
				subTotal: "Subtotal",
				suites: "???jslocalisation.saar5.l.sk.chooseflight.suites???",
				summary: "Here's a summary of your fare conditions",
				superSaver: "???jslocalisation.saar5.l.sk.chooseflight.superSaver???",
				tabKey: "Please use the tab key to move through this application",
				ten: "10",
				terminal: "Terminal",
				termsAndConditions: "Terms and conditions",
				termsAndConditionsFour: "If your PPS Club or Krisflyer Elite Gold membership expires on or before the counter check-in date, you and your travelling party will no longer enjoy that expired tier's complimentary baggage allowance.",
				termsAndConditionsOne: "Complimentary seat selection privileges for expired memberships",
				termsAndConditionsThree: "Complimentary additional baggage privileges for expired memberships",
				termsAndConditionsTwo: "If your PPS Club / Krisflyer membership expires on or before the date of seat selection during booking,your complimentary seat selection privileges will remain unchanged.",
				three: "3",
				to: "???jslocalisation.saar5.l.sk.chooseflight.to???",
				toolTipMsg: "Fly to more places around the world with the Singapore Airlines Group, which includes Singapore Airlines, SilkAir and Scoot.",
				totalFare: "Total fare",
				totalTravel: "Total travel time:",
				tr: "TR",
				travellingtofromanyotherdest: "Travelling to/from any other destination",
				travellingtofromother: "Travelling to/from any other destination",
				travellingtofromusa: "Travelling to/from the USA",
				twoStop: "2-stop",
				two_Stop: "Two-stop",
				upgrade: "Upgrade with miles",
				upgradeAward: "???jslocalisation.saar5.l.sk.chooseflight.upgradeAward???",
				upgradeOne: "Upgrade with miles^",
				upgradePremium: "Upgrade to Premium Economy and enjoy:",
				upgradeTo: "Upgrade to",
				upgrde: "Upgrade",
				withTaxes: "(with taxes and surcharges)"
			 }
		  }
	   }
    };

  var global = SIA.global,
    config = global.config,
    body = $('body'),
    container = $('#container'),
    isChooseDate = null,
    priceDateOutbound = 0,
    clickedBtn,
    miniFareData;
  globalJson.selectedFlightsData = [];
  globalJson.MulticityBookingSummaryData = [];
  globalJson.dataDepart = {};
  globalJson.dataReturn = {};
  /*  globalJson.MulticityData=[];*/
  var inboundSegmentChanged = false;
  var outboundSegmentChanged = false;
  var outboundSegmentChangedKms = false;
  var slideshowpremiumeconomy = function(minFareMap) {
    if (!jQuery.isEmptyObject(minFareMap) && $('#rebookVal').val() != 'true') {
	 //Upsell block made hidden for all scenarios
	 // $(".your-flight-item").removeClass("hidden");
	 if ($('body').hasClass('fs-economy')) {
	   var totalDesktopSlide = 1,
		totalLandscapeSlide = 1;
	   $('[data-slideshow-premium-economy]').each(function() {
		var slider = $(this);
		slider.find('img').each(function() {
		  var self = $(this),
		    newImg = new Image();
		  newImg.onload = function() {
		    slider.css('visibility', 'visible');
		    slider.find('.slides').slick({
			 siaCustomisations: true,
			 dots: false,
			 speed: 300,
			 draggable: true,
			 slidesToShow: totalDesktopSlide,
			 slidesToScroll: totalLandscapeSlide,
			 accessibility: false,
			 arrows: true,
		    });
		  };
		  newImg.src = self.attr('src');
		});
	   });
	 }
    } else {
	 $(".your-flight-item").addClass("hidden");
    }
  };
  var initSlider = function(sliderEl, btnNext, btnPrev) {
    var wrapperSlides = $(sliderEl).find('.slides');

    if (wrapperSlides.is('.slick-initialized')) {
	 wrapperSlides.slick('unslick');
    }

    wrapperSlides.slick({
	 accessibility: true,
	 dots: false,
	 draggable: false,
	 infinite: true,
	 speed: 300,
	 slidesToShow: 7,
	 slidesToScroll: 7,
	 prevArrow: btnPrev,
	 nextArrow: btnNext,
    })

    setTimeout(function() {
	 $('.slick-slide').attr("tabindex", -1);
	 $('.slick-active,.slick-arrow').attr("tabindex", 0);
    }, 500);

    selectItemSlider(wrapperSlides);

    if (!sliderEl.data('slider-outbound')) {
	 sliderEl.find('.slide-item').each(function() {
	   var selfPrice = parseFloat($(this).find('.large-price').text()),
		priceAfterSelect = selfPrice - priceDateOutbound;

	   $(this).find('.large-price').text('+ ' + (priceAfterSelect > 0 ? priceAfterSelect : 0));
	 })
    }
  }

  var selectItemSlider = function(wrapperSlides) {
    var slideItem = wrapperSlides.find('.slick-slide');
    slideItem.each(function() {
	 if (isChooseDate) {
	   $(this).data('date') === isChooseDate && $(this).addClass('selected');
	 }
	 $(this).off('click.selectSlideItem').on('click.selectSlideItem', function(e, isFirst) {
	   e.preventDefault();
	   if ($('body').hasClass('sk-ut-workflow') && !isFirst) {
		return false;
	   }
	   var isOutBound = $(this).closest('[data-fs-slider]').data('slider-outbound');
	   isOutBound && $(this).closest('[data-fs-slider]').attr('data-selected-date', $(this).data('date'));
	   slideItem.removeClass('selected');
	   $(this).addClass('selected');
	   isChooseDate = $(this).data('date');

	   if (isOutBound) {
		priceDateOutbound = parseFloat($(this).find('.large-price').text());
	   }
	 });
    })
  }

  var renderPopupBenefit = function(popupEl, callback) {
    var cabinclss = null;
    var cabin = '';
    $.get(global.config.url.fsEconomyBenefitTemplate, function(data) {

	 var ar = popupEl.find(".popup__content");

	 if ($(".column-trigger-animation.active").find(".text-head").text().trim() !== "") {
	   var cabinclss = $(".column-trigger-animation.active").find(".head-col:first").text().trim();

	   if (cabinclss === 'Premium Economy') {
		cabin = "S";
	   }
	   if (cabinclss === 'First/Suites') {
		cabin = "F";
	   }
	   if (cabinclss === "Economy") {
		cabin = "Y";
	   }
	   if (cabinclss === "Business") {
		cabin = "J";
	   }
	 } else {
	   var cabinclss = $(".column-trigger-animation-1.active").find(".head-col:first").text().trim();

	   if (cabinclss === 'Premium Economy') {
		cabin = "S";
	   }
	   if (cabinclss === 'First/Suites') {
		cabin = "F";
	   }
	   if (cabinclss === "Economy") {
		cabin = "Y";
	   }
	   if (cabinclss === "Business") {
		cabin = "J";
	   }
	 }

	 var content = popupEl.find('.popup__content');
	 var template = window._.template(data, {
	   data: data,
	   cabin: cabin,
	   labels: saar5.l4.sk.chooseflight

	 });
	 $(template).prependTo(content.empty());
	 // reinit js
	 SIA.initTabMenu();
	 SIA.multiTabsWithLongText()

	 if (callback) {
	   callback();
	 }
    });
  }

  var renderPopupPremiumBenefit = function(popupEl, callback) {
    var cabinclss = null;
    var cabin = '';
    $.get(global.config.url.fsEconomyPremiumBenefitTemplate, function(data) {

	 var ar = popupEl.find(".popup__content");

	 if ($(".column-trigger-animation.active").find(".text-head").text().trim() !== "") {
	   var cabinclss = $(".column-trigger-animation.active").find(".head-col:first").text().trim();

	   if (cabinclss === 'Premium Economy') {
		cabin = "S";
	   }
	   if (cabinclss === 'First/Suites') {
		cabin = "F";
	   }
	 } else {
	   var cabinclss = $(".column-trigger-animation-1.active").find(".head-col:first").text().trim();

	   if (cabinclss === 'Premium Economy') {
		cabin = "S";
	   }
	   if (cabinclss === 'First/Suites') {
		cabin = "F";
	   }
	 }
	 var content = popupEl.find('.popup__content');

	 var template = window._.template(data, {
	   data: data,
	   cabin: cabin,
	   labels: saar5.l4.sk.chooseflight
	 });
	 $(template).prependTo(content.empty());
	 // reinit js
	 SIA.initTabMenu();
	 SIA.multiTabsWithLongText()

	 if (callback) {
	   callback();
	 }
    });
  }

  var renderPopupTableFlight = function(content, popupEl, callback) {
    popupEl.find('.popup__content').children().remove();
    content.clone().first().appendTo(popupEl.find('.popup__content'));
    initPopup();
    popupEl.find('.select-fare-block').removeClass('block-show-popup-mobile');
    if(SIA.accordion) {
	 SIA.accordion();
    }
    if($('[data-tooltip]')) {
	 $('[data-tooltip]').kTooltip();
    }
    if (callback) {
	 callback();
    }
  }

  var initPopup = function(data) {
    var triggerPopup = $('[data-trigger-popup]');
    var cb = null;
    triggerPopup.each(function() {
	 var self = $(this);

	 if (typeof self.data('trigger-popup') === 'boolean') {
	   return;
	 }

	 var popup = $(self.data('trigger-popup'));

	 if (!popup.data('Popup')) {
	   popup.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close], .cancel',

		afterHide: function() {
		  container.css('padding-right', '');
            $('.popup:not(.hidden)').length ? body.css('overflow', 'hidden') :  body.css('overflow', '');
		  var ua = navigator.userAgent.toLowerCase();
		  if (ua.indexOf('safari') != -1) {
		    if (ua.indexOf('chrome') > -1) {}
		    	else {
			 body.attr('style', function(i, s) {
                  s && s.replace('overflow:hidden !important;','');
                });
		    }
		  }
		}
	   });
	 }
	 self.off('click.showPopup').on('click.showPopup', function(e) {

	   if ($(".column-trigger-animation.active").find(".text-head").text().trim() !== "") {
		var cb = $(".column-trigger-animation.active").find(".head-col:first").text().trim();
	   } else {
		var cb = $(".column-trigger-animation-1.active").find(".head-col:first").text().trim();
	   }
	   e.preventDefault();
	   var jsonURL = self.data('flight-json-url');
	   if (!self.hasClass('disabled')) {
		// render popup benefit detail
		if (self.data('trigger-popup') === '.popup-view-benefit--krisflyer') {

		  if (cb === 'Economy' || cb === 'Business') {
		    renderPopupBenefit(popup, function() {
			 popup.Popup('show');
			 $(window).trigger('resize');
		    });
		  }
		} else if (self.data('trigger-popup') === '.popup-view-premium-benefit--krisflyer') {

		  if (cb === 'Premium Economy' || cb === 'First/Suites') {
		    renderPopupPremiumBenefit(popup, function() {
			 popup.Popup('show');
			 $(window).trigger('resize');
		    });
		  }
		} else if (self.data('trigger-popup') === '.popup-view-partner-airlines') {
		  popup.Popup('show');
		} else if (self.data('trigger-popup') === '.popup-view-our-partner-airlines') {
		  popup.Popup('show');
		} else if (self.data('trigger-popup') === '.popup-table-flight') {
		  var content = $('.'+ self.data('content-popup'));

		  if($('body').hasClass('sk-ut-workflow') && $(self).hasClass('economy-flight--pey')) {
		    return false;
		  }

		  renderPopupTableFlight(content, popup, function(){
		    if(!$(self).hasClass('not-available')){
			 popup.Popup('show');
			 $(window).trigger('resize');
		    }
		  });
		}
	   }
	 });

    });
  };

  var showHideFilters = function() {
    var fsFilters = $('.flight-search-filter-economy');

    fsFilters.each(function() {
	 var linkShow = $(this).find('.link-show'),
	   linkHide = $(this).find('.link-hide'),
	   content = $(this).find('.content');

	 linkShow.off('click').on('click', function(e) {
	   e.preventDefault();
	   if (!$(this).parent().is('.active')) {
		$(this).parent().addClass('active')
		linkShow.hide();
		content.fadeIn();
	   }
	 });
	 linkHide.off('click', 'a').on('click', 'a', function(e) {
	   e.preventDefault();
	   content.hide();
	   linkShow.fadeIn();
	   $(this).closest('.flight-search-filter-economy').removeClass('active');
	 });
    })
  };

  var showMoreLessDetails = function() {
    var btnMore = $('[data-more-details-table]'),
	 btnLess = $('[data-less-details-table]'),
	 triggerAnimation = $('[data-trigger-animation]'),
	 wrapFlights = $('[data-wrap-flight]'),
	 fareBlocks = $('[data-hidden-recommended]');

    // set data-height for wrap flight
    wrapFlights.each(function() {
	 var flightLegItem = $(this).find('.flight-result-leg'),
	   wrapFlightHeight = 0;

	 flightLegItem.each(function() {
	   wrapFlightHeight += $(this).outerHeight();
	 });

	 $(this).attr('data-wrap-flight-height', wrapFlightHeight);
    });

    btnMore.each(function() {
	 $(this).off('click.showMore').on('click.showMore', function(e) {
	   e.preventDefault();
	   var flightItem = $(this).closest('[data-flight-item]'),
		controlFlight = flightItem.find('.control-flight-station'),
		wrapFlight = flightItem.find('[data-wrap-flight]'),
		controlFlightHeight = wrapFlight.siblings('.control-flight-station').outerHeight() || 84,
		btnLess = wrapFlight.find('[data-less-details-table]');
	   wrapFlightHeight = wrapFlight.data('wrap-flight-height');

	   if (wrapFlight) {
		flightItem.addClass('active');
		wrapFlight.css({
		  'height': wrapFlightHeight - controlFlightHeight + 'px',
		  '-webkit-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
		  '-moz-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
		  '-ms-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
		  '-o-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
		  'transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)'
		})
		btnLess.attr('tabindex', 0);
	   }
	 });
    });

    btnLess.each(function() {
	 $(this).off('click.showLess').on('click.showLess', function(e) {
	   e.preventDefault();
	   var flightItem = $(this).closest('[data-flight-item]'),
		controlFlight = flightItem.find('.control-flight-station'),
		wrapFlight = flightItem.find('[data-wrap-flight]');

	   if (wrapFlight) {
		flightItem.removeClass('active');
		wrapFlight.css({
		  'height': '10px',
		  '-webkit-transform': 'translate3d(0)',
		  '-moz-transform': 'translate3d(0)',
		  '-ms-transform': 'translate3d(0)',
		  '-o-transform': 'translate3d(0)',
		  'transform': 'translate3d(0)'
		})
		$(this).attr('tabindex', -1);
	   }
	 })
    });
  }


  /* var triggerBSP = function(){
		var onLoad=true;
		 var BSPUrl;
		 var data={};
		 var rebookVal = $('#rebookVal').val();
		 if($('body').hasClass('flight-select-page')){
		  if(rebookVal=='true') {
			  BSPUrl =false ? config.url.cibFlightSelectOnChange : config.url.atcFlightSelect;
			  BSPUrl += onLoad;

			  var data = {};
			  var selectedFlightIdDetails;
			  if($("#selectedFlightIdDetails").val()) {
				  selectedFlightIdDetails = $('#selectedFlightIdDetails').val().split(';');
			  }
			  if(selectedFlightIdDetails && selectedFlightIdDetails.length > 0) {
				  $.each(selectedFlightIdDetails,function(i){
					  data['selectedFlightIdDetails['+i+']'] = selectedFlightIdDetails[i];
				  });
			  }
		  }
		  else {
			  BSPUrl =onLoad ? config.url.mpcibFlightSelectOnChange : config.url.mpcibFlightSelect;
			 BSPUrl += '&CIB=true';

			 var data = {
						"selectedFlightIdDetails" : $('#selectedFlightIdDetails').val()
					};
		  }

		 }else if(!$('body').hasClass('flight-select-page')) {
		 $('.trigger-popup-edit-search').addClass('hidden');
			if( typeof($('#flowIndicator').val()) !== 'undefined' ){
				var flow = $("#flowIndicator").val();
				if(flow ==='ORB'){
					BSPUrl= config.url.bookingSummaryJSON;
					BSPUrl += "&ORB=true";
					if( typeof($('#initiator').val()) !== 'undefined' ){
							data = {
								"id" : $('#initiator').val()
							};
					}
				}else{
					BSPUrl = config.url.bookingSummarypanel;
					BSPUrl += '&CIB=true';
					if( typeof($('#initiator').val()) !== 'undefined' ){
						var usExceptionForInsurance = "false";
						if(typeof( $('#usExceptionForInsurance').attr('data-excludedstates') ) !== 'undefined' && $('#usExceptionForInsurance').attr('data-excludedstates') == 'allow'){
							usExceptionForInsurance = "true";
						}
						data = {
							"id" : $('#initiator').val(),
							 "usExceptionForInsurance" : usExceptionForInsurance,
						};
					}



				}

			}

		}
			 var salt = window.name;
				if(salt == ""){
					salt = 'DUPLICATE_SALT';
				}
				$('#btn-secure').addClass('hidden');
				$('#btn-secure-Disabled').addClass('hidden');
			 $.ajax({
			   beforeSend: function(){
				$('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
			   },
			   url: BSPUrl,
			   type: SIA.global.config.ajaxMethod,
			   dataType: 'json',
			   data:data,
			   async: false,
			   cache: false,
			   xhr: function () {
				var xhr = $.ajaxSettings.xhr();
				xhr.onprogress = function (e) {
				    if (e.lengthComputable) {
					 $('[data-booking-summary-panel]').find('.bsp-animate').css({
					   width: (e.loaded/e.total) * 100 + '%'
					 });
				    }
				};
				xhr.upload.onprogress = function (e) {
				    if (e.lengthComputable) {
					 $('[data-booking-summary-panel]').find('.bsp-animate').css({
					   width: (e.loaded/e.total) * 100 + '%'
					 });
				    }
				};
				return xhr;
			   },
			   success: function(reponse) {
				onLoad=false;
				if(reponse){
						currecyCode = reponse.bookingSummary.currency+' ';
						precision = reponse.bookingSummary.precision;
						tttAmount = reponse.bookingSummary.tttAmount;
					}
				/*if(((typeof(reponse["tttEnabled"]) != "undefined") && reponse["tttEnabled"]) === true ){
						// $('#submitPaxDetailsEvent1').remove();
						$('#btn-secure').removeClass('hidden');
						$('.secureFareText').removeClass('hidden');
						$('#btn-secure').prop('disabled', false).removeClass('disabled');
						$('#btn-secure-Disabled').addClass('hidden');
						$('.secureFareText').removeClass('hidden');
						$('.secureFareText').html(saar5.l4.sk.chooseflight.hold+' '+currecyCode + formatNumber(tttAmount,precision));

					}else{
						if($('#btn-secure').length) {
						$('#btn-secure').addClass('hidden');
						$('#btn-secure-Disabled').removeClass('hidden');
						$('.secureFareText').addClass('hidden');
						$('#btn-secure-Disabled').kTooltip();
						}
					}*/
  /*  onLoad = false;
				var data1 = reponse;
				var data2 = reponse.bookingSummary;
						var cookieBSP = {};
						var priceData = {};

						if(getCookie('bspData') !== '') {
							cookieBSP = $.parseJSON(getCookie('bspData'));
						}

						if(getCookie('priceData') !== '') {
							priceData = $.parseJSON(getCookie('priceData'));
						}

						if(cookieBSP && $('body').hasClass('sk-ut-workflow') && !$('body').hasClass('fs-economy')) {
							dataDepart = cookieBSP.depart;
							dataReturn = cookieBSP.return1;
						}
						global.vars.bsresponse=reponse;

				   },
			   complete: function(){
				$('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
				setTimeout(function(){
				  $('[data-booking-summary-panel]').find('.bsp-animate').css({
				    width: 0
				  });
				}, 100)
				var selectedFlight=$('.fare-summary-group').find('[name^=fare-family-]');

			   }
			 });
		};
  */
  var selectFlightAnimation = function() {
    var triggerAnimation = $('[data-trigger-animation]');
    //triggerAnimation.removeAttr('data-trigger-popup');

    triggerAnimation.each(function() {
	 $(this).off('click.toggleAnimation, keydown.toggleAnimation').on('click.toggleAnimation, keydown.toggleAnimation', function(e, isOneAnimation) {
	   $(this).closest(".flight-list-item").find(".title-4--blue").html("Your " + $(this).find(".head-col:first").text() + " Class inflight experience");
	   var parentRecommended = $(this).parent(),
		flightItem = parentRecommended.find('[data-flight-item]'),
		wrapFlight = flightItem.find('[data-wrap-flight]'),
		btnMore = flightItem.find('[data-more-details-table]'),
		flightStation = flightItem.find('.flight-station'),
		hiddenRecommended = parentRecommended.siblings('[data-hidden-recommended]'),
		hiddenRecommended1 = parentRecommended.siblings('[data-hidden-recommended-1]'),
		isOpen = $(this).is('.active'),
		hiddenRecommendedHeight = hiddenRecommended.data('fare-block-height'),
		colIdx = $(this).data('trigger-animation'),
		selfTable = $('[data-col-index="' + colIdx + '"]'),
		code = e.keyCode || e.which;

	   if ($('body').hasClass('sk-ut-workflow') && $(this).hasClass('economy-flight--pey')) {
		return false;
	   }

	   triggerAnimation.not($(this)).each(function() {
		if ($(this).is('.active')) {
		  $(this).trigger('click', true);
		}
	   })

	   if (isOneAnimation || code === 1 || code === 13) {
		// reset animation
		if (wrapFlight) {
		  flightItem.removeClass('active');
		  wrapFlight.css({
		    'height': '10px',
		    '-webkit-transform': 'translate3d(0)',
		    '-moz-transform': 'translate3d(0)',
		    '-ms-transform': 'translate3d(0)',
		    '-o-transform': 'translate3d(0)',
		    'transform': 'translate3d(0)'
		  })
		}
		$('[data-col-index]').find('[data-tooltip], a, input').attr('tabindex', -1);
		if (!$(this).next().is('.not-available')) {
		  $(this).next().attr('tabindex', 0);
		}
		parentRecommended.find('[data-trigger-animation]').removeClass('active');
		hiddenRecommended.removeClass('active economy-flight--green , business-flight--blue');
		hiddenRecommended1.removeClass('active economy-flight--pey , business-flight--red');
		var recommendedFlight = $(this).closest('.flight-list-item').find('.flight-station-item').find('.inner-info');
		recommendedFlight.find('.cabin-color').addClass('hidden');
		// start animation
		if (!isOpen) {
		  if (!$(this).hasClass('not-available')) {
		    selfTable.find('[data-tooltip], input').attr('tabindex', 0);
		    selfTable.find('a').each(function() {
			 var hasDisabled = $(this).closest('.has-disabled').length;

			 !hasDisabled && $(this).attr('tabindex', 0);
		    })
		    $(this).next().attr('tabindex', -1);
		    btnMore.trigger('click.showMore');
		    $(this).addClass('active');

		    //ATC Changes Starts
		    if ($("#sk").val() == "true" && ($("#rebookVal").val() == "true")) {
			 $(".select-fare-table.multy-column .row-select").not(":last-child").addClass("hidden");
			 $(".select-fare-table.multy-column .row-head-select").find(".fare-condition").addClass("hidden");
			 $(".select-fare-table.multy-column .row-select").filter(":last-child").find(".col-select").filter(":first-child").addClass("hidden");
			 $(".select-fare-table.one-fare .row-select .col-select > :not(.col-item--3)").addClass("hidden");
		    }
		    //ATC Changes Ends

		    if ($(this).hasClass('column-trigger-animation')) {
			 var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item').find('.name-plane');
			 var lessDetailsLink = $(this).closest('.flight-list-item').find('.flight-station-item').find('.less-detail');
			 if (!$(body).hasClass('fs-business')) {
			   hiddenRecommended.addClass('active economy-flight--green');
			   recommendedFlightItem.next().removeClass('hidden economy').addClass('economy');
			   if (lessDetailsLink.hasClass("economy")) {
				lessDetailsLink.removeClass("economy");
			   }
			   recommendedFlightItem.next().next().addClass('hidden premium-economy').removeClass('economy');
			 } else {
			   //                  hiddenRecommended.addClass('active business-flight--blue');
			   //                  recommendedFlightItem.next().removeClass('hidden economy').addClass('business');
			   //                  recommendedFlightItem.next().next().addClass('hidden first').removeClass('economy');
			   if ($("#cabinClass").val() == "J") {
				if ($(this).hasClass("business-flight--blue")) {
				  hiddenRecommended.addClass("active business-flight--blue");
				} else {
				  hiddenRecommended.addClass("active business-flight--red");
				}
				recommendedFlightItem.next().removeClass("hidden first").addClass("business");
				recommendedFlightItem.next().next().addClass("hidden first").removeClass("business");

			   } else if ($("#cabinClass").val() == "F") {
				if ($(this).hasClass("business-flight--blue")) {
				  hiddenRecommended.addClass("active business-flight--blue");
				} else {
				  hiddenRecommended.addClass("active business-flight--red");
				}
				recommendedFlightItem.next().removeClass("business").addClass("hidden premium-economy");
				recommendedFlightItem.next().next().removeClass("economy hidden").addClass("first");
			   } else if ($("#cabinClass").val() == "S") {
				if (!$(this).hasClass("business-flight--blue")) {
				  hiddenRecommended.addClass("active economy-flight--pey");
				  recommendedFlightItem.next().next().removeClass("economy").addClass("hidden business");
				  recommendedFlightItem.next().removeClass("hidden business").addClass("premium-economy");
				} else {
				  hiddenRecommended.addClass("active business-flight--blue");
				  recommendedFlightItem.next().removeClass("economy").addClass("hidden premium-economy");
				  recommendedFlightItem.next().next().removeClass("hidden premium-economy").addClass("business");
				}

			   }

			 }

			 /* var discoverElement=hiddenRecommended;
				if(hiddenRecommended.hasClass("active")){
					discoverElement=hiddenRecommended;
				}else{
					discoverElement=hiddenRecommended1;
				}
				var fareFamilyId = $(discoverElement.find(".one-column").find(".btn-price").closest(".col-select").find(".fare-family-id")[0]).text().trim();
				var flightId = discoverElement.find(".one-column").find(".btn-price").closest(".flight-list-item").find(".segment-id").text().trim();
				 discoverElement.find(".one-column").find(".btn-price").closest(".wrap-content-fs").closest(".wrap-content-list").next(".fare-summary-group").find("#fare-family-outbound").val(fareFamilyId);
				discoverElement.find(".one-column").find(".btn-price").closest(".wrap-content-fs").closest(".wrap-content-list").next(".fare-summary-group").find("#flight-outbound").val(flightId.trim());
				setFlightIdDetails(0);
				triggerBSP();
				var cabinClassDesc=[];
				var flightSegments=global.vars.bsresponse.bookingSummary.flight[0].flightSegments;
					flightSegments.forEach(function(value,index,array){
					cabinClassDesc.push(value.cabinClassDesc.substring(0,value.cabinClassDesc.indexOf("(")).trim());
				});
				recommendedFlightItem.parent().each(function(index,bookingSummary){
					$(bookingSummary).append($("<span class='economy cabin-color'>"+cabinClassDesc[index]+"</span>"));
				});*/

		    }
		    if ($(this).hasClass('column-trigger-animation-1')) {
			 var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item').find('.name-plane');
			 if (!$(body).hasClass('fs-business')) {
			   hiddenRecommended1.addClass('active economy-flight--pey');
			   recommendedFlightItem.next().removeClass('economy').addClass('hidden economy');
			   recommendedFlightItem.next().next().removeClass('hidden economy').addClass('premium-economy');
			 } else {
			   //                  hiddenRecommended1.addClass('active business-flight--red');
			   //                  recommendedFlightItem.next().removeClass('economy').addClass('hidden business');
			   //                  recommendedFlightItem.next().next().removeClass('hidden economy').addClass('first');
			   if ($("#cabinClass").val() == "J") {
				if ($(this).hasClass("business-flight--blue")) {
				  hiddenRecommended1.addClass("active business-flight--blue");
				} else {
				  hiddenRecommended1.addClass("active business-flight--red");
				}
				recommendedFlightItem.next().removeClass("economy").addClass("hidden business");
				recommendedFlightItem.next().next().removeClass("hidden economy").addClass("first");
			   } else if ($("#cabinClass").val() == "F") {
				if ($(this).hasClass("business-flight--blue")) {
				  hiddenRecommended1.addClass("active business-flight--blue");
				} else {
				  hiddenRecommended1.addClass("active business-flight--red");
				}
				recommendedFlightItem.next().next().removeClass("economy").addClass("hidden business");
				recommendedFlightItem.next().removeClass("hidden business").addClass("first");
			   } else if ($("#cabinClass").val() == "S") {
				if (!$(this).hasClass("business-flight--blue")) {
				  hiddenRecommended1.addClass("active economy-flight--pey");
				  recommendedFlightItem.next().next().removeClass("economy").addClass("hidden business");
				  recommendedFlightItem.next().removeClass("hidden business").addClass("premium-economy");
				} else {
				  hiddenRecommended1.addClass("active business-flight--blue");
				  recommendedFlightItem.next().removeClass("economy").addClass("hidden premium-economy");
				  recommendedFlightItem.next().next().removeClass("hidden premium-economy").addClass("business");
				}
			   }
			 }
			 /*   var discoverElement=hiddenRecommended;
				if(hiddenRecommended.hasClass("active")){
					discoverElement=hiddenRecommended;
				}else{
					discoverElement=hiddenRecommended1;
				}
				var fareFamilyId = $(discoverElement.find(".one-column").find(".btn-price").closest(".col-select").find(".fare-family-id")[0]).text().trim();
				var flightId = discoverElement.find(".one-column").find(".btn-price").closest(".flight-list-item").find(".segment-id").text().trim();
				discoverElement.find(".one-column").find(".btn-price").closest(".wrap-content-fs").closest(".wrap-content-list").next(".fare-summary-group").find("#fare-family-outbound").val(fareFamilyId);
				discoverElement.find(".one-column").find(".btn-price").closest(".wrap-content-fs").closest(".wrap-content-list").next(".fare-summary-group").find("#flight-outbound").val(flightId.trim());
				setFlightIdDetails(0);
				triggerBSP();
				var cabinClassDesc=[];
				var flightSegments=global.vars.bsresponse.bookingSummary.flight[0].flightSegments;
					flightSegments.forEach(function(value,index,array){
					cabinClassDesc.push(value.cabinClassDesc.substring(0,value.cabinClassDesc.indexOf("(")).trim());
				});
				recommendedFlightItem.parent().each(function(index,bookingSummary){
					$(bookingSummary).append($("<span class='economy cabin-color'>"+cabinClassDesc[index]+"</span>"));
				});*/
		    }
		  }
		}
	   }
	 });
    });
  }

  /*  var getTemplateCarousel = function(sliderEl, btnNext, btnPrev, daysData, callback) {
	 $.get(global.config.url.fsSevenDayFareTemplate, function(data) {
	   var slides = sliderEl.find('.slides');
	   var template = window._.template(data, {
		data: daysData
	   });
	   slides.empty().append($(template));
	   initSlider(sliderEl, btnNext, btnPrev);

	   if(callback) {
		callback();
	   }
	 });
    }*/

  /*var loadCarouselSevenDay = function(sliderEl, btnNext, btnPrev, isNext) {
    var urlSevenday = global.config.url.fsSevenDayFareJson;
    var selfWrapContent = sliderEl.closest('.wrap-content-fs').index();

    if($('body').hasClass('sk-ut-flight-search-a')) {
	   urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-a-1.json' : 'ajax/histogram-bydate-response-sk-a-2.json';
    } else if($('body').hasClass('sk-ut-flight-search-b')) {
	   urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-b-1.json' : 'ajax/histogram-bydate-response-sk-b-2.json';
    }
    $.get(urlSevenday, function(data){
	 var initDays = data.response.byDay,
		currDate = sliderEl.data('current-date'),
		currDateIdx = 0,
		slideToIdx = 0,
		currDateIdx1 = 7;

	 _.map(initDays, function(day, idx){
	   if(day.month === currDate) {
		currDateIdx = idx;
	   }
	   return false;
	 })
	 if (typeof isNext !== 'undefined') {
	   currDateIdx = isNext ? currDateIdx + 7 : currDateIdx - 7;
	 }

	 currDateIdx = currDateIdx < 0 ? 0 : currDateIdx;

	 currDateIdx1 = currDateIdx;

	 if(initDays.length - currDateIdx < 7) {
	   currDateIdx = currDateIdx - (7 - (initDays.length - currDateIdx));
	 }

	 days = initDays.slice(currDateIdx, currDateIdx + 7);
	 getTemplateCarousel(sliderEl, btnNext, btnPrev, days, function() {
	   if (typeof isNext !== 'undefined') {
		sliderEl.data('current-date', initDays[currDateIdx].month);
	   } else {
		$('.slick-slide.slick-active').each(function(){
		  if($(this).data('date') === sliderEl.attr('data-selected-date') && !$(this).is('.selected')) {
		    $(this).trigger('click.selectSlideItem', true);
		  }
		})
	   }
	   btnNext && initDays.length - currDateIdx1 < 7 && currDateIdx >= 0 ? btnNext.attr('disabled', true) : btnNext.attr('disabled', false);
	   btnPrev && currDateIdx === 0 ? btnPrev.attr('disabled', true) : btnPrev.attr('disabled', false);
	 });
    })
  }*/

  var handleActionSlider = function() {
    var slider = $('[data-fs-slider]');
    // init
    slider.each(function() {
	 var self = $(this);
	 btnPrev = $(this).find('.btn-prev'),
	   btnNext = $(this).find('.btn-next');

	 //      loadCarouselSevenDay($(this), btnNext, btnPrev);

	 /* btnPrev.off('click.slidePrev').on('click.slidePrev', function(){
	    loadCarouselSevenDay(self, $(this).siblings('.btn-next'), $(this), false);
	  });

	  btnNext.off('click.slideNext').on('click.slideNext', function(){
	    loadCarouselSevenDay(self, $(this), $(this).siblings('.btn-prev'), true);
	  });*/

    });

  }

  function sformat(s) {
    var fm = [
	 //Math.floor(s / 60 / 60 / 24), // DAYS
	 Math.floor(s / 60 / 60) % 24, // HOURS
	 Math.floor(s / 60) % 60, // MINUTES
	 // s % 60 // SECONDS
    ];
    return $.map(fm, function(v, i) {
	 return ((v < 10) ? '0' : '') + v;
    }).join(':') + " hr ";
  }

  function minTwoDigits(n) {
    return (n < 10 ? '0' : '') + n;
  }

  var formatTimeToHour = function(seconds) {
    return parseFloat(seconds / 3600).toFixed(2) + 'hr';
  }

  var formatTimeToDate = function(seconds) {
    var dateObj = new Date(seconds),
	 date = dateObj.getDate(),
	 month = dateObj.getMonth() + 1,
	 hour = dateObj.getHours(),
	 minute = dateObj.getMinutes();

    return minTwoDigits(hour) + ':' + minTwoDigits(minute);
  }

  var sliderRange = function() {
    var rangeSlider = $('[data-range-slider]');

    rangeSlider.each(function() {
	 var min = $(this).data('min'),
	   max = $(this).data('max'),
	   step = $(this).data('step'),
	   unit = $(this).data('unit'),
	   type = $(this).data('range-slider');
	 labelFrom = '<span class="slider-from ' + type + '"></span',
	   labelTo = '<span class="slider-to ' + type + '"></span',

	   $(this).slider({
		range: true,
		min: min,
		max: max,
		step: step,
		values: [min, max],
		create: function() {
		  var slider = $(this),
		    leftLabel,
		    rightLabel;

		  switch (type) {
		    case "tripDuration":
		    case "layover":
			 leftLabel = $(labelFrom).text(sformat(min));
			 rightLabel = $(labelTo).text(sformat(max));
			 break;
		    case "departure":
		    case "arrival":
			 leftLabel = $(labelFrom).text(formatTimeToDate(min));
			 rightLabel = $(labelTo).text(formatTimeToDate(max));
			 break;
		    default:
			 break;
		  }

		  $(this).append(leftLabel);
		  $(this).append(rightLabel);
		},
		slide: function(event, ui) {
		  var Label;

		  switch (type) {
		    case "tripDuration":
		    case "layover":
			 $(this).find('.slider-from').text(sformat(ui.values[0]));
			 $(this).find('.slider-to').text(sformat(ui.values[1]));
			 break;
		    case "departure":
		    case "arrival":
			 $(this).find('.slider-from').text(formatTimeToDate(ui.values[0]));
			 $(this).find('.slider-to').text(formatTimeToDate(ui.values[1]));
			 break;
		    default:
			 break;
		  }
		}
	   })

	 if (type === 'tripDuration') {
	   $(this).find('.ui-slider-handle').eq(0).remove();
	   $(this).find('.slider-from').remove();
	 }

    });
  }

  var countLayover = function(segment) {
    var countLayover = 0;

    _.map(segment.legs, function(leg) {
	 leg.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;

	 leg.stops.length && _.map(leg.stops, function(stop) {
	   stop.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;
	 })
    })

    return countLayover;
  }

  var getFilterData = function(flightsData) {
    var arr = [];

    _.map(flightsData, function(flight, flightIdx) {
	 var obj = {
	   "nonStop": false,
	   "oneStop": false,
	   "twoStop": false,
	   "codeShare": false
	 };


	 var tripDuration = _.sortBy(flight.segments, function(segment) {
	   return segment.tripDuration;
	 });

	 var departure = _.sortBy(flight.segments, function(segment) {
	   return new Date(segment.departureDateTime.replace(/-/g, '/')).getTime();
	 });

	 var arrival = _.sortBy(flight.segments, function(segment) {
	   return new Date(segment.arrivalDateTime.replace(/-/g, '/')).getTime();
	 });

	 var layover = _.sortBy(flight.segments, function(segment) {
	   var totalLayover = 0;

	   _.map(segment.legs, function(leg, legIdx) {
		totalLayover += leg.layoverDuration;
	   })
	   segment['totalLayover'] = totalLayover
	   return totalLayover
	 });

	 _.map(flight.segments, function(segment) {
	   var count = countLayover(segment);

	   !segment.legs.length && (obj.nonStop = true);
	   switch (count) {
		case 0:
		  !obj.nonStop && (obj.nonStop = true);
		  break;
		case 1:
		  !obj.oneStop && (obj.oneStop = true);
		  break;
		case 2:
		  !obj.twoStop && (obj.twoStop = true);
		  break;
		default:
		  break;
	   }

	   segment.legs.length && _.map(segment.legs, function(leg) {
		if (typeof leg.codeShareFlight === "boolean") {
		  leg.codeShareFlight && (obj.codeShare = true);
		}
	   })
	 })


	 obj['minTripDuration'] = tripDuration[0].tripDuration;
	 obj['maxTripDuration'] = tripDuration[tripDuration.length - 1].tripDuration;
	 obj['minDeparture'] = new Date(departure[0].departureDateTime.replace(/-/g, '/')).getTime();
	 obj['maxDeparture'] = new Date(departure[departure.length - 1].departureDateTime.replace(/-/g, '/')).getTime();
	 obj['minArrival'] = new Date(arrival[0].arrivalDateTime.replace(/-/g, '/')).getTime();
	 obj['maxArrival'] = new Date(arrival[arrival.length - 1].arrivalDateTime.replace(/-/g, '/')).getTime();
	 obj['minLayover'] = layover[0].totalLayover;
	 obj['maxLayover'] = layover[layover.length - 1].totalLayover;

	 arr.push(obj);
    })

    return arr;
  }

  var filterFlights = function(data) {
    var filterBlock = $('[data-flight-filter]'),
	 flightsArr = data.flights;

    filterBlock.each(function() {
	 var flightsBlock = $(this).siblings('.recommended-flight-block'),
	   flightIdx = $(this).data('flightFilter'),
	   nonStopCheckbox = $('input[name="non-stop-' + flightIdx + '"]'),
	   oneStopCheckbox = $('input[name="one-stop-' + flightIdx + '"]'),
	   twoStopCheckbox = $('input[name="two-stop-' + flightIdx + '"]'),
	   codeShareCheckbox = $('input[name="codeshare-' + flightIdx + '"]'),
	   saGroupCheckbox = $('input[name="sa-group-' + flightIdx + '"]'),
	   sliderTripDuration = $(this).find('[data-range-slider="tripDuration"]'),
	   sliderDeparture = $(this).find('[data-range-slider="departure"]'),
	   sliderArrival = $(this).find('[data-range-slider="arrival"]'),
	   filterObj = {
		"stopover": {
		  "nonstopVal": true,
		  "onestopVal": true,
		  "twostopVal": true
		},
		"operating": {
		  "codeshareVal": true
		},
		"tripduration": sliderTripDuration.slider("values"),
		"departure": sliderDeparture.slider("values"),
		"arrival": sliderArrival.slider("values")
	   };

	 // get value from checkbox

	 nonStopCheckbox.off('change.getValue').on('change.getValue', function() {
	   filterObj.stopover.nonstopVal = $(this).is(':checked');
	   handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	 });

	 oneStopCheckbox.off('change.getValue').on('change.getValue', function() {
	   filterObj.stopover.onestopVal = $(this).is(':checked')
	   handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	 });

	 twoStopCheckbox.off('change.getValue').on('change.getValue', function() {
	   filterObj.stopover.twostopVal = $(this).is(':checked')
	   handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	 });

	 codeShareCheckbox.off('change.getValue').on('change.getValue', function() {
	   filterObj.operating.codeshareVal = $(this).is(':checked')
	   handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	 });

	 saGroupCheckbox.off('change.resetFilter').on('change.resetFilter', function() {
	   filterObj.stopover.nonstopVal = false;
	   filterObj.stopover.onestopVal = false;
	   filterObj.stopover.twostopVal = false;
	   filterObj.operating.codeshareVal = false;
	   filterObj.tripduration = [sliderTripDuration.data('min'), sliderTripDuration.data('max')];
	   filterObj.departure = [sliderDeparture.data('min'), sliderDeparture.data('max')];
	   filterObj.arrival = [sliderArrival.data('min'), sliderArrival.data('max')];
	   handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	 });

	 // get value from range slider

	 sliderTripDuration.slider({
	   stop: function(event, ui) {
		filterObj.tripduration = ui.values;
		handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	   }
	 });

	 sliderDeparture.slider({
	   stop: function(event, ui) {
		filterObj.departure = ui.values;
		handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	   }
	 });

	 sliderArrival.slider({
	   stop: function(event, ui) {
		filterObj.arrival = ui.values;
		handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
	   }
	 });

	 // handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);

    });
  }

  var handleFilterFlights = function(el, data, flightData, filterObj, flightIdx) {
    var selfData = [],
	 filterDataStopover = [],
	 filterDataOperating = [],
	 obj = {};

    _.map(filterObj.stopover, function(value, key) {
	 selfData = [];

	 switch (key) {
	   case 'nonstopVal':
		value && (selfData = flightData.filter(function(segment) {
		  var count = countLayover(segment);

		  return segment.legs.length === 0 || count === 0;
		}));
		break;
	   case 'onestopVal':
		value && (selfData = flightData.filter(function(segment) {
		  var count = countLayover(segment);

		  return count === 1;
		}));
		break;
	   case 'twostopVal':
		value && (selfData = flightData.filter(function(segment) {
		  var count = countLayover(segment);

		  return count === 2;
		}));
		break;
	   default:
		break;
	 }

	 filterDataStopover = $.unique([].concat.apply([], [filterDataStopover, selfData]));
    });

    filterDataStopover.length && (flightData = filterDataStopover);

    _.map(filterObj.operating, function(value, key) {
	 selfData = [];

	 _.map(flightData, function(flight) {
	   flight.legs.length && _.map(flight.legs, function(leg) {
		if (key === 'codeshareVal') {
		  if (value) {
		    typeof leg.codeShareFlight !== 'undefined' && selfData.push(flight);
		  } else {
		    typeof leg.codeShareFlight === 'undefined' && selfData.push(flight);
		  }
		  return false;
		}
	   })
	   return false;
	 })

	 filterDataOperating = $.unique([].concat.apply([], [filterDataOperating, selfData]));
    });

    filterDataOperating.length && (flightData = filterDataOperating);

    var tripDuration = filterObj.tripduration;

    selfData = flightData.filter(function(flight) {
	 return flight.tripDuration >= tripDuration[0] && flight.tripDuration <= tripDuration[1];
    });

    flightData = selfData;

    var departureDateTime = filterObj.departure;

    selfData = flightData.filter(function(flight) {
	 return new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() >= departureDateTime[0] && new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() <= departureDateTime[1];
    });

    flightData = selfData;

    var arrivalDateTime = filterObj.arrival;

    selfData = flightData.filter(function(flight) {
	 return new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() >= arrivalDateTime[0] && new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() <= arrivalDateTime[1];
    });

    flightData = selfData;

    obj['segments'] = flightData;

    getTemplateFlightTable(el, data, obj, flightIdx, true);
  }

  var handleLoadmore = function(flightBlock, loadmoreBlock) {
    var flights = flightBlock.find('.flight-list-item'),
	 flightsHidden = flightBlock.find('.flight-list-item.hidden');

    flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
    loadmoreBlock.find('[data-total-flight]').text(flights.length);
    loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
    loadmoreBlock.off('click.loadMore').on('click.loadMore', function(e) {
	 e.preventDefault();
	 var nextFlightIdx = flightBlock.find('.flight-list-item:not(".hidden")').length;
	 flightBlock.find('.flight-list-item.hidden').slice(0, 5).removeClass('hidden');
	 flightsHidden = flightBlock.find('.flight-list-item.hidden');
	 loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
	 flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');

	 setTimeout(function() {
	   flightBlock.find('.flight-list-item').eq(nextFlightIdx).focus().addClass('focus-outline');
	 }, 500);

	 selectFlightAnimation();
	 showMoreLessDetails();
    });
  }
  var labelStatusCheapest = function() {
    var arrPriceCheapest = [];
    var blockWrapFirst = $('.wrap-content-fs').first();
    var colInfoLeft = blockWrapFirst.find('.col-info-left');
    var lowestFoundFlag = false;
    colInfoLeft.each(function(idexLabel) {
	 if ($(this).find('.flight-price').find('.price-cheapest-colum').text() != "") {
	   arrPriceCheapest.push($(this).find('.flight-price').find('.price-cheapest-colum').text());
	   arrPriceCheapest.sort(function(a, b) {
		return parseFloat(a) - parseFloat(b);
	   });
	 }
    });
    colInfoLeft.each(function(idexLabel) {
	 if ($(this).find('.flight-price').find('.price-cheapest-colum').text() === arrPriceCheapest[0] && lowestFoundFlag == false) {
	   lowestFoundFlag = true;
	   $("body").find(".head-col").find(".label-status").remove();
	   $(this).find('.head-col').append('<span class="label-status anim-all"></span');
	 }
    });
  }

  var tableLoadPage = function(isRenderTemplate) {
    !isRenderTemplate && setTimeout(function() {
	 if ($('.main-inner').find('.wrap-content-fs').length > 1) {
	   // $('.wrap-content-fs').last().addClass('hidden');
	   $(".wrap-content-fs").not(":eq(0)").addClass("hidden");
	 };
    }, 500);
    var flightBlockItem = $('.recommended-flight-block').find('.flight-list-item');
    flightBlockItem.each(function(idx) {
	 var rowSelect = $(this).find('[data-hidden-recommended]').find('.multy-column').find('.row-head-select');
	 var rowSelect1 = $(this).find('[data-hidden-recommended-1]').find('.multy-column').find('.row-head-select');
	 var rowSelectOneColumn = $(this).find('[data-hidden-recommended]').find('.one-column').find('.row-head-select');
	 var rowSelectOneColumn1 = $(this).find('[data-hidden-recommended-1]').find('.one-column').find('.row-head-select');
	 colSelectColor = rowSelect.find('.col-select');
	 colSelectColor1 = rowSelect1.find('.col-select');
	 colSelectColorOneColumn = rowSelectOneColumn.find('.col-select');
	 colSelectColorOneColumn1 = rowSelectOneColumn1.find('.col-select');
	 $(this).find('.row-head-select').find('.col-select').removeClass('economy-fs--green-1 , economy-fs--green-2 , economy-fs--green-3 , economy-fs--green-4 , economy-fs--green-5 , economy-fs--green-6 , economy-fs--pey-1');
	 if ($('body').hasClass('fs-business')) {
	   //        flightBlockItem.removeClass('economy-flight-bgd').addClass('business-flight-bgd');
	   //        flightBlockItem.find('.column-trigger-animation').addClass('business-flight--blue');
	   //        flightBlockItem.find('.column-trigger-animation-1').addClass('business-flight--red');
	   //        colSelectColor.each(function(idx){
	   //          $(this).addClass('business-fs--blue-'+idx);
	   //        });
	   //        colSelectColor1.each(function(idx){
	   //          $(this).addClass('business-fs--red-'+idx);
	   //        });
	   //
	   //        colSelectColorOneColumn.each(function(idx){
	   //          $(this).addClass('business-fs--blue-1');
	   //        });
	   //        colSelectColorOneColumn1.each(function(idx){
	   //          $(this).addClass('business-fs--red-1');
	   //        });
	   if ($("#cabinClass").val() == "J") {
		flightBlockItem.removeClass("economy-flight-bgd").addClass("business-flight-bgd");

		if ($(this).find(".column-trigger-animation").hasClass("cabin-class-First") || $(this).find(".column-trigger-animation").hasClass("cabin-class-Business") || $(this).find(".column-trigger-animation-1").hasClass("cabin-class-First") || $(this).find(".column-trigger-animation-1").hasClass("cabin-class-Business")) {

		  $(this).find(".column-trigger-animation").addClass("business-flight--blue");
		  $(this).find(".column-trigger-animation-1").addClass("business-flight--red");
		  colSelectColor.each(function(idx) {
		    $(this).addClass("business-fs--blue-" + idx);
		  });
		  colSelectColor1.each(function(idx) {
		    $(this).addClass("business-fs--red-" + idx);
		  });
		  colSelectColorOneColumn.each(function(idx) {
		    $(this).addClass("business-fs--blue-1");
		  });
		  colSelectColorOneColumn1.each(function(idx) {
		    $(this).addClass("business-fs--red-1");
		  });
		}
	   } else if ($("#cabinClass").val() == "F") {
		$(this).removeClass("economy-flight-bgd").addClass("business-flight-bgd");

		if ($(this).find(".column-trigger-animation").hasClass("cabin-class-First") || $(this).find(".column-trigger-animation").hasClass("cabin-class-Business") || $(this).find(".column-trigger-animation-1").hasClass("cabin-class-First") || $(this).find(".column-trigger-animation-1").hasClass("cabin-class-Business")) {

		  $(this).find(".column-trigger-animation").addClass("business-flight--blue");
		  $(this).find(".column-trigger-animation-1").addClass("business-flight--red");
		  colSelectColor.each(function(idx) {
		    $(this).addClass("business-fs--blue-" + idx);
		  });
		  colSelectColor1.each(function(idx) {
		    $(this).addClass("business-fs--red-" + idx);
		  });
		  colSelectColorOneColumn.each(function(idx) {
		    $(this).addClass("business-fs--blue-1");
		  });
		  colSelectColorOneColumn1.each(function(idx) {
		    $(this).addClass("business-fs--red-1");
		  });
		}
	   } else {
		flightBlockItem.removeClass("economy-flight-bgd").addClass("economy-flight-bgd");
		flightBlockItem.removeClass("economy-flight-bgd").addClass("business-flight-bgd");
		flightBlockItem.addClass("economy-flight-bgd");
		flightBlockItem.find(".column-trigger-animation").addClass("business-flight--blue");
		flightBlockItem.find(".column-trigger-animation-1").addClass("economy-flight--pey");
		colSelectColor.each(function(idx) {
		  $(this).addClass("business-fs--blue-" + idx);
		});
		colSelectColor1.each(function(idx) {
		  $(this).addClass("economy-fs--pey-" + idx);
		});
		colSelectColorOneColumn.each(function(idx) {
		  $(this).addClass("business-fs--blue-1");
		});
		colSelectColorOneColumn1.each(function(idx) {
		  $(this).addClass("economy-fs--pey-1");
		});
	   }
	 } else {
	   colSelectColor.each(function(idx1) {
		$(this).addClass('economy-fs--green-' + idx1);
	   });
	   colSelectColor1.each(function(idx1) {
		$(this).addClass('economy-fs--pey-' + idx1);
	   });

	   colSelectColorOneColumn.each(function(idx1) {
		$(this).addClass('economy-fs--green-1');
	   });
	   colSelectColorOneColumn1.each(function(idx1) {
		$(this).addClass('economy-fs--pey-1');
	   });
	 }
    });

    var dataRecommended = $('[data-hidden-recommended]');
    var dataRecommended1 = $('[data-hidden-recommended-1]');
    dataRecommended.each(function(idxData) {
	 var colSelectItem = $(this).find('.select-fare-table.hidden-mb-small').find('.col-select');
	 var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
	 var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
	 var btnPrice = colSelectItem.find('.btn-price');
	 if (btnPrice.length > 1) {
	   btnPrice.each(function() {
		if (($(this).attr('data-header-class').toLowerCase().includes("lite")) || ($(this).attr('data-header-class').toLowerCase().includes("super saver"))) {
		  $(this).addClass('keep-my-selection');
		}
	   });

	   var buttonFirst = btnPrice.first();
	   buttonFirst.addClass('btn-price-cheapest-select');
	   buttonFirst.closest('.col-select').next().find('.btn-price').addClass('btn-price-cheapest-select');
	 }
	 if (lengthColSelectItem == 2) {
	   rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
	   rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
	 }
    });

    dataRecommended1.each(function(idxData) {
	 var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
	 var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
	 if (lengthColSelectItem == 2) {
	   rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
	   rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
	 }
    });

    labelStatusCheapest();
  }

  var wrapContent, parentFlightList, el, blockParentWrap,
    blockParentColSelect, wrapContents, wrapContentList,
    nameHead, baggage, seatSelection, earnKrisFlyer, upgrade,
    cancellation1, cancellation2, bookingChange1, bookingChange2, noShow1, noShow2, priceCurrentSelected, idx1, idx2;

  var hiddenBlockClick = function(minFareMap) {
    var selfHeight = el.closest('.flight-list-item').find('.recommended-table [data-wrap-flight-height]').data('wrap-flight-height');
    if (el.closest('.wrap-content-fs').index() === wrapContents.length - 1) {
	 el = $('.btn-price.active');
	 var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();

	 var fareFamilyFsCode = el.closest(".col-select").find(".fare-family-id").text().trim(); //added for strict rules
	 var fareFamilyName = el.attr("data-header-class");

	 var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text().trim();
	 var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text().trim();
	 var listRow = el.closest('.select-fare-table').find('.row-select, .row-head-select');
	 var thisCol = el.closest('.col-select');
	 var idx = thisCol.index();
	 var listContents, colRight, colLeft;
	 var noteFare = $('.has-note-fare');
	 listRow.each(function() {
	   $(this).find('.col-select').eq(idx).addClass('col-selected');
	 });
	 var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
	 var colSelected = listRow.find('.col-select.col-selected');
	 var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
	 idx2 = colSelected.find('.index-of').text();
	 nameHead = colSelected.find('.name-header-family').text().replace('fare conditions', "");
	 baggage = colSelected.find('.baggage').text();
	 seatSelection = colSelected.find('.seat-selection').html();
	 earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
	 upgrade = colSelected.find('.upgrade').text();
	 cancellation2 = colSelected.find('.cancellation').text();
	 bookingChange2 = colSelected.find('.booking-change').text();
	 noShow2 = colSelected.find('.no-show').text();
	 listContents = summaryGroup.find('.row-select .column-left span, .row-select .column-right span');
	 colLeft = summaryGroup.find('.col-select.column-left');
	 colRight = summaryGroup.find('.col-select.column-right');
	 summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').attr('class', className);
	 summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').addClass('border-fare-family');


	 if ($.parseJSON($("#selectflightJson").val()).response.tripType != "M") {
	   summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
	   summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
	 }
	 //summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
	 summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
	 summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
	 summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('value', eligibleRecommendationIds);
	 colRight.find('.baggage').text(baggage);
	 colRight.find('.seat-selection').html(seatSelection);

	 // colRight.find('.seat-selection').siblings('.complimentary-note').remove();
	 // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
	 //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
	 //   if(!colRight.find('.seat-selection').siblings('.complimentary-note').length) {
	 //     colRight.find('.seat-selection').parent().append(seatSelectionExtra.clone());
	 //   }
	 // }

	 colRight.find('.earn-krisFlyer').text(earnKrisFlyer);

	 var upgradeTextSummary = colRight.find('.upgrade');
	 upgradeTextSummary.text(upgrade);
	 if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
	   var upgradeText = colSelected.find('.upgrade');
	   var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
	   if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
		upgradeTextSummary.parent().append(upgradeTooltip.clone());
		upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
	   }
	 } else {
	   //upgradeTextSummary.siblings('em').remove();
	   var upgradeContent = '<span class="fare-price upgrade">' + upgrade + '</span>  ' + '<span class="loading loading--small hidden"></span>' + '<span class="ico-right miles-tooltip from-submit-form" ' + 'farefamily="' + el.closest('.col-select').find('.fare-family-id').text().trim(); + '" ' + 'codeOrigin="' + codeOrigin + '" codeDestination="' + codeDestination + '">' + '<em tabindex="0" data-content="<p class=\'tooltip__text-2\'>data here' + '</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" ' + 'aria-label="View more information"></em></span>'
	   upgradeTextSummary.html(upgradeContent);
	 }

	 var outselectedFlightId = ($("#selectedFlightIdDetails").val()).split(";")[0];
	 var outsegmentId = outselectedFlightId.split("|")[0];
	 var outfareFamilyCode = outselectedFlightId.split("|")[1].slice(2, 3);

	 var tripType = $('#tripType').val();

	 if (tripType.trim() === 'R') {
	   var inselectedFlightId = ($("#selectedFlightIdDetails").val()).split(";")[1];
	   var insegmentId = inselectedFlightId.split("|")[0];
	   var infareFamilyCode = inselectedFlightId.split("|")[1].slice(2, 3);
	 }


	 // --- Compare 2 fare conditions
	 noteFare.show();
	 if (outfareFamilyCode < infareFamilyCode) {
	   colLeft.find('.cancellation').text(cancellation1);
	   colLeft.find('.booking-change').text(bookingChange1);
	   colLeft.find('.no-show').text(noShow1);
	   colRight.find('.cancellation').text(cancellation1 + ' *');
	   colRight.find('.booking-change').text(bookingChange1 + ' *');
	   colRight.find('.no-show').text(noShow1 + ' *');
	 } else if (outfareFamilyCode > infareFamilyCode) {
	   colLeft.find('.cancellation').text(cancellation2 + ' *');
	   colLeft.find('.booking-change').text(bookingChange2 + ' *');
	   colLeft.find('.no-show').text(noShow2 + ' *');
	   colRight.find('.cancellation').text(cancellation2);
	   colRight.find('.booking-change').text(bookingChange2);
	   colRight.find('.no-show').text(noShow2);
	 } else {
	   noteFare.hide();
	   colLeft.find('.cancellation').text(cancellation1);
	   colLeft.find('.booking-change').text(bookingChange1);
	   colLeft.find('.no-show').text(noShow1);
	   colRight.find('.cancellation').text(cancellation2);
	   colRight.find('.booking-change').text(bookingChange2);
	   colRight.find('.no-show').text(noShow2);
	 }
	 // --- End

	 var eachColSelect = summaryGroup.find('.row-head-select').find('.col-select');
	 /* eachColSelect.each(function() { // for secure fare
	    var valuettt = eachColSelect.find('.ttt').val();
	    if(valuettt === "0"){
		 summaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
		 summaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
		 summaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
	    } else{
		 summaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
		 summaryGroup.find('.button-group-1').find('.text').addClass('hidden');
		 summaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
	    }
	  });*/

	 // --- Render color accordingly
	 listContents.each(function() {
	   var _this = $(this),
		thisText = _this.text();
	   _this.removeClass('complimentary not-allowed fare-price');
	   if (thisText.indexOf('Complimentary') !== -1) {
		_this.addClass('complimentary');
	   } else if ((thisText.indexOf('Not allowed') !== -1) || (thisText.indexOf('Only available during online check-in') !== -1)) {
		_this.addClass('not-allowed');
	   } else {
		_this.addClass('fare-price');
	   }
	 });
	 // --- End

	 var rcmidCorresponding = el.find('.rcmid-corresponding').text();
	 var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text().trim();
	 var flightId = el.closest('.flight-list-item').find('.segment-id').text().trim();
	 summaryGroup.find('#recommendation-id').val(rcmidCorresponding);
	 summaryGroup.find('#fare-family-inbound').val(fareFamilyId);
	 summaryGroup.find('#flight-inbound').val(flightId.trim());
	 $("#selectedFlightId" + el.closest('.wrap-content-fs').index() + "").val(flightId.trim() + "|" + fareFamilyId); //shaneesh

	 summaryGroup.removeClass('hidden');
	 slideshowpremiumeconomy(minFareMap);
	 listRow.each(function() {
	   $(this).find('.col-select').eq(idx).removeClass('col-selected');
	 });

	 var nameTtem2 = summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
	 $('.flight-search-summary-conditions').find('.tab-item-2').text(nameTtem2.text());
	 $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');

	 showCorrelativeTab();
	 setFlightIdDetails(el.closest('.wrap-content-fs').index());
	 // scrollTop func
	 setTimeout(function() {
	   if ($(".tripType").val() == "O") {
		$("html, body").animate({
		  scrollTop: $(".wrap-content-fs:eq(0)").offset().top - $("[data-booking-summary-panel]").innerHeight() - 20
		}, 0);
	   } else {
		$("html, body").animate({
		  scrollTop: $(".wrap-content-fs:eq(1)").offset().top - $("[data-booking-summary-panel]").innerHeight() - 20
		}, 0);
	   }

	 }, 750);
	 var flightData = {
	   "fareFamilyName": fareFamilyName,
	   "fareFamilyFsCode": fareFamilyFsCode,
	   "upgradeWithMiles": upgrade,
	   "cancellation": cancellation2,
	   "bookingChange": bookingChange2,
	   "noShow": noShow2,
	   "codeOrigin": codeOrigin,
	   "codeDestination": codeDestination
	 }

	 globalJson.selectedFlightsData.push(flightData);
    } else {
	 el = $('.btn-price.active');
	 var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
	 var fareFamilyFsCode = el.closest(".col-select").find(".fare-family-id").text().trim(); //added for strict rules
	 var fareFamilyName = el.attr("data-header-class");
	 var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text().trim();
	 var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text().trim();
	 var listRow1 = el.closest('.select-fare-table').find('.row-select, .row-head-select');
	 var thisCol = el.closest('.col-select');
	 var idx = thisCol.index();
	 listRow1.each(function() {
	   $(this).find('.col-select').eq(idx).addClass('col-selected');
	 });
	 var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
	 var colSelected = listRow1.find('.col-select.col-selected');
	 var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
	 idx1 = colSelected.find('.index-of').text();
	 nameHead = colSelected.find('.name-header-family').text().replace('fare conditions', "");
	 baggage = colSelected.find('.baggage').text();
	 seatSelection = colSelected.find('.seat-selection').text();
	 earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
	 upgrade = colSelected.find('.upgrade').text();
	 cancellation1 = colSelected.find('.cancellation').text();
	 bookingChange1 = colSelected.find('.booking-change').text();
	 noShow1 = colSelected.find('.no-show').text();
	 colLeft = summaryGroup.find('.col-select.column-left');
	 summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').attr('class', className);
	 summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').addClass('border-fare-family');

	 if ($.parseJSON($("#selectflightJson").val()).response.tripType != "M") {
	   summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
	   summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
	 }
	 summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
	 //summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
	 summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
	 summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('value', eligibleRecommendationIds);
	 colLeft.find('.baggage').text(baggage);
	 colLeft.find('.seat-selection').html(seatSelection);

	 // colLeft.find('.seat-selection').siblings('.complimentary-note').remove();
	 // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
	 //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
	 //   if(!colLeft.find('.seat-selection').siblings('.complimentary-note').length) {
	 //     colLeft.find('.seat-selection').parent().append(seatSelectionExtra.clone());
	 //   }
	 // }

	 colLeft.find('.earn-krisFlyer').text(earnKrisFlyer);

	 var upgradeTextSummary = colLeft.find('.upgrade');
	 upgradeTextSummary.text(upgrade);
	 if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
	   var upgradeText = colSelected.find('.upgrade');
	   var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
	   if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
		upgradeTextSummary.parent().append(upgradeTooltip.clone());
		upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
	   }
	 } else {
	   //upgradeTextSummary.siblings('em').remove();
	   var upgradeContent = '<span class="fare-price upgrade">' + upgrade + '</span>  ' + '<span class="loading loading--small hidden"></span>' + '<span class="ico-right miles-tooltip from-submit-form" ' + 'farefamily="' + el.closest('.col-select').find('.fare-family-id').text().trim(); + '" ' + 'codeOrigin="' + codeOrigin + '" codeDestination="' + codeDestination + '">' + '<em tabindex="0" data-content="<p class=\'tooltip__text-2\'>data here' + '</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" ' + 'aria-label="View more information"></em></span>'
	   upgradeTextSummary.html(upgradeContent);
	 }

	 var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text().trim();
	 var flightId = el.closest('.flight-list-item').find('.segment-id').text().trim();
	 summaryGroup.find('#fare-family-outbound').val(fareFamilyId);
	 summaryGroup.find('#flight-outbound').val(flightId.trim());
	 $("#selectedFlightId" + el.closest('.wrap-content-fs').index() + "").val(flightId.trim() + "|" + fareFamilyId); //shaneesh

	 summaryGroup.addClass('hidden');
	 var nameTtem1 = summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
	 $('.flight-search-summary-conditions').find('.tab-item-1').text(nameTtem1.text());
	 $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

	 listRow1.each(function() {
	   $(this).find('.col-select').eq(idx).removeClass('col-selected');
	 });
	 setFlightIdDetails(el.closest('.wrap-content-fs').index());
	 // scrollTop func
	 /*setTimeout(function(){
	   $('html, body').animate({
		scrollTop: $('.wrap-content-fs:eq(0)').offset().top - $('[data-booking-summary-panel]').innerHeight() - 20
	   }, 0);
	 }, 750);*/
	 setTimeout(function() {
	   if ($(".tripType").val() == "M") {
		if (!miniFareData) {
		  $("html, body").animate({
		    scrollTop: $(".wrap-content-fs:eq(0)").offset().top - $("[data-booking-summary-panel]").innerHeight() - 20
		  }, 0);
		}
	   } else {
		$("html, body").animate({
		  scrollTop: $(".wrap-content-fs:eq(0)").offset().top - $("[data-booking-summary-panel]").innerHeight() - 20
		}, 0);
	   }
	 }, 750);
	 var flightData = {
	   "fareFamilyName": fareFamilyName,
	   "fareFamilyFsCode": fareFamilyFsCode,
	   "upgradeWithMiles": upgrade,
	   "cancellation": cancellation1,
	   "bookingChange": bookingChange1,
	   "noShow": noShow1,
	   "codeOrigin": codeOrigin,
	   "codeDestination": codeDestination
	 };

	 globalJson.selectedFlightsData.push(flightData);

    }
    if ($.parseJSON($("#selectflightJson").val()).response.tripType === "M") { //only for multicity
	 $('.sk-multicity').find('li').find("span").addClass("hidden");
    }
    wrapContent = el.closest('.wrap-content-fs');
    el.closest('.upsell').addClass('hidden');
    parentFlightList = el.closest('.flight-list-item');

    if (parentFlightList.find(".col-info.active").length != 0) {
	 parentFlightList.find(".col-info.active").get(0).click();
    } else {
	 parentFlightList.find('.col-info-left').get(0).click();
    }
    parentFlightList.find('.change-flight-item.bgd-white').find('[data-wrap-flight-height]').data('wrap-flight-height', selfHeight);
    parentFlightList.find('.change-flight-item.bgd-white').removeClass('hidden');
    wrapContent.find('.economy-slider , .fs-status-list , .sub-logo , .monthly-view , .flight-search-filter-economy , .recommended-table , .head-recommended, .loadmore-block').addClass('hidden');
    wrapContent.find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
    wrapContent.find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
    wrapContent.find('.col-info-select').removeClass('active');
    parentFlightList.find('[data-less-details-table]').removeClass('hidden').removeClass('premium-economy');

    if (blockParentColSelect.find('.btn-price').hasClass('active') && $('.main-inner').find('.wrap-content-fs').length > 1) {
	 blockParentWrap.next().removeClass('hidden');

	 var dataTriggercolumn = blockParentWrap.next().find('[data-trigger-animation]');
	 if (dataTriggercolumn.hasClass('has-disabled')) {
	   dataTriggercolumn.removeClass('active');
	   dataTriggercolumn.closest('.recommended-table').find('.airline-info').find('.less-detail').get(0).click();
	   dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
	   dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
	 }

    } else {
	 blockParentWrap.next().addClass('hidden');
    }

    el.closest('.wrap-content-fs').find('.flight-list-item').attr('tabindex', -1);
    handleLoadmore(blockParentWrap.next().find('.recommended-flight-block'), blockParentWrap.next().find('[data-loadmore]'));
    handleActionSlider();
    showMoreLessDetails();

    $("#chooseFlightForm").attr("action", $("#actionUrl").val());
    var tripType = $('#tripType').val();
    if (tripType.trim() === 'M') {
	 $('#_eventId').val("reviewMulticityItineraryEvent");
    } else {
	 $('#_eventId').val("reviewItineraryEvent");
    }

  }
  var setFlightIdDetails = function(index) {

    $('#chooseFlightForm').attr('action', $("#actionUrl").val());
    $(".tripType").val($("#tripType").val().trim());
    globalJson.prevSelectedFlightIdDetails = globalJson.selectedFlightIdDetails;
    if ($('#tripType').val().trim() === "R") {
	 var prevIds = globalJson.selectedFlightIdDetails;
	 var splitData = prevIds.split(";");
	 var flightOut = "";
	 var flightIn = "";
	 if ($("#flight-outbound").val().trim() == "" || $("#fare-family-outbound").val().trim() == "") {
	   flightOut = splitData[index];
	 } else {
	   flightOut = $("#flight-outbound").val().trim() + "|" + $("#fare-family-outbound").val().trim();
	 }
	 if (index == 0) {
	   flightIn = globalJson.defaultSelection.split(";")[1]

	 } else {
	   if ($("#flight-inbound").val().trim() == "" || $("#fare-family-inbound").val().trim() == "") {
		flightIn = splitData[index];
	   } else {
		flightIn = $("#flight-inbound").val().trim() + "|" + $("#fare-family-inbound").val().trim();
	   }
	 }
	 globalJson.selectedFlightIdDetails = flightOut + ";" + flightIn;
	 $("[name=selectedFlightIdDetails]").val(flightOut + ";" + flightIn);
    } else if ($('#tripType').val() === "M") {
	 var allFlightDetails = "";
	 var prevIds = globalJson.selectedFlightIdDetails;
	 var splitData = prevIds.split(";");
	 $.each($(".selectedFlightsIds"), function(element, index) {
	   if (allFlightDetails === "" && $(this).val() != "") {
		allFlightDetails = $(this).val() + ";";
	   } else if (allFlightDetails != "" && $(this).val() == "") {
		allFlightDetails = allFlightDetails + splitData[element] + ";";
	   } else if (allFlightDetails == "" && $(this).val() == "") {
		allFlightDetails = allFlightDetails + splitData[element] + ";";
	   } else if (allFlightDetails != "" && $(this).val() != "") {
		allFlightDetails = allFlightDetails + $(this).val() + ";";
	   }
	   /*else{
		allFlightDetails=allFlightDetails+$(this).val()+";";
	   }*/
	 });
	 globalJson.selectedFlightIdDetails = allFlightDetails;
	 $("[name=selectedFlightIdDetails]").val(allFlightDetails);
    } else if ($('#tripType').val().trim() === "O") {
	 var flightOut = $("#flight-inbound").val() + "|" + $("#fare-family-inbound").val();
	 if ($('#rebookVal').val() == 'true' && $("#partialFlown").val() != undefined && $("#partialFlown").val() == "true") {
	   var firstSeg = globalJson.defaultSelection.split(";")[0];
	   $("[name=selectedFlightIdDetails]").val(firstSeg + ";" + flightOut);
	 } else {
	   $("[name=selectedFlightIdDetails]").val(flightOut);
	 }
	 globalJson.selectedFlightIdDetails = flightOut;
    }

  }
  var upgradeAdditional = function() {
    var nextBtn = el.closest('.col-select').next().find('.btn-price');
    var nameHeaderClick = nextBtn.data('header-class');
    var priceClick = el.find('.btn-price-cheapest-colum').text();
    var priceUpgrade = nextBtn.find('.btn-price-cheapest-colum').text();
    el.closest("[data-hidden-recommended]").find(".upsell").find(".currency-code").text(" " + $.parseJSON($("#selectflightJson").val()).response.currency.code);
    el.closest('[data-hidden-recommended]').find('.upsell').find('.name-family').text(nameHeaderClick);
    el.closest('[data-hidden-recommended]').find('.upsell').find('.price-sgd').text(' ' + (parseFloat(priceUpgrade) - parseFloat(priceClick)).toLocaleString(undefined, {
	 minimumFractionDigits: $.parseJSON($("#selectflightJson").val()).response.currency.precision
    }));

    var buttonColIndex = el.closest(".col-select").index();
    var buttonColIndexNext = el.closest(".col-select").index() + 1;
    var tableStart = el.closest(".select-fare-table");
    var baggageDiv = tableStart.find(".row-select:eq(0)").find(".col-select:eq(" + buttonColIndex + ")").find(".baggage").text().trim();
    var baggageDivNext = tableStart.find(".row-select:eq(0)").find(".col-select:eq(" + buttonColIndexNext + ")").find(".baggage").text().trim();
    var earnKFMilesDiv = tableStart.find(".row-select:eq(2)").find(".col-select:eq(" + buttonColIndex + ")").find(".earn-krisFlyer").text().trim();
    var earnKFMilesDivNext = tableStart.find(".row-select:eq(2)").find(".col-select:eq(" + buttonColIndexNext + ")").find(".earn-krisFlyer").text().trim();
    var bookingChangeDiv = tableStart.find(".row-select:eq(5)").find(".col-select:eq(" + buttonColIndex + ")").find(".booking-change").text().trim();
    var bookingChangeDivNext = tableStart.find(".row-select:eq(5)").find(".col-select:eq(" + buttonColIndexNext + ")").find(".booking-change").text().trim();
    var baggageContent = parseInt(baggageDivNext.replace(/[^0-9\.]/g, ''), 10) - parseInt(baggageDiv.replace(/[^0-9\.]/g, ''), 10);
    var earnKFMilesContent = parseInt(earnKFMilesDivNext.replace(/[^0-9\.]/g, ''), 10);
    var bookingChangeContent = parseInt(bookingChangeDiv.replace(/[^0-9\.]/g, ''), 10) - parseInt(bookingChangeDivNext.replace(/[^0-9\.]/g, ''), 10);

    el.closest("[data-hidden-recommended]").find(".upsell").find(".list-items").find(".item:eq(0)").find(".item-info").html("+ " + baggageContent + "Kg");
    el.closest("[data-hidden-recommended]").find(".upsell").find(".list-items").find(".item:eq(1)").find(".item-info").html(earnKFMilesContent + "%");
    el.closest("[data-hidden-recommended]").find(".upsell").find(".list-items").find(".item:eq(2)").find(".des").html("Save " + $.parseJSON($("#selectflightJson").val()).response.currency.code + " " + bookingChangeContent + " on booking changes");
    if (nameHeaderClick != undefined) {
	 if (nameHeaderClick.trim().toLowerCase() === "economy standard") {
	   el.closest("[data-hidden-recommended]").find(".upsell").find(".list-items").find(".item:eq(3)").find(".des").html("Free seat selection at booking");
	 } else {
	   el.closest("[data-hidden-recommended]").find(".upsell").find(".list-items").find(".item:eq(3)").find(".des").html("Pay for seat selection at booking");
	 }
    }
  }

  $(document).on('click.calculatePriceInbound', '.btn-price', function(e) {
    e.preventDefault();

    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').removeClass('has-disabled');


    var arrayPrice1, arrayPrice2;
    wrapContentList = $('.wrap-content-list');
    wrapContents = wrapContentList.find('.wrap-content-fs');
    var wrapContentNotFirst = $('.wrap-content-fs');
    clickedBtn = $(this);
    el = $(this);
    blockParentWrap = el.closest('.wrap-content-fs');
    var dataTriggerAnimation = $('.wrap-content-fs').find('[data-flight-item]');
    var thisItem = $(this);
    var name = $(this).attr("data-header-class");
    var firstWord = name.trim().split(' ');
    var firstWordLast = firstWord[0];
    if (firstWordLast != '' && firstWordLast != 'undefined' && firstWordLast != 'Economy') {
	 $('.your-flight-item').addClass('hidden');
    }
    //Upsell block made hidden for all scenarios
    /*else if(firstWordLast == 'Economy'){

	$('.your-flight-item').removeClass('hidden');
    }*/
    var ak1 = $(this);
    var nextRec = ak1.closest(".wrap-content-fs").next();
    if ($('#tripType').val() === "M") {
	 if ($("body").hasClass("sk-ut-flight-search-a")) {
	   if ($(this).closest("[data-returntrip]").data("returntrip") === 0 && ($(this).data("header-class") !== "Economy Super Saver" || $(this).closest("[data-hidden-recommended]").data("segmentid") !== 0)) {
		return false;
	   } else {
		if ($(this).closest("[data-returntrip]").data("returntrip") === 1 && ($(this).data("header-class") !== "Economy Lite" || $(this).closest("[data-hidden-recommended]").data("segmentid") !== 0)) {
		  return false;
		}
	   }
	 }
	 if ($("body").hasClass("sk-ut-flight-search-b")) {
	   if ($(this).closest("[data-returntrip]").data("returntrip") === 0 && ($(this).data("header-class") !== "Economy Standard" || $(this).closest("[data-hidden-recommended]").data("segmentid") !== 3)) {
		return false;
	   } else {
		if ($(this).closest("[data-returntrip]").data("returntrip") === 1 && ($(this).data("header-class") !== "Economy Flexi" || $(this).closest("[data-hidden-recommended]").data("segmentid") !== 0)) {
		  return false;
		}
	   }
	 }
	 if (!$(this).hasClass("btn-price-cheapest-select")) {
	   var aH = $(".wrap-content-fs").find(".change-flight-item.bgd-white");
	   aH.each(function() {
		$(this).addClass("has-choose");
	   });
	 }

	 var arrayPrice1, arrayPrice2;
	 wrapContentList = $(".wrap-content-list");
	 wrapContents = wrapContentList.find(".wrap-content-fs");
	 //	            var wrapContentNotFirst = el.closest(".wrap-content-fs").next();//$(".wrap-content-fs:not(:eq(0))");
	 var index = el.closest(".wrap-content-fs").index() + 1;
	 var wrapContentNotFirst = $(".wrap-content-fs:eq(" + index + ")");

	 a = $(this);
	 ak = $(this);
	 blockParentWrap = el.closest(".wrap-content-fs");
	 var dataTriggerAnimation = el.closest(".wrap-content-fs").next().find("[data-flight-item]");
	 if (!$(this).hasClass("btn-price-cheapest-select")) {
	   dataTriggerAnimation.each(function() {
		if ($(this).hasClass("active")) {
		  $(this).closest(".flight-list-item").find(".col-info").removeClass("selected-item");
		  $(this).find(".less-detail").get(0).click();
		}
	   });
	 }
	 blockParentWrap.removeClass("selected-item");
	 // if (ak.closest(".wrap-content-fs").index() === 0) {
	 wrapContentNotFirst.find(".change-flight-item.bgd-white").addClass("hidden");
	 wrapContentNotFirst.find(".economy-slider , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended").removeClass("hidden");
	 wrapContentNotFirst.find(".col-select").find(".btn-price").removeClass("active");
	 //  }
	 var listItems = wrapContentNotFirst.find(".flight-list-item");
	 var listCol = listItems.find(".row-select, .row-head-select").find(".col-select:not(:eq(0))");
	 var listPrice = listItems.find(".btn-price").find(".list-price");
	 var oneCol = listItems.find(".one-column");
	 var at = $(this).find(".list-price").text();
	 if (ak1.hasClass("column-left")) {
	   ak1.closest(".flight-list-item").find(".column-trigger-animation").addClass("selected-item");
	 } else {
	   if (ak1.hasClass("column-right")) {
		ak1.closest(".flight-list-item").find(".column-trigger-animation-1").addClass("selected-item");
	   }
	 }
	 upgradeAdditional();
	 blockParentWrap = ak1.closest(".wrap-content-fs");
	 ab = blockParentWrap.find(".col-select");
	 blockParentWrap = el.closest('.wrap-content-fs');
	 blockParentColSelect = blockParentWrap.find('.col-select');
	 blockParentColSelect.find('.btn-price').removeClass('active');
	 ab.find(".btn-price").removeClass("active");
	 $(".col-select").find(".btn-price").removeClass("active");
	 $(".upsell").addClass("hidden");
	 $(this).addClass("active");
	 if ($(this).hasClass("keep-my-selection")) {
	   var aR = $(this).closest("[data-hidden-recommended]").find(".upsell");
	   if (blockParentWrap.index() === 0) {
		aR.removeClass("hidden");
		aR.removeClass("hidden");
	   } else {
		// hiddenBlockClick();
	   }
	   setTimeout(function() {
		aR.find('input[name="btn-keep-selection"]').focus();
	   }, 100);
	 } else {
	   // hiddenBlockClick();
	 }
	 /* if (ak.closest(".wrap-content-fs").index() !== 0) {
		// return;
	  }*/
	 listCol.addClass("has-disabled").attr("tabindex", -1);
	 oneCol.each(function() {
	   if (!$(this).hasClass("hidden")) {
		var a0 = $(this).closest(".select-fare-block").data("col-index");
		var a1 = $(this).closest(".flight-list-item").find("[data-trigger-animation]");
		a1.each(function() {
		  if ($(this).data("trigger-animation") === a0) {
		    $(this).addClass("has-disabled").attr("tabindex", -1);
		  }
		});
	   }
	 });
	 var ax = listItems.find(".btn-price").find(".rcmid");
	 var ap = $(this).find(".rcmid-out").text();
	 var aI = ap.trim();
	 var aT = aI.split(" ");
	 if (ax.length) {
	   ax.each(function() {
		var a7 = $(this);
		var a3 = a7.text().split("-");
		for (var a0 = 0; a0 < a3.length; a0++) {
		  for (var a4 = 0; a4 < a3[a0].length; a4++) {
		    for (var a1 = 0; a1 < aT.length; a1++) {
			 if (a3[a4] !== null || a3[a4] !== "undefined" || a3[a4] !== undefined) {
			   if (a3[a4]) {
				if (aT[a1] === a3[a4].slice(0, 4).trim()) {
				  var a5 = $(this).closest(".select-fare-table").find(".row-select, .row-head-select");
				  var a8 = $(this).closest(".col-select");
				  var a6 = a8.index();
				  var a2 = $(this).closest(".flight-list-item").find("[data-trigger-animation]");
				  a5.each(function() {
				    $(this).find(".col-select").eq(a6).removeClass("has-disabled");
				  });
				  a2.removeClass("has-disabled");
				  $(this).closest(".btn-price").find(".btn-price-cheapest-colum").text(a3[a4].slice(3).trim());
				  $(this).closest(".btn-price").find(".rcmid-corresponding").text(aT[a1]);
				}
			   }
			 }
		    }
		  }
		}
	   });
	 }
	 listItems.each(function() {
	   var a1 = $(this);
	   var a0 = $(this).find(".select-fare-block");
	   var a2 = $(this).find(".col-info-select");
	   a0.each(function() {
		var a4 = $(this).find(".select-fare-table.multy-column.hidden-mb-small .row-head-select");
		var a5 = a4.find(".col-select:not(:eq(0))").length,
		  a6 = a4.find(".col-select.has-disabled").length;
		if (a5 === a6) {
		  var a3 = $(this).data("col-index");
		  a2.each(function() {
		    if ($(this).data("trigger-animation") === a3) {
			 $(this).addClass("has-disabled").attr("tabindex", -1);
		    }
		  });
		}
	   });
	 });
	 var aW = ak1.closest(".wrap-content-fs").next().find(".flight-list-item").find(".column-trigger-animation");
	 var ar = ak1.closest(".wrap-content-fs").next().find(".flight-list-item").find(".column-trigger-animation-1");
	 if (ak.closest('.wrap-content-fs').index() >= 0 && ak1.closest(".wrap-content-fs").next().length > 0) {
	   //	            if (ak.closest(".wrap-content-fs").index()<=$(".wrap-content-fs").length-1) {
	   var aK = [];
	   var aB = ak1.closest(".wrap-content-fs").next();
	   var aF = aB.find(".col-info-left");
	   C = ak1.attr("data-price-segment");
	   aW.each(function(a0) {
		$(this).find(".price-cheapest-outbound").text(C);
		$(this).closest(".flight-list-item").find("[data-hidden-recommended]").find(".price-cheapest-colum").text(C);
		var a2 = $(this).find(".price-cheapest-colum").text();
		var a1 = $(this).find(".price-cheapest-outbound").text();
		if (a2 && a1) {
		  var a3 = parseFloat(a2) - parseFloat(a1);
		  if (a3 < 0) {
		    a3 = 0;
		  }
		  var a7 = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		  var a6 = "";
		  if (a7 == 0) {
		    a6 = "hidden";
		  }
		  var a5 = $(this);
		  var a4 = a3.toFixed(2).indexOf(".");
		  a5.find(".price").text("+ " + parseFloat(a3.toFixed(2).slice(0, a4)).toLocaleString());
		  a5.find(".price").append("<small></small>");
		  a5.find(".price").find("small").text(a3.toFixed(a7).slice(a4)).addClass(a6);
		  a5.find(".price").append('<span class="compare-number hidden">' + a3 + "</span>");
		}
	   });
	   aF.each(function(a0) {
		aK.push($(this).find(".price").find(".compare-number").eq(0).text());
	   });
	   aK.sort(function(a1, a0) {
		return parseFloat(a1) - parseFloat(a0);
	   });
	   aF.each(function() {
		if (parseFloat($(this).find(".flight-price").find(".compare-number").eq(0).text()) === parseFloat(aK[0])) {
		  $(this).find(".head-col").append('<span class="label-status anim-all"></span');
		}
	   });
	   ar.each(function(a2) {
		$(this).find(".price-cheapest-outbound-1").text(C);
		$(this).closest(".flight-list-item").find("[data-hidden-recommended-1]").find(".price-cheapest-colum").text(C);
		var a5 = $(this).find(".price-cheapest-colum").text();
		var a0 = $(this).find(".price-cheapest-outbound-1").text();
		if (a5 && a0) {
		  var a1 = parseFloat(a5) - parseFloat(a0);
		  if (a1 < 0) {
		    a1 = 0;
		  }
		  var a7 = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		  var a6 = "";
		  if (a7 == 0) {
		    a6 = "hidden";
		  }
		  var a4 = $(this);
		  var a3 = a1.toFixed(2).indexOf(".");
		  a4.find(".price").text("+ " + parseFloat(a1.toFixed(2).slice(0, a3)).toLocaleString());
		  a4.find(".price").append("<small></small>");
		  a4.find(".price").find("small").text(a1.toFixed(a7).slice(a3)).addClass(a6);
		}
	   });
	   var ay = ak1.closest(".wrap-content-fs").next().find("[data-hidden-recommended]").find(".col-select").find(".btn-price");
	   var aN = ak1.closest(".wrap-content-fs").next().find("[data-hidden-recommended-1]").find(".col-select").find(".btn-price");
	   ay.each(function(a0) {
		var a1 = $(this).closest("[data-hidden-recommended]").find(".price-cheapest-colum").text();
		var a3 = $(this).attr("data-price-segment");
		var a7 = parseFloat(a3) - parseFloat(a1);
		var a2 = a7.toFixed(2);
		if (a7 < 0) {
		  a7 = 0;
		}
		var a6 = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		var a5 = "";
		if (a6 == 0) {
		  a5 = "hidden";
		}
		var a4 = a2.indexOf(".");
		if (a3 !== "") {
		  /*
								if(a7==0){
									var zeros="";
									if($(this).find(".btn-price-cheapest-1").next().hasClass("unit-small")){
									$(this).find(".btn-price-cheapest-1").next().remove();
								   }
									if($(this).find(".btn-price-cheapest").next().hasClass("unit-small")){
									$(this).find(".btn-price-cheapest").next().remove();
								   }
									for(var i=0;i<a6;i++){zeros=zeros+"0";}
								    $(this).find(".btn-price-cheapest").empty().text("+ 0");
								    $(this).find(".btn-price-cheapest").append("<span class='unit-small "+aI+"'>."+zeros+"</span>");
								    $(this).find(".btn-price-cheapest-1").empty().text("+ 0");
								    $(this).find(".btn-price-cheapest-1").append("<span class='unit-small "+aI+"'>."+zeros+"</span>");
								    $(this).attr("data-price-segment-after", "0.00");
								}else{
									$(this).find(".unit-small").text("");
								    $(this).find(".btn-price-cheapest").empty().text("+ " + parseFloat(a7.toFixed(2).slice(0, a4)).toLocaleString());
								    $(this).find(".btn-price-cheapest").append("<span class='unit-small "+aI+"'>"+a7.toFixed(a6).slice(a4)+"</span>");
								    $(this).find(".btn-price-cheapest-1").empty().text("+ " + parseFloat(a7.toFixed(2).slice(0, a4)).toLocaleString());
								    $(this).find(".btn-price-cheapest-1").append("<span class='unit-small "+aI+"'>"+a7.toFixed(a6).slice(a4)+"</span>");
								    $(this).attr("data-price-segment-after", a7.toFixed(a6).slice(0, a4));
								}

							*/
		  $(this).find(".unit-small").text("");
		  $(this).find(".btn-price-cheapest").empty().html("+ " + parseFloat(a7.toFixed(2).slice(0, a4)).toLocaleString() + "<small class='unit-small " + a5 + "'>" + a7.toFixed(a6).slice(a4) + "<small>");
		  $(this).attr("data-price-segment-after", a7.toFixed(a6).slice(0, a4));
		  $(this).find(".btn-price-cheapest-1").empty().html("+ " + parseFloat(a7.toFixed(2).slice(0, a4)).toLocaleString() + "<small class='unit-small " + a5 + "'>" + a7.toFixed(a6).slice(a4) + "<small>");
		  $(this).attr("data-price-segment-after", a7.toFixed(a6).slice(0, a4));
		}
	   });
	   aN.each(function(a3) {
		var a5 = $(this).closest("[data-hidden-recommended-1]").find(".price-cheapest-colum").text();
		var a4 = $(this).attr("data-price-segment");
		var a0 = parseFloat(a4) - parseFloat(a5);
		var a2 = a0.toFixed(2);
		if (a0 < 0) {
		  a0 = 0;
		}
		var a7 = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		var a6 = "";
		if (a7 == 0) {
		  a6 = "hidden";
		}
		var a1 = a2.indexOf(".");
		if (a4 !== "") {
		  /*
								if(a0==0){
									var zeros="";
									for(var i=0;i<a7;i++){zeros=zeros+"0";}
									if($(this).find(".btn-price-cheapest-1").next().hasClass("unit-small")){
									$(this).find(".btn-price-cheapest-1").next().remove();
								   }
									if($(this).find(".btn-price-cheapest").next().hasClass("unit-small")){
									$(this).find(".btn-price-cheapest").next().remove();
								   }
								    $(this).find(".btn-price-cheapest").empty().text("+ 0");
								    $(this).find(".btn-price-cheapest").append("<span class='unit-small "+aI+"'>."+zeros+"</span>");
								    $(this).find(".btn-price-cheapest-1").empty().text("+ 0");
								    $(this).find(".btn-price-cheapest-1").append("<span class='unit-small "+aI+"'>."+zeros+"</span>");
								    $(this).attr("data-price-segment-after", "0.00");
								}else{
									$(this).find(".unit-small").text("");
								    $(this).find(".btn-price-cheapest").empty().text("+ " + parseFloat(a0.toFixed(2).slice(0, a1)).toLocaleString());
								    $(this).find(".btn-price-cheapest").append("<span class='unit-small "+aI+"'>"+a0.toFixed(a7).slice(a1)+"</span>");
								    $(this).find(".btn-price-cheapest-1").empty().text("+ " + parseFloat(a0.toFixed(2).slice(0, a1)).toLocaleString());
								    $(this).find(".btn-price-cheapest-1").append("<span class='unit-small "+aI+"'>"+a0.toFixed(a7).slice(a1)+"</span>");
								    $(this).attr("data-price-segment-after", a0.toFixed(2).slice(0, a1));
								}

							*/
		  $(this).find(".unit-small").text("");
		  $(this).find(".btn-price-cheapest").empty().html("+ " + parseFloat(a0.toFixed(2).slice(0, a1)).toLocaleString() + "<small class='unit-small " + a6 + "'>" + a0.toFixed(a7).slice(a1) + "<small>");
		  $(this).attr("data-price-segment-after", a0.toFixed(2).slice(0, a1));
		  $(this).find(".btn-price-cheapest-1").empty().html("+ " + parseFloat(a0.toFixed(2).slice(0, a1)).toLocaleString() + "<small class='unit-small " + a6 + "'>" + a0.toFixed(a7).slice(a1) + "<small>");
		  $(this).attr("data-price-segment-after", a0.toFixed(2).slice(0, a1));
		}
	   });
	 }
    } else {
	 // prototype
	 if ($('body').hasClass('sk-ut-flight-search-a')) {
	   if ($(this).closest('[data-returntrip]').data('returntrip') === 0 && ($(this).data('header-class') !== 'Economy Super Saver' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		return false;
	   } else if ($(this).closest('[data-returntrip]').data('returntrip') === 1 && ($(this).data('header-class') !== 'Economy Lite' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		return false;
	   }
	 }

	 if ($('body').hasClass('sk-ut-flight-search-b')) {
	   if ($(this).closest('[data-returntrip]').data('returntrip') === 0 && ($(this).data('header-class') !== 'Economy Standard' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 3)) {
		return false;
	   } else if ($(this).closest('[data-returntrip]').data('returntrip') === 1 && ($(this).data('header-class') !== 'Economy Flexi' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		return false;
	   }
	 }
	 if (!$(this).hasClass('btn-price-cheapest-select')) {
	   var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
	   changeFlightItem.each(function() {
		$(this).addClass('has-choose');
	   });
	 }

	 var arrayPrice1, arrayPrice2;
	 wrapContentList = $('.wrap-content-list');
	 wrapContents = wrapContentList.find('.wrap-content-fs');
	 var wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');
	 clickedBtn = $(this);

	 el = $(this);
	 blockParentWrap = el.closest('.wrap-content-fs');

	 var dataTriggerAnimation = $('.wrap-content-fs').find('[data-flight-item]');
	 if (!$(this).hasClass('btn-price-cheapest-select')) {
	   dataTriggerAnimation.each(function() {
		if ($(this).hasClass('active')) {
		  $(this).closest('.flight-list-item').find('.col-info').removeClass('selected-item')
		  $(this).find('.less-detail').get(0).click();
		}
	   });
	 }
	 blockParentWrap.removeClass('selected-item');
	 if (el.closest('.wrap-content-fs').index() === 0) {
	   wrapContentNotFirst.find('.change-flight-item.bgd-white').addClass('hidden');
	   wrapContentNotFirst.find('.economy-slider , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
	   wrapContentNotFirst.find('.col-select').find('.btn-price').removeClass('active');

	 }

	 var listItems = wrapContentNotFirst.find('.flight-list-item');
	 var listCol = listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
	 var listPrice = listItems.find('.btn-price').find('.list-price');
	 var oneCol = listItems.find('.one-column');
	 var contentListPrice = $(this).find('.list-price').text();

	 // el.closest('.select-fare-block').addClass('selected-item');
	 if (el.hasClass('column-left')) {
	   el.closest('.flight-list-item').find('.column-trigger-animation').addClass('selected-item');
	 } else if (el.hasClass('column-right')) {
	   el.closest('.flight-list-item').find('.column-trigger-animation-1').addClass('selected-item');
	 }

	 upgradeAdditional();
	 blockParentWrap = el.closest('.wrap-content-fs');
	 blockParentColSelect = blockParentWrap.find('.col-select');
	 blockParentColSelect.find('.btn-price').removeClass('active');
	 $('.col-select').find('.btn-price').removeClass('active');
	 $('.upsell').addClass('hidden');
	 $(this).addClass('active');
	 var responsedataJSON = $.parseJSON($('#selectflightJson').val());
	 var data1 = responsedataJSON.response;
	 var data2 = data1;
	 var currencyCode = data1.currency.code;
	 var minFareMap = {};
	 var fareFamilyToTrigger;
	 var lowestPeyrecommendationId;
	 $.each(data1.recommendations, function(i, recommendation) {
	   var fareFamilyPremiumEconomy;
	   $.grep(data1.fareFamilies, function(fareFamily, idx) {
		if (fareFamily.fareFamily === recommendation.fareFamily) {
		  fareFamilyPremiumEconomy = fareFamily;
		}
	   });

	   if (fareFamilyPremiumEconomy.cabinClass === 'S') {
		if (fareFamilyPremiumEconomy.cabinClass in minFareMap) {
		  if (minFareMap[fareFamilyPremiumEconomy.cabinClass] > recommendation.fareSummary.fareDetailsPerAdult.totalAmount) {
		    var minFare = recommendation.fareSummary.fareDetailsPerAdult.totalAmount;
		    var precisionLoc = responsedataJSON.response.currency.precision;
		    if (null == precisionLoc || undefined == precisionLoc) {
			 precisionLoc = 2;
		    }
		    minFare = window.accounting.formatNumber(minFare, precisionLoc);
		    minFareMap[fareFamilyPremiumEconomy.cabinClass] = minFare;
		    lowestPeyrecommendationId = recommendation.recommendationID
		  }
		} else {
		  var minFare = recommendation.fareSummary.fareDetailsPerAdult.totalAmount;
		  var precisionLoc = responsedataJSON.response.currency.precision;
		  if (null == precisionLoc || undefined == precisionLoc) {
		    precisionLoc = 2;
		  }
		  minFare = window.accounting.formatNumber(minFare, precisionLoc);
		  minFareMap[fareFamilyPremiumEconomy.cabinClass] = minFare;
		  lowestPeyrecommendationId = recommendation.recommendationID
		}
	   }
	 });
	 globalJson.lowestPeyrecommendationId = lowestPeyrecommendationId;

	 if ($(this).hasClass('keep-my-selection')) {
	   var upsell = $(this).closest('[data-hidden-recommended]').find('.upsell');
	   if (blockParentWrap.index() === 0) {
		upsell.removeClass('hidden');
		upsell.removeClass('hidden');
	   } else {
		// hiddenBlockClick(minFareMap);
	   }
	   setTimeout(function() {
		upsell.find('input[name="btn-keep-selection"]').focus();
	   }, 100);
	 } else {
	   // hiddenBlockClick(minFareMap);
	 }
	 if (!jQuery.isEmptyObject(minFareMap)) {
	   $(".upgrade-economy-item").find(".sgd-price").text(currencyCode + " " + minFareMap["S"]);
	 }
	 if (el.closest('.wrap-content-fs').index() !== 0) {
	   return;
	 }
	 if ($(".tripType").val() == 'O') {
	   return;
	 }
	 listCol.addClass('has-disabled').attr('tabindex', -1);
	 oneCol.each(function() {
	   if (!$(this).hasClass('hidden')) {
		var thisIdx = $(this).closest('.select-fare-block').data('col-index');
		var thisColSelect = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
		thisColSelect.each(function() {
		  if ($(this).data('trigger-animation') === thisIdx) {
		    $(this).addClass('has-disabled').attr('tabindex', -1);
		  }
		});
	   }
	 });
	 var listRcmID = listItems.find('.btn-price').find('.rcmid');
	 var rcmIdListPrice = $(this).find('.rcmid-out').text();
	 var rcmidListPriceTrim = rcmIdListPrice.trim();
	 var arrayRcmId1 = rcmidListPriceTrim.split(" ");
	 if (listRcmID.length) {
	   listRcmID.each(function() {
		var self = $(this);
		var arrayList = self.text().split("-");
		for (var j = 0; j < arrayList.length; j++) { // 21 63
		  for (var z = 0; z < arrayList[j].length; z++) { //
		    for (var i = 0; i < arrayRcmId1.length; i++) { // 8
			 if (arrayList[z] !== null || arrayList[z] !== "undefined" || arrayList[z] !== undefined) {
			   if (arrayList[z]) {
				if (arrayRcmId1[i] === arrayList[z].slice(0, 4).trim()) {
				  var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
				  var thisCol = $(this).closest('.col-select');
				  var idx = thisCol.index();
				  var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
				  listRow.each(function() {
				    $(this).find('.col-select').eq(idx).removeClass('has-disabled');
				  });
				  thisColSelectOneCol.removeClass('has-disabled');
				  $(this).closest('.btn-price').find('.btn-price-cheapest-colum').text(arrayList[z].slice(3).trim());
				  $(this).closest('.btn-price').find('.rcmid-corresponding').text(arrayRcmId1[i]);
				}
			   }
			 }
		    }
		  }
		}
	   });
	 }
	 listItems.each(function() {
	   var that = $(this);
	   var thisFareBlock = $(this).find('.select-fare-block');
	   var listColInfoSel = $(this).find('.col-info-select');
	   thisFareBlock.each(function() {
		var listRowHeadSel = $(this).find('.select-fare-table.multy-column.hidden-mb-small .row-head-select');
		var listColSelLength = listRowHeadSel.find('.col-select:not(:eq(0))').length,
		  listColSelDisabledLength = listRowHeadSel.find('.col-select.has-disabled').length;
		if (listColSelLength === listColSelDisabledLength) {
		  var idx = $(this).data('col-index');
		  listColInfoSel.each(function() {
		    if ($(this).data('trigger-animation') === idx) {
			 $(this).addClass('has-disabled').attr('tabindex', -1);
		    }
		  });
		}
	   });
	 });
	 var flightColumnTrigger = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation');
	 var flightColumnTrigger1 = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation-1');
	 if (el.closest('.wrap-content-fs').index() === 0) {
	   var arrPriceCheapest = [];
	   var blockWrapFirst = $('.wrap-content-fs').last();
	   var colInfoLeft = blockWrapFirst.find('.col-info-left');
	   //colInfoLeft.find('.head-col').find('.label-status').remove();
	   priceCurrentSelected = el.find('.btn-price-cheapest-colum').text();
	   flightColumnTrigger.each(function(idx) {
		$(this).find('.price-cheapest-outbound').text(priceCurrentSelected);
		$(this).closest('.flight-list-item').find('[data-hidden-recommended]').find('.price-cheapest-colum').text(priceCurrentSelected);
		var priceCheapest = $(this).find('.price-cheapest-colum').text();
		var priceSelectedOutBound = $(this).find('.price-cheapest-outbound').text();
		if (priceCheapest && priceSelectedOutBound) {
		  var totalPriceCabin = parseFloat(priceCheapest) - parseFloat(priceSelectedOutBound);
		  if (totalPriceCabin < 0) {
		    totalPriceCabin = 0;
		  }
		  var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		  var hiddenPrecision = '';
		  if (preCode == 0) {
		    hiddenPrecision = "hidden";
		  }
		  var triggerAnimation = $(this);
		  var priceStyleSmall = totalPriceCabin.toFixed(2).indexOf(".");
		  triggerAnimation.find('.price').text("+ " + parseFloat(totalPriceCabin.toFixed(2).slice(0, priceStyleSmall)).toLocaleString());
		  triggerAnimation.find('.price').append("<small></small>");
		  triggerAnimation.find('.price').find('small').text(totalPriceCabin.toFixed(preCode).slice(priceStyleSmall)).addClass(hiddenPrecision);
		  triggerAnimation.find('.price').append('<span class="compare-number hidden">' + totalPriceCabin + '</span>');
		}
		// }
	   });
	   colInfoLeft.each(function(idexLabel) {
		arrPriceCheapest.push($(this).find('.price').find('.compare-number').eq(0).text());
	   });
	   arrPriceCheapest.sort(function(a, b) {
		return parseFloat(a) - parseFloat(b);
	   });
	   colInfoLeft.each(function() {
		if (parseFloat($(this).find('.flight-price').find('.compare-number').eq(0).text()) === parseFloat(arrPriceCheapest[0])) {
		  $(this).find('.head-col').append('<span class="label-status anim-all"></span');
		}
	   })
	   flightColumnTrigger1.each(function(idx1) {
		$(this).find('.price-cheapest-outbound-1').text(priceCurrentSelected);
		$(this).closest('.flight-list-item').find('[data-hidden-recommended-1]').find('.price-cheapest-colum').text(priceCurrentSelected);
		// if(!$(this).hasClass('has-disabled')){
		var priceCheapest1 = $(this).find('.price-cheapest-colum').text();
		var priceSelectedOutBound1 = $(this).find('.price-cheapest-outbound-1').text();
		if (priceCheapest1 && priceSelectedOutBound1) {
		  var totalPriceCabin1 = parseFloat(priceCheapest1) - parseFloat(priceSelectedOutBound1);
		  if (totalPriceCabin1 < 0) {
		    totalPriceCabin1 = 0;
		  }
		  var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		  var hiddenPrecision = '';
		  if (preCode == 0) {
		    hiddenPrecision = "hidden";
		  }
		  var triggerAnimation1 = $(this);
		  var priceStyleSmall1 = totalPriceCabin1.toFixed(2).indexOf(".");
		  triggerAnimation1.find('.price').text("+ " + parseFloat(totalPriceCabin1.toFixed(2).slice(0, priceStyleSmall1)).toLocaleString());
		  triggerAnimation1.find('.price').append("<small></small>");
		  triggerAnimation1.find('.price').find('small').text(totalPriceCabin1.toFixed(preCode).slice(priceStyleSmall1)).addClass(hiddenPrecision);
		}
		// }
	   });
	   var colSelect = $('.wrap-content-fs').last().find('[data-hidden-recommended]').find('.col-select').find('.btn-price');
	   var colSelect1 = $('.wrap-content-fs').last().find('[data-hidden-recommended-1]').find('.col-select').find('.btn-price');
	   colSelect.each(function(idxColSelect) {
		var priceCheapestColumn = $(this).closest('[data-hidden-recommended]').find('.price-cheapest-colum').text();
		var priceCheapestColumnCabin = $(this).find(".btn-price-cheapest-colum").text();

		var totalPriceBtnColumn = parseFloat(priceCheapestColumnCabin) - parseFloat(priceCheapestColumn);
		var toFixedPrice = totalPriceBtnColumn.toFixed(2);
		if (totalPriceBtnColumn < 0) {
		  totalPriceBtnColumn = 0;
		}
		var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		var hiddenPrecision = '';
		if (preCode == 0) {
		  hiddenPrecision = "hidden";
		}
		var priceStyleUnitSmall = toFixedPrice.indexOf(".");
		if (priceCheapestColumnCabin !== '') {
		  $(this).find('.unit-small').text("");
		  $(this).find('.btn-price-cheapest').empty().html("+ " + parseFloat(totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall)).toLocaleString() +
		    "<small class='unit-small " + hiddenPrecision + "'>" + totalPriceBtnColumn.toFixed(preCode).slice(priceStyleUnitSmall) + "<small>");
		  $(this).find('.btn-price-cheapest-1').empty().html("+ " + parseFloat(totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall)).toLocaleString() +
		    "<small class='unit-small " + hiddenPrecision + "'>" + totalPriceBtnColumn.toFixed(preCode).slice(priceStyleUnitSmall) + "<small>");
		  $(this).attr('data-price-segment-after', totalPriceBtnColumn.toFixed(preCode).slice(0, priceStyleUnitSmall));
		}
	   });
	   colSelect1.each(function(idxColSelect1) {
		var priceCheapestColumn1 = $(this).closest('[data-hidden-recommended-1]').find('.price-cheapest-colum').text();
		var priceCheapestColumnCabin1 = $(this).find(".btn-price-cheapest-colum").text();
		var totalPriceBtnColumn1 = parseFloat(priceCheapestColumnCabin1) - parseFloat(priceCheapestColumn1);
		var toFixedPrice1 = totalPriceBtnColumn1.toFixed(2);
		if (totalPriceBtnColumn1 < 0) {
		  totalPriceBtnColumn1 = 0;
		}
		var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		var hiddenPrecision = '';
		if (preCode == 0) {
		  hiddenPrecision = "hidden";
		}
		var priceStyleUnitSmall1 = toFixedPrice1.indexOf(".");
		if (priceCheapestColumnCabin1 != '') {
		  $(this).find('.unit-small').text("");
		  $(this).find('.btn-price-cheapest').empty().html("+ " + parseFloat(totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1)).toLocaleString() +
		    "<small class='unit-small " + hiddenPrecision + "'>" + totalPriceBtnColumn1.toFixed(preCode).slice(priceStyleUnitSmall1) + "<small>");
		  $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
		  $(this).find('.btn-price-cheapest-1').empty().html("+ " + parseFloat(totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1)).toLocaleString() +
		    "<small class='unit-small " + hiddenPrecision + "'>" + totalPriceBtnColumn1.toFixed(preCode).slice(priceStyleUnitSmall1) + "<small>");
		  $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
		  //        		}
		}
	   });
	 }
    }
    $('[data-trigger-animation]').not('.has-disabled').not('.not-available').attr('tabindex', 0);
    // added for Fare increment is wrong bug fix
    var index = thisItem.closest(".wrap-content-fs").index();
    var len = $(".wrap-content-fs").length - 1;
    var nextWrapContent = $(".wrap-content-fs").next();
    var firstColumn = nextWrapContent.find("[data-hidden-recommended]");
    var secondColumn = nextWrapContent.find("[data-hidden-recommended-1]");
    firstColumn.each(function(aaZ) {
	 var lowestFare = 0.00;
	 var lowestFareCount = 0;
	 var lowestPrec;
	 var lowestFareString = "";
	 var buttons;
	 if ($(this).find(".select-fare-table.multy-column.hidden").length == 0) {
	   buttons = $(this).find(".select-fare-table.multy-column:first").find(".col-select:not(first,.has-disabled)").find(".btn-price");
	 } else {
	   buttons = $(this).find(".one-column:not(.hidden)").find(".col-select").find(".btn-price");
	 }
	 var amountArray = [];
	 buttons.each(function(aZ) {
	   var amount1 = $(this).closest("[data-hidden-recommended]").find(".price-cheapest-colum").text();
	   //            var amount2 = $(this).find(".btn-price-cheapest-colum").text();
	   var amount2 = 0;
	   if ($(".tripType").val() == "M") {
		amount2 = $(this).attr("data-price-segment");
	   } else {
		amount2 = $(this).find(".btn-price-cheapest-colum").text();
	   }
	   var offsetAmount = parseFloat(amount2) - parseFloat(amount1);
	   var offsetAmount1 = offsetAmount.toFixed(2);
	   if (offsetAmount < 0) {
		offsetAmount = 0;
	   }
	   amountArray.push(offsetAmount);
	 });
	 var preVal = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
	 var hiddenFlag = "";
	 var totalPrec = 2;
	 if (preVal == 0) {
	   hiddenFlag = "hidden";
	 } else {
	   totalPrec = preVal;
	 }
	 if (amountArray.length > 0) {
	   amountArray.sort(function sortNumber(a, b) {
		return a - b;
	   });
	   var offsetAmount1 = amountArray[0].toFixed(totalPrec);
	   var precIndex = offsetAmount1.indexOf(".");
	   var lowestFare = parseFloat(offsetAmount1.slice(0, precIndex));
	   var data = parseFloat(lowestFare.toFixed(2).slice(0, precIndex)).toLocaleString();
	   if (amountArray[0] == 0) {
		var zeros = "";
		for (var i = 0; i < preVal; i++) {
		  zeros = zeros + "0";
		}
		lowestFareString = "0<small class=" + hiddenFlag + ">." + zeros + "</small><span class='compare-number hidden'>0." + zeros + "</span>";
	   } else {
		lowestFareString = data + "<small class=" + hiddenFlag + ">" + offsetAmount1.slice(precIndex) + "</small><span class='compare-number hidden'>" + lowestFare + offsetAmount1.slice(precIndex) + "</span>";
	   }
	   $(this).closest(".flight-list-item").find(".column-trigger-animation").find(".price").html("+" + lowestFareString);
	 }

    });

    secondColumn.each(function(aaZ) {
	 var lowestFare = 0.00;
	 var lowestFareCount = 0;
	 var lowestPrec;
	 var lowestFareString = "";
	 var buttons;
	 if ($(this).find(".select-fare-table.multy-column.hidden").length == 0) {
	   buttons = $(this).find(".select-fare-table.multy-column:first").find(".col-select:not(first,.has-disabled)").find(".btn-price");
	 } else {
	   buttons = $(this).find(".one-column:not(.hidden)").find(".col-select").find(".btn-price");
	 }
	 var amountArray = [];
	 buttons.each(function(aZ) {
	   var amount1 = $(this).closest("[data-hidden-recommended-1]").find(".price-cheapest-colum").text();
	   var amount2 = 0;
	   if ($(".tripType").val() == "M") {
		amount2 = $(this).attr("data-price-segment");
	   } else {
		amount2 = $(this).find(".btn-price-cheapest-colum").text();
	   }
	   var offsetAmount = parseFloat(amount2) - parseFloat(amount1);
	   var offsetAmount1 = offsetAmount.toFixed(2);
	   if (offsetAmount < 0) {
		offsetAmount = 0;
	   }
	   amountArray.push(offsetAmount);
	 });
	 var preVal = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
	 var hiddenFlag = "";
	 var totalPrec = 2;
	 if (preVal == 0) {
	   hiddenFlag = "hidden";
	 } else {
	   totalPrec = preVal;
	 }
	 if (amountArray.length > 0) {
	   amountArray.sort(function sortNumber(a, b) {
		return a - b;
	   });
	   var offsetAmount1 = amountArray[0].toFixed(totalPrec);
	   var precIndex = offsetAmount1.indexOf(".");
	   var lowestFare = parseFloat(offsetAmount1.slice(0, precIndex));
	   var data = parseFloat(lowestFare.toFixed(2).slice(0, precIndex)).toLocaleString();
	   if (amountArray[0] == 0) {
		var zeros = "";
		for (var i = 0; i < preVal; i++) {
		  zeros = zeros + "0";
		}
		lowestFareString = "0<small class=" + hiddenFlag + ">." + zeros + "</small><span class='compare-number hidden'>0." + zeros + "</span>";
	   } else {
		lowestFareString = data + "<small class=" + hiddenFlag + ">" + offsetAmount1.slice(precIndex) + "</small><span class='compare-number hidden'>" + lowestFare + offsetAmount1.slice(precIndex) + "</span>";
	   }
	   $(this).closest(".flight-list-item").find(".column-trigger-animation-1").find(".price").html("+" + lowestFareString);
	 }
    });
  });

	$(document).off('click.closeModal').on('click.closeModal', 'input[name="proceed-fare"], input[name="proceed-fare-1"]', function(){

    var el = $(this);
    var wrapContentList = $('.wrap-content-list');
    var wrapContents = wrapContentList.find('.wrap-content-fs');
    var thisWrapContentFs;
    var selectFareBlock = el.closest('.select-fare-block');
    var modalId = selectFareBlock.data('col-index');
    var btnPrice = selectFareBlock.find('.btn-price');
    var btnPriceActive = selectFareBlock.find('.btn-price.active');
    var btnPriceCheapestColum = selectFareBlock.find('.btn-price.active .btn-price-cheapest-colum').text();
    var btnPriceCheapest = selectFareBlock.find('.btn-price.active .btn-price-cheapest, .btn-price.active .btn-price-cheapest-1').text();
    var unitSmall = selectFareBlock.find('.btn-price.active .unit-small').text();
    var colSelect = $('[data-trigger-animation]');
    var activeFareTable, getClassNameColor, codeOrigin, codeDestination, nameHead , listValues, rcmidCorresponding, fareFamilyId, flightId;
    var fareSummaryGroup = $('.fare-summary-group');
    var listTablesSummary = fareSummaryGroup.find('.table-fare-summary.hidden-tb-dt');
    var noteFare = $('.has-note-fare');

    noteFare.show();

    el.css('pointer-events', 'none');
    colSelect.each(function() {
      if($(this).data('trigger-animation') === modalId && btnPrice.hasClass('active')) {
        thisWrapContentFs = $(this).closest('.wrap-content-fs');
        var thisFlightListItem = $(this).closest('.flight-list-item');
        var thisChangeFlightItem = thisFlightListItem.find('.change-flight-item');
        var thisHeadRecommendedSelected = thisWrapContentFs.find('.head-recommended-selected');
        flightId = thisFlightListItem.find('.segment-id').text();

        thisWrapContentFs.find('.economy-slider, .sub-logo , .monthly-view , .status-list , .flight-search-filter-economy, .loadmore-block, .flight-list-item, .recommended-table, .head-recommended').addClass('hidden');
        thisFlightListItem.removeClass('hidden');
        thisChangeFlightItem.removeClass('hidden');
        thisHeadRecommendedSelected.removeClass('hidden');
        thisWrapContentFs.next().removeClass('hidden');

        thisChangeFlightItem.find('.price').text(btnPriceCheapestColum);

        if (thisWrapContentFs.index() === ($('.wrap-content-fs').length-1)) {
          fareSummaryGroup.removeClass('hidden');
          slideshowpremiumeconomy();
          // $('[data-slideshow-premium-economy]').find('.slides')[0].slick.setPosition()
          thisChangeFlightItem.find('.price').text(btnPriceCheapest + unitSmall);
          thisHeadRecommendedSelected.html('<span>Selected return flight</span>');
        }
      };
    });

    function appendValueFareSummary() {
      activeFareTable = btnPriceActive.closest('.select-fare-table');
      getClassNameColor = activeFareTable.find('.row-head-select .col-select').removeClass('col-select').attr('class');
      codeOrigin = thisWrapContentFs.find('.code-origin-airport').first().text();
      codeDestination = thisWrapContentFs.find('.code-destination-airport').first().text();
      nameHead = activeFareTable.find('.row-head-select span').last().text();
      listValues = activeFareTable.find('.row-select .col-select span');
      eligibleRecommendationIds = activeFareTable.find('.eligible-oc-recommendation-ids').text();
      rcmidCorresponding = btnPriceActive.find('.rcmid-corresponding').text();
      fareFamilyId = btnPriceActive.siblings('.fare-family-id').text();
      if(thisWrapContentFs.index() === 0) {
        idx1 = btnPriceActive.siblings('.index-of').text();
      } else {
        idx2 = btnPriceActive.siblings('.index-of').text();
      }

      listTablesSummary.each(function(index) {
        var _this = $(this);
        var valuettt = _this.find('.ttt');

        if(index === thisWrapContentFs.index()) {
          _this.find('.head-wrapper').attr('class', 'head-wrapper').addClass(getClassNameColor);
          _this.find('.code-flight').text(codeOrigin + ' - ' + codeDestination);
          _this.find('.name-header').text(nameHead);
          listValues.each(function() {
            var desItem;
            function appendStyles(e, eDes) {
              if(e.hasClass('fare-price')) {
                eDes.addClass('fare-price');
              }
              if(e.hasClass('not-allowed')) {
                eDes.addClass('not-allowed');
              }
              if(e.hasClass('complimentary')) {
                eDes.addClass('complimentary');
              }
            }
            if ($(this).hasClass('baggage')) {
              desItem = _this.find('.baggage');
              desItem.attr('class', 'baggage').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('seat-selection')) {
              desItem = _this.find('.seat-selection');
              desItem.attr('class', 'seat-selection').text($(this).text());
              appendStyles($(this), desItem);

              // desItem.siblings('.complimentary-note').remove();
              // if($(this).siblings('.complimentary-note').length) {
              //   var seatSelectionExtra = $(this).siblings('.complimentary-note');
              //   if(!desItem.siblings('.complimentary-note').length) {
              //     desItem.parent().append(seatSelectionExtra.clone());
              //   }
              // }
            }
            if ($(this).hasClass('earn-krisFlyer')) {
              desItem = _this.find('.earn-krisFlyer');
              desItem.attr('class', 'earn-krisFlyer').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('upgrade')) {
              desItem = _this.find('.upgrade');
              desItem.attr('class', 'upgrade').text($(this).text());
              appendStyles($(this), desItem);

              if($(this).siblings('[data-tooltip]').length) {
                var upgradeTooltip = $(this).siblings('[data-tooltip]');
                if(!desItem.siblings('[data-tooltip]').length) {
                  desItem.parent().append(upgradeTooltip.clone());
                  desItem.siblings('[data-tooltip]').kTooltip();
                }
              } else {
                desItem.siblings('em').remove();
              }
            }

            if ($(this).hasClass('cancellation')) {
              desItem = _this.find('.cancellation');
              desItem.attr('class', 'cancellation').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('booking-change')) {
              desItem = _this.find('.booking-change');
              desItem.attr('class', 'booking-change').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('no-show')) {
              desItem = _this.find('.no-show');
              desItem.attr('class', 'no-show').text($(this).text());
              appendStyles($(this), desItem);
            }
          })
          valuettt.attr('name', "ttt-"+ eligibleRecommendationIds);
          valuettt.attr('value', eligibleRecommendationIds);
          if(valuettt.val() === "0"){
            fareSummaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
            fareSummaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
            fareSummaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
          } else{
            fareSummaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
            fareSummaryGroup.find('.button-group-1').find('.text').addClass('hidden');
            fareSummaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
          }
        }
      });

      // --- Compare 2 fare conditions
      if(idx1 < idx2) {
        var a1 = listTablesSummary.eq(0).find('.cancellation').attr('class');
        var a2 = listTablesSummary.eq(0).find('.cancellation').text() + ' *';
        var b1 = listTablesSummary.eq(0).find('.booking-change').attr('class');
        var b2 = listTablesSummary.eq(0).find('.booking-change').text() + ' *';
        var c1 = listTablesSummary.eq(0).find('.no-show').attr('class');
        var c2 = listTablesSummary.eq(0).find('.no-show').text() + ' *';
        listTablesSummary.eq(1).find('.cancellation').attr('class', a1).text(a2);
        listTablesSummary.eq(1).find('.booking-change').attr('class', b1).text(b2);
        listTablesSummary.eq(1).find('.no-show').attr('class', c1).text(c2);
      } else if (idx1 > idx2) {
        var a1 = listTablesSummary.eq(1).find('.cancellation').attr('class');
        var a2 = listTablesSummary.eq(1).find('.cancellation').text() + ' *';
        var b1 = listTablesSummary.eq(1).find('.booking-change').attr('class');
        var b2 = listTablesSummary.eq(1).find('.booking-change').text() + ' *';
        var c1 = listTablesSummary.eq(1).find('.no-show').attr('class');
        var c2 = listTablesSummary.eq(1).find('.no-show').text() + ' *';
        listTablesSummary.eq(0).find('.cancellation').attr('class', a1).text(a2);
        listTablesSummary.eq(0).find('.booking-change').attr('class', b1).text(b2);
        listTablesSummary.eq(0).find('.no-show').attr('class', c1).text(c2);
      } else {
        noteFare.hide();
      }
      // --- End

      fareSummaryGroup.find('#recommendation-id').val(rcmidCorresponding);
      if(thisWrapContentFs.index() === wrapContents.length-1) {
        fareSummaryGroup.find('#fare-family-inbound').val(fareFamilyId);
        fareSummaryGroup.find('#flight-inbound').val(flightId.trim());
        $('.flight-search-summary-conditions').find('.tab-item-2').text(codeOrigin + ' - ' + codeDestination);
        $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');
      } else {
        fareSummaryGroup.find('#fare-family-outbound').val(fareFamilyId);
        fareSummaryGroup.find('#flight-outbound').val(flightId.trim());
        $('.flight-search-summary-conditions').find('.tab-item-1').text(codeOrigin + ' - ' + codeDestination);
        $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

        showCorrelativeTab();
      }
    }

    // scrollTop func
    var scrollTopToContent = function() {
      setTimeout(function(){
        $('html, body').animate({
          scrollTop: $(thisWrapContentFs).offset().top - 10
        }, 0);
      }, 750);
    }

    appendValueFareSummary();
    handleActionSlider();
    showMoreLessDetails();
    scrollTopToContent();

   $('.popup-table-flight').Popup('hide');
  });

  $(document).on('click', '.group-btn > #btn-keep-selection', function(e) {
    e.preventDefault();
    el = $(this);
    if (!$(this).hasClass('btn-price-cheapest-select')) {
	 var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
	 changeFlightItem.each(function() {
	   $(this).addClass('has-choose');
	 });
    }
    // hiddenBlockClick();

    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').trigger('click');
  });

  $(document).on('click', '.group-btn > #btn-upgrade', function(e) {
    e.preventDefault();
    if ($('body').hasClass('sk-ut-workflow')) {
	 return false;
    }
    var btnPriceActive = $(this).closest('.flight-list-item').find('.col-select').find('.btn-price.active');
    var parentBtn = btnPriceActive.closest('.col-select');

    var prevUpsell = $(this).closest('.upsell').prev().find('.col-select');
    var btnEqActive = prevUpsell.find('.btn-price');
    // btnEqActive.eq(1).addClass('item-next-active');
    // var btnPriceNext = parentBtn.next().find('.btn-price.item-next-active');
    var btnPriceNext = parentBtn.next().find('.btn-price');
    if (btnPriceNext.length) {
	 btnPriceActive.removeClass('active');
	 btnPriceNext.addClass('active');
	 btnPriceNext.trigger('click');
	 var priceActive = $(this).closest('[data-hidden-recommended]').find('.btn-price.active');
	 var textPriceActive = priceActive.find('.header-family').text();
	 var priceNext = priceActive.find('.btn-price-cheapest-colum').text();
	 var priceCurrent = $(this).closest('.upsell').find('.price-selected ').text();
	 var minus = parseFloat(priceNext) - parseFloat(priceCurrent);
	 $(this).closest('.upsell').find('.name-family').text(textPriceActive);
	 if (btnPriceNext.length) {
	   $(this).closest('.upsell').find('.price-sgd').text(" SGD " + minus.toLocaleString(undefined, {
		minimumFractionDigits: 2
	   }));
	 }
	 $(this).closest('.upsell').find('.price-selected ').text(priceNext);
	 // hiddenBlockClick();
    }

    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').trigger('click');
  });


  $(document).on('click', '.button-group-1 > .button-change', function(e) {
    var wrapContentBlock = $(this).closest('.wrap-content-fs');
    var buttonSelected = $(this).closest('.recommended-flight-block').find('[data-trigger-animation].selected-item');
    if (buttonSelected.length) {
	 buttonSelected.get(0).click();
    }

    setTimeout(function() {
	 $(window).scrollTop(wrapContentBlock.offset().top - $('[data-booking-summary-panel]').innerHeight() - 20);
    }, 500);

    wrapContentBlock.find('.select-fare-block.selected-item').addClass('active');
    if ($('.main-inner').find('.wrap-content-fs').length > 0) { //kp
	 wrapContentBlock.find('.upsell').addClass('hidden');
	 wrapContentBlock.find('.change-flight-item.bgd-white').addClass('hidden');
	 wrapContentBlock.find('.economy-slider , .fs-status-list , .monthly-view , .sub-logo , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
	 wrapContentBlock.find('.col-select').find('.btn-price').removeClass('active');
	 if ($(".tripType").val() == "M") {
	   globalJson.MulticityBookingSummaryData = [];
	   var indexs = $(this).closest('.wrap-content-fs').index();
	   $(".main-inner").find(".wrap-content-fs").each(function(idxs, item) {
		if (indexs <= idxs) {
		  globalJson.MulticityBookingSummaryData.push({
		    "itemIndex": idxs
		  });
		}
		if (indexs < idxs) {

		  $(this).addClass("hidden");
		  $(this).find(".economy-slider , .fs-status-list , .monthly-view , .sub-logo , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended").removeClass("hidden");
		  $(this).find(".change-flight-item.bgd-white").addClass("hidden");
		  $(this).next().find(".col-select").find(".btn-price").removeClass("active");
		}
	   });
	   $('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
	   $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
	 } else {
	   if (wrapContentBlock.index() === 0) {
		wrapContentBlock.next().addClass('hidden');
		wrapContentBlock.next().find('.col-select').find('.btn-price').removeClass('active');
		globalJson.selectedFlightsData = [];
	   } else {
		$('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
		$('.flight-search-summary-conditions').find('.tab-left').trigger('click');
		var data = globalJson.selectedFlightsData[0];
		globalJson.selectedFlightsData = [];
		globalJson.selectedFlightsData[0] = data;

	   }
	 }
	 handleLoadmore(wrapContentBlock.find('.recommended-flight-block'), wrapContentBlock.find('[data-loadmore]'));
	 showMoreLessDetails();
	 handleActionSlider();
    }
    wrapContentBlock.find('.flight-list-item').attr('tabindex', 0);
    $('form.fare-summary-group').addClass('hidden');

    var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
    changeFlightItem.each(function() {
	 if (!changeFlightItem.is(':visible')) {
	   $(this).removeClass('has-choose');
	 } else {
	   $(this).addClass('has-choose');
	 }
    });

  });

  // show 2nd tab when click link right
  function showCorrelativeTab() {
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-left', function() {
	 $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
    });
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-right', function() {
	 $('.flight-search-summary-conditions').find('.tab-right').trigger('click');
    });
  }

  var workflowTable = function(isRenderTemplate) {
    tableLoadPage(isRenderTemplate);
    var priceTable = $('.recommended-table').find('.price');
    var flightListItem = $('.flight-list-item');
    var flightList = $('.wrap-content-fs').first().find('.flight-list-item');
    priceTable.each(function(idx) {
	 var contentInner = $(this).text();
	 if ($.trim(contentInner) === "") {
	   var parentPrice = $(this).closest('.col-info-select');
	   parentPrice.addClass('not-available');
	   parentPrice.find('.flight-price').remove();
	   parentPrice.find('span.not-available').empty().text('Not available');
	 }
    });
    flightListItem.each(function(idx) {
	 var flightStationItem = $(this).find('.flight-station-item');
	 flightStationItem.each(function(idx) {
	   if (!$(this).is(':last-child')) {
		$(this).find('.less-detail').remove();
	   }
	 });
    });
  }
  var getTemplateFlightTable = function(el, data1, flightData, index, isFilter) {
    var loadmoreBlock = el.siblings('[data-loadmore]');
    if (miniFareData) {
	 $.get(global.config.url.fsEconomyMultiStopFlightTable, function(data) {
	   var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
	   var template = window._.template(data, {
		data: data1,
		familyData: miniFareData,
		flight: flightData ? flightData : data1.flights[index],
		flightIdx: index,
		labels: saar5.l4.sk.chooseflight,
		preCodeData: preCode
	   });

	   el.find('.flight-list-item').remove();
	   el.append($(template));
	   el.find('.flight-list-item:gt(4)').addClass('hidden');

	   if (isFilter && index > 0) {
		$(clickedBtn).trigger('click.calculatePriceInbound');
	   }

	   workflowTable(isFilter);

	   selectFlightAnimation();
	   showMoreLessDetails();
	   initPopup();
	   wcag();

	   // init tooltip
	   //if($('[data-tooltip]')) {
	   //  $('[data-tooltip]').kTooltip();
	   //}
	   handleLoadmore(el, loadmoreBlock);

	   var segmentsLength = el.find('.flight-list-item').length;

	   if (!segmentsLength) {
		el.addClass('hidden');
		el.siblings('.no-result-filter').removeClass('hidden');
	   } else {
		el.siblings('.no-result-filter').addClass('hidden');
		el.removeClass('hidden');
	   }
	 })
    } else {


	 var data1 = $.parseJSON($("#selectflightJson").val()).response;
	 var fareFamilyCodeString = "";

	 $.each(data1.fareFamilies, function(idx, item) {
	   if (fareFamilyCodeString == "") {
		fareFamilyCodeString = item.fareFamilyCode + ";";
	   } else {
		fareFamilyCodeString = fareFamilyCodeString + item.fareFamilyCode + ";";
	   }
	 });

	 var RBDString = null;
	 if ($(".tripType").val() === "M") {
	   $.each(data1.recommendations, function(idx, item1) {
		$.each(item1.segmentBounds, function(idx, item2) {
		  $.each(data1.fareFamilies, function(idx, item3) {
		    if (null != item1.fareFamily && item1.fareFamily.includes(item3.fareFamily)) {
			 $.each(item2.segments, function(idx, item4) {
			   $.each(item4.legs, function(idx, item5) {
				if (RBDString == null) {
				  RBDString = item3.fareFamilyCode + "_" + item5.sellingClass + ";";
				} else if (!RBDString.includes(item3.fareFamilyCode)) {
				  RBDString = RBDString + item3.fareFamilyCode + "_" + item5.sellingClass + ";";
				}
			   });
			 });
		    }
		  });
		});

	   });
	 } else {
	   $.each(data1.recommendations, function(idx, item1) {
		$.each(item1.segmentBounds, function(idx, item2) {
		  $.each(data1.fareFamilies, function(idx, item3) {
		    if (item2.fareFamily.includes(item3.fareFamilyCode)) {
			 $.each(item2.segments, function(idx, item4) {
			   $.each(item4.legs, function(idx, item5) {
				if (RBDString == null) {
				  RBDString = item3.fareFamilyCode + "_" + item5.sellingClass + ";";
				} else if (!RBDString.includes(item3.fareFamilyCode)) {
				  RBDString = RBDString + item3.fareFamilyCode + "_" + item5.sellingClass + ";";
				}
			   });
			 });
		    }
		  });
		});

	   });
	 }
	 var boardPointOrigin = $(".recommended-flight-block-0").closest(".wrap-content-fs").find("#firstBoardPointOrigin").val();
	 var boardPointDestination = $(".recommended-flight-block-0").closest(".wrap-content-fs").find("#firstBoardPointDestination").val();
	 var countryOrigin = $(".recommended-flight-block-0").closest(".wrap-content-fs").find("#firstCountryOrigin").val();
	 var countryDestination = $(".recommended-flight-block-0").closest(".wrap-content-fs").find("#firstCountryDestination").val();
	 var locale = $("#itenaryLocale").val();
	 var dataToSend = {
	   "FareFamilyCodes": fareFamilyCodeString,
	   "RBD": RBDString,
	   "boardPointOrigin": boardPointOrigin,
	   "boardPointDestination": boardPointDestination,
	   "countryOrigin": countryOrigin,
	   "countryDestination": countryDestination,
	   "locale": locale
	 }
	 var sk = $('#sk').val();

	 $.ajax({
	   url: global.config.url.getFareForm,
	   type: SIA.global.config.ajaxMethod,
	   data: dataToSend,
	   dataType: 'json',
	   success: function(response) {
		miniFareData = response
		$.get(global.config.url.fsEconomyMultiStopFlightTable, function(data) {
		  var preCode = $.parseJSON($("#selectflightJson").val()).response.currency.precision;
		  var template = window._.template(data, {
		    data: data1,
		    familyData: response,
		    flight: flightData ? flightData : data1.flights[index],
		    flightIdx: index,
		    labels: saar5.l4.sk.chooseflight,
		    preCodeData: preCode
		  });

		  el.find('.flight-list-item').remove();
		  el.append($(template));
		  el.find('.flight-list-item:gt(4)').addClass('hidden');

		  if (isFilter && index === 1) {
		    $(clickedBtn).trigger('click.calculatePriceInbound');
		  }

		  workflowTable(isFilter);

		  selectFlightAnimation();
		  showMoreLessDetails();
		  initPopup();
		  wcag();

		  // init tooltip
		  if ($('[data-tooltip]')) {
		    // $('[data-tooltip]').kTooltip();
		  }
		  handleLoadmore(el, loadmoreBlock);

		  var segmentsLength = el.find('.flight-list-item').length;

		  if (!segmentsLength) {
		    el.addClass('hidden');
		    el.siblings('.no-result-filter').removeClass('hidden');
		  } else {
		    el.siblings('.no-result-filter').addClass('hidden');
		    el.removeClass('hidden');
		  }
		})
	   }
	 });
    }
  }


  var renderFlightTable = function(data1) {
    var flightBlock = $('.recommended-flight-block');

    flightBlock.each(function(index) {
	 var self = $(this);
	 if (index === 0) {
	   getTemplateFlightTable(self, data1, null, index);
	 } else {
	   setTimeout(function() {
		getTemplateFlightTable(self, data1, null, index);
		if ($('#rebookVal').val() == "false") {
		  if ($("#isRefrestClick").val() == "false" && $("#selectedFlightIdDetailsforRefresh").val() == 'null') {
		    setTimeout(function() {
			 SIA.preloader.hide();
		    }, 1000);
		  }
		} else {
		  setTimeout(function() {
		    SIA.preloader.hide();
		  }, 500);
		}
	   }, 2000);
	 }
    })
  }
  var wcag = function() {
    var groupBtnFooter = $('.row-footer-select'),
	 tableBlocks = $('.select-fare-block '),
	 colFlightNotAvail = $('[data-trigger-animation].not-available'),
	 btnUpgrade = $('input[name="btn-upgrade"]'),
	 bookingSummaryPanel = $('.bsp-booking-summary');

    $(document).off('keydown.focusNext').on('keydown.focusNext', '.tooltip__close', function(e) {
	 var code = e.keyCode || e.which;
	 isSAGroup = $(this).siblings('.tooltip__content').find('[data-tooltip-sagroup]').length,
	   tootipIdx = $('[data-tooltip-sagroup]').data('tooltip-sagroup');

	 if (code === 13 && isSAGroup) {
	   var input = $('input[name="codeshare-' + tootipIdx + '"]');
	   setTimeout(function() {
		input.focus();
	   }, 100)
	 }
    })

    colFlightNotAvail.attr('tabindex', -1);

    tableBlocks.each(function() {
	 $(this).find('[data-tooltip], a').attr('tabindex', -1);
    })

    groupBtnFooter.each(function() {
	 var btnPriceLast = $(this).find('.btn-price').last();

	 btnPriceLast.off('keydown.tabToNextFlight').on('keydown.tabToNextFlight', function(e) {
	   var code = e.keyCode || e.which,
		colIdx = $(this).closest('.select-fare-block').data('col-index'),
		nextCol = $('[data-trigger-animation="' + colIdx + '"]').next();

	   if (nextCol.length && !nextCol.is('.not-available')) {
		nextCol.attr('tabindex', 0);
		setTimeout(function() {
		  nextCol.focus();
		}, 100);
	   }
	 })
    });

    btnUpgrade.each(function() {
	 $(this).off('keydown.tabToNextFlight').on('keydown.tabToNextFlight', function(e) {
	   var code = e.keyCode || e.which,
		colIdx = $(this).closest('.upsell').data('col-index'),
		nextCol = $('[data-trigger-animation="' + colIdx + '"]').next();

	   $(this).closest('.upsell').addClass('hidden');
	   if (nextCol.length && !nextCol.is('.not-available')) {
		nextCol.attr('tabindex', 0);
		setTimeout(function() {
		  nextCol.focus();
		}, 100);
	   }
	 })
    });

    var initTabBsp = function() {
	 var btnBsp = bookingSummaryPanel.find('input.btn-1'),
	   btnMoreDetails = bookingSummaryPanel.find('.more-detail'),
	   btnLessDetails = bookingSummaryPanel.find('.less-detail'),
	   linkTriggerPopup = bookingSummaryPanel.find('[data-trigger-popup]').eq(0),
	   linkEditSearch = bookingSummaryPanel.find('.bsp-booking-summary__heading .search-link'),
	   saysTableSummary = $('#table-summary__0');
	 linkEditSearch.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function(e) {
	   var code = e.keyCode || e.which || e.charCode;

	   if (code === 9) {
		e.preventDefault();
		btnBsp.focus();
	   }
	 });
	 btnBsp.each(function(index) {
	   $(this).off('keydown.tabToNextContent').on('keydown.tabToNextContent', function(e) {
		var code = e.keyCode || e.which || e.charCode;

		if (code === 9) {
		  e.preventDefault();
		  if (index === 0) {
		    btnMoreDetails.focus();
		  } else {
		    linkTriggerPopup.focus();
		  }
		}
		if (e.shiftKey && code === 9) {
		  e.preventDefault();
		  linkEditSearch.addClass('focus-outline').focus();
		}
	   })
	 });
	 btnMoreDetails.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function(e) {
	   var code = e.keyCode || e.which || e.charCode;

	   if (code === 9) {
		e.preventDefault();
		saysTableSummary.addClass('focus-outline').focus();
	   }
	   if (e.shiftKey && code === 9) {
		e.preventDefault();
		btnBsp.eq(0).focus();
	   }
	 });
	 linkTriggerPopup.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function(e) {
	   var code = e.keyCode || e.which || e.charCode;

	   if (e.shiftKey && code === 9) {
		e.preventDefault();
		btnBsp.eq(1).focus();
	   }
	 });
	 btnLessDetails.off('keydown.tabToNextContent').on('keydown.tabToNextContent', function(e) {
	   var code = e.keyCode || e.which || e.charCode;

	   if (code === 9) {
		e.preventDefault();
		saysTableSummary.addClass('focus-outline').focus();
	   }
	 });
    }

   /* ally.when.key({

	 'ctrl+y': function() {

	   $('input[name="select-inbound"]').focus();
	   initTabBsp();

	 },

    });*/
  }

  var resetFilter = function() {
    var btnReset = $('[data-reset-filter]');

    btnReset.each(function() {
	 $(this).off('click.resetFilter').on('click.resetFilter', function(e) {
	   e.preventDefault();
	   var filterBlock = $(this).closest('.no-result-filter').siblings('[data-flight-filter]'),
		listCheckbox = filterBlock.find('input[type="checkbox"]').not("[disabled]"),
		listRangeSlider = filterBlock.find('[data-range-slider]'),
		checkboxReset = filterBlock.find('input[type="checkbox"]:disabled');

	   listCheckbox.each(function() {
		$(this).prop('checked', false);
	   })

	   listRangeSlider.each(function() {
		var min = $(this).data('min'),
		  max = $(this).data('max');
		$(this).slider("option", "values", [min, max]);
	   })

	   checkboxReset.prop('checked', true).trigger('change.resetFilter');

	 })
    })
  }


  $("#edit-edit-search-2").off("click.showLoading").on("click.showLoading", function() {
    SIA.preloader.show();
  });

  //codes for default flight selection on page load
  var setDefaultFlightSelection = function(data1) {
    setTimeout(function() {
	 var defaultReId = data1.defaultRecommendationID;
	 var defaultReArr = $.grep(data1.recommendations, function(recommendation) {
	   return recommendation.recommendationID === defaultReId;
	 })[0];
	 var arrSegmentCheck = [],
	   fareCheck = [],
	   fareIdx = 0;
	 for (var i = 0; i < defaultReArr.segmentBounds.length; i++) {
	   arrSegmentCheck.push(defaultReArr.segmentBounds[i].segments[0].segmentID);
	   fareCheck.push(defaultReArr.fareFamily);
	 }
	 var flight_data = "";
	 for (var j = 0; j < arrSegmentCheck.length; j++) {
	   flight_data = flight_data + arrSegmentCheck[j] + "|" + fareCheck[j] + ";";
	 }
	 globalJson.defaultSelection = flight_data.trim();
	 globalJson.selectedFlightIdDetails = flight_data.trim();
	 /* if($("#selectedFlightIDsFromSession").val()!=""
			  && $("#selectedFlightIDsFromSession").val()!="null" && $("#selectedFlightIDsFromSession").val()!=undefined ){
			  $("#selectedFlightIdDetails").val($("#selectedFlightIDsFromSession").val());
			  $("#isRefreshReady").val(true);
			  retainSelectedData();
		  }else{*/
	 $("#selectedFlightIdDetails").val(flight_data.trim());
	 $("#isRefreshReady").val("true");
	 SIA.bookingSummnary();
	 //		  }
    }, 100);
  }

  var renderCombinationsJson = function() {
    var templateBookingPayment;
    var appendDiv;
    if (!$('body').hasClass('fs-economy')) {
	 appendDiv = $('.combinations-json');
    } else {
	 appendDiv = $('.fs-economy').find('.top-main-inner');
    }

    var combinationsJson = function(data1) {
	 var rebookVal = $('#rebookVal').val();
	 var sk = $('#sk').val();
	 var isTTTFlowEnabled = $('#isTTTFlowEnabled').val();
	 if (!$('body').hasClass('fs-economy')) {
	   $.get(global.config.url.combinationsJsonTpl, function(data) {
		var template = window._.template(data, {
		  data: data1

		});
		templateBookingPayment = $(template);
		appendDiv.append(templateBookingPayment);
	   });
	 } else {
	   $.get(global.config.url.fsEconomyMultiStop, function(data) {
		var filterArr = getFilterData(data1.flights);
		var curCode = $.parseJSON($("#selectflightJson").val()).response.currency.code;
		var seeFares = $("#seeFares").val();
		var actionUrl = $("#actionUrl").val();
		var template = window._.template(data, {
		  currencyCode: curCode,
		  seeDaysFare: seeFares,
		  data: data1,
		  filterArr: filterArr,
		  rebookVal: rebookVal,
		  isTTTFlowEnabled: isTTTFlowEnabled,
		  labels: saar5.l4.sk.chooseflight,
		  actionUrl: actionUrl
		});
		templateBookingPayment = $(template);
		appendDiv.append(templateBookingPayment);
		renderFlightTable(data1);
		showHideFilters();
		handleActionSlider();
		sliderRange();
		filterFlights(data1);
		resetFilter(data1.flights);
		setDefaultFlightSelection(data1);
		var urlPage;
		if ($('body').hasClass('sk-ut-flight-search-a')) {
		  urlPage = 'sk-ut-passenger-details-a.html';
		} else if ($('body').hasClass('sk-ut-flight-search-b')) {
		  urlPage = 'sk-ut-passenger-details-b.html';
		}

		if (urlPage) {
		  $('form[name="flight-search-summary"]').attr('action', urlPage);
		}

		if (sk == "true" && ($('#rebookVal').val() == "true")) {
		  $('.fare-summary').addClass("hidden");
		}

	   });
	 }
    };
    //    var url;
    //    if($('body').hasClass('fs-economy-response-page')){
    //      url = "ajax/flightsearch-response.json";
    //    }
    //    if($('body').hasClass('fs-economy-response-new-page') || $('body').hasClass('fs-economy')){
    //      url = "ajax/sin-sfo-30 JUNE-2017-Most-updated.json";
    //    }
    //    if($('body').hasClass('fs-business')){
    //      url = "ajax/sin-sfo-business-first.json";
    //    }
    //    if($('body').hasClass('fs-economy-scoot')){
    //      url = "ajax/sin-sfo-economy-scoot.json";
    //    }
    //    if($('body').hasClass('fs-economy-two-column-premium-economy')){
    //      url = "ajax/sin-sfo-tow-column-premium-economy.json";
    //    }
    //    if($('body').hasClass('fs-economy-sin-maa-roundtrip-page')){
    //      url = "ajax/flight-search-SIN-MAA-Roundtrip-2A1C1I.json";
    //    }
    //    if($('body').hasClass('fs-economy-four-column')){
    //      url = "ajax/sin-sfo-economy-four-column.json";
    //    }
    var data1 = $.parseJSON($("#selectflightJson").val());



    //    $.ajax({
    //      url: url,
    //      type: SIA.global.config.ajaxMethod,
    //      dataType: 'json',
    //      success: function(response) {
    //        var data1 = response.response;

    /* var ocRec={
			"ocRecommendationID" : 0,
			"tttInfoBound" : [
				{
				"travllerInfo" : [{"type" : "ADT","count" : 1 }],
				"pricingAmountType" : "PU",
				"pricingAmount" : 10
				},
				{
				"travllerInfo" : [{"type" : "ADT","count" : 1}],
				"pricingAmountType" : "PU",
				"pricingAmount" : 10
				}
				],
				"totalPricingAmout" : 60
			};

	data1.response.ocRecommendations.push(ocRec);*/

    combinationsJson(data1.response);
    //      }
    //    });
  };

  var popup1 = $('.popup--flights-details-sf');
  var popup2 = $('.flight-search-summary-conditions');
  $(document).on('click', '.flights-details-sf', function(e) {
    e.preventDefault();
    // prototype
    if ($('body').hasClass('sk-ut-workflow')) {
	 return false;
    }
    //
    popup1.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.trigger-summary-of-fare-condition', function(e) {
    e.preventDefault();
    if ($(this).hasClass('link-left')) {
	 $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
	 $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
    } else {
	 $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').removeClass('active');
	 $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').addClass('active');
    }
    popup2.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.popup__close', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    event.preventDefault();
  });

  if ($('body').hasClass('fs-economy-page')) {
    renderCombinationsJson();
  }

  var urlPage;
  if ($('body').hasClass('sk-ut-flight-search-a')) {
    urlPage = 'sk-ut-passenger-details-a.html';
  } else if ($('body').hasClass('sk-ut-flight-search-b')) {
    urlPage = 'sk-ut-passenger-details-b.html';
  }

  if (urlPage) {
    document.cookie = 'priceData' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $('body').hasClass('sk-ut-workflow') && $(document).off('submit.changePage').on('submit.changePage', 'form[name="flight-search-summary"]', function(e) {
	 e.preventDefault()
	 window.location.replace(urlPage);
    });
  }



  /*FOR EARN MILES STARTS*/


  /**
   * @name SIA
   * @description Define global KFMileHowToEarn functions
   * @version 1.0
   */
  var getTooltipMiles = function(tooltip) {
    $(tooltip).parent().find(".loading--small").removeClass("hidden");
    $(tooltip).hide();
    var awardType = "";
    var cabinClass = $("#cabinClass").val();
    var ffName = $(tooltip).attr('farefamily');

    var cabinClassNext = "";
    if ($(tooltip).closest(".select-fare-block").next().find(".fare-family-id").length > 0) {
	 cabinClassNext = $(tooltip).closest(".select-fare-block").next().find(".fare-family-id:first").prev().attr("data-header-class");
    }
    var isSecondColumn = true;
    if ($(tooltip).closest(".select-fare-block").next().hasClass("select-fare-block")) {
	 isSecondColumn = false;
    }
    var tooltipShowFlag = true;
    /*if($("#cabinClass").val()=="J" && aE==""){
	tooltipShowFlag=false;
    }else*/
    if ($("#cabinClass").val() == "F") {
	 tooltipShowFlag = false;
    }

    var peyPresent = $('.recommended-flight-block').find('.flight-list-item').find('.row-head-select').find('.col-select').hasClass('economy-fs--pey-1');
    if (ffName.includes("Premium Economy")) {
	 awardType = "BS";
    } else if (ffName.includes("Economy") && null != peyPresent && peyPresent) {
	 awardType = "PU";
    } else if (ffName.includes("Economy")) {
	 awardType = "BU";
    } else if (ffName.includes("Business")) {
	 awardType = "FU";
    }
    var tooltipData = {};
    var skFlag = "";
    if ($('body').hasClass('fs-economy')) {
	 skFlag = "SK";
    }
    if ($(tooltip).hasClass("from-submit-form")) {
	 tooltipData = {
	   "calculate-mile-1": $(tooltip).attr("codeOrigin"),
	   "calculate-mile-2": $(tooltip).attr("codeDestination"),
	   "schedule-kind-1": $('.tripType').val(),
	   "calculate-mile-3": awardType,
	   "skFlag": skFlag
	 };
    } else {
	 tooltipData = {
	   "calculate-mile-1": $(tooltip).closest(".wrap-content-fs").find(".origin-city-code").val(),
	   "calculate-mile-2": $(tooltip).closest(".wrap-content-fs").find(".destination-city-code").val(),
	   "schedule-kind-1": $('.tripType').val(),
	   "calculate-mile-3": awardType,
	   "skFlag": skFlag
	 };
    }
    $.ajax({
	 url: SIA.global.config.url.kfUseTableJSON,
	 data: tooltipData,
	 dataType: 'json',
	 type: SIA.global.config.ajaxMethod,
	 success: function(data) {
	   if (tooltipShowFlag) {
		//E to PEY and B starts
		if (!ffName.includes("Premium Economy") && ffName.includes("Economy") && null != peyPresent && peyPresent) {
		  awardType = 'BU';
		  if ($(tooltip).hasClass("from-submit-form")) {
		    tooltipData = {
			 "calculate-mile-1": $(tooltip).attr("codeOrigin"),
			 "calculate-mile-2": $(tooltip).attr("codeDestination"),
			 "schedule-kind-1": $('.tripType').val(),
			 "calculate-mile-3": awardType,
			 "skFlag": skFlag
		    };
		  } else {
		    tooltipData = {
			 "calculate-mile-1": $(tooltip).closest(".wrap-content-fs").find(".origin-city-code").val(),
			 "calculate-mile-2": $(tooltip).closest(".wrap-content-fs").find(".destination-city-code").val(),
			 "schedule-kind-1": $('.tripType').val(),
			 "calculate-mile-3": awardType,
			 "skFlag": skFlag
		    };
		  }
		  $.ajax({
		    url: SIA.global.config.url.kfUseTableJSON,
		    data: tooltipData,
		    dataType: 'json',
		    type: SIA.global.config.ajaxMethod,
		    success: function(dataBU) {
			 console.log("data : " + JSON.stringify(data));
			 console.log("dataBU : " + JSON.stringify(dataBU));

			 data.classMiles = data.classMiles.concat(dataBU.classMiles);
			 console.log("data after : " + JSON.stringify(data));

			 var higherClass;
			 var farFamilyName;
			 var secondHigherLevel;
			 if (ffName.includes("Economy")) {
			   farFamilyName = ffName;
			   higherClass = "Premium Economy";
			   secondHigherLevel = "Business";
			 }
			 var miles = "";
			 var category = "";
			 var returnString = "";
			 var saverCount = 0;
			 if (data && data.errorMessage && data.errorMessage != undefined && data.errorMessage == 'AE') {
			   $(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Unable to process your request.</strong><br><br></p>");
			   setTimeout(function() {
				$(tooltip).find(".ico-info-round-fill").kTooltip();
				$(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
			   }, 10);

			   $(tooltip).parent().find(".loading--small").addClass("hidden");
			   $(tooltip).show();
			 } else {
			   $.each(data.classMiles, function(index, item) {
				miles = item.miles;
				category = item.category;
				if (category.includes("Business")) {
				  higherClass = "Business";
				} else if (category.includes("Premium Economy")) {
				  higherClass = "Premium Economy";
				}
				if (!farFamilyName.includes("Business") && !farFamilyName.includes("Premium Economy") && category.includes(farFamilyName)) {
				  if (category.includes("Saver")) {
				    //if(saverCount==1){higherClass=secondHigherLevel;}
				    returnString += saar5.l4.sk.chooseflight.saverUpgradeFrom + " " + farFamilyName.split(" ")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				    saverCount = 1;
				  } else if (category.includes("Advantage")) {
				    returnString += saar5.l4.sk.chooseflight.advantageUpgradeFrom + " " + farFamilyName.split(" ")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				  }
				} else if (farFamilyName.includes("Business") || farFamilyName.includes("Premium Economy")) {
				  if (category.includes("Saver")) {
				    //if(saverCount==1){higherClass=secondHigherLevel;}
				    returnString += saar5.l4.sk.chooseflight.saverUpgradeFrom + " " + farFamilyName.split("-")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				    saverCount = 1;
				  } else if (category.includes("Advantage")) {
				    returnString += saar5.l4.sk.chooseflight.advantageUpgradeFrom + " " + farFamilyName.split("-")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				  }
				}

			   })
			   if (returnString) {
				$(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>NUMBER OF MILES REQUIRED</strong><br><br>" + returnString + "</p>");
				setTimeout(function() { //for opening tooltip
				  $(tooltip).find(".ico-info-round-fill").kTooltip();
				  $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
				}, 10);
				$(tooltip).parent().find(".loading--small").addClass("hidden");
				$(tooltip).show();
			   } else {
				$(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Miles Unavailable.</strong><br></p>");
				setTimeout(function() { //for opening tooltip
				  $(tooltip).find(".ico-info-round-fill").kTooltip();
				  $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
				}, 10);
				$(tooltip).parent().find(".loading--small").addClass("hidden");
				$(tooltip).show();
			   }
			 }

		    },
		    error: function(xhr, status) {
			 alert('error : inner ajax BU');
		    }
		  });
		} else {
		  //E to PEY and B ends
		  var higherClass;
		  var farFamilyName;
		  var secondHigherLevel;
		  if (ffName.includes("Premium Economy")) {
		    farFamilyName = ffName;
		    if (cabinClassNext != "" && (cabinClassNext.includes("Business") || isSecondColumn)) {
			 higherClass = "Business";
			 secondHigherLevel = "First/Suites";
		    } else if (cabinClass == 'S') {
			 higherClass = "First/Suites";
		    } else if (cabinClassNext == "" && cabinClass == 'Y') {
			 higherClass = "Business";
			 secondHigherLevel = "First/Suites";
		    }
		  } else if (ffName.includes("Economy")) {
		    farFamilyName = ffName;
		    if (cabinClassNext != "" && (cabinClassNext.includes("Premium Economy") || isSecondColumn)) {
			 higherClass = "Premium Economy";
			 secondHigherLevel = "Business";
		    } else {
			 higherClass = "Business";
		    }
		  } else if (ffName.includes("Business")) {
		    farFamilyName = ffName;
		    higherClass = "First/Suites";
		  }
		  var miles = "";
		  var category = "";
		  var returnString = "";
		  var saverCount = 0;
		  if (data && data.errorMessage && data.errorMessage != undefined && data.errorMessage == 'AE') {
		    $(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Unable to process your request.</strong><br><br></p>");
		    setTimeout(function() {
			 $(tooltip).find(".ico-info-round-fill").kTooltip();
			 $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
		    }, 10);

		    $(tooltip).parent().find(".loading--small").addClass("hidden");
		    $(tooltip).show();
		  } else {
		    $.each(data.classMiles, function(index, item) {
			 miles = item.miles;
			 category = item.category;

			 if (ffName.includes("Premium Economy") && category.includes("Business")) {
			   higherClass = "Business";
			 } else if (ffName.includes("Business") && category.includes("First")) {
			   higherClass = "First/Suites";
			 }

			 if (!farFamilyName.includes("Business") && !farFamilyName.includes("Premium Economy") && category.includes(farFamilyName)) {
			   if (category.includes("Saver")) {
				//if(saverCount==1){higherClass=secondHigherLevel;}
				returnString += saar5.l4.sk.chooseflight.saverUpgradeFrom + " " + farFamilyName.split(" ")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				saverCount = 1;
			   } else if (category.includes("Advantage")) {
				returnString += saar5.l4.sk.chooseflight.advantageUpgradeFrom + " " + farFamilyName.split(" ")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
			   }
			 } else if (farFamilyName.includes("Business") || (farFamilyName.includes("Premium Economy") && category.includes("Business"))) {
			   if (category.includes("Saver")) {
				//if(saverCount==1){higherClass=secondHigherLevel;}
				returnString += saar5.l4.sk.chooseflight.saverUpgradeFrom + " " + farFamilyName.split("-")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
				saverCount = 1;
			   } else if (category.includes("Advantage")) {
				returnString += saar5.l4.sk.chooseflight.advantageUpgradeFrom + " " + farFamilyName.split("-")[0] + " " + saar5.l4.sk.chooseflight.to + " " + higherClass + ": <strong>" + miles + saar5.l4.sk.chooseflight.milesRequiredMsg + " </strong><br>";
			   }
			 }

		    })
		    if (returnString) {
			 $(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>NUMBER OF MILES REQUIRED</strong><br><br>" + returnString + "</p>");
			 setTimeout(function() { //for opening tooltip
			   $(tooltip).find(".ico-info-round-fill").kTooltip();
			   $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
			 }, 10);
			 $(tooltip).parent().find(".loading--small").addClass("hidden");
			 $(tooltip).show();
		    } else {
			 $(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Miles Unavailable.</strong><br></p>");
			 setTimeout(function() { //for opening tooltip
			   $(tooltip).find(".ico-info-round-fill").kTooltip();
			   $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
			 }, 10);
			 $(tooltip).parent().find(".loading--small").addClass("hidden");
			 $(tooltip).show();
		    }
		  }
		}
	   } else {
		$(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Miles Unavailable.</strong><br></p>");
		setTimeout(function() { //for opening tooltip
		  $(tooltip).find(".ico-info-round-fill").kTooltip();
		  $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
		}, 10);
		$(tooltip).parent().find(".loading--small").addClass("hidden");
		$(tooltip).show();
	   }
	 },
	 error: function(xhr, status) {
	   $(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Miles Unavailable.</strong><br></p>");
	   setTimeout(function() { //for opening tooltip
		$(tooltip).find(".ico-info-round-fill").kTooltip();
		$(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
	   }, 10);
	   $(tooltip).parent().find(".loading--small").addClass("hidden");
	   $(tooltip).show();
	 }
    });
  }

  $(document).off("click", ".miles-tooltip").on("click", ".miles-tooltip", function() {
    getTooltipMiles(this);
  });

  //accrual miles starts

  /**
   * @name SIA
   * @description Define global KFMile Earn functions
   * @version 1.0
   */
  var getTooltipMilesEarn = function(tooltip) {
    var cabinClass = $("#cabinClass").val();
    var ffName = $(tooltip).attr('farefamily');
    var tooltipData = {};
    var skFlag = "";
    if ($('body').hasClass('fs-economy')) {
	 skFlag = "SK";
    }

    var tripMile = $('#edit-radio-4').val();
    var originMile = $('#origin-city-code').val();
    var destinationMile = $('#destination-city-code').val();
    //var btn-calculateMile = "";

    var parms = {
	 tripType: tripMile,
	 origin: originMile,
	 destination: destinationMile

    };


    var tooltipData = {};
    $.ajax({
	 url: "/accrualMiles.form?",
	 data: parms,
	 dataType: 'json',
	 type: SIA.global.config.ajaxMethod,
	 success: function(data) {
	   //console.log(JSON.stringify(data));
	   if (typeof data != 'undefined' && data.errorMessage != "undefined" && data.errorMessage != null && data.errorMessage.length > 0) {

		$(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>Unable to process your request.</strong><br><br></p>");
		setTimeout(function() {
		  $(tooltip).find(".ico-info-round-fill").kTooltip();
		  $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
		}, 10);

		$(tooltip).parent().find(".loading--small").addClass("hidden");
		$(tooltip).show();

	   } else {

		var higherClass;
		var farFamilyName = ffName;
		var earnMile = "";
		$.each(data.accrualMiles, function(index, item) {
		  /*if(item.cabinClass == "Economy") {*/
		  $.each(item.accrualVO, function(indexz, itemz) {
		    if (itemz.fareFamily == farFamilyName) {
			 earnMile = itemz.miles;

			 /*var abc = itemz.sellingClass.split(",");
									var matching = false;
									$.each(abc,function(indexx, itemx){
										if(itemx == "L") {
											matching = true;
									}
								 });
									if(matching == true) {
										earnMile = itemz.miles;

								 }*/
		    }
		  });
		  /*}*/
		});


		$(tooltip).find(".ico-info-round-fill").attr("data-content", "<p class='tooltip__text-2'><strong>NUMBER OF MILES ACCRUED </strong><br><br>" + earnMile + "</p>");
		setTimeout(function() { //for opening tooltip
		  $(tooltip).find(".ico-info-round-fill").kTooltip();
		  $(tooltip).find(".ico-info-round-fill").trigger("click.showTooltip");
		}, 10);

		$(tooltip).parent().find(".loading--small").addClass("hidden");
		$(tooltip).show();
	   }
	 },
	 error: function(xhr, status) {
	   SIA.preloader.hide();
	 }
    });
  }

  $(document).off("click", ".miles-tooltipEarn").on("click", ".miles-tooltipEarn", function() {
    getTooltipMilesEarn(this);
  });
  //accrual miles ends

  setTimeout(function() {
    $(".sub-logo").find(".ico-info-round-fill").kTooltip();
    $(".content-filter-search").find(".ico-info-round-fill").kTooltip();

  }, 1000);

  /*FOR EARN MILES ENDS*/

  /*for edit search*/
  $('#originCIB').blur(function() {
    var dataValue = $('#originCIB').val();
    if (dataValue && dataValue.indexOf('-') != -1) {
	 var splitdataValue = dataValue.lastIndexOf('-');
	 $('#origin').val(dataValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
    } else {
	 $('#origin').val(dataValue);
    }
  });
  $('#destinationCIB').blur(function() {
    var dataValue = $('#destinationCIB').val();
    if (dataValue && dataValue.indexOf('-') != -1) {
	 var splitdataValue = dataValue.lastIndexOf('-');
	 $('#destination').val(dataValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
    } else {
	 $('#destination').val(dataValue);
    }
  });
  $('#edit-cabin-1').change(function() {
    var dataValue = $('#edit-cabin-1').val();
    if (dataValue && dataValue == 'economy') {
	 $('#cabinClass').val('Y');
    } else if (dataValue && dataValue == 'premiumeconomy') {
	 $('#cabinClass').val('S');
    } else if (dataValue && dataValue == 'business') {
	 $('#cabinClass').val('J');
    } else if (dataValue && dataValue == 'firstSuite') {
	 $('#cabinClass').val('F');
    }
  });



  var prepopulatecity = function() {
    var cibOriginOptions = $('#cib-17-1').find('option');
    var cibDestOptions = $('#cib-17-2').find('option');
    for (var i = 0; i < cibOriginOptions.length; i++) {
	 var cibOriginValue = cibOriginOptions[i].getAttribute('data-text'); //Used if text box content is like "Singapore - SIN)",
	 //var cibOriginValue = cibOriginOptions[i].innerHTML;    // Used if text box content is like "Singapore,Singapore (Changi Intl - SIN)",
	 if ($('select#cib-17-1').find(cibOriginOptions[i]).attr("selected")) {
	   $('#originCIB').val(cibOriginValue);
	   if (cibOriginValue && cibOriginValue.indexOf('-') != -1) {
		var splitdataValue = cibOriginValue.lastIndexOf('-');
		$('#origin').val(cibOriginValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
	   }
	   break;
	 }
    }
    for (var i = 0; i < cibDestOptions.length; i++) {
	 var cibDestValue = cibDestOptions[i].getAttribute('data-text');
	 if ($('select#cib-17-2').find(cibDestOptions[i]).attr("selected")) {
	   $('#destinationCIB').val(cibDestValue);
	   if (cibDestValue && cibDestValue.indexOf('-') != -1) {
		var splitdataValue = cibDestValue.lastIndexOf('-');
		$('#destination').val(cibDestValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
	   }
	   break;
	 }
    }
    var orbOriginOptions = $('#cib-17-3').find('option');
    var orbDestOptions = $('#cib-17-4').find('option');
    for (var i = 0; i < orbOriginOptions.length; i++) {
	 var orbOriginValue = orbOriginOptions[i].getAttribute('data-text');
	 if ($('select#cib-17-3').find(orbOriginOptions[i]).attr("selected")) {
	   $('#originORB').val(orbOriginValue);
	   if (orbOriginValue && orbOriginValue.indexOf('-') != -1) {
		var splitdataValue = orbOriginValue.lastIndexOf('-');
		$('#orbOrigin').val(orbOriginValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
	   }
	   break;
	 }
    }
    for (var i = 0; i < orbDestOptions.length; i++) {
	 var orbDestValue = orbDestOptions[i].getAttribute('data-text');
	 if ($('select#cib-17-4').find(orbDestOptions[i]).attr("selected")) {
	   $('#destinationORB').val(orbDestValue);
	   if (orbDestValue && orbDestValue.indexOf('-') != -1) {
		var splitdataValue = orbDestValue.lastIndexOf('-');
		$('#orbDestination').val(orbDestValue.substring(splitdataValue + 1, splitdataValue + 5).trim());
	   }
	   break;
	 }
    }
  }

  prepopulatecity();
  // Turbo-B3 ends
  //ATC change starts

  var populateForInboundSegmentChange = function() {
    var selectedFlightId = ($("#selectedFlightIdDetails").val()).split(";")[0];
    var segmentId = selectedFlightId.split("|")[0];
    var fareFamilyCode = selectedFlightId.split("|")[1];
    var recommendedList = $(".wrap-content-list").find(".wrap-content-fs:eq(0)").find(".flight-list-item");

    var targetDiv;
    recommendedList.each(function() {
	 var thisSegmentId = $(this).find(".segment-id").text().trim();
	 if (segmentId === thisSegmentId) {
	   targetDiv = $(this);
	   return false;
	 }
    });

    $(targetDiv.find(".fare-family-id")).each(function() {
	 if ($(this).text() == fareFamilyCode) {
	   $(this).prev(".btn-price").trigger("click.calculatePriceInbound");
	   $(this).prev(".btn-price").trigger("click.getFlightContent");
	 }
    });
  };

  var populateForOutboundSegmentChange = function() {
    var selectedFlightId = ($("#selectedFlightIdDetails").val()).split(";")[1];
    var segmentId = selectedFlightId.split("|")[0];
    var fareFamilyCode = selectedFlightId.split("|")[1];
    var recommendedList = $(".wrap-content-list").find(".wrap-content-fs:eq(1)").find(".flight-list-item");
    var targetDiv;
    recommendedList.each(function() {
	 var thisSegmentId = $(this).find(".segment-id").text().trim();
	 if (segmentId === thisSegmentId) {
	   targetDiv = $(this);
	   return false;
	 }
    });

    $(targetDiv.find(".fare-family-id")).each(function() {
	 if ($(this).text() == fareFamilyCode) {
	   $(this).prev(".btn-price").trigger("click.calculatePriceInbound");
	   $(this).prev(".btn-price").trigger("click.getFlightContent");
	   $(".fare-summary-group").removeClass("hidden");
	 }
    });
  };
  var atcFunctions = function() {
    if ($("#selectedFlightIdDetailsforRefresh").val() == "" || $("#selectedFlightIdDetailsforRefresh").val() == "null" || $("#selectedFlightIdDetailsforRefresh").val() == undefined) {

	 $(document).on("click", ".btn-price", function() {
	   if (!($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && $('#isOutboundSegmentChanged').val() == "false" && $('#tripType').val() == "R")) {
		if (!($(this).hasClass("keep-my-selection")) && $('#isOutboundSegmentChanged').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && outboundSegmentChanged == false) {
		  if ($('#isOutboundSegmentChanged').val() == "true") {
		    setTimeout(function() {
			 populateForOutboundSegmentChange();
		    }, 2000);
		    outboundSegmentChanged = true;
		  }
		}
	   }
	 });

	 $(document).on("click", 'input[name="btn-keep-selection"]', function() {
	   if (!($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && $('#isOutboundSegmentChanged').val() == "false" && $('#tripType').val() == "R")) {
		if ($('#isOutboundSegmentChanged').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && outboundSegmentChangedKms == false) {

		  if ($('#isOutboundSegmentChanged').val() == "true") {
		    setTimeout(function() {
			 populateForOutboundSegmentChange();
		    }, 2000);
		    outboundSegmentChangedKms = true;
		  }

		}
	   }
	 });


	 $(document).ajaxComplete(function(event, xhr, settings) {
	   if (!($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && $('#isOutboundSegmentChanged').val() == "false" && $('#tripType').val() == "R")) {
		if (settings.url.indexOf("fs-booking-summary-panel") >= 0) {

		  if (!($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "true" && $('#isOutboundSegmentChanged').val() == "true") && $('#tripType').val() == "R") {
		    if ($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "true" && inboundSegmentChanged === false && $('#tripType').val() == "R") {
			 populateForInboundSegmentChange();
			 inboundSegmentChanged = true;
		    }
		  }

		  if (!($('#rebookVal').val() == "true" && $('#isInboundSegmentChanged').val() == "false" && $('#isOutboundSegmentChanged').val() == "false") && $('#tripType').val() == "R") {
		    if ($('#rebookVal').val() == "true" && $('#isOutboundSegmentChanged').val() == "true" && $('#tripType').val() == "R") {
			 if (globalJson.dataDepart != undefined && inboundSegmentChanged == false) {
			   populateForOutboundSegmentChange();
			   $(".fare-summary-group").addClass("hidden");
			   inboundSegmentChanged = true;
			 }
		    }
		  }
		}
	   }
	 });
    }
  }
  atcFunctions();



  //ATC change ends



  var popupCabinMismatch = $('.popout--mismatch');
  if (!popupCabinMismatch.data('Popup')) {
    popupCabinMismatch.Popup({
	 overlayBGTemplate: config.template.overlay,
	 modalShowClass: '',
	 triggerCloseModal: '.popup__close, [data-close]',
	 afterShow: function() {
	   flyingFocus = $('#flying-focus');
	   if (flyingFocus.length) {
		flyingFocus.remove();
	   }
	 },
	 closeViaOverlay: false
    });
  }

  var pop = $('#cabinChk').val();
  if (pop == 'true') {
    popupCabinMismatch.Popup('show');
  }
  var getSelectedFlightData = function(data1) {
    var defaultReId = data1.defaultRecommendationID;
    var defaultReArr = $.grep(data1.recommendations, function(recommendation) {
	 return recommendation.recommendationID === defaultReId;
    })[0];
    var arrSegmentCheck = [],
	 fareCheck = [],
	 fareIdx = 0;
    for (var i = 0; i < defaultReArr.segmentBounds.length; i++) {
	 arrSegmentCheck.push(defaultReArr.segmentBounds[i].segments[0].segmentID);
	 fareCheck.push(defaultReArr.fareFamily);
    }
    var flight_data = "";

    for (var j = 0; j < arrSegmentCheck.length; j++) {
	 flight_data = flight_data + arrSegmentCheck[j] + "|" + fareCheck[j] + ";";
    }
  }
}();
