/**
 * @name SIA
 * @description Define global histogramDetails functions
 * @version 1.0
 */
SIA.histogramDetails = function(){
	var global = SIA.global;
	var config = global.config;
	var dialsChartItem = $('.dials-chart__item');
	var initChart = function(){
		var blockChart = $('.block--account-summary-chart');
		var chartContent = $('.chart-content', blockChart);
		if(!chartContent.length){
			return;
		}
		var currency = 'SGD';
		var categories = globalJson.sqcAtAGlanceChartData ? globalJson.sqcAtAGlanceChartData.categories : {
			m: ['F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D', 'J'],
			t: ['FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC', 'JAN']
		};
		var data =  globalJson.sqcAtAGlanceChartData ? globalJson.sqcAtAGlanceChartData.data : [638.80, 238.80, 338.80, 438.80, 328.80, 438.80, 438.80, 638.80, 338.80, 538.80, 638.80, 438.80];

		chartContent.highcharts({
			chart: {
				type: 'column'
			},
			colors: ['#CFE2DE'],
			title: {
				text: ''
			},
			subtitle: {
				text: ''
			},
			exporting: {
				enabled: false
			},
			credits: {
				enabled: false
			},
			xAxis: {
				min: 0,
				max: 4,
				categories: categories.t,
				tickColor: '#F7F7F7',
				lineColor: '#F7F7F7',
				labels: {
					style: {
						fontWeight: 'bold',
						color: '#000000',
						fontSize: '13px',
						'font-family': '"proxima-nova", "Open Sans", "Arial", "Helvetica", "sans-serif"'
					}
				},
				plotLines: [{
				 label: {
		          text: 2018,
		          rotation: 0
		          },
		          opposite: true,
		          value: 10.5,
		          width: '1',
		          color: '#ccc',
				  dashStyle: 'dash',
		          zIndex: 5,
				}]
			},
			yAxis: {
				title: {
					text: ''
				},
				gridLineWidth: 0,
				labels: {
					enabled: false
				}
			},
			plotOptions: {
				series: {
					connectNulls: true,
					pointWidth: 70
				},
				column: {
			        states: {
			            hover: {
			                color: '#428B77'                                                           
			            }
			        }
			    }
			},
			tooltip: {
				shadow: false,
				borderWidth: 0,
				backgroundColor: '#00266B',
				formatter: function () {
					var numberData = currency + ' ' + this.y;
					$('.chart-number').text('');
					$('.chart-number').text(numberData);
					return currency + ' ' + this.y;
				},
				style: {
					color: '#FFFFFF',
					fontSize: '13px',
					'font-family': '"proxima-nova", "Open Sans", "Arial", "Helvetica", "sans-serif"'
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				showInLegend: false,
				lineWidth: 0.5,
				shadow: false,
				marker: {
					fillColor: '#F7F7F7',
					lineWidth: 0.5,
					radius: 3,
					lineColor: null,
					states: {
						hover: {
							fillColor: '#00266B',
							radius: 5,
							lineWidthPlus: 0,
							radiusPlus: 0
						}
					}
				},
				states: {
					hover: {
						halo: {
							size: 2,
							opacity: 1
						}
					}
				},
				data: data
			}]
		});
	};
	var initModule = function(){
		initChart();
	};

	setTimeout(function(){
		initModule();
	}, 500);
};