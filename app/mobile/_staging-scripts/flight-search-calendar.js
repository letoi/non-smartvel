/**
 * @name SIA
 * @description Define global flightSearchCalendar functions
 * @version 1.0
 */
SIA.flightSearchCalendar = function(){
	// var global = SIA.global;
	// var config = global.config;
	// var win = global.vars.win;

	if(!$('.flight-search-calendar-page, .orb-flight-schedule').length){
		return;
	}

	var bindActions = function () {
		// body...
		var flightSearchCalendar = $('.flight-search-calendar');
		var next = $('.flight-search-calendar__control .slick-next', flightSearchCalendar);
		var prev = $('.flight-search-calendar__control .slick-prev', flightSearchCalendar);
		next.attr('href', next.data('url-mobile')).find('.number').text(3);
		prev.attr('href', prev.data('url-mobile')).find('.number').text(3);

		var hiddenItem = function(number) {
			flightSearchCalendar.each(function() {
				var fsCalendar = $(this);
				var items = fsCalendar.find('.flight-search-calendar__item');
				var len = items.length;
				var idx = fsCalendar.find('input[type="radio"]:checked')
					.closest('.flight-search-calendar__item').index();

				var l = idx <= 1 ? 0 : ((idx > 1 && idx + number <= len) ? (idx - 1) : (len - number));
				var r = l + number - 1;

				items.removeClass('hidden-tablet hidden-mb').addClass('hidden').filter(function(index) {
					return index >= l && index <= r;
				}).removeClass('hidden');
			});
		};

		hiddenItem(3);
	};

	if($('.flight-search-calendar-page').length) {
		//CIB page
		bindActions();
	}
	else {
		//ORB Flight Schedule page
		$.get(SIA.global.config.url.orbFlightSchedule, function(html) {
			var template = window._.template(html, {
				data: globalJson.flightSchedule
			});

			$('#form-flight-schedule').html(template);

			bindActions();
		});
	}
};
