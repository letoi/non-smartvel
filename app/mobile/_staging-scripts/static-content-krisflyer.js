SIA.staticContentKrisflyer = function(){
	var global = SIA.global,
			win = global.vars.win,
			config = global.config,
			totalMobileSlide = 1,
			totalLandscapeSlide = 2;

	// banner slider
	var bannerSlider = $('#banner-slider');
	var imgBannerLength = bannerSlider.find('img.img-main').length - 1;
	var loadBackgroundBanner = function(self, idx){
		// self.closest('.slide-item').css({
		// 	'background-image': 'url(' + self.attr('src') + ')'
		// });
		// self.closest('.slide-item').css({
		// 	'background-position': self.closest('.slide-item').data('mobile-bg')
		// });
		// self.attr('src', config.imgSrc.transparent);
		if(idx === imgBannerLength){
			bannerSlider.find('.loading').hide();
			bannerSlider.css('visibility', 'visible');
			bannerSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					draggable: true,
					infinite: true,
					arrows: false,
					speed: 500,
					fade: true,
					autoplay: false,
					accessibility: false,
					pauseOnHover: false,
					slide: 'div',
					cssEase: 'linear'
				});
		}
	};
	bannerSlider.find('img.img-main').each(function(idx) {
		var self = $(this);
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundBanner(self, idx);
		};
		nI.src = self.attr('src');
	});

	// Highlight slider
	var highlightSlider = $('#highlight-slider');
	var wrapperHLS = highlightSlider.parent();
	var imgHighlightLength = highlightSlider.find('img').length - 1;
	var loadBackgroundHighlight = function(self, parentSelt, idx){
		parentSelt.css({
			'background-image': 'url(' + self.attr('src') + ')'
		});
		self.attr('src', config.imgSrc.transparent);
		if(idx === imgHighlightLength){
			if(window.innerWidth > 480){
				highlightSlider.width(wrapperHLS.width() + 22);
			}
			else{
				highlightSlider.width(wrapperHLS.width());
			}
			highlightSlider.css('visibility', 'visible');
			highlightSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: 2,
					slidesToScroll: 2,
					accessibility: false,
					autoplay: false,
					pauseOnHover: false,
					responsive: [
						{
							breakpoint: 768,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}
					]
				});
			win.off('resize.highlightSlider').on('resize.highlightSlider',function() {
				if(window.innerWidth > 480){
					highlightSlider.width(wrapperHLS.width() + 22);
				}
				else{
					highlightSlider.width(wrapperHLS.width());
				}
			}).trigger('resize.highlightSlider');
		}
	};

	highlightSlider.find('img').each(function(idx) {
		var self = $(this);
		var parentSelt = self.parent();
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundHighlight(self, parentSelt, idx);
		};
		nI.src = self.attr('src');
	});

	$('[data-tablet-slider]').each(function() {
		var slider = $(this);
		slider.find('img').each(function() {
			var self = $(this),
					newImg = new Image();

			newImg.onload = function() {
				slider.css('visibility', 'visible');
				slider.find('.slides')
					.on('init', function(slick) {
						$(this).find('.slick-list').attr('aria-live', 'off'); // disable verbose aria-live
					})
					.slick({
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: totalLandscapeSlide,
						slidesToScroll: totalLandscapeSlide,
						accessibility: false,
						arrows: false,
						responsive: [
							{
								breakpoint: 480,
								settings: {
									slidesToShow: totalMobileSlide,
									slidesToScroll: totalMobileSlide
								}
							}
						]
					});
			};
			newImg.src = self.attr('src');
		});
	});
};
