/**
 * @name SIA
 * @description Define function to carousel fade
 * @version 1.0
 */
SIA.carouselFade = function() {
	var carouselFade = $('[data-carousel-fade]');

	if(carouselFade.length){
		carouselFade.each(function(){
			var carousel = $(this);
			var option = carousel.data('option') ? $.parseJSON(carousel.data('option').replace(/\'/gi, '"')) : {};
			option.siaCustomisations = true;
			var imgPromotionLength = carousel.find('img').length - 1;

			var loadBackground = function(self, parentSelt, idx) {
				if (idx === imgPromotionLength) {
					carousel.css('visibility', 'visible');
					carousel.find('.slides')
						.slick(option);
				}
			};

			option.pauseOnHover = false;
			carousel.find('img').each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				nI.onload = function() {
					loadBackground(self, parentSelt, idx);
				};

				nI.src = self.attr('src');
			});
		});
	}
};
