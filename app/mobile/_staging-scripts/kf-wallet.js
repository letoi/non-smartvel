/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
SIA.kfWallet = function(){
  var global = SIA.global;
  var config = global.config;
  var kfWalletClass = $('.kf-wallet');
  var container = global.vars.container;
  var win = $(window);
  var elCheckBox;

  var showHideEditCard = function() {
    var kfListCard = kfWalletClass.find('.kf-list-card'),
        listItems = kfListCard.find('.item'),
        linkEditCard = kfListCard.find('.link-edit'),
        btnSave = kfListCard.find('.btn-save'),
        inputDefault = kfListCard.find('.custom-checkbox');
    linkEditCard.off('click.editCard').on('click.editCard', function(e) {
      e.preventDefault();
      var _this = $(this);
      var thisItem = _this.closest('.item'),
          thisContentInfor = thisItem.find('.content-infor'),
          thisContentEdit = thisItem.find('.content-edit'),
          thisInputDefault = thisItem.find('.custom-checkbox');
      thisContentInfor.hide();
      thisContentEdit.fadeIn();
    });
  };

  var insertValueInput = function(){
    var formBlockValue = $('.item').find('form');
    formBlockValue.each(function(){   
      $(this).find('.select__text.card-currency').empty();
      var monthEdit = $(this).find('.month-edit').text();
      var yearEdit = $(this).find('.year-edit').text();
      var cardCurrency = $(this).find('.card-currency').text();
      var contentMonth = $(this).find('.select__text.month');
      var contentYear = $(this).find('.select__text.year');
      var contentcardCurrency = $(this).find('.select__text.card-currency');
      contentMonth.text(monthEdit);
      var selectInputMonth = contentMonth.closest('[data-customselect]').find('select').find('option');
      selectInputMonth.each(function() {
        if(contentMonth.text() === $(this).attr('value')){
          $(this).attr('selected', 'selected');
        }
      });
      contentYear.text(yearEdit);
      var selectInputYear = contentYear.closest('[data-customselect]').find('select').find('option');
      selectInputYear.each(function() {
        if(contentYear.text() === $(this).attr('value')){
          $(this).attr('selected', 'selected');
        }
      });
      contentcardCurrency.text(cardCurrency);
      contentcardCurrency.closest('[data-customselect]').find('input').attr('value', cardCurrency);  
    }); 
  };

  var initPopup = function() {
    var triggerPopup = $('[data-trigger-popup=".popup--delete-card"]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));
      var measureScrollbar = (function(){
        var a = document.createElement('div');
        a.className = 'modal-scrollbar-measure';
        $('body').append(a);
        var b = a.offsetWidth - a.clientWidth;
        return $(a).remove(), b;
      })();

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: SIA.global.config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',

          afterHide: function(){
            container.css('padding-right', '');
            $('body').css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) { 
              if (ua.indexOf('chrome') > -1) { } 
              // else {
              //   $('body').attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
              // }
            }
          }
        });
      }
      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        if (!self.hasClass('disabled')) {
          win.trigger('click.hideTooltip');
          popup.Popup('show');
          $(this).addClass('clicked-show-popup');
        }
      });

    });
  };

  $(document).find('#btn-proceed-delete').on('click', function(){
    var form = $('form').find('.clicked-show-popup').closest('form');
    form.attr('action',form.attr('action')).trigger('submit');
  });

  $(document).find('#btn-cancel-delete').on('click', function(){
    $('form').find('.clicked-show-popup').removeClass('clicked-show-popup');
  });

  var removeCheckbox = function(elCheckBox){    
    elCheckBox.addClass('checked-trigger');   
    var formBlock = $('.kf-list-card').find('form');    
    var checkBoxNoChecked = formBlock.find('.custom-checkbox').find('input');   
    checkBoxNoChecked.closest('.custom-checkbox').removeClass('has-disabled').removeAttr('data-disabled-checkbox');   
    checkBoxNoChecked.each(function(){    
      if(!$(this).hasClass('checked-trigger')){   
        $(this).removeAttr('checked');    
        $(this).closest('.item').removeClass('is-default');   
      } else{   
        elCheckBox.closest('.item').addClass('is-default');   
      }   
    });   
  };

	var renderKfDashboard = function(){
    var templatekfDashboard;
    var appendDiv = $('.block-content-dashboard');

    var fsDashboardTpl = function(data1){
        $.get(global.config.url.kfDashboard, function (data) {
          var template = window._.template(data, {
            data: data1
          });
          templatekfDashboard = $(template);
          appendDiv.append(templatekfDashboard);
          showHideEditCard();
          SIA.initCustomSelect();
          appendDiv.find('form').each(function(){
            $(this).validate({
              errorPlacement: global.vars.validateErrorPlacement,
              success: global.vars.validateSuccess
            });
          });
          var alertCheckin = $('.block-content-dashboard').find('.checkin-alert');
          alertCheckin.each(function() { 
            if(!$(this).hasClass('hidden')){
              $(this).closest('.block-content-dashboard').find('.checkin-alert.hidden').remove();
              $(this).closest('.block-content-dashboard').find('.checkin-alert').addClass('hidden');
              $('.block-content-dashboard').find('.checkin-alert').eq(0).removeClass('hidden');
            }
          });
          var blockCustomCheckbox = $('[data-disabled-checkbox]');
          var blockDisabledCheckbox = $('.custom-checkbox').data('disabled-checkbox');
          if(blockDisabledCheckbox == true){
            blockCustomCheckbox.addClass('has-disabled');
            blockCustomCheckbox.find('input').attr('checked', true);
            blockCustomCheckbox.find('input').attr('has-disabled', true);
          }
          initPopup();
          insertValueInput();
          var chekedCheckbox = $('.item').find('.custom-checkbox').find('input');   
          chekedCheckbox.on('click', function(){    
            if(!$(this).closest('.custom-checkbox').hasClass('has-disabled')){    
              elCheckBox = $(this);   
              elCheckBox.closest('.kf-list-card').find('input').removeClass('checked-trigger');   
              removeCheckbox(elCheckBox);   
            }   
          });
          
        });
      };
    $.ajax({
      url: global.config.url.jsonKfDashboard,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data1 = reponse.digitalWalletVO;
        fsDashboardTpl(data1);
      }
    });
  };

  renderKfDashboard();
};
