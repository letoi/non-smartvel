
/**
 * @name SIA
 * @description scrollTop to block
 * @version 1.0
 */
SIA.scrollTop = function(){
	var link = $('[data-scroll-top]');

	if(link.length){
		link.off('click.scrollTop').on('click.scrollTop', function(e){
			e.preventDefault();
			$('html, body').animate({scrollTop: $($(this).attr('href')).offset().top}, 400);
		});
	}
};
