var SIA = SIA || {};

SIA.milesCalculator = function() {
  var p = {};

  p.global = SIA.global;
  p.emData = globalJson.earnMiles;

  p.headingTpl = '<div class="data-origin"><h3 class="title-4--blue"><%= origin %> to <%= destination %></h3></div>';

  p.alertBoxTpl = '<div class="alert-block checkin-alert" style="display: block;">\
		<div class="inner">\
			<div class="alert__icon"><em class="ico-close-round-fill"></em></div>\
			<div class="alert__message"><%= msg %></div>\
		</div>\
	</div>';

  p.selSumTpl = '<span class="miles__flown__text"><%= tripType %></span>';

  p.milesResultTable2 = '<table class="table-1 table-responsive miles-calculator-table">\
  	  <thead class="hidden-mb">\
  		  <tr>\
  			  <th class="table-width-0 th-width">\
  			  <span class="fare__family__table_title1"><%= header1 %></span>\
  			  </th>\
  			  <th class="table-width-0">\
  			  <span class="table__text__left"><%= header2 %></span>\
  			  </th>\
  		  </tr>\
  	  </thead>\
  	  <tbody>\
  		<%= tableContent %>\
  	  </tbody>\
  	</table>';

  var init = function() {
    // Cache containers and ui elements
    p.headingWrap = $('#headingWrap');
    p.tabWrapper = $('#tabWrapper');
    p.accrualNote = $('#accrualNote');

    p.milesCalcTypeNav = $('#milesCalcTypeNav');
    p.milesCalcContent = $('#milesCalcContent');

    // Set initial States
    p.body = SIA.global.vars.body;

    p.curMilesTab = parseInt(p.body.data('active-tab'));

    p.milesCalcTab = new SIA.SimpleTab();
    p.milesCalcTab.p.curTab = p.curMilesTab;

    p.milesCalcTab.init(p.milesCalcTypeNav, p.milesCalcContent);

    if (typeof p.emData != 'undefined' && p.emData.errorMessage.length > 0) {
      showErrorMsg(p.alertBoxTpl, p.emData.errorMessage, p.body.find('.earn-miles'));
    }

    SIA.EarnMiles.init();

    // redeem miles
    SIA.RedeemMiles.init(p.headingTpl, p.selSumTpl, p.alertBoxTpl, p.milesResultTable2, p.body.find('.redeem-miles'), SIA.milesCalculator.toggleBtnEvents, SIA.milesCalculator.showErrorMsg);

    // upgrade flight
    SIA.UpgradeFlight.init(p.headingTpl, p.selSumTpl, p.alertBoxTpl, p.body.find('.upgrade-flight'), SIA.milesCalculator.toggleBtnEvents, SIA.milesCalculator.showErrorMsg);
  };

  // for redeem miles & upgrade flight toggle button
  var toggleBtnEvents = function(el, inputTypeEl) {
    var returnBtn = el.find('.return-btn');
    var oneWayBtn = el.find('.one-way-btn');

    oneWayBtn.parent().on({
      'click': function(e) {
        e.preventDefault();

        returnBtn.parent().removeClass('miles-cta-active');
        oneWayBtn.parent().addClass('miles-cta-active');

        // add selected value in hidden input field
        $(inputTypeEl).attr('value', 'O');
      }
    });

    returnBtn.parent().on({
      'click': function(e) {
        e.preventDefault();

        oneWayBtn.parent().removeClass('miles-cta-active');
        returnBtn.parent().addClass('miles-cta-active');

         // add selected value in hidden input field
         $(inputTypeEl).attr('value', 'R');
      }
    });
  };

  var showErrorMsg = function(alertBoxTpl, msg, el) {
    // get container to prepend alert box and prepare template for it
    el.find('.miles-calculator-component').prepend($(_.template(alertBoxTpl, {'msg': msg})));

    // hide the tab content
    el.find('hr').addClass('hidden');
    el.find('.cabin-class-tab-control').addClass('hidden');
  };

  var oPublic = {
    init: init,
    toggleBtnEvents: toggleBtnEvents,
    showErrorMsg: showErrorMsg,
    p: p
  };

  return oPublic;
}();

SIA.EarnMiles = function() {
  var p = {};

  p.global = SIA.global;

  // em = Earn Miles
  p.emData = globalJson.earnMiles;

  p.headingTpl = '<div class="data-origin"><h3 class="title-4--blue"><%= origin %> to <%= destination %></h3></div>';
  p.subHeadingTpl = '<span class="miles__flown__text">Actual miles flown - <%= tripType %> <strong><%= actualMiles %></strong> miles</span>';
  p.selSumTpl = '<span class="miles__flown__text"><%= tripType %></span>';
  p.milesResultTable = '<div class="tab-content" role="tabpanel" data-tab-id="<%= id %>">\
      <div class="miles-calculator-table">\
  		<table class="table-1 table-responsive">\
  		  <thead class="hidden-mb">\
  		      <tr>\
                <th class="table-width-0 th-width">\
  		          <span class="fare__family__table_title1">Booking Class</span>\
  		          </th>\
                <th class="table-width-0">\
  		          <span class="table__text__left">KrisFlyer miles Earned</span>\
  		          </th>\
  		      </tr>\
  		  </thead>\
  		  <tbody>\
  		  	<%= tableContent %>\
  		  </tbody>\
  		</table>\
  	</div></div>';
  p.milesResultTableContent = '<tr class="<%= isEven %>">\
      <td>\
        <span class="fare-title">Booking class</span><br>\
        <span class="fare__family__title"><%= title %></span><br>\
        <span class="fare__family__codes"><%= codes %></span>\
      </td>\
      <td>\
        <span class="fare-title">KrisFlyer miles earned</span><br>\
        <span class="miles-num-text table__text__left"><strong><%= miles %></strong> miles</span>\
      </td>\
      </tr>';

  var init = function() {
    // Cache containers and ui elements
    p.oneWayBtn = $('#btnOneWay');
    p.returnBtn = $('#btnReturn');

    p.earnMilesContent = $('#earnMilesContent');
    p.headingWrap = $('#headingWrap');
    p.calcTabList = $('#calcTabList');
    p.tabWrapper = $('#tabWrapper');
    p.accrualNote = $('#accrualNote');

    p.body = SIA.global.vars.body;

    p.isRoundTrip = true;

    p.activeEarnTab = 0;

    // set tripType click handler
    p.oneWayBtn.parent().on({
      'click': function(e) {
        e.preventDefault();

        p.returnBtn.parent().removeClass('miles-cta-active');
        p.oneWayBtn.parent().addClass('miles-cta-active');

        p.isRoundTrip = false;

        // add selected value in hidden input field
        $('[name="earnMilesTripType"]').attr('value', 'O');

        paintHeading();
        paintEarnMiles();
      }
    });
    p.returnBtn.parent().on({
      'click': function(e) {
        e.preventDefault();

        p.oneWayBtn.parent().removeClass('miles-cta-active');
        p.returnBtn.parent().addClass('miles-cta-active');

        p.isRoundTrip = true;

        // add selected value in hidden input field
        $('[name="earnMilesTripType"]').attr('value', 'R');

        paintHeading();
        paintEarnMiles();
      }
    });

    if (typeof p.emData != 'undefined') {
      paintHeading();
      paintEarnMiles();
      painEarnMilesModal();
      populateCabinTypeClass(p.emData.accrualMiles, $('.earn-miles #calcTabSelect'));
    } else {
      disablePainting($('.earn-miles'));
    }
  };

  var disablePainting = function(el) {
    el.find('hr').addClass('hidden');
    el.find('.cabin-class-tab-control').addClass('hidden');
  };

  var paintHeading = function() {
    p.headingWrap.html('');

    var heading = $(_.template(p.headingTpl, {
      'origin': p.emData.origin,
      'destination': p.emData.destination
    }));

    p.headingWrap.append(heading);

    var shData = {
      'actualMiles': p.isRoundTrip
        ? p.emData.actualMilesRoundTrip.toLocaleString()
        : p.emData.actualMilesOneWay.toLocaleString(),
      'tripType': p.isRoundTrip
        ? 'Return'
        : 'One Way'
    };

    var subHead = $(_.template(p.subHeadingTpl, shData));
    p.headingWrap.append(subHead);
  };

  var paintEarnMiles = function() {
    var accrualMiles = p.emData.accrualMiles;
    var acL = accrualMiles.length;

    p.tabWrapper.html('');

    p.calcTabList.find('a[data-cabin-class]').each(function() {
      var c = $(this).data('cabin-class');
      var t = $(this);
      for (var i = 0; i < p.emData.accrualMiles.length; i++) {
        if (p.emData.accrualMiles[i].cabinClass.toLowerCase().trim() == c) {
          paintTable(p.emData.accrualMiles[i]);
        }

        t.parent().on({
            'click.earnTab': function(e){
                p.activeEarnTab = parseInt($(this).data('target-tab'));
            }
        });
      }
    });

    p.accrualNote.html(p.emData.accrualNote);

    if (typeof p.earnMilesTab != 'undefined')
      p.earnMilesTab.destroy();
    p.earnMilesTab = new SIA.SimpleTab();
    
    p.earnMilesTab.p.curTab = p.activeEarnTab;

    p.earnMilesTab.init($('#calcTabList'), $('#tabWrapper'));
  }

  var paintTable = function(data) {
    var accrual = data.accrualVO;
    var al = accrual.length;

    var activeTripType = p.isRoundTrip
      ? 'R'
      : 'O';
    var tableContentList = '';

    var rowCount = 0;
    for (var i = 0; i < al; i++) {
      var tripType = accrual[i].tripType;

      if (tripType === activeTripType) {
        // check if even / odd for changing the table content color background
        var isEven = '';
        if (rowCount % 2 == 0) {
          rowClass = 'even';
        } else {
          rowClass = 'odd';
        }

        // Paint the Table accrualMiles Data
        var tableContent = $(_.template(p.milesResultTableContent, {
          'isEven': rowClass,
          'title': accrual[i].fareFamily,
          'codes': accrual[i].sellingClass,
          'miles': accrual[i].miles.toLocaleString()
        }));

        tableContentList += tableContent[0].outerHTML;

        rowCount++;
      }
    }

    var tableEl = $(_.template(p.milesResultTable, {
      'tableContent': tableContentList,
      'id': p.tabWrapper.children().length
    }));

    p.tabWrapper.append(tableEl);
  };

  var painEarnMilesModal = function() {
    // data-trigger-popup
    var trigger = p.accrualNote.parent().find('[data-target-tpl]');

    $.ajax({
      url: 'template/' + trigger.data('target-tpl'),
      context: document.body,
      success: function(tplObj) {
        p.global.vars.body.append($(tplObj));
        initPopup();
      }
    });
  };

  var initPopup = function() {
    var triggerPopup = $('[data-target-tpl]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));
      var measureScrollbar = (function() {
        var a = document.createElement('div');
        a.className = 'modal-scrollbar-measure';
        $('body').append(a);
        var b = a.offsetWidth - a.clientWidth;
        return $(a).remove(),
        b;
      })();

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: p.global.config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',
          afterHide: function() {
            p.global.vars.container.css('padding-right', '');
            $('body').css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
              if (ua.indexOf('chrome') > -1) {} else {
                $('body').attr('style', function(i, s) {
                  return s.replace('overflow:hidden !important;', '');
                });
              }
            }
          }
        });
      }

      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        if (!self.hasClass('disabled')) {
          p.global.vars.win.trigger('click.hideTooltip');
          popup.Popup('show');
        }
      });

    });
  };

  var populateCabinTypeClass = function(data, el) {
    var cbVal = [];
    var tabList = el.find('option');

    // get all cabin class in json data
    $.each(data, function(index, value) {
      var val = data[index].cabinClass;
      cbVal.push(val.toLowerCase().trim());
    });

    // show available cabin class on tabs
    for (var i = 0; i < tabList.length; i++) {
      var item = tabList[i];
      var val = $(item).attr('data-label');
      var str = val.toLowerCase().trim();

      // if not found, hide this tab
      if (cbVal.indexOf(str) <= -1) {
        $(item).addClass('hidden');
      }
    }
  };

  var oPublic = {
    init: init,
    p: p
  };

  return oPublic;
}();

SIA.RedeemMiles = function() {
  var p = {};

  // rm = Redeem Miles
  p.rmData = globalJson.redeemMiles;

  // declare templates
  p.rmTableContentTpl = '<tr class="<%= isEven %>">\
    <td>\
    <span class="fare-title">Award type</span><br>\
		  <span class="fare__family__title"><%= title %> <%= toolTip %></span>\
		</td>\
    <td>\
    <span class="fare-title">KrisFlyer miles required</span><br>\
		  <span class="miles-num-text table__text__left"><strong><%= miles %></strong> miles</span>\
		</td>\
  </tr>';

  // initialize default states
  p.isRoundTrip = true;
  p.roundTrip = 'R';

  var init = function(titleTpl, selSumTpl, alertBoxTpl, tableMainTpl, tabMainContent, toggleBtnEvents, showErrorMsg) {
    p.rmTitleTpl = titleTpl;
    p.rmSelSumTpl = selSumTpl;
    p.rmTableMainTpl = tableMainTpl;
    p.rmTabMainEl = tabMainContent;
    p.rmHeadingWrap = p.rmTabMainEl.find('.heading-wrap');
    p.rmTableWrap = p.rmTabMainEl.find('.tab-wrapper');
    p.specialNoteWrap = p.rmTabMainEl.find('.special-note');
    p.noteWrap = p.rmTabMainEl.find('.note');

    // check first if there is business domain error in json
    if (typeof p.rmData != 'undefined' && p.rmData.errorMessage.length > 0 && p.rmData.redeemMiles.length == 0) {
      showErrorMsg(alertBoxTpl, p.rmData.errorMessage, p.rmTabMainEl);
    } else {
      if (typeof p.rmData != 'undefined') {
        paintHeading();
        paintTable();
      } else {
        disablePainting(p.rmTabMainEl);
      }
    }

    toggleBtnEvents(p.rmTabMainEl, '[name="redeemMilesTripType"]');
  };

  var disablePainting = function(el) {
    el.find('hr').addClass('hidden');
    el.find('.cabin-class-tab-control').addClass('hidden');
  };

  var paintHeading = function(getSelTripType, tableMainEl) {
    p.rmHeadingWrap.html('');

    p.rmHeadingWrap.append($(_.template(p.rmTitleTpl, {
      'origin': p.rmData.origin,
      'destination': p.rmData.destination
    })));
  };

  var paintTable = function() {
    p.rmTableWrap.html('');

    // get fare conditions tooltip content
    var mfcTpl = 'template/miles-calculator-tooltip.tpl';
    $.ajax({
      'url': mfcTpl,
      'async': false,
      'success': function(tplOjb) {
        p.toolTipTpl = tplOjb;
      }
    });

    var redeemMilesData = p.rmData.redeemMiles[0];

    // generate table according to selected cabin class type
    var tableRows = '';
    var tripType = '';
    var redeemVO = redeemMilesData.redeemVO;
    for (var i = 0; i < redeemVO.length; i++) {
      tripType = redeemVO[i].tripType;

      var tableRow = $(_.template(p.rmTableContentTpl, {
        'isEven': (
          ((i % 2) == 0)
          ? 'even'
          : 'odd'),
        'toolTip': p.toolTipTpl,
        'title': redeemVO[i].description,
        'miles': redeemVO[i].miles.toLocaleString()
      }));

      tableRows += tableRow[0].outerHTML;
    }

    p.rmHeadingWrap.append($(_.template(p.rmSelSumTpl, {
      'tripType': (tripType == 'R')
        ? 'Return'
        : 'One way'
    })));

    p.rmTableWrap.append($(_.template(p.rmTableMainTpl, {
      'header1': 'Award type',
      'header2': 'KrisFlyer miles required',
      'tableContent': tableRows,
      'id': ''
    })));

    // check if note is available
    var noteTxt = redeemMilesData.note;
    if (noteTxt != '') {
      p.noteWrap.html(noteTxt);
    }

    // check if special note is available
    var specialNoteTxt = redeemMilesData.specialNote;
    if (specialNoteTxt != '') {
      p.specialNoteWrap.html(specialNoteTxt);
    } else {
      p.specialNoteWrap.html('');
    }

    // initialize the ktooltip plugin
    $('[data-tooltip]').kTooltip();
  };

  var oPublic = {
    init: init,
    public: p
  };

  return oPublic;
}();

SIA.UpgradeFlight = function() {
  var p = {}

  // uf = Upgrade Flight
  p.ufData = globalJson.upgradeFlight;

  // declare templates
  p.ufSubtitleTpl = '<p class="upgrade-table-intro">Upgrade from <span class="mc-bold"><%= cabinClass %></span> &#8226;</em> <%= tripType %></p>';
  p.ufTableMainTpl = '<table class="table-1 table-responsive miles-calculator-table"> \
	  <thead class="hidden-mb"> \
		<tr> \
		  <th scope="col"><span class="fare-family-text">Upgrade from booking class</span></th> \
		  <th scope="col"><span class="fare-family-text">Redemption type</span></th> \
		  <th scope="col"><span class="fare-family-text">KrisFlyer miles required</span></th> \
		</tr> \
	  </thead> \
	  <tbody> \
	  <%= tableContent %> \
	  </tbody> \
	</table>';
  p.ufTableRowTpl = '<tr class="<%= isEven %>">  \
    <td> \
    <span class="fare-title">Upgrade from booking class</span><br>\
		<span class="fare-family-title"><%= fareFamily %></span> \
		<span class="fare-family-codes"><%= sellingClass %></span> \
	  </td> \
    <td> \
    <span class="fare-title">Redemption type</span><br>\
		<span class="miles-num-text table-tex-left"><%= redemptionType1 %></span><br> \
		<span class="miles-num-text table-tex-left"><%= redemptionType2 %></span> \
	  </td> \
    <td> \
    <span class="fare-title">KrisFlyer miles required</span><br>\
		<span class="miles-num-text table-text-left"><strong><%= miles1 %></strong> miles</span><br> \
		<span class="miles-num-text table-text-left"><strong><%= miles2 %></strong> miles</span> \
	  </td> \
	</tr>';
  p.noteTpl = '<p><%= note %></p>';
  p.specialNoteTpl = '<p><%= specialNote %></p>';
  p.termsConditionTpl = '<p class="terms-condition-text"><a href="#"><em class="ico-point-r ico-point-r-mid"></em>Terms and conditions</a></p>';

  var init = function(titleTpl, selSumTpl, alertBoxTpl, tabMainContent, toggleEvent, showErrorMsg) {
    p.ufTitleTpl = titleTpl;
    p.ufSelSumTpl = selSumTpl;
    p.ufTableMainEl = tabMainContent;
    p.titleWrap = p.ufTableMainEl.find('.heading-wrap');
    p.tableResult = p.ufTableMainEl.find('.miles-calculator-result');

    // check if there is a business domain error in json data
    if (typeof p.ufData != 'undefined' && p.ufData.errorMessage.length > 0 && p.ufData.upgradeMiles.length == 0) {
      showErrorMsg(alertBoxTpl, p.ufData.errorMessage, p.ufTableMainEl);
    } else {
      if (typeof p.ufData != 'undefined') {
        paintHeading();
        paintTable();
      } else {
        disablePainting(p.ufTableMainEl);
      }
    }

    toggleEvent(p.ufTableMainEl, '[name="upgradeFlightTripType"]');
  };

  var disablePainting = function(el) {
    el.find('hr').addClass('hidden');
    el.find('.cabin-class-tab-control').addClass('hidden');
  };

  var paintHeading = function(getSelTripType, tableMainEl) {
    p.titleWrap.html('');

    p.titleWrap.append($(_.template(p.ufTitleTpl, {
      'origin': p.ufData.origin,
      'destination': p.ufData.destination
    })));
  };

  var paintTable = function(getSelTripType, tableMainEl) {
    p.tableResult.html('');

    var upgradeMiles = p.ufData.upgradeMiles;

    // prepare table result
    for (var i = 0; i < upgradeMiles.length; i++) {
      var upgradeMile = upgradeMiles[i];

      // prepare table row result
      var tripType = '';
      var table = '';
      var tableRows = '';
      var fareFamilyRBD = upgradeMile.fareFamilyRBD;
      for (var j = 0; j < fareFamilyRBD.length; j++) {
        var ffRBD = fareFamilyRBD[j];
        tripType = ffRBD.tripType;
        var redemptionTypeVO = ffRBD.redemptionTypeVO;

        var tbrow = $(_.template(p.ufTableRowTpl, {
          'isEven': (
            ((j % 2) == 0)
            ? 'even'
            : 'odd'),
          'fareFamily': ffRBD.fareFamily,
          'sellingClass': ffRBD.sellingClass,
          'redemptionType1': redemptionTypeVO[0].redemptionType,
          'redemptionType2': redemptionTypeVO[1].redemptionType,
          'miles1': redemptionTypeVO[0].miles.toLocaleString(),
          'miles2': redemptionTypeVO[1].miles.toLocaleString()
        }));

        tableRows += tbrow[0].outerHTML;
      }

      tripType = (tripType === 'R')
        ? 'Return'
        : 'One way';

      if (i == 0) {
        p.titleWrap.append($(_.template(p.ufSelSumTpl, {'tripType': tripType})));
      }

      // prepare subtitle
      var subtitleTxt = $(_.template(p.ufSubtitleTpl, {
        'cabinClass': upgradeMile.category,
        'tripType': tripType
      }));

      table += subtitleTxt[0].outerHTML;
      table += tableRows;

      // add to main table
      var mainTable = $(_.template(p.ufTableMainTpl, {'tableContent': table}));

      p.tableResult.append(mainTable);

      // get note message
      p.tableResult.append($(_.template(p.noteTpl, {'note': upgradeMile.note})));

      // check if special note message is available
      p.tableResult.append($(_.template(p.specialNoteTpl, {
        'specialNote': (upgradeMile.specialNote)
          ? upgradeMile.specialNote
          : ''
      })));

      // add terms and condition
      p.tableResult.append(p.termsConditionTpl);

      if (upgradeMiles.length >= 2 && i == 0) {
        p.tableResult.append('<br>');
      }
    }
  };

  var oPublic = {
    init: init,
    public: p
  };

  return oPublic;
}();

SIA.SimpleTab = function() {
  var p = {};
  p.tabs = null;
  p.tabPanels = null;
  p.curTab = 0;

  var init = function(tabWrapper, tabContentWrapper) {
    p.tabContentWrapper = tabContentWrapper;
    p.tabSelect = tabWrapper.find('#calcTabSelect');
    p.tabList = tabWrapper.find('.miles-calculation-service-tab');

    p.tabSelect.prop("selectedIndex", p.curTab);

    resetStates();

    p.tabContentWrapper.find('[data-tab-id="'+p.curTab+'"]').addClass('active');
    p.tabList.find('[data-target-tab="'+p.curTab+'"]').addClass('active');

    p.tabSelect.off().on({
      'change': function(e) {
        var t = $(this);
        var label = p.tabSelect.find('option:selected').data('label');
        p.tabSelect.parent().find('.select__text').html(label);

        resetStates();

        p.tabContentWrapper.find('> [data-tab-id="' + t.val() + '"]').addClass('active');
        p.tabList.find('> [data-target-tab="' + t.val() + '"]').addClass('active');
      }
    });
  };

  var resetStates = function(){
    p.tabContentWrapper.find('> [data-tab-id]').removeClass('active');
    p.tabList.find('> [data-target-tab]').removeClass('active');
  };

  var destroy = function() {
    p.tabSelect.off();
  };

  var oPublic = {
    init: init,
    destroy: destroy,
    p: p
  };

  return oPublic;
};

$(function() {
  // check if underscore is loaded
  var waitForUnderscore = function() {
    setTimeout(function() {
      if (typeof _ == 'undefined') {
        waitForUnderscore();
      } else {
        SIA.milesCalculator.init();
      }
    }, 100);
  };

  typeof _ == 'undefined'
    ? waitForUnderscore()
    : SIA.milesCalculator.init();
});
