
/**
 * @name SIA
 * @description Define global Check image
 * @version 1.0
 */
SIA.checkImage = function(){
	var checkImage = $('[data-check-image]');

	checkImage.each(function(){
		var self = $(this);

		if(self.find('img').length){
			self.addClass('has-image');
		}
	});
};
