/**
 * @name SIA
 * @description Define global flightSchedule functions
 * @version 1.0
 */

SIA.flightSchedule = function() {
	var global = SIA.global;
	var config = global.config;
	// var win = global.vars.win;
	var noJsonTemplate = '<div class="alert-block checkin-alert error-alert">' +
		'<div class="inner">' +
		'<div class="alert__icon"><em class="ico-close-round-fill"></em></div>' +
		'<div class="alert__message">' + L10n.flightStatus.nodata + '</div>' +
		'</div>' +
		'</div>';

	var flightSchedulesPage = $('.flight-schedules-page');

	if (!flightSchedulesPage.length) {
		return;
	}

	var isRentina = function() {
		return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 2)) && /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	};

	var tabContents = flightSchedulesPage.find('.flight-schedules .tab-content');

	var flightScheduleTabPopup = function(hdlers, ppup, closePpup, pContent) {
		var handlers = hdlers,
			popup = ppup;

		if (handlers.length && popup.length) {
			handlers.each(function(index, el) {
				global.vars.popupGesture(popup, $(el), closePpup, pContent, true);
			});
		}
	};

	// var activeSchedule = flightSchedulesPage.find('.flight-schedules .tab-content.active');
	// var deactiveSchedule = flightSchedulesPage.find('.flight-schedules .tab-content:not(".active")');

	// var fillContent = function(res){
	// 	$.get(config.url.flightScheduleTemplate, function (data) {
	// 		var template = window._.template(data, {
	// 			data: res
	// 		});
	// 		contentSchedule.html(template);

	// 		// init after render html template
	// 		var popupAvailable = $('.popup--check-available');
	// 		var triggerCheckAvailable = contentSchedule.find('.schedule-check-availability');
	// 		// var tableDate = contentSchedule.find('.tablet-mobile table.table-date');
	// 		// var timerSetHeightFlightSchedule = 0;
	// 		var next = contentSchedule.find('.button-group .btn--next');
	// 		var prev = contentSchedule.find('.button-group .btn--prev');
	// 		var flyingFocus = $('#flying-focus');

	// 		if(!popupAvailable.data('Popup')){
	// 			popupAvailable.Popup({
	// 				overlayBGTemplate: config.template.overlay,
	// 				modalShowClass: '',
	// 				triggerCloseModal: '.popup__close',
	// 				afterShow: function(){
	// 					flyingFocus = $('#flying-focus');
	// 					if(flyingFocus.length){
	// 						flyingFocus.remove();
	// 					}
	// 				}
	// 			});
	// 		}

	// 		next.off('click.nextflight').on('click.nextflight', function(e){
	// 			e.preventDefault();
	// 			fillContent(globalJson.flightSchedulePrev);
	// 			setTimeout(function(){
	// 				$('html,body').animate({
	// 					'scrollTop': 0
	// 				});
	// 			}, 200);
	// 		});
	// 		prev.off('click.nextflight').on('click.nextflight', function(e){
	// 			e.preventDefault();
	// 			fillContent(globalJson.flightScheduleNext);
	// 			setTimeout(function(){
	// 				$('html,body').animate({
	// 					'scrollTop': 0
	// 				});
	// 			}, 200);
	// 		});

	// 		// win.off('resize.setHeightFlightSchedule').on('resize.setHeightFlightSchedule', function(){
	// 		// 	clearTimeout(timerSetHeightFlightSchedule);
	// 		// 	timerSetHeightFlightSchedule = setTimeout(function(){
	// 		// 		tableDate.each(function(idx){
	// 		// 			var tr = $(this).find('tr');
	// 		// 			if(win.width() < config.tablet - config.width.docScroll && win.width() >= config.mobile - config.width.docScroll){
	// 		// 				tr.eq(1).height($(this).prev().outerHeight() - (idx ? 0 : tr.eq(0).height()));
	// 		// 			}
	// 		// 			else{
	// 		// 				tr.eq(1).removeAttr('style');
	// 		// 			}
	// 		// 		});
	// 		// 	},150);
	// 		// }).trigger('resize.setHeightFlightSchedule');


	// 		triggerCheckAvailable.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
	// 			e.preventDefault();
	// 			popupAvailable.Popup('show');
	// 		});
	// 		// used for mobile
	// 		var checkFlightDetails = $('.check-flight-details');
	// 		// tab search
	// 		flightScheduleTabPopup(checkFlightDetails, checkFlightDetails.next(), '.popup__close', '.search-flight__content');

	// 	}, 'html');
	// };

	var fillContent = function(res, contentSchedule) {
		contentSchedule.html(config.template.loadingMedium);
		$.get(config.url.flightScheduleTemplate, function(data) {
			var template = window._.template(data, {
				data: res
			});
			contentSchedule.html(template);

			contentSchedule.off('click.showInformation').on('click.showInformation', '.flights--detail span', function() {
				var infFlightDetail = $(this).next();
				var self = $(this);
				// var flightsDetail = self.closest('.flights--detail');

				if (isRentina() && $('html').height() > 20000) {
					$('body').addClass('ip6');
				}

				if (infFlightDetail.is('.hidden')) {
					infFlightDetail.removeClass('hidden').hide();
				}

				self.toggleClass('active');

				// if(flightsDetail.data('infor')){
				infFlightDetail.stop().slideToggle(400);
				// }
				// else{
				// 	if(!infFlightDetail.is(':visible')) {
				// 		self.children('em').addClass('hidden');
				// 		self.children('.loading').removeClass('hidden');
				// 		$.ajax({
				// 			url: global.config.url.flightSearchFareFlightInfoJSON,
				// 			dataType: 'json',
				// 			type: global.config.ajaxMethod,
				// 			data: {
				// 				flightNumber: self.parent().data('flight-number'),
				// 				carrierCode: self.parent().data('carrier-code'),
				// 				date: self.parent().data('date'),
				// 				origin: self.parent().data('origin')
				// 			},
				// 			success: function(data) {
				// 				var flyingTime = '';
				// 				for(var ft in data.flyingTimes){
				// 					flyingTime = data.flyingTimes[ft];
				// 				}
				// 				var textAircraftType = L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType;
				// 				var textFlyingTime = L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime;
				// 				var rowAircraftType = $('<p>' + textAircraftType + '</p>');
				// 				var rowFlyingTime = $('<p>' + textFlyingTime + '</p>');
				// 				infFlightDetail.empty();
				// 				infFlightDetail.append(rowAircraftType, rowFlyingTime);
				// 				infFlightDetail.stop().slideToggle(400);
				// 			},
				// 			error: function(xhr, status) {
				// 				if(status !== 'abort'){
				// 					window.alert(L10n.flightSelect.errorGettingData);
				// 				}
				// 			},
				// 			complete: function() {
				// 				self.children('em').removeClass('hidden');
				// 				self.children('.loading').addClass('hidden');
				// 			}
				// 		});
				// 	}
				// 	else {
				// 		infFlightDetail.stop().slideToggle(400);
				// 	}
				// }
			});

			// init after render html template
			var popupAvailable = $('.popup--check-available');
			var triggerCheckAvailable = contentSchedule.find('.schedule-check-availability');
			// var tableDate = contentSchedule.find('.tablet-mobile table.table-date');
			// var timerSetHeightFlightSchedule = 0;
			// var next = contentSchedule.find('.button-group .btn--next');
			// var prev = contentSchedule.find('.button-group .btn--prev');
			var flyingFocus = $('#flying-focus');

			if (!popupAvailable.data('Popup')) {
				popupAvailable.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close',
					afterShow: function() {
						flyingFocus = $('#flying-focus');
						if (flyingFocus.length) {
							flyingFocus.remove();
						}
					},
					beforeHide: function() {
						//popupAvailable.find('[data-customselect]').customSelect('hide');
					}
					// closeViaOverlay: false
				});
			}

			// next.off('click.nextflight').on('click.nextflight', function(e){
			// 	e.preventDefault();
			// 	fillContent(globalJson.flightSchedulePrev);
			// 	setTimeout(function(){
			// 		$('html,body').animate({
			// 			'scrollTop': 0
			// 		});
			// 	}, 200);
			// });
			// prev.off('click.nextflight').on('click.nextflight', function(e){
			// 	e.preventDefault();
			// 	fillContent(globalJson.flightScheduleNext);
			// 	setTimeout(function(){
			// 		$('html,body').animate({
			// 			'scrollTop': 0
			// 		});
			// 	}, 200);
			// });

			// win.off('resize.setHeightFlightSchedule').on('resize.setHeightFlightSchedule', function(){
			// 	clearTimeout(timerSetHeightFlightSchedule);
			// 	timerSetHeightFlightSchedule = setTimeout(function(){
			// 		tableDate.each(function(idx){
			// 			var tr = $(this).find('tr');
			// 			if(win.width() < config.tablet - config.width.docScroll && win.width() >= config.mobile - config.width.docScroll){
			// 				tr.eq(1).height($(this).prev().outerHeight() - (idx ? 0 : tr.eq(0).height()));
			// 			}
			// 			else{
			// 				tr.eq(1).removeAttr('style');
			// 			}
			// 		});
			// 	},150);
			// }).trigger('resize.setHeightFlightSchedule');


			triggerCheckAvailable.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e) {
				e.preventDefault();
				popupAvailable.Popup('show');
			});
			// used for mobile
			var checkFlightDetails = $('.check-flight-details');
			// tab search
			flightScheduleTabPopup(checkFlightDetails, checkFlightDetails.next(), '.popup__close', '.search-flight__content');
		});
	};

	// $('.tabs--1').off('afterChange.triggerResize').on('afterChange.triggerResize', function(){
	// 	win.trigger('resize.setHeightFlightSchedule');
	// });

	var moreSevenTabs = function() {
		var wrapperTab = $('[data-wrapper-tab]', flightSchedulesPage),
			selectTab = wrapperTab.find('[data-select-tab]');
		if (wrapperTab.length) {
			var customSelectEl = wrapperTab.find('[data-customselect]'),
				curIndex = selectTab.prop('selectedIndex'),
				tabs = customSelectEl.closest('.tab').find('> li');

			var showTab = function() {
				curIndex = selectTab.prop('selectedIndex');
				tabs.eq(curIndex).removeClass('hidden');
			};

			selectTab.off('change.removeClassActive').on('change.removeClassActive', function() {
				showTab();
			});
		}
	};

	// fillContent(globalJson.flightSchedule.outBoundFlight, activeSchedule);
	// fillContent(globalJson.flightSchedule.inBoundFlight, deactiveSchedule);
	if (globalJson.flightSchedule) {
		// if(globalJson.flightSchedule.outBoundFlight) {
		// 	fillContent(globalJson.flightSchedule.outBoundFlight, activeSchedule, 'outBoundFlight');
		// }
		// if(globalJson.flightSchedule.inBoundFlight) {
		// 	fillContent(globalJson.flightSchedule.inBoundFlight, deactiveSchedule, 'inBoundFlight');
		// }
		tabContents.each(function(idx) {
			var tabContent = $(this);
			var res = globalJson.flightSchedule.boundList[idx];
			if (res) {
				fillContent(res, tabContent);
			} else {
				tabContent.html(noJsonTemplate);
			}
			// if(globalJson.flightSchedule.boundList[idx]){
			// 	fillContent(globalJson.flightSchedule.boundList[idx], $(this));
			// }
		});
		moreSevenTabs();
	} else {
		window.noJsonHandler = true;
		tabContents.html(noJsonTemplate);
		// used for mobile
		var checkFlightDetails = $('.check-flight-details');
		// tab search
		flightScheduleTabPopup(checkFlightDetails, checkFlightDetails.next(), '.popup__close', '.search-flight__content');
	}

	flightSchedulesPage.find('.check-flight-details').click(function() {
		$('#container').css('position', 'static');
	});

	// var wrapperTab = $('[data-wrapper-tab]')/*,
	// 		selectTab = wrapperTab.find('[data-select-tab]')*/;

	// if(wrapperTab.length) {
	// 	// selectTab.css('width', wrapperTab.find('.tab .active').outerWidth());
	// 	// selectTab.on('change.changeTestCustom', function() {
	// 	// 	setTimeout(function() { selectTab.css('width', wrapperTab.find('.tab .active').outerWidth()); }, 400);
	// 	// });

	// 	var customSelectEl = wrapperTab.find('[data-customselect]'),
	// 			dataCustomSel = customSelectEl.find('select').data('customSelect');
	// 	if(dataCustomSel) {
	// 		dataCustomSel.options.afterSelect = function() {
	// 			var displayTxtEl = customSelectEl.find(dataCustomSel.options.textCustom),
	// 					txt = displayTxtEl.text(),
	// 					txtReplace = customSelectEl.data('replaceTextByPlane'),
	// 					regx = new RegExp(' ' + txtReplace + ' ', 'gi');
	// 			displayTxtEl.html(txt.replace(regx, ' <em class="ico-plane"></em> '));
	// 		};
	// 		dataCustomSel.options.afterSelect();
	// 	}

	// 	wrapperTab.find('li.tab-item').removeClass('hidden');
	// }
};
