 /**
  * @name SIA
  * @description Define global home functions
  * @version 1.0
  */
SIA.darkSiteHome = function() {
	var global = SIA.global;
	// var vars = global.vars;
	var win = global.vars.win;
	var config = global.config;
	var popupPromo = global.vars.popupPromo;
	var travelWidget = $('#travel-widget');
	var formManageBooking = travelWidget.find('#form-manage-booking');
	var formCheckIn = travelWidget.find('#form-check-in');
	var formPackage = travelWidget.find('#form-packages');
	var formFlightStatus = travelWidget.find('#form-flight-status');
	var formFlightStatus1 = travelWidget.find('#form-flight-status-1');
	var travelWidgetVisibleInput = 'input[type="text"]';
	var samSungGalaxyS5ModelCode = 'SAMSUNG SM-G900';
	var popupPromoMember = $('.popup--promo-code-kf-member');
	var popupPromoKF = $('[data-popup-promokf]');
	var loginBtn = $('[data-trigger-popup]');

	$.validator.addMethod('bookingEticket', function(value) {
		if (value.length === 6 || value.length === 13) {
			if (value.length === 6) {}
			if (value.length === 13) {}
		} else {
			return false;
		}
		return true;
	}, L10n.validator.bookingEticket);

	var formTravel = travelWidget.find('#form-book-travel');
	var radioFilter = formTravel.find('.form-group--tooltips input[type="radio"]');
	var radioTooltips = formTravel.find('.radio-tooltips');

	radioFilter.each(function(index, el) {
		$(el).off('change.showTooltip').on('change.showTooltip', function() {
			radioTooltips.removeClass('active');
			radioTooltips.eq(index).addClass('active');
		});
		if ((global.vars.isIE() && global.vars.isIE() < 9) || global.vars.isSafari) {
			$(el).off('afterTicked.showTooltip').on('afterTicked.showTooltip', function() {
				$(el).trigger('change.showTooltip');
			});
		}
	});

	var _formPromotionValidation = function() {
		popupPromo.find('.form--promo').validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var validateFormGroup = function(formGroup) {
		formGroup.each(function() {
			var self = $(this);
			self.off('click.triggerValidate').on('click.triggerValidate', function() {
				formGroup.not(self).each(function() {
					if ($(this).data('change')) {
						$(this).find('select, input').valid();
					}
				});
			});

			self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function() {
				formGroup.not(self).each(function() {
					if ($(this).data('change')) {
						$(this).find('select, input').valid();
					}
				});
			}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function() {
				self.data('change', true);
			});
			self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function() {
				self.data('change', true);
			});
		});
	};


	var _manageBookingValidation = function() {
		var formGroup = formManageBooking.find('.form-group');
		validateFormGroup(formGroup);
		formManageBooking.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _checkInValidation = function() {
		var formGroup = formCheckIn.find('.form-group');
		validateFormGroup(formGroup);
		formCheckIn.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatusValidation = function() {
		var formGroup = formFlightStatus.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatus1Validation = function() {
		var formGroup = formFlightStatus1.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus1.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var formPromoKFValidation = function() {
		popupPromoKF.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			invalidHandler: function(form) {
				$(form).find('input').trigger('focus.promoCode');
			}
		});
	};

	var _formPackageValidation = function() {
		var ppSearchLeaving = $('.popup--search-leaving');
		var btnContinue = ppSearchLeaving.find('[data-continue]');

		ppSearchLeaving.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			beforeShow: function() {
				travelWidget.hide();
				ppSearchLeaving.data('parentContainerStyle', true);
			},
			afterShow: function() {
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
				orientationChanged = false;
				$(window).on('orientationchange.close-all-popup', function() {
					orientationChanged = true;
				});
			},
			beforeHide: function() {
				travelWidget.show();
			},
			afterHide: function() {
				if (!orientationChanged) {
					travelWidget.tabMenu('onResize');
				}
				$(window).off('orientationchange.close-all-popup');
				if (ppSearchLeaving.data('parentContainerStyle')) {
					ppSearchLeaving.removeData('parentContainerStyle');
					travelWidget.tabMenu('onResize');
				}
			}
		});

		btnContinue.off('click.searchPackage').on('click.searchPackage', function() {
			ppSearchLeaving.Popup('hide');
			formPackage[0].submit();
		});

		var formGroup = formPackage.find('.form-group');
		validateFormGroup(formGroup);

		formPackage.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				ppSearchLeaving.Popup('show');
				return false;
			}
		});
	};

	var initBookingTab = function() {
		travelWidget.tabMenu({
			tab: 'ul.tab .tab-item',
			tabContent: 'div.tab-content',
			activeClass: 'active',
			templateOverlay: config.template.overlay,
			zIndex: config.zIndex.tabContentOverlay,
			isPopup: true,
			afterChange: function() {
				if (isSamSungGalaxyS5()) {
					$('footer.footer').css('display', 'none');
					$('div.main-inner div.wrapper').css('height', '0px');
				}
			},
			beforeClosePopup: function() {
				if (isSamSungGalaxyS5()) {
					$('footer.footer').css('display', '');
					$('div.main-inner div.wrapper').css('height', '');
				}
			}
		});
	};

	initBookingTab();

	var highlightSlider = $('#highlight-slider');
	var wrapperHLS = highlightSlider.parent();
	var imgHighlightLength = highlightSlider.find('img').length - 1;
	var loadBackgroundHighlight = function(self, parentSelt, idx) {
		parentSelt.css({
			'background-image': 'url(' + self.attr('src') + ')'
		});
		self.attr('src', config.imgSrc.transparent);
		if (idx === imgHighlightLength) {
			if (window.innerWidth > 480) {
				highlightSlider.width(wrapperHLS.width() + 22);
			} else {
				highlightSlider.width(wrapperHLS.width());
			}
			highlightSlider.css('visibility', 'visible');
			highlightSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: 2,
					slidesToScroll: 2,
					accessibility: false,
					autoplay: false,
					pauseOnHover: false,
					responsive: [{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					}, {
						breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}]
				});
			win.off('resize.highlightSlider').on('resize.highlightSlider', function() {
				if (window.innerWidth > 480) {
					highlightSlider.width(wrapperHLS.width() + 22);
				} else {
					highlightSlider.width(wrapperHLS.width());
				}
			}).trigger('resize.highlightSlider');
		}
	};
	highlightSlider.find('img').each(function(idx) {
		var self = $(this);
		var parentSelt = self.parent();
		var nI = new Image();
		nI.onload = function() {
			loadBackgroundHighlight(self, parentSelt, idx);
		};
		nI.src = self.attr('src');
	});

	var triggerProCode = $('[data-promo-code-popup]');
	var flyingFocus = $('#flying-focus');

	if (globalJson.loggedUser) {
		popupPromo = popupPromoMember;
		global.vars.popupPromo = popupPromoMember;
	}

	var orientationChanged = false;
	popupPromo.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		beforeShow: function() {
			travelWidget.hide();
			popupPromo.data('parentContainerStyle', true);
		},
		afterShow: function() {
			flyingFocus = $('#flying-focus');
			if (flyingFocus.length) {
				flyingFocus.remove();
			}
			orientationChanged = false;
			$(window).on('orientationchange.close-all-popup', function() {
				orientationChanged = true;
			});
		},
		beforeHide: function() {
			travelWidget.show();
		},
		afterHide: function() {
			if (!orientationChanged) {
				travelWidget.tabMenu('onResize');
			}
			$(window).off('orientationchange.close-all-popup');
			if (popupPromo.data('parentContainerStyle')) {
				popupPromo.removeData('parentContainerStyle');
				travelWidget.tabMenu('onResize');
			}
		},
		triggerCloseModal: '.popup__close'
	});
	triggerProCode.off('click.showPromo').on('click.showPromo', function(e) {
		e.preventDefault();
		popupPromo.Popup('show');
	});

	_formPromotionValidation();
	_manageBookingValidation();
	_checkInValidation();
	_flightStatusValidation();
	_flightStatus1Validation();
	formPromoKFValidation();
	_formPackageValidation();
	loginBtn.off('click.triggerLoginPopup').on('click.triggerLoginPopup', function(e) {
		e.preventDefault();
		var loginPopupEl = $(loginBtn.data('popup'));
		if (loginBtn.data('keepContainerStyle')) {
			loginPopupEl.data('Popup').options.beforeShow = function() {
				loginPopupEl.data('parentContainerStyle', true);
			};
			loginPopupEl.data('Popup').options.afterHide = function() {
				if (global.vars.detectDevice.isMobile()) {
					travelWidget.tabMenu('onResize');
				}
				if (loginPopupEl.data('parentContainerStyle')) {
					loginPopupEl.removeData('parentContainerStyle');
				}
			};
		}
		loginPopupEl.Popup('show');
	});

	var checkEmptyInput = function(input) {
		var isEmpty = false;
		input.each(function() {
			if (!$(this).val()) {
				isEmpty = true;
			}
		});
		return isEmpty;
	};

	var changeText = function(form, input, btn) {
		var inputs = form.find(input);
		var b = form.find(btn);
		inputs.each(function() {
			var self = $(this);
			self.off('change.checkEmptyInput').on('change.checkEmptyInput', function() {
				if (!checkEmptyInput(inputs)) {
					b.val(L10n.home.proceed);
				} else {
					b.val(L10n.home.retrive);
				}
			});
		});
	};
	changeText(formManageBooking, travelWidgetVisibleInput, '#retrieve-1');
	changeText(formCheckIn, travelWidgetVisibleInput, '#retrieve-2');
	var isSamSungGalaxyS5 = function() {
		var result = false;
		if (window.navigator.userAgent.indexOf(samSungGalaxyS5ModelCode) >= 0) {
			result = true;
		}
		return result;
	};

	var bookingWidgetSwitch = function() {
		var manageBookingTabs = $('[data-manage-booking]');
		var manageBookingForms = $('[data-manage-booking-form]');

		var checkinTabs = $('[data-checkin]');
		var checkinForms = $('[data-checkin-form]');

		var flightStatusTabs = $('[data-flight-status]');
		var flightStatusForms = $('[data-flight-status-form]');

		var apply = function(tabs, form, dataTab, dataForm) {
			tabs
				.off('change.switch-tab')
				.on('change.switch-tab', function() {
					var data = $(this).data(dataTab);
					if (!dataForm) {
						dataForm = dataTab + '-form';
					}
					form.removeClass('active').filter('[data-' + dataForm + '="' + data + '"]').addClass('active');
				});
		};

		apply(manageBookingTabs, manageBookingForms, 'manage-booking');
		apply(checkinTabs, checkinForms, 'checkin');
		apply(flightStatusTabs, flightStatusForms, 'flight-status');

		var flightStatusFormSecond = flightStatusForms.filter('[data-flight-status-form="by-number"]');
		var optionDepartingArriving = flightStatusFormSecond.find('[data-option] input');
		var departingArriving = flightStatusFormSecond.find('[data-target]');

		optionDepartingArriving
			.off('change.changeDepartingArriving')
			.on('change.changeDepartingArriving', function() {
				var index = optionDepartingArriving.index($(this));
				departingArriving.removeClass('hidden').eq(index === 0 ? 1 : 0).addClass('hidden');
			});
	};

	bookingWidgetSwitch();

	var winH = win.height();
	popupPromoMember.find('input').off('focus.promoCode').on('focus.promoCode', function() {
		popupPromoMember.height(winH).find('.popup__inner').height(
			popupPromoMember.find('.popup__content').outerHeight(true) + 30
		);
	});

	var hideElement = function() {
		var bannerSlider = $('#banner-slider');
		bannerSlider.removeClass('flexslider');
		bannerSlider.find('img').remove();
		$('.wrapper.first').remove();
		$('.beta-footer').addClass('hidden');
		$('.footer-top').addClass('hidden');
		$('.footer .social').addClass('hidden');
	};

	// var initPackagesSlider = function(){
	// 	var packages = $('.packages');
	// 	var packagesSlider = packages.find('[data-slideshow]');

	// 	var startSlider = function(self, parentSelt, idx){
	// 		var imgBannerLength = packagesSlider.find('.slide-item img').length - 1;
	// 		var option = packagesSlider.data('option') ? $.parseJSON(packagesSlider.data('option').replace(/\'/gi, '"')) : {};
	// 		option.pauseOnHover = false;

	// 		if (!vars.isIEMobile()) {
	// 			parentSelt.css({
	// 				'background-image': 'url(' + self.attr('src') + ')'
	// 			});
	// 			self.attr('src', config.imgSrc.transparent);
	// 		}

	// 		if(idx === imgBannerLength){
	// 			setTimeout(function(){
	// 				packagesSlider.css('visibility', 'visible');
	// 				packagesSlider.find('.slides').slick(option);
	// 			}, 2000);
	// 		}
	// 	};

	// 	packagesSlider.find('.slide-item img').each(function(idx) {
	// 		var self = $(this);
	// 		var parentSelt = self.parent();
	// 		var nI = new Image();
	// 		nI.onload = function() {
	// 			startSlider(self, parentSelt, idx);
	// 		};

	// 		nI.src = self.attr('src');
	// 	});
	// };

	var packageSlideShow = $('[data-slideshow]');

	var initPackagesSlider = function(){
		var option = packageSlideShow.data('option') ? $.parseJSON(packageSlideShow.data('option').replace(/\'/gi, '"')) : {};
		option.siaCustomisations = true;
		var imgPromotionLength = packageSlideShow.find('img').length - 1;
		var loadBackgroundPromotion = function(self, parentSelt, idx){
			if(idx === imgPromotionLength){
				packageSlideShow.css('visibility', 'visible');
				packageSlideShow.find('.slides').slick(option);
			}
		};

		option.autoplay = false;
		packageSlideShow.find('img').each(function(idx) {
			var self = $(this);
			var parentSelt = self.parent();
			var nI = new Image();
			nI.onload = function(){
				loadBackgroundPromotion(self, parentSelt, idx);
			};
			nI.src = self.attr('src');
		});
	};

	initPackagesSlider();
	hideElement();
};
