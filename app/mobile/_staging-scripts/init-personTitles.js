/**
 * @name SIA
 * @description Define global initPersonTitles functions
 * @version 1.0
 */
SIA.initPersonTitles = function(){
	// var generateOption = function(jsonData) {
	// 	var options = '';
	// 	for(var i = 0; i < jsonData.length; i++) {
	// 		options += '<option value="' + jsonData[i] + '">' + jsonData[i] + '</option>';
	// 	}
	// 	return options;
	// };

	var generateOption = function(jsonData, isSelected, initValue) {
		var options = '';
		isSelected = isSelected || false;
		var template = function(i){
			var tmp = '<option ' + (isSelected && i === 0 ? 'selected="selected"' : '') + ' value="' + (isSelected && i === 0 ? '' : jsonData[i]) + '">' + jsonData[i] + '</option>';
			if(initValue){
				tmp = '<option ' + (jsonData[i] === initValue ? 'selected="selected"' : '') + ' value="' + (isSelected && i === 0 ? '' : jsonData[i]) + '">' + jsonData[i] + '</option>';
			}
			return tmp;
		};

		for(var i = 0; i < jsonData.length; i++) {
			options += template(i);
		}

		return options;
	};

	if(globalJson.titles) {
		$('[data-person-title]').each(function() {
			var that = $(this);
			var initValue = that.find('option:selected').text();
			that.html(generateOption(globalJson.titles, true, initValue)).defaultSelect('refresh');
			// $(this).html(generateOption(globalJson.titles)).defaultSelect('refresh');
		});
	}

	if(globalJson.otherTitles) {
		$('[data-person-title]').each(function() {
			$(this).off('change.title').on('change.title', function() {
				if(this.value.toLowerCase() === 'others') {
					var that = $(this);
					that.children().last().remove();
					that.append(generateOption(globalJson.otherTitles, false)).defaultSelect('refresh');
				}
			});
		});
	}
};
