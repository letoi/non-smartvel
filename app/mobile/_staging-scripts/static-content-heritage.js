/**
 * @name SIA
 * @description Define global staticContentHeritage functions
 * @version 1.0
 */
SIA.staticContentHeritage = function() {
	var initNavMenu = function() {
		var bodyEl = $('html,body'),
				listBlockEl = $('[data-list-block]'),
				navEl = $('[data-device-submenu]'),
				menuLink = navEl.find('select');

		menuLink.off('change.scrollToBlock').on('change.scrollToBlock', function(e) {
			e.preventDefault();
			var self = $(this),
					idBlock = self.val();

			bodyEl.animate({scrollTop: $('#' + idBlock).offset().top}, 400);
		});

		listBlockEl.children().last().addClass('last');
	};

	var initModule = function() {
		initNavMenu();
	};

	initModule();
};
