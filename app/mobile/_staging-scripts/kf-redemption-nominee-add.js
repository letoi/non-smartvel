/**
 * @name SIA
 * @description Define global kf-personal-detail function
 * @version 1.0
 */
SIA.KFRedemptionNominee = function(){
	if(!$('.add-redemption-nominee-page').length){
		return;
	}
	var global = SIA.global;
	var formRedemptionNominee = $('.form--redemption-nominee');
	var wrapCheckInput = $('[data-disable-value]',formRedemptionNominee);
	var radio = $('input:radio', formRedemptionNominee);
	formRedemptionNominee.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess,
		invalidHandler: global.vars.invalidHandler
	});
	wrapCheckInput.each(function(){
		var wrap = $(this);
		var checkBox = $('input:checkbox', wrap);
		var inputSibling = $('input:text', wrap);
		if(inputSibling.data('ruleRequired')) {
			checkBox.each(function(i){
				var self = $(this);
				var sibl = inputSibling.eq(i);
				self.off('change.disabledInputSibling').on('change.disabledInputSibling', function(){
					if(self.is(':checked')){
						sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
						wrap.find('.text-error').remove();
						wrap.find('.error').removeClass('error');
					}
					else{
						sibl.prop('disabled', false).closest('span').removeClass('disabled');
					}
				});
			});
		}
	});
	radio.each(function(){
		var self = $(this);
		var selfParent = self.parents('.form-group');
		var nomineeCheckBox = selfParent.find('.authorised-nominee');

		self.off('change').on('change', function(){
			if( self.val() === 'yes' ){
				nomineeCheckBox.show();
				nomineeCheckBox.find('input').attr('data-rule-required', true);
			}
			else{
				nomineeCheckBox.hide();
				nomineeCheckBox.find('input').attr('data-rule-required', false);
				selfParent.find('.error').removeClass('error');
				selfParent.find('.text-error').remove();
			}
		});
	});
};
