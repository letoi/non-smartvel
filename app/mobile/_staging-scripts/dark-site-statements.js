/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteStatements = function() {
	var players = [];

	var grayscaleLogo = function(){
		var logo = $('.logo').find('img');
		var newSrc = {
			svg: 'images/svg/logo-singaporeairlines-white.svg',
			png: 'images/logo-singaporeairlines-white.png'
		};
		logo.eq(0).attr('src', newSrc.svg);
		logo.eq(1).attr('src', newSrc.png);
	};
	grayscaleLogo();

	var initYoutubePlayer = function(playerInfoList) {
		var createPlayer = function(playerInfo) {
			return new window.YT.Player(playerInfo.id, {
				height: playerInfo.h,
				width: playerInfo.w,
				videoId: playerInfo.url
			});
		};

		for (var i = 0; i < playerInfoList.length; i++) {
			var curplayer = createPlayer(playerInfoList[i]);
			players.push(curplayer);
		}
	};

	var initSlider = function() {
		var youtubeList = $('.watch-list-1');
		var slideEl = $('.slick-slide');
		var activeEl = $('.slick-active');
		var listPlayers = [];

		var updateElement = function() {
			slideEl = $('.slick-slide').find('.info-watch-1');
			activeEl = $('.slick-active').find('.info-watch-1');
			slideEl.each(function(idx) {
				var item = $(this);
				var playerOpt = {};
				item.attr('id', 'player-mb-' + idx);
				playerOpt.id = 'player-mb-' + idx;
				playerOpt.url = item.closest('a').data('statement-url');
				playerOpt.w = item.width();
				playerOpt.h = item.height();
				listPlayers.push(playerOpt);
			});
		};

		var handleArrows = function() {
			var slides = $('.slick-slide');
			var len = slides.length;
			var prevBtn = $('.flexslider-prev');
			var nextBtn = $('.flexslider-next');
			nextBtn.css({
				'display': slides.eq(len - 1).hasClass('slick-active') ? 'none' : 'block'
			});
			prevBtn.css({
				'display': slides.eq(0).hasClass('slick-active') ? 'none' : 'block'
			});
		};

		youtubeList.on('init', function() {
			handleArrows();
			updateElement();
			initYoutubePlayer(listPlayers);
		}).on('afterChange', function() {
			handleArrows();
		}).on('beforeChange', function() {
			activeEl = $('.slick-active').find('.info-watch-1');
			var id = activeEl.attr('id');
			var currentPlayer = players.filter(function(a) {
				return $(a.f).attr('id') === id;
			});
			currentPlayer[0].stopVideo();
		}).slick({
			siaCustomisations: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			draggable: false,
			slide: 'div',
			useCSS: false,
			infinite: false,
			touchMove: false,
			nextArrow: '<a href="#" class="slick-next flexslider-next">Next</a>',
			prevArrow: '<a href="#" class="slick-prev flexslider-prev">Prev</a>'
		});
	};

	var initModule = function() {
		initSlider();
	};

	initModule();
};
