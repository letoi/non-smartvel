/**
 * @name SIA
 * @description Define global togglePrivacy functions
 * @version 1.0
 */
SIA.togglePrivacy = function() {
	var privacyEl = $('[data-toggle-privacy]');
	var toggleChb = privacyEl.find('input:checkbox');
	var toggleEl = $('[data-privacy-passenger]');

	toggleChb
		.off('change.togglePrivacy')
		.on('change.togglePrivacy', function() {
			toggleEl.toggleClass('hidden', !$(this).prop('checked'));
		}).trigger('change.togglePrivacy');
};
