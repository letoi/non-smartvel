/**
 * @name SIA
 * @description Define function to positioning specific block when window scrolling
 * @version 1.0
 */
SIA.blockScroll = function() {
	var win = $(window);
	var scrollTimer = null;
	var global = SIA.global;
	var block = $('[data-block-scroll]');
	var leftQb = block.offset().left;
	var footer = $('footer.footer');
	var winHeight = win.height();
	var footTop = footer.offset().top;
	var contentEl = $('[data-main-content]');
	var isIgnoreTop = block.height() > contentEl.height();

	var safariOnMac = (Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0) && (navigator.appVersion.indexOf('Mac')!==-1);
	var scrollWidth = safariOnMac ? 17 : 0;
	var blockTop = (window.innerWidth - scrollWidth <= global.config.tablet) ? 0 : block.offset().top;
	var tmp = 1;

	win.off('scroll.blockScroll').on('scroll.blockScroll', function() {
		clearTimeout(scrollTimer);
		scrollTimer = setTimeout(function() {
			calcOffsetblock(win.scrollTop(), blockTop);
		}, 10);
	}).off('resize.resetBlockScroll').on('resize.resetBlockScroll', function() {
		// blockTop = block.offset().top;
		clearTimeout(scrollTimer);
		scrollTimer = setTimeout(function() {
			if (window.innerWidth - scrollWidth <= global.config.tablet) {
				block.removeAttr('style');
			} else {
				block.removeAttr('style');
				if(tmp){
					blockTop = block.offset().top;
					tmp = 0;
				}
				leftQb = block.offset().left;
				calcOffsetblock(win.scrollTop(), blockTop);

			}
		}, 300);
	}).trigger('scroll.blockScroll');

	// Set or remove style for block need scroll
	var calcOffsetblock = function(currentTop, qbTop) {
		if (block.length) {
			if (window.innerWidth - scrollWidth >= global.config.tablet) {
				var winTop = $(window).scrollTop();
				var bmHeight = block.outerHeight();
				winHeight = win.height();
				footTop = footer.offset().top;
				isIgnoreTop = block.height() > contentEl.height();
				if(!isIgnoreTop){
					if (currentTop > qbTop) {
						block.css({
							'position': 'fixed',
							'left': leftQb
						});
					} else {
						block.removeAttr('style');
					}
				}else{
					block.removeAttr('style');
				}

				if(bmHeight <= winHeight) {
					//if widget's height is smaller than window's height
					if(winTop + bmHeight >= footTop-20) {
						block.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});


					}
					else {
						block.css({
							top: '10px',
							bottom: ''
						});
					}
				}
				else {
					//widget's height is larger than window's height
					if(winTop + winHeight >= footTop) {
						block.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});
					}
					else {
						block.css({
							top: '10px',
							bottom: ''
						});
					}
				}
			}
		}
	};
};
