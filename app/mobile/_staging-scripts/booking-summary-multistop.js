/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
SIA.bookingSummnary = function(){
	var global = SIA.global;
	var config = global.config;
	var dataDepart = {},
      dataReturn = {};
  	var cookieData = {};
	//var win = global.vars.win;

	$.validator.addMethod('validateEmail', function(value) {
		if(!value.length){
			return true;
		}
		var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return emailReg.test(value);
	}, L10n.validator.validateEmail);

	$.validator.addMethod('validateAtLeastOne', function(value, el, param) {
		var form = $(el).closest('form');
		var allField = form.find(param);
		var valid = false;
		allField.find('input:text, input[type="tel"]').each(function(){
			if($.trim($(this).val()) !== ''){
				valid = true;
				return false;
			}
		});
		return valid;
	}, L10n.validator.validateAtLeastOne);

	if(!$('.sia-breadcrumb').length || $('.passenger-details-page').length){
		if(!$('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
      return;
    }
	}

	// init checkAllList function
	var booking = $('.booking');
	booking.each(function(){
		var bookingPassengerHeading = $('.booking-passenger-heading', this);
		var btn = $('.btn-1', this);
		bookingPassengerHeading.each(function(i){
			var self = $(this);
			global.vars.checkAllList(self.find(':checkbox'), self.siblings('.booking-passenger-list'), function(con){
				if(!i){
					if(con){
						btn.removeClass('disabled').prop('disabled', false);
					}
					else{
						btn.addClass('disabled').prop('disabled', true);
					}
				}
			});
		});
	});

	// $('.booking-control a.btn-1').off('click.disabled').on('click.disabled', function(e){
	// 	if($(this).hasClass('disabled')){
	// 		e.preventDefault();
	// 	}
	// });

	// Init accordion
	var initAccordion = function(){
		if($('.main-inner .booking').length){
			if(!$('.main-inner').data('accordion')) {
				$('.main-inner').accordion({
					wrapper: '.booking .booking-item',
					triggerAccordion: 'a.booking-passenger__control',
					contentAccordion: 'div.booking-passenger-content',
					activeClass: 'active',
					duration: 600
				});
			}
		}
	};

	/*var enableCheckboxes = function(trigger, trigger2, nextFlight, passenger1, passenger2, openNextFlight){
		trigger.off('change.enableCheckboxes').on('change.enableCheckboxes', function(){
			if(trigger.is(':checked')){
				nextFlight.removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': false
				}).parent().removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', false).parent().removeClass('disabled');
				if(!openNextFlight.hasClass('active')){
					openNextFlight.trigger('click.accordion');
				}
			}
			else{
				nextFlight.addClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': true,
					'checked': false
				}).parent().addClass('disabled').eq(0).closest('ul').prev().find(':checkbox').prop({
					'checked': false
				});
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', true).parent().addClass('disabled');
			}
		});
		if(trigger.is(':checked')){
			trigger.trigger('change.checkAllList').trigger('change.enableCheckboxes');
		}

		passenger1.each(function(idx){
			var self = $(this);
			self.off('change.selectPassenger').on('change.selectPassenger', function(){
				if(self.is(':checked')){
					trigger2.prop('disabled', false).parent().removeClass('disabled');
					passenger2.eq(idx).prop({
						'disabled': false
					}).parent().removeClass('disabled');
					passenger2.eq(idx).trigger('change.checkAllList');
					if(!openNextFlight.hasClass('active')){
						openNextFlight.trigger('click.accordion');
					}
					if(nextFlight.hasClass('disabled')){
						nextFlight.removeClass('disabled');
					}
				}
				else{
					passenger2.eq(idx).prop({
						'disabled': true,
						'checked': false
					}).parent().addClass('disabled');
					if(!passenger1.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).is(':checked')){
						nextFlight.addClass('disabled');
						trigger2.prop('disabled', true).parent().addClass('disabled');
					}
					passenger2.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).trigger('change.checkAllList');
				}
			});
		});
	};

	var bookingCheckboxAllFirstFlight = $('#booking-checkbox-all-1');
	var bookingCheckboxAllSecondFlight = $('#booking-checkbox-all-2');
	var wrapperFirstFlight = bookingCheckboxAllFirstFlight.closest('.booking-item');
	var nextFlight = wrapperFirstFlight.siblings('.booking-item');
	initAccordion();
	enableCheckboxes(bookingCheckboxAllFirstFlight, bookingCheckboxAllSecondFlight, nextFlight.children('.sub-heading-3--dark'), bookingCheckboxAllFirstFlight.closest('.booking-passenger-heading').next().find(':checkbox'), nextFlight.find(':checkbox').not('#booking-checkbox-all-2'), nextFlight.find('a.booking-passenger__control'));*/

	// Enable checkbox
	var enableCheckboxes = function(trigger, trigger2, nextFlight, passenger1, passenger2, openNextFlight){
		var trigger2Length = trigger2.length,
				passenger2Length = passenger2.length,
				loopCheckbox = passenger2Length / trigger2Length;

		trigger.off('change.enableCheckboxes').on('change.enableCheckboxes', function(){
			if(trigger.is(':checked')){
				nextFlight.removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': false
				}).parent().removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', false).parent().removeClass('disabled');
				if(!openNextFlight.hasClass('active')){
					openNextFlight.trigger('click.accordion');
				}
			}
			else{
				nextFlight.addClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': true,
					'checked': false
				}).parent().addClass('disabled').eq(0).closest('ul').prev().find(':checkbox').prop({
					'checked': false
				});
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', true).parent().addClass('disabled');
			}
		});

		if(trigger.is(':checked')){
			trigger.trigger('change.checkAllList').trigger('change.enableCheckboxes');
		}

		passenger1.each(function(idx){
			var self = $(this);
			self.off('change.selectPassenger').on('change.selectPassenger', function(){
				if(self.is(':checked')){
					trigger2.prop('disabled', false).parent().removeClass('disabled');

					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).prop({
						'disabled': false
					}).parent().removeClass('disabled');

					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).trigger('change.checkAllList');

					// if(!openNextFlight.hasClass('active')){
					// 	openNextFlight.trigger('click.accordion');
					// }

					openNextFlight.each(function(){
						var self = $(this);
						if(!self.hasClass('active')){
							self.addClass('active').siblings('.booking-passenger-content').slideDown(400);
						}
					});

					if(nextFlight.hasClass('disabled')){
						nextFlight.removeClass('disabled');
					}
				}
				else{
					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).prop({
						'disabled': true,
						'checked': false
					}).parent().addClass('disabled');

					if(!passenger1.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).is(':checked')){
						nextFlight.addClass('disabled');
						trigger2.prop('disabled', true).parent().addClass('disabled');
					}

					passenger2.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).trigger('change.checkAllList');
				}
			});
		});
	};

	var bookingCheckboxAllFirstFlight = $('#booking-checkbox-all-1');
	var bookingCheckboxAllOtherFlight = $('[data-checkbox-all]').not(bookingCheckboxAllFirstFlight);
	var wrapperFirstFlight = bookingCheckboxAllFirstFlight.closest('.booking-item');
	var nextFlight = wrapperFirstFlight.siblings('.booking-item');
	initAccordion();

	enableCheckboxes(
		bookingCheckboxAllFirstFlight,
		bookingCheckboxAllOtherFlight,
		nextFlight.children('.sub-heading-3--dark'),
		bookingCheckboxAllFirstFlight.closest('.booking-passenger-heading').next().find(':checkbox'),
		nextFlight.find(':checkbox').not(bookingCheckboxAllOtherFlight),
		nextFlight.find('a.booking-passenger__control')
	);

	var formTwo = $('#form-booking-2');
	formTwo.find(':checkbox').each(function() {
		if($(this).is(':checked')) {
			formTwo.find('input[type="submit"]').removeClass('disabled').prop('disabled', false);
			formTwo.find('.booking-passenger__control').trigger('click.accordion');
			return;
		}
	});

	var flyingFocus = $('#flying-focus');

	// cancel popup
	var cancelCheckIn = $('.cancel-flight');
	var cancelAllFlight = $('.cancel-all-flight');
	var popupCancelFlight = $('.popup--checkin-cancel');
	var popupCancelFlightConfirm = $('.popup--checkin-cancel-confirm');
	var popupCancelAllFlight = $('.popup--checkin-cancel-all');
	var popupCancelAllFlightConfirm = $('.popup--checkin-cancel-all-confirm');
	var popupCheckinErrorMessage = $('.popup--checkin-error-message');

	popupCancelFlight.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#success-flight-submit').off('click.confirm').on('click.confirm', function(e){
		e.preventDefault();
		popupCancelFlight.Popup('hide');
		// fix for Safari
		setTimeout(function(){
			popupCancelFlightConfirm.Popup('show');
		}, 200);
	});

	popupCheckinErrorMessage.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	});

	popupCancelAllFlight.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#cancel-checkbox-cancel-submit').off('click.confirm').on('click.confirm', function(e){
		e.preventDefault();
		popupCancelAllFlight.Popup('hide');
		// fix for Safari
		setTimeout(function(){
			popupCancelAllFlightConfirm.Popup('show');
		}, 200);
	});

	popupCancelFlightConfirm.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	});

	popupCancelAllFlightConfirm.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	});

	cancelCheckIn.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(!$(this).hasClass('disabled')){
				if(self.hasClass('first-flight')){
					popupCheckinErrorMessage.Popup('show');
				}
				else{
					popupCancelFlight.Popup('show');
				}
			}
		});
		self.closest('li').prev().find('a').off('click.changeSeat').on('click.changeSeat', function(e){
			if($(this).hasClass('disable')){
				e.preventDefault();
			}
		});
	});

	cancelAllFlight.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(self.hasClass('disabled')){
				return;
			}
			if(self.hasClass('first-flight')){
				popupCheckinErrorMessage.Popup('show');
			}
			else{
				popupCancelAllFlight.Popup('show');
			}
		});
	});
	popupCancelFlight.find('.table-default > .table-row--heading').each(function(){
		global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
	});
	popupCancelAllFlight.find('.table-default > .table-row--heading').each(function(){
		global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
	});

	// sms popup
	// var sms = $('.trigger-sms');
	// var popupSMS = $('#sendPhoneNumber');
	// var formSMS = popupSMS.find('.form--phone-number');
	// var sendFormSMS = $('.popup--phone-number .form--phone-number');
	// var popupSMSContent = popupSMS.find('.popup__content > div');
	// var popupReSMS = $('.popup--phone-number');
	// var isReSMS = false;

	// popupSMS.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	closeViaOverlay: false
	// });
	// // .find('#enter-phone-email-submit-1').off('click.confirm').on('click.confirm', function(){
	// 	// e.preventDefault();
	// 	// if(formSMS.valid()) {
	// 	// 	isReSMS = true;
	// 	// 	popupSMSContent.eq(0).addClass('hidden');
	// 	// 	popupSMSContent.eq(1).removeClass('hidden');
	// 	// 	setTimeout(function(){
	// 	// 		popupSMS.Popup('reset');
	// 	// 		popupSMS.Popup('reposition');
	// 	// 		flyingFocus = $('#flying-focus');
	// 	// 		if(flyingFocus.length){
	// 	// 			flyingFocus.remove();
	// 	// 		}
	// 	// 	}, 100);
	// 	// }
	// // });
	// popupReSMS.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	closeViaOverlay: false
	// });

	// sms.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
	// 	e.preventDefault();
	// 	if(isReSMS){
	// 		popupReSMS.Popup('show');
	// 	}
	// 	else{
	// 		popupSMS.Popup('show');
	// 	}
	// });

	// // email
	// var emailTrigger = $('.trigger-email');
	// var popupEmailAddress = $('#sendEmail');
	// var emailForm = popupEmailAddress.find('form');
	// var popupEmailAddressContent = popupEmailAddress.find('.popup__content > div');
	// // var popupEmailAddress = $('.popup--email-address1');
	// // var popupEmailConfirm = $('.popup--email-confirm1');

	// popupEmailAddress.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	afterHide: function(){
	// 		popupEmailAddressContent.eq(1).addClass('hidden');
	// 		popupEmailAddressContent.eq(0).removeClass('hidden');
	// 	},
	// 	closeViaOverlay: false
	// });
	// // .find('#email-confirm-submit-2').off('click.confirm').on('click.confirm', function(){
	// // 	// e.preventDefault();
	// // 	if(emailForm.valid()) {
	// // 		popupEmailAddressContent.eq(0).addClass('hidden');
	// // 		popupEmailAddressContent.eq(1).removeClass('hidden');
	// // 		setTimeout(function(){
	// // 			popupEmailAddress.Popup('reset');
	// // 			popupEmailAddress.Popup('reposition');
	// // 			flyingFocus = $('#flying-focus');
	// // 			if(flyingFocus.length){
	// // 				flyingFocus.remove();
	// // 			}
	// // 		}, 100);
	// // 	}
	// // });
	// popupEmailAddress.find('#email-address-submit-3').off('click.addEmail').on('click.addEmail', function(e){
	// 	e.preventDefault();
	// 	var newInput = $(config.template.addEmail.format(($(this).closest('.table-row').prev().children().length + 1), L10n.validation.email)).appendTo($(this).closest('.table-row').prev());
	// 	if(!window.Modernizr.input.placeholder){
	// 		newInput.find('input').placeholder();
	// 		newInput.find('input').addClear();
	// 	}
	// });

	// emailTrigger.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
	// 	e.preventDefault();
	// 	popupEmailAddress.Popup('show');
	// });

	// boarding-pass
	var boardingPassTrigger =  $('.boarding-pass');
	var popupBoarding = $('.popup--boarding-3');
	var popupBoardingContent = popupBoarding.find('.popup__content > div:first');
	var popupBoardingEmail = popupBoarding.find('.boarding-1');
	var popupBoardingSMS = popupBoarding.find('.boarding-2');
	var boardingForm = popupBoardingContent.find('form');
	var show = function(popup){
		popup.show().css('opacity', 0).animate({'opacity': 1}, 400);
		popupBoardingContent.animate({'opacity': 0}, 400).hide();
	};

	// set value for Boarding SMS
	var setValueForBoardingSMS = function(){
		var boardingSMS = popupBoarding.find('.tab-content.sms'),
				smsRow = boardingSMS.find('.table-row');
		var countryVal, areaVal, phoneVal;
		smsRow.each(function(idx, ele){
			countryVal= $(ele).find('[data-country]').val();
			areaVal= $(ele).find('[data-area]').val();
			phoneVal= $(ele).find('[data-phone]').val();

			popupBoardingSMS.find('.table-row').eq(idx).find('[data-country] .select__text').text(countryVal);
			popupBoardingSMS.find('.table-row').eq(idx).find('[data-area] input').val(areaVal);
			popupBoardingSMS.find('.table-row').eq(idx).find('[data-phone] input').val(phoneVal);
		});
	};

	// set value for Boarding Email
	var setValueForBoardingEmail = function(){
		var boardingEmail = popupBoarding.find('.tab-content.email'),
				emailRow = boardingEmail.find('.table-row');
		var emailVal;
		emailRow.each(function(idx, ele){
			emailVal= $(ele).find('[data-email]').val();
			popupBoardingEmail.find('.table-row').eq(idx).find('[data-email] input').val(emailVal);
		});
	};

	popupBoarding.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#boarding-10-submit').off('click.confirm').on('click.confirm', function(){
		// e.preventDefault();
		if(boardingForm.valid()) {
			var tab = $(this).closest('form').find('.tabs--1').find('.tab-item.active');
			if(tab.index()){
				setValueForBoardingSMS();
				show(popupBoardingSMS);
			}
			else{
				setValueForBoardingEmail();
				show(popupBoardingEmail);
			}
		}
	});

	popupBoarding.find('[data-tab]').off('afterChange.resizeModal').on('afterChange.resizeModal', function(){
		// popupBoarding.css('height', win.height());
		// popupBoarding.find('.popup__content').css('height', win.height());
		setTimeout(function(){
			popupBoarding.Popup('reset');
		}, 700);
	});

	boardingPassTrigger.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(!self.hasClass('disable')){
				popupBoardingContent.show();
				popupBoardingContent.css({
					'opacity': 1,
					'visibility': 'visible'
				});
				popupBoardingSMS.hide();
				popupBoardingEmail.hide();
				popupBoarding.Popup('show');
			}
			// else{
				// popupCheckinErrorMessage.Popup('show');
			// }
		});
	});

	// dynamic height
	/*var bkif = $('.booking-info');
	var timerSetHeight = null;
	if(bkif.length){
		var setHeightChecked = function(){
			bkif.each(function(){
				var h = 0;
				var ch = $(this).children();
				ch.css('height', '');
				ch.each(function(){
					var self = $(this);
					if(h < self.height()){
						h = self.height();
					}
				});
				ch.height(h);
			});
		};
		setHeightChecked();
		win.off('resize.setHeightChecked').on('resize.setHeightChecked', function(){
			clearTimeout(timerSetHeight);
			timerSetHeight = setTimeout(function(){
				setHeightChecked();
			}, 200);
		});
	}*/

	var tableDefault = $('.popup--checkin-cancel-all .table-default');
	tableDefault.each(function(){
		$(this).find(':checkbox').each(function(colIndex){
			$(this).off('change.checkProcedure').on('change.checkProcedure',function(){
				var checkState = $(this).prop('checked');
				checkboxBackwardEffect(tableDefault,colIndex,checkState);
			});
		});
	});

	var checkboxBackwardEffect = function(currentArea,currentCol,currentState){
		currentArea.nextAll('div.table-default').each(function(){
			$(this).find(':checkbox').not('[disabled]').each(function(index){
				if(currentCol === index){
					if(currentState){
						$(this).prop('checked', true);
					}
					else{
						$(this).prop('checked', false);
					}
					$(this).trigger('change.checkAllList');
				}
			});
		});
	};
	// $('[data-printed-boarding-pass="true"]').find('a.cancel-flight').addClass('disabled');

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		var doValidate = function(els){
	// 			var pass = true;
	// 			els.each(function(){
	// 				if(!pass){
	// 					return;
	// 				}
	// 				pass = $(this).valid();
	// 				// fix for checkin- sms
	// 				if(els.closest('[data-validate-col]').length && pass){
	// 					els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
	// 				}
	// 			});
	// 		};
	// 		// self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 		// 	formGroup.each(function(){
	// 		// 		if($(this).data('change')){
	// 		// 			doValidate($(this).find('select, input'));
	// 		// 		}
	// 		// 	});
	// 		// });

	// 		// self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
	// 		// 	formGroup.not(self).each(function(){
	// 		// 		if($(this).data('change')){
	// 		// 			doValidate($(this).find('select, input'));
	// 		// 		}
	// 		// 	});
	// 		// }).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 		// 	self.data('change', true);
	// 		// });

	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
	// 			self.data('change', true);
	// 			$(this).valid();
	// 		}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
	// 			if($(this).val()){
	// 				self.data('change', true);
	// 			}
	// 			else{
	// 				// fix for checkin- sms
	// 				if($(this).closest('[data-validate-col]').length){
	// 					doValidate($(this).closest('[data-validate-row]').find('input'));
	// 				}
	// 			}
	// 		});
	// 	});
	// };

	// Validate
	var initValidateBoarding = function() {
		boardingForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function(){
				return false;
			}
		});
	};

	initValidateBoarding();

	// var initValidateSMS = function() {
	// 	formSMS.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess,
	// 		invalidHandler: function(form, validator) {
	// 			if(validator.numberOfInvalids()) {
	// 				var scrollTo = 0;
	// 				scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').position().top - 10;
	// 				popupSMS.scrollTop(scrollTo);
	// 			}
	// 		},
	// 		submitHandler: function(){
	// 			isReSMS = true;
	// 			popupSMSContent.eq(0).addClass('hidden');
	// 			popupSMSContent.eq(1).removeClass('hidden');
	// 			popupSMS.Popup('reset');
	// 			popupSMS.Popup('reposition');
	// 			flyingFocus = $('#flying-focus');
	// 			if(flyingFocus.length){
	// 				flyingFocus.remove();
	// 			}
	// 			return false;
	// 		}
	// 	});
	// 	validateFormGroup(formSMS.find('.table-row'));
	// };

	// initValidateSMS();

	// var initValidateSendSMS = function() {
	// 	sendFormSMS.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess
	// 	});
	// 	// validateFormGroup(sendFormSMS.find('.table-row'));
	// };

	// initValidateSendSMS();

	// var initValidateEmail = function() {
	// 	emailForm.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess,
	// 		submitHandler: function() {
	// 			popupEmailAddressContent.eq(0).addClass('hidden');
	// 			popupEmailAddressContent.eq(1).removeClass('hidden');
	// 			popupEmailAddress.Popup('reset');
	// 			popupEmailAddress.Popup('reposition');
	// 			flyingFocus = $('#flying-focus');
	// 			if(flyingFocus.length){
	// 				flyingFocus.remove();
	// 			}
	// 			// setTimeout(function(){
	// 			// }, 100);
	// 			return false;
	// 		},
	// 		invalidHandler: function(form, validator) {
	// 			if(validator.numberOfInvalids()) {
	// 				var divScroll = emailForm.closest('.popup'),
	// 						scrollTo = 0;
	// 				divScroll.scrollTop(scrollTo);
	// 				scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top - 20;
	// 				divScroll.scrollTop(scrollTo);
	// 			}
	// 		}
	// 	});
	// };

	// initValidateEmail();

	// Get data from JSON append to Flight info.
	var getFlightInfo = function() {
		var bookingInfoGroup = $('.booking-info-group');
		bookingInfoGroup.find('.flights--detail span').off('click.showInfo').on('click.showInfo', function() {
			var self = $(this);
			if(self.next('.details').is(':not(:visible)')) {
				self.children('em').addClass('hidden');
				self.children('.loading').removeClass('hidden');

				$.ajax({
					url: config.url.flightSearchFareFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						self.toggleClass('active');
						var html = '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						for(var ft in res.flyingTimes) {
							html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
						}
						self.next('.details').html(html).hide().removeClass('hidden').stop().slideToggle(400);
					},
					error: function(jqXHR, textStatus) {
						// console.log(jqXHR);
						if(textStatus === 'timeout') {
							window.alert(L10n.flightSelect.timeoutGettingData);
						}
						else {
							window.alert(L10n.flightSelect.errorGettingData);
						}
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				self.toggleClass('active');
				self.next('.details').stop().slideToggle(400);
			}
		});
	};

	getFlightInfo();

	var emailAndSMS = function(){
		var triggerEmailSMS = $('[data-trigger-email-sms]');
		var sendCheckinConfirmationPopup = $('.popup--send-checkin-confirmation');
		var sendCheckinForm = $('#send-checkin-confirmation');
		var popupEmailConfirm = sendCheckinConfirmationPopup.find('.popup--email-confirm');
		var PopupSuccessfullySent = sendCheckinConfirmationPopup.find('.popup--successfully-sent');
		var emailSMS = sendCheckinConfirmationPopup.find('.popup--email-sms');
		var tabEmailSMS = sendCheckinConfirmationPopup.find('.tabs--1');
		var addEmailAddress = sendCheckinConfirmationPopup.find('#email-address-submit-3');
		var formEmailConfirm = sendCheckinConfirmationPopup.find('.form--email-confirm');
		var formPhoneNumber = sendCheckinConfirmationPopup.find('.form--phone-number');

		var setValueEmailConfirmPopup = function(){
			var arr = [];
			var renderData = function(){
				sendCheckinForm.find('.form--email-address input').each(function() {
					var self = $(this);
					if (self.val()){
						arr.push(self.val());
					}
				});
			};

			renderData();

			formEmailConfirm.find('.table-default').empty();

			for(var i = 0; i< arr.length; i++){
				$(config.template.addEmailConfirm.format(i + 1, arr[i])).appendTo(formEmailConfirm.find('.table-default'));
			}

		};

		// set value SMS success sent Popup
		var setValueSMSSuccessfullySentPopup = function(){
			var arr = [];
			var countryVal, areaVal, phoneVal;
			var renderData = function(){
				formPhoneNumber.find('.table-row').each(function(idx, el){
					countryVal = $(el).find('[data-country]').val();
					areaVal = $(el).find('[data-area]').val();
					phoneVal = $(el).find('[data-phone]').val();

					if(countryVal){
						arr.push({
							country: countryVal,
							area: areaVal,
							phone: phoneVal
						});
					}
				});
			};

			renderData();

			PopupSuccessfullySent.find('.table-default').empty();

			for(var i = 0; i < arr.length; i++){
				$(config.template.AddSMSSuccessfullySent.format(i + 1, arr[i].country, arr[i].area, arr[i].phone)).appendTo(PopupSuccessfullySent.find('.table-default'));

			}
		};

		sendCheckinConfirmationPopup.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, .btn-back-booking',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			afterHide: function(){
				PopupSuccessfullySent.addClass('hidden');
				popupEmailConfirm.addClass('hidden');
				emailSMS.removeClass('hidden');
			},
			closeViaOverlay: false
		});

		// Validate form group
		var validateFormGroup = function(formGroup){
			formGroup.each(function(){
				var self = $(this);
				var doValidate = function(els){
					var pass = true;
					els.each(function(){
						if(!pass){
							return;
						}
						pass = $(this).valid();
						// fix for checkin- sms
						if(els.closest('[data-validate-col]').length && pass){
							els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
						}
					});
				};
				self.off('click.triggerValidate').on('click.triggerValidate', function(){
					formGroup.each(function(){
						if($(this).data('change')){
							doValidate($(this).find('input'));
						}
					});
				});

				self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
					formGroup.not(self).each(function(){
						if($(this).data('change')){
							doValidate($(this).find('input'));
						}
					});
				}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
					self.data('change', true);
				});

				self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
					self.data('change', true);
					$(this).valid();
				}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
					if($(this).val()){
						self.data('change', true);
					}
					else{
						// fix for checkin- sms
						if($(this).closest('[data-validate-col]').length){
							doValidate($(this).closest('[data-validate-row]').find('input'));
						}
					}
				});
			});
		};

		// Validate email and SMS
		var initValidateEmailAndSMS = function(){
			sendCheckinForm.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess,
				submitHandler: function() {
					var currentTab = sendCheckinForm.find('.tabs--1').find('.tab-item.active');

					SIA.preloader.show();
					emailSMS.addClass('hidden');
					if(currentTab.index()){
						setValueSMSSuccessfullySentPopup();
						PopupSuccessfullySent.removeClass('hidden');
					}else{
						setValueEmailConfirmPopup();
						popupEmailConfirm.removeClass('hidden');
					}
					setTimeout(function(){
						sendCheckinConfirmationPopup.Popup('reset');
						sendCheckinConfirmationPopup.Popup('reposition');
						SIA.preloader.hide();
					}, 100);

					return false;
				},
				invalidHandler: function(form, validator) {
					if(validator.numberOfInvalids()) {
						var divScroll = sendCheckinForm.find('[data-scroll-content]'),
								scrollTo = 0;
						divScroll.scrollTop(scrollTo);
						scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top;
						divScroll.scrollTop(scrollTo);
					}
				}
			});
			validateFormGroup(sendCheckinForm.find('[data-validate-row]'));
		};

		// start validate after blur for validateAtLeastOne
		var setRuleForValidate = function(){
			if(formPhoneNumber.find('[data-rule-validateatleastone]').length){
				formPhoneNumber.find('[data-validate-row]').find('input').on('blur.blurValidate', function(){
					var that = $(this);
					var inputAtLeastOne = that.closest('[data-validate-row]').find('input');
					inputAtLeastOne.not(that).valid();
				});
			}
		};

		setRuleForValidate();
		// end validate after blur for validateAtLeastOne

		initValidateEmailAndSMS();

		triggerEmailSMS.off('click.showEmailSMSPopup').on('click.showEmailSMSPopup', function(e){
			e.preventDefault();
			sendCheckinConfirmationPopup.Popup('show');
		});

		// tab afterChange event
		tabEmailSMS.off('afterChange.handleBtnES').on('afterChange.handleBtnES', function(){
			var self = $(this);
			var form = self.closest('form');
			var currentTab = form.find('.tabs--1').find('.tab-item.active');

			if(currentTab.index()){
				form.find('#email-address-submit-3').addClass('hidden');
			}else{
				form.find('#email-address-submit-3').removeClass('hidden');
			}
		});

		addEmailAddress.off('click.addEmailAddress').on('click.addEmailAddress', function(e){
			e.preventDefault();
			// var self = $(this);
			var newRow = $(config.template.addEmail.format(($(this).closest('form').find('.form--email-address .table-row').length + 1), L10n.validation.email)).appendTo($(this).closest('form').find('.form--email-address .table-row').parent());
			if(!window.Modernizr.input.placeholder){
				newRow.find('input').placeholder();
				newRow.find('input').addClear();
			}
		});
	};

	emailAndSMS();
	var parentDataMore, parentDataLess, serachLink,
		win = $(window), valueTopBlockHeading;

	var appendBlockTpl = $('[data-booking-summary-panel]');
	var appendBreakdownDetailTpl = $('.popup--flights-details').find('.popup__content');

	var mpAddOnsDetail = function(data1, dataDepart, dataReturn, priceData){
		if(!$('body').hasClass('fs-economy-page') && !$('body').hasClass('sk-ut-workflow')){
	    $.get(global.config.url.bookingSummaryPanelTpl, function (data) {
	      var template = window._.template(data, {
	        data: data1
	      });
	      var templateEl = $(template);
	      if(!$('.bsp-booking-summary').find('.bsp-booking-summary__content').length) {
	      	appendBlockTpl.append(templateEl);

            var str = $('[data-refont-cash]').text();
            var tmp = str.slice(-2);
            var res = str.replace(/[09][0-9]$/gm, '<p>'+tmp+'</p>');
            $('[data-refont-cash]').html(res);
            $('[data-replace-miles]').text(function () {
              return $(this).text().replace(".", ",");
            });

	      }
          if($('.bsp-booking-summary').length){
          	valueTopBlockHeading = $('.bsp-booking-summary').offset().top;
	      	}
          var heightBspBooking = $('.bsp-booking-summary').find('.bsp-booking-summary__content');
          heightBspBooking.css('max-height', '200px');
	    });
		} else {
			$.get(global.config.url.fsBookingSummaryPanelTplMultiStop, function (data) {
	      var template = window._.template(data, {
	        data: data1,
	        dataDepart: dataDepart,
	        dataReturn: dataReturn,
	        priceData: priceData
	      });
	      var templateEl = $(template);
	      appendBlockTpl.children().not('.bsp-animate').remove();
	      if(!$('.bsp-booking-summary').find('.bsp-booking-summary__content').length) {
	      	appendBlockTpl.prepend(templateEl)
	      }
          if($('.bsp-booking-summary').length){
          	valueTopBlockHeading = $('.bsp-booking-summary').offset().top;
	      	}
          var heightBspBooking = $('.bsp-booking-summary').find('.bsp-booking-summary__content');
          heightBspBooking.css('max-height', '200px');

          var checkTravelParty = $('.number-passengers').data('travel-party');

          if(checkTravelParty) {
            var str = '<div class="travel-party"><div class="travel-thumb"><span class="ico ico-preferred-group"></span></div><div class="travel-content"><div class="title">Your travel party enjoy complimentary advance seat selection, as you\'re travelling with a child</div></div></div>'

            $('.travel-party').remove();
            $('.main-inner').prepend($(str));
          }

	    });

	    $(document).off('click.scrollToInbound').on('click.scrollToInbound', 'input[name="select-inbound"]', function(){
	     setTimeout(function(){
	      var inboundTableTop = $('.recommended-flight-block:eq(1)').offset().top;
	      $(window).scrollTop(inboundTableTop - 400);
	     }, 500);
	    })

	    $(document).off('click.scrollToOutbound').on('click.scrollToOutbound', 'input[name="select-outbound"]', function(){
	     setTimeout(function(){
	      var outboundTableTop = $('.recommended-flight-block:eq(0)').offset().top;
	      $(window).scrollTop(outboundTableTop - 300);
	     }, 500);
	    })

		}
	};
  var urlBspPanel;
  if ($('body').hasClass('mp-add-ons-six-city-page')) {
    urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-six-city.json';
  } else if ($('body').hasClass('mp-payment-bundle-pax')) {
    urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-bundle-pax.json';
  } else if ($('body').hasClass('mp-payment-insurance')) {
    urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-insurance.json';
  } else if ($('body').hasClass('mp-1-add-ons-page')) {
    urlBspPanel = config.url.bookingSummaryPanelAddOnsJson;
  } else if ($('body').hasClass('sk-ut-flight-search-a') || $('body').hasClass('sk-ut-passenger-detail-a') || $('body').hasClass('sk-ut-seatmap-a') || $('body').hasClass('sk-ut-payments-a')) {
    urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-a.json';
  } else if ($('body').hasClass('sk-ut-flight-search-b') || $('body').hasClass('sk-ut-passenger-detail-b') || $('body').hasClass('sk-ut-seatmap-b') || $('body').hasClass('sk-ut-payments-b')) {
    urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-b.json';
  } else if ($('body').hasClass('review-refund-page')) {
		urlBspPanel = 'ajax/mb-review-atc-refund.json';
	} else if ($('body').hasClass('review-atc-pay') || $('body').hasClass('atc-payment-page')) {
		urlBspPanel = 'ajax/mb-review-atc-pay.json';
	} else if ($('body').hasClass('fs-railway') && $('body').hasClass('fs-economy-page')) {
		urlBspPanel = 'ajax/fs-sk-railway-multistop.json';
	} else {
    urlBspPanel = config.url.bookingSummaryPanelJson;
  }

  var deleteCookie = function(name) {
      document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    var saveDataCookie = function() {
      $(document).off('click.saveDataCookie').on('click.saveDataCookie', 'input[name="btn-next"]', function(e){
        e.preventDefault();
        var d = new Date();
            d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            deleteCookie('bspData');
            document.cookie = 'bspData' + "=" + JSON.stringify(cookieData) + ";" + expires + ";path=/";
        $(this).closest('form').trigger('submit');
      })
    }

    saveDataCookie();

    var getCookie = function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }


	var renderBSP = function(dataDepart, dataReturn) {
	    $.ajax({
	      beforeSend: function(){
	        $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
	      },
	      url: urlBspPanel,
	      type: SIA.global.config.ajaxMethod,
	      dataType: 'json',
	      async: true,
	      cache: false,
	      xhr: function () {
	        var xhr = $.ajaxSettings.xhr();
	        xhr.onprogress = function (e) {
	            if (e.lengthComputable) {
	              $('[data-booking-summary-panel]').find('.bsp-animate').css({
	                width: (e.loaded/e.total) * 100 + '%'
	              });
	            }
	        };
	        xhr.upload.onprogress = function (e) {
	            if (e.lengthComputable) {
	              $('[data-booking-summary-panel]').find('.bsp-animate').css({
	                width: (e.loaded/e.total) * 100 + '%'
	              });
	            }
	        };
	        return xhr;
	      },
	      success: function(reponse) {
	        var data1 = reponse;
	        var data2 = reponse.bookingSummary;

	        var cookieBSP;
	        var priceData = {};

	        if(getCookie('bspData') !== '') {
	          cookieBSP = $.parseJSON(getCookie('bspData'));
	        }

	        if(getCookie('priceData') !== '') {
        		priceData = $.parseJSON(getCookie('priceData'));
      		}

	        if(cookieBSP && $('body').hasClass('sk-ut-workflow') && !$('body').hasClass('fs-economy')) {
	          dataDepart = cookieBSP.depart;
	          dataReturn = cookieBSP.return;
	        }

	        (dataDepart || dataReturn) ? mpAddOnsDetail(data2, dataDepart, dataReturn, priceData) : mpAddOnsDetail(data2);
	        mpCostBreakdownDetail(data1);
	        getContentFlight();
	      },
	      complete: function(){
	        $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
	        setTimeout(function(){
	          $('[data-booking-summary-panel]').find('.bsp-animate').css({
	            width: 0
	          });
	        }, 100)
	      }
	    });
	}

	$('body').hasClass('economy-seatmap-page') && deleteCookie('priceData');
	renderBSP();

	var mpCostBreakdownDetail = function(data1){
	  var appendBreakdownDetailTpl = $('.popup--flights-details').find('.popup__content');

	    $.get(global.config.url.mpCostBreakdownTpl, function (data) {
	      var template = window._.template(data, {
	        data: data1
	      });
	      var templateEl = $(template);
	      appendBreakdownDetailTpl.append(templateEl);
	    });
	};

	var getContentFlight = function() {

	  $(document).off('click.getFlightContent').on('click.getFlightContent', '.btn-price', function(){
	    // prototype
		if($('body').hasClass('sk-ut-flight-search-a')) {
		  if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Super Saver' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		    return false;
		  } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Lite' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		    return false;
		  }
		}

		if($('body').hasClass('sk-ut-flight-search-b')) {
		  if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Standard' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 3)) {
		    return false;
		  } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Flexi' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
		    return false;
		  }
		}
		//
	    var colIndex = $(this).closest('.select-fare-block').data('col-index'),
	    	btnPriceHiddenRecommend = $('[data-col-index="'+ colIndex +'"]').eq(0).find('[data-table-mobile] [data-header-class="'+ $(this).data('header-class') +'"]'),
	    	rowFlight = btnPriceHiddenRecommend.closest('.flight-list-item'),
	        flightIdx = btnPriceHiddenRecommend.closest('.recommended-flight-block').data('returntrip'),
	        listLeg = rowFlight.find('.recommended-table [data-wrap-flight] .flight-result-leg'),
	        price = btnPriceHiddenRecommend.is('[data-price-segment-after]') ? parseFloat($(this).data('price-segment-after')) : parseFloat($(this).data('price-segment')),
	        arr = [],
	        headerClass = $(this).data('header-class'),
	        totalTime,
	        flightData = {};

	    totalTime = rowFlight.data('timetotal');
	    listLeg.each(function(){
	      var self = $(this),
	        flightStation = self.find('.flight-station--inner');
	      flightStation.each(function(){
	        var obj = {};

	        obj['originHour'] = $(this).find('[data-origin-hour]').data('origin-hour');
	        obj['originCountryname'] = $(this).find('[data-origin-countryname]').data('origin-countryname');
	        obj['originDate'] = $(this).find('[data-origin-date]').data('origin-date');
	        obj['originAirportname'] = $(this).find('[data-origin-airportname]').data('origin-airportname');
	        obj['originterminal'] = $(this).find('[data-origin-terminal]').data('origin-terminal');
	        obj['destinationHour'] = $(this).find('[data-destination-hour]').data('destination-hour');
	        obj['destinationCountryname'] = $(this).find('[data-destination-countryname]').data('destination-countryname');
	        obj['destinationDate'] = $(this).find('[data-destination-date]').data('destination-date');
	        obj['destinationAirportname'] = $(this).find('[data-destination-airportname]').data('destination-airportname');
	        obj['destinationTerminal'] = $(this).find('[data-destination-terminal]').data('destination-terminal');
	        obj['layover'] = $(this).next().data('layovertime');
	        obj['timeFlight'] = $(this).find('[data-timeflight]').data('timeflight');
	        obj['operationName'] = $(this).find('[data-operationname]').data('operationname');
	        obj['flightNumber'] = $(this).find('[data-flightnumber]').data('flightnumber');
	        obj['planeName'] = $(this).find('[data-planename]').data('planename');
	        obj['headerClass'] = headerClass;

	        arr.push(obj);
	      })
	    })

	    flightData['totalTime'] = totalTime;
	    flightData['data'] = arr;
	    flightData['flightidx'] = flightIdx;
	    flightData['price'] = price;

	    if(flightIdx === 0) {
	    	if($('body').hasClass('sk-ut-flight-search-a')) {
	    		flightData['fare'] = 1290;
	    	}
	    	if($('body').hasClass('sk-ut-flight-search-b')) {
	    		flightData['fare'] = 3487.4;
	    	}
	    	dataDepart = flightData;
	    	cookieData['depart'] = flightData;
	    } else {
	    	if($('body').hasClass('sk-ut-flight-search-a')) {
	    		flightData['fare'] = 1485;
	    	}
	    	if($('body').hasClass('sk-ut-flight-search-b')) {
	    		flightData['fare'] = 4187.4;
	    		flightData['tax'] = 836.6;
	    	}
	    	dataReturn = flightData;
	    	cookieData['return'] = flightData;
	    }

	    $(document).off('click.upgradeBSP').on('click.upgradeBSP', 'input[name="btn-upgrade"]', function(){
	    	btnPriceHiddenRecommend.closest('.table-economy-green').next().find('.btn-price').trigger('click.getFlightContent', true);
	    })
	  })

	  $(document).off('click.renderBSP').on('click.renderBSP', 'input[name="proceed-fare"], input[name="proceed-fare-1"]', function(){
	  	var isOutbound = parseInt($(this).closest('[data-col-index]').data('col-index').split('-')[0]) === 0;

	  	isOutbound && $('[data-returntrip="1"]').find('.change-flight-item').not('.hidden').find('input[name="change-button"]').trigger('click');

	  	renderBSP(dataDepart, dataReturn);
	  })

	  $(document).off('click.resetBSP').on('click.resetBSP', 'input[name="change-button"]', function(){
	    var flightIdx = $(this).closest('.recommended-flight-block').data('returntrip');

	    if(flightIdx === 0) {
        dataDepart = {};
        dataReturn = {};
      } else {
        dataReturn = {};
      }

	    renderBSP(dataDepart, dataReturn);
	  })
	}

	var clickMoreDetails = function(){
		parentDataMore.find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap').addClass('flight-result-leg-wrap-1');
		parentDataMore.find('.bsp-total-fare').next().addClass('hidden');
    	parentDataMore.closest('.bsp-booking-summary__content').css('max-height', 'none');
		parentDataMore.find('.bsp-booking-summary__content-detail').removeClass('hidden');
		parentDataMore.find('.bsp-booking-summary__content-detail').css('display', 'block');
		parentDataMore.find('.bsp-total-fare').addClass('expand-bsp');
		parentDataMore.find('.bsp-booking-summary__content-detail').find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap-1').addClass('flight-result-leg-wrap');
	};
	var clickLessDetails = function(){
		parentDataLess.find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap').addClass('flight-result-leg-wrap-1');
		parentDataMore.find('.bsp-total-fare').next().next().addClass('hidden');
   	parentDataMore.closest('.bsp-booking-summary__content').css('max-height', '200px');
		parentDataMore.find('.bsp-total-fare').next().removeClass('hidden');
		parentDataMore.find('.bsp-total-fare').removeClass('expand-bsp');
		parentDataMore.find('.bsp-total-fare').next().find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap-1').addClass('flight-result-leg-wrap');
		var valueTopBlockHeading = $('.bsp-booking-summary').offset().top;
		$("html, body").animate({ scrollTop: valueTopBlockHeading }, 450);
	};
	$(document).on('click','.more-detail[data-more-details]', function(e){
		e.preventDefault();
		parentDataMore = $(this).closest('.bsp-booking-summary__content-control');
		clickMoreDetails();
	});
	$(document).on('click','.less-detail[data-less-details]', function(e){
		e.preventDefault();
		parentDataLess = $(this).closest('.bsp-booking-summary__content-control');
		clickLessDetails();
	});
	$(document).on('click','.plus-more-detail > .link-4', function(e){
		e.preventDefault();
		parentDataMore = $(this).closest('.bsp-booking-summary__content-control');
		clickMoreDetails();
		setOverflow();
	});
	var popup1 = $('.popup--flights-details'),
    popup2 = $('.popup--add-ons-baggage'),
    popup3 = $('.popup--add-ons-summary');
    popup4 = $('.popup-booking-widget-booking-summary');
    popup5 = $('.popup--flight-addon-term-condition');
    popup6 = $('.flight-search-summary-conditions');
  $(document).on('click', '.trigger-popup-flights-details', function(e) {
  	e.preventDefault();
  	//prototype
		if($('body').hasClass('sk-ut-workflow')) {
			return false;
		}
		//
    popup1.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.trigger-popup-add-ons-baggage', function(e) {
  	e.preventDefault();
  	//prototype
		if($('body').hasClass('sk-ut-workflow')) {
			return false;
		}
		//
    if(!$('body').hasClass('fs-economy')){
      popup3.Popup('show');
    } else {
    	$('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
      popup6.Popup('show');
    }
    $('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.trigger-popup-add-ons-summary', function(e) {
  	e.preventDefault();
  	//prototype
		if($('body').hasClass('sk-ut-workflow')) {
			return false;
		}
		//
    popup2.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.trigger-popup-edit-search', function(e) {
  	e.preventDefault();
  	$('[data-stickywidget]').remove();
	  $('.popup-booking-widget-booking-summary').find('.booking-widget-booking-summary').css('display', 'block');
  	$('.popup-booking-widget-booking-summary').find('.booking-widget-block').removeClass('hidden');
    popup4.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.popup__close, .popup__close_2', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    popup3.Popup('hide');
    popup4.Popup('hide');
    popup5.Popup('hide');
    event.preventDefault();
  });
  // win.off('scroll').on('scroll', function(){
  // 	var blockHeading = $('.bsp-booking-summary');
  //     if($(this).scrollTop() >= valueTopBlockHeading) {
  //        	blockHeading.css({
		//    'top' : '0',
		//    'position' : 'fixed',
		//    'right' : '0',
		//    'left' : '0',
		//    'z-index' : '10'
		// });
  //     } else {
  //       blockHeading.css('position', 'static');
  //     }

  //     var elPos = $('.bsp-booking-summary__heading').offset().top;
  //     $('[data-stickywidget]').css('top', elPos - 4);
  // });

  var onWaySearchFlight = $('.custom-radio--1').find('#rdo-search-edit-return-2');
  var returnSearchFlight = $('.custom-radio--1').find('#rdo-search-edit-return-1');
  onWaySearchFlight.off('change').on('change', function(){
  	$('#select-edit-date-3').closest('.one-four').removeClass('hidden');
  	$('.form-group').find('[data-return-flight]').addClass('hidden');
  });
  returnSearchFlight.off('change').on('change', function(){
  	$('#select-edit-date-3').closest('.one-four').addClass('hidden');
  	$('.form-group').find('[data-return-flight]').removeClass('hidden');
  });

    var triggerThis, timer,
		nextThis,	prevThis,
		blockContentFlight, parentThisTrigger,
		blockCheckSeleted, blockWaperContent,
		blockCheckSeletedItemSq,
		lengthSeletedItemSq, termsConditionsSelf,
		buttonSelect = $('[data-selected-button]'),
		triggerlinkAccordion = $('.addons-landing-content').find('a[data-accordion-trigger]');

	var triggerButtonSelect = function(){
		blockContentFlight = triggerThis.closest('.your-flight-item');
		blockCheckSeleted = blockContentFlight.find('[data-select-item]');
		triggerThis.addClass('hidden');
	  	triggerThis.attr('data-selected-button', false).removeClass('selected-button');
			nextThis.attr('data-selected-button', true).addClass('selected-button');
			triggerThis.addClass('hidden');
	  	nextThis.removeClass('hidden');
	  	prevThis.removeClass('hidden');
	  	if(blockContentFlight.find('[data-selected-button]').hasClass('selected-button')){
	  	  blockCheckSeleted.eq(0).attr('data-select-item', false).addClass('hidden');
	  	  blockCheckSeleted.eq(0).next().attr('data-select-item', true).removeClass('hidden');
	  	  $('.btn-seat-selection').removeClass('disabled');
	  	}else{
	  	  blockCheckSeleted.eq(0).attr('data-select-item', false).removeClass('hidden');
	  	  blockCheckSeleted.eq(0).next().attr('data-select-item', true).addClass('hidden');
	  	  $('.btn-seat-selection').addClass('disabled');
	  	}
		blockCheckSeletedItemSq = triggerThis.closest('.addons-landing-content');
		lengthSeletedItemSq = blockCheckSeletedItemSq.find('.selected-button').not('.hidden');
		var text = lengthSeletedItemSq.length > 1 ? lengthSeletedItemSq.length + ' Bundles selected' : lengthSeletedItemSq.length + ' Bundle selected';
		if(triggerThis.closest('[data-alarcate]').length) {
		  text = lengthSeletedItemSq.length > 1 ? lengthSeletedItemSq.length + ' Preferred Seats selected' : lengthSeletedItemSq.length + ' Preferred Seat selected';
		}
		if(lengthSeletedItemSq.length > 0){
		  blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(text);
		}else if(lengthSeletedItemSq.length < 1){
		  blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(text);
		}
		if(nextThis.hasClass('selected-button')){
		  triggerThis.closest('.weight-flight-item').next('.preferred-flight--info').removeClass('hidden');
		}else{
		  triggerThis.closest('.weight-flight-item').next('.preferred-flight--info').addClass('hidden');
		}
		var blockDataPss = triggerThis.closest('.data-pss-xbag');
		if(blockDataPss){
			var blockDataPssItem = blockDataPss.find('[data-selected-button]');
       	blockDataPssItem.removeClass('hidden').removeClass('selected-button-1');
       	blockDataPssItem.next().addClass('hidden').removeClass('selected-button-1');
       	triggerThis.addClass('hidden').removeClass('selected-button');
       	triggerThis.next().removeClass('hidden').addClass('selected-button-1');
       	var lengthSeletedItemSqPss = blockCheckSeletedItemSq.find('.selected-button-1').not('.hidden');
       	blockCheckSeletedItemSq.find('a > .bundle-selected > span').text('');
       	var text = lengthSeletedItemSqPss.length > 1 ? lengthSeletedItemSqPss.length + ' Bundles selected' : lengthSeletedItemSqPss.length + ' Bundle selected';
       	if(triggerThis.closest('[data-alarcate]').length) {
       	  text = lengthSeletedItemSqPss.length > 1 ? lengthSeletedItemSqPss.length + ' Preferred Seats selected' : lengthSeletedItemSqPss.length + ' Preferred Seat selected';
       	}
       	lengthSeletedItemSqPss.length && blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(text);
       	if(lengthSeletedItemSqPss.length > 0){
       	  blockCheckSeletedItemSq.find('a > .bundle-selected').addClass('hidden');
       	}
       }
  };

	buttonSelect.off('click').on('click', function(){
		triggerThis = $(this);
		nextThis = triggerThis.next('[data-selected-button]');
		prevThis = triggerThis.prev('[data-selected-button]');
		if(nextThis.length){
			triggerButtonSelect();
		}else if(prevThis.length){
			triggerButtonSelect();
		}
	});

  var linkExpanded = $('[data-select-item]');
  var triggerExpanded = function(){
  	if(parentThisTrigger.hasClass('active')){
			parentThisTrigger.find('.icon-point-rotate').removeClass('hidden');
		}else{
			setTimeout(function(){ parentThisTrigger.find('.icon-point-rotate').addClass('hidden'); }, 300);
		}
		if(blockWaperContent.find('select').attr('data-dropdpown-selected') === 'true'){
			blockWaperContent.find('[data-select-item]').eq(0).addClass('hidden');
			blockWaperContent.find('[data-select-item]').eq(0).next().removeClass('hidden');
			blockWaperContent.find('select').attr('data-dropdpown-selected', false);
		}else if(blockWaperContent.find('select').attr('data-dropdpown-selected') === 'false'){
			blockWaperContent.find('[data-select-item]').eq(0).removeClass('hidden');
			blockWaperContent.find('[data-select-item]').eq(0).next().addClass('hidden');
			blockWaperContent.find('select').attr('data-dropdpown-selected', true);
		}
  };
	linkExpanded.on('click', function(){
		clearTimeout(timer);
		blockWaperContent = $(this).closest('.your-flight-item');
		parentThisTrigger = $(this).parent();
		timer = setTimeout(function(){
  		triggerExpanded();
  	}, 300);
	});
  triggerlinkAccordion.on('click', function(){
  	var findItemSelected = $(this).closest('.addons-landing-content').find('.selected-button');
  	if($(this).hasClass('active')){
  		if(findItemSelected.length > 0){
				$(this).closest('.addons-landing-content').find('a > .bundle-selected').removeClass('hidden');
			}else if(findItemSelected.length < 1){
				$(this).closest('.addons-landing-content').find('a > .bundle-selected').addClass('hidden');
			}
  	}else{
			$(this).closest('.addons-landing-content').find('a > .bundle-selected').addClass('hidden');
  	}
  });

  var changeTotalfare = $('[data-total-fare]');
  changeTotalfare.off('click').on('click', function(){
  	var valuetotalFare = $(this).closest('.select-price').find('.sgd-price').text();
  	$('[data-booking-summary-panel]').addClass('bsp-loading');
  	$('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
  	setTimeout(function(){ $('[data-booking-summary-panel]').addClass('bsp-loaded'); }, 100);
  	$('.total-fare--inner').find('[data-more-details]').css('opacity', '0');
  	triggerThis = $(this);
		nextThis = triggerThis.next('[data-selected-button]');
		prevThis = triggerThis.prev('[data-selected-button]');
		if(nextThis.length){
			triggerButtonSelect();
		}else if(prevThis.length){
			triggerButtonSelect();
		}
  	setTimeout(function(){
  		$('.bsp-booking-summary__content-control').find('.total-cost').find('.unit').text(valuetotalFare);
  		$('[data-booking-summary-panel]').removeClass('bsp-loaded bsp-loading');
  		$('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
  		$('.total-fare--inner').find('[data-more-details]').css('opacity', '1');
  		var selfBookingSummaryInfo = $('.total-fare--inner').find('.flights__info');
  		selfBookingSummaryInfo.find('.says').remove();
			selfBookingSummaryInfo.prepend('<span class="says">Booking summary, Selected Fare:</span>');
			$('#booking-summary__info-copy').html('');
			$('#booking-summary__info-copy').attr('tabindex', '0');
			$('#booking-summary__info-copy').delay(300).html( selfBookingSummaryInfo.text().replace('Booking summary, ', ''));
  	}, 2100);
  });


  var clickedPreferred = function(){
  	var checkClickedPreferred = $('.check-preferred').find('[data-selected-button]');
  	checkClickedPreferred.each(function() {
    if($(this).hasClass('selected-button')){
		  	$(this).trigger('dblclick');
		  	popup5.Popup('show');
		    $('body').find('.fadeInOverlay').addClass('overlay');
		    var termsConditionsFlight = $('.terms-conditions-flight').find('#accept-condition');
		    termsConditionsFlight.removeAttr('checked');
		    termsConditionsSelf = termsConditionsFlight;
		    checkInputChecked();
     	}
 		});
  };

  $('.trigger-popup-flights-addon-term-condition').on('click', function(e) {
  	e.preventDefault();
  	clickedPreferred();
  });

  var checkInputChecked = function(){
  	var buttonGroup = $('.popup--flight-addon-term-condition').find('.button-group-1');
  	if(termsConditionsSelf.is(':checked')){
  		buttonGroup.find('#btn-proceed').attr('disabled', false);
  		buttonGroup.find('#btn-proceed').removeClass('disabled');
  	}else{
  		buttonGroup.find('#btn-proceed').addClass('disabled');
  		buttonGroup.find('#btn-proceed').attr('disabled', true);
  	}
  }

  $('.trigger-popup-flights-addon-term-condition').on('click', function(e) {
  	e.preventDefault();
  	clickedPreferred();
  });

  $('.trigger-popup-flights-addon-term-condition').on('dblclick',function(){
		popup5.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
    var termsConditionsFlight = $('.terms-conditions-flight').find('#accept-condition');
    termsConditionsFlight.removeAttr('checked');
    termsConditionsSelf = termsConditionsFlight;
    checkInputChecked();
  });

  var termsConditionsFlight = $('.terms-conditions-flight').find('#accept-condition');
  termsConditionsFlight.off('click').on('click', function(){
  	termsConditionsSelf = $(this);
  	checkInputChecked();
  });
  if($('body').hasClass('review-refund-page')){
    setTimeout(function(){
      $('[data-replace-miles]').text(function () {
        return $(this).text().replace(".", ",");
      });
    }, 500);
  }
  var renderBookingPayment = function(){
    var templateBookingPayment;
    var appendDiv = $('[data-accordion-append]');

    var mpBookingPayment = function(data1){
        var urlBookingPaymentTpl;
        if ($('body').hasClass('mp-payment-bundle-pax')) {
          urlBookingPaymentTpl = global.config.url.mpBookingPaymentBundlePaxTpl
        } else if ($('body').hasClass('mp-payment-insurance')) {
          urlBookingPaymentTpl = global.config.url.mpBookingPaymentInsuranceTpl
        } else if($('body').is('.mp-1-payments')) {
		  urlBookingPaymentTpl = global.config.url.mp1BookinngPaymentTpl
 		} else {
          urlBookingPaymentTpl = global.config.url.mpBookingPaymentTpl;
        }
        $.get(urlBookingPaymentTpl, function (data) {
          var template = window._.template(data, {
            data: data1
          });
          templateBookingPayment = $(template);
          appendDiv.append(templateBookingPayment);

        	var dataRemoveLastAdd = $('[data-remove-last-add]');
	        dataRemoveLastAdd.each(function() {
	         	var thisText = $(this).text();
	         	thisText = thisText.slice(0, thisText.lastIndexOf('+'));
	        	$(this).text(thisText);
	     		});

          var dataRemoveLastComma = $('[data-remove-last-comma]');
          dataRemoveLastComma.each(function() {
            var thisText = $(this).text();
            thisText = thisText.slice(0, thisText.lastIndexOf(','));
            $(this).text(thisText);
          });
          SIA.accordion();
          if($('[data-tooltip]')) {
            $('[data-tooltip]').kTooltip();
          }
        });
      };
    $.ajax({
      url: urlBspPanel,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data1 = reponse.bookingSummary;
        mpBookingPayment(data1);
      }
    });
  };
  if($('body').hasClass('mp-payments')){
    renderBookingPayment();
  }
  if($('body').hasClass('mp-add-ons-page') || $('body').hasClass('add-bundle-booking') || $('body').hasClass('mp-1-add-insurance') ){
  	var triggerPopup = $('[data-trigger-mobile-popup] input');
	  triggerPopup.closest('[data-trigger-mobile-popup]').removeAttr('data-accordion-trigger aria-expanded');
	  SIA.global.vars.popupGesture($('.block-show-popup-mobile'), triggerPopup, '.popup__close, .confirm-price-button', '.block-flight-details');
  }

  // Expand accordions for mp-payments
  $('.for-your-flight-block .your-flight-item').each(function() {
		var flightDetailsInner = $(this).find('.block-flight-details--inner');
		$(flightDetailsInner).eq(0).find('[data-accordion-trigger]').addClass('active');
  });

  // Remove 1st accordion
  $('.for-your-trip-block .your-flight-item').find('[data-accordion-content]').removeAttr('data-accordion-content');

  htmlAmenitiesAppend = '<ul class="list-amenities"><li><em class="ico-swim-36"></em><span class="text-icon">Swimming pool</span></li><li><em class="ico-2-lounge"></em><span class="text-icon">Lounge</span></li><li><em class="ico-cup"></em><span class="text-icon">Breakfast</span></li><li><em class="ico-2-hotel"></em><span class="text-icon">24 hour services</span></li><li><em class="ico-info-3"></em><span class="text-icon">Concierge</span></li><li><em class="ico-2-fitness"></em><span class="text-icon">Fitness centre</span></li><li class="spa"><em class="ico-2-spa"></em><span class="text-icon">Spa and wellness</span></li><li><em class="ico-wifi"></em><span class="text-icon">Wifi in public area</span></li><li><em class="ico-assistance"></em><span class="text-icon">Disability support</span></li></ul>';
  var hotelAmenities = $(document).find('.hotel-amenities');
  var listAmenities = hotelAmenities.find('.amenities-content');
  var blockHotelRoomDetails = $(document).find('.hotel-room--details');
  var dataRemoveRoom = blockHotelRoomDetails.find('[data-remove-room]');
  if($('body').hasClass('mp-add-ons-page')) {
  	listAmenities.empty();
  	listAmenities.append(htmlAmenitiesAppend);
  	dataRemoveRoom.attr('value', 'SELECTED').css('background-color', '#00266b');
  }
};
