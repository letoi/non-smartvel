/**
 * @name SIA
 * @description manage booking functions
 * @version 1.0
 */
SIA.excessBaggage = function(){
	var global = SIA.global;
	var upgradeFlightForm = $('#form-choose-flight-upgrade');
	/*var upgradeMile = function() {
		var btnUpgradeMile = $('[data-disable-click]');

		btnUpgradeMile.each(function() {
			var that = $(this),
					tooltipData = that.data('kTooltip');
			if(tooltipData) {
				tooltipData.options.afterShow = function() {
					that.addClass('disabled');
				};
				tooltipData.options.afterClose = function() {
					that.removeClass('disabled');
				};
			}
		});
	};

	upgradeMile();*/

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	var paramUrl = sURLVariables[1];
	var sName = paramUrl ? paramUrl.split('=')[1] : '';
	var getURLParams = function(sParam) {
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1];
			}
		}
	};

	var renderAddOn = function(){
		var fillContent = function(res) {
			var listAddOns = $('.add-ons__list');
			if(listAddOns.length){
				$.get(global.config.url.addOnMbTemplate, function (data) {
					var template = window._.template(data, {
						data: res
					});
					listAddOns.html(template);

					listAddOns
						.off('click.disabledLink')
						.on('click.disabledLink', '[data-disabled-link]', function(e) {
							if ($(this).hasClass('disabled')) {
								e.preventDefault();
								e.stopImmediatePropagation();
							}
						});
				}, 'html');
			}
		};
		if(getURLParams('addon')){
			$.get('ajax/JSONS/' + sName, function(data){
				fillContent(data);
			});
		}
		else{
			fillContent(globalJson.dataAddOn);
		}
	};

	upgradeFlightForm.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: function(label, element){
			global.vars.validateSuccess(label, element);
		}
	});

	var getBaggageMealInfo = function() {
		var bookingDetails = $('[data-pax-id]');

		$.ajax({
			url: globalJson && globalJson.bagMealUrl ? globalJson.bagMealUrl.mealUrl :
				global.config.url.excessMealJSON,
			dataType: 'json',
			success: function(mealJson){
				if (mealJson && mealJson.MealVO &&
					mealJson.MealVO.passengerAndMealAssociationVO &&
					mealJson.MealVO.passengerAndMealAssociationVO.length) {
					renderMealTpl(mealJson.MealVO);
				}
			},
			error: function() {
				window.alert(L10n.manageBooking.mealAjaxErr);
			}
		});

		$.ajax({
			url: globalJson && globalJson.bagMealUrl ? globalJson.bagMealUrl.bagUrl :
				global.config.url.excessBaggageJSON,
			dataType: 'json',
			success: function(bagJson){
				if (bagJson && bagJson.excessBaggage && bagJson.excessBaggage.length) {
					renderBagTpl(bagJson.excessBaggage);
				}
			},
			error: function() {
				window.alert(L10n.manageBooking.bagAjaxErr);
			}
		});

		var renderMealTpl = function(mealJson) {
			if (bookingDetails.length) {
				$.get(global.config.url.bookingDetailMealTemplate, function (mealTpl) {
					bookingDetails.each(function() {
						var self = $(this);
						var paxId = self.data('pax-id');
						var segmentId = self.data('segment-id');
						var isInf = !!self.data('infrant');

						var mealHtml = window._.template(mealTpl, {
							data: getMealInfo(mealJson.passengerAndMealAssociationVO, paxId,
								segmentId, isInf)
						});

						var bookingBlocks = self.find('.booking-details');
						if (bookingBlocks.length) {
							$(mealHtml).insertAfter(bookingBlocks.eq(0));
						}
						else {
							self.prepend(mealHtml);
						}
					});
				}, 'html');
			}
		};

		var getMealInfo = function(paxAndMealVo, paxId, segmentId, isInf) {
			var i = 0;
			for (i = 0; i < paxAndMealVo.length; i++) {
				var meal = paxAndMealVo[i];
				if (meal.passengerId === paxId &&
					(meal.passengerType === 'INF') === isInf &&
					meal.flightDateInformationVO && meal.flightDateInformationVO.length) {
					return {
						mealInfo: getMealDetail(meal.flightDateInformationVO, segmentId),
						paxId: paxId
					};
				}
			}
			return null;
		};

		var getMealDetail = function(flightDateInfo, segmentId) {
			var i = 0;
			for (i = 0; i < flightDateInfo.length; i++) {
				var info = flightDateInfo[i];
				if (info.segmentID === segmentId) {
					return info;
				}
			}
			return null;
		};

		var renderBagTpl = function(bagJson) {
			if (bookingDetails.length) {
				$.get(global.config.url.excessBaggageTemplate, function (bagTpl) {
					bookingDetails.each(function() {
						var self = $(this);
						var paxId = self.data('pax-id');
						var segmentId = self.data('segment-id');
						var isInf = !!self.data('infrant');

						var bagHtml = window._.template(bagTpl, {
							data: getBaggageInfo(bagJson, paxId, segmentId, isInf)
						});

						self.append(bagHtml);
					});
				}, 'html');
			}
		};

		var getBaggageInfo = function(bagJson, paxId, segmentId, isInf) {
			var i = 0;
			for (i = 0; i < bagJson.length; i++) {
				var bag = bagJson[i];
				if (bag.id === paxId && (bag.paxType === 'INF') === isInf) {
					return {
						segmentDetail: getSegmentDetail(bag.segmentDetails, segmentId),
						paxId: paxId
					};
				}
			}
			return null;
		};

		var getSegmentDetail = function(segDetails, segmentId) {
			if (segDetails && segDetails.length) {
				var i = 0;
				for (i = 0; i < segDetails.length; i++) {
					var segment =  segDetails[i];
					if (segment.segmentID === segmentId) {
						return segment;
					}
				}
				return null;
			}
			return null;
		};
	};

	var getFlightInfo = function() {
		$('.flights--detail > span').off('click.get-flight-info').on('click.get-flight-info', function() {
			var self = $(this);
			self.toggleClass('active');
			if(self.siblings('.details').is('.hidden')) {
				$.ajax({
					url: SIA.global.config.url.flightSearchFareFlightInfoJSON,
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('data-date'),
						origin: self.parent().data('origin'),
					},
					type: SIA.global.config.ajaxMethod,
					dataType: 'json',
					beforeSend: function() {
						self.children('em').addClass('hidden');
						self.children('.loading').removeClass('hidden');
					},
					success: function(data) {
						var flyingTime = '';
						for(var ft in data.flyingTimes){
							flyingTime = data.flyingTimes[ft];
						}

						var html = '';
						html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType + '</p>';
						html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime + '</p>';

						// self.children('em').toggleClass('ico-point-d ico-point-u');
						self.siblings('.details').html(html).hide().removeClass('hidden').slideToggle(400);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						window.alert(errorThrown);
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				self.siblings('.details').slideToggle(400, function() {
					$(this).addClass('hidden');
				});
				// self.children('em').toggleClass('ico-point-u ico-point-d');
			}
		});
	};

	var initModule = function() {
		getFlightInfo();
		getBaggageMealInfo();
		renderAddOn();
	};

	initModule();
};
