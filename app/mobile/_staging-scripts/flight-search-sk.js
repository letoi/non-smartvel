/**
 * @name SIA
 * @description Define global flight economy functions
 * @version 1.0
 */

SIA.flightEconomy = function(){
  var global = SIA.global,
      config = global.config,
      body =$('body'),
      container = $('#container'),
      slider = $('[data-fs-slider]'),
      wrapperSlides = slider.find('.slides'),
      btnPrev = slider.find('.btn-prev'),
      btnNext = slider.find('.btn-next'),
      isChooseDate = null,
      priceDateOutbound = 0,
      clickedBtn,
      ajaxFareData,
      miniFareData;
  var slideshowpremiumeconomy = function(){
    if($('body').hasClass('fs-economy')){
      var totalDesktopSlide = 1,
      totalLandscapeSlide = 1;
      $('[data-slideshow-premium-economy]').each(function() {
        var slider = $(this);
        slider.find('img').each(function() {
          var self = $(this),
              newImg = new Image();
          newImg.onload = function() {
            slider.css('visibility', 'visible');
            slider.find('.slides').slick({
              siaCustomisations: true,
              dots: false,
              speed: 300,
              draggable: true,
              slidesToShow: totalDesktopSlide,
              slidesToScroll: totalLandscapeSlide,
              accessibility: false,
              arrows: true,
            });
          };
          newImg.src = self.attr('src');
        });
      });
    }
  };
  var initSlider = function(sliderEl, btnNext, btnPrev) {
    var wrapperSlides = $(sliderEl).find('.slides');

    if(wrapperSlides.is('.slick-initialized')) {
      wrapperSlides.slick('unslick');
    }

    wrapperSlides.slick({
      dots: false,
      draggable: false,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 3,
      prevArrow: btnPrev,
      nextArrow: btnNext,
    })

    selectItemSlider(wrapperSlides);

    if(!sliderEl.data('slider-outbound')) {
      sliderEl.find('.slide-item').each(function(){
        var selfPrice = parseFloat($(this).find('.large-price').text()),
            priceAfterSelect = selfPrice - priceDateOutbound;

        $(this).find('.large-price').text('+ ' + (priceAfterSelect > 0 ? priceAfterSelect : 0));
      })
    }
  }

  var selectItemSlider = function(wrapperSlides) {
    var slideItem = wrapperSlides.find('.slick-slide');
    slideItem.each(function(){
      if (isChooseDate) {
        $(this).data('date') === isChooseDate && $(this).addClass('selected');
      }
      $(this).off('click.selectSlideItem').on('click.selectSlideItem', function(e, isFirst){
        e.preventDefault();
        if($('body').hasClass('sk-ut-workflow') && !isFirst) {
          return false;
        }
        var isOutBound = $(this).closest('[data-fs-slider]').data('slider-outbound');
        isOutBound && $(this).closest('[data-fs-slider]').attr('data-selected-date', $(this).data('date'));
        slideItem.removeClass('selected');
        $(this).addClass('selected');
        isChooseDate = $(this).data('date');

        if (isOutBound) {
          priceDateOutbound = parseFloat($(this).find('.large-price').text());
        }
      });
    })
  }

  var renderPopupBenefit = function(popupEl, callback) {
    $.get(global.config.url.fsEconomyBenefitTemplate, function (data) {
      var content = popupEl.find('.popup__content');
      var template = window._.template(data, {
        data: data
      });
      $(template).prependTo(content.empty());
      // reinit js
      SIA.initTabMenu();
      SIA.multiTabsWithLongText()

      if (callback) {
        callback();
      }
    });
  }

  var renderPopupPremiumBenefit = function(popupEl, callback) {
    $.get(global.config.url.fsEconomyPremiumBenefitTemplate, function(data) {
      var content = popupEl.find('.popup__content');
      var template = window._.template(data, {
        data: data
      });
      $(template).prependTo(content.empty());
      // reinit js
      SIA.initTabMenu();
      SIA.multiTabsWithLongText()

      if (callback) {
        callback();
      }
    });
  }

  var renderPopupUpsell = function(popupEl, callback) {
    $.get(global.config.url.fsUpsell, function(data) {
      var content = popupEl.find('.popup__content');
      var template = window._.template(data, {
        data: data
      });
      $(template).prependTo(content.empty());

      if (callback) {
        callback();
      }
    });
  }

  var renderPopupTableFlight = function(content, popupEl, callback) {
    popupEl.find('.popup__content').children().remove();
    content.clone().first().appendTo(popupEl.find('.popup__content'));
    initPopup();
    popupEl.find('.select-fare-block').removeClass('block-show-popup-mobile');
    if(SIA.accordion) {
      SIA.accordion();
    }
    if($('[data-tooltip]')) {
      $('[data-tooltip]').kTooltip();
    }
    if (callback) {
      callback();
    }
  }

  var initPopup = function(data) {
    var triggerPopup = $('[data-trigger-popup]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',

          afterHide: function(){
            container.css('padding-right', '');
            $('.popup:not(.hidden)').length ? body.css('overflow', 'hidden') : body.css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
              if (ua.indexOf('chrome') > -1) { }
              else {
                body.attr('style', function(i, s) {
                  s && s.replace('overflow:hidden !important;','');
                });
              }
            }
          }
        });
      }
      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        var jsonURL = self.data('flight-json-url');
        if (!self.hasClass('disabled')) {
          // render popup benefit detail
          if (self.data('trigger-popup') === '.popup-view-benefit--krisflyer') {
            renderPopupBenefit(popup, function(){
              popup.Popup('show');
              $(window).trigger('resize');
            });
          } else if (self.data('trigger-popup') === '.popup-view-partner-airlines') {
            popup.Popup('show');
          } else if (self.data('trigger-popup') === '.popup-view-premium-benefit--krisflyer') {
            renderPopupPremiumBenefit(popup, function(){
              popup.Popup('show');
              $(window).trigger('resize');
            });
          } else if(self.data('trigger-popup') === '.popup--oal') {
           popup.Popup('show');
           $(window).trigger('resize');
          } else if (self.data('trigger-popup') === '.popup-upsell') {
            // prototype
            if($('body').hasClass('sk-ut-flight-search-a')) {
              if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Super Saver' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
              } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Lite' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
              }
            }

            if($('body').hasClass('sk-ut-flight-search-b')) {
              if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Standard' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 3)) {
                return false;
              } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Flexi' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
                return false;
              }
            }
            //
            renderPopupUpsell(popup, function(){
              popup.Popup('show');
              $(window).trigger('resize');
            });
          } else if (self.data('trigger-popup') === '.popup-table-flight') {
            var content = $('.'+ self.data('content-popup'));

            if($('body').hasClass('sk-ut-workflow') && $(self).hasClass('economy-flight--pey')) {
              return false;
            }

            renderPopupTableFlight(content, popup, function(){
              if(!$(self).hasClass('not-available')){
                popup.Popup('show');
                $(window).trigger('resize');
              }
            });
          } else if (self.data('trigger-popup') === '.popup-view-our-partner-airlines') {
            popup.Popup('show');
            $(window).trigger('resize');
          }
        }
      });
    });
  };

  var getTemplateCarousel = function(sliderEl, btnNext, btnPrev, daysData, callback) {
    $.get(global.config.url.fsSevenDayFareTemplate, function(data) {
      var slides = sliderEl.find('.slides');
      var template = window._.template(data, {
        data: daysData
      });
      slides.empty().append($(template));
      initSlider(sliderEl, btnNext, btnPrev);

      if(callback) {
        callback();
      }
    });
  }

  var loadCarouselSevenDay = function(sliderEl, btnNext, btnPrev, isNext) {
    var urlSevenday = global.config.url.fsSevenDayFareJson;
    var selfWrapContent = sliderEl.closest('.wrap-content-fs').index();

    if($('body').hasClass('sk-ut-flight-search-a')) {
        urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-a-m-1.json' : 'ajax/histogram-bydate-response-sk-a-m-2.json';
    } else if($('body').hasClass('sk-ut-flight-search-b')) {
        urlSevenday = selfWrapContent === 0 ? 'ajax/histogram-bydate-response-sk-b-m-1.json' : 'ajax/histogram-bydate-response-sk-b-m-2.json';
    }
    $.get(urlSevenday, function(data){
      var initDays = data.response.byDay,
          currDate = sliderEl.data('current-date'),
          currDateIdx = 0,
          slideToIdx = 0,
          currDateIdx1 = 3;

      _.map(initDays, function(day, idx){
        if(day.month === currDate) {
          currDateIdx = idx;
        }
        return false;
      })
      if (typeof isNext !== 'undefined') {
        currDateIdx = isNext ? currDateIdx + 3 : currDateIdx - 3;
      }

      currDateIdx = currDateIdx < 0 ? 0 : currDateIdx;

      currDateIdx1 = currDateIdx;

      if(initDays.length - currDateIdx < 3) {
        currDateIdx = currDateIdx - (3 - (initDays.length - currDateIdx));
      }

      days = initDays.slice(currDateIdx, currDateIdx + 3);
      getTemplateCarousel(sliderEl, btnNext, btnPrev, days, function() {
        if (typeof isNext !== 'undefined') {
          sliderEl.data('current-date', initDays[currDateIdx].month);
        } else {
          $('.slick-slide.slick-active').each(function(){
            if($(this).data('date') === sliderEl.attr('data-selected-date') && !$(this).is('.selected')) {
              $(this).trigger('click.selectSlideItem', true);
            }
          })
        }
        btnNext && initDays.length - currDateIdx1 < 5 && currDateIdx >= 0 ? btnNext.attr('disabled', true) : btnNext.attr('disabled', false);
        btnPrev && currDateIdx === 0 ? btnPrev.attr('disabled', true) : btnPrev.attr('disabled', false);
      });
    })
  }

  var handleActionSlider = function() {
    var slider = $('[data-fs-slider]');
    // init
    slider.each(function(){
      var self = $(this);
          btnPrev = $(this).find('.btn-prev'),
          btnNext = $(this).find('.btn-next');

      loadCarouselSevenDay($(this), btnNext, btnPrev);

      btnPrev.off('click.slidePrev').on('click.slidePrev', function(){
        loadCarouselSevenDay(self, $(this).siblings('.btn-next'), $(this), false);
      });

      btnNext.off('click.slideNext').on('click.slideNext', function(){
        loadCarouselSevenDay(self, $(this), $(this).siblings('.btn-prev'), true);
      });

    });

  }

  var showHideFilters = function() {
    var fsFilters = $('.flight-search-filter-economy');

    fsFilters.each(function(){
      var linkShow = $(this).find('.link-show'),
          linkHide = $(this).find('.link-hide'),
          content = $(this).find('.content');

      linkShow.off('click').on('click', function(e) {
        e.preventDefault();
        if(!$(this).parent().is('.active')) {
          $(this).parent().addClass('active')
          linkShow.hide();
          content.fadeIn();
        }
      });
      linkHide.off('click', 'a').on('click', 'a', function(e) {
        e.preventDefault();
        e.stopPropagation();
        content.hide();
        linkShow.fadeIn();
        $(this).closest('.flight-search-filter-economy').removeClass('active');
      });
    })
  };

  var showMoreLessDetails = function() {
    var btnMore = $('[data-more-details-table]'),
        btnLess = $('[data-less-details-table]'),
        triggerAnimation = $('[data-trigger-animation]'),
        wrapFlights = $('[data-wrap-flight]'),
        fareBlocks = $('[data-hidden-recommended]');

    // set data-height for wrap flight
    wrapFlights.each(function(){
      var flightLegItem = $(this).find('.flight-result-leg'),
          wrapFlightHeight = 0;

      flightLegItem.each(function(){
        wrapFlightHeight += $(this).outerHeight();
      });

      $(this).attr('data-wrap-flight-height', wrapFlightHeight);
    });

    btnMore.each(function(){
      $(this).off('click.showMore').on('click.showMore', function(e){
        e.preventDefault();
        var flightItem = $(this).closest('[data-flight-item]'),
            controlFlight = flightItem.find('.control-flight-station'),
            wrapFlight = flightItem.find('[data-wrap-flight]'),
            controlFlightHeight = wrapFlight.siblings('.control-flight-station').outerHeight() || 84,
            btnLess = wrapFlight.find('[data-less-details-table]');
        wrapFlightHeight = wrapFlight.data('wrap-flight-height');

        if (wrapFlight) {
          flightItem.addClass('active');
          wrapFlight.css({
            'height': wrapFlightHeight - controlFlightHeight + 'px',
            '-webkit-transform' : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-moz-transform'    : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-ms-transform'     : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-o-transform'      : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            'transform'         : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)'
          })
          btnLess.attr('tabindex', 0);
        }
      });
    });

    btnLess.each(function(){
      $(this).off('click.showLess').on('click.showLess', function(e){
        e.preventDefault();
        var flightItem = $(this).closest('[data-flight-item]'),
            controlFlight = flightItem.find('.control-flight-station'),
            wrapFlight = flightItem.find('[data-wrap-flight]');

        if (wrapFlight) {
          flightItem.removeClass('active');
          wrapFlight.css({
            'height': '10px',
            '-webkit-transform' : 'translate3d(0)',
            '-moz-transform'    : 'translate3d(0)',
            '-ms-transform'     : 'translate3d(0)',
            '-o-transform'      : 'translate3d(0)',
            'transform'         : 'translate3d(0)'
          })
          $(this).attr('tabindex', -1);
        }
      })
    });
  }

  var formatTimeToHour = function(seconds) {
    return parseFloat(seconds / 3600).toFixed(2) + 'hr';
  }

  var formatTimeToDate = function(seconds) {
    var dateObj = new Date(seconds),
        date = dateObj.getDate(),
        month = dateObj.getMonth() + 1,
        hour = dateObj.getHours(),
        minute = dateObj.getMinutes();

    return hour + ':' + minute;
  }

  var sliderRange = function(){
    var rangeSlider = $('[data-range-slider]');

    rangeSlider.each(function(){
      var min = $(this).data('min'),
          max = $(this).data('max'),
          step = $(this).data('step'),
          unit = $(this).data('unit'),
          type = $(this).data('range-slider');
          labelFrom = '<span class="slider-from '+ type +'"></span',
          labelTo = '<span class="slider-to '+ type +'"></span',

      $(this).slider({
        range: true,
        min: min,
        max: max,
        step: step,
        values: [min, max],
        create: function() {
          var slider = $(this),
              leftLabel,
              rightLabel;

          switch( type ) {
            case "tripDuration": case "layover":
              leftLabel = $(labelFrom).text(formatTimeToHour(min));
              rightLabel = $(labelTo).text(formatTimeToHour(max));
              break;
            case "departure": case "arrival":
              leftLabel = $(labelFrom).text(formatTimeToDate(min));
              rightLabel = $(labelTo).text(formatTimeToDate(max));
              break;
            default:
              break;
          }

          $(this).append(leftLabel);
          $(this).append(rightLabel);
        },
        slide: function(event, ui) {
          var Label;

          switch( type ) {
            case "tripDuration": case "layover":
              $(this).find('.slider-from').text(formatTimeToHour(ui.values[0]));
              $(this).find('.slider-to').text(formatTimeToHour(ui.values[1]));
              break;
            case "departure": case "arrival":
              $(this).find('.slider-from').text(formatTimeToDate(ui.values[0]));
              $(this).find('.slider-to').text(formatTimeToDate(ui.values[1]));
              break;
            default:
              break;
          }
        }
      })

      if(type === 'tripDuration') {
       $(this).find('.ui-slider-handle').eq(0).remove();
       $(this).find('.slider-from').remove();
      }

    });
  }

  var countLayover = function(segment) {
    var countLayover = 0;

    _.map(segment.legs, function(leg){
      leg.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;

      leg.stops.length && _.map(leg.stops, function(stop){
        stop.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;
      })
    })

    return countLayover;
  }

  var getFilterData = function(flightsData){
    var arr = [];

    _.map(flightsData, function(flight, flightIdx){
      var obj = {
        "nonStop" : false,
        "oneStop": false,
        "twoStop": false,
        "codeShare": false
      };


      var tripDuration = _.sortBy(flight.segments, function(segment){
          return segment.tripDuration;
      });

      var departure = _.sortBy(flight.segments, function(segment){
          return new Date(segment.departureDateTime.replace(/-/g, '/')).getTime();
      });

      var arrival = _.sortBy(flight.segments, function(segment){
          return new Date(segment.arrivalDateTime.replace(/-/g, '/')).getTime();
      });

      var layover = _.sortBy(flight.segments, function(segment){
          var totalLayover = 0;

          _.map(segment.legs, function(leg, legIdx){
            totalLayover += leg.layoverDuration;
          })
          segment['totalLayover'] = totalLayover
          return totalLayover
      });

      _.map(flight.segments, function(segment){
        var count = countLayover(segment);

        !segment.legs.length && (obj.nonStop = true);
        switch(count) {
          case 0:
            !obj.nonStop && (obj.nonStop = true);
            break;
          case 1:
            !obj.oneStop && (obj.oneStop = true);
            break;
          case 2:
            !obj.twoStop && (obj.twoStop = true);
            break;
          default:
            break;
        }

        segment.legs.length && _.map(segment.legs, function(leg){
          if(typeof leg.codeShareFlight === "boolean") {
            leg.codeShareFlight && (obj.codeShare = true);
          }
        })
      })


      obj['minTripDuration'] = tripDuration[0].tripDuration;
      obj['maxTripDuration'] = tripDuration[tripDuration.length - 1].tripDuration;
      obj['minDeparture'] = new Date(departure[0].departureDateTime.replace(/-/g, '/')).getTime();
      obj['maxDeparture'] = new Date(departure[departure.length - 1].departureDateTime.replace(/-/g, '/')).getTime();
      obj['minArrival'] = new Date(arrival[0].arrivalDateTime.replace(/-/g, '/')).getTime();
      obj['maxArrival'] = new Date(arrival[arrival.length - 1].arrivalDateTime.replace(/-/g, '/')).getTime();
      obj['minLayover'] = layover[0].totalLayover;
      obj['maxLayover'] = layover[layover.length - 1].totalLayover;

      arr.push(obj);
    })

    return arr;
  }

  var filterFlights = function(data){
    var filterBlock = $('[data-flight-filter]'),
        flightsArr = data.flights;

    filterBlock.each(function(){
      var flightsBlock = $(this).siblings('.recommended-flight-block'),
          flightIdx = $(this).data('flightFilter'),
          nonStopCheckbox = $('input[name="non-stop-'+ flightIdx +'"]'),
          oneStopCheckbox = $('input[name="one-stop-'+ flightIdx +'"]'),
          twoStopCheckbox = $('input[name="two-stop-'+ flightIdx +'"]'),
          codeShareCheckbox = $('input[name="codeshare-'+ flightIdx +'"]'),
          saGroupCheckbox = $('input[name="sa-group-'+ flightIdx +'"]'),
          sliderTripDuration = $(this).find('[data-range-slider="tripDuration"]'),
          sliderDeparture = $(this).find('[data-range-slider="departure"]'),
          sliderArrival = $(this).find('[data-range-slider="arrival"]'),
          btnFilter = $(this).find('input[name="btn-filter"]'),
          filterObj = {
            "stopover" : {
              "nonstopVal": true,
              "onestopVal": true,
              "twostopVal": true
            },
            "operating" : {
              "codeshareVal": true
            },
            "tripduration": sliderTripDuration.slider("values"),
            "departure": sliderDeparture.slider("values"),
            "arrival": sliderArrival.slider("values")
          };

      // get value from checkbox

      nonStopCheckbox.off('change.getValue').on('change.getValue', function(){
        filterObj.stopover.nonstopVal = $(this).is(':checked');
      });

      oneStopCheckbox.off('change.getValue').on('change.getValue', function(){
        filterObj.stopover.onestopVal = $(this).is(':checked')
      });

      twoStopCheckbox.off('change.getValue').on('change.getValue', function(){
        filterObj.stopover.twostopVal = $(this).is(':checked')
      });

      codeShareCheckbox.off('change.getValue').on('change.getValue', function(){
        filterObj.operating.codeshareVal = $(this).is(':checked')
      });

      saGroupCheckbox.off('change.resetFilter').on('change.resetFilter', function(){
        filterObj.stopover.nonstopVal = false;
        filterObj.stopover.onestopVal = false;
        filterObj.stopover.twostopVal = false;
        filterObj.operating.codeshareVal = false;
        filterObj.tripduration = [sliderTripDuration.data('min'), sliderTripDuration.data('max')];
        filterObj.departure = [sliderDeparture.data('min'), sliderDeparture.data('max')];
        filterObj.arrival = [sliderArrival.data('min'), sliderArrival.data('max')];
        handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
      });

      // get value from range slider

      sliderTripDuration.slider({
        stop: function(event, ui) {
          filterObj.tripduration = ui.values;
        }
      });

      sliderDeparture.slider({
        stop: function(event, ui) {
          filterObj.departure = ui.values;
        }
      });

      sliderArrival.slider({
        stop: function(event, ui) {
          filterObj.arrival = ui.values;
        }
      });

      btnFilter.off('click.handleFilter').on('click.handleFilter', function(){
        handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
      });

      // handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);

    });
  }

  var handleFilterFlights = function(el, data, flightData, filterObj, flightIdx) {
    var selfData = [],
        filterDataStopover = [],
        filterDataOperating = [],
        obj = {};

     _.map(filterObj.stopover, function(value, key){
      selfData = [];

      switch(key) {
        case 'nonstopVal':
          value && (selfData = flightData.filter(function(segment){
            var count = countLayover(segment);

            return segment.legs.length === 0 || count === 0;
          }));
          break;
        case 'onestopVal':
          value && (selfData = flightData.filter(function(segment){
            var count = countLayover(segment);

            return count === 1;
          }));
          break;
        case 'twostopVal':
          value && (selfData = flightData.filter(function(segment){
            var count = countLayover(segment);

            return count === 2;
          }));
          break;
        default:
          break;
      }

      filterDataStopover = $.unique([].concat.apply([],[filterDataStopover, selfData]));
    });

    filterDataStopover.length && (flightData = filterDataStopover);

     _.map(filterObj.operating, function(value, key){
      selfData = [];

      _.map(flightData, function(flight){
        flight.legs.length && _.map(flight.legs, function(leg){
          if(key === 'codeshareVal') {
            if(value) {
              typeof leg.codeShareFlight !== 'undefined' && selfData.push(flight);
            } else {
              typeof leg.codeShareFlight === 'undefined' && selfData.push(flight);
            }
            return false;
          }
        })
        return false;
      })

      filterDataOperating = $.unique([].concat.apply([],[filterDataOperating, selfData]));
    });

    filterDataOperating.length && (flightData = filterDataOperating);

    var tripDuration = filterObj.tripduration;

    selfData = flightData.filter(function(flight){
      return flight.tripDuration >= tripDuration[0] &&  flight.tripDuration <= tripDuration[1];
    });

    flightData = selfData;

    var departureDateTime = filterObj.departure;

    selfData = flightData.filter(function(flight){
      return new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() >= departureDateTime[0] &&  new Date(flight.departureDateTime.replace(/-/g, '/')).getTime()  <= departureDateTime[1];
    });

    flightData = selfData;

    var arrivalDateTime = filterObj.arrival;

    selfData = flightData.filter(function(flight){
      return new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() >= arrivalDateTime[0] &&  new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() <= arrivalDateTime[1];
    });

    flightData = selfData;

    obj['segments'] = flightData;

    getTemplateFlightTable(el, data, obj, flightIdx, true);

  }

  var handleLoadmore = function(flightBlock, loadmoreBlock) {
    var flights = flightBlock.find('.flight-list-item'),
        flightsHidden = flightBlock.find('.flight-list-item.hidden');

    flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
    loadmoreBlock.find('[data-total-flight]').text(flights.length);
    loadmoreBlock.find('[data-loaded-flight]').text(1);
    loadmoreBlock.off('click.loadMore').on('click.loadMore', function(e){
      e.preventDefault();
      flightBlock.find('.flight-list-item.hidden').removeClass('hidden');
      loadmoreBlock.addClass('hidden');
      showMoreLessDetails();
    });
  }

  var labelStatusCheapest = function(){
    var arrPriceCheapest = [];
    var blockWrapFirst =  $('.wrap-content-fs').first();
    var colInfoLeft = blockWrapFirst.find('.col-info-left');
    colInfoLeft.each(function(idexLabel){
      arrPriceCheapest.push($(this).find('.flight-price').find('.price-cheapest-colum').text());
      arrPriceCheapest.sort(function(a, b) {
       return parseFloat(a) - parseFloat(b);
      });
      if($(this).find('.flight-price').find('.price-cheapest-colum').text() === arrPriceCheapest[0]){
        $(this).find('.head-col').append('<span class="label-status anim-all"></span');
      }
    });
  }

  var tableLoadPage = function(isRenderTemplate){
    !isRenderTemplate && setTimeout(function(){
      if($('.main-inner').find('.wrap-content-fs').length == 2){
        $('.wrap-content-fs').last().addClass('hidden');
      };
    }, 500);
    if($('body').hasClass('fs-business')) {
      var flightBlockItem = $('.recommended-flight-block').find('.flight-list-item');
      flightBlockItem.each(function(idx){
        var tablesPopup = $(this).find('[data-hidden-recommended]').find('.table-economy-green');
        var tablesPopup1 = $(this).find('[data-hidden-recommended-1]').find('.table-economy-green');
        flightBlockItem.removeClass('economy-flight-bgd').addClass('business-flight-bgd');
        flightBlockItem.find('.column-trigger-animation').addClass('business-flight--blue');
        flightBlockItem.find('.column-trigger-animation-1').addClass('business-flight--red');
        tablesPopup.each(function(idx){
          $(this).find('.row-head-select .col-select').addClass('business-fs--blue-'+(idx+1));
        });
        tablesPopup1.each(function(idx){
            $(this).find('.row-head-select .col-select').addClass('business-fs--red-'+(idx+1));
        });
      });
    }

    var dataRecommended = $('[data-hidden-recommended]');
    var dataRecommended1 = $('[data-hidden-recommended-1]');
    dataRecommended.each(function(idxData){
      var thisWrapperContentFs = $(this).closest('.wrap-content-fs');
      var btnPrice = $(this).find('.table-economy-green.hidden-tb-dt .btn-price');
      if(btnPrice.length > 1){
        btnPrice.first().addClass('btn-price-cheapest-select');
        btnPrice.each(function(index) {
          if(index+1 < btnPrice.length && thisWrapperContentFs.index() === 0) {
            if($('body').hasClass('sk-ut-workflow')) {
              ($(this).data('price-segment') !== 2012) && $(this).attr('data-trigger-popup', '.popup-upsell');
            } else {
              $(this).attr('data-trigger-popup', '.popup-upsell');
            }
          }
        });
      }
    });
    dataRecommended1.each(function(idxData){
      var thisWrapperContentFs = $(this).closest('.wrap-content-fs');
      var btnPrice = $(this).find('.table-economy-green.hidden-tb-dt .btn-price');
      if(btnPrice.length > 1){
        btnPrice.first().addClass('btn-price-cheapest-select');
        btnPrice.each(function(index) {
          if(index+1 < btnPrice.length && thisWrapperContentFs.index() === 0) {
            $(this).attr('data-trigger-popup', '.popup-upsell');
          }
        });
      }
    });
    labelStatusCheapest();
  }

  var wrapContent, parentFlightList, el, blockParentWrap,
      blockParentColSelect , wrapContents , wrapContentList,
      nameHead , baggage , seatSelection ,earnKrisFlyer , upgrade,
      cancellation, bookingChange, noShow, priceCurrentSelected;

  $(document).on('click','[data-total-fare]', function(){
    $('[data-booking-summary-panel]').addClass('bsp-loading');
    // $('.total-fare--inner').find('[data-more-details]').css('opacity', '0');
      $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
    setTimeout(function(){
      $('[data-booking-summary-panel]').addClass('bsp-loaded');
    }, 500);
    setTimeout(function(){
      $('[data-booking-summary-panel]').removeClass('bsp-loaded bsp-loading');
      // $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
      // $('.total-fare--inner').find('[data-more-details]').css('opacity', '1');
    }, 2100);
  });

  $(document).on('click.calculatePriceInbound', '.btn-price', function(e) {
    e.preventDefault();
    // prototype
    if($('body').hasClass('sk-ut-flight-search-a')) {
      if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Super Saver' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
        return false;
      } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Lite' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
        return false;
      }
    }

    if($('body').hasClass('sk-ut-flight-search-b')) {
      if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '0' && ($(this).data('header-class') !== 'Economy Standard' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 3)) {
        return false;
      } else if($(this).closest('[data-hidden-recommended]').data('col-index').split('-')[0] === '1' && ($(this).data('header-class') !== 'Economy Flexi' || $(this).closest('[data-hidden-recommended]').data('segmentid') !== 0)) {
        return false;
      }
    }
    //
    clickedBtn = $(this);
    var arrayPrice1, arrayPrice2;
    wrapContentList = $('.wrap-content-list');
    wrapContents = wrapContentList.find('.wrap-content-fs');
    var wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');
    var listTables = wrapContentNotFirst.find('.table-economy-green.hidden-tb-dt');
    var listPrice = wrapContentNotFirst.find('.flight-list-item').find('.btn-price').find('.list-price');
    var contentListPrice = $(this).find('.list-price').text();
    var listSelectFareBlock = wrapContentNotFirst.find('.select-fare-block');
    var colSelect = $('[data-trigger-animation]');

    el = $(this);
    blockParentWrap = el.closest('.wrap-content-fs');
    blockParentColSelect =  blockParentWrap.find('.col-select');
    blockParentColSelect.find('.btn-price').removeClass('active');
    $('.col-select').find('.btn-price').removeClass('active');
    $(this).addClass('active')

    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').removeClass('has-disabled');

    if (el.closest('[data-col-index]').data('col-index').split('-')[0] != 0) {
      return;
    }

    listTables.addClass('has-disabled');

    var listRcmID = wrapContentNotFirst.find('.flight-list-item').find('.btn-price').find('.rcmid');
    var rcmIdListPrice = $(this).find('.rcmid-out').text();
    var rcmidListPriceTrim = rcmIdListPrice.trim();
    var arrayRcmId1 = rcmidListPriceTrim.split(" ");
    if(listRcmID.length){
      listRcmID.each(function(){
        var self = $(this);
        var arrayList = self.text().split("-");
        for(var j = 0 ; j < arrayList.length ; j++){ // 21 63
          for(var z = 0 ; z < arrayList[j].length ; z++){ //
              for(var i = 0 ; i < arrayRcmId1.length ; i++){ // 8
                if(arrayList[z] !== null || arrayList[z] !== "undefined" || arrayList[z] !== undefined ){
                  if(arrayList[z]){
                    if(arrayRcmId1[i] === arrayList[z].slice(0 , 4).trim()){

                      var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
                      var thisCol = $(this).closest('.col-select');
                      var idx = thisCol.index();
                      var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
                      listRow.each(function() {
                        $(this).find('.col-select').eq(idx).removeClass('has-disabled');
                      });
                      thisColSelectOneCol.removeClass('has-disabled');

                      $(this).closest('.btn-price').find('.btn-price-cheapest-colum').text(arrayList[z].slice(3).trim());
                      $(this).closest('.btn-price').find('.rcmid-corresponding').text(arrayRcmId1[i]);
                    }
                  }
                }
              }
          }
        }
      });
    }

    var contentListPriceTrim = contentListPrice.trim();
    arrayPrice1 = contentListPriceTrim.split(" ");
    listPrice.each(function(){
      var self = $(this);
      arrayPrice2 = self.text().split(" ");
      for(var i = 0; i < arrayPrice1.length; i++){
        for(var j = 0; j < arrayPrice2.length; j++){
          if(arrayPrice1[i] === arrayPrice2[j]){
            var thisListTables = $(this).closest('.select-fare-block').find('.table-economy-green.hidden-tb-dt');
            var idx = $(this).closest('.col-select').index()-1;
            thisListTables.each(function(index) {
              if (index === idx) {
                $(this).removeClass('has-disabled');
              }
            });
          }
        }
      }
    });

    // remove class has-disabled for cabin if has all items disabled
    listSelectFareBlock.each(function() {
      var thisListTables = $(this).find('.table-economy-green.hidden-tb-dt');
      var thisListTablesHasDisabled = $(this).find('.table-economy-green.hidden-tb-dt.has-disabled');
      var idx = $(this).data('col-index');

      if(thisListTables.length === thisListTablesHasDisabled.length) {
        colSelect.each(function() {
          if($(this).data('trigger-animation') === idx) {
            $(this).addClass('has-disabled');
          }
        });
      }
    });

    var flightColumnTrigger = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation');
    var flightColumnTrigger1 = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation-1');
      priceCurrentSelected = el.find('.btn-price-cheapest-colum').text();
    var arrPriceCheapest = [];
    var blockWrapFirst =  $('.wrap-content-fs').last();
    var colInfoLeft = blockWrapFirst.find('.col-info-left');
    colInfoLeft.find('.head-col').find('.label-status').remove();

      flightColumnTrigger.each(function(idx){
      $(this).find('.price-cheapest-outbound').text(priceCurrentSelected);
      $(this).closest('.flight-list-item').find('[data-hidden-recommended]').find('.price-cheapest-colum').text(priceCurrentSelected);
      // if(!$(this).hasClass('has-disabled')){
        var priceCheapest = $(this).find('.price-cheapest-colum').text();
        var priceSelectedOutBound = $(this).find('.price-cheapest-outbound').text();
        if(priceCheapest && priceSelectedOutBound) {
          var totalPriceCabin = parseFloat(priceCheapest) - parseFloat(priceSelectedOutBound);
          if(totalPriceCabin < 0 ){
            totalPriceCabin = 0;
          }
          var triggerAnimation = $(this);
          var priceStyleSmall = totalPriceCabin.toFixed(2).indexOf(".");
          triggerAnimation.find('.price').text("+ " + parseFloat(totalPriceCabin.toFixed(2).slice(0, priceStyleSmall)).toLocaleString());
          triggerAnimation.find('.price').find('small').text(parseFloat(totalPriceCabin.toFixed(2).slice(priceStyleSmall)).toLocaleString());

          triggerAnimation.find('.price').append('<span class="compare-number hidden">'+totalPriceCabin+'</span>');
        }
      // }
      });

      colInfoLeft.each(function(idexLabel){
        arrPriceCheapest.push($(this).find('.price').find('.compare-number').eq(0).text());
      });

      arrPriceCheapest.sort(function(a, b) {
       return parseFloat(a) - parseFloat(b);
      });

      colInfoLeft.each(function(){
        if(parseFloat($(this).find('.flight-price').find('.compare-number').eq(0).text()) === parseFloat(arrPriceCheapest[0])){
          $(this).find('.head-col').append('<span class="label-status anim-all"></span');
        }
      })

      flightColumnTrigger1.each(function(idx1){
        $(this).find('.price-cheapest-outbound-1').text(priceCurrentSelected);
        $(this).closest('.flight-list-item').find('[data-hidden-recommended-1]').find('.price-cheapest-colum').text(priceCurrentSelected);
        // if(!$(this).hasClass('has-disabled')){
          var priceCheapest1 = $(this).find('.price-cheapest-colum').text();
          var priceSelectedOutBound1 = $(this).find('.price-cheapest-outbound-1').text();
          if(priceCheapest1 && priceSelectedOutBound1) {
            var totalPriceCabin1 = parseFloat(priceCheapest1) - parseFloat(priceSelectedOutBound1);
            if(totalPriceCabin1 < 0 ){
              totalPriceCabin1 = 0;
            }
            var triggerAnimation1 = $(this);
            var priceStyleSmall1 = totalPriceCabin1.toFixed(2).indexOf(".");
            triggerAnimation1.find('.price').text("+ " + parseFloat(totalPriceCabin1.toFixed(2).slice(0, priceStyleSmall1)).toLocaleString());
            triggerAnimation1.find('.price').append("<small></small>");
            triggerAnimation1.find('.price').find('small').text(totalPriceCabin1.toFixed(2).slice(priceStyleSmall1));
          }
        // }
      });
      var colSelect = $('.wrap-content-fs').last().find('[data-hidden-recommended]').find('.col-select').find('.btn-price');
      var colSelect1 = $('.wrap-content-fs').last().find('[data-hidden-recommended-1]').find('.col-select').find('.btn-price');
      colSelect.each(function(idxColSelect){
        var priceCheapestColumn = $(this).closest('[data-hidden-recommended]').find('.price-cheapest-colum').text();
        var priceCheapestColumnCabin = $(this).find('.btn-price-cheapest-colum').text();

        var totalPriceBtnColumn = parseFloat(priceCheapestColumnCabin) - parseFloat(priceCheapestColumn);
        var toFixedPrice = totalPriceBtnColumn.toFixed(2);
        if(totalPriceBtnColumn < 0 ){
          totalPriceBtnColumn = 0;
        }

        var priceStyleUnitSmall = toFixedPrice.indexOf(".");
        if(priceCheapestColumnCabin !== ''){
          $(this).find('.unit-small').text("");
          $(this).find('.btn-price-cheapest').empty().text("+ " + parseFloat(totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall)).toLocaleString());
          $(this).find('.unit-small').text(totalPriceBtnColumn.toFixed(2).slice(priceStyleUnitSmall));
          $(this).attr('data-price-segment-after', totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall));
        }
      });

      colSelect1.each(function(idxColSelect1){
        var priceCheapestColumn1 = $(this).closest('[data-hidden-recommended-1]').find('.price-cheapest-colum').text();
        var priceCheapestColumnCabin1 = $(this).find('.btn-price-cheapest-colum').text();

        var totalPriceBtnColumn1 = parseFloat(priceCheapestColumnCabin1) - parseFloat(priceCheapestColumn1);
        var toFixedPrice1 = totalPriceBtnColumn1.toFixed(2);

        if(totalPriceBtnColumn1 < 0 ){
          totalPriceBtnColumn1 = 0;
        }

        var priceStyleUnitSmall1 = toFixedPrice1.indexOf(".");
        if(priceCheapestColumnCabin1 !== ''){
          $(this).find('.unit-small').text("");
          $(this).find('.btn-price-cheapest-1').empty().text("+ " + parseFloat(totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1)).toLocaleString());
          $(this).find('.unit-small').text(totalPriceBtnColumn1.toFixed(2).slice(priceStyleUnitSmall1));
          $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
        }
      });
  });

  $(document).on('click', '.group-btn > #btn-upgrade', function(e) {
    e.preventDefault();
    if($('body').hasClass('sk-ut-workflow')) {
      return false;
    }
    var btnPriceActive =  $('.popup-table-flight').find('.btn-price.active');
    var parentTable = btnPriceActive.closest('.table-economy-green');
    var btnPriceNext = parentTable.next().find('.btn-price');
    if(btnPriceNext.length){
      btnPriceActive.removeClass('active');
      btnPriceNext.addClass('active');
    }

    //once user selected upsell will go to selected state.
    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').trigger('click');
  });

  // Upon upsell modal user straight to selected mode
  $(document).on('click', '#btn-keep-selection', function(e) {
    e.preventDefault();
    $('.popup-table-flight').find('input[name="proceed-fare"], input[name="proceed-fare-1"]').trigger('click');
  });

  var idx1, idx2;

  $(document).off('click.closeModal').on('click.closeModal', 'input[name="proceed-fare"], input[name="proceed-fare-1"]', function(){
    var el = $(this);
    var wrapContentList = $('.wrap-content-list');
    var wrapContents = wrapContentList.find('.wrap-content-fs');
    var thisWrapContentFs;
    var selectFareBlock = el.closest('.select-fare-block');
    var modalId = selectFareBlock.data('col-index');
    var btnPrice = selectFareBlock.find('.btn-price');
    var btnPriceActive = selectFareBlock.find('.btn-price.active');
    var btnPriceCheapestColum = selectFareBlock.find('.btn-price.active .btn-price-cheapest-colum').text();
    var btnPriceCheapest = selectFareBlock.find('.btn-price.active .btn-price-cheapest, .btn-price.active .btn-price-cheapest-1').text();
    var unitSmall = selectFareBlock.find('.btn-price.active .unit-small').text();
    var colSelect = $('[data-trigger-animation]');
    var activeFareTable, getClassNameColor, codeOrigin, codeDestination, nameHead , listValues, rcmidCorresponding, fareFamilyId, flightId;
    var fareSummaryGroup = $('.fare-summary-group');
    var listTablesSummary = fareSummaryGroup.find('.table-fare-summary.hidden-tb-dt');
    var noteFare = $('.has-note-fare');

    noteFare.show();

    el.css('pointer-events', 'none');
    colSelect.each(function() {
      if($(this).data('trigger-animation') === modalId && btnPrice.hasClass('active')) {
        thisWrapContentFs = $(this).closest('.wrap-content-fs');
        var thisFlightListItem = $(this).closest('.flight-list-item');
        var thisChangeFlightItem = thisFlightListItem.find('.change-flight-item');
        var thisHeadRecommendedSelected = thisWrapContentFs.find('.head-recommended-selected');
        flightId = thisFlightListItem.find('.segment-id').text();

        thisWrapContentFs.find('.economy-slider, .sub-logo , .monthly-view , .status-list , .flight-search-filter-economy, .loadmore-block, .flight-list-item, .recommended-table, .head-recommended').addClass('hidden');
        thisFlightListItem.removeClass('hidden');
        thisChangeFlightItem.removeClass('hidden');
        thisHeadRecommendedSelected.removeClass('hidden');
        thisWrapContentFs.next().removeClass('hidden');

        thisChangeFlightItem.find('.price').text(btnPriceCheapestColum);

        if (thisWrapContentFs.index() === ($('.wrap-content-fs').length-1)) {
          fareSummaryGroup.removeClass('hidden');
          slideshowpremiumeconomy();
          // $('[data-slideshow-premium-economy]').find('.slides')[0].slick.setPosition()
          thisChangeFlightItem.find('.price').text(btnPriceCheapest + unitSmall);
          thisHeadRecommendedSelected.html('<span>Selected return flight</span>');
        }
      };
    });

    function appendValueFareSummary() {
      activeFareTable = btnPriceActive.closest('.select-fare-table');
      getClassNameColor = activeFareTable.find('.row-head-select .col-select').removeClass('col-select').attr('class');
      codeOrigin = thisWrapContentFs.find('.code-origin-airport').first().text();
      codeDestination = thisWrapContentFs.find('.code-destination-airport').first().text();
      nameHead = activeFareTable.find('.row-head-select span').last().text();
      listValues = activeFareTable.find('.row-select .col-select span');
      eligibleRecommendationIds = activeFareTable.find('.eligible-oc-recommendation-ids').text();
      rcmidCorresponding = btnPriceActive.find('.rcmid-corresponding').text();
      fareFamilyId = btnPriceActive.siblings('.fare-family-id').text();
      if(thisWrapContentFs.index() === 0) {
        idx1 = btnPriceActive.siblings('.index-of').text();
      } else {
        idx2 = btnPriceActive.siblings('.index-of').text();
      }

      listTablesSummary.each(function(index) {
        var _this = $(this);
        var valuettt = _this.find('.ttt');

        if(index === thisWrapContentFs.index()) {
          _this.find('.head-wrapper').attr('class', 'head-wrapper').addClass(getClassNameColor);
          _this.find('.code-flight').text(codeOrigin + ' - ' + codeDestination);
          _this.find('.name-header').text(nameHead);
          listValues.each(function() {
            var desItem;
            function appendStyles(e, eDes) {
              if(e.hasClass('fare-price')) {
                eDes.addClass('fare-price');
              }
              if(e.hasClass('not-allowed')) {
                eDes.addClass('not-allowed');
              }
              if(e.hasClass('complimentary')) {
                eDes.addClass('complimentary');
              }
            }
            if ($(this).hasClass('baggage')) {
              desItem = _this.find('.baggage');
              desItem.attr('class', 'baggage').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('seat-selection')) {
              desItem = _this.find('.seat-selection');
              desItem.attr('class', 'seat-selection').text($(this).text());
              appendStyles($(this), desItem);

              // desItem.siblings('.complimentary-note').remove();
              // if($(this).siblings('.complimentary-note').length) {
              //   var seatSelectionExtra = $(this).siblings('.complimentary-note');
              //   if(!desItem.siblings('.complimentary-note').length) {
              //     desItem.parent().append(seatSelectionExtra.clone());
              //   }
              // }
            }
            if ($(this).hasClass('earn-krisFlyer')) {
              desItem = _this.find('.earn-krisFlyer');
              desItem.attr('class', 'earn-krisFlyer').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('upgrade')) {
              desItem = _this.find('.upgrade');
              desItem.attr('class', 'upgrade').text($(this).text());
              appendStyles($(this), desItem);

              if($(this).siblings('[data-tooltip]').length) {
                var upgradeTooltip = $(this).siblings('[data-tooltip]');
                if(!desItem.siblings('[data-tooltip]').length) {
                  desItem.parent().append(upgradeTooltip.clone());
                  desItem.siblings('[data-tooltip]').kTooltip();
                }
              } else {
                desItem.siblings('em').remove();
              }
            }

            if ($(this).hasClass('cancellation')) {
              desItem = _this.find('.cancellation');
              desItem.attr('class', 'cancellation').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('booking-change')) {
              desItem = _this.find('.booking-change');
              desItem.attr('class', 'booking-change').text($(this).text());
              appendStyles($(this), desItem);
            }
            if ($(this).hasClass('no-show')) {
              desItem = _this.find('.no-show');
              desItem.attr('class', 'no-show').text($(this).text());
              appendStyles($(this), desItem);
            }
          })
          valuettt.attr('name', "ttt-"+ eligibleRecommendationIds);
          valuettt.attr('value', eligibleRecommendationIds);
          if(valuettt.val() === "0"){
            fareSummaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
            fareSummaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
            fareSummaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
          } else{
            fareSummaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
            fareSummaryGroup.find('.button-group-1').find('.text').addClass('hidden');
            fareSummaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
          }
        }
      });

      // --- Compare 2 fare conditions
      if(idx1 < idx2) {
        var a1 = listTablesSummary.eq(0).find('.cancellation').attr('class');
        var a2 = listTablesSummary.eq(0).find('.cancellation').text() + ' *';
        var b1 = listTablesSummary.eq(0).find('.booking-change').attr('class');
        var b2 = listTablesSummary.eq(0).find('.booking-change').text() + ' *';
        var c1 = listTablesSummary.eq(0).find('.no-show').attr('class');
        var c2 = listTablesSummary.eq(0).find('.no-show').text() + ' *';
        listTablesSummary.eq(1).find('.cancellation').attr('class', a1).text(a2);
        listTablesSummary.eq(1).find('.booking-change').attr('class', b1).text(b2);
        listTablesSummary.eq(1).find('.no-show').attr('class', c1).text(c2);
      } else if (idx1 > idx2) {
        var a1 = listTablesSummary.eq(1).find('.cancellation').attr('class');
        var a2 = listTablesSummary.eq(1).find('.cancellation').text() + ' *';
        var b1 = listTablesSummary.eq(1).find('.booking-change').attr('class');
        var b2 = listTablesSummary.eq(1).find('.booking-change').text() + ' *';
        var c1 = listTablesSummary.eq(1).find('.no-show').attr('class');
        var c2 = listTablesSummary.eq(1).find('.no-show').text() + ' *';
        listTablesSummary.eq(0).find('.cancellation').attr('class', a1).text(a2);
        listTablesSummary.eq(0).find('.booking-change').attr('class', b1).text(b2);
        listTablesSummary.eq(0).find('.no-show').attr('class', c1).text(c2);
      } else {
        noteFare.hide();
      }
      // --- End

      fareSummaryGroup.find('#recommendation-id').val(rcmidCorresponding);
      if(thisWrapContentFs.index() === wrapContents.length-1) {
        fareSummaryGroup.find('#fare-family-inbound').val(fareFamilyId);
        fareSummaryGroup.find('#flight-inbound').val(flightId.trim());
        $('.flight-search-summary-conditions').find('.tab-item-2').text(codeOrigin + ' - ' + codeDestination);
        $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');
      } else {
        fareSummaryGroup.find('#fare-family-outbound').val(fareFamilyId);
        fareSummaryGroup.find('#flight-outbound').val(flightId.trim());
        $('.flight-search-summary-conditions').find('.tab-item-1').text(codeOrigin + ' - ' + codeDestination);
        $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

        showCorrelativeTab();
      }
    }

    // scrollTop func
    var scrollTopToContent = function() {
      setTimeout(function(){
        $('html, body').animate({
          scrollTop: $(thisWrapContentFs).offset().top - 10
        }, 0);
      }, 750);
    }

    appendValueFareSummary();
    handleActionSlider();
    showMoreLessDetails();
    scrollTopToContent();
  });

  $(document).on('click', '.button-group-1 > .button-change', function(e) {
    var thisWrapContentFs = $(this).closest('.wrap-content-fs');
    var fareSummaryGroup = $('.fare-summary-group');

    thisWrapContentFs.nextAll()
      .addClass('hidden')
      .find('.btn-price').removeClass('active');

    thisWrapContentFs.find('.change-flight-item, .head-recommended-selected').addClass('hidden');
    fareSummaryGroup.addClass('hidden');
    thisWrapContentFs.find('.economy-slider, .sub-logo , .monthly-view , .status-list , .flight-search-filter-economy, .flight-list-item, .recommended-table, .head-recommended, .loadmore-block').removeClass('hidden');
    thisWrapContentFs.find('.btn-price').removeClass('active');

    if(thisWrapContentFs.index() === 1) {
      $('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
      $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
    }
    handleLoadmore(thisWrapContentFs.find('.recommended-flight-block'), thisWrapContentFs.find('[data-loadmore]'));
    showMoreLessDetails();
    handleActionSlider();
    $('form.fare-summary-group').addClass('hidden');
  });

  // show 2nd tab when click link right
  function showCorrelativeTab() {
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-left', function() {
      $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
    });
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-right', function() {
      $('.flight-search-summary-conditions').find('.tab-right').trigger('click');
    });
  }

  var workflowTable = function(isRenderTemplate){
    tableLoadPage(isRenderTemplate);
    var priceTable = $('.recommended-table').find('.price');
    var flightListItem = $('.flight-list-item');
    var flightList = $('.wrap-content-fs').first().find('.flight-list-item');
    priceTable.each(function(idx){
      var contentInner = $(this).text();
      if($.trim(contentInner) === ""){
        var parentPrice = $(this).closest('.col-info-select');
        parentPrice.addClass('not-available');
        parentPrice.find('.flight-price').remove();
        parentPrice.find('span.not-available').empty().text('Not available');
      }
    });
    flightListItem.each(function(idx){
      var flightStationItem = $(this).find('.flight-station-item');
      flightStationItem.each(function(idx){
        if(!$(this).is(':last-child')){
          $(this).find('.less-detail').remove();
        }
      });
    });
  }
  var getTemplateFlightTable = function(el, data1, flightData, index, isFilter) {
      var loadmoreBlock = el.siblings('[data-loadmore]');
      var miniUrl = 'ajax/minifare-conditions.json';
      var upgradeUrl = 'ajax/fare-conditions-upgrade.json';

      if($('body').hasClass('sk-ut-flight-search-a')){
        miniUrl = 'ajax/minifare-conditions-a.json';
        upgradeUrl = 'ajax/fare-conditions-upgrade-a.json';
      } else if($('body').hasClass('sk-ut-flight-search-b')){
        miniUrl = 'ajax/minifare-conditions-b.json';
        upgradeUrl = 'ajax/fare-conditions-upgrade-b.json';
      }

      if($('body').hasClass('fs-sk-mixed-rbd-biz')){
         miniUrl = 'ajax/sk-minifare-conditions.json';
      }

      if($('body').hasClass('fs-economy-rbd')){
           miniUrl = 'ajax/sk-minifare-conditions-pey-biz-syd-maa-new.json';
      }

      var tplUrl = ''; 
      if ($('body').hasClass('fs-OAL-prompt')) {
        miniUrl = 'ajax/sk-fare-families-264-minifare.json';
        tplUrl = global.config.url.fsEconomyFlightTable2;
      } else {
        tplUrl = global.config.url.fsEconomyFlightTable
      }

      if(miniFareData) {
        $.get(tplUrl, function(data) {
          var template = window._.template(data, {
            data: data1,
            familyData: miniFareData,
            flight: flightData ? flightData : data1.flights[index],
            flightIdx: index
          });

          el.find('.flight-list-item').remove();
          el.append($(template));
          el.find('.flight-list-item:gt(4)').addClass('hidden');

          if(isFilter && index === 1) {
            $(clickedBtn).trigger('click.calculatePriceInbound');
          }

          showMoreLessDetails();
          initPopup();
          // slideshowpremiumeconomy();
          // init tooltip
          if($('[data-tooltip]')) {
            $('[data-tooltip]').kTooltip();
          }
          workflowTable(isFilter);
          handleLoadmore(el, loadmoreBlock);

          var segmentsLength = el.find('.flight-list-item').length;

          if(!segmentsLength) {
            el.addClass('hidden');
            el.siblings('.no-result-filter').removeClass('hidden');
          } else {
            el.siblings('.no-result-filter').addClass('hidden');
            el.removeClass('hidden');
          }

        })
      } else {
        $.ajax({
          url: miniUrl,
          type: SIA.global.config.ajaxMethod,
          dataType: 'json',
          success: function(fareData) {
            $.ajax({
              url: miniUrl,
              type: SIA.global.config.ajaxMethod,
              dataType: 'json',
              success: function(response) {
                  miniFareData = response
                  $.get(tplUrl, function(data) {
                    var template = window._.template(data, {
                      fareData: fareData,
                      data: data1,
                      familyData: response,
                      flight: flightData ? flightData : data1.flights[index],
                      flightIdx: index
                    });

                    el.find('.flight-list-item').remove();
                    el.append($(template));
                    el.find('.flight-list-item:gt(4)').addClass('hidden');

                    if(isFilter && index === 1) {
                      $(clickedBtn).trigger('click.calculatePriceInbound');
                    }

                    showMoreLessDetails();
                    initPopup();
                    // slideshowpremiumeconomy();
                    // init tooltip
                    if($('[data-tooltip]')) {
                      $('[data-tooltip]').kTooltip();
                    }
                    workflowTable(isFilter);
                    handleLoadmore(el, loadmoreBlock);

                    var segmentsLength = el.find('.flight-list-item').length;

                    if(!segmentsLength) {
                      el.addClass('hidden');
                      el.siblings('.no-result-filter').removeClass('hidden');
                    } else {
                      el.siblings('.no-result-filter').addClass('hidden');
                      el.removeClass('hidden');
                    }

                  })
                }
            });
          }
        })
      }
  }

  var renderFlightTable = function(data1) {
    var flightBlock = $('.recommended-flight-block');

    flightBlock.each(function(index){
      var self = $(this);
      if(index === 0) {
        getTemplateFlightTable (self, data1, null, index);
      } else {
        setTimeout(function(){
          getTemplateFlightTable (self, data1, null, index);
        }, 2000);
      }
    })
  }

  var resetFilter = function() {
    var btnReset = $('[data-reset-filter]');

    btnReset.each(function(){
      $(this).off('click.resetFilter').on('click.resetFilter', function(e){
        e.preventDefault();
        var filterBlock = $(this).closest('.no-result-filter').siblings('[data-flight-filter]'),
            listCheckbox = filterBlock.find('input[type="checkbox"]').not("[disabled]"),
            listRangeSlider = filterBlock.find('[data-range-slider]'),
            checkboxReset = filterBlock.find('input[type="checkbox"]:disabled');

        listCheckbox.each(function(){
          $(this).prop('checked', false);
        })

        listRangeSlider.each(function(){
          var min = $(this).data('min'),
              max = $(this).data('max');
          $(this).slider("option", "values", [min, max]);
        })

        checkboxReset.prop('checked', true).trigger('change.resetFilter');

      })
    })
  }

  var renderCombinationsJson = function(){
    var templateBookingPayment;
    var appendDiv;
    if(!$('body').hasClass('fs-economy')){
      appendDiv = $('.combinations-json');
    }else{
      appendDiv = $('.fs-economy').find('.top-main-inner');
    }

    var combinationsJson = function(data1){
      if(!$('body').hasClass('fs-economy')){
        $.get(global.config.url.combinationsJsonTpl, function (data) {
          var template = window._.template(data, {
            data: data1
          });
          templateBookingPayment = $(template);
          appendDiv.append(templateBookingPayment);
        });
      }else{
        $.get(global.config.url.fsEconomy, function (data) {
          var filterArr = getFilterData(data1.flights);
          var template = window._.template(data, {
            data: data1,
            filterArr: filterArr
          });
          templateBookingPayment = $(template);
          appendDiv.append(templateBookingPayment);
          renderFlightTable(data1);
          showHideFilters();
          handleActionSlider();
          sliderRange();
          filterFlights(data1);
          resetFilter(data1.flights);

          var urlPage;
          if($('body').hasClass('sk-ut-flight-search-a')){
            urlPage = 'sk-ut-passenger-details-a.html';
          } else if($('body').hasClass('sk-ut-flight-search-b')){
            urlPage = 'sk-ut-passenger-details-b.html';
          }

          if(urlPage) {
            $('form[name="flight-search-summary"]').attr('action', urlPage);
          }
        });
      }
    };
    var url;
    if($('body').hasClass('fs-economy-response-page')){
      url = "ajax/flightsearch-response.json";
    }
    if($('body').hasClass('fs-economy-response-new-page') || $('body').hasClass('fs-economy')){
      url = "ajax/sin-sfo-30 JUNE-2017-Most-updated.json";
    }
    if($('body').hasClass('sk-ut-flight-search-a')){
      url = "ajax/sk-ut-flight-search-a.json";
    }
    if($('body').hasClass('sk-ut-flight-search-b')){
      url = "ajax/sk-ut-flight-search-b.json";
    }
    if($('body').hasClass('fs-business')){
      url = "ajax/sin-sfo-business-first.json";
    }
    if($('body').hasClass('fs-economy-scoot')){
      url = "ajax/sin-sfo-economy-scoot.json";
    }
    if($('body').hasClass('fs-economy-two-column-premium-economy')){
      url = "ajax/sin-sfo-tow-column-premium-economy.json";
    }
    if($('body').hasClass('fs-economy-sin-maa-roundtrip-page')){
      url = "ajax/flight-search-SIN-MAA-Roundtrip-2A1C1I.json";
    }
    if($('body').hasClass('fs-economy-four-column')){
      url = "ajax/sin-sfo-economy-four-column.json";
    }

    if ($('body').hasClass('fs-OAL-prompt')) {
      url = 'ajax/sk-fare-families-264.json'; 
    }

    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(response) {
        var data1 = response.response;
        combinationsJson(data1);
      }
    });
  };

  var popup1 = $('.popup--flights-details-sf');
  var popup2 = $('.flight-search-summary-conditions');
  $(document).on('click', '.flights-details-sf', function(e) {
    e.preventDefault();
    // prototype
    if($('body').hasClass('sk-ut-workflow')) {
      return false;
    }
    //
    popup1.Popup('show');
     $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.trigger-summary-of-fare-condition', function(e) {
    e.preventDefault();
    if($(this).hasClass('link-left')){
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
    } else{
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').removeClass('active');
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').addClass('active');
    }
    popup2.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });

  $(document).on('click', '.popup__close', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    event.preventDefault();
  });

  if($('body').hasClass('fs-economy-page')){
    renderCombinationsJson();
  }

  var urlPage;
  if($('body').hasClass('sk-ut-flight-search-a')){
    urlPage = 'sk-ut-passenger-details-a.html';
  } else if($('body').hasClass('sk-ut-flight-search-b')){
    urlPage = 'sk-ut-passenger-details-b.html';
  }

  if(urlPage) {
    document.cookie = 'priceData' +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $('body').hasClass('sk-ut-workflow') && $(document).off('submit.changePage').on('submit.changePage', 'form[name="flight-search-summary"]', function(e){
      e.preventDefault()
      window.location.replace(urlPage);
    });
  }
};
