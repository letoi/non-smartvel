SIA.MealsSelection = function() {
	var p = {};

	p.global = SIA.global;
	p.config = p.global.config;
	p.body = p.global.vars.body;
	p.win = p.global.vars.win;
	p.htmlBody = $('html, body');

	// Declare templates
	p.accordionWrapperTpl = '<div id="accordionWrapper" class="meals-accordion-wrapper <%= initClass %>" />';
	p.fltBlkTpl = '<div class="flightmeal-control" />';
	p.fltTitleTpl = '<h3 class="title-4--blue" />';
	p.accordionWrapTpl = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content"></div></div>';
	p.accordionItemWrapTpl = '<div data-accordion="1" data-accordion-index="<%= id %>" class="block-2 accordion accordion-box" />';
	p.accordionTriggerTpl = '<div data-accordion-trigger="1" class="accordion__control">\
			 <div class="fm-accordion-label">\
				<ul class="meal-menu">\
				  <li class="flight-ind"><%= origin %> - <%= destination %></li>\
				  <li class="meal-service-name"><%= mealType %></li>\
				  <li class="meal-sub-title-blue" data-label="<%= menuSelection %>"><%= menuSelection %></li>\
				</ul>\
				<a href="#" data-trigger-popup=".popup--meals-selection-<%= targetClass %>"><span class="fm-ico-text">Select / Change</span>\
				<em class="ico-point-d"></em></a>\
			 </div>\
		  </div>';
	p.accordionContentTpl = '<aside class="popup popup-meals menu-list popup--meals-selection-<%= id %> hidden">\
		  <div class="popup__inner">\
				<div class="popup__content">\
					 <a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>\
					 <div data-accordion-content="1" class="accordion__content">\
						  <h2 class="light-box-title"><%= origin %> - <%= destination %> <strong><span class="popup-meal-type"><%= mealType %></span></strong></h2>\
						  <p class="light-box-text">Choose your main course from any of these menus</p>\
						  <h3 class="fm-title-4--blue">All meals</h3>\
						  <div class="item-container">\
								<article class="promotion-item promotion-item--1">\
									 <a href="#" class="promotion-item__inner" data-trigger-popup=".popup--btc-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available"></div>\
													 </div>\
													 <img src="images/bg-book-the-cook.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													 <div class="flight-item__info-1">\
														  <p class="info-promotions">Book the Cook</p>\
													 </div>\
													 <div class="flight-item__info-2">\
														  <p class="info-promotions"><em class="ico-point-r"></em></p>\
													 </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
								<article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %>">\
									 <a href="#" class="promotion-item__inner" data-trigger-popup=".popup--inflight-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available">\
														  </div>\
													 </div>\
													 <img src="images/bg-inflight-menu.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													 <div class="flight-item__info-1">\
														  <%= mealsSelectionAvailable %>\
														  <p class="info-promotions">Inflight Menu</p>\
													 </div>\
													 <div class="flight-item__info-2">\
														  <p class="info-promotions"><em class="ico-point-r"></em></p>\
													 </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
								<article class="promotion-item promotion-item--1">\
									 <a href="#" class="promotion-item__inner" data-trigger-popup=".popup--spm-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available"></div>\
													 </div>\
													 <img src="images/bg-special-meal.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													 <div class="flight-item__info-1">\
														  <p class="info-promotions">Special Meals</p>\
													 </div>\
													 <div class="flight-item__info-2">\
														  <p class="info-promotions"><em class="ico-point-r"></em></p>\
													 </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
						  </div>\
						  <hr>\
						  <div class="not-eating-supper">\
								<div class="custom-checkbox custom-checkbox--1">\
									 <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" class="not-eating" <%= checked %>>\
									 <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;m not eating <%= mealType %></label>\
									 <p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you won&apos;t be having <%= mealType %>. Our flight attendants will not approach you during mealtime. If you change your mind, however, you may still select your meal by returning to this page at least 24 hours before your departure.</p>\
								</div>\
						  </div>\
						  <div class="button-group-1 btn-confirm hidden">\
								<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
						  </div>\
					 </div>\
				</div>\
		  </div>\
	 </aside>';

	p.accordionInfantContentTpl = '<aside class="popup popup-meals menu-list popup--meals-selection-<%= id %> hidden infant-meals">\
		  <div class="popup__inner">\
				<div class="popup__content">\
					 <a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>\
					 <div data-accordion-content="1" class="accordion__content">\
						  <h2 class="light-box-title"><%= origin %> - <%= destination %> <strong><span class="popup-meal-type"><%= mealType %></span></strong></h2>\
						  <p class="light-box-text">Choose your main course from any of these menus</p>\
						  <h3 class="fm-title-4--blue">Infant Meals</h3>\
						  <div class="inf-container"></div>\
						  <hr>\
						  <div class="not-eating-supper">\
								<div class="custom-checkbox custom-checkbox--1">\
									 <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" <%= checked %> class="not-eating">\
									 <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">Bring my own meal</label>\
									 <p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you will be bringing your own meal. Our flight attendants will not approach you during mealtime. If you change your mind, however, you still select your meal by returning to this page at least 24 hours before your departure.</p>\
								</div>\
						  </div>\
						  <div class="button-group-1 btn-confirm">\
								<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
						  </div>\
					 </div>\
				</div>\
		  </div>\
	 </aside>';

	p.checkinAlert = '<div class="alert-block checkin-alert" style="top: 15px;">\
		<div class="inner">\
		  <div class="alert__icon"><em class="ico-alert"> </em></div>\
		  <div class="alert__message"><%= checknMsg %></div>\
		</div>\
	 </div>';
	p.mealsLabelTpl = '<span class="meals-label btn-small btn-grey">VIEW ONLY</span>';

	// To be used to index each vertical accordion tab to reference when changing arrow text
	p.accordTabArr = [];
	p.accordContentArr = [];
	p.accordionTabIndex = 0;
	// cache meals that are not ready for reset
	p.notReadyMeals = [];

	p.currInflightMenuArr = [];

	p.curPaxIndex = 0;
	p.paxNavCount = 0;
	p.paxArr = [];

    // Index all meal modules
	p.ifml = [];
	p.spml = [];
	p.btcml = [];
	p.infml = [];

	// declare dish icon for inflight menu scenario
	p.inflightDishIcons = {
		'ICP': 'ico-4-salmon',
		'DWH': 'ico-4-fork-1',
		'WHS': 'ico-4-fork',
		'EPG': 'ico-4-amet',
		'PLF': 'ico-4-cook',
		'MTL': 'ico-4-fork',
		'VGT': 'ico-4-leaf',
		'LCL': 'ico-4-heart',
		'LCA': 'ico-4-sandwich',
		'LCH': 'ico-4-fork',
		'CNY': 'ico-4-info',
		'XMAS': 'ico-4-pine',
		'DEP': 'ico-4-coffee',
		'TWG': 'ico-4-fork',
		'LFD': 'ico-4-cook'
	};

	var init = function() {
		p.json = globalJson.mealsInfo;

		// Assign the initial states
		getInitialStates();

		p.mealSelectionForm = $('#mealSelection');
		p.container = $('#container');
		p.paxListWrapper = $('#paxListWrapper');
		p.curWinWidth = p.win.width();

		// Update the initial data for the pax
		setupData();

		// Paint pax navigation
		paintPaxNav();

		// Paint meals menu
		paintMealsMenu();
	};

	var setupData = function(){
        // Add hidden input field in form
        p.input = $('<input type="hidden" value="" />');
        p.mealSelectionForm.prepend(p.input);

        p.data = {};
        p.data.paxList = [];

        // Loop through the passenger list
        for (var i = 0; i < p.json.paxList.length; i++) {
            var d = p.json.paxList[i];
            var f = p.json.flightSegment;

            var pax = {};
            // Add base pax info
            pax.paxId = d.paxId;
            pax.paxType = d.paxType;
            pax.paxName = d.paxName;

            // Add selected meals array
            // to hold each flightSector's mealServiceCode
            pax.selectedMeals = [];

            for (var j = 0; j < f.length; j++) {
                var segment = {
                    'segmentId': f[j].segmentId,
                    'selectedMeal':{}
                }

                // loop through the sectors
                for (var k = 0; k < f[j].flightSector.length; k++) {
                    var paxServiceType = getServiceType(i);
                    var serviceTypes = f[j].flightSector[k][paxServiceType];

                    for (var l = 0; l < serviceTypes.length; l++) {
                        var mealServiceCode = serviceTypes[l].mealServiceCode;
                        // setup initial values for each service code
                        var mealSelection = {
                            'isEating': true,
                            'isALaCarte': false,
                            'mealCategoryCode':null,
                            'mealCode': null,
                            'mealMenuName': null,
                            'aLaCarteData': null
                        }
                        segment.selectedMeal[mealServiceCode] = mealSelection;
                    }
                }

                pax.selectedMeals.push(segment);
            }

            p.data.paxList.push(pax);

        }
    };

	var updateData = function(menu){
        // Update data
        var pid = parseInt(menu.attr('data-paxid'));
        var sid = parseInt(menu.attr('data-segment'));
        var service = menu.attr('data-meal-servicecode');
        var categoryCode = menu.attr('data-meal-category');
        var mealCode = menu.attr('data-meal-mealcode');
        var mealMenuName = menu.attr('data-meal-menuname');
        var isALC = menu.attr('data-isalc');
        var aLaCarteData = null;

		if(typeof menu.attr('data-alc-json') != 'undefined') aLaCarteData = JSON.parse(menu.attr('data-alc-json'));

        var meal = p.data.paxList[pid].selectedMeals[sid].selectedMeal[service];

        // Check if element clicked is a checkbox
        if(menu.hasClass('not-eating')) {
            meal.isEating = !menu.prop('checked');
			meal.mealCode = null;
	        meal.mealMenuName = null;
        } else if(isALC && typeof aLaCarteData != 'undefined') {
            console.log(isALC, typeof isALC, aLaCarteData);
            meal.isEating = true;
            meal.mealCode = mealCode;
            meal.mealCategoryCode = categoryCode;
            meal.mealMenuName = mealMenuName;
            meal.isALaCarte = isALC;
            meal.aLaCarteData = aLaCarteData;
        } else {
            if(menu.hasClass('selected')) {
                meal.isEating = true;
                meal.mealCode = mealCode;
                meal.mealCategoryCode = categoryCode;
                meal.mealMenuName = mealMenuName;
                meal.isALaCarte = isALC;
            }else {
                meal.mealCode = null;
                meal.mealCategoryCode = null;
                meal.mealMenuName = null;
            }
        }

        p.input.val(JSON.stringify(p.data));
        console.log(p.data);
    };

	var getInitialStates = function() {
		for (var i = 0; i < p.json.paxList.length; i++) {
			if (p.json.paxList[i].paxId === p.json.passengerStartingPoint) {
				p.curPaxIndex = i;
				p.passengerStartingPoint = i;
			}
		}

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			if (p.json.flightSegment[i].segmentId == p.json.startSegment) p.startSegment = i;
		}

		for (var i = 0; i < p.json.flightSegment[p.startSegment].flightSector.length; i++) {
			if (p.json.flightSegment[p.startSegment].flightSector[i].sectorId == p.json.startSector) p.startSector = i;
		}

		p.startMealService = p.json.startMealService;
	};

	var paintMealsMenu = function(initClass = '') {
		// Reset initial values
		p.accordionTabIndex = 0;
		p.accordionWrapper = null;
		// remove existing triggers
		for (var h = 0; h < p.accordContentArr.length; h++) {
			p.accordContentArr[h].remove();
		}

		// remove existing menus
		_.each(p.ifml, function(menu){
			menu.destroy();
		});
		_.each(p.spml, function(menu){
			menu.destroy();
		});
		_.each(p.btcml, function(menu){
			menu.destroy();
		});
		_.each(p.infml, function(menu){
			menu.destroy();
		});

		// Empty the array holders
		p.accordTabArr = [];
		p.accordContentArr = [];
		p.notReadyMeals = [];

		p.ifml = [];
		p.spml = [];
		p.btcml = [];
		p.infml = [];

		// Create new accordion wrapper
		p.accordionWrapper = $(_.template(p.accordionWrapperTpl, {
			'initClass': initClass
		}));

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			// create new flight block
			var flSegment = $(p.fltBlkTpl);

			// create flight segment title
			var segmentTitle = $(p.fltTitleTpl);
			segmentTitle.html((i + 1) + '. ' + p.json.flightSegment[i].departureAirportName + ' to ' + p.json.flightSegment[i].arrivalAirportName);

			// Add title to segment block
			flSegment.append(segmentTitle);

			// check if second segment available for meal selection
			secSegmentAvailable(flSegment, i);

			// create accordion-wrapper block
			var accordionWrap = $(p.accordionWrapTpl);

			var accordionContentWrap = accordionWrap.find('.accordion-wrapper-content');

			for (var j = 0; j < p.json.flightSegment[i].flightSector.length; j++) {
				var data = p.json.flightSegment[i].flightSector[j];

				// get current pax type if adult, child or infant
				p.serviceType = getServiceType(p.curPaxIndex);

				var mealService = data[p.serviceType];

				for (var k = 0; k < mealService.length; k++) {
					// Each meal service will need an accordian item wrapper
					var accItemWrap = $(_.template(p.accordionItemWrapTpl, {
						'id': (p.accordionTabIndex + 1)
					}));

					// Check if infant
                    var mealCategoryCode, mealServiceCode;
                    if(p.serviceType == 'mealServicesInfant') {
                        mealCategoryCode = mealService[k].allMeals[0].mealCategoryInfant[0].mealCategoryCode;
                        mealServiceCode = mealService[k].mealServiceCode;
                    } else {
                        mealCategoryCode = mealService[k].allMeals[1].mealCategoryInflight[0].mealCategoryCode;
                        mealServiceCode = mealService[k].mealServiceCode;
                    }

					// check if is eating
					var curPaxData = p.data.paxList[p.curPaxIndex].selectedMeals[i].selectedMeal[mealService[k].mealServiceCode];

					var menuLabel = 'Inflight Menu';
					if (curPaxData.mealMenuName && curPaxData.isEating) {
						menuLabel = curPaxData.mealMenuName;
					}

					if (!curPaxData.isEating) {
						menuLabel = 'Not Eating';
					}

					// Create accordion trigger
					var accTrigger = $(_.template(p.accordionTriggerTpl, {
						'origin': data.departureAirportCode,
						'destination': data.arrivalAirportCode,
						'mealType': mealService[k].mealServiceName,
						'menuSelection': menuLabel,
						'targetClass': p.accordionTabIndex
					}));

					p.accordTabArr.push(accTrigger);

					// Check all meals availability attribute only shows up in inflight meal
					mealAvailData = mealsAvailable(mealService, k);

					// Create accordion content
					var contentTpl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? p.accordionInfantContentTpl : p.accordionContentTpl;

					// check if meal is not available for order
					mealsSelectionAvail = p.curPaxObj.paxType.toLowerCase() != 'infant' ? checkMealsSelAvailable(mealService, k) : {
						'mealsLabel': '',
						'viewOnly': ''
					};

					// Cache inflight setupData
					var inflightData = p.curPaxObj.paxType.toLowerCase() == 'infant' ? mealService[k].allMeals[0].mealCategoryInfant[0] : mealService[k].allMeals[1].mealCategoryInflight[0];

					var targetInflightMenu = i + '-' + j + '-' + k;
					var mealNotReadyMsg = mealAvailData.mealNotReadyMsg;

					var checked = curPaxData.isEating ? '' : 'checked';
					var notEatingTxt = curPaxData.isEating ? ' hidden' : '';

					var accContent = $(_.template(contentTpl, {
						'origin': data.departureAirportCode,
						'destination': data.arrivalAirportCode,
						'mealType': mealService[k].mealServiceName,
						'mealNotReady': mealAvailData.mealNotReady,
						'mealNotReadyMsg': mealNotReadyMsg,
						'id': p.accordionTabIndex,
						'viewOnly': mealsSelectionAvail.viewOnly,
						'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
						'targetTrigger': p.accordionTabIndex,
						'targetClass': targetInflightMenu,
						'segmentId': i,
						'sectorId': j,
						'paxId': p.curPaxIndex,
						'mealCategoryCode': inflightData.mealCategoryCode,
						'mealServiceCode': mealService[k].mealServiceCode,
						'checked': checked,
						'notEatingTxt': notEatingTxt
					}));

					if (inflightData.isMealSelectionAvailable == 'false' && inflightData.isViewMenuEligible == 'false') {
						p.notReadyMeals.push(accContent);
					}

					// paint inflight menu
					if (p.serviceType == 'mealServicesInfant') {
						var infantMealCat = getMealCategory('mealCategoryInfant', mealService[k].allMeals);
						var infm = new SIA.InfantMeal(infantMealCat,
							p.data,
							data.departureAirportCode,
							data.arrivalAirportCode,
							accContent,
							accordionContentWrap,
							i, j, k,
							p.curPaxIndex,
							mealServiceCode,
							mealService[k],
							p.accordionTabIndex,
							p.body);
                        infm.init();
						p.infml.push(infm);
					}else {
						// Init Inflight Menu
                        var inflightMealCat = getMealCategory('mealCategoryInflight', mealService[k].allMeals);
                        var ifm = new SIA.InflightMenu(inflightMealCat,
							p.data,
							accContent,
							accordionContentWrap,
							i, j, k,
							p.curPaxIndex,
							mealServiceCode,
							mealService[k],
							p.accordionTabIndex,
							p.body);
                        ifm.init();
						p.ifml.push(ifm);

						// Init book the cook menu
						var btcm = new SIA.BookTheCook(
							getMealCategory('mealCategoryBTC', mealService[k].allMeals),
							p.inflightDishIcons,
							null,
							null,
							i, j, p.curPaxIndex,
							mealService[k].mealServiceCode,
							p.data,
							data.departureAirportCode,
							data.arrivalAirportCode,
							mealService[k].mealServiceName,
							(p.accordionTabIndex + 1));

						btcm.init(i + '-' + j + '-' + k, p.body);
						p.btcml.push(btcm);

						// Init special meals menu
						var spm = new SIA.SpecialMeal(
							getMealCategory('mealCategorySPML', mealService[k].allMeals),
							p.inflightDishIcons,
							null,
							null,
							i, j, p.curPaxIndex,
							mealService[k].mealServiceCode,
							p.data,
							data.departureAirportCode,
							data.arrivalAirportCode,
							mealService[k].mealServiceName,
							(p.accordionTabIndex + 1),
							accContent);
						spm.init(i + '-' + j + '-' + k, p.body);
						p.spml.push(spm);
					}

					p.accordContentArr.push(accContent);

					// add the trigger and content in the wrapper
					accItemWrap.append(accTrigger);
					accItemWrap.append(accContent);

					// Add accordion item wrapper to accordion group wrapper
					accordionContentWrap.append(accItemWrap);

					p.accordionTabIndex++;

				}
			}

			// Add accordion-wrapper block
			flSegment.append(accordionWrap);

			// Add flight segment to accordion wrapper
			p.accordionWrapper.append(flSegment);

			// add accordion wrapper to mealSelection block
			$('#mealSelection .block-inner').prepend(p.accordionWrapper);
		}

		// Add checkbox listeners
		addCheckboxEvents();

		disableSecSegAvailable();

		// init popup when repainting on pax navigation
		initTriggerPopup();

		// remove click listener for popups on not available meals
		for (var i = 0; i < p.notReadyMeals.length; i++) {
			p.notReadyMeals[i].find('[data-trigger-popup]').off();
		}

		p.accordion = new SIA.Accordion();
		p.accordion.init();
	};

	var setMainMenuLbl = function(el, txt) {
		$('[data-accordion-index="' + el.data('main-menu-target') + '"]').find('.meal-sub-title-blue').html(txt);
	};

	var addCheckboxEvents = function() {
		// get all checkbox for not eating then add change listener
		var checkBox = $('[data-checktype="not-eating"]');
		checkBox.on({
			'change': function() {
				var t = $(this);
				var trigger = p.accordTabArr[parseInt(t.attr('data-target-trigger'))].find('.meal-sub-title-blue');
				var button = t.parent().parent().next().find('.confirm-not-eating');
				if (t.prop('checked')) {
					t.next().next().removeClass('hidden');
					button.parent().removeClass('hidden');

					button.off().on({
						'click': function(e) {
							e.preventDefault();
							$(this).parent().parent().prev().trigger('click');

							// remove all selected meals
							notEatingResetMeals($(this), ['ifm', 'btcm', 'spm', 'infm']);

							// change trigger text to not eating only if user clicks on confirm
							var msg = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'Not eating';
							trigger.text(msg);
						}
					});
				} else {
					t.next().next().addClass('hidden');
					trigger.text(trigger.attr('data-label'));
					button.parent().addClass('hidden');
					button.off()
				}

				updateData(t);
			}
		});
	};

	var paintPaxNav = function() {
		if (p.paxArr.length > 0) {
			for (var h = 0; h < p.paxArr.length; h++) {
				p.paxArr[h].off();
			}
		}

		p.paxArr = [];

		// cache paxlist
		var paxList = p.json.paxList;

		// Get dynamic pax width
		p.paxWidth = p.win.width() > 414 ? p.paxListWrapper.parent().width() * 0.5 : p.paxListWrapper.parent().width();

		// Loop through pax list and add to DOM
		for (var i = 0; i < paxList.length; i++) {
			var pax;
            if(paxList[i].paxType.toLowerCase() === 'infant') {
                pax = $('<li class="pax-nav"><a href="#" data-id="'+i+'"><span>'+(i+1)+'. '+paxList[i].paxName+' (Infant)</span></a></li>');
            }else {
                pax = $('<li class="pax-nav"><a href="#" data-id="'+i+'"><span>'+(i+1)+'. '+paxList[i].paxName+'</span></a></li>');
            }

			// Add active class to current pax
			if (i == p.curPaxIndex) {
				pax.addClass('active');
				p.activePax = i;
			}

			// add pax-start class if first pax is active pax
			if (p.curPaxIndex == 0) p.paxListWrapper.parent().addClass('pax-start');

			p.paxListWrapper.append(pax);
			p.paxArr.push(pax);
		}

		// Add conditionals for different pax counts
		if (paxList.length === 1) {
			p.paxListWrapper.parent().addClass('hidden');
			$('#main-inner').find('.main-heading').text('Meals selection for ' + paxList[0].paxName);
		} else {
			// initialise pax nav event listeners
			initPaxNav();
		}

	};

	var initPaxNav = function() {
		// Get reference to the buttons
		p.paxLt = p.paxListWrapper.prev();
		p.paxRt = p.paxListWrapper.next();

		if (typeof p.paxListWrapper.slick == 'function') {
			// Get reference to the buttons
			p.paxLt = p.paxListWrapper.prev();
			p.paxRt = p.paxListWrapper.next();

			p.paxListWrapper.slick({
				dots: false,
				infinite: false,
				responsive: [{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 415,
					settings: {
						slidesToShow: 1
					}
				}],
				slidesToShow: 1,
				speed: 600,
				draggable: true,
				slidesToScroll: 1,
				accessibility: true,
				arrows: true,
				prevArrow: p.paxLt,
				nextArrow: p.paxRt,
				initialSlide: p.curPaxIndex,
				zIndex: 0
			}).on({
				'afterChange': function(event, slick, currentSlide, nextSlide) {
					if (!$(slick.$slides[0]).hasClass('slick-active')) {
						p.paxLt.show();
						p.paxListWrapper.parent().removeClass('pax-start');
					} else {
						p.paxLt.hide();
						p.paxListWrapper.parent().addClass('pax-start');
					}

					if (!$(slick.$slides[slick.$slides.length - 1]).hasClass('slick-active')) {
						p.paxRt.show();
						p.paxListWrapper.parent().removeClass('pax-end');
					} else {
						p.paxRt.hide();
						p.paxListWrapper.parent().addClass('pax-end');
					}
				},
				'beforeChange': function(event, slick, currentSlide, nextSlide) {
					// // trigger slide change if portrait
					var ua = navigator.userAgent.toLowerCase();
					var isAndroid = ua.indexOf("android") > -1;

					if(isAndroid) {
						var o = window.screen.orientation.type;
						if (o === "portrait-secondary" || o === "portrait-primary") {
							p.paxArr[nextSlide].find('a').trigger('click');
						}
					}else {
						var o = window.orientation;
						if (o === 0 || o === 180) {
							p.paxArr[nextSlide].find('a').trigger('click');
						}
					}
				}
			});

			addPaxNavListeners();

		} else {
			setTimeout(function() {
				initPaxNav();
			}, 10);
		}
	};

	var addPaxNavListeners = function() {
		// add listeners on each pax element
		for (var i = 0; i < p.paxArr.length; i++) {
			p.paxArr[i].find('a').off().on({
				'click': function(e) {
					e.preventDefault();
					e.stopImmediatePropagation();

					var t = $(this);

					if (!t.parent().hasClass('active')) {
						p.curPaxIndex = parseInt(t.attr('data-id'));

						// Reset other pax nav and enable the clicked pax
						resetAllPax(t);

						p.activePax = p.curPaxIndex;

						// scroll the page up to focus on the new menu
						// after scroll show the next accordion wrapper
						var top = p.paxListWrapper.parent().offset().top;
						var bodyTop = p.htmlBody.prop('scrollTop');

						// index the current accordion wrapper
						p.exitWrapper = null;
						p.exitWrapper = p.accordionWrapper;

						// Add class to prepare curobj for transition
						p.exitWrapper.addClass('pre-slide-out');

						// debugger;

						// After adding the exit wrapper reference, paint the next wrapper for the next pax
						paintMealsMenu('pre-slide-in-right');

						showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
					}
				}
			});
		}
	};

	var resetAllPax = function(obj) {
		for (var i = 0; i < p.paxArr.length; i++) {
			p.paxArr[i].removeClass('active');
		}

		obj.parent().addClass('active');
	};

	/* slide out the accordion wrapper
	 */
	var showNextPaxAccordion = function(curObj, nextObj) {
		// Add listener to exitwrapper
		curObj.one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd', function() {
			curObj.remove();
		});

		setTimeout(function() {
			// Add class to exit wrapper to start animating out
			curObj.addClass('slide-out-left');
			// Add class to current accordion wrapper to start animating in
			nextObj.removeClass('pre-slide-in-right');
		}, 10);
	};

	/* check if second segment available for meal selection
	 */
	var secSegmentAvailable = function(flSegment, index) {
		var mealSelWaitListedData = p.json.flightSegment[index];
		var secSegMealSelectionStatus = mealSelWaitListedData.isSegmentAvailableForMealSelection;
		var secSegMealSelectionMsg = mealSelWaitListedData.segmentMealSelectionStatusMessage;

		if (secSegMealSelectionStatus == 'false') {
			var checkinAlert = $(_.template(p.checkinAlert, {
				checknMsg: secSegMealSelectionMsg
			}));

			// add checkin alert to second segment block
			flSegment.append(checkinAlert);

			// change accordion arrow text && icon to gray to disable
			flSegment.addClass('disable-accordion');
		}
	};

	/* disable events for all second segment meals selection
	 */
	var disableSecSegAvailable = function() {
		$('.disable-accordion [data-accordion-trigger="1"]').unbind();

		// remove href attrib to disable clickable
		$('.disable-accordion a').removeAttr('data-trigger-popup');
		$('.disable-accordion a').removeAttr('href');
	};

	/* Check all meals availability attribute only shows up in inflight meal
	 */
	var mealsAvailable = function(mealService, index) {
		var mealNotReady = '';
		var mealNotReadyMsg = '';

		if (p.serviceType != 'mealServicesInfant') {
			var mealServiceData = mealService[index].allMeals[1].mealCategoryInflight[0];
			var isViewMenuEligible = mealServiceData.isViewMenuEligible;
			var isMealSelAvail = mealServiceData.isMealSelectionAvailable;

			mealNotReady = ((isViewMenuEligible == 'false') && (isMealSelAvail == 'false')) ? 'meal-not-ready' : '';
			mealNotReadyMsg = mealServiceData.isViewMenuDate;
		}

		return {
			mealNotReady: mealNotReady,
			mealNotReadyMsg: mealNotReadyMsg
		};
	};

	/* check if meal is not available for order
	 */
	var checkMealsSelAvailable = function(mealService, index) {
		var mealsAvailData = mealService[index].allMeals[1].mealCategoryInflight[0];
		var isViewMenuEligible = mealsAvailData.isViewMenuEligible;
		var isMealSelAvail = mealsAvailData.isMealSelectionAvailable;

		if ((isViewMenuEligible == 'true') && (isMealSelAvail == 'false')) {
			return {
				'mealsLabel': p.mealsLabelTpl,
				'viewOnly': 'view-only'
			};
		} else {
			return {
				'mealsLabel': '',
				'viewOnly': ''
			};
		}
	};

	/* get current pax type if adult, child or infant
	 */
	var getServiceType = function(paxIndex) {
		p.curPaxObj = p.json.paxList[paxIndex];
		serviceType = '';
		switch (p.curPaxObj.paxType.toLowerCase()) {
			case 'adult':
				serviceType = 'mealServicesAdult';
				break;
			case 'child':
				serviceType = 'mealServicesChild';
				break;
			case 'infant':
				serviceType = 'mealServicesInfant';
				break;
			default:
				serviceType = 'mealServicesAdult';
		}

		return serviceType;
	};

	var getMealCategory = function(type, arr) {
		for (var i = 0; i < arr.length; i++) {
			if (typeof arr[i][type] != 'undefined') {
				return arr[i][type];
			}
		}
	};

	var notEatingResetMeals = function(el, meals) {
		var id = el.closest('aside').attr('class').split(' ')[3].slice(-1);
		var indexes = {
			'0' : '0-0-0',
			'1' : '0-1-0',
			'2' : '0-1-1',
			'3' : '1-0-0',
			'4' : '1-0-1',
			'5' : '1-1-0',
		}

		removeMealSel(meals, indexes[id]);
	};

	var removeNotEatingCheck = function(id) {
		var indexes = {
			'0-0-0': '0',
			'0-1-0': '1',
			'0-1-1': '2',
			'1-0-0': '3',
			'1-0-1': '4',
			'1-1-0': '5',
		}

		var notEatingEl = $('body .popup--meals-selection-' + indexes[id]).find('.not-eating-supper');
		notEatingEl.find('.not-eating-msg').addClass('hidden');
		notEatingEl.find('input').prop('checked', false);
		notEatingEl.next().addClass('hidden');
		notEatingEl.find('.fm-cbox').removeClass('checkbox_color');
	}

	var resetMeals = function(el, meals) {
		var id = el.closest('aside').attr('class').split(' ')[3].slice(-5);

		removeMealSel(meals, id);
		removeNotEatingCheck(id);
	};

	var removeMealSel = function(meals, id) {
		var inputBtn = ' .selected .fm-select-btn';
		var mealList = {
			'btcm': '.popup--btc-menu-',
			'ifm': '.popup--inflight-menu-',
			'spm': '.popup--spm-menu-'
		};

		$.each(meals, function(i, meal) {
			var inputSel = $('body ' + (mealList[meal] + id) + inputBtn);
			inputSel.closest('[data-meal-item]').removeClass('selected');
			inputSel.attr('value', 'Select');
		});
	}

	// Copied from script.js
	var initTriggerPopup = function() {
		var triggerPopup = $('[data-trigger-popup]');
		var flyingFocus = $('#flying-focus');

		triggerPopup.each(function() {
			var self = $(this);
			if (typeof self.data('trigger-popup') === 'boolean') {
				return;
			}
			var popup = $(self.data('trigger-popup'));
			if (!popup.data('Popup')) {
				popup.Popup({
					overlayBGTemplate: p.config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]'
				});
			}
			self.off('click.showPopup').on('click.showPopup', function(e) {
				e.preventDefault();

				if (!self.data('isDisabledStopIm')) {
					e.stopImmediatePropagation();
				}
				if (!self.hasClass('disabled')) {
					popup.Popup('show');
				}
			});
		});
	};

	var oPublic = {
		init: init,
		updateData: updateData,
		resetMeals: resetMeals,
		setMainMenuLbl: setMainMenuLbl,
		public: p
	};
	return oPublic;
}();

SIA.InflightMenu = function(
	jsonData,
    paxData,
    accContent,
	accordionContentWrap,
    segmentId,
    sectorId,
    serviceId,
    paxId,
    mealServiceCode,
	mealService,
	accordionTabIndex,
	body) {

	var p = {};

	// Inflight Meals TPL
	p.inflightMainTpl = '<aside class="popup popup-meals menu-list-inflight popup--inflight-menu-<%= targetClass %> hidden">\
		<div class = "popup__inner">\
			 <div class="popup__content">\
				  <a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span></a>\
				  <h3 class="light-box-title"><%= origin %> - <%= destination %><strong><span class="popup-meal-type"><%= mealType %></span></strong></h3>\
				  <p class="light-box-btn-mb<%= origin %>"><a href="#" class="lightbox-btn-back" data-close="true"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to all meals</a></p>\
				  <div class="alert-block info-box<%= viewOnlyAlert %>">\
					 <div class="inner">\
						<div class="alert__icon"><em class="ico-tooltips tool_alert"></em></div>\
						<div class="alert__message font_color alert-text-color-blue"><%= viewOnlyMsg %></div>\
					 </div>\
				  </div>\
				  <h3 class="fm-title-4--blue"><%= inflightTitle %></h3>\
				  <p class="light-box-text"><%= mealDesc %></p>\
				  <%= mealTypeSub %>\
				  <div class="item-wrapper"></div>\
			 </div>\
		  </div>\
		</aside>';

	p.inflightMenuItemTpl = '<article data-meal-item class="promotion-item inflight-menu-item--1 menu-item<%= selectedClass %>">\
		<span class="fm-inf-menu-select-badge">Selected</span>\
		<div class="fm-inf-menu">\
			 <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
			 <p class="fm-inf-menu-desc"><%= mealMenuDescription %></p>\
			 <div class="fm-footnote-txt">\
				  <em class="<%= foodIcon %> fm-footnote-logo"></em>\
				  <span class="fm-dish-text"><%= specialityInfo %></span>\
				  <input type="button" name="select" value="<%= inputVal %>" class="fm-select-btn right<%= selectClass %>" data-main-menu-target="<%= triggerId %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>" data-label="<%= mealMenuName %>">\
				  <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
						<span class="btn-small view-only-bg view-box btn-view-only">VIEW ONLY</span>\
						<span class="onboard">Order on board</span>\
				  </div>\
			 </div>\
		</div>\
		</article>';

	p.inflightMenuSpecialItemTpl = '<article data-meal-item class="promotion-item inflight-menu-item--1 menu-item<%= selectedClass %>">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="fm-ysh-choice"><%= specialMealName %></span>\
			 <div class="fm-inf-menu">\
				  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
				  <p class="fm-inf-menu-desc"><%= mealMenuDescription %></p>\
				  <div class="fm-table-wrapper">\
					 <div class="fm-table-left">\
						  <table class="fm-table-inner table-responsive">\
								<thead class="hidden-mb">\
								</thead>\
								<tbody class="menu-info"></tbody>\
						  </table>\
					 </div>\
				  </div>\
				  <div class="fm-footnote-txt">\
						<em class="<%= foodIcon %> fm-footnote-logo"></em>\
						<span class="fm-dish-text"><%= specialityInfo %></span>\
						<input type="button" name="select" value="<%= inputVal %>" class="fm-select-btn right<%= selectClass %>" data-main-menu-target="<%= triggerId %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>" data-label="<%= mealMenuName %>">\
						<div class="btn-wrap-view-only<%= viewOnlyClass %>">\
							 <span class="btn-small view-only-bg view-box btn-view-only">VIEW ONLY</span>\
							 <span class="onboard">Order on board</span>\
						</div>\
				  </div>\
			 </div>\
		</article>';

	p.selectedALC = [];
    p.isALC = false;

	var init = function(){
		p.mealCatName = jsonData[0].mealMenuCategoryName;
		p.data = SIA.MealsSelection.public.json.flightSegment[segmentId].flightSector[sectorId];

		p.curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealService.mealServiceCode];

		p.isALC = mealServiceCode === 'ACL_110';

		paintInflightMenu();
	};

	var paintInflightMenu = function(){
		var targetInflightMenu = segmentId + '-' + sectorId + '-' + serviceId;

		// Check if block is not available
		if (typeof mealAvailData.mealNotReadyMsg != 'undefined') {
			targetClass = '';
		}

		var mealDesc = '';
		var mealTypeSub = '';
		if(p.isALC) {
			mealDesc = 'You can choose to pre-order one option from each meal service now, but there isn&#8217;t fixed time where they&#8217;ll be served the meal(s). Pre-ordering will help to guarantee that you&#8217;ll get some things that you would like to eat. You can also choose to order more from the various meal services when you are on the flight.';
			mealTypeSub = '';
        } else if (mealServiceCode == 'MNB_110') {
			mealDesc = 'You can choose to pre-order one option from each meal service now, but there isn&apos;t a fixed time where they&apos;ll be served the meal(s). Pre-ordering will help you guarantee that you&apos;ll get some things that you would like to eat. You can also choose to order more from the various meal services when you are on the flight.';
			mealTypeSub = '<h3 class="fm-title2-black">Your choice of '+mealService.mealServiceName.toLowerCase()+'</h3>';
        } else {
			mealDesc = 'Select your main course from our award-winning inflight menu, or choose from our Ethnic Set, featuring dishes inspired by local and regional cuisines.';
			mealTypeSub = '<h3 class="fm-title2-black">Your choice of '+mealService.mealServiceName.toLowerCase()+'</h3>';
        }

		// Paint Inflight Meal menu during painting
		// Get special attributes while we have indexes available
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = '';
		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
		}

		p.infm = $(_.template(p.inflightMainTpl, {
			'origin': p.data.departureAirportCode,
			'destination': p.data.arrivalAirportCode,
			'mealType': mealService.mealServiceName,
			'mealTypeSub': mealTypeSub,
			'id': SIA.MealsSelection.public.accordionTabIndex,
			'inflightTitle': jsonData[0].mealMenuCategoryName,
			'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
			'targetTrigger': SIA.MealsSelection.public.accordionTabIndex,
			'targetClass': targetInflightMenu,
			'viewOnlyMsg': viewOnlyMsg,
			'viewOnlyAlert': viewOnlyAlert,
			'mealDesc': mealDesc
		}));

		if(p.isALC) {
			paintALaCarte();
		}else {
			addInflightMeal(jsonData[0].mealSubcategory[0], p.infm.find('.item-wrapper'));
		}

		body.append(p.infm);
	};

	var paintALaCarte = function(){
		// Setup a la carte data
		var curPaxALCData = SIA.MealsSelection.public.data.paxList[paxId].selectedMeals[segmentId].selectedMeal['ACL_110'].aLaCarteData;

		if(curPaxALCData === null) {
			p.alcData = {};
			for (var j = 0; j < jsonData[0].mealSubcategory.length; j++) {
				var curData = jsonData[0].mealSubcategory[j];
				p.alcData[curData.mealSubCategoryCode] = {};

				p.alcData[curData.mealSubCategoryCode].mealCategoryCode = null;
				p.alcData[curData.mealSubCategoryCode].mealCode = null;
				p.alcData[curData.mealSubCategoryCode].mealMenuName = null;
				p.alcData[curData.mealSubCategoryCode].mealServiceCode = null;
				p.alcData[curData.mealSubCategoryCode].mealServiceName = null;
			}
		}else {
			p.alcData = curPaxALCData;
		}

		p.alcInput = $('<input type="button" class="hidden selected" name="A La Carte Selection" value="A La Carte Selection" data-isEating="true" data-inflightMenuselect-id="" data-segment="" data-sector="" data-service-id="" data-isalc="true" data-paxid="" data-meal-category="" data-meal-servicecode="" data-meal-menuname="" data-meal-mealcode="" />');

		var alaCarteWrap = $('<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content" /></div>');
		var alcWrap = alaCarteWrap.find('.accordion-wrapper-content');

		alaCarteWrap.append(p.alcInput);

		var wrap = p.infm.find('.item-wrapper');
		wrap.append(alaCarteWrap);
		wrap.addClass('a-la-carte');

		for (var i = 0; i < jsonData[0].mealSubcategory.length; i++) {
			var subCat = jsonData[0].mealSubcategory[i];

			// Set templates
			var accordBox = $('<div data-accordion="1" data-accordion-index="'+(i+1)+'" class="block-2 accordion accordion-box" />');
			var active = i > 0 ? '' : ' active';
			var alcAccordTrigger = $('<div data-accordion-trigger="1" class="accordion__control'+active+'">\
				<div class="fm-accordion-label">\
					<ul class="meal-menu">\
						<li class="meal-service-name">'+subCat.mealSubCategoryName+'</li>\
						<li class="selected-meal"></li>\
					</ul>\
	                <span class="fm-ico-text hidden">Select / Change</span>\
	                <em class="ico-point-d"></em>\
	              </div>\
	            </div>');
			var alcAccordCont = $('<div data-accordion-content="1" class="accordion__content inflight-alacarte-content" />');

			accordBox.append(alcAccordTrigger);
			accordBox.append(alcAccordCont);

			addInflightMeal(subCat, alcAccordCont);

			alcWrap.append(accordBox);
		}
	};

	var addInflightMeal = function(data, wrap){
		// Paint Inflight Meal menu during painting
		// Get special attributes while we have indexes available
		var inflMenuViewOnly = false;
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = '';
		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
			inflMenuViewOnly = true;
		}

		for (var l = 0; l < data.Meals.length; l++) {
			// cache meal item
			var mealItem = data.Meals[l];
			var menuTPL = mealItem.isSpecialMeal == "true" ? p.inflightMenuSpecialItemTpl : p.inflightMenuItemTpl;

			var specialMealName = '';

			if (typeof mealItem.specialMealName != 'undefined') specialMealName = mealItem.specialMealName;

			// Get current icon
			var foodIcon, footNote;
			if(typeof mealItem.specialityInfo !== 'undefined'){
				foodIcon = SIA.MealsSelection.public.inflightDishIcons[mealItem.specialityInfo.dishIconId];
				footNote = mealItem.specialityInfo.specialityFootNote;
			}

			var selectClass = '';
			var viewOnlyClass = ' hidden';
			if (typeof mealItem.isViewOnly != 'undefined' && mealItem.isViewOnly) {
				selectClass = ' hidden';
				viewOnlyClass = '';
			}

			if (p.curPaxData.mealCategoryCode == jsonData[0].mealCategoryCode && p.curPaxData.mealCode == mealItem.mealCode) {
				selectedClass = ' selected';
				inputVal = 'Remove';
			} else {
				selectedClass = '';
				inputVal = 'Select';
			}

			if(p.isALC && p.curPaxData.aLaCarteData !== null && p.alcData[data.mealSubCategoryCode].mealCode === mealItem.mealCode){
				selectedClass = ' selected';
				inputVal = 'Remove';
			}

			var menuItem = $(_.template(menuTPL, {
				'mealMenuName': mealItem.mealMenuName,
				'mealMenuDescription': mealItem.mealMenuDescription,
				'specialityInfo': footNote,
				'specialMealName': specialMealName,
				'selectClass': selectClass,
				'viewOnlyClass': viewOnlyClass,
				'foodIcon': foodIcon,
				'triggerId': (accordionTabIndex + 1),
				'segmentId': segmentId,
				'sectorId': sectorId,
				'paxId': paxId,
				'mealCategoryCode': jsonData[0].mealCategoryCode,
				'mealServiceCode': mealService.mealServiceCode,
				'mealCode': mealItem.mealCode,
				'mealMenuName': mealItem.mealMenuName,
				'selectedClass': selectedClass,
				'inputVal': inputVal
			}));
			menuItem.find('.fm-select-btn').attr('data-mealsubcat-code', data.mealSubCategoryCode);
			menuItem.find('.fm-select-btn').attr('data-mealsubcat-name', data.mealSubCategoryName);

			// Add click listener on item
			addMenuItemListener(menuItem);

			if (inflMenuViewOnly) {
				menuItem.find('input').hide();
				menuItem.find('.btn-wrap-view-only').hide();
			}

			// Check if ethnic meal
			if (mealItem.isEthnicMeal == 'true' && typeof mealItem.ethinicMeallist != 'undefined') {
				var tableCont = menuItem.find('.menu-info');
				for (var m = 0; m < mealItem.ethinicMeallist.length; m++) {
					var item = $('<tr><td><span>' + mealItem.ethinicMeallist[m].ethinicMealMenuName + '</span></td><td class="item-list"></td></tr>');

					for (var n = 0; n < mealItem.ethinicMeallist[m].itemInfo.length; n++) {
						var itemList = $('<span>' + mealItem.ethinicMeallist[m].itemInfo[n].ethinicMealMenuDescription + '</span>')
						item.find('.item-list').append(itemList);
					}

					tableCont.append(item);
				}
			}

			wrap.append(menuItem);
		}
	};

	var addMenuItemListener = function(menu) {
		menu.find('input').parent().parent().on({
			'click': function(e) {
				e.preventDefault();
				// Cache jq objects
				var t = $(this).find('.fm-select-btn');
				var cont = t.parent().parent().parent();

				// search and remove any previous selected meal
				SIA.MealsSelection.resetMeals(t, ['btcm', 'spm']);

				if(p.isALC){
					var curMealSubCatCode = t.attr('data-mealsubcat-code');
					var curMealSubCatName = t.attr('data-mealsubcat-name');
				}

				if (!cont.hasClass('selected')) {
					// reset all select items on wrapper
					cont.parent().find('.menu-item').removeClass('selected');
					cont.parent().find('input').val('Select');

					// Add selected class to the input and to the container
					t.addClass('selected');
					cont.addClass('selected');

					t.val('Remove');

					if(p.isALC){
                        p.alcData[curMealSubCatCode].mealCategoryCode = t.attr('data-meal-category');
                        p.alcData[curMealSubCatCode].mealCode = t.attr('data-meal-mealcode');
                        p.alcData[curMealSubCatCode].mealMenuName = t.attr('data-meal-menuname');
                        p.alcData[curMealSubCatCode].mealServiceCode = t.attr('data-meal-servicecode');
                        p.alcData[curMealSubCatCode].mealServiceName = curMealSubCatName;

	                    p.alcInput.attr('data-inflightMenuselect-id', t.data().inflightmenuselectId);
	                    p.alcInput.attr('data-paxid', t.data('paxid'));
	                    p.alcInput.attr('data-sector', t.data().sector);
	                    p.alcInput.attr('data-segment', t.data().segment);
	                    p.alcInput.attr('data-service-id', t.data().serviceId);
	                    p.alcInput.attr('data-meal-category', t.data().mealCategory);
	                    p.alcInput.attr('data-meal-servicecode', t.data().mealServicecode);
	                    p.alcInput.attr('data-alc-json', JSON.stringify(p.alcData));

						cont.parent().prev().find('.selected-meal').text('Selected');
                    }
				} else {
					cont.removeClass('selected');
					t.removeClass('selected');
					t.val('Select');

					if(p.isALC) {
                        p.alcData[curMealSubCatCode].mealCategoryCode = null;
                        p.alcData[curMealSubCatCode].mealCode = null;
                        p.alcData[curMealSubCatCode].mealMenuName = null;
                        p.alcData[curMealSubCatCode].mealServiceCode = null;
                        p.alcData[curMealSubCatCode].mealServiceName = null;

						cont.parent().prev().find('.selected-meal').text('');
                    }
				}

				if(p.isALC) {
					// Update the user data here
					var parentAccMealSummary = '';

                    _.each(p.alcData, function(v, k, l){
                        if(v.mealMenuName != null) {
                            // set the title info
                            parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName;
                        }
                    });

					SIA.MealsSelection.setMainMenuLbl(t, parentAccMealSummary.substring(4));
					SIA.MealsSelection.updateData(p.alcInput);
				}else {
					SIA.MealsSelection.setMainMenuLbl(t, t.data('label'));
					SIA.MealsSelection.updateData(t);
				}
			}
		});
	};

	var destroy = function(){
		p.infm.remove();
	};

	var oPublic = {
		init: init,
		public: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.InfantMeal = function(
	jsonData,
    paxData,
	origin,
	destination,
    accContent,
	accordionContentWrap,
    segmentId,
    sectorId,
    serviceId,
    paxId,
    mealServiceCode,
	mealService,
	accordionTabIndex,
	body,
	accordContainer) {

	var p = {};

	// Infant Meals TPL
	p.infantMeal = '<article class="promotion-item inflight-menu-item--1 menu-item <%= isSel %>">\
			<div class="fm-inf-menu int-prompt">\
				<span class="fm-inf-menu-select-badge">Selected</span>\
			  	<span class="special-badge <%= isSpecial %>">SPECIAL</span>\
			  	<h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
			  	<p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
			  	<div class="fm-footnote-txt">\
				  	<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				  	<span class="fm-dish-text"><%= specialityFootNote %></span>\
				  	<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
			  </div>\
			  <div class="confirmation-prompt hidden">\
  				  <p class="meal-sub-title-blue"> Selecting a <span><%= mealName %></span> will change your other infant meals selection from  from <%= origin %> to <%= destination %> to <span><%= mealName %></span> as well. </p>\
  				  <div class="button-group-1">\
  					  <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
  					  <a href="#" class="btn-2 btn-cancel">Cancel</a>\
  				  </div>\
  			</div>\
			</div>\
		</article>';
	p.infantMeals = [];
	p.curPrompt = null;
	p.currentMeal = null;

	var init = function(){
		// console.log(jsonData);
		p.infml = accContent.find('.inf-container');

		paintInfantMeals();
		addMealItemListener(p.infml);

		p.infmlChkBox = p.infml.parent().find('[data-checktype="not-eating"]');
		p.infmlChkBox.off().on({
			'change.infm': function() {
				var t = $(this);
				if(t.prop('checked') && p.currentMeal !== null) {
					unSelectMeal(p.currentMeal);
					SIA.MealsSelection.setMainMenuLbl(t, 'Inflight Menu');
					SIA.MealsSelection.updateData(t);
				}
			}
		});
	};

	var paintInfantMeals = function(){
		var mealsData = jsonData[0].mealSubcategory[0].Meals;
		for (var i = 0; i < mealsData.length; i++) {
			var meal = paintInfantMealItem(mealsData[i], i);
			p.infml.append(meal);
			addMealItemListener(meal);

			p.infantMeals.push(meal);
		}
	};

	var paintInfantMealItem = function(data, btnIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';
			specialityFootNote = data.specialityInfo.specialityFootNote;
			footIcon = SIA.MealsSelection.public.inflightDishIcons[data.specialityInfo.dishIconId];
		}

		// check for selected menu in data
		var selMealsel = SIA.MealsSelection.public.data.paxList[parseInt(paxId)].selectedMeals[parseInt(segmentId)].selectedMeal;
		selMeal = selMealsel[mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		return $(_.template(p.infantMeal, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': accordionTabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': segmentId,
			'sector': sectorId,
			'paxid': paxId,
			'mealServiceCode': mealServiceCode,
			'mealCatCode': jsonData[0].mealCategoryCode,
			'origin': origin,
			'destination': destination,
			'menuTarget': accordionTabIndex+1
		}));
	}

	var addMealItemListener = function(mc) {
		var allBtns = mc.find('.fm-select-btn');

		$.each(allBtns, function(index, btn) {
			$(btn).parent().parent().on('click', function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				// check if current meal card is selected again, then remove it
				if (parentEl.hasClass('selected')) {
					unSelectMeal(t);

					SIA.MealsSelection.setMainMenuLbl(t, 'Inflight Menu');
					SIA.MealsSelection.updateData(t);

					p.currentMeal = null;
				} else {
					// Check for special meals selected in other sectors in same leg
					var otherINFM = getOtherINFM(t);
					if(otherINFM.length > 0) {
						t.val('Selected');
						parentEl.addClass('int-prompt');
						parentEl.find('.confirmation-prompt').removeClass('hidden');

						// Remove any selected prompt
                        if(p.curPrompt !== null) {
                            p.curPrompt.prev().find('.fm-select-btn').val('Select');
                            p.curPrompt.addClass('hidden');
                            p.curPrompt.parent().removeClass('int-prompt');
                        }

						parentEl.find('.btn-cancel').off().on({
							'click': function(e){
								e.preventDefault();
								e.stopImmediatePropagation();

								$(this).off();

								t.val('Select');
								parentEl.removeClass('int-prompt');
								parentEl.find('.confirmation-prompt').addClass('hidden');
							}
						});

						parentEl.find('.btn-proceed').off().on({
							'click': function(e){
								e.preventDefault();
								e.stopImmediatePropagation();

								// remove listener to prevent double calls on otherSM
								$(this).off();

								// reset other meals before assigning selected class to current item
								SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

								selectMeal(t);

								p.currentMeal = t;

								// apply selection to other special meals in same leg
								selectSameMeal(t, otherINFM);

								// assign data
								SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
								SIA.MealsSelection.updateData(t);

								unselectChkBox();
							}
						});

						var prompt = t.parent().parent().find('.confirmation-prompt');
						p.curPrompt = prompt;
					}else {
						// Remove any selected prompt
                        if(p.curPrompt !== null) {
                            p.curPrompt.prev().find('.fm-select-btn').val('Select');
                            p.curPrompt.addClass('hidden');
                            p.curPrompt.parent().removeClass('int-prompt');

                            p.curPrompt = null;
                        }

						// search and remove any previous selected meal
						p.infml.find('.fm-select-btn').each(function(){
							unSelectMeal($(this));
						});

						selectMeal(t);

						p.currentMeal = t;

						SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
						SIA.MealsSelection.updateData(t);

						unselectChkBox();
					}
				}
			});
		});
	};

	var unselectChkBox = function(){
		p.infmlChkBox.prop("checked", false);
		p.infmlChkBox.parent().find('.not-eating-msg').addClass('hidden');
	};

	var selectSameMeal = function(meal, otherINFM){
		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.infml.length; i++) {
			var infm = SIA.MealsSelection.public.infml[i];

			infm.p.infml.find('.fm-select-btn').each(function(){
				var t = $(this);

				// check on each selected sector
				for (var j = 0; j < otherINFM.length; j++) {
					var curSector = otherINFM[j];

					// Unselect other selected special meals in same sector
					if( t.attr('data-segment') === meal.attr('data-segment')
					&& t.attr('data-meal-servicecode') === curSector.k ) {
						unSelectMeal(t);
					}

					// Find the selected special meal in current segment and select it
					if( t.attr('data-segment') === meal.attr('data-segment')
					&& t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')
					&& t.attr('data-meal-servicecode') === curSector.k ) {
						selectMeal(t);

						SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var selectMeal = function(meal){
		var parentEl = meal.parent().parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	var unSelectMeal = function(meal){
		var parentEl = meal.parent().parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
	};

	var getOtherINFM = function(meal){
		var otherSelectedINFM = [];
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];

        _.each(curSegment.selectedMeal, function(v, k, l){
            if(k !== mealServiceCode && v.mealCategoryCode === 'INF' && v.mealCode !== meal.attr('data-meal-mealcode')){
				otherSelectedINFM.push({'k': k, 'val': v });
			}
        });

        return otherSelectedINFM;
    };

	var destroy = function(){
		for (var i = 0; i < p.infantMeals.length; i++) {
			p.infantMeals[i].remove();
		}

		p.infantMeals = [];
		accContent.remove();
	};

	var oPublic = {
		init: init,
		p: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.BookTheCook = function(
	data,
	mealIcons,
	mealSel,
	bckAllMeal,
	segment,
	sector,
	paxid,
	mealServiceCode,
	jsonData,
	origin,
	destination,
	mealType,
	mainMenuTarget) {

	var p = {};

	p.BTCJson = data;
	p.mealIcons = mealIcons;
	mealSelEvent = mealSel;
	bckAllMealsEvent = bckAllMeal;
	p.segment = segment;
	p.sector = sector;
	p.paxid = paxid;
	p.mealServiceCode = mealServiceCode;
	p.btcMealCatCode = data[0].mealCategoryCode;
	p.data = jsonData;
	p.origin = origin;
	p.destination = destination;
	p.mealType = mealType;
	p.mainMenuTarget = mainMenuTarget;

	// declare templates
	p.BTCMain = '<aside class="popup popup-meals menu-list-btc popup--btc-menu-<%= targetClass %> hidden">';
	p.BTCCuisineCatMainWrapperTpl = '<div class="popup__inner">\
			 <div class="popup__content">\
				  <div class="book-the-cook">\
						<a href="#" class="popup__close">\
							 <span class="ui-helper-hidden-accessible">Close</span>&#xe60d; \
						</a>\
						<h3 class="light-box-title"><%= originDest %> <strong><%= mealType %></strong></h3>\
						<p class="light-box-btn-mb">\
							<a href="#" class="lightbox-btn-back" data-close="true">\
								<em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to all meals \
							</a>\
						</p>\
						<h3 class="fm-title-4--blue">Book the Cook</h3>\
						<p class="light-box-text">Select from our wide range of Book the Cook meals at least 24 hours before your departure, and enjoy world class dishes crafted by our International Culinary Panel.</p>\
						<p class="search-heading"> Have something in mind? </p>\
						<div class="static-content">\
							 <div class="static-details">\
								  <div class="btc-search">\
										<label for="input-search-1" class="hidden"></label>\
										<em class="ico-search search-btn"></em>\
										<span class="input-1">\
										  <input type="text" name="input-search-1" value="" placeholder="Search" />\
										</span>\
								  </div>\
								  <a href="#" class="clear-search hidden"><em class="ico-point-r"></em>Clear search result</a>\
								  <div class="tabs-component">\
										<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">\
											 <ul class="tab tab-list" role="tablist">\
												  <li class="more-item">\
														<a href="#">More<em class="ico-dropdown"></em></a>\
												  </li>\
											 </ul>\
											 <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">\
												  <label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span>\
												  <select class="more-tab" name="calcTabSelect">\
												  </select>\
											 </div>\
											 <div class="tab-wrapper btc-meal-list" id="tabWrapper">\
											 </div>\
										</div>\
								  </div>\
							 </div>\
						</div>\
				  </div>\
			 </div>\
		</div>';
	p.mealMain = '<div <%= isHidden %> data-tab-content="<%= index %>">';
	p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
	p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';
	p.BTCLoadMoreBtn = '<div class="load-more">\
		<input type="button" name="select" id="" value="Load more" class="load-more-btn">\
	 </div>';
	p.BTCCruisineCatListItemTpl = '<li class="tab-item <%= isActive %>" aria-selected="true" role="tab" data-tab-menu="<%= tabMenu %>">\
		  <a href="#"><%= menuName %></a>\
	 </li>';
	p.BTCMealItem = '<div data-meal-item class="fm-inf-menu <%= isSel %>">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="special-badge <%= isSpecial %>">SPECIAL</span>\
		  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
		  <p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
		  <div class="fm-footnote-txt">\
				<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				<span class="fm-dish-text"><%= specialityFootNote %></span>\
				<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
		  </div>\
	 </div>';
	p.searchRsltTpl = '<p class="search-result-msg"></p><div class="search-results btc-meal-list"></div>';

	var init = function(targetMenu, body) {
		p.btcm = paintBTCMeal(targetMenu);
		mealSelectionEvent(p.btcm);

		body.append(p.btcm);
	};

	var paintBTCMeal = function(targetMenu) {
		var mealSubcategoryJson = p.BTCJson[0].mealSubcategory;

		var bookTheCookEl = $(_.template(p.BTCMain, {
			'targetClass': targetMenu
		}));

		bookTheCookEl.append($(_.template(p.BTCCuisineCatMainWrapperTpl, {
			'originDest': p.origin + ' - ' + p.destination,
			'mealType': p.mealType
		})));

		paintContent(
			mealSubcategoryJson,
			5, // max number per meal list
			bookTheCookEl.find('.tab-list'),
			bookTheCookEl.find('.more-tab'),
			bookTheCookEl.find('#tabWrapper')
		);

		// attach events
		loadMoreBtnEvent(bookTheCookEl, mealSubcategoryJson, 5);
		searchBtnEvent(bookTheCookEl);

		var moreBtn = new SIA.MoreTab(bookTheCookEl);
		moreBtn.init();

		return bookTheCookEl;
	};

	var paintContent = function(data, maxPerList, tabListEl, moreTabEl, mealListEl) {
		for (var i = 0; i < data.length; i++) {
			// show only one menu on tab
			if (i < 1) {
				tabListEl.prepend($(_.template(p.BTCCruisineCatListItemTpl, {
					'isActive': ((i == 0) ? 'active' : ''),
					'menuName': data[i].mealSubCategoryName,
					'tabMenu': i
				})));
			}

			// add to more dropdown list
			moreTabEl.append(_.template(p.selOptionItemTpl, {
				'tabMenu': i,
				'menuName': data[i].mealSubCategoryName
			}));

			// create the Cuisine Category - Meal listing
			var mealListingWrapper = $(_.template(p.mealMain, {
				'isHidden': ((i != 0) ? ' class="hidden"' : ''),
				'index': i
			}));

			var cruisineCatMealListingJson = data[i].Meals;
			for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
				if (j < maxPerList) {
					mealListingWrapper.append(paintBTCEachMealItem(cruisineCatMealListingJson[j], j, i));
				}
			}

			// check if there is more than 5 items, then add a load more button
			if (cruisineCatMealListingJson.length > maxPerList) {
				mealListingWrapper.append(p.BTCLoadMoreBtn);
			}

			mealListEl.append(mealListingWrapper);
		}
	};

	var paintBTCEachMealItem = function(data, btnIndex, tabIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';
			specialityFootNote = data.specialityInfo.specialityFootNote;
			footIcon = p.mealIcons[data.specialityInfo.dishIconId];
		}

		// check for selected menu in data
		var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
		selMeal = selMealsel[p.mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		return $(_.template(p.BTCMealItem, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': tabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': p.segment,
			'sector': p.sector,
			'paxid': p.paxid,
			'mealServiceCode': p.mealServiceCode,
			'mealCatCode': p.btcMealCatCode,
			'menuTarget': p.mainMenuTarget
		}));
	}

	var loadMoreBtnEvent = function(el, data, maxPerList) {
		var loadMoreBtns = el.find('.load-more-btn');
		var content = '';

		for (var i = 0; i < loadMoreBtns.length; i++) {
			$(loadMoreBtns[i]).on('click', function() {
				var t = $(this);

				// get parent tab content
				var tabContentIndex = t.parent().parent().attr('data-tab-content');

				// get parent load more button
				var loadMoreBtnMain = t.parent();

				// get current index data
				var currentData = data[tabContentIndex].Meals;

				// compute if there is more than 5 meals left
				var leftMealsLength = currentData.length - maxPerList;
				var limitResult = (leftMealsLength >= maxPerList) ? true : false;
				for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
					if (k == (maxPerList - 1) && limitResult) {
						break;
					}

					var el = paintBTCEachMealItem(currentData[j], j, tabContentIndex);
					content += el[0].outerHTML;
				}

				// append before load more button
				$(content).insertBefore(loadMoreBtnMain);

				// check if there is more left data, then hide the load more button
				// get total list length on tab content
				var tabContentMain = t.parent().parent();
				var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
				if (tabContentLength == currentData.length) {
					loadMoreBtnMain.addClass('hidden');
				}

				// add meal selection event
				mealSelectionEvent(tabContentMain);
			});
		}
	};

	var searchJSON = function(data, val) {
		var result = [];
		$.each(data, function(i, value) {
			$.each(data[i].Meals, function(j, value) {
				var mealMenuName = value.mealMenuName.toLowerCase();
				var mealMenuDescription = value.mealMenuDescription.toLowerCase();

				val = val.toLowerCase().trim();
				if ((mealMenuName.indexOf(val) >= 0) || (mealMenuDescription.indexOf(val) >= 0)) {
					result.push({
						'cat': i,
						'item': j
					});
				}
			});
		});

		return result;
	};

	var searchBtnEvent = function(el) {
		el.find('.search-btn').on('click', function(e) {
			e.preventDefault();

			var t = $(this);
			var parentEl = t.closest('.static-details');

			// hide btc tabs list
			parentEl.find('.tabs-component').addClass('hidden');

			// remove search message and result first
			parentEl.parent().find('.search-result-msg').remove();
			parentEl.parent().find('.search-results').remove();

			// show clear result link
			parentEl.find('.clear-search').removeClass('hidden');
			var searchRsltEl = parentEl.parent().append($(p.searchRsltTpl));

			// fetch search value
			var val = t.closest('.btc-search').find('[name="input-search-1"]').val();
			var mealSubCat = p.BTCJson['0'].mealSubcategory;

			// search in JSON the searched value
			var result = searchJSON(mealSubCat, val);

			if (result.length >= 1) {
				// generate search result message
				searchRsltEl.find('.search-result-msg').html(result.length + ' search results for \'' + val + '\'');

				// generate meals blocks
				var resCatIds = [];
				$.each(result, function(i, meal) {
					var resultsEl = searchRsltEl.find('.search-results');

					if (resCatIds.indexOf(meal.cat) <= -1) {
						resCatIds.push(meal.cat);

						resultsEl.append(_.template(p.searchMealTitleTpl, {
							'mealName': mealSubCat[meal.cat].mealSubCategoryName
						}));
					}

					resultsEl.append(paintBTCEachMealItem(mealSubCat[meal.cat].Meals[meal.item], meal.item, 0));
				});

				mealSelectionEvent(searchRsltEl);
			} else {
				// if no search result found
				searchRsltEl.find('.search-result-msg').html('There are no meals matching \'' + val + '\'. There are no results. Amend your search criteria to try again.');
			}
		});

		el.find('.clear-search').on('click', function(e) {
			e.preventDefault();

			var t = $(this);
			var mainEl = t.closest('.static-content');
			var searchResultEl = mainEl.find('.search-results');
			var selMeal = mainEl.find('.search-results .selected .fm-select-btn').attr('data-meal-mealcode');

			// clear search results
			mainEl.find('.search-result-msg').remove();
			searchResultEl.remove();

			// clear search field value
			t.prev().find('[name="input-search-1"]').val('');

			// hide clear search result link
			t.addClass('hidden');

			// re-paint btc meal
			var btc = t.next();
			btc.removeClass('hidden');

			// search and remove any previous selected meal
			SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm']);

			// re-select meal
			var selMealEl = btc.find('.btc-meal-list [data-meal-mealcode="' + selMeal + '"]');
			selMealEl.attr('value', 'Remove');
			selMealEl.closest('.fm-inf-menu').addClass('selected');

			// re-init meal selection event
			mealSelectionEvent(btc);
		});
	};

	var mealSelectionEvent = function(el) {
		var allBtns = el.find('.fm-select-btn');

		$.each(allBtns, function(index, btn) {
			$(btn).parent().parent().on('click', function(e) {
				e.preventDefault();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				// check if current meal card is selected again, then remove it
				if (parentEl.hasClass('selected')) {
					// Add selected class to the input and to the container
					parentEl.removeClass('selected');
					t.removeClass('selected');

					// remove hidden class to show the selected badge
					badgeEl.addClass('hidden');

					t.attr('value', 'Select');

					SIA.MealsSelection.setMainMenuLbl(t, 'Inflight Menu');
					SIA.MealsSelection.updateData(t);
				} else {
					// search and remove any previous selected meal
					SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm']);

					// Add selected class to the input and to the container
					parentEl.addClass('selected');
					t.addClass('selected');

					// remove hidden class to show the selected badge
					badgeEl.removeClass('hidden');

					t.attr('value', 'Remove');

					SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
					SIA.MealsSelection.updateData(t);
				}
			});
		});
	};

	var destroy = function(){
		p.btcm.remove();
	};

	var oPublic = {
		init: init,
		paintBTCMeal: paintBTCMeal,
		public: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.SpecialMeal = function(
	data,
	mealIcons,
	mealSel,
	bckAllMeal,
	segment,
	sector,
	paxid,
	mealServiceCode,
	jsonData,
	origin,
	destination,
	mealType,
	mainMenuTarget,
	accordContainer) {

	var p = {};

	p.SPMJson = data;
	p.mealIcons = mealIcons;
	mealSelEvent = mealSel;
	bckAllMealsEvent = bckAllMeal;
	p.segment = segment;
	p.sector = sector;
	p.paxid = paxid;
	p.mealServiceCode = mealServiceCode;
	p.spmMealCatCode = data[0].mealCategoryCode;
	p.data = jsonData;
	p.origin = origin;
	p.destination = destination;
	p.mealType = mealType;
	p.mainMenuTarget = mainMenuTarget;

	// index all meal items
	p.allMealItems = [];
	p.curPrompt = null;

	// declare templates
	p.SPMMain = '<aside class="popup popup-meals menu-list-spm popup--spm-menu-<%= targetClass %> hidden">';
	p.SPMCuisineCatMainWrapperTpl = '<div class="popup__inner">\
			 <div class="popup__content">\
				  <div class="book-the-cook">\
						<a href="#" class="popup__close">\
							 <span class="ui-helper-hidden-accessible">Close</span>&#xe60d; \
						</a>\
						<h3 class="light-box-title"><%= originDest %> <strong><%= mealType %></strong></h3>\
						<p class="light-box-btn-mb">\
							<a href="#" class="lightbox-btn-back" data-close="true">\
								<em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to all meals \
							</a>\
						</p>\
						<h3 class="fm-title-4--blue">Special Meals</h3>\
						<p class="light-box-text">Select from our wide range of Book the Cook meals at least 24 hours before your departure, and enjoy world class dishes crafted by our International Culinary Panel.</p>\
						<div class="static-content">\
							 <div class="static-details">\
								  <div class="tabs-component">\
										<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">\
											 <ul class="tab tab-list" role="tablist">\
												  <li class="more-item">\
														<a href="#">More<em class="ico-dropdown"></em></a>\
												  </li>\
											 </ul>\
											 <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">\
												  <label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span>\
												  <select class="more-tab" name="calcTabSelect">\
												  </select>\
											 </div>\
											 <div class="tab-wrapper btc-meal-list" id="tabWrapper">\
											 </div>\
										</div>\
								  </div>\
							 </div>\
						</div>\
				  </div>\
			 </div>\
		</div>';
	p.mealMain = '<div <%= isHidden %> data-tab-content="<%= index %>">';
	p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
	p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';
	p.SPMLoadMoreBtn = '<div class="load-more"><input type="button" name="select" id="" value="Load more" class="load-more-btn"></div>';
	p.SPMCruisineCatListItemTpl = '<li class="tab-item <%= isActive %>" aria-selected="true" role="tab" data-tab-menu="<%= tabMenu %>"><a href="#"><%= menuName %></a></li>';
	p.SPMMealItem = '<div data-meal-item class="fm-inf-menu <%= isSel %>">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="special-badge <%= isSpecial %>">SPECIAL</span>\
		  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
		  <p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
		  <div class="fm-footnote-txt">\
				<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				<span class="fm-dish-text"><%= specialityFootNote %></span>\
				<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
		  </div>\
		  <div class="confirmation-prompt hidden">\
				<p class="meal-sub-title-blue"> Selecting <span><%= mealName %></span> will change your other special meals selection from <%= origin %> to <%= destination %> to <span><%= mealName %></span> as well. </p>\
				<div class="button-group-1">\
			  	<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
			  	<a href="#" class="btn-2 btn-cancel">Cancel</a>\
			</div>\
		  </div>\
	 </div>';
	p.searchRsltTpl = '<p class="search-result-msg"></p><div class="search-results spm-meal-list"></div>';

	var init = function(targetMenu, body) {
		p.spm = paintSPMMeal(targetMenu);
		mealSelectionEvent(p.spm);

		body.append(p.spm);
	};

	var getOtherSpecials = function(meal){
        var otherSelectedSM = [];
        var pax = SIA.MealsSelection.public.data.paxList[paxid];
        var curSegment = pax.selectedMeals[segment];

        _.each(curSegment.selectedMeal, function(v, k, l){
            if(k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode !== meal.attr('data-meal-mealcode')){
				otherSelectedSM.push({'k': k, 'val': v });
			}
        });

        return otherSelectedSM;
    };

	var paintSPMMeal = function(targetMenu) {
		var mealSubcategoryJson = p.SPMJson[0].mealSubcategory;

		var specialMealEl = $(_.template(p.SPMMain, {
			'targetClass': targetMenu
		}));

		specialMealEl.append($(_.template(p.SPMCuisineCatMainWrapperTpl, {
			'originDest': p.origin + ' - ' + p.destination,
			'mealType': p.mealType
		})));

		paintContent(
			mealSubcategoryJson,
			5, // max number per meal list
			specialMealEl.find('.tab-list'),
			specialMealEl.find('.more-tab'),
			specialMealEl.find('#tabWrapper')
		);

		// attach events
		loadMoreBtnEvent(specialMealEl, mealSubcategoryJson, 5);

		var moreBtn = new SIA.MoreTab(specialMealEl);
		moreBtn.init();

		return specialMealEl;
	};

	var paintContent = function(data, maxPerList, tabListEl, moreTabEl, mealListEl) {
		for (var i = 0; i < data.length; i++) {
			// show only one menu on tab
			if (i < 1) {
				tabListEl.prepend($(_.template(p.SPMCruisineCatListItemTpl, {
					'isActive': ((i == 0) ? 'active' : ''),
					'menuName': data[i].mealSubCategoryName,
					'tabMenu': i
				})));
			}

			// add to more dropdown list
			moreTabEl.append(_.template(p.selOptionItemTpl, {
				'tabMenu': i,
				'menuName': data[i].mealSubCategoryName
			}));

			// create the Cuisine Category - Meal listing
			var mealListingWrapper = $(_.template(p.mealMain, {
				'isHidden': ((i != 0) ? ' class="hidden"' : ''),
				'index': i
			}));

			var cruisineCatMealListingJson = data[i].Meals;
			for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
				if (j < maxPerList) {
					var meal = paintSPMEachMealItem(cruisineCatMealListingJson[j], j, i);
					mealListingWrapper.append(meal);
					p.allMealItems.push(meal)
				}
			}

			// check if there is more than 5 items, then add a load more button
			if (cruisineCatMealListingJson.length > maxPerList) {
				mealListingWrapper.append(p.SPMLoadMoreBtn);
			}

			mealListEl.append(mealListingWrapper);
		}
	};

	var paintSPMEachMealItem = function(data, btnIndex, tabIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';
			specialityFootNote = data.specialityInfo.specialityFootNote;
			footIcon = p.mealIcons[data.specialityInfo.dishIconId];
		}

		// check for selected menu in data
		var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
		selMeal = selMealsel[p.mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		return $(_.template(p.SPMMealItem, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': tabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': p.segment,
			'sector': p.sector,
			'paxid': p.paxid,
			'mealServiceCode': p.mealServiceCode,
			'mealCatCode': p.spmMealCatCode,
			'menuTarget': p.mainMenuTarget,
			'origin': p.origin,
			'destination': p.destination
		}));
	}

	var loadMoreBtnEvent = function(el, data, maxPerList) {
		var loadMoreBtns = el.find('.load-more-btn');
		var content = '';

		for (var i = 0; i < loadMoreBtns.length; i++) {
			$(loadMoreBtns[i]).on('click', function() {
				var t = $(this);

				// get parent tab content
				var tabContentIndex = t.parent().parent().attr('data-tab-content');

				// get parent load more button
				var loadMoreBtnMain = t.parent();

				// get current index data
				var currentData = data[tabContentIndex].Meals;

				// compute if there is more than 5 meals left
				var leftMealsLength = currentData.length - maxPerList;
				var limitResult = (leftMealsLength >= maxPerList) ? true : false;
				for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
					if (k == (maxPerList - 1) && limitResult) {
						break;
					}

					var el = paintSPMEachMealItem(currentData[j], j, tabContentIndex);
					content += el[0].outerHTML;
				}

				// append before load more button
				$(content).insertBefore(loadMoreBtnMain);

				// check if there is more left data, then hide the load more button
				// get total list length on tab content
				var tabContentMain = t.parent().parent();
				var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
				if (tabContentLength == currentData.length) {
					loadMoreBtnMain.addClass('hidden');
				}

				// add meal selection event
				mealSelectionEvent(tabContentMain);
			});
		}
	};

	var mealSelectionEvent = function(el) {
		var allBtns = el.find('.fm-select-btn');

		$.each(allBtns, function(index, btn) {
			$(btn).parent().parent().on('click', function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				// check if current meal card is selected again, then remove it
				if (parentEl.hasClass('selected')) {
					unSelectMeal(t);

					SIA.MealsSelection.setMainMenuLbl(t, 'Inflight Menu');
					SIA.MealsSelection.updateData(t);
				} else {
					// Check for special meals selected in other sectors in same leg
					var otherSM = getOtherSpecials(t);
					if(otherSM.length > 0) {
						t.val('Selected');

						// Remove any selected prompt
                        if(p.curPrompt !== null) {
                            p.curPrompt.prev().find('.fm-select-btn').val('Select');
                            p.curPrompt.addClass('hidden');
                            p.curPrompt.parent().removeClass('int-prompt');
                        }

						parentEl.addClass('int-prompt');
						parentEl.find('.confirmation-prompt').removeClass('hidden');

						parentEl.find('.btn-cancel').off().on({
							'click': function(e){
								e.preventDefault();
								e.stopImmediatePropagation();

								$(this).off();

								t.val('Select');
								parentEl.removeClass('int-prompt');
								parentEl.find('.confirmation-prompt').addClass('hidden');

								p.curPrompt = null;
							}
						});

						parentEl.find('.btn-proceed').off().on({
							'click': function(e){
								e.preventDefault();
								e.stopImmediatePropagation();

								// remove listener to prevent double calls on otherSM
								$(this).off();

								// reset other meals before assigning selected class to current item
								SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

								selectMeal(t);

								// apply selection to other special meals in same leg
								selectSameMeal(t, otherSM);

								// assign data
								SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
								SIA.MealsSelection.updateData(t);
							}
						});

						var prompt = t.parent().parent().find('.confirmation-prompt');
						p.curPrompt = prompt;
					}else {
						// Remove any selected prompt
                        if(p.curPrompt !== null) {
                            p.curPrompt.prev().find('.fm-select-btn').val('Select');
                            p.curPrompt.addClass('hidden');
                            p.curPrompt.parent().removeClass('int-prompt');

                            p.curPrompt = null;
                        }

						// search and remove any previous selected meal
						SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
						selectMeal(t);

						SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		});
	};

	var selectSameMeal = function(meal, otherSM){
		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.spml.length; i++) {
			var spml = SIA.MealsSelection.public.spml[i];

			spml.p.spm.find('.fm-select-btn').each(function(){
				var t = $(this);

				// check on each selected sector
				for (var j = 0; j < otherSM.length; j++) {
					var curSector = otherSM[j];

					// Unselect other selected special meals in same sector
					if( t.attr('data-segment') === meal.attr('data-segment')
					&& t.attr('data-meal-servicecode') === curSector.k ) {
						unSelectMeal(t);
					}

					// Find the selected special meal in current segment and select it
					if( t.attr('data-segment') === meal.attr('data-segment')
					&& t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')
					&& t.attr('data-meal-servicecode') === curSector.k ) {
						selectMeal(t);

						SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var selectMeal = function(meal){
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	var unSelectMeal = function(meal){
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
	};

	var destroy = function(){
		for (var i = 0; i < p.allMealItems.length; i++) {
			p.allMealItems[i].remove();
		}

		p.spm.remove();
		accordContainer.remove();
	};

	var oPublic = {
		init: init,
		paintSPMMeal: paintSPMMeal,
		p: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.MoreTab = function(el) {
	var p = {};

	p.el = el;

	var init = function() {
		evnt();
	};

	var evnt = function() {
		var moreTab = p.el.find('.more-tab');

		moreTab.on('change', function(e) {
			e.preventDefault();

			var t = $(this);
			var tabIndex = t.val();
			var tabContentEl = moreTab.parent().next();

			t.parent().prev().find('.tab-item a').html(t.find('option').eq(tabIndex).text())

			$.each(tabContentEl.find('[data-tab-content]'), function(i, value) {
				$(value).addClass('hidden');
			});

			// show the current selected tab index content
			tabContentEl.find('[data-tab-content]').eq(tabIndex).removeClass('hidden');
		});
	};

	var oPublic = {
		init: init
	};

	return oPublic;
};

SIA.Accordion = function(){
	var vars = SIA.global.vars;
	var win = vars.win;
	var body = vars.body;
	var htmlBody = $('html, body');
	var timeAnimate = 400;
	// This function use for init accordion
	var initAccordion = function(){
		var contentWrapper = $('[data-accordion-wrapper=1]');
		var	firstTriggerAccordion = '[data-accordion-trigger=1]',
			secondTriggerAccordion = '[data-accordion-trigger=2]',
			firstContentAccordion = '[data-accordion-content=1]',
			secondContentAccordion = '[data-accordion-content=2]';

		if(contentWrapper.find('[data-accordion=1]').length){
			contentWrapper.each(function(){
				var self = $(this);
				var openAllAccordion = $('.open-all-btn', self);
				var $firstTriggerAccordion = $(firstTriggerAccordion, self),
					$secondTriggerAccordion = $(secondTriggerAccordion, self);
				var animateHtmlBody = function(trigger){
					if(trigger.offset().top === win.scrollTop()){
						return;
					}
					htmlBody.stop().animate({
						scrollTop: trigger.offset().top
					}, timeAnimate, function() {
						// fix issue for set window.location.hash faqs pages
						if (body.hasClass('faqs-pages')) {
							if (trigger.hasClass('open-all-btn')) {
								window.location.hash = '';
							}
							else {
								trigger.trigger('setHash.faqsPages');
							}
						}
					});
				};
				var scrollTop = -1;
				var clearTimerUpdate = null;
				var dT = 300;

				var autoUpdateScrollTop = function(trigger){
					clearTimerUpdate = setTimeout(function(){
						if(scrollTop === trigger.offset().top){
							animateHtmlBody(trigger);
							scrollTop = -1;
							clearTimeout(clearTimerUpdate);
						}
						else{
							scrollTop = trigger.offset().top;
							autoUpdateScrollTop(trigger);
						}
					}, dT);
				};

				openAllAccordion.off('click.openAllAccordion').on('click.openAllAccordion', function(e){
					e.preventDefault();

					if(openAllAccordion.hasClass('open')){
						if($firstTriggerAccordion.length){
							$firstTriggerAccordion.siblings(firstContentAccordion).stop().slideUp(400);
						}
						if($secondTriggerAccordion.length){
							$secondTriggerAccordion.filter('.active').removeClass('active').siblings(secondContentAccordion).slideUp(400);
						}
						openAllAccordion.removeClass('open active').html(L10n.accordion.open);
					}
					else{
						if($firstTriggerAccordion.length){
							$firstTriggerAccordion.not('.active').siblings(firstContentAccordion).stop().slideDown(400);
							$firstTriggerAccordion.filter('.active').removeClass('active');
						}
						if($secondTriggerAccordion.length){
							$secondTriggerAccordion.not('.active').addClass('active').siblings(secondContentAccordion).slideDown(400);
						}
						animateHtmlBody(openAllAccordion);
						openAllAccordion.addClass('open active').html(L10n.accordion.collapse);
					}
				});

				$firstTriggerAccordion.off('beforeAccordion.refeshOpenAll').on('beforeAccordion.refeshOpenAll', function(){
					var self = $(this);
					if(openAllAccordion.hasClass('open')){
						$firstTriggerAccordion.not(self).addClass('active');
						openAllAccordion.removeClass('open').html(L10n.accordion.open);
						autoUpdateScrollTop(self);
					}
				});

				if(self.data('accordion')){
					self.accordion('refresh');
				}
				else{
					self.accordion({
						wrapper: '[data-accordion-wrapper-content=1]',
						triggerAccordion: firstTriggerAccordion,
						contentAccordion: firstContentAccordion,
						activeClass: 'active',
						duration: timeAnimate,
						beforeAccordion: function(trigger, content){
							var isAddonPage = body.is('.add-ons-1-landing-page') || body.is('.mp-add-ons-page'),
									sld = isAddonPage === true ? trigger.find('.slides') : content.find('.slides');

							if(sld.length){
								if(!sld.find('.slide-item').width()){
									var slideWidth = (sld.closest('.accordion__content').parent().width() / 2) - 50;
									sld.find('.slick-track').width(100000);
									sld.find('.slide-item').width(slideWidth);
								}
								// sld.slickGoTo(sld.slickCurrentSlide());
								sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));
							}

							// if (trigger.hasClass('active') && openAllAccordion.hasClass('open')){
							// 	openAllAccordion.removeClass('open').text(L10n.accordion.open);
							// 	$firstTriggerAccordion.not(trigger).trigger('click.accordion');
							// }
						},
						afterAccordion: function(trigger){
							if(trigger.hasClass('active') && !body.is('.promotion-enhancement')){
								animateHtmlBody(trigger);
							}
						}
					});
				}
			});

		}
		var accordionContentInfo = $('[data-accordion-wrapper=2]');
		if(accordionContentInfo.find('[data-accordion=2]').length){

			var animateHtmlBody = function(trigger){
				if(trigger.offset().top === win.scrollTop()){
					return;
				}
				htmlBody.stop().animate({
					scrollTop: trigger.offset().top
				}, timeAnimate);
			};

			if(accordionContentInfo.data('accordion')){
				accordionContentInfo.accordion('refresh');
			}
			else{
				accordionContentInfo.accordion({
					wrapper: '[data-accordion-wrapper-content=2]',
					triggerAccordion: secondTriggerAccordion,
					contentAccordion: secondContentAccordion,
					activeClass: 'active',
					duration: timeAnimate,
					beforeAccordion: function(trigger, content){
						var sld = trigger.find('.slides');
						if(sld.length){
							if(sld.find('.slide-item').width() < sld.closest('.accordion__control').width()){
								var slideWidth = sld.closest('.accordion__control').width();
								sld.find('.slick-track').width(100000);
								sld.find('.slide-item').width(slideWidth);

							}
							sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));

						}

					},
					afterAccordion: function(trigger){
						if(trigger.hasClass('active')){
							animateHtmlBody(trigger);
						}

					}
				});
			}
		}
	};

	var oPublic = {
		init: initAccordion
	};

	return oPublic;
};

// check if underscore is loaded
var waitForUnderscore = function() {
	setTimeout(function() {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.MealsSelection.init();
		}
	}, 100);
};

$(function() {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.MealsSelection.init();
});
