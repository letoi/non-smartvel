/**
 * @name SIA
 * @description Define global kf-personal-detail function
 * @version 1.0
 */
SIA.KFPersonalDetail = function(){
	
	var global = SIA.global;
	var config = global.config;
	var formPersonalDetail = $('[data-form-non-editable]'),
			btnCancel = $('[data-button-cancel]'),
			btnDelete = $('[data-button-delete]'),
			btnShowPIN = $('[data-btn-show-pin]'),
			lnkServiceFee = $('[data-link-service-fee]'),
			popupDeleteKf = $('[data-popup-delete-nominee]'),
			popupServiceFees = $('[data-popup-service-fees]'),
			btnFirstSubmit = $('#btn-sumit-1'),
			btnLastSubmit = $('#btn-sumit-2'),
			btnEditSec = $('#btn-sumit-3'),
			hiddenRowEmpty = $('[data-check-empty-value]'),
			currentPinEl = $('[data-current-pin]'),
			redemtionNomineeEl = $('[data-redemtion-nominee]'),
			btnAddNominee = $('[data-add-nominee]'),
			checkPinEl = $('[data-check-pin]'),
			rowQuestionDisabled = $('[data-row-question]'),
			resGender = $('[data-gender-resource]'),
			refGender = $('[data-gender-reference]'),
			ariaPin = $('#aria-update-pin'),
			ariaSec = $('#aria-update-sec');

	var correctDate = function(){
		var target = $('[data-rule-validatedate]');
		var detectDate = function(d, m, y, el){
			var nd = new Date();
			var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
			if(d > getLastDate.getDate()){
				el.val(getLastDate.getDate());
				el.closest('[data-customselect]').customSelect('refresh');
			}
		};
		target.each(function(){
			var self = $(this);
			var data = self.data('rule-validatedate');
			var date = $(data[0]);
			var month = $(data[1]);
			var year = $(data[2]);
			// console.log(date, month, year);

			date.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
			month.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
			year.closest('[data-customselect]').off('afterSelect.correctDate').on('afterSelect.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
		});
	};

	correctDate();

	var resetFieldData = function(el) {
		var parentEl = el.closest('.autocomplete');
		if(parentEl.data('autocomplete')) {
			parentEl.find('[autocomplete]').data('uiAutocomplete')._value(
				el.find('[selected]').length ? el.find(':selected').data('text') : ''
			);
		} else {
			el.closest('.custom-select').customSelect('refresh');
		}
	};

	var foreachField = function(isDisabled, formEl, isResetData) {
		isDisabled = isDisabled || false;
		formEl = formEl || formPersonalDetail;
		isResetData = isResetData || false;
		formEl.find(':input').each(function() {
			var that = $(this);
			if(this.type !== 'submit') {
				if(isResetData && that.is('select')) {
					resetFieldData(that);
				}
				if(!that.is(btnDelete) && !that.hasClass('non-active')) {
					that.prop('disabled', isDisabled);
				}
			} else {
				that.val(isDisabled ? L10n.kfProfiles.edit : L10n.kfProfiles.save);
			}
		});
	};

	var initAjax = function(url, data, type, callback) {
		type = type || 'json';
		callback = callback || $.noop();
		if(url && data) {
			$.ajax({
				url: url,
				type: global.config.ajaxMethod,
				dataType: type,
				data: data,
				success: function(res) {
					if(res) {
						callback();
					}
				},
				error: function(jqXHR, textStatus) {
					console.log(textStatus);
				}
			});
		}
	};

	var hiddenRowInfo = function() {
		if(hiddenRowEmpty.length) {
			hiddenRowEmpty.each(function() {
				var that = $(this);
				if(!that.find(':input').val()) {
					that.addClass('hidden');
				} else {
					that.removeClass('hidden');
				}
			});
		}
	};

	var detectGender = function(resourceElement, referenceElement) {
		if(!resourceElement.length || !referenceElement.length) {
			return;
		}
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	detectGender(resGender, refGender);

	if(!popupDeleteKf.data('Popup')){
		popupDeleteKf.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}

	if(!popupServiceFees.data('Popup')){
		popupServiceFees.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}

	formPersonalDetail.each(function() {
		$(this).validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			invalidHandler: global.vars.invalidHandler,
			onfocusout: global.vars.validateOnfocusout,
			submitHandler: function() {
				var that = $(this.currentForm);
				if(that.hasClass('non-editable')) {
					var screenH;
					if(btnLastSubmit.length) {
						screenH = btnLastSubmit.offset().top - global.vars.win.scrollTop();
					}
					if(hiddenRowEmpty.length) {
						hiddenRowEmpty.removeClass('hidden');
					}
					if(currentPinEl.length) {
						if(that.find(currentPinEl).length) {
							currentPinEl.val('');
						}
					}
					that.removeClass('non-editable');
					checkPostcode();
					foreachField(false, that);
					detectGender(resGender, refGender);
					that.data('validatedOnce', false);
					if(!that.data('initTitleDropdown')) {
						SIA.initPersonTitles();
						that.data('initTitleDropdown', true);
					}
					if(btnLastSubmit.length && btnFirstSubmit.length) {
						if(global.vars.win.scrollTop() > btnFirstSubmit.offset().top){
							global.vars.win.scrollTop(btnLastSubmit.offset().top - screenH);
						}
					}
				} else {
					var validator = that.data('validator');
					if(validator.checkForm()) {
						if(that.data('formSubmitAjax')) {
							initAjax(config.url.kfMessageMarkJSON, that.find(':input').serialize(), null, function() {
								that.addClass('non-editable');
								foreachField(true, that);
								that.find('[data-pin]').each(function() {
									var pinEle = $(this);
									if(pinEle.data('pin')) {
										pinEle.attr('type', 'password');
										btnShowPIN.removeClass('active').text(L10n.kfProfiles.showPin);
									}
								});

								if(that.find('#aria-update-pin').length > 0) {
									$('#aria-update-pin').text(L10n.wcag.updateSuccess);
								}

								if(that.find('#aria-update-sec').length > 0) {
									$('#aria-update-sec').text(L10n.wcag.updateSuccess);
								}
							});
							return false;
						}
						return true;
					}
				}
				return false;
			}
		});
	});

	if(redemtionNomineeEl.length && redemtionNomineeEl.length >= 5) {
		btnAddNominee.addClass('disabled').on('click', function(e){ e.preventDefault(); });
	}

	if(formPersonalDetail.hasClass('non-editable')) {
		hiddenRowInfo();
		foreachField(true);
	}

	if(checkPinEl.length && rowQuestionDisabled.length) {
		var isDelay = false,
				minLength = checkPinEl.data('ruleMinlength');
		checkPinEl.off('keypress.checkSecurityPin').on('keypress.checkSecurityPin', function() {
			if(!isDelay) {
				isDelay = true;
				setTimeout(function() {
					if(!checkPinEl.hasClass('error') && checkPinEl.val().length >= minLength) {
						initAjax(config.url.sqcCheckPIN, {pin: checkPinEl.val()}, 'json', function() {
							rowQuestionDisabled.find('.grid-disabled').removeClass('disabled');
							rowQuestionDisabled.find(':input').removeAttr('disabled');
						});
					}
					isDelay = false;
				}, 1000);
			}
		});
	}

	$('#country').off('blur.editForm').on('blur.editForm', function(){
		checkPostcode();
	});

	function checkPostcode(){
		var countryVal = $('#country').val();
		var postCode = $('#postcode');
		if(countryVal === 'Singapore'){
			postCode.attr('data-rule-required', 'true');
			postCode.attr('data-msg-required', 'Singapore must have a postcode!');
		}else{
			postCode.removeAttr('data-rule-required');
			postCode.removeAttr('data-msg-required');
		}
	}

	btnCancel.off('click.cancelEdit').on('click.cancelEdit', function() {
		var currentForm = $(this).closest('form'),
				pinField = currentForm.find('[data-pin]'),
				autocompleteField = currentForm.find('[data-autocomplete]'),
				screenH;
		if(btnLastSubmit.length) {
			screenH = btnLastSubmit.offset().top - global.vars.win.scrollTop();
		}
		hiddenRowInfo();
		pinField.attr('type','password');
		if(currentForm.find(btnShowPIN).length) {
			btnShowPIN.removeClass('active').text(L10n.kfProfiles.showPin);
		}
		currentForm.get(0).reset();
		currentForm.data('validator').resetForm();
		foreachField(true, currentForm, true);
		currentForm.addClass('non-editable');
		if(btnLastSubmit.length && btnFirstSubmit.length) {
			if(global.vars.win.scrollTop() > btnFirstSubmit.offset().top){
				global.vars.win.scrollTop(btnLastSubmit.offset().top - screenH);
			}
		}
		if(autocompleteField.length) {
			autocompleteField.removeClass('default');
		}
		if(currentForm.find(checkPinEl).length && currentForm.find(rowQuestionDisabled).length) {
			rowQuestionDisabled.find('.grid-disabled').addClass('disabled');
			rowQuestionDisabled.find(':input').attr('disabled', 'disabled');
		}
	});

	btnShowPIN.each(function(){
		var self = $(this),
			isShow = false;

		self.off('click.showPIN').on('click.showPIN', function(e) {
			if(!self.hasClass('active')) {
				self.addClass('active').text(L10n.kfProfiles.hidePin);
				isShow = true;
			} else {
				self.removeClass('active').text(L10n.kfProfiles.showPin);
				isShow = false;
			}
			self.closest('form').find('[data-pin]').each(function() {
				var that = $(this);
				if(that.data('pin')) {
					that.attr('type', isShow ? 'text' : 'password');
				}
			});
			e.preventDefault();
		});
	});

	btnDelete.off('click.deleteNominee').on('click.deleteNominee', function() {
		popupDeleteKf.Popup('show');
	});

	lnkServiceFee.off('click.deleteNominee').on('click.deleteNominee', function(e) {
		e.preventDefault();
		popupServiceFees.Popup('show');
	});

	var wcag = function(){
		var pin = $('#update-pin'),
				sec = $('#update-sec');

		if(ariaPin.length === 0) {
			pin.prepend('<span class="ui-helper-hidden-accessible" id="aria-update-pin" aria-live="assertive"></span>')
		};

		if(ariaSec.length === 0) {
			sec.prepend('<span class="ui-helper-hidden-accessible" id="aria-update-sec" aria-live="assertive"></span>')
		};

		btnFirstSubmit.off('click.security').on('click.security', function(e){
			var _self = $(this);
			setTimeout(function(){
				_self.parents('#update-pin').find('input:first').focus();
			}, 100);

		});

		btnEditSec.off('click.security').on('click.security', function(e){
			var _self = $(this);
			setTimeout(function(){
				_self.parents('#update-sec').find('input:first').focus();
			}, 100);

		});
	};
	wcag();

	$('[data-print-card]').off('click.memberCard').on('click.memberCard', function (e) {
		e.preventDefault();

    var width = $(window).width()*0.8;
    var height = $(window).height()*0.8;

    var content = '<!DOCTYPE html>' +
                  '<html>' +
                  $('head')[0].innerHTML +
                  '<body onload="window.focus(); window.print(); window.close();">' +
                  '<div style="text-align: center; margin-top: 30px;"><img src="' + $(this).data('print-card') + '" style="width: auto; height: auto; max-width: 321px; max-height: 204px;" /></div>' +
                  '</body>' +
                  '</html>';

    var options = "toolbar=no,location=no,directories=no,menubar=no,scrollbars=yes,width=" + width + ",height=" + height;
    var printWindow = window.open('', 'print', options);

    printWindow.document.open();
    printWindow.document.write(content);
    printWindow.document.close();
    printWindow.focus();

	});

	var redirectCancelLinkListener = function() {
		$("[data-2fa-cancel-link]").off().on("click", function(evt) {
			evt.preventDefault();
			
			var redirectLink = $(this).data("2fa-cancel-link");
			window.location.href = redirectLink;
		});
	};

	redirectCancelLinkListener();

	if($('body').hasClass('personal-details-page')){
		var anchorResend = $('#form--login').find('a.code-resend');
		var inputOTP = $('#membership-1');
		inputOTP.attr('maxlength','6');
		var buttonVerify = $('input#submit-1');
		var removeError = function(){
			if($('#form--login').find('span.security-input-form').hasClass('form-error')){
				$('#form--login').find('span.security-input-form').removeClass('form-error');
				$('#form--login').find('span.left').addClass('hide-span');
				inputOTP.removeClass('input-text-error');
			}
			inputOTP.val("");
		}

		var addError = function(){
			if(!$('#form--login').find('span.security-input-form').hasClass('form-error')){
				$('#form--login').find('span.security-input-form').addClass('form-error');
				$('#form--login').find('span.left').removeClass('hide-span');
				$('#form--login').find('span.left').text("Enter a valid OTP");
				inputOTP.addClass('input-text-error');
			}
		}
		var validateInput = function(){
			if(inputOTP.val().trim().length != 6){
				addError();
				return false;
			}else{
				return true;
			}
		}
		inputOTP.click( removeError );
		inputOTP.on('change', validateInput);
		inputOTP.on('keyup',function(){
			if(isNaN(this.value[this.value.length-1])){
				this.value = this.value.slice(0,-1);
				if(this.value == ''){
					$('#form--login').find('a.ico-cancel-thin').css("display","none");
				}
			}
		})
		anchorResend.click( removeError );
		buttonVerify.click(function(){
			if (!inputOTP.val().trim().length){
				addError();
			}else{
				if(!$('#form--login').find('span.security-input-form').hasClass('form-error')){
					if(validateInput()){
						document.getElementById("form--login").submit();
					}
				}
			}	
		});
	}
};
