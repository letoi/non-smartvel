SIA.GigyaLogin = function() {
	var p = {};

	p.containerName = 'loginContainer';
	p.containerId = $('#' + p.containerName);
	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	var init = function() {
		gigya.accounts.showScreenSet({
	    screenSet:'RU-RegistrationLogin',
	    containerID: p.containerName,
	    startScreen: "gigya-login-screen",
	    onAfterScreenLoad: function() {
	    	evnts();
	    }
	  });
	};

	var evnts = function() {
		//  find email field attached by gigya, save html and remove
    	p.emailInput = p.containerId.find('.gigya-login-form .user-email-address input');

    	// remove clear button
    	p.emailInput.parent().find('.add-clear-text').remove();

    	attachEvnt({
    		el: p.emailInput,
    		keypress: function() {
	    		var t = $(this);

	    		inputValidator(
					t,
					validateEmail(t.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
	    	},
	    	blur: function() {
	    		var t = $(this);

	    		if (t.val() == '') {
	    			inputValidator(
						t,
						validateEmpty(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	    		} else {
	    			inputValidator(
						t,
						validateEmail(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	    		}
	    	}
    	});

    	p.passInput = p.containerId.find('.gigya-login-form .user-password input');
 		var togglePass = new SIA.TogglePassword(
 			p.passInput.parent(),
 			p.passInput);
 		togglePass.init();

 		attachEvnt({
 			el: p.passInput,
 			blur: function() {
	 			var t = $(this);

	 			if(t.val() == '') {
	 				inputValidator(
						t,
						validateEmpty(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	 			} else {
	 				inputValidator(
						t,
						validatePassword(t.val().length),
						p.gigyaErrClss,
						p.gigyaValidClss);
	 			}
	 		}
 		});

 		// remove gigya error message element
 		var gigyaErrorMsgEl = p.containerId.find('.gigya-login-form  .gigya-error-display');
 		p.containerId.find('.gigya-login-form  .gigya-error-display').remove();

 		// move gigya error message element to top
 		p.containerId.parent().find('.form-group.form-group--tooltips').before(gigyaErrorMsgEl);
 		gigyaErrorMsgEl.prepend('<em class="ico-close-round-fill"></em>');
	};

	var attachEvnt = function(obj) {
		var evnts = ['keypress', 'blur', 'click'];

		if (typeof obj != 'undefined' && typeof obj.el != 'undefined') {
			for (var i = 0, iLen = evnts.length; i < iLen; i++) {
				var evntType = evnts[i];
				var evntFunc = obj[evntType];

				if (typeof evntFunc != 'undefined') {
					obj.el.on(evntType, evntFunc);
				}
			}
		}
	};

	var validateEmail = function(val) {
		var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

		return {
			state: emailReg.test(val),
			msg: 'Please enter a valid email address.'
		};
	};

	var validateEmpty = function(val) {
		return {
			state: (val != '') ? true : false,
			msg: 'This field is required'
		};
	};

	var validatePassword = function(val) {
		return {
			state: (val >= 6) ? true : false,
			msg: 'This field requires atleast 6 digits.'
		};
	};

	var addErrState = function(el, inputclss, msgClss, errMsg) {
		el.addClass(inputclss);

		var msgEl = el.next();
		msgEl.addClass(msgClss);
		msgEl.html(errMsg);
	};

	var addValidState = function(el, inputclss, msgClss) {
		el.addClass(inputclss);
	};

	var removeInputState = function(el, inputclss, msgClss) {
		el.removeClass(inputclss);

		var msgEl = el.next()
		msgEl.removeClass(msgClss);
		msgEl.html('');
	};

	var inputValidator = function(el, result, errClss, validClss) {
		if (!result.state) {
			addErrState(el, errClss.input, errClss.msg, result.msg);
		} else {
			removeInputState(el, errClss.input, errClss.msg);
			addValidState(el, validClss);
		}
	};

	return {
		init: init,
		p: p
	};
}();

SIA.TogglePassword = function(appendEl, input) {
	var p = {};

	p.appendEl = appendEl;
	p.input = input;
	p.iconShow  = 'images/show-eye.png';
 	p.iconHide = 'images/hidden-eye.png';
 	p.icon = '<img data-togglePass class="pin-eye-icon" src="">';

	var init = function() {
		p.appendEl.css('position', 'relative');

		var icon = $(p.icon);
		p.appendEl.append(icon);
		icon.attr('src', p.iconShow);

		evnt(p.appendEl.find('[data-togglePass]'), p.input);
	};

	var evnt = function(el, input) {
		el.on('click', function() {
			var inputType = input.attr('type');
			var iconEl = input.parent().find('[data-togglePass]');

			if (inputType == 'password') {
				input.attr('type', 'text');
				iconEl.attr('src', p.iconHide);
			} else if (inputType == 'text') {
				input.attr('type', 'password');
				iconEl.attr('src', p.iconShow);
			}
		});
	};

	return { init: init };
};

SIA.RadioTab = function() {
	var p = {};

	p.gigyaEl = $('#gigyaContainer');
	p.gigyaTabs = 'data-radiotab';
	p.gigyaContents = 'data-radiotab-content';

	var init = function() {
		// show default checked radio
		showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + p.gigyaEl.find('[checked="checked"]').attr(p.gigyaTabs) + '"]'));

		evnt(p.gigyaEl.find('[' + p.gigyaTabs + ']'));
	};

	var evnt = function(el) {
		el.on('click', function() {
			var t = $(this);

			// hide all contents
			p.gigyaEl.find('[' + p.gigyaContents + ']').addClass('hidden');

			// show current select tab content
			showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + t.attr(p.gigyaTabs) + '"]'));
		});
	};

	var showContent = function(el) {
		el.removeClass('hidden');
	};

	return {
		init: init,
		p: p
	};
}();

SIA.GigyaLogin.init();
SIA.RadioTab.init();
