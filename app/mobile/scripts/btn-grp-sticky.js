var SIA = SIA || {};

SIA.BtnGroupSticky = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;

	var p = {};
 // btn-grp-sticky
	var checkPosition = function(){
		if($(window).scrollTop() > p.defaultTop && !p.stickyGrp.hasClass('btn-grp-sticky')) {
			p.stickyGrp.addClass('btn-grp-sticky');
		}

		if($(window).scrollTop() < p.defaultTop && p.stickyGrp.hasClass('btn-grp-sticky')) p.stickyGrp.removeClass('btn-grp-sticky');

		p.animKeyFrame = requestAnimationFrame(checkPosition);
	};

	var init = function(){
		p.stickyGrp = $('[data-sticky-group]');
		p.defaultTop = p.stickyGrp.position().top + p.stickyGrp.outerHeight() * 2.5;

		p.animKeyFrame = requestAnimationFrame(checkPosition);
	}

	var oPublic = {
		init: init
	};

	return oPublic;
}();

SIA.BtnGroupSticky.init();
