SIA.MPManageBooking = function () {

	var templateDate = {
		bookingOverviewData: {
			flights: [],
			destinationImage: "",
			rules: {}
		},
		passengerAddOnsData: {},
		tripAddon: {
			tripAddons: {
				insurance: {
					passengers: []
				},
				agodaHotel: {
					hotelRating: '0'
				},
			}
		},
		openFlights: "",
		activePage: "your_booking",
		carousellPage: 1,
		direction: "left",
		disabled: false,
		noKrisFlyer: [],
		platform: "mobile"
	}

	var init = function () {
		Vue.use(VTooltip);
		Vue.component('vue-slide-up-down', VueSlideUpDown);
		Vue.component("manage-booking", {
			template: "#mp-manage-booking-template",
			data() {
				return templateDate
			},
			methods: {
				timeParser: timeParser,
				addFlight: addFlight,
				flightIsActive: flightIsActive,
				isNotLast: isNotLast,
				changeActivePage: changeActivePage,
				nextImage: nextImage,
				prevImage: prevImage,
				changeTile: changeTile,
				getNoKrisFlyerCount: getNoKrisFlyerCount
			},
			computed: {
				background: getBackGround
			},
			mounted: onMount,
			created: onCreated,
		});

		new Vue({
			el: "#app-container",
			data: {
				flightSegmentsLightBox: []
			},
			methods: {
				timeParser: timeParser
			},
			mounted: function () {
				setTimeout(function(){
					initTriggerPopup();
				}, 1000);
			}
		});

		$('.add-ons-for-your-trip-content_img').slick({
            infinite: false,
            dots: true,
            arrows: false
		});

		
	};

	var initTriggerPopup = function () {
		// console.log('trigger init');
		var triggerPopup = $('.accordion_trigger');

		triggerPopup.each(function () {
			var self = $(this);
			self.attr('data-trigger-popup', '.popup--flight-info');
			
			var popup = $(self.data('trigger-popup'));
			if (!popup.data('Popup')) {
				popup.Popup({
					modalShowClass: '',
					triggerCloseModal: '.popup__close',
					afterShow: function() {},
					beforeShow: function() {},
					beforeHide: function(){}
				});
			}
			self.off('click.showPopup').on('click.showPopup', function (e) {
				e.preventDefault();

				e.stopImmediatePropagation();
				popup.Popup('show');
			});
		});
	};

	var deepClone = function (list) {
		return JSON.parse(JSON.stringify(list));
	};

	var changeActivePage = function (page) {
		var self = this;
		self.activePage = page;
	}

	var getNoKrisFlyerCount = function (passengerAddOnsData, noKrisFlyer) {
		var passengerData = deepClone(passengerAddOnsData);
		_.each(passengerData.passengers, function (passenger) {
			if (!passenger.frequentFlyerDetails) {
				noKrisFlyer.push(passenger.firstName + ' ' + passenger.lastName);
			}
		});
	}

	var nextImage = function () {
		var self = this;
		self.direction = "left";
		self.carousellPage = self.carousellPage == 4 ? 1 : self.carousellPage + 1;
		self.disabled = true;
		setTimeout(function () {
			self.disabled = false;
		}, 1000);
	}
	var prevImage = function () {
		var self = this;
		self.direction = "right";
		self.carousellPage = self.carousellPage == 1 ? 4 : self.carousellPage - 1;
		self.disabled = true;
		setTimeout(function () {
			self.disabled = false;
		}, 1000);
	}

	var changeTile = function (number) {
		var self = this;
		self.direction = number > self.carousellPage ? "left" : "right";
		self.carousellPage = number;
		self.disabled = true;
		setTimeout(function () {
			self.disabled = false;
		}, 1000);
	}

	var getBackGround = function () {
		var self = this;
		return {
			'background-image': 'url(' + self.bookingOverviewData.destinationImage + ')'
		}
	}

	var timeParser = function (time) {
		var hours = parseInt(time / (60 * 60));
		var mins = (time % (60 * 60)) / 60;
		return hours != 0 ? `${hours}hrs ${mins}mins` : `${mins}mins`;
	}

	var flightIsActive = function (flightId) {
		var self = this;
		return self.openFlights == flightId;
	}

	var addFlight = function (flight) {
		var self = this;
		var flightId = flight.flightID;
		if (self.openFlights == flightId) {
			self.openFlights = "";
		} else if (self.openFlights != "") {
			self.openFlights = flightId;
		} else {
			self.openFlights = flightId;
		}
		self.$emit("load-flight", flight);
	}

	var isNotLast = function (key1, key2) {
		var self = this;
		return key2 != self.bookingOverviewData.flights[key1].flightSegments.length - 1;
	}

	var onMount = function () {
		var apiRequest = [
			axios.get('ajax/JSONS/MP/ManageBookingOverView.json'),
			axios.get('ajax/JSONS/MP/PassengerAddons.json'),
			axios.get('ajax/JSONS/MP/TripAddon.json')
		];
		var self = this;
		axios.all(apiRequest)
			.then(axios.spread(function (bookingOverview, passengerAddOns, tripAddon) {
				self.bookingOverviewData = bookingOverview.data;
				self.passengerAddOnsData = passengerAddOns.data;
				self.tripAddon = tripAddon.data;
				getNoKrisFlyerCount(self.passengerAddOnsData, self.noKrisFlyer);
			}));
		$($("div.onboard-your-flights-tiles_img")[0]).css('background-image', 'url(../images/bitmap-hover-1.png)');
		$($("div.onboard-your-flights-tiles_img")[1]).css('background-image', 'url(../images/bitmap-hover-2.png)');
		$($("div.onboard-your-flights-tiles_img")[2]).css('background-image', 'url(../images/bitmap-hover-3.png)');
		$($("div.onboard-your-flights-tiles_img")[3]).css('background-image', 'url(../images/bitmap-hover-4.png)');
	};

	var onCreated = function () {
		console.log("Created");
	}

	return {
		init: init
	};
}();

var waitForLibraries = function (fn) {
	setTimeout(function () {
		if (typeof _ == "undefined" ||
			typeof Vue == "undefined" ||
			typeof VTooltip == "undefined" ||
			typeof axios == "undefined"
		) {
			waitForLibraries(fn);
		} else {
			fn.init();
		}
	}, 100);
};

$(function () {
	waitForLibraries(SIA.MPManageBooking);
});
