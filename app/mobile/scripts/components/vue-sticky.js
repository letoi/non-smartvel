SIA.VueSticky = function() {

    var init = function() {
        Vue.directive("scroll-component", {
            bind: function(el, binding, node) {
                el.state = binding.value;
                el.state = checkWidth();
                onScrollEvent(el, node);
                onResizeEvent(el);
            }, 
            update: function(el, binding) {
                el.state = binding.value;
            }
        });
    };

    var onScrollEvent = function(el, node) {
        window.addEventListener("scroll", function() {
            var nodeParent = node.elm.closest(".v-scroll-parent");
            var elementHeight = node.elm.offsetHeight;
            var scrollTopValue = nodeParent.getBoundingClientRect().top;
            var scrollBottomValue = nodeParent.getBoundingClientRect().bottom - elementHeight;
            
            if (scrollTopValue <= 0 && scrollBottomValue >= 0 && el.state) {
                if (el.style.position != "fixed") el.style.position = "fixed";
                el.style.top = "0px";
                el.style.bottom = "";
            } else if (scrollBottomValue < 0 && el.state) {
                if (el.style.position != "absolute") el.style.position = "absolute";
                el.style.top = "";
                el.style.bottom = "0px";
            } else {
                el.removeAttribute("style");
            }
        });
    };

    var onResizeEvent = function(el) {
        window.addEventListener("resize", function() {
           el.state = checkWidth();
        });
    };

    var checkWidth = function() {
        return (window.innerWidth > 775);
    };

    return {
        init: init
    }
}();

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof Vue == "undefined") {
            return waitForLibraries(fn);
        }

        fn.init();
    }, 100);
};

$(function() {
    waitForLibraries(SIA.VueSticky);
});