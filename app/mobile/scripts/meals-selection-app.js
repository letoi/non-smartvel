SIA.MealsSelection = function () {
	var p = {};
	var undoMeal;
	var mealCode = {
        SPML: "Special Meals",
        BTC: "Book the Cook"
    };

	p.global = SIA.global;
	p.config = p.global.config;
	p.body = p.global.vars.body;
	p.win = p.global.vars.win;
	p.htmlBody = $('html, body');
	p.openModals = [];

	// Declare templates
	p.removeButton = '<a class="meal-menu-remove-button" data-remove-trigger="1" href="#">Remove</a>';
	p.accordionWrapperTpl = '<div id="accordionWrapper" class="meals-accordion-wrapper <%= initClass %>" />';
	p.fltBlkTpl = '<div class="flightmeal-control" />';
	p.fltTitleTpl = '<h3 class="title-4--black" />';
	p.accordionWrapTpl = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content"></div></div>';
	p.accordionItemWrapTpl = '<div data-accordion="1" data-accordion-index="<%= id %>" class="block-2 accordion accordion-box" />';
	p.accordionTriggerTplOld = '<div data-accordion-trigger="1" class="accordion__control">\
			 <div class="fm-accordion-label">\
				<ul class="meal-menu">\
				  <li class="meal-service-name"><%= mealType %></li>\
				  <li class="flight-ind"><%= origin %> <em class="ico-airplane-2"></em> <%= destination %></li>\
				</ul>\
				<a href="#" class="select-change-btn" data-trigger-popup=".popup--meals-selection-<%= targetClass %>"><span class="fm-ico-text ecopey-meal">Change</span>\
				<em class="ico-point-d"></em></a>\
				<div class="selected-meal-cont">\
					<p class="meal-sub-title-black" data-label="<%= menuSelectionLbl %>"><%= menuSelection %></p>\
					<em class="ico-delete-2 hidden" data-remove-trigger="1" data-segment="<%= segmentId %>"></em>\
				</div>\
				<div class="remove-prompt hidden confirmation-prompt confirmation-remove--blue">\
					<em class="ico-alert"></em>\
					<p class="meal-sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod tempor incididunt ut labore et dolore.</p>\
					<div class="button-group-1">\
						<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
						<a href="#" class="btn-2 btn-cancel">Cancel</a>\
					</div>\
				</div>\
			 </div>\
		  </div>';
	p.accordionTriggerTpl = '<div data-accordion-trigger="1" class="accordion__control accordion-<%- targetId %> <%= (isScoot) ? "bah-accordion-block" : "" %>">\
		  <div class="fm-accordion-label">\
			<ul class="meal-menu">\
				<li class="meal-service-name"><%= mealType %></li>\
				<li class="flight-ind"><%= origin %> <em class="ico-airplane-2"></em> <%= destination %></li>\
			</ul>\
			<a href="#" class="select-change-btn" data-trigger-popup=".popup--meals-selection-<%= targetClass %>"><span class="fm-ico-text ecopey-meal">Change</span>\
			<em class="ico-point-d"></em></a>\
			<div class="selected-meal-cont">\
				<div class="<%= (!isScoot) ? "hidden" : "" %> bah-label-block">\
					<img class="bah-logo" src="images/svg/tr.svg" alt="tr Logo" longdesc="img-desc.html">\
					<span class="bah-text">Scoot-operated flight</span>\
				</div>\
				<p class="meal-sub-title-black" data-label="<%= menuSelectionLbl %>"><%= menuSelection %></p>\
				<em class="ico-delete-2 hidden" data-remove-trigger="1"></em>\
			</div>\
			<div class="remove-prompt hidden confirmation-prompt confirmation-remove--blue">\
				<p class="meal-sub-title-blue"> When you remove this meal selection, note that there will not be a refund for\
				the meal which you&apos;ve previously paid for. You can select another meal to replace this. </p> \
				<div class="button-group-1">\
					<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
		  </div>\
	   </div>';

	p.accordionContentTpl = '<aside class="popup popup-meals menu-list popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
		  	<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
				<div class="popup__content">\
					 <div data-accordion-content="1" class="accordion__content">\
						  <h3 class="fm-title-4--blue">Select a category</h3>\
						  <div class="item-container">\
								<article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %>">\
									 <a href="#" class="promotion-item__inner <%= inflNotReady %>" data-trigger-popup=".popup--inflight-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available">\
														  </div>\
													 </div>\
													 <img src="images/bg-inflight-menu.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													 <div class="meal-label">\
													  	<span class="view-only-btn"> VIEW ONLY </span>\
						                                <span><%= inflLabel %></span>\
						                                <em class="ico-point-r"></em>\
						                            </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
								<article class="promotion-item promotion-item--1">\
									 <a href="#" class="promotion-item__inner <%= btcNotReady %>" data-trigger-popup=".popup--btc-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available"></div>\
													 </div>\
													 <img src="images/bg-book-the-cook.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													 <div class="meal-label">\
						                                <span><%= btcLabel %></span>\
						                                <em class="ico-point-r"></em>\
						                            </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
								<article class="promotion-item promotion-item--1">\
									 <a href="#" class="promotion-item__inner <%= spmNotReady %>" data-trigger-popup=".popup--spm-menu-<%= targetClass %>">\
										  <figure>\
												<div class="flight-item">\
													 <div class="overlay-meal">\
														  <p class="not-available-text"><%= mealNotReadyMsg %></p>\
														  <div class="overlay-not-available"></div>\
													 </div>\
													 <img src="images/bg-special-meal.png" alt="Dolore magna aliqua " longdesc="img-desc.html">\
													<div class="meal-label">\
						                              <span><%= spmlLabel %></span>\
						                              <em class="ico-point-r"></em>\
						                          </div>\
												</div>\
										  </figure>\
									 </a>\
								</article>\
						  </div>\
						  <hr>\
						  <div class="not-eating-supper">\
								<div class="custom-checkbox custom-checkbox--1">\
									 <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
									 <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <span><%= mealType %></span></label>\
									 <p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you won&apos;t be having <span><%= mealType %></span>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                        				Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
								</div>\
						  </div>\
						  <div class="button-group-1 btn-confirm hidden">\
								<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
						  </div>\
					 </div>\
				</div>\
		  </div>\
	 </aside>';

	p.accordionContentTplNoImage = '<aside class="popup popup-meals menu-list popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
			<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
 				<div class="popup__content">\
						<div data-accordion-content="1" class="accordion__content">\
 						  <h3 class="fm-title-4--blue">Select a category</h3>\
 						  <div class="item-container">\
								<article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %> no-image">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item <%= inflNotReady %>"  data-trigger-popup=".popup--inflight-menu-<%= targetClass %>">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label">\
                                 	<span class="view-only-btn"> VIEW ONLY </span>\
                                    <span>Inflight Menu</span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc">A sumptuous selection of award-winning dishes</p>\
                                    <p class="not-available-text">The menu will be available for selection from 7 September onwards</p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item <%= btcNotReady %>" data-trigger-popup=".popup--btc-menu-<%= targetClass %>">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label">\
                                    <span>Book the Cook</span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc">Savour a fine dining experience in the air specially created by our International Culinary Panel.</p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item <%= spmNotReady %>" data-trigger-popup=".popup--spm-menu-<%= targetClass %>">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label">\
                                    <span>Special Meals</span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc">Meals put together with care for those with specific dietary requirements</p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
 						  </div>\
 						  <hr>\
 						  <div class="not-eating-supper">\
 								<div class="custom-checkbox custom-checkbox--1">\
 									 <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
 									 <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <span><%= mealType %></span></label>\
 									 <p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you won&apos;t be having <span><%= mealType %></span>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                   Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
 								</div>\
 						  </div>\
 						  <div class="button-group-1 btn-confirm hidden">\
 								<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
 						  </div>\
 					 </div>\
 				</div>\
 		  </div>\
 	 	</aside>';
	p.accordionInfantContentTpl = '<aside class="popup popup-meals menu-list popup--meals-selection-<%= id %> hidden infant-meals" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
		  	<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
				<div class="popup__content">\
					 <div data-accordion-content="1" class="accordion__content">\
						  <h3 class="fm-title-4--blue">Infant Meals</h3>\
						  <div class="inf-container"></div>\
						  <hr>\
						  <div class="not-eating-supper">\
								<div class="custom-checkbox custom-checkbox--1">\
									 <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" <%= checked %> class="not-eating">\
									 <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">Bring my own meal</label>\
									 <p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you will be bringing your own meal. Our flight attendants will not approach you during mealtime. If you change your mind, however, you still select your meal by returning to this page at least 24 hours before your departure.</p>\
								</div>\
						  </div>\
						  <div class="button-group-1 btn-confirm hidden">\
								<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
						  </div>\
					 </div>\
				</div>\
		  </div>\
	 </aside>';

	p.checkinAlert = '<div class="alert-block checkin-alert" style="top: 15px;">\
		<div class="inner">\
		  <div class="alert__icon"><em class="ico-alert"> </em></div>\
		  <div class="alert__message"><%= checknMsg %></div>\
		</div>\
	 </div>';
	p.mealsLabelTpl = '<span class="meals-label btn-small btn-grey">VIEW ONLY</span>';

	p.accordionContentTplEco = '<aside class="popup popup-meals menu-type-list menu-list popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
			<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
			<div class="popup__content">\
				<div data-accordion-content="1" class="accordion__content">\
					<h3 class="fm-title-4--blue">Select a category</h3>\
					<div class="item-container">\
						<% if (false) { %>\
						<article class="promotion-item promotion-item--1">\
							<a href="#" data-main-index="<%= id %>" class="promotion-item__inner inflight-menu meals-menu-item" data-inlf>\
								<figure>\
									<div class="flight-item inflight-economy-hr">\
										<div class="overlay-meal">\
											<div class="overlay-not-available">\
											</div>\
										</div>\
										<div class="meal-label ml-inflight-container meal-inf-ecopey">\
											<span class="inflight-inner-container">\
                                                <%= inflLabel %> - Select your meal on board your flight\
                                                <div class="select-label-container">\
                                                    <span class="pull-right select-label">Select</span>\
                                                </div>\
                                        		<span class="inflight-badge hidden">Selected</span>\
											</span>\
											<input type="button" name="select" class="hidden" value="Select" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>">\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
						<% } %>\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" class="promotion-item__inner special-meals meals-menu-item <%= spmNotReady %>" data-trigger-popup=".popup--spm-menu-<%= targetClass %>">\
								<figure>\
									<div class="flight-item">\
										<div class="overlay-meal">\
											<p class="not-available-text"><%= mealNotReadyMsg %></p>\
											<div class="overlay-not-available"></div>\
										</div>\
										<div class="meal-label epe-meal-container">\
											<span>Special Meals</span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc">Meals put together with care for those with specific dietary requirements</p>\
											<span class="inflight-badge hidden">Selected</span>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
					</div>\
					<hr>\
					<div class="not-eating-supper">\
						<div class="custom-checkbox custom-checkbox--1">\
								<input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" data-main-menu-target="<%= id+1 %>" class="not-eating" <%= checked %>>\
								<label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <span><%= mealType %></span></label>\
								<p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you won&apos;t be having <span><%= mealType %></span>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
								Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
						</div>\
					</div>\
					<div class="button-group-1 btn-confirm hidden">\
						<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
					</div>\
				</div>\
			</div>\
		</div>\
	</aside>';

	p.accordionContentTplPey = '<aside class="popup popup-meals menu-type-list menu-list popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
			<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
			<div class="popup__content">\
				<div data-accordion-content="1" class="accordion__content">\
					<h3 class="fm-title-4--blue">Select a category</h3>\
					<div class="item-container">\
						<% if (false) { %>\
						<article class="promotion-item promotion-item--1">\
							<a href="#" data-main-index="<%= id %>" class="promotion-item__inner inflight-menu meals-menu-item <%= inflNotReady %>">\
								<figure>\
									<div class="flight-item inflight-economy-hr">\
										<div class="overlay-meal">\
											<div class="overlay-not-available">\
											</div>\
										</div>\
										<div class="meal-label ml-inflight-container meal-inf-ecopey">\
											<span class="inflight-inner-container">\
												<%= inflLabel %> - Select your meal on board your flight\
												<div class="select-label-container">\
                                                    <span class="pull-right select-label">Select</span>\
                                                </div>\
                                        		<span class="inflight-badge hidden">Selected</span>\
											</span>\
											<input type="button" name="select" class="hidden" value="Select" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>">\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
						<% } %>\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item <%= btcNotReady %>" data-trigger-popup=".popup--btc-menu-<%= targetClass %>">\
								<figure>\
									<div class="flight-item">\
										<div class="overlay-meal">\
											<p class="not-available-text"><%= mealNotReadyMsg %></p>\
											<div class="overlay-not-available">\
											</div>\
										</div>\
										<div class="meal-label epe-meal-container">\
											<span>Book the Cook</span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc">Savour a fine dining experience in the air specially created by our International Culinary Panel.</p>\
											<span class="inflight-badge hidden">Selected</span>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" class="promotion-item__inner special-meals meals-menu-item <%= spmNotReady %>" data-trigger-popup=".popup--spm-menu-<%= targetClass %>">\
								<figure>\
									<div class="flight-item">\
										<div class="overlay-meal">\
											<p class="not-available-text"><%= mealNotReadyMsg %></p>\
											<div class="overlay-not-available"></div>\
										</div>\
										<div class="meal-label epe-meal-container">\
											<span>Special Meals</span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc">Meals put together with care for those with specific dietary requirements</p>\
											<span class="inflight-badge hidden">Selected</span>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
					</div>\
					<hr>\
					<div class="not-eating-supper">\
						<div class="custom-checkbox custom-checkbox--1">\
								<input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" data-main-menu-target="<%= id+1 %>" class="not-eating" <%= checked %>>\
								<label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&apos;d prefer not to have <span><%= mealType %></span></label>\
								<p class="not-eating-msg<%= notEatingTxt %>">Thank you for indicating that you won&apos;t be having <span><%= mealType %></span>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
								Or simply approach our flight attendants on board, who will be happy to serve you any meal that&apos;s available from our Inflight Menu.</p>\
						</div>\
					</div>\
					<div class="button-group-1 btn-confirm hidden">\
						<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
					</div>\
				</div>\
			</div>\
		</div>\
	</aside>';

	p.accordionContentTplBah = '<aside class="popup popup-meals menu-type-list menu-list popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
			<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
			<div class="popup__content">\
				<div data-accordion-content="1" class="accordion__content">\
				<h3 class="fm-title-4--blue">Select a category</h3>\
					<div class="item-container">\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" data-trigger-popup=".popup--bah-hot-meals-<%- targetId %>" class="promotion-item__inner hot-meals meals-menu-item">\
								<figure>\
									<div class="flight-item">\
										<div class="meal-label">\
											<span class="meal-category-title"><%= htmLabel %></span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc"><%= htmDesc %></p>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" data-trigger-popup=".popup--bah-light-meals-<%- targetId %>" class="promotion-item__inner light-meals meals-menu-item">\
								<figure>\
									<div class="flight-item">\
										<div class="meal-label">\
											<span class="meal-category-title"><%= lmlLabel %></span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc"><%= lmlDesc %></p>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
						<article class="promotion-item promotion-item--1 no-image">\
							<a href="#" data-trigger-popup=".popup--bah-premium-meals-<%- targetId %>" class="promotion-item__inner premium-meals meals-menu-item">\
								<figure>\
									<div class="flight-item">\
										<span class="<%= (isPrslcsPreOrder) ? "" : "hidden" %> pre-order-badge">PRE-ORDER ONLY</span>\
										<div class="meal-label">\
											<span class="meal-category-title"><%= prslcsLabel %></span>\
											<em class="ico-point-r"></em>\
											<p class="meal-category-desc"><%= prslcsDesc %></p>\
											<div class="price-range-block">\
												<span class="meal-price-label">FROM</span>\
												<span class="meal-price">SGD 8.00</span>\
											</div>\
										</div>\
									</div>\
								</figure>\
							</a>\
						</article>\
					</div>\
					<hr>\
					<div class="button-group-1 btn-confirm hidden">\
						<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
					</div>\
				</div>\
			</div>\
		</div>\
	</aside>';

	p.accordionContentTplBahLong = '<aside class="popup popup-meals menu-type-list menu-list popup--longhaul-<%= targetId %> popup--meals-selection-<%= id %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
		<div class="popup__inner">\
			<a href="#" class="popup__close hidden" data-close="true"><span class="ui-helper-hidden-accessible">Close</span></a>\
			<div class="popup__content">\
				<div data-accordion-content="1" class="accordion__content">\
					<h3 class="fm-title-4--blue">Select a category</h3>\
					<div class="item-container"></div>\
					<hr>\
					<div class="button-group-1 btn-confirm hidden">\
						<a href="#" class="btn-1 confirm-not-eating">Confirm</a>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div class="sticky-confirm-bar no-selection">\
			<button class="btn-1" data-close-all="true">Done</button>\
		</div>\
	</aside>';

	p.bahMenuContent = '<aside class="popup popup-meals popup--bah-<%= targetClass %> popup--bah-<%- className %>-<%= targetClass %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">\
			<div class = "popup__inner">\
				<div class="popup__content">\
					<p class="light-box-btn-mb"><a href="#" class="lightbox-btn-back" data-close="true"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to categories</a></p>\
					<h3 class="fm-title-4--blue"><%= mealTitle %></h3>\
					<p class="light-box-text"><%= mealDesc %></p>\
					<div class="item-wrapper"></div>\
				</div>\
			</div>\
			<div class="sticky-confirm-bar no-selection">\
				<button class="btn-1" data-close-all="true">Done</button>\
			</div>\
		</aside>';

	p.menuItemBah = '<article data-meal-item class="promotion-item inflight-menu-item--1 menu-item meal-of-bah">\
		<span class="fm-inf-menu-select-badge">Selected</span>\
		<div class="fm-inf-menu <%- !speciality.length ? "no-dish-icon" : "" %>">\
			<h4 class="fm-inf-menu-title" aria-hidden="true"><%- mealName %></h4>\
			<% if (price) { %>\
				<span class="meal-price"><%- currency %> <%- price %></span>\
			<% } %>\
			<p class="fm-inf-menu-desc" aria-hidden="true"><%- mealDescription %></p>\
			<div class="fm-footnote-txt">\
				<span class="food-icon-set">\
					<% _.each(speciality, function(special) {%>\
					<% if (icons[special.dishIconId]) { %>\
						<em class="<%- icons[special.dishIconId].className %> fm-footnote-logo"></em>\
					<% } %>\
					<span class="fm-dish-text">\
						<%- special.specialityFootNote %>\
					</span>\
					<% }) %>\
				</span>\
				<input type="button" name="select" value="Select" class="fm-select-btn right" data-paxid="<%- paxId %>" data-segment="<%- segmentId %>" data-meal-servicecode="<%- mealServiceCode %>" data-meal-category="<%- mealCategory %>" data-meal-mealcode="<%- mealCode %>" data-meal-menuname="<%- mealName %>" data-meal-price="<%- price %>">\
			</div>\
			<div data-prompt="initial" class="confirmation-prompt hidden">\
				<p class="meal-sub-title-blue"> You have an existing premium meal which will be replaced with this new meal selection. There will not be a refund for the meal which you&apos;ve previously paid for. </p>\
				<div class="button-group-1">\
					<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
		</div>\
	</article>';

	p.inflightAlertPrompt = $('<div class="inflight-confirmation-prompt confirmation-prompt">\
		<em class="ico-alert inflight-ico-alert"></em>\
		<p class="meal-sub-title-black">You&apos;ve changed your meal choice to <span>Inflight Menu</span>. <a href="#" class="inflight-link-undo"> <em class="ico-point-r"></em> Undo</a></p>\
	</div>');

	p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
		<em class="ico-alert"></em>\
		<p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
	</div>');

	p.removeButton = " <a class='meal-menu-remove-button' data-remove-trigger='1' href='#'>Remove</a>";

	p.stickyCloseAll = $('<div class="sticky-flight-meal hide">'+
			'<span class="popup-meal-type"></span>'+
			'<span class="light-box-title"><span class="popup-title-origin"></span> <em class="ico-airplane-2"></em> <span class="popup-title-dest"></span></span>'+
			'<button class="popup__close" data-close-all="true"><span class="ui-helper-hidden-accessible">Close</span></button>'+
		'</div>');

	p.inflightLabel = '<span class="title-selected">Inflight Menu</span>';
	p.selectedLabel = '<span><%= mealName %> (<%= mealType %>)</span>';

	p.mealDictionary = {
		"SPML": "Special Meals",
		"BTC": "Book the Cook"
	};
	
	// To be used to index each vertical accordion tab to reference when changing arrow text
	p.accordTabArr = [];
	p.accordContentArr = [];
	p.accordionTabIndex = 0;
	// cache meals that are not ready for reset
	p.notReadyMeals = [];

	p.currInflightMenuArr = [];

	p.curPaxIndex = 0;
	p.paxNavCount = 0;
	p.paxArr = [];

	// Index all meal modules
	p.ifml = [];
	p.spml = [];
	p.btcml = [];
	p.infml = [];

	p.cabinClassCodes = {
        pey: ['P', 'T', 'S'],
        eco: ['N', 'Q', 'W', 'H', 'M', 'E', 'B', 'Y'],
		biz: ['C', 'D', 'J', 'Z', 'U'],
		first: ['R', 'A', 'F']
    };

    p.isPey = false;
	p.isEco = false;
	p.isBiz = false;
	p.isFirst = false;
	
	p.frmCnfrm = false;

	// declare dish icon for inflight menu scenario
	p.inflightDishIcons = {
		'ICP': 'ico-4-salmon',
		'DWH': 'ico-4-fork-1',
		'WHS': 'ico-4-fork',
		'EPG': 'ico-4-amet',
		'PLF': 'ico-4-cook',
		'MTL': 'ico-4-fork',
		'VGT': 'ico-4-leaf',
		'LCL': 'ico-4-heart',
		'LCA': 'ico-4-sandwich',
		'LCH': 'ico-4-fork',
		'CNY': 'ico-4-info',
		'XMAS': 'ico-4-pine',
		'DEP': 'ico-4-coffee',
		'TWG': 'ico-4-fork',
		'LFD': 'ico-4-cook'
	};

	p.bspIsDisplayed = false;

	var init = function () {
		p.json = globalJson.mealsInfo;

		// Assign the initial states
		getInitialStates();

		p.mealSelectionForm = $('#mealSelection');
		p.container = $('#container');
		p.paxListWrapper = $('#paxListWrapper');
		p.curWinWidth = p.win.width();

		// Update the initial data for the pax
		setupData();

		// Paint pax navigation
		paintPaxNav();

		// Paint meals menu
		paintMealsMenu();

		if ($("body").hasClass("meals-selection-bah")) {
			SIA.clickActivity.init();
		}

		initCloseAllPopup();
		$(document).ready(function() {
			onModalOpen();
		});
	};

	var onModalOpen = function(el) {
		if ((el || $("body")).hasClass("select-change-btn")) {
			p.currentOpenModal = el.closest(".accordion-box");
		}
	};

	var setupData = function () {
		// Add hidden input field in form
		var mealsPreselectedBSP = [];
		p.input = $('<input type="hidden" value="" />');
		p.mealSelectionForm.prepend(p.input);

		p.data = {};
		p.data.paxList = [];

		// Loop through the passenger list
		for (var i = 0; i < p.json.paxList.length; i++) {
			var d = p.json.paxList[i];
			var f = p.json.flightSegment;

			var pax = {};
			// Add base pax info
			pax.paxId = d.paxId;
			pax.paxType = d.paxType;
			pax.paxName = d.paxName;

			// Add selected meals array
			// to hold each flightSector's mealServiceCode
			pax.selectedMeals = [];
			pax.preselectedMeals = [];

			for (var j = 0; j < f.length; j++) {
				var segment = {
					'segmentId': f[j].segmentId,
					'selectedMeal': {}
				}

				// loop through the sectors
				for (var k = 0; k < f[j].flightSector.length; k++) {
					var paxServiceType = getServiceType(i);
					var serviceTypes = f[j].flightSector[k][paxServiceType];

					for (var l = 0; l < serviceTypes.length; l++) {
						var mealServiceCode = serviceTypes[l].mealServiceCode;
						var preselectedData = d.selectedMeal[j];
						var flightDetails = f[j].flightSector[k];
						var isPreselectedMeal = false;
						// setup initial values for each service code
						var mealSelection = {
							'isEating': true,
							'isALaCarte': false,
							'mealCategoryCode': null,
							'mealCode': null,
							'mealMenuName': null,
							'aLaCarteData': null
						};

						if (
							$("body").hasClass("meals-selection-bah") &&
							preselectedData &&
							preselectedData.mealServiceCode == mealServiceCode &&
							preselectedData.mealCategoryCode == "PRSLCS"
						) {
							if (
								preselectedData.segmentId == f[j].segmentId &&
								preselectedData.sectorId == f[j].flightSector[k].sectorId
							) {
								isPreselectedMeal = true;
								mealSelection.mealCategoryCode = preselectedData.mealCategoryCode;
								mealSelection.mealCode = preselectedData.mealCode;
								mealSelection.mealMenuName = preselectedData.mealMenuName;
								mealSelection.mealPrice = preselectedData.price;
								mealSelection.isPreselected = true;
							}
						}
						segment.selectedMeal[mealServiceCode] = mealSelection;
						if (isPreselectedMeal) {
							pax.preselectedMeals.push(JSON.parse(JSON.stringify((segment))));
						}
					}
				}
				pax.selectedMeals.push(segment);
			}

			p.data.paxList.push(pax);
			p.input.val(JSON.stringify(p.data))
		}
	};

	var showBSP = function(bspDetails) {
		_.each(bspDetails, function(bsp) {
			SIA.BSPUtil.addBSPTpl(bsp.ids, bsp.pax);
		 });
	};

	var updateData = function (menu) {
		// Update data
		var pid = parseInt(menu.attr('data-paxid'));
		var sid = parseInt(menu.attr('data-segment'));
		var service = menu.attr('data-meal-servicecode');
		var categoryCode = menu.attr('data-meal-category');
		var mealCode = menu.attr('data-meal-mealcode');
		var mealMenuName = menu.attr('data-meal-menuname');
		var mealPrice = menu.attr('data-meal-price');
		var isALC = menu.attr('data-isalc');
		var aLaCarteData = null;

		if (typeof menu.attr('data-alc-json') != 'undefined') aLaCarteData = JSON.parse(menu.attr('data-alc-json'));

		var meal = p.data.paxList[pid].selectedMeals[sid].selectedMeal[service];
		delete meal.isPreselected;
		// Check if element clicked is a checkbox
		if (menu.hasClass('not-eating')) {
			meal.isEating = !menu.prop('checked');
			meal.mealCode = null;
			meal.mealMenuName = null;
		} else if (isALC && typeof aLaCarteData != 'undefined') {
			meal.isEating = true;
			meal.mealCode = mealCode;
			meal.mealCategoryCode = categoryCode;
			meal.mealMenuName = mealMenuName;
			meal.isALaCarte = isALC;
			meal.aLaCarteData = aLaCarteData;
		} else {
			if (menu.hasClass('selected')) {
				meal.isEating = true;
				meal.mealCode = mealCode;
				meal.mealCategoryCode = categoryCode;
				meal.mealMenuName = mealMenuName;
				meal.isALaCarte = isALC;
				meal.mealPrice = mealPrice;
			} else {
				meal.mealCode = null;
				meal.mealCategoryCode = null;
				meal.mealMenuName = null;
				meal.mealPrice = null;
			}
		}
		
		p.input.val(JSON.stringify(p.data));
	};

	var getInitialStates = function () {
		for (var i = 0; i < p.json.paxList.length; i++) {
			if (p.json.paxList[i].paxId === p.json.passengerStartingPoint) {
				p.curPaxIndex = i;
				p.passengerStartingPoint = i;
			}
		}

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			if (p.json.flightSegment[i].segmentId == p.json.startSegment) p.startSegment = i;
		}

		for (var i = 0; i < p.json.flightSegment[p.startSegment].flightSector.length; i++) {
			if (p.json.flightSegment[p.startSegment].flightSector[i].sectorId == p.json.startSector) p.startSector = i;
		}

		p.startMealService = p.json.startMealService;
	};

	var paintMealsMenu = function (initClass = '') {
		// Reset initial values
		p.accordionTabIndex = 0;
		p.accordionWrapper = null;
		// remove existing triggers
		for (var h = 0; h < p.accordContentArr.length; h++) {
			p.accordContentArr[h].remove();
		}

		// remove existing menus
		_.each(p.ifml, function (menu) {
			menu.destroy();
		});
		_.each(p.spml, function (menu) {
			menu.destroy();
		});
		_.each(p.btcml, function (menu) {
			menu.destroy();
		});
		_.each(p.infml, function (menu) {
			menu.destroy();
		});

		// Empty the array holders
		p.accordTabArr = [];
		p.accordContentArr = [];
		p.notReadyMeals = [];

		p.ifml = [];
		p.spml = [];
		p.btcml = [];
		p.infml = [];

		// Create new accordion wrapper
		p.accordionWrapper = $(_.template(p.accordionWrapperTpl, {
			'initClass': initClass
		}));

		for (var i = 0; i < p.json.flightSegment.length; i++) {
			// create new flight block
			var flSegment = $(p.fltBlkTpl);

			// create flight segment title
			var segmentTitle = $(p.fltTitleTpl);
			segmentTitle.html((i + 1) + '. ' + p.json.flightSegment[i].departureAirportName + ' to ' + p.json.flightSegment[i].arrivalAirportName);

			// Add title to segment block
			flSegment.append(segmentTitle);

			// check if second segment available for meal selection
			secSegmentAvailable(flSegment, i);

			// create accordion-wrapper block
			var accordionWrap = $(p.accordionWrapTpl);

			var accordionContentWrap = accordionWrap.find('.accordion-wrapper-content');

			// Check cabin class
            p.isPey = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.pey); // Check if PEY
			p.isEco = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.eco); // Check if Economy
			p.isBiz = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.biz); // Check if Bisuness
			p.isFirst = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.first); // Check if First/Suite
			
			if ($('body').hasClass('meals-selection-bah')) {
				p.isEco = true;
			}

			for (var j = 0; j < p.json.flightSegment[i].flightSector.length; j++) {
				var data = p.json.flightSegment[i].flightSector[j];

				// get current pax type if adult, child or infant
				p.serviceType = getServiceType(p.curPaxIndex);

				var mealService = data[p.serviceType];

				for (var k = 0; k < mealService.length; k++) {
					// Each meal service will need an accordian item wrapper
					var accItemWrap = $(_.template(p.accordionItemWrapTpl, {
						'id': (p.accordionTabIndex + 1)
					}));

					// Check if infant
					var mealCategoryCode, mealServiceCode;
					if (p.serviceType == 'mealServicesInfant') {
						mealCategoryCode = mealService[k].allMeals[0].mealCategoryInfant[0].mealCategoryCode;
						mealServiceCode = mealService[k].mealServiceCode;
					} else {
						if (mealService[k].allMeals[0].mealCategoryLongHaul) {
							mealCategoryCode = "LONGHAUL";
						} else if (mealService[k].allMeals[1].mealCategoryInflight) {
							mealCategoryCode = mealService[k].allMeals[1].mealCategoryInflight[0].mealCategoryCode;
						} else {
							mealCategoryCode = mealService[k].allMeals[1].mealCategoryLightMeals[0].mealCategoryCode;
						}
						mealServiceCode = mealService[k].mealServiceCode;
					}

					// check if is eating
					p.curPaxData = p.data.paxList[p.curPaxIndex].selectedMeals[i].selectedMeal[mealService[k].mealServiceCode];

					var inflLbl = 'Inflight Menu';
					if(p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					
					var menuLabel = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : inflLbl;
					var menuSelectionLbl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : 'Inflight Menu';

					if (p.curPaxData.mealMenuName && p.curPaxData.isEating) {
						menuLabel = p.curPaxData.mealMenuName;
					}

					if (p.curPaxData.mealMenuName && p.curPaxData.isEating) {
						// Add meal category indicator
						var mealTypeIndicator;
						if (p.curPaxData.mealCategoryCode === 'SPML') mealTypeIndicator = ' (Special Meals)';
						if (p.curPaxData.mealCategoryCode === 'INFM') mealTypeIndicator = ' (Inflight Meal)';
						if (p.curPaxData.mealCategoryCode === 'BTC') mealTypeIndicator = ' (Book the Cook)';
						if (p.curPaxData.mealCategoryCode === 'INF') mealTypeIndicator = ' (Infant Meal)';
						if (p.curPaxData.mealCategoryCode === 'HTM') mealTypeIndicator = ' (Hot Meal)';
						if (p.curPaxData.mealCategoryCode === 'LML') mealTypeIndicator = ' (Light Meal)';
						if (p.curPaxData.mealCategoryCode === 'PRSLCS') mealTypeIndicator = ' (Premium Selections)';

						var selectedLabel = '';
						if (p.isEco || p.isPey) selectedLabel = '<span class="title-selected hidden">Selected:</span> ';

						menuLabel = p.curPaxData.mealMenuName + mealTypeIndicator;
					} else if (mealService[k].mealServiceCode == "MEAL_115" && !p.curPaxData.mealCode) {
						if (mealService[k].allMeals[0].mealCategoryLongHaul) {
							menuLabel = '<span class="title-selected">Long Haul Meal Bundle</span>'
						} else {
							menuLabel = '<span class="title-selected">Hot Meal</span>';
						}
					}

					// Check if theres existing A la carte data
					if (p.curPaxData.isALaCarte === 'true') {
						var parentAccMealSummary = '';
						_.each(p.curPaxData.aLaCarteData, function (v, k, l) {
							if (v.mealMenuName != null) {
								// set the title info
								parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';
							}

						});
						menuLabel = parentAccMealSummary.substring(4);
					}

					if (!p.curPaxData.isEating) {
						menuLabel = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + mealService[k].mealServiceName;
					}

					// Create accordion trigger
					var triggerAcc = p.accordionTriggerTplOld
					if (data.isScoot) {
						triggerAcc = p.accordionTriggerTpl;
					}
					var accTrigger = $(_.template(triggerAcc, {
						'origin': data.departureAirportCode,
						'destination': data.arrivalAirportCode,
						'mealType': mealService[k].mealServiceName,
						'menuSelection': menuLabel,
                        'menuSelectionLbl': menuSelectionLbl,
						'targetClass': p.accordionTabIndex,
						'isScoot': data.isScoot,
						'targetId': i + "" + j + "" + p.curPaxIndex,
						'segmentId': i
					}));
					
					if (p.curPaxData.mealCategoryCode == "SPML" || p.curPaxData.mealCategoryCode == "BTC") {
						var selectedLabel = '';
						if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected hidden">Selected:</span> ';
						var curMLbl = selectedLabel + accTrigger.find("li:nth-child(3)").html();
						accTrigger.find("li:nth-child(3)").html(curMLbl);
						accTrigger.find("li:nth-child(3)").append(p.removeButton);
						accTrigger.find("em.ico-delete-2").removeClass("hidden");
						removeButtonListener();
					}

					if (!p.curPaxData.isEating) {
						accTrigger.find("em.ico-delete-2").addClass("hidden");
					}

					accTrigger.find('[data-trigger-popup]').on({
						'cilck': function(){
							p.frmCnfrm = false;
						}
					});

					p.accordTabArr.push(accTrigger);

					// Check all meals availability attribute only shows up in inflight meal
					mealAvailData = mealsAvailable(mealService, k);

					// get contentTpl option
					var allMeals = mealService[k].allMeals;
					var hasNoImg = false,
						btcImg, inflImg, spmlImg, htmImg, lmlImg, prslcsImg,
						btcLabel, inflLabel, spmlLabel, htmLabel, lmlLabel, prslcsLabel,
						btcDesc, inflDesc, spmlDesc, htmDesc, lmlDesc, prslcsDesc,
						hasBtcImg = true,
						hasInflImg = true,
						hasSpmlImg = true,
						hasHtmImg = true, 
						hasLmlImg = true, 
						hasPrslcsImg = true,
						isPrslcsPreOrder = false;

					var inflNotReady = '',
						btcNotReady = '',
						spmNotReady = '';

					for (var m = 0; m < allMeals.length; m++) {
						if (typeof allMeals[m].mealCategoryBTC !== 'undefined') {
							btcImg = allMeals[m].mealCategoryBTC[0].mealCategoryImage;
							btcDesc = allMeals[m].mealCategoryBTC[0].mealCategoryDescription;
							btcLabel = allMeals[m].mealCategoryBTC[0].mealMenuCategoryName;

							if (typeof btcImg === 'undefined' || btcImg === '') hasBtcImg = false;
							if (typeof allMeals[m].mealCategoryBTC === 'undefined') hasBtcImg = false;

							var mealSelectAvail = allMeals[m].mealCategoryBTC[0].isMealSelectionAvailable;
							var isViewMenuEligible = allMeals[m].mealCategoryBTC[0].isViewMenuEligible;
							if (typeof mealSelectAvail !== undefined) {
								if (mealSelectAvail === 'false' && isViewMenuEligible === 'false') btcNotReady = 'meal-selection-off';
							}
						}
						if (typeof allMeals[m].mealCategoryInflight !== 'undefined') {
							inflImg = allMeals[m].mealCategoryInflight[0].mealCategoryImage;
							inflDesc = allMeals[m].mealCategoryInflight[0].mealCategoryDescription;
							inflLabel = allMeals[m].mealCategoryInflight[0].mealMenuCategoryName;

							if (typeof inflImg === 'undefined' || inflImg === '') hasInflImg = false;
							if (typeof allMeals[m].mealCategoryInflight === 'undefined') hasInflImg = false;

							var mealSelectAvail = allMeals[m].mealCategoryInflight[0].isMealSelectionAvailable;
							var isViewMenuEligible = allMeals[m].mealCategoryInflight[0].isViewMenuEligible;
							if (typeof mealSelectAvail !== undefined) {
								if (mealSelectAvail === 'false' && isViewMenuEligible === 'false') inflNotReady = 'meal-selection-off';
							}
						}
						if (typeof allMeals[m].mealCategorySPML !== 'undefined') {
							spmlImg = allMeals[m].mealCategorySPML[0].mealCategoryImage;
							spmlDesc = allMeals[m].mealCategorySPML[0].mealCategoryDescription;
							spmlLabel = allMeals[m].mealCategorySPML[0].mealMenuCategoryName;

							if (typeof spmlImg === 'undefined' || spmlImg === '') hasSpmlImg = false;
							if (typeof allMeals[m].mealCategorySPML === 'undefined') hasSpmlImg = false;

							var mealSelectAvail = allMeals[m].mealCategorySPML[0].isMealSelectionAvailable;
							var isViewMenuEligible = allMeals[m].mealCategorySPML[0].isViewMenuEligible;
							if (typeof mealSelectAvail !== undefined) {
								if (mealSelectAvail === 'false' && isViewMenuEligible === 'false') spmNotReady = 'meal-selection-off';
							}
						}
						if (typeof allMeals[m].mealCategoryHotMeals !== 'undefined') {
							htmImg = allMeals[m].mealCategoryHotMeals[0].mealCategoryImage;
							htmDesc = allMeals[m].mealCategoryHotMeals[0].mealCategoryDescription;
							htmLabel = allMeals[m].mealCategoryHotMeals[0].mealMenuCategoryName;
							if (typeof htmImg === 'undefined' || htmImg === '') hasHtmImg = false;
							if (typeof allMeals[m].mealCategoryHotMeals === 'undefined') hasHtmImg = false;
						}
						if (typeof allMeals[m].mealCategoryLightMeals !== 'undefined') {
							lmlImg = allMeals[m].mealCategoryLightMeals[0].mealCategoryImage;
							lmlDesc = allMeals[m].mealCategoryLightMeals[0].mealCategoryDescription;
							lmlLabel = allMeals[m].mealCategoryLightMeals[0].mealMenuCategoryName;
							if (typeof lmlImg === 'undefined' || lmlImg === '') hasLmlImg = false;
							if (typeof allMeals[m].mealCategoryLightMeals === 'undefined') hasLmlImg = false;
						}
						if (typeof allMeals[m].mealCategoryPremiumSelections !== 'undefined') {
							prslcsImg = allMeals[m].mealCategoryPremiumSelections[0].mealCategoryImage;
							prslcsDesc = allMeals[m].mealCategoryPremiumSelections[0].mealCategoryDescription;
							prslcsLabel = allMeals[m].mealCategoryPremiumSelections[0].mealMenuCategoryName;
							if (typeof prslcsImg === 'undefined' || prslcsImg === '') hasPrslcsImg = false;
							if (typeof allMeals[m].mealCategoryPremiumSelections === 'undefined') hasPrslcsImg = false;
							isPrslcsPreOrder = (allMeals[m].mealCategoryPremiumSelections[0].isPreOrder == "true") ? true : false;
						}
					}

					var nContentTpl = (!hasBtcImg || !hasInflImg || !hasSpmlImg) ? p.accordionContentTplNoImage : p.accordionContentTpl;
					// Change content based on cabin class
                    if(p.isPey) nContentTpl = p.accordionContentTplPey;
                    if(p.isEco) nContentTpl = p.accordionContentTplEco;

					// Create accordion content
					var contentTpl;
					var isBahMenu = getMealCategory('mealCategoryLightMeals', mealService[k].allMeals);
					var isBahLongMenu = getMealCategory('mealCategoryLongHaul', mealService[k].allMeals);

					if ($('body').hasClass('meals-selection-bah') && isBahMenu) {
						nContentTpl = p.accordionContentTplBah;
					} else if ($('body').hasClass('meals-selection-bah-long') && isBahLongMenu) {
						nContentTpl = p.accordionContentTplBahLong;
					} else if (p.isEco) {
						nContentTpl = p.accordionContentTplEco;
					}

					contentTpl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? p.accordionInfantContentTpl : nContentTpl;

					// check if meal is not available for order
					mealsSelectionAvail = p.curPaxObj.paxType.toLowerCase() != 'infant' ? checkMealsSelAvailable(mealService, k) : {
						'mealsLabel': '',
						'viewOnly': ''
					};

					// Cache inflight setupData
					var inflightData = {};
					if (p.curPaxObj.paxType.toLowerCase() == 'infant') {
						inflightData = mealService[k].allMeals[0].mealCategoryInfant[0];
					} else if (mealService[k].allMeals[0].mealCategoryLongHaul) {
						inflightData = mealService[k].allMeals[0].mealCategoryLongHaul[0];
					} else if (mealService[k].allMeals[1].mealCategoryInflight) {
						inflightData = mealService[k].allMeals[1].mealCategoryInflight[0];
					}

					var targetInflightMenu = i + '-' + j + '-' + k;
					var mealNotReadyMsg = mealAvailData.mealNotReadyMsg;

					var checked = p.curPaxData.isEating ? '' : 'checked';
					var notEatingTxt = p.curPaxData.isEating ? ' hidden' : '';

					var accContent = $(_.template(contentTpl, {
						'origin': data.departureAirportCode,
						'destination': data.arrivalAirportCode,
						'mealType': mealService[k].mealServiceName,
						'mealNotReady': mealAvailData.mealNotReady,
						'mealNotReadyMsg': mealNotReadyMsg,
						'id': p.accordionTabIndex,
						'viewOnly': mealsSelectionAvail.viewOnly,
						'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
						'targetTrigger': p.accordionTabIndex,
						'targetClass': targetInflightMenu,
						'segmentId': i,
						'sectorId': j,
						'paxId': p.curPaxIndex,
						'targetId': i + "" + j + "" + p.curPaxIndex,
						'mealCategoryCode': mealCategoryCode,
						'mealServiceCode': mealService[k].mealServiceCode,
						'checked': checked,
						'notEatingTxt': notEatingTxt,
						'btcImg': btcImg,
						'inflImg': inflImg,
						'spmlImg': spmlImg,
						'htmImg': htmImg,
						'lmlImg': lmlImg,
						'prslcsImg': prslcsImg,
						'btcLabel': btcLabel,
						'inflLabel': inflLabel,
						'spmlLabel': spmlLabel,
						'htmLabel': htmLabel,
						'lmlLabel': lmlLabel,
						'prslcsLabel': prslcsLabel,
						'btcDesc': btcDesc,
						'inflDesc': inflDesc,
						'spmlDesc': spmlDesc,
						'htmDesc': htmDesc,
						'lmlDesc': lmlDesc,
						'prslcsDesc': prslcsDesc,
						'isPrslcsPreOrder': isPrslcsPreOrder,
						'inflNotReady': inflNotReady,
						'btcNotReady': btcNotReady,
						'spmNotReady': spmNotReady
					}));

					if (inflightData.isMealSelectionAvailable == 'false' && inflightData.isViewMenuEligible == 'false') {
						p.notReadyMeals.push(accContent);
					}
					
					if (p.serviceType == 'mealServicesInfant') { // paint infant menu
						var infantMealCat = getMealCategory('mealCategoryInfant', mealService[k].allMeals);
						var infm = new SIA.InfantMeal(infantMealCat,
							p.data,
							data.departureAirportCode,
							data.arrivalAirportCode,
							accContent,
							accordionContentWrap,
							i, j, k,
							p.curPaxIndex,
							mealServiceCode,
							mealService[k],
							p.accordionTabIndex,
							p.body);
						infm.init();
						p.infml.push(infm);
					} else {
						// Init Inflight Menu
						var inflightMealCat = getMealCategory('mealCategoryInflight', mealService[k].allMeals);
						if (inflightMealCat) {
							var ifm = new SIA.InflightMenu(inflightMealCat,
								p.data,
								accContent,
								accordionContentWrap,
								i, j, k,
								p.curPaxIndex,
								mealServiceCode,
								mealService[k],
								p.accordionTabIndex,
								p.body);
							ifm.init();
							p.ifml.push(ifm);
						}
						// Init book the cook menu
						var btcMealCat = getMealCategory('mealCategoryBTC', mealService[k].allMeals);
						if (btcMealCat) {
							var btcm = new SIA.BookTheCook(
								btcMealCat,
								p.inflightDishIcons,
								null,
								null,
								i, j, p.curPaxIndex,
								mealService[k].mealServiceCode,
								p.data,
								data.departureAirportCode,
								data.arrivalAirportCode,
								mealService[k].mealServiceName,
								(p.accordionTabIndex + 1));

							btcm.init(i + '-' + j + '-' + k, p.body);
							p.btcml.push(btcm);
						}
						// Init special meals menu
						var spmMealCat = getMealCategory('mealCategorySPML', mealService[k].allMeals);
						if (spmMealCat) {
							var spm = new SIA.SpecialMeal(
								spmMealCat,
								p.inflightDishIcons,
								null,
								null,
								i, j, p.curPaxIndex,
								mealService[k].mealServiceCode,
								p.data,
								data.departureAirportCode,
								data.arrivalAirportCode,
								mealService[k].mealServiceName,
								(p.accordionTabIndex + 1),
								accContent);
							spm.init(i + '-' + j + '-' + k, p.body);
							p.spml.push(spm);
						}
						var hotMealsCat = getMealCategory('mealCategoryHotMeals', mealService[k].allMeals);
						if (hotMealsCat) {
							SIA.BahMealMenu(hotMealsCat, 
								data.departureAirportCode, 
								data.arrivalAirportCode, 
								i, 
								j, 
								p.curPaxIndex, 
								mealService[k].mealServiceName,
								mealService[k].mealServiceCode,
								p.curPaxData,
								p.data.paxList[p.curPaxIndex].paxName,
								accContent
							);
						}
						var lightMealsCat = getMealCategory('mealCategoryLightMeals', mealService[k].allMeals);
						if (lightMealsCat) {
							SIA.BahMealMenu(lightMealsCat, 
								data.departureAirportCode, 
								data.arrivalAirportCode, 
								i, 
								j, 
								p.curPaxIndex, 
								mealService[k].mealServiceName,
								mealService[k].mealServiceCode,
								p.curPaxData,
								p.data.paxList[p.curPaxIndex].paxName,
								accContent
							);
						}
						var premiumMealsCat = getMealCategory('mealCategoryPremiumSelections', mealService[k].allMeals);
						if (premiumMealsCat) {
							SIA.BahMealMenu(premiumMealsCat, 
								data.departureAirportCode, 
								data.arrivalAirportCode, 
								i, 
								j, 
								p.curPaxIndex, 
								mealService[k].mealServiceName,
								mealService[k].mealServiceCode,
								p.curPaxData,
								p.data.paxList[p.curPaxIndex].paxName,
								accContent
							);
						}
						var bahLongMenu = getMealCategory('mealCategoryLongHaul', mealService[k].allMeals);
						if (bahLongMenu) {
							SIA.BahMealMenuLong(bahLongMenu, 
								data.departureAirportCode, 
								data.arrivalAirportCode, 
								i, 
								j, 
								p.curPaxIndex, 
								mealService[k].mealServiceName,
								mealService[k].mealServiceCode,
								p.curPaxData);
						}
					}

					p.accordContentArr.push(accContent);

					// add the trigger and content in the wrapper
					accItemWrap.append(accTrigger);
					accItemWrap.append(accContent);

					// check for past meals
					if(typeof p.json.paxList[p.curPaxIndex].pastMeals !== 'undefined') {
						var pastMeals = p.json.paxList[p.curPaxIndex].pastMeals[mealService[k].mealServiceCode];
						if(p.json.paxList[p.curPaxIndex].paxType.toLowerCase() !== 'infant' && (p.isBiz || p.isFirst) && typeof pastMeals !== 'undefined') {
							paintPastMeals(accContent, pastMeals, i, j, k);
						}
					}

					// Add accordion item wrapper to accordion group wrapper
					accordionContentWrap.append(accItemWrap);

					p.accordionTabIndex++;

					defaultSelection(accItemWrap);

					accContent.find('.inflight-menu.meals-menu-item').on({
						'click': function(e){
							if($(this).find('.meal-inf-ecopey').length) {
								e.preventDefault();
								e.stopImmediatePropagation();
								chooseEPeInflightMenu($(this));
							}else {
								return;
							}
						}
					});
					
				}
			}

			// Add accordion-wrapper block
			flSegment.append(accordionWrap);

			// Add flight segment to accordion wrapper
			p.accordionWrapper.append(flSegment);

			// add accordion wrapper to mealSelection block
			$('#mealSelection .block-inner').prepend(p.accordionWrapper);
		}

		// Add checkbox listeners
		addCheckboxEvents();

		disableSecSegAvailable();

		// init popup when repainting on pax navigation
		initTriggerPopup();

		// add modal close listener
		onModalClose();

		// remove click listener for popups on not available meals
		for (var i = 0; i < p.notReadyMeals.length; i++) {
			p.notReadyMeals[i].find('[data-trigger-popup]meal-selection-off').off();
			var target = p.notReadyMeals[i].find('[data-trigger-popup].meal-selection-off').attr('data-trigger-popup');

			// remove after being painted
			setTimeout(function () {
				$('body').find(target).remove();
			}, 100);
		}

		p.accordion = new SIA.Accordion();
		p.accordion.init();
	};

	var deselectEcoPeyMenu = function(el){
		var mainMenuLbl = $('[data-accordion-index="' + el.attr('data-main-menu-target') + '"]');
		var wrapper = $(mainMenuLbl.find('[data-trigger-popup]').attr('data-trigger-popup'));
		if(el.hasClass('not-eating')) wrapper = el.closest('.popup-meals.menu-list');

		var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
		var inflightBadge = wrapper.find(".inflight-badge");
		
		inflightBadge.addClass("hidden");
		inflightBadge.removeClass("badge-active");
		wrapperInflightMenu.removeClass("selected-border");
		wrapperInflightMenu.find(".select-label").removeClass("hidden");
	};

	var removeInputData = function (parent) {
		var input = $("#mealSelection").find("[type='hidden']");
		var btnElem = $(parent.find(".fm-select-btn").get(0));
		var paxId = btnElem.data("paxid");
		var segment = btnElem.data("segment");
		var mealCode = btnElem.data("meal-servicecode");

		var pax = JSON.parse(input.val());
		pax.paxList[paxId].selectedMeals[segment].selectedMeal[mealCode] = {
			"isEating": true,
			"isALaCarte": false,
			"mealCategoryCode": null,
			"mealCode": null,
			"mealMenuName": null,
			"mealPrice": null,
			"aLaCarteData": null
		}

		input.val(JSON.stringify(pax));
		p.data.paxList = pax.paxList;
	};

	var removeButtonListener = function() {
        setTimeout(function() {
            $("[data-accordion-trigger]").find("[data-remove-trigger]").off()
                .on({
					'click' : function(evt) {
						var self = $(this);
						var wrapper = self.closest(".accordion-box");
						
						// Trace the modal window containers
						var segmentWrapper = wrapper.closest(".accordion-wrapper-content");
						var wrapperBtn = $(wrapper.find('[data-trigger-popup]'));
						var wrapperTarget = $(wrapperBtn.attr('data-trigger-popup'));
						var wrapMenuModal = $(wrapperTarget.find('[data-trigger-popup]'));
						var prompt = wrapper.find('.remove-prompt');

						var pax = p.data.paxList[p.curPaxIndex];
						var segmentMeals = Object.values(pax.selectedMeals[self.attr('data-segment')].selectedMeal);
						var segmentMeal = _.findWhere(segmentMeals, {isEating: true, mealCategoryCode: "SPML"});
						
						if(segmentMeal && (segmentMeal.mealMenuName + ' (Special Meals)') !== self.prev().text()) {
							prompt.find('.menu-label').text(segmentMeal.mealMenuName);
						} else {
							prompt.find('.menu-label').text('Inflight Menu');
						}

						self.addClass('hidden');
						
						prompt.removeClass('hidden');
						prompt.find('.btn-proceed').off().on({
							'click': function(){
								self.removeClass('hidden');

								if (true) {
									prompt.addClass('hidden');
									var resultSet  = _.map(segmentWrapper.find("[data-trigger-popup]"), function(_self) {
										var mealType = { type: "INF" };
										_self = $(_self);
										var menuTarget = $(_self.data("trigger-popup"));
										_.each(menuTarget.find("[data-trigger-popup]"), function(__self) {
											__self = $(__self);
											var triggerClassname = __self.data("trigger-popup");
											var mealTarget = $(triggerClassname).find(".fm-inf-menu.selected");
											var selectedBtn = mealTarget.find(".fm-select-btn");
											if (/btc-menu/.test(triggerClassname) && selectedBtn.length) {
												mealType = {
													type: "BTC",
													button: selectedBtn
												};
											} else if (/spm-menu/.test(triggerClassname) && selectedBtn.length) {
												mealType = {
													type: "SPM",
													button: selectedBtn
												};
											}
											mealType.parent = $(triggerClassname);
											mealType.isSelf = _self.is(wrapperBtn);
										});
										mealType.wrapper = _self.closest(".accordion-box");
										return mealType;
									});
									var findSelfButton = _.findWhere(resultSet, {isSelf: true});
									var filterOthers = _.where(resultSet, {isSelf: false});
									var infOther = _.findWhere(filterOthers, {type: "INF"});
									var spmOther = _.findWhere(filterOthers, {type: "SPM"});
									if (findSelfButton.type == "SPM" && spmOther) {
										_.each(resultSet, function(result) { 
											if (result.type == "SPM") {
												removeInputData(result.parent);
												resetMeals(result.button, ['ifm', 'btcm', 'spm']);
												deselectPastMeal(findSelfButton);
												changeAccordionLabel(result.wrapper, $(p.inflightLabel), true);
											}
										});
									} else if (findSelfButton.type == "BTC" && spmOther) {
										var code = spmOther.button.data("meal-mealcode");
										var btn = findSelfButton.parent.find("[data-meal-mealcode='"+code+"']");
										var tpl = $(_.template(p.selectedLabel, {
											mealName: btn.data("meal-menuname"),
											mealType: p.mealDictionary[btn.data("meal-category")]
										}));
										resetMeals(findSelfButton.button, ['ifm', 'btcm', 'spm']);
										deselectPastMeal(findSelfButton);
										selectMeal(btn);
										updateData(btn);
										changeAccordionLabel(findSelfButton.wrapper, tpl);

										// after reverting to spml, select past meal again
										selectPastMeal(btn);
									} else if ((findSelfButton.type == "BTC" || findSelfButton.type == "SPM") && infOther) {
										removeInputData(findSelfButton.parent);
										resetMeals(findSelfButton.button, ['ifm', 'btcm', 'spm']);
										deselectPastMeal(findSelfButton);
										changeAccordionLabel(findSelfButton.wrapper, $(p.inflightLabel), true);
									}

									return; 
								}
			
								var modalTargets = [];
								wrapMenuModal.each(function(){
									if(!$(this).hasClass('meal-selection-off')) modalTargets.push($(this).attr('data-trigger-popup'));
								});
								
								var wrapperInflightMenu = wrapperTarget.find(".ml-inflight-container");
								var inflightBadge = wrapperTarget.find(".inflight-badge");
								
								inflightBadge.removeClass("hidden");
								inflightBadge.addClass("badge-active");
								wrapperInflightMenu.addClass("selected-border");
								wrapperInflightMenu.find(".select-label").addClass("hidden");
								
								for(var i = 0; i < modalTargets.length; i ++) {
									$(modalTargets[i]).find(".fm-inf-menu.selected").each(function() {
										var self = $(this);
										var mealButton = self.find(".fm-select-btn");
										// unSelectMeal(mealButton);
										mealButton.trigger('click');
										undoMeal = mealButton;
									});
								}
			
								wrapper.find(".meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
								
								wrapper.find('[data-remove-trigger]').addClass('hidden');
							}
						});
						prompt.find('.btn-cancel').off().on({
							'click': function(){
								prompt.addClass('hidden');
								self.removeClass('hidden');
							}
						});

						// removeInputData(wrapper);
						evt.preventDefault();
						evt.stopPropagation();
					},
					'proceed' : function(evt) {
						var self = $(this);
						var wrapper = self.closest(".accordion-box");
						
						// Trace the modal window containers
						var segmentWrapper = wrapper.closest(".accordion-wrapper-content");
						var wrapperBtn = $(wrapper.find('[data-trigger-popup]'));
						var wrapperTarget = $(wrapperBtn.attr('data-trigger-popup'));
						var wrapMenuModal = $(wrapperTarget.find('[data-trigger-popup]'));
						var prompt = wrapper.find('.remove-prompt');

						self.removeClass('hidden');
						
						if (true) {
							prompt.addClass('hidden');
							var resultSet  = _.map(segmentWrapper.find("[data-trigger-popup]"), function(_self) {
								var mealType = { type: "INF" };
								_self = $(_self);
								var menuTarget = $(_self.data("trigger-popup"));
								_.each(menuTarget.find("[data-trigger-popup]"), function(__self) {
									__self = $(__self);
									var triggerClassname = __self.data("trigger-popup");
									var mealTarget = $(triggerClassname).find(".fm-inf-menu.selected");
									var selectedBtn = mealTarget.find(".fm-select-btn");
									if (/btc-menu/.test(triggerClassname) && selectedBtn.length) {
										mealType = {
											type: "BTC",
											button: selectedBtn
										};
									} else if (/spm-menu/.test(triggerClassname) && selectedBtn.length) {
										mealType = {
											type: "SPM",
											button: selectedBtn
										};
									}
									mealType.parent = $(triggerClassname);
									mealType.isSelf = _self.is(wrapperBtn);
								});
								mealType.wrapper = _self.closest(".accordion-box");
								return mealType;
							});
							var findSelfButton = _.findWhere(resultSet, {isSelf: true});
							var filterOthers = _.where(resultSet, {isSelf: false});
							var infOther = _.findWhere(filterOthers, {type: "INF"});
							var spmOther = _.findWhere(filterOthers, {type: "SPM"});
							if (findSelfButton.type == "SPM" && spmOther) {
								_.each(resultSet, function(result) { 
									if (result.type == "SPM") {
										removeInputData(result.parent);
										resetMeals(result.button, ['ifm', 'btcm', 'spm']);
										changeAccordionLabel(result.wrapper, $(p.inflightLabel), true);
									}
								});
							} else if (findSelfButton.type == "BTC" && spmOther) {
								var code = spmOther.button.data("meal-mealcode");
								var btn = findSelfButton.parent.find("[data-meal-mealcode='"+code+"']");
								var tpl = $(_.template(p.selectedLabel, {
									mealName: btn.data("meal-menuname"),
									mealType: p.mealDictionary[btn.data("meal-category")]
								}));
								resetMeals(findSelfButton.button, ['ifm', 'btcm', 'spm']);
								selectMeal(btn);
								updateData(btn);
								changeAccordionLabel(findSelfButton.wrapper, tpl);
							} else if ((findSelfButton.type == "BTC" || findSelfButton.type == "SPM") && infOther) {
								removeInputData(findSelfButton.parent);
								resetMeals(findSelfButton.button, ['ifm', 'btcm', 'spm']);
								changeAccordionLabel(findSelfButton.wrapper, $(p.inflightLabel), true);
							}
							return; 
						}
	
						var modalTargets = [];
						wrapMenuModal.each(function(){
							if(!$(this).hasClass('meal-selection-off')) modalTargets.push($(this).attr('data-trigger-popup'));
						});
						
						var wrapperInflightMenu = wrapperTarget.find(".ml-inflight-container");
						var inflightBadge = wrapperTarget.find(".inflight-badge");
						
						inflightBadge.removeClass("hidden");
						inflightBadge.addClass("badge-active");
						wrapperInflightMenu.addClass("selected-border");
						wrapperInflightMenu.find(".select-label").addClass("hidden");
						
						for(var i = 0; i < modalTargets.length; i ++) {
							$(modalTargets[i]).find(".fm-inf-menu.selected").each(function() {
								var self = $(this);
								var mealButton = self.find(".fm-select-btn");
								// unSelectMeal(mealButton);
								mealButton.trigger('click');
								undoMeal = mealButton;
							});
						}
	
						wrapper.find(".meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span>');
						
						wrapper.find('[data-remove-trigger]').addClass('hidden');

						// removeInputData(wrapper);
						evt.preventDefault();
						evt.stopPropagation();
					}
                });
        }, 500);
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	
	var changeAccordionLabel = function(wrapper, el, hideBin) {
		wrapper.find(".meal-sub-title-black").html("");
		wrapper.find(".meal-sub-title-black").append(el);
		wrapper.find("[data-remove-trigger]")[hideBin ? "addClass" : "removeClass"]("hidden");
	};

	var defaultSelection = function(wrapper) {
		wrapper.find('[data-trigger-popup]').each(function(){
			var t = $(this).parent().parent();
			
			var targetModalMenu = $(t.find('[data-trigger-popup]').attr('data-trigger-popup'));
			var wrapperInflightMenu = t.find(".ml-inflight-container");
			var inflightBadge = t.find(".inflight-badge");

			if (targetModalMenu.find(".fm-inf-menu.selected").length < 1) {
				inflightBadge.removeClass("hidden");
				// inflightBadge.addClass("badge-active");
				wrapperInflightMenu.addClass("selected-border");
				wrapperInflightMenu.find(".select-label").addClass("hidden");
			}
		});
	};

	var chooseEPeInflightMenu = function(el) {
        var inflightBadge = el.find(".inflight-badge");
        if (!inflightBadge.hasClass("badge-active")) {
            inflightBadge.addClass("badge-active");
			inflightBadge.removeClass("hidden");
			
            el.find(".ml-inflight-container").addClass("selected-border");
            el.find(".ml-inflight-container").find(".select-label").addClass("hidden");
            
			var modalId = parseInt(el.attr('data-main-index')) + 1;
			var mainMenuLbl = $('[data-accordion-index="' + modalId + '"]');
			mainMenuLbl.find('[data-remove-trigger]').trigger('click');

			if(undoMeal.hasClass('not-eating')) {
				undoMeal.trigger('change', { "undoTrigger": true });
				undoMeal = null;
			}else {
				var prompt = $(p.inflightAlertPrompt);
            	// prompt.insertAfter(el);
				// undoButtonListener(prompt);
			}
            
        }
    };
	
	var undoButtonListener = function(prompt) {
		
        $(".inflight-link-undo").on("click", function(e) {
            e.preventDefault();
			e.stopPropagation();

			prompt.remove();

			undoMeal.trigger('click');
			undoMeal = null;
		});
		
		$(document).off().on('click.promptUndoCheck', function (e) {
			var t = $(e.target);
			if (t.hasClass('passenger-info__text') ||
				t.hasClass('meal-tab-item') ||
				t.hasClass('accordion__control') ||
				t.hasClass('ico-point-d') ||
				t.hasClass('fm-ico-text') ||
				t.hasClass('meal-service-name') ||
				t.hasClass('flight-ind') ||
				t.hasClass('btn-back') ||
				t.hasClass('fm-inf-menu') ||
				t.hasClass('search-btn') ||
				t.hasClass('popup__close') ||
				t.hasClass('lightbox-btn-back')
			) {
				$(".inflight-link-undo").parent().parent().remove();
                $(document).off('click.promptUndoCheck');
			}
        });
    };

	var setMainMenuLbl = function (el, txt, removeBin) {
		var accordionContent = $('[data-accordion-index="' + el.data('main-menu-target') + '"]');
		var mainMenuLbl = accordionContent.find('.meal-sub-title-black');
		mainMenuLbl.html(txt);
		mainMenuLbl.parent().find('[data-remove-trigger]').removeClass('hidden');
		// mainMenuLbl.append(p.removeButton);
		if (removeBin) {
			accordionContent.find("em.ico-delete-2").addClass("hidden");
		}
		removeButtonListener();
	};

	var addCheckboxEvents = function () {
		// get all checkbox for not eating then add change listener
		var checkBox = $('[data-checktype="not-eating"]');
		checkBox.on({
			'change': function (evt, json) {
				var t = $(this);
				var trigger = p.accordTabArr[parseInt(t.attr('data-target-trigger'))].find('.meal-sub-title-black');
				var button = t.parent().parent().next().find('.confirm-not-eating');
				json = (json || {});
				if (json.undoTrigger) {
					t.prop("checked", false);
					t.next().next().addClass('hidden');
					trigger.html('<span class="title-selected">'+trigger.attr('data-label')+'</span>');
					trigger.siblings("em").addClass("hidden");

					return updateData(t);
				}
				if (t.prop('checked')) {
					t.next().next().removeClass('hidden');
					button.parent().removeClass('hidden');

					var forScroll = p.global.vars.isIos ? p.body : p.htmlBody;
					
					// Scroll modal to view the button
					var modalContent = t.parent().parent().parent().parent().parent().parent();
					var isAndroid = navigator.userAgent.toLowerCase().indexOf('android');
  					if(isAndroid > -1) {
						modalContent.scrollTop(modalContent.prop('scrollHeight'));
					}else {
						setTimeout(function(){
							modalContent.stop().animate({
								scrollTop: modalContent.prop('scrollHeight')
							}, 600);
						}, 60);
					}
					var parentWrapper = t.closest(".popup__content");
					var pastMeal = {};
					_.map(parentWrapper.find("[data-trigger-popup]"), function(_self) {
						_self = $(_self);
						var _selfTargetClassName = _self.data("trigger-popup");
						var _selfTarget = $(_selfTargetClassName);
						_.each(_selfTarget.find(".fm-inf-menu.selected"), function(__self) {
							__self = $(__self);
							var btn = __self.find(".fm-select-btn");
							pastMeal = {
								type: btn.data("meal-category"),
								mealCode: btn.data("meal-mealcode"),
								wrapperClass: _selfTargetClassName,
								button: btn
							}
						});
					});
					button.off().on({
						'click': function (e) {
							e.preventDefault();
							var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
							var segmentMeals = pax.selectedMeals[t.data("segment")].selectedMeal;
							segmentMeals[t.data("meal-servicecode")].pastMeal = pastMeal;
							// remove all selected meals
							if (pastMeal.button) {
								resetMeals(pastMeal.button, ['ifm', 'btcm', 'spm'], true);
							}
							// notEatingResetMeals($(this), ['ifm', 'btcm', 'spm']);

							deselectEcoPeyMenu($(this).parent().prev().find('.not-eating'));

							undoMeal = $(this).parent().prev().find('.not-eating');

							// change trigger text to not eating only if user clicks on confirm
							var msg = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + t.attr('data-meal-type').toLowerCase();
							trigger.text(msg);
							trigger.siblings("em").addClass("hidden");

							updateData(t);

							button.parent().addClass('hidden');

							p.frmCnfrm = true;

							// Trigger close button
							$(this).parent().parent().parent().prev().trigger('click');
							p.stickyCloseAll.addClass('hide');
						}
					});
				} else {
					button.parent().removeClass('hidden');

					// Scroll modal to view the button
					var modalContent = t.parent().parent().parent().parent().parent().parent();
					var isAndroid = navigator.userAgent.toLowerCase().indexOf('android');
  					if(isAndroid > -1) {
						modalContent.scrollTop(modalContent.prop('scrollHeight'));
					}else {
						setTimeout(function(){
							modalContent.stop().animate({
								scrollTop: modalContent.prop('scrollHeight')
							}, 600);
						}, 60);
					}

					button.off().on({
						'click': function (e) {
							e.preventDefault();
							var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
							var segmentMeals = pax.selectedMeals[t.data("segment")].selectedMeal;
							var segmentMeal = segmentMeals[t.data("meal-servicecode")];
							if (segmentMeal.pastMeal.type == "BTC") {
								var btn = $(segmentMeal.pastMeal.wrapperClass).find("[data-meal-mealcode='"+segmentMeal.pastMeal.mealCode+"']");
								var tpl = $(_.template(p.selectedLabel, {
									mealName: btn.data("meal-menuname"),
									mealType: p.mealDictionary[btn.data("meal-category")]
								}));
								selectMeal(btn);
								updateData(btn);
								changeAccordionLabel(p.currentOpenModal, tpl);
							} else if (segmentMeal.pastMeal.type == "SPML") {
								var segmentMealValues = Object.values(segmentMeals);
								var findSPMLMeal = _.findWhere(segmentMealValues, {isEating: true, mealCategoryCode: "SPML"});
								if (findSPMLMeal) {
									var btn = $(segmentMeal.pastMeal.wrapperClass).find("[data-meal-mealcode='"+findSPMLMeal.mealCode+"']");
									var tpl = $(_.template(p.selectedLabel, {
										mealName: btn.data("meal-menuname"),
										mealType: p.mealDictionary[btn.data("meal-category")]
									}));
									selectMeal(btn);
									updateData(btn);
									changeAccordionLabel(p.currentOpenModal, tpl);
								} else {
									removeInputData($(segmentMeal.pastMeal.wrapperClass));
									changeAccordionLabel(p.currentOpenModal, $(p.inflightLabel), true);
								}
							} else {
								trigger.html('<span class="title-selected">'+trigger.attr('data-label')+'</span>');
								trigger.siblings("em").addClass("hidden");

								updateData(t);
							}
							t.next().next().addClass('hidden');
							button.parent().addClass('hidden');
							button.off();
							// Trigger close button
							$(this).parent().parent().parent().prev().trigger('click');
							p.stickyCloseAll.addClass('hide');
						}
					});
				}
			}
		});
	};

	var paintPaxNav = function () {
		if (p.paxArr.length > 0) {
			for (var h = 0; h < p.paxArr.length; h++) {
				p.paxArr[h].off();
			}
		}

		p.paxArr = [];

		// cache paxlist
		var paxList = p.json.paxList;

		// Get dynamic pax width
		p.paxWidth = p.win.width() > 414 ? p.paxListWrapper.parent().width() * 0.5 : p.paxListWrapper.parent().width();
		p.paxListWrapper.append($('<h4 class="pax-list-heading">Select passenger</h4>'));
		// Add select field
		p.paxSelect = $('<select id="mealPaxSelect" class="meal-pax-select"></select>');
		p.paxListWrapper.append(p.paxSelect);

		// Loop through pax list and add to DOM
		for (var i = 0; i < paxList.length; i++) {
			var pax;
			if (paxList[i].paxType.toLowerCase() === 'infant') {
				pax = $('<option class="pax-nav" data-id="' + i + '" value="' + paxList[i].paxName + '">' + paxList[i].paxName + ' (Infant)</option>');
			} else {
				pax = $('<option class="pax-nav" data-id="' + i + '" value="' + paxList[i].paxName + '">' + paxList[i].paxName + '</option>');
			}

			// Add active class to current pax
			if (i == p.curPaxIndex) {
				pax.attr('selected', 'selected');
				p.activePax = i;
			}

			p.paxSelect.append(pax);
			p.paxArr.push(pax);
		}

		// Add conditionals for different pax counts
		if (paxList.length === 1) {
			p.paxListWrapper.parent().addClass('hidden');
			$('#main-inner').find('.main-heading').text('Meal selection');
		} else {
			// initialise pax nav event listeners
			// initPaxNav();
			addPaxNavListeners();
		}

	};

	var initPaxNav = function () {
		// Get reference to the buttons
		p.paxLt = p.paxListWrapper.prev();
		p.paxRt = p.paxListWrapper.next();

		if (typeof p.paxListWrapper.slick == 'function') {
			// Get reference to the buttons
			p.paxLt = p.paxListWrapper.prev();
			p.paxRt = p.paxListWrapper.next();

			p.paxListWrapper.slick({
				dots: false,
				infinite: false,
				responsive: [{
					breakpoint: 768,
					settings: {
						slidesToShow: 2
					}
				}, {
					breakpoint: 415,
					settings: {
						slidesToShow: 1
					}
				}],
				slidesToShow: 1,
				speed: 600,
				draggable: true,
				slidesToScroll: 1,
				accessibility: true,
				arrows: true,
				prevArrow: p.paxLt,
				nextArrow: p.paxRt,
				initialSlide: p.curPaxIndex,
				zIndex: 0
			}).on({
				'afterChange': function (event, slick, currentSlide, nextSlide) {
					if (!$(slick.$slides[0]).hasClass('slick-active')) {
						p.paxLt.show();
						p.paxListWrapper.parent().removeClass('pax-start');
					} else {
						p.paxLt.hide();
						p.paxListWrapper.parent().addClass('pax-start');
					}

					if (!$(slick.$slides[slick.$slides.length - 1]).hasClass('slick-active')) {
						p.paxRt.show();
						p.paxListWrapper.parent().removeClass('pax-end');
					} else {
						p.paxRt.hide();
						p.paxListWrapper.parent().addClass('pax-end');
					}
				},
				'beforeChange': function (event, slick, currentSlide, nextSlide) {
					// // trigger slide change if portrait
					var ua = navigator.userAgent.toLowerCase();
					var isAndroid = ua.indexOf("android") > -1;

					if (isAndroid) {
						var o = window.screen.orientation.type;
						if (o === "portrait-secondary" || o === "portrait-primary") {
							p.paxArr[nextSlide].find('a').trigger('click');
						}
					} else {
						var o = window.orientation;
						if (o === 0 || o === 180) {
							p.paxArr[nextSlide].find('a').trigger('click');
						}
					}
				}
			});

			addPaxNavListeners();

		} else {
			setTimeout(function () {
				initPaxNav();
			}, 10);
		}
	};

	var addPaxNavListeners = function () {
		p.paxListWrapper.prev().hide();
		p.paxListWrapper.next().hide();

		p.paxSelect.on({
			'change': function(){
				var curPax = $(this).find('option:selected');
				p.curPaxIndex = parseInt(curPax.attr('data-id'));
				p.activePax = p.curPaxIndex;

				// scroll the page up to focus on the new menu
				// after scroll show the next accordion wrapper
				var top = p.paxListWrapper.parent().offset().top;
				var bodyTop = p.htmlBody.prop('scrollTop');

				// index the current accordion wrapper
				p.exitWrapper = null;
				p.exitWrapper = p.accordionWrapper;

				// Add class to prepare curobj for transition
				p.exitWrapper.addClass('pre-slide-out');

				// debugger;

				// After adding the exit wrapper reference, paint the next wrapper for the next pax
				paintMealsMenu('pre-slide-in-right');

				showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
			}
		});
		// add listeners on each pax element
		// for (var i = 0; i < p.paxArr.length; i++) {
		// 	p.paxArr[i].find('a').off().on({
		// 		'click': function (e) {
		// 			e.preventDefault();
		// 			e.stopImmediatePropagation();

		// 			var t = $(this);
		// 			if (!t.parent().hasClass('active') && !t.hasClass('is-disabled')) {
		// 				p.curPaxIndex = parseInt(t.attr('data-id'));

		// 				// Reset other pax nav and enable the clicked pax
		// 				resetAllPax(t);

		// 				p.activePax = p.curPaxIndex;

		// 				// scroll the page up to focus on the new menu
		// 				// after scroll show the next accordion wrapper
		// 				var top = p.paxListWrapper.parent().offset().top;
		// 				var bodyTop = p.htmlBody.prop('scrollTop');

		// 				// index the current accordion wrapper
		// 				p.exitWrapper = null;
		// 				p.exitWrapper = p.accordionWrapper;

		// 				// Add class to prepare curobj for transition
		// 				p.exitWrapper.addClass('pre-slide-out');

		// 				// debugger;

		// 				// After adding the exit wrapper reference, paint the next wrapper for the next pax
		// 				paintMealsMenu('pre-slide-in-right');

		// 				showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
		// 			}
		// 		}
		// 	});
		// }
	};

	var resetAllPax = function (obj) {
		for (var i = 0; i < p.paxArr.length; i++) {
			p.paxArr[i].removeClass('active');
		}

		obj.parent().addClass('active');
	};

	var paintPastMeals = function(container, pastMeals, segmentId, sectorId, serviceId) {
		var dataBtn = container.find(".not-eating-supper").find("input");
		var currentMealData = p.data.paxList[p.curPaxIndex].selectedMeals[segmentId].selectedMeal[dataBtn.data("meal-servicecode")];
		var pastMealsWrap = $('\
			<h3 class="fm-title-4--blue">Pick a favourite meal from a past flight</h3>\
			<div class="past-meals-wrap">\
				<button type="button" class="past-meal__btn slider-btn-left"><em class="ico-point-l"></em></button>\
				<div class="past-meals-slider">\
					<ul class="past-meals-container"></ul>\
				</div>\
				<button type="button" class="past-meal__btn slider-btn-right"><em class="ico-point-r"></em></button>\
			</div>\
		');

		var pastMealContainer = pastMealsWrap.find('.past-meals-container');

		for(var i = 0; i < pastMeals.length; i++) {
			var pastMeal = pastMeals[i];
			pastMealContainer.append($(
				'<div data-meal-item class="fm-inf-menu' + ((pastMeal.mealCode == currentMealData.mealCode) ? ' selected' : '') + '">\
					<span class="fm-inf-menu-select-badge">Selected</span>\
					<h4 class="fm-inf-menu-title">'+pastMeal.mealMenuName+'</h4>\
					<span class="fm-inf-menu-sub-title">'+pastMeal.mealMenuCategoryName+'</span>\
					<p class="fm-inf-menu-desc">'+pastMeal.mealMenuDescription+'</p>\
					<div class="fm-footnote-txt">\
						<input type="button" name="" value="' + ((pastMeal.mealCode == currentMealData.mealCode) ? ' Remove' : 'Select') + '" data-mealcattype="speacial-meal-list" class="fm-select-btn past-meal right" data-segment="'+segmentId+'" data-sector="'+sectorId+'" data-paxid="'+p.curPaxIndex+'" data-meal-category="'+pastMeal.mealCategoryCode+'" data-meal-servicecode="'+pastMeal.mealServiceCode+'" data-service-id="'+serviceId+'" data-meal-mealcode="'+pastMeal.mealCode+'" data-meal-menuname="'+pastMeal.mealMenuName+'">\
					</div>\
				</div>'
			))
		}

		var menuHeading = container.find('.fm-title-4--blue');
		if(menuHeading.text() === 'Select a meal') menuHeading.text('Or discover new favourites');

		var firstHr = container.find('.item-container');
		firstHr.before(pastMealsWrap);

		addPastMealsListener(pastMealsWrap.find('.past-meals-container'), pastMealsWrap);
	};
	
	var addPastMealsListener = function(slider, container){
		// Start slick slider
		var btnLeft = container.find('.slider-btn-left');
		var btnRight = container.find('.slider-btn-right');

		var meals = slider.find('.fm-inf-menu');

		var pastMealRemovePrompt = '<div class="remove-pastmeal"></div>'

		// Initialise slick slider based on meals length
		if(meals.length > 1) {
			slider.addClass('past-meals-slick');

			slider.slick({
				dots: true,
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				speed: 400,
				draggable: true,
				accessibility: true,
				arrows: true,
				prevArrow: btnLeft,
				nextArrow: btnRight,
				initialSlide: 0,
				variableWidth: true,
				zIndex: 0,
				adaptiveHeight: false
			}).on({
				'afterChange': function (event, slick, currentSlide, nextSlide) {
					if (!$(slick.$slides[0]).hasClass('slick-active')) {
						btnLeft.show();
					} else {
						btnLeft.hide();
					}

					if (!$(slick.$slides[slick.$slides.length - 1]).hasClass('slick-active')) {
						btnRight.show();
					} else {
						btnRight.hide();
					}
				},
				'breakpoint': function (event, slick, breakpoint) {
					
				}
			});

			// Hide prev button by default
			btnLeft.hide();
			container.find('.slick-dots').before(pastMealRemovePrompt);
		}else {
			slider.addClass('past-meals-no-slick');
			btnLeft.addClass('hidden');
			btnRight.addClass('hidden');

			slider.parent().append(pastMealRemovePrompt);
		}

		// Add click events for each past meal
		meals.each(function(){
			var t = $(this);
			t.off().on({
				'click': function(e){
					e.preventDefault();
					var popupEl = getPastMealPopup(t);
					var btnWrapper = findPastMeal(popupEl, t.find('.fm-select-btn'));
					var parentWrapper = t.closest('.past-meals-wrap');
					var removePrompt = parentWrapper.find('.remove-pastmeal');
					if(t.hasClass('selected')) {
						p.currentOpenModal.find('[data-remove-trigger]').trigger("click");
						p.currentOpenModal.find('.remove-prompt').addClass("hidden");
						var transferPrompt = p.currentOpenModal.find('.remove-prompt').clone(true, true);
						transferPrompt.removeClass("hidden");
						t.find(".fm-select-btn").addClass("hidden");
						parentWrapper.find(".remove-pastmeal").html("").append(transferPrompt);
						transferPrompt
							.find(".btn-proceed")
							.on("click.overrideBtn", function() {
								t.removeClass('selected');
								t.find('.fm-select-btn').val('Select');
								transferPrompt.addClass("hidden");
								t.find(".fm-select-btn").removeClass("hidden");
							});
						transferPrompt
							.find(".btn-cancel")
							.on("click.overrideBtn", function() {
								transferPrompt.addClass("hidden");
								t.find(".fm-select-btn").removeClass("hidden");
							});
					} else {
						removePastMealSelection(t);
						resetMeals(btnWrapper.find('.fm-select-btn'), ['btcm', 'spm', 'ifm']);
						btnWrapper.trigger("click");
						findPromptOpen(btnWrapper);
						t.addClass('selected');
						t.find('.fm-select-btn').val('Remove');
					}
				}
			})
		});
	};

	var removePastMealSelection = function(buttonSelected) {
		var preselected = buttonSelected.closest(".past-meals-container").find(".fm-inf-menu.selected");
		
		preselected.removeClass("selected");
		preselected.find('.fm-select-btn').val('Select');
	};

	var getPastMealPopup = function(btnWrapper) {
		var popupClasses = {
			"SPML": "popup--spm-menu",
			"BTC": "popup--btc-menu",
			"INFM": "popup--inflight-menu"
		};
		var btn = btnWrapper.find('.fm-select-btn');
		var category = btn.data("meal-category");
		var accContent = btnWrapper.closest(".accordion__content");
		var triggers = accContent.find(".promotion-item").find("[data-trigger-popup]");
		var popupClass = _.chain(triggers)
			.filter(function(el) {
				el = $(el);
				var reg = new RegExp(popupClasses[category], "i");
				return reg.test(el.data("trigger-popup"));
			})
			.map(function(el) { return $(el).data("trigger-popup") })
			.value()[0];
		var mealMenuPopup = $(popupClass);
		return mealMenuPopup;
	};

	var findPastMeal = function(popup, btn) {
		var data = btn.data("meal-mealcode");
		var mealBtn = popup.find('[data-meal-mealcode='+data+']');
		return mealBtn.closest(".fm-inf-menu");
	};

	var findPromptOpen = function(btnWrapper) {
		var openPrompt = btnWrapper.find('.confirmation-prompt:not(.hidden):not([data-prompt="initial"])');
		openPrompt.find(".btn-proceed").trigger("click");
	};

	var deselectPastMeal = function(findSelfButton){
		// deselect all past meals with same type in the same segment
		$('.fm-select-btn.past-meal[data-segment="'+findSelfButton.button.attr('data-segment')+'"][data-meal-category="'+findSelfButton.button.attr('data-meal-category')+'"][data-meal-mealcode="'+findSelfButton.button.attr('data-meal-mealcode')+'"]').each(function(){
			var btn = $(this);
			btn.val('Select').parent().parent().removeClass('selected');
		});
	};

	var selectPastMeal = function(meal){
		var curMeal;
		if(meal.attr('data-meal-category') === 'SPML') {
			curMeal = $('.fm-select-btn.past-meal[data-segment="'+meal.attr('data-segment')+'"][data-meal-category="'+meal.attr('data-meal-category')+'"][data-meal-mealcode="'+meal.attr('data-meal-mealcode')+'"]');
		} else {
			curMeal = $('.fm-select-btn.past-meal[data-segment="'+meal.attr('data-segment')+'"][data-meal-category="'+meal.attr('data-meal-category')+'"][data-meal-mealcode="'+meal.attr('data-meal-mealcode')+'"][data-meal-servicecode="'+meal.attr('data-meal-servicecode')+'"]');
		}

		curMeal.closest('.past-meals-container').find('.fm-inf-menu').removeClass('selected').find('.fm-select-btn').val('Select');

		curMeal.each(function(){
			var btn = $(this);
			btn.val('Remove').parent().parent().addClass('selected');
		});
	};

	/* slide out the accordion wrapper
	 */
	var showNextPaxAccordion = function (curObj, nextObj) {
		// Add listener to exitwrapper
		curObj.one('transitionend' || 'webkitTransitionEnd' || 'mozTransitionEnd' || 'oTransitionEnd', function () {
			curObj.remove();
		});

		setTimeout(function () {
			// Add class to exit wrapper to start animating out
			curObj.addClass('slide-out-left');
			// Add class to current accordion wrapper to start animating in
			nextObj.removeClass('pre-slide-in-right');
		}, 10);
	};

	/* check if second segment available for meal selection
	 */
	var secSegmentAvailable = function (flSegment, index) {
		var mealSelWaitListedData = p.json.flightSegment[index];
		var secSegMealSelectionStatus = mealSelWaitListedData.isSegmentAvailableForMealSelection;
		var secSegMealSelectionMsg = mealSelWaitListedData.segmentMealSelectionStatusMessage;

		if (secSegMealSelectionStatus == 'false') {
			var checkinAlert = $(_.template(p.checkinAlert, {
				checknMsg: secSegMealSelectionMsg
			}));

			// add checkin alert to second segment block
			flSegment.append(checkinAlert);

			// change accordion arrow text && icon to gray to disable
			flSegment.addClass('disable-accordion');
		}
	};

	/* disable events for all second segment meals selection
	 */
	var disableSecSegAvailable = function () {
		$('.disable-accordion [data-accordion-trigger="1"]').unbind();

		// remove href attrib to disable clickable
		$('.disable-accordion a').removeAttr('data-trigger-popup');
		$('.disable-accordion a').removeAttr('href').addClass('is-disabled');
	};

	/* Check all meals availability attribute only shows up in inflight meal
	 */
	var mealsAvailable = function (mealService, index) {
		var mealNotReady = '';
		var mealNotReadyMsg = '';

		if (p.serviceType != 'mealServicesInfant') {
			var mealServiceData;
			if (mealService[index].allMeals[0].mealCategoryLongHaul) {
				mealServiceData = mealService[index].allMeals[0].mealCategoryLongHaul[0]
			} else if (mealService[index].allMeals[1].mealCategoryInflight) {
				mealServiceData = mealService[index].allMeals[1].mealCategoryInflight[0];
			} else if (mealService[index].allMeals[1].mealCategoryLightMeals) {
				mealServiceData = mealService[index].allMeals[1].mealCategoryLightMeals[0];
			}
			var isViewMenuEligible = mealServiceData.isViewMenuEligible;
			var isMealSelAvail = mealServiceData.isMealSelectionAvailable;

			mealNotReady = ((isViewMenuEligible == 'false') && (isMealSelAvail == 'false')) ? 'meal-not-ready' : '';
			mealNotReadyMsg = mealServiceData.isViewMenuDate;
		}

		return {
			mealNotReady: mealNotReady,
			mealNotReadyMsg: mealNotReadyMsg
		};
	};

	/* check if meal is not available for order
	 */
	var checkMealsSelAvailable = function (mealService, index) {
		var mealsAvailData;
		if (mealService[index].allMeals[0].mealCategoryLongHaul) {
			mealsAvailData = mealService[index].allMeals[0].mealCategoryLongHaul[0]
		} else if (mealService[index].allMeals[1].mealCategoryInflight) {
			mealsAvailData = mealService[index].allMeals[1].mealCategoryInflight[0];
		} else if (mealService[index].allMeals[1].mealCategoryLightMeals) {
			mealsAvailData = mealService[index].allMeals[1].mealCategoryLightMeals[0];
		}
		var isViewMenuEligible = mealsAvailData.isViewMenuEligible;
		var isMealSelAvail = mealsAvailData.isMealSelectionAvailable;

		if ((isViewMenuEligible == 'true') && (isMealSelAvail == 'false')) {
			return {
				'mealsLabel': p.mealsLabelTpl,
				'viewOnly': 'view-only'
			};
		} else {
			return {
				'mealsLabel': '',
				'viewOnly': ''
			};
		}
	};

	/* get current pax type if adult, child or infant
	 */
	var getServiceType = function (paxIndex) {
		p.curPaxObj = p.json.paxList[paxIndex];
		serviceType = '';
		switch (p.curPaxObj.paxType.toLowerCase()) {
			case 'adult':
				serviceType = 'mealServicesAdult';
				break;
			case 'child':
				serviceType = 'mealServicesChild';
				break;
			case 'infant':
				serviceType = 'mealServicesInfant';
				break;
			default:
				serviceType = 'mealServicesAdult';
		}

		return serviceType;
	};

	var getMealCategory = function (type, arr) {
		for (var i = 0; i < arr.length; i++) {
			if (typeof arr[i][type] != 'undefined') {
				return arr[i][type];
			}
		}
	};

	var notEatingResetMeals = function (el, meals) {
		var id = el.closest('aside').attr('class').split(' ')[4].slice(-1);

		var indexes = {
			'0': '0-0-0',
			'1': '0-1-0',
			'2': '0-1-1',
			'3': '1-0-0',
			'4': '1-0-1',
			'5': '1-1-0',
			'6': '1-1-1',
		}

		var curIndex = indexes[id];

		if(!el.hasClass('not-eating')){
			var checkBox = el.parent().prev().find('.not-eating');
			curIndex = checkBox.attr('data-segment') + '-' + checkBox.attr('data-sector') + '-'  + checkBox.attr('data-paxid');
		}

		removeMealSel(meals, curIndex);
	};

	var removeNotEatingCheck = function (id) {
		var indexes = {
			'0-0-0': '0',
			'0-1-0': '1',
			'0-1-1': '2',
			'1-0-0': '3',
			'1-0-1': '4',
			'1-1-0': '5',
		}

		var notEatingEl = $('body .popup--meals-selection-' + indexes[id]).find('.not-eating-supper');
		notEatingEl.find('.not-eating-msg').addClass('hidden');
		notEatingEl.find('input').prop('checked', false);
		notEatingEl.next().addClass('hidden');
		notEatingEl.find('.fm-cbox').removeClass('checkbox_color');
	}

	var removeNotEatingCheckSPML = function (input) {
		var targetMain = p.container.find('[data-accordion-index="'+ input.attr('data-main-menu-target') +'"]');
		
		var notEatingEl = $('body ' + targetMain.find('[data-trigger-popup]').attr('data-trigger-popup')).find('.not-eating-supper');
		notEatingEl.find('.not-eating-msg').addClass('hidden');
		notEatingEl.find('input').prop('checked', false);
		notEatingEl.next().addClass('hidden');
		notEatingEl.find('.fm-cbox').removeClass('checkbox_color');
	}

	var resetMeals = function (el, meals, isChecking) {
		var id = el.closest('aside').attr('class').split(' ')[3].slice(-5);

		removeMealSel(meals, id);
		if (!isChecking) {
			removeNotEatingCheck(id);
		}
	};

	var removeMealSel = function (meals, id) {
		var inputBtn = ' .selected .fm-select-btn';
		var mealList = {
			'btcm': '.popup--btc-menu-',
			'ifm': '.popup--inflight-menu-',
			'spm': '.popup--spm-menu-'
		};

		$.each(meals, function (i, meal) {
			var inputSel = $('body ' + (mealList[meal] + id) + inputBtn);
			inputSel.closest('[data-meal-item]').removeClass('selected');
			inputSel.attr('value', 'Select');
		});
	}

	// Copied from script.js
	var initTriggerPopup = function () {
		var triggerPopup = $('[data-trigger-popup]');
		var flyingFocus = $('#flying-focus');

		triggerPopup.each(function () {
			var self = $(this);
			
			if (typeof self.data('trigger-popup') === 'boolean' || self.hasClass('meal-selection-off')) {
				return;
			}
			var popup = $(self.data('trigger-popup'));
			if (!popup.data('Popup')) {
				popup.Popup({
					overlayBGTemplate: p.config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '[data-close]',
					afterShow: function() {
						$("[data-accordion-trigger]").trigger("click.removeHandleEvent");
						p.stickyCloseAll.removeClass('hide');

						p.openModals = [];

						$('.popup.popup-meals').each(function(){
							if(!$(this).hasClass('hidden')) {
								p.openModals.push($(this));
							}
						});

						if(p.openModals.length > 1) {
							p.openModals[0].addClass('temp-hidden');
						}
					},
					beforeShow: function() {
						p.curScrollTop = $(window).scrollTop() !== 0 ? $(window).scrollTop() : p.curScrollTop;

						p.stickyCloseAll.find('.popup-meal-type').html(popup.attr('data-mealtype'));
						p.stickyCloseAll.find('.popup-title-origin').html(popup.attr('data-origin'));
						p.stickyCloseAll.find('.popup-title-dest').html(popup.attr('data-dest'));
						onModalOpen(self);
						if (popup.hasClass("menu-type-list")) {
							onModalPopupTypeList(popup);
						}
					},
					beforeHide: function(){
						if(p.openModals.length > 1) {
							p.openModals[0].removeClass('temp-hidden');
						}

					},
					afterHide: function() {
						var popupTriggered = $(".popup.animated:not(.temp-hidden)");
						if (popupTriggered.hasClass("menu-type-list")) {
							onModalPopupTypeList(popupTriggered);
						}
					}
				});
			}
			self.off('click.showPopup').on('click.showPopup', function (e) {
				e.preventDefault();

				if (!self.data('isDisabledStopIm')) {
					e.stopImmediatePropagation();
				}
				if (!self.hasClass('disabled')) {
					popup.Popup('show');
				}
			});
		});
	};

	var onModalPopupTypeList = function(el) {
		var pax = SIA.MealsSelection.public.data.paxList[p.curPaxIndex];
		var btn = el.find(".custom-checkbox").find("input");
		var segmentMeals = pax.selectedMeals[btn.data("segment")].selectedMeal[btn.data("meal-servicecode")];
		el.find(".inflight-badge").each(function() {
			$(this).addClass("hidden").removeClass("badge-active").closest(".meal-label").removeClass("ml-inflight-container selected-border");
		});
		if (segmentMeals.mealCategoryCode == "SPML" && segmentMeals.isEating) {
			var mealLabel = el.find(".special-meals").find(".meal-label");
			mealLabel.addClass("ml-inflight-container selected-border");
			mealLabel.find(".inflight-badge")
				.removeClass("hidden")
				.addClass("badge-active");
		} else if (segmentMeals.mealCategoryCode == "BTC" && segmentMeals.isEating) {
			var mealLabel = el.find(".book-the-cook-menu").find(".meal-label");
			mealLabel.addClass("ml-inflight-container selected-border");
			mealLabel.find(".inflight-badge")
				.removeClass("hidden")
				.addClass("badge-active");
		} else {
			el.find(".meal-label").each(function() {
				$(this).removeClass("ml-inflight-container selected-border");
			});
		}
	};

	var onModalClose = function(){
		$('.menu-type-list .popup__close').on({
			'click': function(){
				var t = $(this);
				var confirmBtn = t.next().find('.button-group-1.btn-confirm');
				if(!confirmBtn.hasClass('hidden') && !p.frmCnfrm) {
					confirmBtn.prev().find('.not-eating').trigger('click');
				}
			}
		});
	};

	var checkCabinClass = function(curCC, classArr){
        var isMatch = false;
        for (var i = 0; i < classArr.length; i++) {
            var cc = classArr[i];
            if(curCC === cc) {
                isMatch = true;
                break;
            }
        }

        return isMatch;
	};
	
	// Add close all utility
	var initCloseAllPopup = function(){
		p.container.append(p.stickyCloseAll);

		$('[data-close-all]').on('click', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();
			$('body').find('.popup-meals [data-close]').trigger('click');

			$('body').find('.popup__content > div').each(function(){
				$(this).scrollTop(0);
			});

			$('.sticky-confirm-bar').addClass('no-selection');

			$(window).scrollTop(p.curScrollTop);

			p.stickyCloseAll.addClass('hide');
		});
	};

	var oPublic = {
		init: init,
		updateData: updateData,
		resetMeals: resetMeals,
		removeNotEatingCheckSPML: removeNotEatingCheckSPML,
		setMainMenuLbl: setMainMenuLbl,
		deselectEcoPeyMenu: deselectEcoPeyMenu,
		removeButtonListener: removeButtonListener,
		removeInputData: removeInputData,
		public: p,
		initCloseAllPopup: initCloseAllPopup,
		removePastMealSelection: removePastMealSelection,
		selectPastMeal: selectPastMeal
	};
	return oPublic;
}();

SIA.clickActivity = function() {
	var initClick = function() {
		var elList = [
			".lightbox-btn-back",
			".popup__close"
		];
		$(elList.join(",")).on("click", function(evt) {
			hideAlertPrompt();
		});
	};

	var hideAlertPrompt = function() {
		$("body")
			.find('.popup-meals')
			.find(".confirmation-prompt:not(.hidden)")
			.each(function() {
				var $this = $(this);
				$this.addClass("hidden");
				$this.closest(".meal-of-bah:not(.selected)").find(".fm-select-btn").val("Select");
			});
	};

	var init = function() {
		initClick();
	};

	return {
		init: init,
		hideAlertPrompt: hideAlertPrompt
	}
}();

SIA.InflightMenu = function (
	jsonData,
	paxData,
	accContent,
	accordionContentWrap,
	segmentId,
	sectorId,
	serviceId,
	paxId,
	mealServiceCode,
	mealService,
	accordionTabIndex,
	body) {

	var p = {};

	// Inflight Meals TPL
	p.inflightMainTpl = '<aside class="popup popup-meals menu-list-inflight popup--inflight-menu-<%= targetClass %> hidden">\
		<div class = "popup__inner">\
			 <div class="popup__content">\
				  <p class="light-box-btn-mb"><a href="#" class="lightbox-btn-back" data-close="true"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to categories</a></p>\
				  <div class="alert-block info-box<%= viewOnlyAlert %>">\
					 <div class="inner">\
						<div class="alert__icon"><em class="ico-tooltips tool_alert"></em></div>\
						<div class="alert__message font_color alert-text-color-blue"><%= viewOnlyMsg %></div>\
					 </div>\
				  </div>\
				  <h3 class="fm-title-4--blue"><%= inflightTitle %></h3>\
				  <p class="light-box-text"><%= mealDesc %></p>\
				  <%= mealTypeSub %>\
				  <div class="item-wrapper"></div>\
			 </div>\
		  </div>\
		  <div class="sticky-confirm-bar no-selection">\
			<button class="btn-1" data-close-all="true">Done</button>\
		  </div>\
		</aside>';

	p.inflightMenuItemTpl = '<article data-meal-item class="promotion-item inflight-menu-item--1 menu-item<%= selectedClass %>">\
		<span class="fm-inf-menu-select-badge">Selected</span>\
		<div class="fm-inf-menu">\
			 <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
			 <p class="fm-inf-menu-desc"><%= mealMenuDescription %></p>\
			 <div class="fm-footnote-txt">\
				  <span class="food-icon-set"><%= foodIconSet %></span>\
				  <input type="button" name="select" value="<%= inputVal %>" class="fm-select-btn right<%= selectClass %>" data-main-menu-target="<%= triggerId %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>" data-label="<%= mealMenuName %>">\
				  <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
						<span class="btn-small view-only-bg view-box btn-view-only">VIEW ONLY</span>\
						<span class="onboard">Order on board</span>\
				  </div>\
			 </div>\
		</div>\
		</article>';

	p.inflightMenuSpecialItemTpl = '<article data-meal-item class="promotion-item inflight-menu-item--1 menu-item<%= selectedClass %>">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="fm-ysh-choice"><%= specialMealName %></span>\
			 <div class="fm-inf-menu">\
				  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
				  <p class="fm-inf-menu-desc"><%= mealMenuDescription %></p>\
				  <div class="fm-table-wrapper">\
					 <div class="fm-table-left">\
						  <table class="fm-table-inner table-responsive">\
								<thead class="hidden-mb">\
								</thead>\
								<tbody class="menu-info"></tbody>\
						  </table>\
					 </div>\
				  </div>\
				  <div class="fm-footnote-txt">\
						<span class="food-icon-set"><%= foodIconSet %></span>\
						<input type="button" name="select" value="<%= inputVal %>" class="fm-select-btn right<%= selectClass %>" data-main-menu-target="<%= triggerId %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>" data-label="<%= mealMenuName %>">\
						<div class="btn-wrap-view-only<%= viewOnlyClass %>">\
							 <span class="btn-small view-only-bg view-box btn-view-only">VIEW ONLY</span>\
							 <span class="onboard">Order on board</span>\
						</div>\
				  </div>\
			 </div>\
		</article>';

	// applied special meal prompt
	p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
			<em class="ico-alert"></em>\
			<p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
		</div>');

	p.selectedALC = [];
	p.isALC = false;

	var init = function () {
		p.mealCatName = jsonData[0].mealMenuCategoryName;
		p.data = SIA.MealsSelection.public.json.flightSegment[segmentId].flightSector[sectorId];

		p.curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealService.mealServiceCode];

		p.isALC = mealServiceCode === 'ACL_110';

		paintInflightMenu();
		SIA.MealsSelection.initCloseAllPopup();
	};

	var paintInflightMenu = function () {
		var targetInflightMenu = segmentId + '-' + sectorId + '-' + serviceId;

		// Check if block is not available
		if (typeof mealAvailData.mealNotReadyMsg != 'undefined') {
			targetClass = '';
		}

		var mealDesc = '';
		var mealTypeSub = '';
		if (p.isALC) {
			mealDesc = 'You can choose to pre-order one option from each meal service now, but there isn&#8217;t fixed time where they&#8217;ll be served the meal(s). Pre-ordering will help to guarantee that you&#8217;ll get some things that you would like to eat. You can also choose to order more from the various meal services when you are on the flight.';
			mealTypeSub = '';
		} else if (mealServiceCode == 'MNB_110') {
			mealDesc = 'You can choose to pre-order one option from each meal service now, but there isn&apos;t a fixed time where they&apos;ll be served the meal(s). Pre-ordering will help you guarantee that you&apos;ll get some things that you would like to eat. You can also choose to order more from the various meal services when you are on the flight.';
			mealTypeSub = '<h3 class="fm-title2-black">Your choice of ' + mealService.mealServiceName.toLowerCase() + '</h3>';
		} else {
			mealDesc = 'Choose a delicious main course from our inflight menu to complete your inflight dining experience.';
			mealTypeSub = '<h3 class="fm-title2-black">Your choice of ' + mealService.mealServiceName.toLowerCase() + '</h3>';
		}

		// Paint Inflight Meal menu during painting
		// Get special attributes while we have indexes available
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = '';
		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
		}

		p.infm = $(_.template(p.inflightMainTpl, {
			'origin': p.data.departureAirportCode,
			'destination': p.data.arrivalAirportCode,
			'mealType': mealService.mealServiceName,
			'mealTypeSub': mealTypeSub,
			'id': SIA.MealsSelection.public.accordionTabIndex,
			'inflightTitle': jsonData[0].mealMenuCategoryName,
			'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
			'targetTrigger': SIA.MealsSelection.public.accordionTabIndex,
			'targetClass': targetInflightMenu,
			'viewOnlyMsg': viewOnlyMsg,
			'viewOnlyAlert': viewOnlyAlert,
			'mealDesc': mealDesc
		}));

		if (p.isALC) {
			paintALaCarte();
		} else {
			addInflightMeal(jsonData[0].mealSubcategory[0], p.infm.find('.item-wrapper'));
		}

		body.append(p.infm);
	};

	var paintALaCarte = function () {
		// Setup a la carte data
		var curPaxALCData = SIA.MealsSelection.public.data.paxList[paxId].selectedMeals[segmentId].selectedMeal['ACL_110'].aLaCarteData;

		if (curPaxALCData === null) {
			p.alcData = {};
			for (var j = 0; j < jsonData[0].mealSubcategory.length; j++) {
				var curData = jsonData[0].mealSubcategory[j];
				p.alcData[curData.mealSubCategoryCode] = {};

				p.alcData[curData.mealSubCategoryCode].mealCategoryCode = null;
				p.alcData[curData.mealSubCategoryCode].mealCode = null;
				p.alcData[curData.mealSubCategoryCode].mealMenuName = null;
				p.alcData[curData.mealSubCategoryCode].mealServiceCode = null;
				p.alcData[curData.mealSubCategoryCode].mealServiceName = null;
			}
		} else {
			p.alcData = curPaxALCData;
		}

		p.alcInput = $('<input type="button" class="hidden selected" name="A La Carte Selection" value="A La Carte Selection" data-isEating="true" data-inflightMenuselect-id="" data-segment="" data-sector="" data-service-id="" data-isalc="true" data-paxid="" data-meal-category="" data-meal-servicecode="" data-meal-menuname="" data-meal-mealcode="" />');

		var alaCarteWrap = $('<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content" /></div>');
		var alcWrap = alaCarteWrap.find('.accordion-wrapper-content');

		alaCarteWrap.append(p.alcInput);

		var wrap = p.infm.find('.item-wrapper');
		wrap.append(alaCarteWrap);
		wrap.addClass('a-la-carte');

		for (var i = 0; i < jsonData[0].mealSubcategory.length; i++) {
			var subCat = jsonData[0].mealSubcategory[i];

			// Set templates
			var accordBox = $('<div data-accordion="1" data-accordion-index="' + (i + 1) + '" class="block-2 accordion accordion-box" />');
			var active = i > 0 ? '' : ' active';
			var alcAccordTrigger = $('<div data-accordion-trigger="1" class="accordion__control' + active + '">\
				<div class="fm-accordion-label">\
					<ul class="meal-menu">\
						<li class="meal-service-name">' + subCat.mealSubCategoryName + '</li>\
						<li class="selected-meal"></li>\
					</ul>\
	                <span class="fm-ico-text hidden">Select / Change</span>\
	                <em class="ico-point-d"></em>\
	              </div>\
	            </div>');
			var alcAccordCont = $('<div data-accordion-content="1" class="accordion__content inflight-alacarte-content" />');

			accordBox.append(alcAccordTrigger);
			accordBox.append(alcAccordCont);

			addInflightMeal(subCat, alcAccordCont);

			alcWrap.append(accordBox);
		}
	};

	var addInflightMeal = function (data, wrap) {
		// Paint Inflight Meal menu during painting
		// Get special attributes while we have indexes available
		var inflMenuViewOnly = false;
		var viewOnlyAlert = ' hidden';
		var viewOnlyMsg = '';
		if (jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
			viewOnlyAlert = '';
			viewOnlyMsg = jsonData[0].isViewMenuDate;
			inflMenuViewOnly = true;
		}

		for (var l = 0; l < data.Meals.length; l++) {
			// cache meal item
			var mealItem = data.Meals[l];
			var menuTPL = mealItem.isSpecialMeal == "true" ? p.inflightMenuSpecialItemTpl : p.inflightMenuItemTpl;

			var specialMealName = '';

			if (typeof mealItem.specialMealName != 'undefined') specialMealName = mealItem.specialMealName;

			// Get current icon
			var foodIcon, footNote;
			if (typeof mealItem.specialityInfo !== 'undefined') {
				foodIcon = SIA.MealsSelection.public.inflightDishIcons[mealItem.specialityInfo.dishIconId];
				footNote = mealItem.specialityInfo.specialityFootNote;
			}

			var foodIconSet = '';
			if (typeof mealItem.specialityInfo !== 'undefined') {
				for (var m = 0; m < mealItem.specialityInfo.length; m++) {
					var br = m === mealItem.specialityInfo.length - 1 ? '' : '<br>';
					foodIconSet += '<em class="' + SIA.MealsSelection.public.inflightDishIcons[mealItem.specialityInfo[m].dishIconId] + ' fm-footnote-logo"></em>\
					<span class="fm-dish-text"> ' + mealItem.specialityInfo[m].specialityFootNote + ' </span>' + br;
				}
			}

			var selectClass = '';
			var viewOnlyClass = ' hidden';
			if (typeof mealItem.isViewOnly != 'undefined' && mealItem.isViewOnly) {
				selectClass = ' hidden';
				viewOnlyClass = '';
			}

			if (p.curPaxData.mealCategoryCode == jsonData[0].mealCategoryCode && p.curPaxData.mealCode == mealItem.mealCode) {
				selectedClass = ' selected';
				inputVal = 'Remove';
			} else {
				selectedClass = '';
				inputVal = 'Select';
			}

			if (p.isALC && p.curPaxData.aLaCarteData !== null && p.alcData[data.mealSubCategoryCode].mealCode === mealItem.mealCode) {
				selectedClass = ' selected';
				inputVal = 'Remove';
			}

			var menuItem = $(_.template(menuTPL, {
				'mealMenuName': mealItem.mealMenuName,
				'mealMenuDescription': mealItem.mealMenuDescription,
				'specialityInfo': footNote,
				'specialMealName': specialMealName,
				'selectClass': selectClass,
				'viewOnlyClass': viewOnlyClass,
				'foodIcon': foodIcon,
				'triggerId': (accordionTabIndex + 1),
				'segmentId': segmentId,
				'sectorId': sectorId,
				'paxId': paxId,
				'mealCategoryCode': jsonData[0].mealCategoryCode,
				'mealServiceCode': mealService.mealServiceCode,
				'mealCode': mealItem.mealCode,
				'mealMenuName': mealItem.mealMenuName,
				'selectedClass': selectedClass,
				'inputVal': inputVal,
				'foodIconSet': foodIconSet
			}));
			menuItem.find('.fm-select-btn').attr('data-mealsubcat-code', data.mealSubCategoryCode);
			menuItem.find('.fm-select-btn').attr('data-mealsubcat-name', data.mealSubCategoryName);

			if (inflMenuViewOnly) {
				menuItem.find('input').hide();
				menuItem.find('.btn-wrap-view-only').hide();
			} else {
				// Add click listener on item
				addMenuItemListener(menuItem);
			}

			// Check if ethnic meal
			if (mealItem.isEthnicMeal == 'true' && typeof mealItem.ethinicMeallist != 'undefined') {
				var tableCont = menuItem.find('.menu-info');
				for (var m = 0; m < mealItem.ethinicMeallist.length; m++) {
					var item = $('<tr><td><span>' + mealItem.ethinicMeallist[m].ethinicMealMenuName + '</span></td><td class="item-list"></td></tr>');

					for (var n = 0; n < mealItem.ethinicMeallist[m].itemInfo.length; n++) {
						var itemList = $('<span>' + mealItem.ethinicMeallist[m].itemInfo[n].ethinicMealMenuDescription + '</span>');
						item.find('.item-list').append(itemList);
					}

					// Add additional description
					var ethnicMealDesc = mealItem.ethinicMeallist[m].ethnicMealMenuDescription;
					if (typeof ethnicMealDesc !== 'undefined' && ethnicMealDesc !== '') {
						item.append('<td class="ethnic-meal-desc">' + ethnicMealDesc + '</td>');
					}

					tableCont.append(item);
				}
			}

			wrap.append(menuItem);
		}
	};

	var addMenuItemListener = function (menu) {
		menu.find('input').parent().parent().on({
			'click': function (e) {
				e.preventDefault();
				// Cache jq objects
				var t = $(this).find('.fm-select-btn');
				var cont = t.parent().parent().parent();

				if (t.hasClass('hidden')) return;

				// search and remove any previous selected meal
				SIA.MealsSelection.resetMeals(t, ['btcm', 'spm']);

				if (p.isALC) {
					var curMealSubCatCode = t.attr('data-mealsubcat-code');
					var curMealSubCatName = t.attr('data-mealsubcat-name');
				}

				if (!cont.hasClass('selected')) {
					// reset all select items on wrapper
					cont.parent().find('.menu-item').removeClass('selected');
					cont.parent().find('input').val('Select');

					// Add selected class to the input and to the container
					t.addClass('selected');
					cont.addClass('selected');
					cont.find('.fm-inf-menu').addClass('selected');

					t.val('Remove');

					p.infm.find('.sticky-confirm-bar').removeClass('no-selection');

					if (p.isALC) {
						p.alcData[curMealSubCatCode].mealCategoryCode = t.attr('data-meal-category');
						p.alcData[curMealSubCatCode].mealCode = t.attr('data-meal-mealcode');
						p.alcData[curMealSubCatCode].mealMenuName = t.attr('data-meal-menuname');
						p.alcData[curMealSubCatCode].mealServiceCode = t.attr('data-meal-servicecode');
						p.alcData[curMealSubCatCode].mealServiceName = curMealSubCatName;

						p.alcInput.attr('data-inflightMenuselect-id', t.data().inflightmenuselectId);
						p.alcInput.attr('data-paxid', t.data('paxid'));
						p.alcInput.attr('data-sector', t.data().sector);
						p.alcInput.attr('data-segment', t.data().segment);
						p.alcInput.attr('data-service-id', t.data().serviceId);
						p.alcInput.attr('data-meal-category', t.data().mealCategory);
						p.alcInput.attr('data-meal-servicecode', t.data().mealServicecode);
						p.alcInput.attr('data-alc-json', JSON.stringify(p.alcData));

						cont.parent().prev().find('.selected-meal').text('Selected');
					}

					SIA.MealsSelection.setMainMenuLbl(t, t.data('label') + ' (Inflight Menu)');
					SIA.MealsSelection.updateData(t);
				} else {
					cont.removeClass('selected');
					t.removeClass('selected');
					t.val('Select');

					if (p.isALC) {
						p.alcData[curMealSubCatCode].mealCategoryCode = null;
						p.alcData[curMealSubCatCode].mealCode = null;
						p.alcData[curMealSubCatCode].mealMenuName = null;
						p.alcData[curMealSubCatCode].mealServiceCode = null;
						p.alcData[curMealSubCatCode].mealServiceName = null;

						cont.parent().prev().find('.selected-meal').text('');
					}
					SIA.MealsSelection.setMainMenuLbl(t, 'Inflight Menu', true);
					// Check if there are selected special meals then select same meal for this service code
					for (var i = 0; i < SIA.MealsSelection.public.spml.length; i++) {
						var spml = SIA.MealsSelection.public.spml[i];
						if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
							spml.selectSegmentMeal();
							p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);

							cont.after(p.spmlAlertPrompt);

							$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
								var t = $(e.target);
								if (t.hasClass('passenger-info__text') ||
									t.hasClass('meal-tab-item') ||
									t.hasClass('accordion__control') ||
									t.hasClass('ico-point-d') ||
									t.hasClass('fm-ico-text') ||
									t.hasClass('meal-service-name') ||
									t.hasClass('flight-ind') ||
									t.hasClass('btn-back') ||
									t.hasClass('fm-inf-menu') ||
									t.hasClass('search-btn') ||
									t.hasClass('popup__close') ||
									t.hasClass('lightbox-btn-back')
								) {
									p.spmlAlertPrompt.detach();

									$(document).off('click.spmlPromptCheck');

								}
							});

							$('.more-tab').on('change.promptCheck', function (e) {

								$('.more-tab').off('change.promptCheck');
								$(document).off('click.spmlPromptCheck');

								p.spmlAlertPrompt.detach();

							});


							break;
						}
					}

					p.infm.find('.sticky-confirm-bar').addClass('no-selection');
				}

				if (p.isALC) {
					// Update the user data here
					var parentAccMealSummary = '';

					_.each(p.alcData, function (v, k, l) {
						if (v.mealMenuName != null) {
							// set the title info
							parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';
						}
					});
					
					SIA.MealsSelection.setMainMenuLbl(t, parentAccMealSummary.substring(4));
					SIA.MealsSelection.updateData(p.alcInput);
				} 
			}
		});
	};

	var destroy = function () {
		p.infm.remove();
	};

	var oPublic = {
		init: init,
		public: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.InfantMeal = function (
	jsonData,
	paxData,
	origin,
	destination,
	accContent,
	accordionContentWrap,
	segmentId,
	sectorId,
	serviceId,
	paxId,
	mealServiceCode,
	mealService,
	accordionTabIndex,
	body,
	accordContainer) {

	var p = {};

	// Infant Meals TPL
	p.infantMeal = '<article class="promotion-item inflight-menu-item--1 menu-item <%= isSel %>">\
			<div class="fm-inf-menu int-prompt">\
				<span class="fm-inf-menu-select-badge">Selected</span>\
			  	<span class="special-badge <%= isSpecial %>">SPECIAL</span>\
			  	<h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
			  	<p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
			  	<div class="fm-footnote-txt">\
				  	<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				  	<span class="fm-dish-text"><%= specialityFootNote %></span>\
				  	<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
			  	</div>\
			  	<div data-prompt="with-selection" class="confirmation-prompt hidden">\
					<p class="meal-sub-title-blue"> Selecting a <span><%= mealName %></span> will change your other infant meals selection from  from <%= origin %> to <%= destination %> to <span><%= mealName %></span> as well. </p>\
					<div class="button-group-1">\
						<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
						<a href="#" class="btn-2 btn-cancel">Cancel</a>\
					</div>\
  				</div>\
				<div data-prompt="initial" class="confirmation-prompt hidden">\
					<p class="meal-sub-title-blue"> Selecting <span><%= mealName %></span> will change all your meal selections from <%= origin %> to <%= destination %> to <span><%= mealName %></span>.</p>\
					<div class="button-group-1">\
						<input type="submit" value="Proceed" class="btn-1 btn-proceed">\
						<a href="#" class="btn-2 btn-cancel">Cancel</a>\
					</div>\
		  		</div>\
			</div>\
		</article>';
	p.infantMeals = [];
	p.curPrompt = null;
	p.currentMeal = null;

	var init = function () {
		p.infml = accContent.find('.inf-container');

		paintInfantMeals();
		addMealItemListener(p.infml);

		p.infmlChkBox = p.infml.parent().find('[data-checktype="not-eating"]');

		setTimeout(function () {
			p.infmlChkBox.on({
				'change.infm': function () {
					var t = $(this);
					if (t.prop('checked') && p.currentMeal !== null) {
						unSelectMeal(p.currentMeal);
						SIA.MealsSelection.setMainMenuLbl(t, 'I don’t need an infant meal');
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}, 600);
	};

	var paintInfantMeals = function () {
		var mealsData = jsonData[0].mealSubcategory[0].Meals;
		for (var i = 0; i < mealsData.length; i++) {
			var meal = paintInfantMealItem(mealsData[i], i);
			p.infml.append(meal);
			addMealItemListener(meal);

			p.infantMeals.push(meal);
		}
	};

	var paintInfantMealItem = function (data, btnIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';

		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';
			specialityFootNote = data.specialityInfo.specialityFootNote;
			footIcon = SIA.MealsSelection.public.inflightDishIcons[data.specialityInfo.dishIconId];
		}

		// check for selected menu in data
		var selMealsel = SIA.MealsSelection.public.data.paxList[parseInt(paxId)].selectedMeals[parseInt(segmentId)].selectedMeal;
		selMeal = selMealsel[mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		return $(_.template(p.infantMeal, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': accordionTabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': segmentId,
			'sector': sectorId,
			'paxid': paxId,
			'mealServiceCode': mealServiceCode,
			'mealCatCode': jsonData[0].mealCategoryCode,
			'origin': origin,
			'destination': destination,
			'menuTarget': accordionTabIndex + 1
		}));
	}

	var addMealItemListener = function (mc) {
		var allBtns = mc.find('.fm-select-btn');

		$.each(allBtns, function (index, btn) {
			$(btn).parent().parent().on('click', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				// check if current meal card is selected again, then remove it
				if (parentEl.hasClass('selected')) {
					unSelectMeal(t);

					SIA.MealsSelection.setMainMenuLbl(t, 'Infant Meal');
					SIA.MealsSelection.updateData(t);

					p.currentMeal = null;
				} else {
					// Check for special meals selected in other sectors in same leg
					var otherINFM = getOtherINFM(t);

					if (parentEl.hasClass('int-prompt') && p.curPrompt !== null) {
						t.val('Select');
						parentEl.removeClass('int-prompt');
						p.curPrompt.addClass('hidden');
						parentEl.find('.btn-cancel').off();
						parentEl.find('.btn-proceed').off();

						p.curPrompt = null;
					} else {
						if (otherINFM.length > 0) {
							var prompt = t.parent().parent().find('[data-prompt="with-selection"]');
						} else {
							var prompt = t.parent().parent().find('[data-prompt="initial"]');
						}

						t.val('Selected');
						parentEl.addClass('int-prompt');
						prompt.removeClass('hidden');

						// Remove any selected prompt
						if (p.curPrompt !== null) {
							p.curPrompt.parent().find('.fm-select-btn').val('Select');
							p.curPrompt.addClass('hidden');
							p.curPrompt.parent().removeClass('int-prompt');
							p.curPrompt.find('.btn-cancel').off();
							p.curPrompt.find('.btn-proceed').off();
						}

						parentEl.find('.btn-cancel').off().on({
							'click': function (e) {
								e.preventDefault();
								e.stopImmediatePropagation();

								$(this).off();

								t.val('Select');
								parentEl.removeClass('int-prompt');
								parentEl.find('.confirmation-prompt').addClass('hidden');
							}
						});

						parentEl.find('.btn-proceed').off().on({
							'click': function (e) {
								e.preventDefault();
								e.stopImmediatePropagation();

								// remove listener to prevent double calls on otherSM
								$(this).off();

								// reset other meals before assigning selected class to current item
								SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

								selectMeal(t);

								p.currentMeal = t;

								// apply selection to other special meals in same leg
								selectSameMeal(t);

								// assign data
								SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname') + ' (Infant Meal)');
								SIA.MealsSelection.updateData(t);

								unselectChkBox();
							}
						});

						p.curPrompt = prompt;
					}
				}
			});
		});
	};

	var selectSameMeal = function (meal) {
		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.infml.length; i++) {
			var infm = SIA.MealsSelection.public.infml[i];

			infm.p.infml.find('.fm-select-btn').each(function () {
				var t = $(this);

				// Unselect other selected special meals in same sector
				if (t.attr('data-segment') === meal.attr('data-segment')) {
					unSelectMeal(t);
				}

				// Find the selected special meal in current segment and select it
				if (t.attr('data-segment') === meal.attr('data-segment') &&
					t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')) {
					selectMeal(t);

					SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname') + ' (Infant Meal)');
					SIA.MealsSelection.updateData(t);
				}
			});
		}
	};

	var unselectChkBox = function () {
		p.infmlChkBox.prop("checked", false);
		p.infmlChkBox.parent().find('.not-eating-msg').addClass('hidden');
		p.infmlChkBox.parent().parent().next().addClass('hidden');
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	var unSelectMeal = function (meal) {
		var parentEl = meal.parent().parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
	};

	var getOtherINFM = function (meal) {
		var otherSelectedINFM = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];

		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'INF' && v.mealCode !== meal.attr('data-meal-mealcode')) {
				otherSelectedINFM.push({
					'k': k,
					'val': v
				});
			}
		});

		return otherSelectedINFM;
	};

	var destroy = function () {
		for (var i = 0; i < p.infantMeals.length; i++) {
			p.infantMeals[i].remove();
		}

		p.infantMeals = [];
		accContent.remove();
	};

	var oPublic = {
		init: init,
		p: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.BahMealMenu = function(
		jsonData,
		origin,
		destination,
		segmentId,
		sectorId,
		paxId,
		mealServiceName,
		mealServiceCode,
		paxData,
		paxName,
		accContent
	) {
	var p = SIA.MealsSelection.public;
	var body = $("body");
	var allMeals = [];
	var menuTpl;
	var mealTriggerClasses = [
		{
			"mealCode": "HTM",
			"className": "hot-meals",
			"name": "Hot Meal"
		},
		{
			"mealCode": "PRSLCS",
			"className": "premium-meals",
			"name": "Premium Selection"
		},
		{
			"mealCode": "LML",
			"className": "light-meals",
			"name": "Light Meal"
		}
	];
	var dishIcons = {
		"HLL": {
			className: "ico-halal"
		},
		"SPC": {
			className: "ico-5-Spicy"
		},
		"VEG": {
			className: "ico-4-leaf"
		}
	};
	var targetId = segmentId + "" + sectorId + "" + paxId;
	var currentPax = {};
	var loadLimit = 5;

	p.loadMoreButton = '<div class="load-more hidden">\
		<input type="button" name="select" value="Load more" class="load-more-btn" />\
	</div>';

	var getAllMenuOnSubCategory = function(meals) {
		_.each(meals, function(meal) {
			if (meal.hasOwnProperty("mealSubCategoryCode")) {
				allMeals = allMeals.concat(meal.Meals);
			}
		});
	};

	var clearExisitingPopUp = function(className, targetId) {
		$(".popup--bah-" + className + "-" + targetId).each(function() {
			$(this).remove();
		});
	};

	var paintMenuContent = function(meal) {
		var triggerClass = _.findWhere(mealTriggerClasses, {mealCode: meal.mealCategoryCode});
		clearExisitingPopUp(triggerClass.className, targetId);
		menuTpl = _.template(p.bahMenuContent, {
			targetClass: targetId,
			origin: origin,
			destination: destination,
			mealType: mealServiceName,
			mealTitle: meal.mealMenuCategoryName,
			mealDesc: meal.mealCategoryDescription,
			className: triggerClass.className
		});
		menuTpl = $(menuTpl);
		paintMenuList(menuTpl, meal);
		body.append(menuTpl);

		SIA.MealsSelection.initCloseAllPopup();
	};

	var paintMenuList = function(parentWrapper, meal) {
		var elementList = [];
		var currencyCode = "";
		var triggerClass = _.findWhere(mealTriggerClasses, { mealCode: meal.mealCategoryCode });
		var container = parentWrapper.find(".item-wrapper");
		_.each(allMeals, function(mealItem, idx) {
			var mealTpl = _.template(p.menuItemBah, {
				mealName: mealItem.mealMenuName,
				mealDescription: mealItem.mealMenuDescription,
				price: mealItem.price,
				currency: mealItem.currency,
				speciality: (mealItem.specialityInfo || []),
				icons: dishIcons,
				paxId: paxId,
				segmentId: segmentId,
				mealServiceCode: mealServiceCode,
				mealCategory: meal.mealCategoryCode,
				mealCode: mealItem.mealCode
			});
			var templateLoaded = $(mealTpl);
			if (idx >= loadLimit) {
				templateLoaded.addClass("hidden");
			}
			addClickListener(templateLoaded);
			var selectedMeal = templateLoaded.find('[data-meal-mealcode="'+paxData.mealCode+'"]');
			if (selectedMeal.length) {
				var wrapper = selectedMeal.closest(".meal-of-bah");
				wrapper.addClass("selected");
				toggleButton(wrapper, false);
				setTimeout(function() {
					changeAccordionLabel(selectedMeal);
				}, 500);
			}
			elementList.push(templateLoaded);
			currencyCode = mealItem.currency;
		});
		if (meal.mealCategoryCode == "PRSLCS") {
			var lowestPrice = getLowestPrice();
			accContent.find(".price-range-block").find(".meal-price").text(currencyCode + " " + lowestPrice.toFixed(2));
		}
		var loadButtonEl = $(p.loadMoreButton);
		elementList.push(loadButtonEl);
		container.append(elementList);
		if (container.find(".meal-of-bah.hidden").length) {
			container.find(".load-more").removeClass("hidden");
			loadMoreButtonListener(container, loadButtonEl);
		}
	};

	var getCurrentPax = function() {
		if (p.data.paxList[paxId].preselectedMeals[segmentId]) {
			currentPax = _.clone(p.data.paxList[paxId].preselectedMeals[segmentId].selectedMeal[mealServiceCode]);
		} else {
			currentPax = _.clone(p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode]);
		}
	};

	var getLowestPrice = function() {
		var lowestPrice = 0;
		_.each(allMeals, function(meal) {
			if (lowestPrice == 0) {
				return lowestPrice = parseFloat(meal.price);
			}

			lowestPrice = (parseFloat(meal.price) < lowestPrice) ? parseFloat(meal.price) : lowestPrice;
		});

		return lowestPrice;
	};	

	var loadMoreButtonListener = function(container, loadButtonEl) {
		loadButtonEl.find(".load-more-btn").off().on("click", function() {
			var elementList = container.find(".meal-of-bah.hidden").splice(0, loadLimit);
			_.each(elementList, function(el) {
				el = $(el);
				el.removeClass("hidden");
			})
			if (!container.find(".meal-of-bah.hidden").length) {
				loadButtonEl.addClass("hidden");
			}
		});
	};

	var addClickListener = function(el) {
		el.off().on("click.bahMenuList", function() {
			getCurrentPax();

			var $this = $(this);
			var isSelected = $this.hasClass("selected");
			if (!isSelected) {
				var canSelect = forfeitSelection($this);
				if (!canSelect) return;
			}
			if (isSelected && currentPax.isPreselected) {
				return cancelSelection($this, $this.find(".fm-select-btn"));
			}
			var toggledButton = toggleButton($this, isSelected);
			clearAllSelected($this);
			if (!isSelected) {
				SIA.MealsSelection.updateData(toggledButton);
				changeAccordionLabel(toggledButton);

				menuTpl.find('.sticky-confirm-bar').removeClass('no-selection');
			} else {
				var wrapper = $this.closest(".popup__content");
				SIA.clickActivity.hideAlertPrompt();
				SIA.MealsSelection.removeInputData(wrapper);
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				changeAccordionLabel(toggledButton, true);

				menuTpl.find('.sticky-confirm-bar').addClass('no-selection');
			}
		});
	};

	var countPax = function() {
		var count = 0;
		_.each(p.data.paxList, function(data) {
			if (data.paxType.toLowerCase() == "adult") {
				count ++;
			}
		});

		$("[data-booking-summary-panel]").find(".number-passengers").text(count + " Adults");
	};

	var clearAllSelected = function($this) {
		$(".popup--bah-" + targetId).each(function() {
			$(this).find(".meal-of-bah.selected").each(function() {
				var $current = $(this);
				var isSelected = $current.hasClass("selected");
				if ($this.is($current)) return;
				toggleButton($current, isSelected);
			});
		});
	};

	var forfeitSelection = function(el) {
		var buttonElement = el.find(".fm-select-btn");
		var selectedMealCode = buttonElement.data("meal-category");
		var selectedPrice = parseFloat(buttonElement.data("meal-price"));
		var paxId = buttonElement.attr('data-paxid');
		
		if (
			selectedMealCode == currentPax.mealCategoryCode &&
			selectedPrice == parseFloat(currentPax.mealPrice) &&
			selectedMealCode == "PRSLCS"
		) {
			return equalForfeitSelection(el, buttonElement);
		} else if (
			selectedMealCode != "PRSLCS" &&
			currentPax.mealCategoryCode == "PRSLCS"
		) {
			return fullForfeitSelection(el, buttonElement);
		} else if (
			selectedMealCode == currentPax.mealCategoryCode &&
			selectedPrice < parseFloat(currentPax.mealPrice) &&
			selectedMealCode == "PRSLCS"
		) {
			return partialForfeitSelection(el, buttonElement);
		} else if (
			currentPax.mealCategoryCode &&
			selectedPrice > parseFloat((currentPax.mealPrice ? currentPax.mealPrice : "0")) &&
			selectedMealCode == "PRSLCS"
		) {
			return topUpForfeitSelection(el, buttonElement);
		}
		if (selectedMealCode == "PRSLCS" && !p.bspIsDisplayed) {
			p.bspIsDisplayed = true;
			changeCTAButton("PROCEED TO PAYMENT");
			SIA.BSPUtil.displayBSP(function() {
				countPax();
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: selectedPrice,
					paxName: paxName,
					origin: origin,
					destination: destination
				});
			});
		} else if (selectedMealCode != "PRSLCS") {
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
		} else {
			changeCTAButton("PROCEED TO PAYMENT");
			SIA.BSPUtil.addBSPTpl({
				segmentId: segmentId,
				sectorId: sectorId
			}, {
				paxId: paxId,
				price: selectedPrice,
				paxName: paxName,
				origin: origin,
				destination: destination
			});
		}

		return true;
	};

	var changeCTAButton = function(inputText) {
		$(".btn-cta-submit").val(inputText.toUpperCase());
	};

	var equalForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. No additional payment will be required as the price of your previous meal is the same as your new meal.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var fullForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. Note that there will not be a refund for the meal which you've previously paid for.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var partialForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal premium selection. Payment is not required for your new meal as the price is lower than you've paid for your previous meal. Note that there will not be any refund for your previous meal as well.";
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var topUpForfeitSelection = function(el, buttonElement) {
		var promptMessage = "You have an existing premium meal which will be replaced with this new meal selection. To confirm, proceed to pay for this new meal selection.";
		if (currentPax.mealCategoryCode != "PRSLCS") {
			promptMessage = "You have an existing meal which will be replaced with this new meal selection. To confirm, proceed to pay for this new meal selection.";
		}
		setButtonPreSelected(el, buttonElement, promptMessage);
		return false;
	};

	var cancelSelection = function(el, buttonElement) {
		var promptMessage = "When you remove this meal selection, note that there will not be a refund for the meal which you've previously paid for. You can select another meal to replace this.";
		setButtonPreSelected(el, buttonElement, promptMessage, true);
		return false;
	};

	var setButtonPreSelected = function(el, buttonElement, promptMessage, isCancel) {
		SIA.clickActivity.hideAlertPrompt();
		var mealWrapper = buttonElement.closest(".meal-of-bah");
		var confirmPrompt = mealWrapper.find(".confirmation-prompt");

		if(
			segmentId === parseInt(buttonElement.attr('data-segment')) &&
			mealServiceCode === buttonElement.attr('data-meal-servicecode') &&
			!currentPax.isPreselected &&
			!isCancel
		){
			var price = buttonElement.data("meal-price");
			var paxId = buttonElement.attr('data-paxid');
			if (buttonElement.data("meal-category") == "PRSLCS" && !p.bspIsDisplayed) {
				p.bspIsDisplayed = true;
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.displayBSP(function() {
					countPax();
					SIA.BSPUtil.addBSPTpl({
						segmentId: segmentId,
						sectorId: sectorId
					}, {
						paxId: paxId,
						price: price,
						paxName: paxName,
						origin: origin,
						destination: destination
					});
				});
			} else if (buttonElement.data("meal-category") == "PRSLCS") {
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: price,
					paxName: paxName,
					origin: origin,
					destination: destination
				});
			} else if (buttonElement.data("meal-category") != "PRSLCS") {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
			}

			clearAllSelected(el);
			toggleButton(el, false);
			SIA.MealsSelection.updateData(buttonElement);
			changeAccordionLabel(buttonElement);
		}else {
			confirmPrompt.find(".meal-sub-title-blue").text(promptMessage);
			confirmPrompt.removeClass("hidden");
			if (!isCancel) {
				buttonElement.val("Selected");
			}
			addPromptListener(el, buttonElement, confirmPrompt, isCancel);
		}
	};

	var addPromptListener = function(el, buttonEl, prompt, isCancel) {
		var cancelBtn = prompt.find(".btn-cancel");
		var proceedBtn = prompt.find(".btn-proceed");
		var price = buttonEl.data("meal-price");
		var paxId = buttonEl.attr('data-paxid');

		// Make sure while prompt is shown, button is set to Remove
		if(buttonEl.val() == 'Select') buttonEl.val('Remove');

		proceedBtn.off().on("click", function(evt) {
			var canDisplayBSP = false;
			evt.preventDefault();
			evt.stopImmediatePropagation();
			var currentPrice = parseFloat(currentPax.mealPrice);

			prompt.addClass("hidden");
			clearAllSelected(el);
			if (isCancel) {
				SIA.MealsSelection.removeInputData(el);
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				changeAccordionLabel(buttonEl, true);
				proceedBtn.off();
				cancelBtn.off();
				return;
			} else {
				toggleButton(el, false);
				SIA.MealsSelection.updateData(buttonEl);
				changeAccordionLabel(buttonEl);
				proceedBtn.off();
				cancelBtn.off();
			}

			if (!currentPax.isPreselected) {
				canDisplayBSP = true;
			}
			if (price > currentPrice) {
				canDisplayBSP = true;
			} else {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
				return;
			}

			if (buttonEl.data("meal-category") == "PRSLCS" && !p.bspIsDisplayed && canDisplayBSP) {
				p.bspIsDisplayed = true;
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.displayBSP(function() {
					countPax();
					if (!$(".bah-subtotal").length && price > currentPrice) { 
						SIA.BSPUtil.paintSubTotal(currentPax);
					}
					SIA.BSPUtil.addBSPTpl({
						segmentId: segmentId,
						sectorId: sectorId
					}, {
						paxId: paxId,
						price: price,
						paxName: paxName,
						origin: origin,
						destination: destination,
						currentPaxPrice: currentPax.mealPrice
					});
				});
			} else if (buttonEl.data("meal-category") == "PRSLCS" && canDisplayBSP) {
				changeCTAButton("PROCEED TO PAYMENT");
				SIA.BSPUtil.addBSPTpl({
					segmentId: segmentId,
					sectorId: sectorId
				}, {
					paxId: paxId,
					price: price,
					paxName: paxName,
					origin: origin,
					destination: destination,
					currentPaxPrice: currentPax.mealPrice
				});
			} else if (buttonEl.data("meal-category") != "PRSLCS") {
				SIA.BSPUtil.removeBSP({
					segmentId: segmentId,
					sectorId: sectorId
				}, paxId);
			}
		});

		cancelBtn.off().on("click", function(evt) {
			evt.preventDefault();
			evt.stopImmediatePropagation();

			prompt.addClass("hidden");
			if (!isCancel) {
				buttonEl.val("Select");
			}
			proceedBtn.off();
			cancelBtn.off();
		});
	};

	var changeAccordionLabel = function(el, isRemoving) {
		var accordionBoxWrapper = $(".accordion-" + targetId).find(".fm-accordion-label").find("[data-label]");
		var triggerClass = _.findWhere(mealTriggerClasses, {mealCode: el.data("meal-category")});
		var price = el.data("meal-price");
		var labelTpl;
		if (!isRemoving) {
			labelTpl = '<span>\
							<span class="title-selected hidden">Selected:</span> ' 
							+ el.data("meal-menuname")
							+ " (" + triggerClass.name + ") "
							+ "<bold>" + (price ? " - SGD " + price + "" : "") + "</bold>"
							+ "</span>";
			accordionBoxWrapper.parent().find('.ico-delete-2').removeClass('hidden');
		} else {
			labelTpl = '<span class="title-selected">Hot Meal</span>';
			accordionBoxWrapper.parent().find('.ico-delete-2').addClass('hidden');
		}
		var labelRendered = $((el ? labelTpl : defaultTpl));
		accordionBoxWrapper.html(labelRendered);
		if (!isRemoving) {
			addRemoveBahListener(labelRendered, el);
		} else {
			toggleButton(el.closest(".meal-of-bah"), true);
		}
	};

	var addRemoveBahListener = function(el, buttonEl) {
		var meal = p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];
		
		el.parent().parent().find('.ico-delete-2').off().on("click", function(evt) {
			evt.stopImmediatePropagation();
			evt.preventDefault();
			if (meal.isPreselected) {
				$("[data-accordion-trigger]").trigger("click.removeHandleEvent");
				var prompt = el.closest(".accordion-box").find(".remove-prompt");
				removeBahPrompt(prompt, buttonEl, menuTpl.find(".popup__content"));
				return;
			}

			SIA.MealsSelection.removeInputData(menuTpl.find(".popup__content"));
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
			changeAccordionLabel(buttonEl, true);
		});
	};

	var removeBahPrompt = function(prompt, buttonEl, inputData) {
		prompt.removeClass("hidden");
		var proceed = prompt.find(".btn-proceed");
		var cancel = prompt.find(".btn-cancel");
		var classes = "[data-accordion-trigger]";

		$(document).find(classes).on("click.removeHandleEvent", function(evt) {
			prompt.addClass("hidden");
			proceed.off();
			cancel.off();
			$(classes).off("click.removeHandleEvent");
		});

		inputData.find(".confirmation-prompt:not(.hidden)").each(function() {
			$(this).addClass("hidden");
		});

		proceed.on("click", function(evt) {
			evt.preventDefault();

			SIA.MealsSelection.removeInputData(inputData);
			SIA.BSPUtil.removeBSP({
				segmentId: segmentId,
				sectorId: sectorId
			}, paxId);
			changeAccordionLabel(buttonEl, true);
			prompt.addClass("hidden");

			proceed.off();
			cancel.off();
		});

		cancel.on("click", function(evt) {
			evt.preventDefault();
			prompt.addClass("hidden");
			proceed.off();
			cancel.off();
		});
	};

	var toggleButton = function($this, status) {
		var mealButton = $this.find(".fm-select-btn");
		$this[status ? "removeClass" : "addClass"]("selected");
		mealButton[status ? "removeClass" : "addClass"]("selected");
		mealButton.val(status ? "Select" : "Remove");
		return mealButton;
	};

	getAllMenuOnSubCategory(jsonData[0].mealSubcategory);
	paintMenuContent(jsonData[0]);
};

SIA.BahMealMenuLong = function(
		jsonData,
		origin,
		destination,
		segmentId,
		sectorId,
		paxId,
		mealServiceName,
		mealServiceCode,
		paxData
	) {
		var allMeals = jsonData;
		var p = SIA.MealsSelection.public;
		var targetId = segmentId + "" + sectorId + "" + paxId;
		var wrapper;
		var dishIcons = {
			"VEG": {
				className: "ico-4-leaf"
			}
		};

		var addMealsList = function() {
			var elementList = [];
			wrapper = $(".popup--longhaul-" + targetId);
			var itemContainer = wrapper.find(".item-container").html("");
			_.each(allMeals, function(mealItem) {
				var concatDescription = descriptionConcatenate(mealItem.mealMenuDescription);
				var mealTpl = _.template(p.menuItemBah, {
					mealName: mealItem.mealMenuName,
					mealDescription: "",
					price: false,
					currency: mealItem.currency,
					speciality: (mealItem.specialityInfo || []),
					icons: dishIcons,
					paxId: paxId,
					segmentId: segmentId,
					mealServiceCode: mealServiceCode,
					mealCategory: "LONGHAUL",
					mealCode: mealItem.mealCode
				});
				var templateLoaded = $(mealTpl);
				templateLoaded.html($('<div class="promotion-item__inner">' + templateLoaded.html() + '</div>'));
				addClickListener(templateLoaded);
				var selectedMeal = templateLoaded.find('[data-meal-mealcode="'+paxData.mealCode+'"]');
				if (selectedMeal.length) {
					var accWrapper = selectedMeal.closest(".meal-of-bah");
					accWrapper.addClass("selected");
					toggleButton(accWrapper, false);
					setTimeout(function() {
						changeAccordionLabel(selectedMeal);
					}, 500);
				}
				templateLoaded.find(".fm-inf-menu-desc").append(concatDescription);
				elementList.push(templateLoaded);
			});

			itemContainer.append(elementList);

			SIA.MealsSelection.initCloseAllPopup();
		}

		var descriptionConcatenate = function(mealDescription) {
			var elementList = [];
			_.each(mealDescription, function(description) {
				elementList.push($("<p>" + description.mealSubDescription + "</p>"));
			});
	
			return elementList;
		};

		var addClickListener = function(el) {
			el.off().on("click.bahMenuList", function() {
				var $this = $(this);
				var isSelected = $this.hasClass("selected");

				wrapper.find(".meal-of-bah.selected").each(function() {
					var $current = $(this);
					var isSelected = $current.hasClass("selected");
					if ($this.is($current)) return;
					toggleButton($current, isSelected);
				});
				var toggledButton = toggleButton($this, isSelected);
				if (!isSelected) {
					SIA.MealsSelection.updateData(toggledButton);
					changeAccordionLabel(toggledButton);

					wrapper.find('.sticky-confirm-bar').removeClass('no-selection');
				} else {
					var accWrapper = $this.closest(".popup__content");
					SIA.MealsSelection.removeInputData(accWrapper);
					changeAccordionLabel(toggledButton, true);
					
					wrapper.find('.sticky-confirm-bar').addClass('no-selection');
				}
			});
		};

		var changeAccordionLabel = function(el, isRemoving) {
			var accordionBoxWrapper = $(".accordion-" + targetId).find(".fm-accordion-label").find("[data-label]");
			var labelTpl;
			if (!isRemoving) {
				labelTpl = '<span>\
								<span class="title-selected hidden">Selected:</span> ' 
								+ el.data("meal-menuname") + " "
								+ "</span>";

				accordionBoxWrapper.parent().find('.ico-delete-2').removeClass('hidden');
			} else {
				labelTpl = '<span class="title-selected">Long Haul Meal Bundle</span>';
				accordionBoxWrapper.parent().find('.ico-delete-2').addClass('hidden');
			}
			var labelRendered = $((el ? labelTpl : defaultTpl));
			accordionBoxWrapper.html(labelRendered);
			if (!isRemoving) {
				addRemoveBahListener(labelRendered, el);
			} else {
				toggleButton(el.closest(".meal-of-bah"), true);
			}
		};

		var addRemoveBahListener = function(el, buttonEl) {
			el.parent().parent().find('.ico-delete-2').off().on("click", function(evt) {
				evt.stopImmediatePropagation();
				evt.preventDefault();
	
				SIA.MealsSelection.removeInputData(wrapper.find(".popup__content"));
				changeAccordionLabel(buttonEl, true);
			});
		};

		var toggleButton = function($this, status) {
			var mealButton = $this.find(".fm-select-btn");
			$this[status ? "removeClass" : "addClass"]("selected");
			mealButton[status ? "removeClass" : "addClass"]("selected");
			mealButton.val(status ? "Select" : "Remove");
			return mealButton;
		};

		setTimeout(function() { 
			addMealsList();
		}, 500);
};

SIA.BookTheCook = function (
	data,
	mealIcons,
	mealSel,
	bckAllMeal,
	segment,
	sector,
	paxid,
	mealServiceCode,
	jsonData,
	origin,
	destination,
	mealType,
	mainMenuTarget) {

	var p = {};

	p.BTCJson = data;
	p.mealIcons = mealIcons;
	mealSelEvent = mealSel;
	bckAllMealsEvent = bckAllMeal;
	p.segment = segment;
	p.sector = sector;
	p.paxid = paxid;
	p.mealServiceCode = mealServiceCode;
	p.btcMealCatCode = data[0].mealCategoryCode;
	p.data = jsonData;
	p.origin = origin;
	p.destination = destination;
	p.mealType = mealType;
	p.mainMenuTarget = mainMenuTarget;

	// declare templates
	p.BTCMain = '<aside class="popup popup-meals menu-list-btc popup--btc-menu-<%= targetClass %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">';
	p.BTCCuisineCatMainWrapperTpl = '<div class="popup__inner">\
		<div class="popup__content">\
			<div class="book-the-cook">\
				<p class="light-box-btn-mb">\
					<a href="#" class="lightbox-btn-back" data-close="true">\
						<em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to categories \
					</a>\
				</p>\
				<h3 class="fm-title-4--blue">Book the Cook</h3>\
				<p class="light-box-text">Choose a Book the Cook main course at least 24 hours before departure to enjoy an exquisite meal crafted by our International Culinary Panel of chefs.</p>\
				<p class="search-heading"> Have a meal in mind? </p>\
				<div class="static-content">\
						<div class="static-details">\
							<div class="btc-search">\
								<label for="input-search-1" class="hidden"></label>\
								<em class="ico-search search-btn"></em>\
								<span class="input-1">\
									<input type="text" name="input-search-1" value="" placeholder="Search" />\
								</span>\
							</div>\
							<a href="#" class="clear-search hidden"><em class="ico-point-r"></em>Clear search result</a>\
							<div class="tabs-component">\
								<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">\
										<ul class="tab tab-list" role="tablist">\
											<li class="more-item">\
												<a href="#">More<em class="ico-dropdown"></em></a>\
											</li>\
										</ul>\
										<div class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">\
											<label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span>\
											<select class="more-tab" name="calcTabSelect">\
											</select>\
										</div>\
										<div class="tab-wrapper btc-meal-list" id="tabWrapper">\
										</div>\
								</div>\
							</div>\
						</div>\
				</div>\
			</div>\
		</div>\
	</div>\
	<div class="sticky-confirm-bar no-selection">\
		<button class="btn-1" data-close-all="true">Done</button>\
	</div>';
	p.mealMain = '<div <%= isHidden %> data-tab-content="<%= index %>">';
	p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
	p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';
	p.BTCLoadMoreBtn = '<div class="load-more">\
		<input type="button" name="select" id="" value="Load more" class="load-more-btn">\
	 </div>';
	p.BTCCruisineCatListItemTpl = '<li class="tab-item <%= isActive %>" aria-selected="true" role="tab" data-tab-menu="<%= tabMenu %>">\
		  <a href="#"><%= menuName %></a>\
	 </li>';
	p.BTCMealItem = '<div data-meal-item class="fm-inf-menu <%= isSel %>" data-img="none">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
		  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
		  <p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
		  <div class="fm-footnote-txt">\
				<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				<span class="fm-dish-text"><%= specialityFootNote %></span>\
				<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
		  </div>\
		  <div class="confirmation-prompt confirmation-remove--blue hidden">\
				<em class="ico-alert"></em>\
				<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod tempor incididunt ut labore et dolore.</p>\
				<div class="button-group-1">\
					<input type="button" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
	 </div>';

	p.BTCMealItemWithImage = '<div data-meal-item class="fm-inf-menu with-image <%= isSel %>" data-img="<%= btcImg %>">\
		  <img src="<%= btcImg %>"/>\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
		  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
		  <p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
		  <div class="fm-footnote-txt">\
				<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				<span class="fm-dish-text"><%= specialityFootNote %></span>\
				<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
		  </div>\
		  <div class="confirmation-prompt confirmation-remove--blue hidden">\
				<em class="ico-alert"></em>\
				<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span class="menu-label">Inflight Meal</span> sed do eiusmod tempor incididunt ut labore et dolore.</p>\
				<div class="button-group-1">\
					<input type="button" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
	 </div>';
	p.searchRsltTpl = '<p class="search-result-msg"></p><div class="search-results btc-meal-list"></div>';

	// applied special meal prompt
	p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
		<em class="ico-alert"></em>\
		<p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
	</div>');
	
	var init = function (targetMenu, body) {
		p.btcm = paintBTCMeal(targetMenu);
		mealSelectionEvent(p.btcm);

		body.append(p.btcm);

		SIA.MealsSelection.initCloseAllPopup();
	};

	var paintBTCMeal = function (targetMenu) {
		var mealSubcategoryJson = p.BTCJson[0].mealSubcategory;

		var bookTheCookEl = $(_.template(p.BTCMain, {
			'targetClass': targetMenu,
			'mealType': p.mealType,
			'origin': p.origin,
			'destination': p.destination
		}));

		bookTheCookEl.append($(_.template(p.BTCCuisineCatMainWrapperTpl, {
			'mealType': p.mealType
		})));

		paintContent(
			mealSubcategoryJson,
			5, // max number per meal list
			bookTheCookEl.find('.tab-list'),
			bookTheCookEl.find('.more-tab'),
			bookTheCookEl.find('#tabWrapper')
		);

		// attach events
		loadMoreBtnEvent(bookTheCookEl, mealSubcategoryJson, 5);
		searchBtnEvent(bookTheCookEl);

		var moreBtn = new SIA.MoreTab(bookTheCookEl);
		moreBtn.init();

		return bookTheCookEl;
	};

	var paintContent = function (data, maxPerList, tabListEl, moreTabEl, mealListEl) {
		for (var i = 0; i < data.length; i++) {
			// show only one menu on tab
			if (i < 1) {
				tabListEl.prepend($(_.template(p.BTCCruisineCatListItemTpl, {
					'isActive': ((i == 0) ? 'active' : ''),
					'menuName': data[i].mealSubCategoryName,
					'tabMenu': i
				})));
			}

			// add to more dropdown list
			moreTabEl.append(_.template(p.selOptionItemTpl, {
				'tabMenu': i,
				'menuName': data[i].mealSubCategoryName
			}));

			// create the Cuisine Category - Meal listing
			var mealListingWrapper = $(_.template(p.mealMain, {
				'isHidden': ((i != 0) ? ' class="hidden"' : ''),
				'index': i
			}));

			var cruisineCatMealListingJson = data[i].Meals;
			for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
				if (j < maxPerList) {
					var btcMeal = paintBTCEachMealItem(cruisineCatMealListingJson[j], j, i);
					mealListingWrapper.append(btcMeal);
				}
			}

			// check if there is more than 5 items, then add a load more button
			if (cruisineCatMealListingJson.length > maxPerList) {
				mealListingWrapper.append(p.BTCLoadMoreBtn);
			}

			mealListEl.append(mealListingWrapper);
		}
	};

	var paintBTCEachMealItem = function (data, btnIndex, tabIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var btcSpecialMealName;
		if (data.isSpecialMeal === 'true' && typeof data.specialMealName !== 'undefined') {
			btcSpecialMealName = data.specialMealName;
		} else {
			btcSpecialMealName = 'SPECIAL';
		}

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';

			var specialtyInfo = data.specialityInfo[0];
			if (typeof specialtyInfo !== 'undefined') {
				specialityFootNote = data.specialityInfo[0].specialityFootNote;
				footIcon = p.mealIcons[data.specialityInfo[0].dishIconId];
			} else {
				// try old format of json data
				if (typeof data.specialityInfo !== 'undefined') {
					specialityFootNote = data.specialityInfo.specialityFootNote;
					footIcon = p.mealIcons[data.specialityInfo.dishIconId];
				}
			}

		}


		// check for selected menu in data
		var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
		selMeal = selMealsel[p.mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		var tpl;
		if (typeof data.mealImage !== 'undefined' && data.mealImage !== '') {
			tpl = p.BTCMealItemWithImage;
		} else {
			tpl = p.BTCMealItem;
		}

		return $(_.template(tpl, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': tabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': p.segment,
			'sector': p.sector,
			'paxid': p.paxid,
			'mealServiceCode': p.mealServiceCode,
			'mealCatCode': p.btcMealCatCode,
			'menuTarget': p.mainMenuTarget,
			'btcSpecialMealName': btcSpecialMealName,
			'btcImg': data.mealImage
		}));
	}

	var loadMoreBtnEvent = function (el, data, maxPerList) {
		var loadMoreBtns = el.find('.load-more-btn');
		var content = '';

		for (var i = 0; i < loadMoreBtns.length; i++) {
			$(loadMoreBtns[i]).on('click', function () {
				var t = $(this);

				// get parent tab content
				var tabContentIndex = t.parent().parent().attr('data-tab-content');

				// get parent load more button
				var loadMoreBtnMain = t.parent();

				// get current index data
				var currentData = data[tabContentIndex].Meals;

				// compute if there is more than 5 meals left
				var leftMealsLength = currentData.length - maxPerList;
				var limitResult = (leftMealsLength >= maxPerList) ? true : false;
				for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
					if (k == (maxPerList - 1) && limitResult) {
						break;
					}

					var el = paintBTCEachMealItem(currentData[j], j, tabContentIndex);
					content += el[0].outerHTML;
				}

				// append before load more button
				$(content).insertBefore(loadMoreBtnMain);

				// check if there is more left data, then hide the load more button
				// get total list length on tab content
				var tabContentMain = t.parent().parent();
				var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
				if (tabContentLength == currentData.length) {
					loadMoreBtnMain.addClass('hidden');
				}

				// add meal selection event
				mealSelectionEvent(tabContentMain);
			});
		}
	};

	var searchJSON = function (data, val) {
		var result = [];
		$.each(data, function (i, value) {
			$.each(data[i].Meals, function (j, value) {
				var mealMenuName = value.mealMenuName.toLowerCase();
				var mealMenuDescription = value.mealMenuDescription.toLowerCase();

				val = val.toLowerCase().trim();
				if ((mealMenuName.indexOf(val) >= 0) || (mealMenuDescription.indexOf(val) >= 0)) {
					result.push({
						'cat': i,
						'item': j
					});
				}
			});
		});

		return result;
	};

	var searchBtnEvent = function (el) {
		el.find('.search-btn').on('click', function (e) {
			e.preventDefault();

			var t = $(this);
			var parentEl = t.closest('.static-details');

			// hide btc tabs list
			parentEl.find('.tabs-component').addClass('hidden');

			// remove search message and result first
			parentEl.parent().find('.search-result-msg').remove();
			parentEl.parent().find('.search-results').remove();

			// show clear result link
			parentEl.find('.clear-search').removeClass('hidden');
			var searchRsltEl = parentEl.parent().append($(p.searchRsltTpl));

			// fetch search value
			var val = t.closest('.btc-search').find('[name="input-search-1"]').val();
			var mealSubCat = p.BTCJson['0'].mealSubcategory;

			// search in JSON the searched value
			var result = searchJSON(mealSubCat, val);

			if (result.length >= 1) {
				// generate search result message
				searchRsltEl.find('.search-result-msg').html(result.length + ' search results for \'' + val + '\'');

				// generate meals blocks
				var resCatIds = [];
				$.each(result, function (i, meal) {
					var resultsEl = searchRsltEl.find('.search-results');

					if (resCatIds.indexOf(meal.cat) <= -1) {
						resCatIds.push(meal.cat);

						resultsEl.append(_.template(p.searchMealTitleTpl, {
							'mealName': mealSubCat[meal.cat].mealSubCategoryName
						}));
					}

					resultsEl.append(paintBTCEachMealItem(mealSubCat[meal.cat].Meals[meal.item], meal.item, 0));
				});

				mealSelectionEvent(searchRsltEl);
			} else {
				// if no search result found
				searchRsltEl.find('.search-result-msg').html('There are no meals matching \'' + val + '\'. There are no results. Amend your search criteria to try again.');
			}
		});

		el.find('.clear-search').on('click', function (e) {
			e.preventDefault();

			var t = $(this);
			var mainEl = t.closest('.static-content');
			var searchResultEl = mainEl.find('.search-results');
			var selMeal = mainEl.find('.search-results .selected .fm-select-btn').attr('data-meal-mealcode');

			// clear search results
			mainEl.find('.search-result-msg').remove();
			searchResultEl.remove();

			// clear search field value
			t.prev().find('[name="input-search-1"]').val('');

			// hide clear search result link
			t.addClass('hidden');

			// re-paint btc meal
			var btc = t.next();
			btc.removeClass('hidden');

			// search and remove any previous selected meal
			SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm']);

			// re-select meal
			var selMealEl = btc.find('.btc-meal-list [data-meal-mealcode="' + selMeal + '"]');
			selMealEl.attr('value', 'Remove');
			selMealEl.closest('.fm-inf-menu').addClass('selected');

			// re-init meal selection event
			mealSelectionEvent(btc);
		});
	};

	var mealSelectionEvent = function (el) {
		var allBtns = el.find('.fm-select-btn');
		
		$.each(allBtns, function (index, btn) {
			$(btn).parent().parent().off().on('click', function (e) {
				e.preventDefault();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				// check if current meal card is selected again, then remove it
				if (parentEl.hasClass('selected')) {
					// if ($("body").hasClass("meals-selection-economy")) {
						var labelName = "Inflight Meal";
						var pax = SIA.MealsSelection.public.data.paxList[paxid];
						var segmentMeals = Object.values(pax.selectedMeals[segment].selectedMeal);
						var segmentMeal = _.findWhere(segmentMeals, {isEating: true, mealCategoryCode: "SPML"});
						if (segmentMeal) {
							labelName = segmentMeal.mealMenuName;
						}
						var parentWrapper = t.closest(".fm-inf-menu");
						var allPrompt = parentWrapper.find(".confirmation-prompt");
						var removePrompt = parentWrapper.find(".confirmation-remove--blue");
						
						var allSegmentPrompts = parentWrapper.parent().find('.confirmation-prompt');

						allSegmentPrompts.each(function() { $(this).addClass("hidden").prev().find('.fm-select-btn').removeClass('hidden'); });
						allPrompt.each(function() { $(this).addClass("hidden") });
						removePrompt.find(".menu-label").text(labelName);
						removePrompt.removeClass("hidden");
						t.addClass('hidden');

						removePrompt.find(".btn-cancel")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								allPrompt.each(function() { $(this).addClass("hidden") });

								t.removeClass('hidden');

							});

						removePrompt.find(".btn-proceed")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								setStatePastMealsOnModal(false);
								var segment = SIA.MealsSelection.public.currentOpenModal;
								segment.find("[data-remove-trigger]").trigger("proceed");
								removePrompt.addClass("hidden");

								t.removeClass('hidden');
							});
						return;
					// }

					// Add selected class to the input and to the container
					parentEl.removeClass('selected');
					t.removeClass('selected');

					// remove hidden class to show the selected badge
					badgeEl.addClass('hidden');

					t.attr('value', 'Select');
					
					SIA.MealsSelection.removeButtonListener();

					var inflLbl = 'Inflight Menu';
                    if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';

					SIA.MealsSelection.setMainMenuLbl(t, inflLbl, true);
					SIA.MealsSelection.updateData(t);

					p.btcm.find('.sticky-confirm-bar').addClass('no-selection');

					// Check if there are selected special meals then select same meal for this service code
					for (var i = 0; i < SIA.MealsSelection.public.spml.length; i++) {
						var spml = SIA.MealsSelection.public.spml[i];
						if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
							spml.selectSegmentMeal();
							p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);

							parentEl.after(p.spmlAlertPrompt);

							$(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
								var t = $(e.target);
								if (t.hasClass('passenger-info__text') ||
									t.hasClass('meal-tab-item') ||
									t.hasClass('accordion__control') ||
									t.hasClass('ico-point-d') ||
									t.hasClass('fm-ico-text') ||
									t.hasClass('meal-service-name') ||
									t.hasClass('flight-ind') ||
									t.hasClass('btn-back') ||
									t.hasClass('fm-inf-menu') ||
									t.hasClass('search-btn') ||
									t.hasClass('popup__close') ||
									t.hasClass('lightbox-btn-back')
								) {
									p.spmlAlertPrompt.detach();

									$(document).off('click.spmlPromptCheck');
								}
							});

							$('.more-tab').on('change.promptCheck', function (e) {

								$('.more-tab').off('change.promptCheck');
								$(document).off('click.spmlPromptCheck');

								p.spmlAlertPrompt.detach();

							});

							break;
						}
					}
				} else {
					var parentWrapper = t.closest(".fm-inf-menu");
					var allSegmentPrompts = parentWrapper.parent().find('.confirmation-prompt');
					allSegmentPrompts.each(function() { $(this).addClass("hidden").prev().find('.fm-select-btn').removeClass('hidden'); });
					
					// search and remove any previous selected meal
					SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

					// Add selected class to the input and to the container
					parentEl.addClass('selected');
					t.addClass('selected');
					t.removeClass('hidden');

					// remove hidden class to show the selected badge
					badgeEl.removeClass('hidden');

					t.attr('value', 'Remove');


					p.btcm.find('.sticky-confirm-bar').removeClass('no-selection');

					SIA.MealsSelection.deselectEcoPeyMenu(t);

					var selLabel = '';
					if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected hidden">Selected:</span> ';
					SIA.MealsSelection.setMainMenuLbl(t, selLabel + t.attr('data-meal-menuname') + ' (Book the Cook)');
					SIA.MealsSelection.updateData(t);
					setStatePastMealsOnModal(true);

					if(!t.hasClass('past-meal')) SIA.MealsSelection.selectPastMeal(t);
				}
			});
		});
	};

	var setStatePastMealsOnModal = function(state) {
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var trigger = SIA.MealsSelection.public.currentOpenModal.find("[data-trigger-popup]");
		var el = $(trigger.data("trigger-popup"));
		var btn = el.find(".not-eating-supper").find("input");
		var serviceCode = btn.data("meal-servicecode");
		var mealData = pax.selectedMeals[segment].selectedMeal[serviceCode];
		var pastBtn = $(el.find(".past-meals-wrap").find('[data-meal-mealcode='+ mealData.mealCode +']'));
		SIA.MealsSelection.removePastMealSelection(pastBtn);
		if (state) {
			pastBtn.closest(".fm-inf-menu").addClass('selected');
			pastBtn.val('Remove');
		}
	};

	var destroy = function () {
		p.btcm.remove();
	};

	var oPublic = {
		init: init,
		paintBTCMeal: paintBTCMeal,
		public: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.SpecialMeal = function (
	data,
	mealIcons,
	mealSel,
	bckAllMeal,
	segment,
	sector,
	paxid,
	mealServiceCode,
	jsonData,
	origin,
	destination,
	mealType,
	mainMenuTarget,
	accordContainer) {

	var p = {};

	p.SPMJson = data;
	p.mealIcons = mealIcons;
	mealSelEvent = mealSel;
	bckAllMealsEvent = bckAllMeal;
	p.segment = segment;
	p.segmentId = segment;
	p.sector = sector;
	p.paxid = paxid;
	p.mealServiceCode = mealServiceCode;
	p.spmMealCatCode = data[0].mealCategoryCode;
	p.data = jsonData;
	p.origin = origin;
	p.destination = destination;
	p.mealType = mealType;
	p.mainMenuTarget = mainMenuTarget;

	// index all meal items
	p.allMealItems = [];
	p.curPrompt = null;

	// declare templates
	p.SPMMain = '<aside class="popup popup-meals menu-list-spm popup--spm-menu-<%= targetClass %> hidden" data-mealtype="<%= mealType %>" data-origin="<%= origin %>" data-dest="<%= destination %>">';
	p.SPMCuisineCatMainWrapperTpl = '<div class="popup__inner">\
			<div class="popup__content">\
				<div class="book-the-cook">\
					<p class="light-box-btn-mb">\
						<a href="#" class="lightbox-btn-back" data-close="true">\
							<em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to categories \
						</a>\
					</p>\
					<h3 class="fm-title-4--blue">Special Meals</h3>\
					<p class="light-box-text">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
					before departure, or 48 hours for nut-free and Kosher meals.</p>\
					<div class="static-content">\
							<div class="static-details">\
								<div class="tabs-component">\
									<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">\
											<ul class="tab tab-list" role="tablist">\
												<li class="more-item">\
													<a href="#">More<em class="ico-dropdown"></em></a>\
												</li>\
											</ul>\
											<div class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">\
												<label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span>\
												<select class="more-tab" name="calcTabSelect">\
												</select>\
											</div>\
											<div class="tab-wrapper btc-meal-list" id="tabWrapper">\
											</div>\
									</div>\
								</div>\
							</div>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div class="sticky-confirm-bar no-selection">\
			<button class="btn-1" data-close-all="true">Done</button>\
		</div>';
	p.mealMain = '<div <%= isHidden %> data-tab-content="<%= index %>">';
	p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
	p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';
	p.SPMLoadMoreBtn = '<div class="load-more"><input type="button" name="select" id="" value="Load more" class="load-more-btn"></div>';
	p.SPMCruisineCatListItemTpl = '<li class="tab-item <%= isActive %>" aria-selected="true" role="tab" data-tab-menu="<%= tabMenu %>"><a href="#"><%= menuName %></a></li>';
	p.SPMMealItem = '<div data-meal-item class="fm-inf-menu <%= isSel %>">\
		  <span class="fm-inf-menu-select-badge">Selected</span>\
		  <!-- <span class="special-badge <%= isSpecial %>">SPECIAL</span> -->\
		  <h4 class="fm-inf-menu-title"><%= mealMenuName %></h4>\
		  <p class="fm-inf-menu-desc"><%= mealMenuDesc %></p>\
		  <div class="fm-footnote-txt">\
				<em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em>\
				<span class="fm-dish-text"><%= specialityFootNote %></span>\
				<input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" data-main-menu-target="<%= menuTarget %>">\
		  </div>\
			<div data-prompt="with-selection" class="confirmation-prompt confirmation-prompt--blue hidden">\
				<p class="meal-sub-title-blue"> Selecting <span><%= mealName %></span> will change your other special meals selection from <%= origin %> to <%= destination %> to <span><%= menuName %></span> as well. </p>\
				<div class="button-group-1">\
					<input type="button" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
			<div class="confirmation-prompt confirmation-remove--blue hidden">\
				<em class="ico-alert"></em>\
				<p class="meal-sub-title-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit <span>Inflight Meal</span> sed do eiusmod tempor incididunt ut labore et dolore.</p>\
				<div class="button-group-1">\
					<input type="button" value="Proceed" class="btn-1 btn-proceed">\
					<a href="#" class="btn-2 btn-cancel">Cancel</a>\
				</div>\
			</div>\
			<div data-prompt="initial" class="confirmation-prompt hidden">\
			 <em class="ico-alert"></em>\
				<p class="meal-sub-title-black">For your convenience, all your meal choices from <%= origin %> to <%= destination %> have been changed to <span><%= mealName %></span> as well. If you prefer other meals, make your selections below. </p>\
			</div>\
	 </div>';

	p.searchRsltTpl = '<p class="search-result-msg"></p><div class="search-results spm-meal-list"></div>';

	var init = function (targetMenu, body) {
		p.spm = paintSPMMeal(targetMenu);
		mealSelectionEvent(p.spm);

		body.append(p.spm);

		SIA.MealsSelection.initCloseAllPopup();
	};

	var paintSPMMeal = function (targetMenu) {
		var mealSubcategoryJson = p.SPMJson[0].mealSubcategory;

		var specialMealEl = $(_.template(p.SPMMain, {
			'targetClass': targetMenu,
			'mealType': p.mealType,
			'origin': p.origin,
			'destination': p.destination
		}));

		specialMealEl.append($(_.template(p.SPMCuisineCatMainWrapperTpl, {
			'originDest': p.origin + ' <em class="ico-airplane-2"></em> ' + p.destination,
			'mealType': p.mealType
		})));

		paintContent(
			mealSubcategoryJson,
			5, // max number per meal list
			specialMealEl.find('.tab-list'),
			specialMealEl.find('.more-tab'),
			specialMealEl.find('#tabWrapper')
		);

		// attach events
		loadMoreBtnEvent(specialMealEl, mealSubcategoryJson, 5);

		var moreBtn = new SIA.MoreTab(specialMealEl);
		moreBtn.init();

		return specialMealEl;
	};

	var paintContent = function (data, maxPerList, tabListEl, moreTabEl, mealListEl) {
		for (var i = 0; i < data.length; i++) {
			// show only one menu on tab
			if (i < 1) {
				tabListEl.prepend($(_.template(p.SPMCruisineCatListItemTpl, {
					'isActive': ((i == 0) ? 'active' : ''),
					'menuName': data[i].mealSubCategoryName,
					'tabMenu': i
				})));
			}

			// add to more dropdown list
			moreTabEl.append(_.template(p.selOptionItemTpl, {
				'tabMenu': i,
				'menuName': data[i].mealSubCategoryName
			}));

			// create the Cuisine Category - Meal listing
			var mealListingWrapper = $(_.template(p.mealMain, {
				'isHidden': ((i != 0) ? ' class="hidden"' : ''),
				'index': i
			}));

			var cruisineCatMealListingJson = data[i].Meals;
			for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
				var meal = paintSPMEachMealItem(cruisineCatMealListingJson[j], j, i);
				mealListingWrapper.append(meal);
				p.allMealItems.push(meal);

				if (j > maxPerList - 1) meal.addClass('hidden');
			}

			// check if there is more than 5 items, then add a load more button
			if (cruisineCatMealListingJson.length > maxPerList) {
				mealListingWrapper.append(p.SPMLoadMoreBtn);
			}

			mealListEl.append(mealListingWrapper);
		}
	};

	var getOtherSpecials = function (meal) {
		var otherSelectedSM = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var curSegment = pax.selectedMeals[segment];

		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode !== meal.attr('data-meal-mealcode')) {
				otherSelectedSM.push({
					'k': k,
					'val': v
				});
			}
		});

		return otherSelectedSM;
	};
	var getMoreToSelect = function (meal) {
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var curSegment = pax.selectedMeals[segment];

		var hasSPMLToChange = false;
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== meal.attr('data-meal-servicecode') && (v.mealCategoryCode === 'SPML' || v.mealCategoryCode === null) && v.isEating) {
				hasSPMLToChange = true;
			}
		});

		return hasSPMLToChange;
	};

	var paintSPMEachMealItem = function (data, btnIndex, tabIndex) {
		// check if it's special meal
		var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

		var footLogo = 'hidden';
		var footIcon = '';
		var specialityFootNote = '';
		if (typeof data.specialityInfo !== 'undefined') {
			footLogo = '';

			var specialtyInfo = data.specialityInfo[0];
			if (typeof specialtyInfo !== 'undefined') {
				specialityFootNote = data.specialityInfo[0].specialityFootNote;
				footIcon = p.mealIcons[data.specialityInfo[0].dishIconId];
			} else {
				// try old format of json data
				if (typeof data.specialityInfo !== 'undefined') {
					specialityFootNote = data.specialityInfo.specialityFootNote;
					footIcon = p.mealIcons[data.specialityInfo.dishIconId];
				}
			}
		}

		// check for selected menu in data
		var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
		selMeal = selMealsel[p.mealServiceCode];
		var isSel = '';
		var selected = 'Select';
		if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
			isSel = 'selected';
			selected = 'Remove';
		}

		return $(_.template(p.SPMMealItem, {
			'isSel': isSel,
			'selected': selected,
			'isSpecial': isSpecialAvailable,
			'mealMenuName': data.mealMenuName,
			'mealMenuDesc': data.mealMenuDescription,
			'footLogo': footLogo,
			'footIcon': footIcon,
			'specialityFootNote': specialityFootNote,
			'index': btnIndex,
			'tabIndex': tabIndex,
			'mealName': data.mealMenuName,
			'mealCode': data.mealCode,
			'segment': p.segment,
			'sector': p.sector,
			'paxid': p.paxid,
			'mealServiceCode': p.mealServiceCode,
			'menuName': data.mealMenuName,
			'mealCatCode': p.spmMealCatCode,
			'menuTarget': p.mainMenuTarget,
			'origin': p.origin,
			'destination': p.destination,
		}));
	}

	var loadMoreBtnEvent = function (el, data, maxPerList) {
		var loadMoreBtns = el.find('.load-more-btn');
		var content = '';

		for (var i = 0; i < loadMoreBtns.length; i++) {
			$(loadMoreBtns[i]).on('click', function () {
				var t = $(this);

				// get parent tab content
				var tabContentIndex = t.parent().parent().attr('data-tab-content');

				// get parent load more button
				var loadMoreBtnMain = t.parent();
				var loadMoreContainer = t.parent().parent();

				// get current index data
				var currentData = data[tabContentIndex].Meals;

				// compute if there is more than 5 meals left
				var leftMealsLength = currentData.length - maxPerList;
				var limitResult = (leftMealsLength >= maxPerList) ? true : false;
				for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
					if (k == (maxPerList - 1) && limitResult) break;
					loadMoreContainer.find('.fm-inf-menu').eq(j).removeClass('hidden');
				}

				// append before load more button
				$(content).insertBefore(loadMoreBtnMain);

				// check if there is more left data, then hide the load more button
				// get total list length on tab content
				var tabContentMain = t.parent().parent();
				var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
				if (tabContentLength == currentData.length) {
					loadMoreBtnMain.addClass('hidden');
				}

				// add meal selection event
				mealSelectionEvent(tabContentMain);
			});
		}
	};

	var mealSelectionEvent = function (el) {
		var allBtns = el.find('.fm-select-btn');

		$.each(allBtns, function (index, btn) {
			$(btn).parent().parent().on('click', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this).find('.fm-select-btn');
				var parentEl = t.parent().parent();
				var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

				var otherSM = getOtherSpecials(t);
				var haveItemsToUpdate = getMoreToSelect(t) || $('body').hasClass('meals-selection-bah');

				// check if current meal card is selected again, then remove it
				if (t.val() === 'Remove') {
					if (true) {
						var parentWrapper = t.closest(".fm-inf-menu");
						var allPrompt = parentWrapper.find(".confirmation-prompt");
						var removePrompt = parentWrapper.find(".confirmation-remove--blue");

						t.addClass('hidden');
						
						allPrompt.each(function() { $(this).addClass("hidden") });
						removePrompt.removeClass("hidden");

						removePrompt.find(".btn-cancel")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								allPrompt.each(function() { $(this).addClass("hidden") });
							});

						removePrompt.find(".btn-proceed")
							.off().on("click", function(evt) {
								evt.preventDefault();
								evt.stopImmediatePropagation();
								setStatePastMealsOnModal(false);
								var segment = SIA.MealsSelection.public.currentOpenModal;
								segment.find("[data-remove-trigger]").trigger("proceed");
								removePrompt.addClass("hidden");
								t.removeClass('hidden');
							});
						return;
					}
					unSelectMeal(t);

					var inflLbl = 'Inflight Menu';
					if (SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
					
					p.spm.find('.sticky-confirm-bar').addClass('no-selection');

					SIA.MealsSelection.setMainMenuLbl(t, inflLbl, true);
					SIA.MealsSelection.updateData(t);
				} else {
					// Check for special meals selected in other sectors in same leg
					if (haveItemsToUpdate) {
						if (otherSM.length > 0) {
							var prompt = parentEl.find('[data-prompt="with-selection"]');
						} else {
							var prompt = parentEl.find('[data-prompt="initial"]');
						}

						// Remove any selected prompt
						if (p.curPrompt !== null) {
							p.curPrompt.prev().find('.fm-select-btn').val('Select');
							p.curPrompt.addClass('hidden');
							p.curPrompt.parent().removeClass('int-prompt');
							p.curPrompt.find('.btn-cancel').off();
							p.curPrompt.find('.btn-proceed').off();
						}

						if (otherSM.length > 0) {
							t.val('Selected');

							parentEl.addClass('int-prompt');

							prompt.find('.btn-cancel').off().on({
								'click': function (e) {
									e.preventDefault();
									e.stopImmediatePropagation();

									$(this).off();

									t.val('Select');
									parentEl.removeClass('int-prompt');
									parentEl.find('.confirmation-prompt').addClass('hidden');

									p.curPrompt = null;
								}
							});

							prompt.find('.btn-proceed').off().on({
								'click': function (e) {
									e.preventDefault();
									e.stopImmediatePropagation();

									t.removeClass('hidden');
									// remove listener to prevent double calls on otherSM
									$(this).off();

									// reset other meals before assigning selected class to current item
									SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
									
									SIA.MealsSelection.removeNotEatingCheckSPML(t);
									
									selectMeal(t);

									// apply selection to other special meals in same leg
									selectSameMeal(t, otherSM);

									SIA.MealsSelection.deselectEcoPeyMenu(t);

									p.spm.find('.sticky-confirm-bar').removeClass('no-selection');

									// assign data
									var selLabel = '';
									if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected hidden">Selected:</span> ';
									SIA.MealsSelection.setMainMenuLbl(t, selLabel + t.attr('data-meal-menuname') + ' (Special Meals)');
									SIA.MealsSelection.updateData(t);
									setStatePastMealsOnModal(true);
								}
							});

							prompt.removeClass('hidden');
						} else {
							// reset other meals before assigning selected class to current item
							SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);
							SIA.MealsSelection.removeNotEatingCheckSPML(t);

							selectMeal(t);

							p.spm.find('.sticky-confirm-bar').removeClass('no-selection');

							SIA.MealsSelection.deselectEcoPeyMenu(t);

							// apply selection to other special meals in same leg
							selectSameMeal(t, otherSM);

							// assign data
							var selLabel = '';
							if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected hidden">Selected:</span> ';
							SIA.MealsSelection.setMainMenuLbl(t, selLabel + t.attr('data-meal-menuname') + ' (Special Meals)');
							SIA.MealsSelection.updateData(t);

							prompt.removeClass('hidden');

							// prompt.removeClass('hidden');
							$(document).off().on('click.promptCheck', function (e) {
								var t = $(e.target);
								if (t.hasClass('passenger-info__text') ||
									t.hasClass('meal-tab-item') ||
									t.hasClass('accordion__control') ||
									t.hasClass('meal-sub-title-black') ||
									t.hasClass('ico-point-d') ||
									t.hasClass('fm-ico-text') ||
									t.hasClass('meal-service-name') ||
									t.hasClass('flight-ind') ||
									t.hasClass('lightbox-btn-back') ||
									t.hasClass('popup__close')
								) {
									p.curPrompt.addClass('hidden');
									p.curPrompt.parent().removeClass('int-prompt');

									$(document).off('click.promptCheck');
								}
							});


							$('.more-tab').on('change.promptCheck', function (e) {

								$('.more-tab').off('change.promptCheck');
								$(document).off('click.spmlPromptCheck');

								p.curPrompt.detach();

							});
						}

						p.curPrompt = prompt;
					} else {
						// reset other meals before assigning selected class to current item
						SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

						selectMeal(t);

						p.spm.find('.sticky-confirm-bar').removeClass('no-selection');

						SIA.MealsSelection.deselectEcoPeyMenu(t);

						// assign data
						var selLabel = '';
						if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected hidden">Selected:</span> ';
						SIA.MealsSelection.setMainMenuLbl(t, selLabel + t.attr('data-meal-menuname') + ' (Special Meals)');
						SIA.MealsSelection.updateData(t);
					}

					if(!t.hasClass('past-meal')) SIA.MealsSelection.selectPastMeal(t);

					setStatePastMealsOnModal(true);
				}
			});
		});
	};

	var setStatePastMealsOnModal = function(state) {
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var triggers = SIA.MealsSelection.public.currentOpenModal.closest(".accordion-wrapper-content").find("[data-trigger-popup]");
		_.each(triggers, function(el) {
			el = $($(el).data("trigger-popup"));
			var btn = el.find(".not-eating-supper").find("input");
			var serviceCode = btn.data("meal-servicecode");
			var mealData = pax.selectedMeals[segment].selectedMeal[serviceCode];
			var pastBtn = $(el.find(".past-meals-wrap").find('[data-meal-mealcode='+mealData.mealCode+']'));
			SIA.MealsSelection.removePastMealSelection(pastBtn);
			if (state) {
				pastBtn.closest(".fm-inf-menu").addClass('selected');
				pastBtn.val('Remove');
			}
		});
	};

	var selectSameMeal = function (meal) {
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var curSegment = pax.selectedMeals[segment];
		// loop through each special meal in MealSelection module
		for (var i = 0; i < SIA.MealsSelection.public.spml.length; i++) {
			var spml = SIA.MealsSelection.public.spml[i];

			spml.p.spm.find('.fm-select-btn').each(function () {
				var t = $(this);

				var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];

				if (typeof curCatCode !== 'undefined' && curCatCode.isEating && t.attr('data-meal-category') === 'SPML' && (curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {
					// Unselect other selected special meals in same sector
					if (t.attr('data-segment') === meal.attr('data-segment')) {
						unSelectMeal(t);
					}

					// Find the selected special meal in current segment and select it
					if (t.attr('data-segment') === meal.attr('data-segment') &&
						t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')) {
						selectMeal(t);

						SIA.MealsSelection.deselectEcoPeyMenu(t);
						
						var selLabel = '';
						if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected hidden">Selected:</span> ';
						SIA.MealsSelection.setMainMenuLbl(t, selLabel + t.attr('data-meal-menuname') + ' (Special Meals)');
						SIA.MealsSelection.updateData(t);
					}
				}
			});
		}
	};

	var getSelectedSpml = function () {
		var selectedSPML = '';
		var pax = SIA.MealsSelection.public.data.paxList[paxid];
		var curSegment = pax.selectedMeals[segment];
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (v.mealCategoryCode === 'SPML' && v.mealCode !== null) {
				selectedSPML = v.mealCode;
			}
		});

		return selectedSPML;
	};

	var getSameSpecials = function (meal) {
		var sameSelectedSM = {};
		sameSelectedSM['length'] = 0;
		sameSelectedSM['items'] = [];
		var pax = SIA.MealsSelection.public.data.paxList[paxId];
		var curSegment = pax.selectedMeals[segmentId];
		_.each(curSegment.selectedMeal, function (v, k, l) {
			if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode === meal.attr('data-meal-mealcode')) {
				sameSelectedSM.items.push({
					'k': k,
					'val': v
				});
			}

			sameSelectedSM.length++;
		});

		return sameSelectedSM;
	};

	// Used to select special meals when a meal is unselected from BTC or INFLM
	var selectSegmentMeal = function (mealServiceCode, segmentId) {
		var selectedMealCode = getSelectedSpml();

		for (var i = 0; i < p.allMealItems.length; i++) {
			var meal = p.allMealItems[i].find('.fm-select-btn');
			if (meal.attr('data-meal-mealcode') === selectedMealCode) {

				var parentEl = meal.parent().parent();
				var accordionContent = meal.closest('.accordion__content');
				var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');

				selectMeal(meal);

				p.curSMLName = meal.attr('data-meal-menuname');
				var selLabel = '';
				if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selLabel = '<span class="title-selected">Selected:</span> ';
				accordionTabMealTitle.html(selLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');

				// assign data
				SIA.MealsSelection.updateData(meal);

				break;
			}
		}
	};

	var selectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		// Add selected class to the input and to the container
		parentEl.addClass('selected');
		meal.addClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.removeClass('hidden');

		meal.attr('value', 'Remove');
	};
	var unSelectMeal = function (meal) {
		var parentEl = meal.parent().parent();
		var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

		// Add selected class to the input and to the container
		parentEl.removeClass('selected');
		meal.removeClass('selected');

		// remove hidden class to show the selected badge
		badgeEl.addClass('hidden');

		// reset prompt
		parentEl.removeClass('int-prompt');
		parentEl.find('.confirmation-prompt').addClass('hidden');

		meal.attr('value', 'Select');
	};

	var destroy = function () {
		for (var i = 0; i < p.allMealItems.length; i++) {
			p.allMealItems[i].remove();
		}

		p.spm.remove();
		accordContainer.remove();
	};

	var oPublic = {
		init: init,
		paintSPMMeal: paintSPMMeal,
		getSelectedSpml: getSelectedSpml,
		selectSegmentMeal: selectSegmentMeal,
		p: p,
		destroy: destroy
	};

	return oPublic;
};

SIA.MoreTab = function (el) {
	var p = {};

	p.el = el;

	var init = function () {
		evnt();
	};

	var evnt = function () {
		var moreTab = p.el.find('.more-tab');

		moreTab.on('change', function (e) {
			e.preventDefault();

			var t = $(this);
			var tabIndex = t.val();
			var tabContentEl = moreTab.parent().next();

			t.parent().prev().find('.tab-item a').html(t.find('option').eq(tabIndex).text())

			$.each(tabContentEl.find('[data-tab-content]'), function (i, value) {
				$(value).addClass('hidden');
			});

			// show the current selected tab index content
			tabContentEl.find('[data-tab-content]').eq(tabIndex).removeClass('hidden');
		});
	};

	var oPublic = {
		init: init
	};

	return oPublic;
};

SIA.Accordion = function () {
	var vars = SIA.global.vars;
	var win = vars.win;
	var body = vars.body;
	var htmlBody = $('html, body');
	var timeAnimate = 400;
	// This function use for init accordion
	var initAccordion = function () {
		var contentWrapper = $('[data-accordion-wrapper=1]');
		var firstTriggerAccordion = '[data-accordion-trigger=1]',
			secondTriggerAccordion = '[data-accordion-trigger=2]',
			firstContentAccordion = '[data-accordion-content=1]',
			secondContentAccordion = '[data-accordion-content=2]';

		if (contentWrapper.find('[data-accordion=1]').length && !contentWrapper.parent().hasClass('disable-accordion')) {
			contentWrapper.each(function () {
				var self = $(this);
				var openAllAccordion = $('.open-all-btn', self);
				var $firstTriggerAccordion = $(firstTriggerAccordion, self),
					$secondTriggerAccordion = $(secondTriggerAccordion, self);
				var animateHtmlBody = function (trigger) {
					if (trigger.offset().top === win.scrollTop()) {
						return;
					}
					htmlBody.stop().animate({
						scrollTop: trigger.offset().top
					}, timeAnimate, function () {
						// fix issue for set window.location.hash faqs pages
						if (body.hasClass('faqs-pages')) {
							if (trigger.hasClass('open-all-btn')) {
								window.location.hash = '';
							} else {
								trigger.trigger('setHash.faqsPages');
							}
						}
					});
				};
				var scrollTop = -1;
				var clearTimerUpdate = null;
				var dT = 300;

				var autoUpdateScrollTop = function (trigger) {
					clearTimerUpdate = setTimeout(function () {
						if (scrollTop === trigger.offset().top) {
							animateHtmlBody(trigger);
							scrollTop = -1;
							clearTimeout(clearTimerUpdate);
						} else {
							scrollTop = trigger.offset().top;
							autoUpdateScrollTop(trigger);
						}
					}, dT);
				};

				openAllAccordion.off('click.openAllAccordion').on('click.openAllAccordion', function (e) {
					e.preventDefault();

					if (openAllAccordion.hasClass('open')) {
						if ($firstTriggerAccordion.length) {
							$firstTriggerAccordion.siblings(firstContentAccordion).stop().slideUp(400);
						}
						if ($secondTriggerAccordion.length) {
							$secondTriggerAccordion.filter('.active').removeClass('active').siblings(secondContentAccordion).slideUp(400);
						}
						openAllAccordion.removeClass('open active').html(L10n.accordion.open);
					} else {
						if ($firstTriggerAccordion.length) {
							$firstTriggerAccordion.not('.active').siblings(firstContentAccordion).stop().slideDown(400);
							$firstTriggerAccordion.filter('.active').removeClass('active');
						}
						if ($secondTriggerAccordion.length) {
							$secondTriggerAccordion.not('.active').addClass('active').siblings(secondContentAccordion).slideDown(400);
						}
						animateHtmlBody(openAllAccordion);
						openAllAccordion.addClass('open active').html(L10n.accordion.collapse);
					}
				});

				$firstTriggerAccordion.off('beforeAccordion.refeshOpenAll').on('beforeAccordion.refeshOpenAll', function () {
					var self = $(this);
					if (openAllAccordion.hasClass('open')) {
						$firstTriggerAccordion.not(self).addClass('active');
						openAllAccordion.removeClass('open').html(L10n.accordion.open);
						autoUpdateScrollTop(self);
					}
				});

				if (self.data('accordion')) {
					self.accordion('refresh');
				} else {
					self.accordion({
						wrapper: '[data-accordion-wrapper-content=1]',
						triggerAccordion: firstTriggerAccordion,
						contentAccordion: firstContentAccordion,
						activeClass: 'active',
						duration: timeAnimate,
						beforeAccordion: function (trigger, content) {
							var isAddonPage = body.is('.add-ons-1-landing-page') || body.is('.mp-add-ons-page'),
								sld = isAddonPage === true ? trigger.find('.slides') : content.find('.slides');

							if (sld.length) {
								if (!sld.find('.slide-item').width()) {
									var slideWidth = (sld.closest('.accordion__content').parent().width() / 2) - 50;
									sld.find('.slick-track').width(100000);
									sld.find('.slide-item').width(slideWidth);
								}
								// sld.slickGoTo(sld.slickCurrentSlide());
								sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));
							}

							// if (trigger.hasClass('active') && openAllAccordion.hasClass('open')){
							// 	openAllAccordion.removeClass('open').text(L10n.accordion.open);
							// 	$firstTriggerAccordion.not(trigger).trigger('click.accordion');
							// }
						},
						afterAccordion: function (trigger) {
							if (trigger.hasClass('active') && !body.is('.promotion-enhancement')) {
								animateHtmlBody(trigger);
							}
						}
					});
				}
			});

		}
		var accordionContentInfo = $('[data-accordion-wrapper=2]');
		if (accordionContentInfo.find('[data-accordion=2]').length) {

			var animateHtmlBody = function (trigger) {
				if (trigger.offset().top === win.scrollTop()) {
					return;
				}
				htmlBody.stop().animate({
					scrollTop: trigger.offset().top
				}, timeAnimate);
			};

			if (accordionContentInfo.data('accordion')) {
				accordionContentInfo.accordion('refresh');
			} else {
				accordionContentInfo.accordion({
					wrapper: '[data-accordion-wrapper-content=2]',
					triggerAccordion: secondTriggerAccordion,
					contentAccordion: secondContentAccordion,
					activeClass: 'active',
					duration: timeAnimate,
					beforeAccordion: function (trigger, content) {
						var sld = trigger.find('.slides');
						if (sld.length) {
							if (sld.find('.slide-item').width() < sld.closest('.accordion__control').width()) {
								var slideWidth = sld.closest('.accordion__control').width();
								sld.find('.slick-track').width(100000);
								sld.find('.slide-item').width(slideWidth);

							}
							sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));

						}

					},
					afterAccordion: function (trigger) {
						if (trigger.hasClass('active')) {
							animateHtmlBody(trigger);
						}

					}
				});
			}
		}
	};

	var oPublic = {
		init: initAccordion
	};

	return oPublic;
};

SIA.BSPUtil = function() {

	var p = SIA.MealsSelection.public;

	var addBSPTpl = function(ids, pax) {
		var segment = ids.segmentId + "" + ids.sectorId;
		var listTpl = '<ul class="bsp-flights-cost__details" data-bsp-mealid="<%- paxId %>">\
			<span><%-paxName.toUpperCase()%></span>\
			<li data-bsp-currentprice="<%- currentPrice ? currentPrice : 0 %>" data-bsp-segment="<%- segment %>">\
				<span class="bsp-addons__flightinfo"><%-origin%> - <%-destination%></span>\
				<br>\
				<span style="float:left; min-width: 50%" class="bsp-addons__details"> Premium Selection</span>\
				<span class="price" data-bsp-mealprice="<%-price.toFixed(2)%>" data-bsp-segment-paxid="<%- paxId %>"><%-price.toFixed(2)%></span>\
			</li>\
		</ul>'
		var segmentTpl = '<li data-bsp-currentprice="<%- currentPrice ? currentPrice : 0 %>" data-bsp-segment="<%- segment %>">\
			<span class="bsp-addons__flightinfo"><%-origin%> - <%-destination%></span>\
			<br>\
			<span style="float:left; min-width: 50%" class="bsp-addons__details"> Premium Selection</span>\
			<span class="price" data-bsp-mealprice="<%-price.toFixed(2)%>" data-bsp-segment-paxid="<%- paxId %>"><%-price.toFixed(2)%></span>\
		</li>';

		var listEl = $(_.template(listTpl, {
			paxId: pax.paxId,
			segment: segment,
			origin: pax.origin,
			destination: pax.destination,
			price: parseFloat(pax.price),
			paxName: pax.paxName,
			currentPrice: parseFloat(pax.currentPaxPrice)
		}));

		var mealEl = $("[data-bsp-mealid='" + pax.paxId + "']");
		var segmentEl = mealEl.find("[data-bsp-segment='" + segment + "']");
		
		if (!mealEl.length) {
			if (!mealEl.length) {
				if (!$("[data-bsp-mealid]").length) {
					listEl.insertAfter($(".append-meal-list"));
				} else {
					listEl.insertAfter($("[data-bsp-mealid]").last());
				}
				
			}
		}
		if (segmentEl.length) {
			segmentEl.find(".bsp-addons__flightinfo").text(pax.origin + " - " + pax.destination);
			segmentEl.find(".price").data("bsp-mealprice", pax.price);
			segmentEl.find(".price").text(pax.price);
		} else {
			var newSegment = $(_.template(segmentTpl, {
				segment: segment,
				origin: pax.origin,
				destination: pax.destination,
				price: parseFloat(pax.price),
				paxId: pax.paxId,
				currentPrice: parseFloat(pax.currentPaxPrice)
			}));
			mealEl.append(newSegment);
		}
		
		calculatePaid();
		calculateTotal();
	};

	var calculatePaid = function() {
		var total = 0;
		$("[data-bsp-currentprice]").each(function() {
			var $this = $(this);
			total += parseFloat($this.data("bsp-currentprice"));
		});
		setSubTotal(total);
	};

	var calculateTotal = function() {
		var total = 0;
		var currency = "SGD"
		$("[data-bsp-mealprice]").each(function() {
			var $this = $(this);
			total += parseFloat($this.data("bsp-mealprice"));
		});

		if (!total) {
			var bookingPanel = $("[data-booking-summary-panel]");
			bookingPanel.html("");
			bookingPanel.html($('<div class="bsp-animate"></div>'));
			bookingPanel.addClass('hidden');
			$('.sia-breadcrumb').addClass('hidden');
			p.bspIsDisplayed = false;
			changeCTAButton("SAVE AND EXIT");
			return;
		}
		total = total.toFixed(2);
		if ($(".sub-total-price").length) {
			$(".sub-total-price").find(".total-price").text(total);
			var current = parseFloat(total);
			var paid = parseFloat($(".previous-price").find(".total-price").text());
			var newTotal = current - paid;
			total = newTotal.toFixed(2);
		}
		$(".grand-total__price").text(currency + " " + total);
		$(".total-cost").find(".unit").text(currency + " " + total.split(".")[0]);
		$(".total-cost").find(".unit").append($('<span class="unit-small">.'+(total.split(".")[1] || "00")+'</span>'))
	};

	var removeBSP = function(ids, paxId) {
		var segment = ids.segmentId + "" + ids.sectorId;
		var bspMealWrapper = $("[data-bsp-mealid='"+paxId+"']");
		bspMealWrapper.find("[data-bsp-segment='"+segment+"']").remove();
		if (!bspMealWrapper.find("[data-bsp-segment]").length) {
			bspMealWrapper.remove();
		}
		calculatePaid();
		calculateTotal();
	};

	var displayBSP = function(cb) {
		SIA.bookingSummnaryBubblePrice(cb);
		$(".sia-breadcrumb").removeClass("hidden");
	};

	var changeCTAButton = function(inputText) {
		$(".btn-cta-submit").val(inputText.toUpperCase());
	};

	var paintSubTotal = function(currentPax) {
		var totalPrice = parseFloat(currentPax.mealPrice);
		var tpl = '<ul class="bsp-flights-cost__details bah-subtotal">\
			<li class="sub-total sub-total-price">\
				<span>Sub-total</span>\
				<span class="total-price">0.00</span>\
			</li>\
			<li class="sub-total previous-price">\
				<span>Previously paid</span>\
				<span class="total-price">'+totalPrice.toFixed(2)+'</span>\
			</li>\
		</ul>';

		$(".bsp-grand-total").prepend($(tpl));
	};

	var setSubTotal = function(total) {
		$(".bah-subtotal").find(".previous-price").find(".total-price").text(total.toFixed(2));
	};

	return {
		addBSPTpl: addBSPTpl,
		removeBSP: removeBSP,
		displayBSP: displayBSP,
		paintSubTotal: paintSubTotal
	};
}();

// check if underscore is loaded
var waitForUnderscore = function () {
	setTimeout(function () {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.MealsSelection.init();
		}
	}, 100);
};

$(function () {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.MealsSelection.init();
});
