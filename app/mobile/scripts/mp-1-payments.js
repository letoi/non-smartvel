SIA.Mp1Payments = function() {

    var itineraryBookingInfoTpl = `<div class="booking-info-group-1">
        <div class="booking-info-content-1">
            <div class="for-bottom_border">
                <h4 class="title-5-blue"><%= title.code %> • <%= title.city %></h4>
                <span class="flights_type"><%= cabinClass %></span>
            </div>
            <div class="flight-station-inner-1">
                <span class="stop-time-1"><%= stops ? stops + " stop • " : "" %><%= totalTravelTime %></span>
                <% _.each(flightSegments, function(segment) { %>
                <div class="flight-station-info_1">
                    <div class="flight-station_1">
                        <span class="hour"><%= segment.departure.airportCode %> <%= segment.departure.time %></span>
                        <span class="country-name">
                            <strong><%= segment.departure.cityName %></strong>
                        </span>
                        <span class="date"><%= segment.departure.date %></span>
                        <span class="return-flights-place"><%= segment.departure.airportName %> Terminal <%= segment.departure.terminal %></span>
                    </div>
                    <div class="station-stop_1">
                        <span class="station-stop-detail">
                            <em class="ico-airplane-2"></em>
                            <hr>
                            <span class="time"><%= segment.flyingTime %></span>
                        </span>
                    </div>
                    <div class="return-flights_1">
                        <span class="hour"><%= segment.arrival.airportCode %> <%= segment.arrival.time %></span>
                        <span class="country-name">
                            <strong><%= segment.arrival.cityName %></strong>
                        </span>
                        <span class="date"><%= segment.arrival.date %></span>
                        <span class="return-flights-place"><%= segment.arrival.airportName %> Terminal <%= segment.arrival.terminal %></span>
                    </div>
                    <div class="airline-info_1">
                        <div class="inner-info">
                            <span class="airline-detail singapore-logo ">

                                <img src="images/svg/sq.svg" alt="sq Logo" longdesc="img-desc.html">

                                <span class="name">
                                    <strong><%= segment.carrierName %> •</strong>
                                    <span><%= segment.carrierCode %> <%= segment.flightNumber %></span>
                                </span>
                            </span>
                            <span class="name-plane"><%= segment.airCraftType %></span>
                            <span class="economy-1"><%= segment.cabinClass %></span>
                        </div>
                    </div>
                </div>
                <% }) %>
            </div>
        </div>
    </div>`;

    var itineraryBookingFlightTpl = `<div class="flights-info-group">\
        <div class="flights-info-content">\
            <div class="for-bottom_border">\
                <h4 class="title-5-blue">Flights</h4>\
            </div>\
            <div class="flight_details">\
                <div class="flight-station-content_1">\
                    <div class="fare-title_1">\
                        <span class="fare-title">\
                            <strong>FARE</strong>\
                        </span>\
                        <span class="aud-title">\
                            <strong><%= currency %></strong>\
                        </span>\
                    </div>\
                    <% _.each(paxDetails, function(pax) { %>
                    <div class="fare-name-amount">\
                        <span class="fare-name">• <%= pax.paxName %></span>\
                        <span class="aud-amount"><%= numberWithCommas(pax.fareDetails.fareAmount) %></span>\
                    </div>\
                    <% }) %>
                    <div class="taxes-amount_1">\
                        <span class="taxes_1">Airport / Government taxes</span>\
                        <span class="taxes-amount"><%= numberWithCommas(taxTotal) %></span>\
                    </div>\
                    <div class="taxes-amount_1">\
                        <span class="taxes_1">Carrier Surcharges</span>\
                        <span class="taxes-amount"><%= numberWithCommas(surchargeTotal) %></span>\
                    </div>\
                    <div class="subtot-amount_1">\
                        <span class="subtotal_1">\
                            <strong>Sub-total</strong>\
                        </span>\
                        <span class="subtotal-amount">\
                            <strong><%= numberWithCommas(calculateTable(paxDetails)) %></strong>\
                        </span>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>`;

    var passengerAddOnsTpl = `<div class="passenger1">
        <div class="person">
            <span><%= paxName %></span>
            <div class="hidden">
                <span class="elite">ELITE SILVER</span>
            </div>
        </div>
        <div class="flight-title-amount">
            <span>
                <strong><%= currency %></strong>
            </span>
        </div>
        <div class="flight-details1">
            <% _.each(flightLegs, function(leg) { %>
            <div class="flight-details--1">
                <span class="flight-country">
                    <span class="country1"><%= leg.originCode %></span> -
                    <span class="country2"><%= leg.destinationCode %></span>
                </span>
                <span class="flight-deal"><%= getFlightLabel(leg.flightDetails) %></span>
                <span class="flight-amount"><%= numberWithCommas(leg.totalAmount) %>
                    <a data-trigger-popup=".popup--remove-deal" data-origin="<%= leg.originCode %>" data-origin-name="<%= leg.originName %>" data-destination="<%= leg.destinationCode %>" data-destination-name="<%= leg.destinationName %>" data-pax-id="<%= id %>"  data-flight-details='<%= JSON.stringify(leg.flightDetails) %>' class="cancel-deal" href="#" <%= leg.totalAmount ? '' : 'style="visibility: hidden"' %>>
                        <em class="ico-delete"></em>
                    </a>
                </span>
            </div>
            <% }) %>
            <div class="flights-subtotal">
                <span class="subtotal1">
                    <strong>Sub-total</strong>
                </span>
                <span class="flights-sub-amount">
                    <strong><%= numberWithCommas(subTotal) %></strong>
                </span>
            </div>
        </div>
    </div>`;

    var passengerFlightsTpl = `<div class="flights-info-content">
        <div class="info-content-1">
            <div class="group-title">
                <h5><%= paxName %></h5>
            </div>
            <% _.each(flightLegs, function(leg, idx) { %>
            <div class="departure-flight">
                <div class="depart-title">
                    <span><%= idx + 1 %>. </span>
                    <span><%= leg.title %></span>
                </div>

                <div class="depart-content-1">
                    <div class="depart-seat">
                        <div class="seat1">
                            <span>
                                <em class="ico-change-seat"></em>Seat</span>
                        </div>
                        <div class="seat-infos">
                            <% _.each(leg.seats, function(seat) { %>
                            <div class="seat-info1">
                                <span class="seat-detail">
                                    <span class="seat-country1"><%= seat.titleFrom %></span> -
                                    <span class="seat-country2"><%= seat.titleTo %></span> •
                                    <span class="flight-tracker"><%= seat.titleFlightTracker %></span>
                                </span>
                                <span class="seat-detail1">
                                    <% if (seat.seatNumber) { %>
                                    <strong class="seat-number">
                                        <%= seat.seatNumber %>
                                    </strong>
                                    <% } %>
                                    <%= seat.seatType ? (' - ' + seat.seatType) : 'Not selected' %>
                                    <% if (seat.seatNumber && seat.isPackService) { %>
                                    <a href="#">
                                        <img data-tooltip="true" data-type="2" data-content="Use your KrisRyer miles to pay for all or a part of your airfare (including taxes and surcharges). additional baggage and seats. KrisRyer miles, Elite miles and PPS Value will be earned in the proportion to the amount (excluding taxes) that you have paid with your credit/debit card." src="images/svg/icon-bundle.svg">
                                    </a>
                                    <% } %>
                                    <span>
                            </div>
                            <% }) %>
                        </div>
                    </div>
                    <div class="depart-baggage">
                        <div class="baggage1">
                            <span class="baggage-title">
                                <em class="ico-business-1"></em>Baggage</span>
                        </div>
                        <div class="wrapper-bag">
                            <div class="baggage-info1">
                                <div>
                                    <span class="baggage-detail">
                                        <span class="baggage-country1"><%= leg.originCode %></span> -
                                        <span class="baggage-country2"><%= leg.destinationCode %></span>
                                    </span>
                                </div>
                                <span class="baggage1">Included:
                                    <% if (leg.baggage.freeBaggage) { %>
                                    <span class="baggage-qty">
                                        <strong><%= leg.baggage.freeBaggage %>kg</strong>
                                    </span>
                                    <% } %>
                                    <a href="#" data-trigger-popup=".popup--terms-and-conditions" class="kris-members">
                                        <em class="ico-point-r"></em> View extra baggage allowance for KrisFlyer and PPS Club members</a>
                                </span>
                                <% if (leg.baggage.additionalBaggage) { %>
                                <span class="baggage2">Additional:
                                    <span class="baggage-qty"><%= leg.baggage.additionalBaggage %>kg</span>
                                    <a href="#">
                                        <img data-tooltip="true" data-type="2" data-content="Use your KrisRyer miles to pay for all or a part of your airfare (including taxes and surcharges). additional baggage and seats. KrisRyer miles, Elite miles and PPS Value will be earned in the proportion to the amount (excluding taxes) that you have paid with your credit/debit card." src="images/svg/icon-bundle.svg">
                                    </a>
                                </span>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% }) %>
        </div>
    </div>`;

    var tripTravelInsuranceTpl = `<div class="trip-related_1">
        <div class="trip-related-title">
            <h5><%= name %></h5>
        </div>

        <div class="trip-content">
            <div class="trip-details1">
                <span class="trip-details--1">
                    <span class="trip-label">Person covered: </span>
                    <span class="trip-text"><%= coveredPersons %></span>
                </span>
                <span class="trip-details--1">
                    <span class="trip-label">Travel location: </span>
                    <span class="trip-text"><%= travelLocation %></span>
                </span>
                <span class="trip-details--1">
                    <span class="trip-label">Date range: </span>
                    <span class="trip-text"><%= description %></span>
                </span>
            </div>
            <div class="AIG-logo">
                <img src="images/aig-logo.png" alt="Travel insurance" longdesc="img-desc.html">
            </div>
        </div>
    </div>`;

    var tripHotelTpl = `<div class="trip-related_2">
        <div class="hotel-header">
            <div class="hotel-title">
                <h5>Hotel</h5>
            </div>
        </div>
        <div class="agoda-logo">
            <img src="images/addon-agoda-logo.png" alt="mb-payment-v2" longdesc="img-desc.html">
        </div>
        <div class="hotel-info-1">
            <div class="hotel-pic-1">
                <img src="<%= image %>" alt="hotel2">
            </div>
            <div class="hotel-room-details">
                <div class="hotel-room-details--1">
                    <div class="title_1">
                        <span><%= name %></span>
                        <span><%= address %></span>
                    </div>
                    <div class="hotel_1">
                        <span class="hotel-details check_in">
                            <span class="room-label">Check-in date:</span>
                            <span class="room-text"><%= checkInDate %></span>
                        </span>
                        <span class="hotel-details check_out">
                            <span class="room-label">Check-out date:</span>
                            <span class="room-text"><%= checkOutDate %></span>
                        </span>
                        <span class="hotel-details guests">
                            <span class="room-label">Guests: </span>
                            <span class="room-text"><%= guest %></span>
                        </span>
                        <span class="hotel-details room-types">
                            <span class="room-label">Room types:</span>
                            <span class="room-text"><%= roomType %></span>
                        </span>
                        <span class="hotel-details number-of-nights">
                            <span class="room-label">No. of nights:</span>
                            <span class="room-text"><%= numberOfNights %></span>
                        </span>
                        <span class="hotel-details breakfast <%= isBreakfastIncluded ? "" : "hidden" %>">
                            <span class="room-label">Breakfast:</span>
                            <span class="room-text"> Included for all rooms</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

    var tripCarTpl = `<div class="car-related_2">
        <div class="car-header">
            <div class="car-title">
                <h5>Car rental</h5>
            </div>
        </div>
        <div class="rental-logo">
            <img src="images/addon-rentalcars-logo.png" alt="mb-payment-v2" longdesc="img-desc.html">
        </div>
        <div class="car-info-1">
            <div class="car-pic-1">
                <img src="<%= image %>" alt="car">
            </div>
            <div class="car-details">
                <div class="car-title">
                    <span class="car-title_1">
                        <strong><%= name %></strong>
                    </span>
                </div>
                <div class="car-pickup">
                    <span class="car-details--1 pick-up">
                        <span class="car-label">Pick up: </span>
                        <span class="car-text"><%= pickupDate %></span>
                    </span>
                    <span class="car-details--1 pick-up-location">
                        <span class="car-label">Location: </span>
                        <span class="car-text"><%= pickupLocation %></span>
                    </span>
                    <span class="car-details--1 drop-off">
                        <span class="car-label">Drop off: </span>
                        <span class="car-text"><%= dropoffDate %></span>
                    </span>
                    <span class="car-details--1 drop-off-location">
                        <span class="car-label">Location: </span>
                        <span class="car-text"><%= dropoffLocation %></span>
                    </span>
                </div>

            </div>
        </div>
    </div>`;

    var tripAddOnsCostTpl = `<div class="add-ons_1">
        <div class="add-ons-content">
            <span class="add-ons-title">
                <strong>ADD-ONS</strong>
                <span class="aud-title-1">
                    <strong><%= currency %></strong>
                </span>
            </span>
            <% _.each(commonAddons, function(addOns) { %>
            <span class="add-ons-details-1">
                <span class="addons-travel"><%= addOns.type %></span>
                <a href="#">Change</a>
                <span class="addons-travel-amount"><%= numberWithCommas(addOns.amount) %>
                    <em data-trigger-popup=".popup--remove-trip-addons" data-trip-type="<%= addOns.type %>" class="ico-delete cancel-trip-add-ons"></em>
                </span>
            </span>
            <% }) %>
            <span class="addons-subtotal">
                <strong>Sub-total</strong>
                <span class="addons-sub-amount">
                    <strong><%= numberWithCommas(subTotal) %></strong>
                </span>
            </span>
        </div>
    </div>`;

    var costBreakDownTpl = `<div class="person-total-details-1">
        <div class="person-name-1">
            <span><%= paxName %></span>
            <span class="elite hidden">ELITE SILVER</span>
        </div>
        <div class="total-titles">
            <div class="total-services-title">
                <span>Services</span>
            </div>
            <div class="total-amount-title">
                <span>Amount (<%= currency %>)</span>
            </div>
        </div>
        <div class="air-fare-details">
            <div class="air-fare-list">
                <span class="air-fare-title">
                    <strong>Air Fare</strong>
                    <span class="air-fare-amount">
                        <strong><%= numberWithCommas(fareAmount) %></strong>
                    </span>
                </span>
                <% if (taxes) { %>
                <span class="air-fare-title">
                    <strong>Airport / Government taxes:</strong>
                </span>
                    <% _.each(taxes, function(tax) { %>
                    <span class="services-tax"><%= tax.description %> (<%= tax.code %>)
                        <span class="air-fare-amount"><%= numberWithCommas(tax.amount) %></span>
                    </span>
                    <% }) %>
                <% } %>
                <% if (surcharge) { %>
                    <span class="air-fare-title">
                        <strong>Carrier Surcharges</strong>
                    </span>
                    <% _.each(surcharge, function(surchargeItem) { %>
                    <span class="services-tax"><%= surchargeItem.description %> (<%= surchargeItem.code %>)
                        <span class="air-fare-amount"><%= numberWithCommas(surchargeItem.amount) %></span>
                    </span>
                    <% }) %>
                <% } %>
            </div>
            <div class="deals-info">
                <span class="deals">
                    <strong>Deals</strong>
                </span>
                <% _.each(flightLegs, function(leg) { %>
                <span class="deals-1">
                    <span class="deals-country">
                        <span><%= leg.originCode %></span>-
                        <span><%= leg.destinationCode %></span>
                    </span>
                    <span class="seats-1"><%= getFlightLabel(leg.flightDetails) %></span>
                    <span class="air-fare-amount"><%= numberWithCommas(calculateAddOns(leg.flightDetails)) %></span>
                </span>
                <% }) %>
            </div>
            <span class="sub-total">
                <strong>Sub-total</strong>
                <span class="air-fare-amount">
                    <strong><%= numberWithCommas(subTotal) %></strong>
                </span>
            </span>
        </div>
    </div>`;

    var tripAddOnsBreakDown = `<div class="trip-addons">
        <div class="trip-addons-title">
            <div class="trip-addons-list">
                <span class="trip-add-title">
                    <strong>Trip add-ons</strong>
                </span>
                <% _.each(commonAddons, function(addOn) { %>
                <span class="trip-add-1"><%= addOn.name %>
                    <span class="trip-amount"><%= numberWithCommas(addOn.amount) %></span>
                </span>
                <% }) %>
            </div>
            <span class="sub-total">
                <strong>Sub-total</strong>
                <span class="trip-amount">
                    <strong><%= numberWithCommas(subTotal) %></strong>
                </span>
            </span>
        </div>
    </div>`;

    var removeDealSeatTpl = `<div class="seat-baggage--1">
        <span class="seat-icon-1">
            <em class="ico-change-seat"></em>
        </span>
        <span clas="seat-info-details">
            <span class="seat-info"><%= bundleSeat %></span>
            <% _.each(flightDetails, function(details) { %>
            <span class="flight-info--1">
                <span class="country--1"><%= details.from %></span> -
                <span class="country--2"><%= details.to %></span>
                <span class="seat--info">(<%= details.flightNo.replace(details.operatingAirlineCode, details.operatingAirlineCode + " ") %>)</span>
            </span>
            <% }) %>
        </span>
    </div>`;

    var removeDealBaggageTpl = `<div class="seat-baggage--1">
        <span class="seat-icon-1">
            <em class="ico-business-1"></em>
        </span>
        <span clas="seat-info-details">
            <span class="seat-info">Additional Baggage</span>
            <span class="flight-info--1">
                <span class="baggage--qty"><%= additionalBaggage %> kg</span>
            </span>
            <span class="flight-info--1">
                <span class="country--1"><%= origin %></span> -
                <span class="country--2"><%= destination %></span>
            </span>
        </span>
    </div>`;

    var creditCardOptionsTpl = `<option
        class="<%= isExpired ? 'has-disabled disabled-card' : '' %>"
        <%= isExpired ? 'disabled="true"' : '' %>
        <%= isDefault ? 'selected' : '' %>
        value="<%= image %>.png" 
        data-id="customSelect-4-option-<%= idx %>"
        data-month="<%= month %>" 
        data-year="<%= year %>" 
        data-name="<%= name %>"
        data-hidden-card="<%= cardHiddenNumber %>" 
        data-card="<%= cardNumber %>">
        <%= cardHiddenNumber %> <%= cardNumber %> <%= (isDefault) ? "(Default)" : "" %><%= (isExpired) ? "(Expired)" : "" %>
    </option>`;

    var closeAccordionButtonTpl = `<div class="close-btn">
        <input type="submit" name="submit-mp-payment"
        value="close" class="close-btn--1 close-accordion-button">
        <i class="ico-point-u"></i>
    </div>`;

    var templateData = {
        flights: [],
        currency: "",
        paxDetails: [],
        taxTotal: 0,
        commonAddons: [],
        costPayableByMiles: 0,
        costPayableByCash: 0,
        creditCards: []
    };

    var init = function() {
        onMount();
        onCreditCardMount();
        customValidate();
        enableCustomSlider();
        strickyScroll(".sticky-block");
        addAddressListener();
        submitButtonState();

        $(".close-popup").off().on("click", function() {
            $(this).closest(".popup").Popup("hide");
        });
    };

    var onMount = function() {
        $.get("ajax/JSONS/MP/Review_and_Payment.json", function(response) {
            var data = response.bookingSummary;
            templateData.currency = data.currency;
            templateData.flights = _.map(data.flight, function(flight) {
                var flightSegments = flight.flightSegments;
                var startSegment = flightSegments[0];
                var endSegment = flightSegments.length > 1 ? flightSegments[flightSegments.length - 1] : false;
                flight.title = {
                    code: `${startSegment.carrierCode} ${startSegment.flightNumber}`,
                    city: `${startSegment.departure.cityName}`
                };
                if (endSegment) {
                    flight.title.code += ` & ${endSegment.carrierCode} ${endSegment.flightNumber}`,
                    flight.title.city += ` to ${endSegment.arrival.cityName}`
                }
                flight.stops = flightSegments.length - 1;
                flight.cabinClass = startSegment.cabinClassDesc;
                return flight;
            });
            templateData.paxDetails = data.paxDetails;
            templateData.taxTotal = data.taxTotal;
            templateData.commonAddons = data.commonAddons;
            templateData.costPayableByMiles = data.costPayableByMiles;
            templateData.costPayableByCash = data.costPayableByCash;
            renderBookingInfoGroup();
            renderBookingFlightGroup();

            renderPassengerAddOnsGroup();
            renderPassengerFlightsGroup();

            renderTravelInsuranceGroup();
            renderHotelGroup();
            renderCarGroup();
            renderTripAddOnsCostGroup();

            renderCostBreakDownGroup();
            SIA.AccordionJS.init();
            renderAccordionCloseButton();
            hideOnMobile();
            customSelectOnMobile();
        });
    };

    var customSelectOnMobile = function() {
        $("[data-customselect]").each(function() {
            var _self = $(this);
            _self.find("select").removeAttr("style");
            _self.find("select").css("z-index", "999");
        });
    };

    var renderAccordionCloseButton = function() {
        var buttonParents = $(".mobile-close-accordion");
        buttonParents.each(function() {
            var _self = $(this);
            _self.append($(_.template(closeAccordionButtonTpl, {})));
        });
        addAccordionMobileClose();
    };

    var addAccordionMobileClose = function() {
        $(".close-accordion-button").off().on("click", function() {
            var accordionTrigger = $(this).closest("[data-accordion-state]").find("[data-accordion-trigger]");
            accordionTrigger.trigger("click");
        });
    };

    var hideOnMobile = function() {
        $(".hide-on-mobile").each(function() {
            $(this).addClass("hidden");
        });
    };

    var onCreditCardMount = function() {
        $.get("ajax/kf-wallet.json", function(response) {
            templateData.creditCards = response.digitalWalletVO.digitalWalletCardVO;
            renderCreditCardGroup();
        });
    };

    var enableCustomSlider = function() {
        setTimeout(function() {
            var slider = $("#slider-range");
            if (slider.find(".ui-slider-handle").length) {
                customSlider(slider);
            } else {
                enableCustomSlider();
            }
        }, 500);
    };

    var customSlider = function(slider) {
        slider.wrap(`<div class="ui-slider-back"></div>`);
        slider.find(".ui-slider-handle").append(`<div class="ui-slider-icon"></div>`);
    };

    var customValidate = function() {
        var checkValid = function(el) {
            $(el).valid();
        };

        $("#payments-promo-code").validate({
            onkeyup: checkValid,
            onfocusout: checkValid,
            errorPlacement: function(error, el) {
                var errorPlace = el.closest(".form-group");
                errorPlace.find(".text-error").remove();
                errorPlace.append($(`<p class="text-error"><span>${error.text()}</span></p>`));
                errorPlace.addClass("error");
            },
            success: function(label, el) {
                var errorPlace = $(el).closest(".form-group");
                errorPlace.find(".text-error").remove();
                errorPlace.removeClass("error");
            }
        });

        $("#payments-form").validate({
            onfocusout: checkValid,
            errorPlacement: function(error, el) {
                var errorPlace = el.closest(".grid-inner");
                if (!errorPlace.length) {
                    errorPlace = el.closest(".grid-col");
                }
                errorPlace.find(".text-error").remove();
                errorPlace.append($(`<p class="text-error"><span>${error.text()}</span></p>`));
                errorPlace.addClass("error");
            },
            success: function(label, el) {
                var errorPlace = $(el).closest(".grid-inner");
                if (!errorPlace.length) {
                    errorPlace = $(el).closest(".grid-col");
                }
                errorPlace.find(".text-error").remove();
                errorPlace.removeClass("error");
            }
        });
    };

    var renderBookingInfoGroup = function() {
        var tpls = _.map(templateData.flights, function(flight) {
            return $(_.template(itineraryBookingInfoTpl, {
                title: flight.title,
                stops: flight.stops,
                cabinClass: flight.cabinClass,
                totalTravelTime: flight.totalTravelTime,
                flightSegments: flight.flightSegments
            }));
        });
        $(".accordion-items.itinerary").find(".booking-info-group-tpl").append(tpls);
    };

    var renderBookingFlightGroup = function() {
        var surchargeTotal = _.reduce(templateData.paxDetails, function(currentValue, pax) {
            return currentValue + _.reduce(pax.fareDetails.surcharge, function(_currentValue, surcharge) {
                return _currentValue + surcharge.amount;
            },0);
        }, 0);
        var taxTotal = _.reduce(templateData.paxDetails, function(currentValue, pax) {
            return currentValue + _.reduce(pax.fareDetails.taxes, function(_currentValue, tax) {
                return _currentValue + tax.amount;
            },0);
        }, 0);
        var total = calculateTable(templateData.paxDetails);
        var tpl = $(_.template(itineraryBookingFlightTpl, {
            currency: templateData.currency,
            paxDetails: templateData.paxDetails,
            taxTotal: taxTotal,
            surchargeTotal: surchargeTotal,
            numberWithCommas: numberWithCommas,
            calculateTable: calculateTable
        }));
        $(".accordion-items.itinerary").find(".flights-info-group-tpl").append(tpl);
        renderSubCost(".accordion-items.itinerary", total);
    };

    var renderSubCost = function(className, textDisplay) {
        var trigger = $(className).find("[data-accordion-trigger]");
        trigger.find(".sub-currency").text(templateData.currency);
        trigger.find(".sub-price").text(numberWithCommas(textDisplay));
    };

    var renderPassengerAddOnsGroup = function() {
        var legSubTotal = 0;
        $(".accordion-items.passenger-flight").find(".flight-title-amount").find("span").text(templateData.currency);
        var tpls = _.map(templateData.paxDetails, function(pax, idx) {
            var flightLegs = checkFlightSegmentIndex(pax.detailsPerFlight);
            var subTotal = calculateAddOnsSubTotal(flightLegs);
            legSubTotal += subTotal;
            return $(_.template(passengerAddOnsTpl, {
                id: pax.id,
                paxName: pax.paxName,
                currency: templateData.currency,
                flightLegs: flightLegs,
                getFlightLabel: getFlightLabel,
                numberWithCommas: numberWithCommas,
                subTotal: subTotal
            }));
        });
        $(".accordion-items.passenger-flight").find(".passenger-add-ons-tpl").append(tpls);
        addCancelDealListener();
        renderSubCost(".accordion-items.passenger-flight", legSubTotal);
    };

    var renderPassengerFlightsGroup = function() {
        var tpls = _.map(templateData.paxDetails, function(pax) {
            var flightLegs = checkFlightSegmentIndex(pax.detailsPerFlight);
            return $(_.template(passengerFlightsTpl, {
                paxName: pax.paxName,
                flightLegs: flightLegs
            }));
        });

        $(".accordion-items.passenger-flight").find(".passenger-flights-tpl").append(tpls);
    };

    var renderTravelInsuranceGroup = function() {
        var find = _.findWhere(templateData.commonAddons, {type: "Travel Insurance"});
        if (!find) {
            return;
        }
        var tpl = $(_.template(tripTravelInsuranceTpl, {
            coveredPersons: _.map(templateData.paxDetails, function(pax) {
                return pax.paxName;
            }).join(", "),
            name: find.name,
            description: find.description,
            travelLocation: find.travelLocation
        }));
        $(".accordion-items.trip-add-ons").find(".trip-add-ons-tpl").append(tpl);
    };

    var renderHotelGroup = function() {
        var find = _.findWhere(templateData.commonAddons, {type: "Hotel"});
        if (!find) {
            return;
        }
        var hotelFormatData = formatHotelData(find);
        var tpl = $(_.template(tripHotelTpl, {
            name: find.name,
            description: find.description,
            address: find.address,
            image: find.image,
            checkInDate: find.checkInDate,
            checkOutDate: find.checkOutDate,
            guest: find.guest,
            numberOfNights: hotelFormatData.numberOfNights,
            isBreakfastIncluded: hotelFormatData.isBreakfastIncluded,
            roomType: hotelFormatData.roomType
        }));
        $(".accordion-items.trip-add-ons").find(".trip-add-ons-tpl").append(tpl);
    };

    var renderCarGroup = function() {
        var find = _.findWhere(templateData.commonAddons, {type: "Car rental"});
        if (!find) {
            return;
        }
        var tpl = $(_.template(tripCarTpl, {
            name: find.name,
            image: find.image,
            pickupLocation: find.pickupLocation,
            pickupDate: find.pickupDate,
            dropoffLocation: find.dropoffLocation,
            dropoffDate: find.dropoffDate
        }));
        $(".accordion-items.trip-add-ons").find(".trip-add-ons-tpl").append(tpl);
    };

    var renderTripAddOnsCostGroup = function() {
        var total = _.reduce(templateData.commonAddons, function(currentValue, addOn) {
            return currentValue + addOn.amount;
        }, 0)
        var tpl = $(_.template(tripAddOnsCostTpl, {
            currency: templateData.currency,
            commonAddons: templateData.commonAddons,
            numberWithCommas: numberWithCommas,
            subTotal: total
        }));

        $(".accordion-items.trip-add-ons").find(".trip-add-ons-tpl").append(tpl);
        addCancelTripAddOnsListener();
        renderSubCost(".accordion-items.trip-add-ons", total);
    };

    var renderCostBreakDownGroup = function() {
        var accordionEl = $(".accordion-items.cost-break-down");
        accordionEl.find(".total-amount-currency").text(`(${templateData.currency})`);
        var costBreakDownTotal = 0;
        var tpls = _.map(templateData.paxDetails, function(pax) {
            var flightLegs = checkFlightSegmentIndex(pax.detailsPerFlight);
            var subTotal = calculateBreakDownSubTotal(pax, flightLegs);
            costBreakDownTotal += subTotal;
            return $(_.template(costBreakDownTpl, {
                paxName: pax.paxName,
                currency: templateData.currency,
                fareAmount: pax.fareDetails.fareAmount,
                taxes: pax.fareDetails.taxes,
                surcharge: pax.fareDetails.surcharge,
                numberWithCommas: numberWithCommas,
                flightLegs: flightLegs,
                getFlightLabel: getFlightLabel,
                calculateAddOns: calculateAddOns,
                numberWithCommas: numberWithCommas,
                subTotal: subTotal
            }));
        });
        var tripAddOnsSubtotal = _.reduce(templateData.commonAddons, function(currentValue, addOn) {
            return currentValue + addOn.amount;
        }, 0);
        costBreakDownTotal += tripAddOnsSubtotal;
        tpls.push(
            $(_.template(tripAddOnsBreakDown, {
                commonAddons: templateData.commonAddons,
                subTotal: tripAddOnsSubtotal,
                numberWithCommas: numberWithCommas
            }))
        );
        accordionEl.find(".cost-break-down-tpl").append(tpls);
        accordionEl.find(".grand-total-currency").each(function() {
            $(this).text(templateData.currency);
        });
        renderSubCost(".accordion-items.cost-break-down", costBreakDownTotal);
        accordionEl.find(".grand-total-amount").text(numberWithCommas(costBreakDownTotal));
        accordionEl.find(".payable-krisflyer").text(numberWithCommas(templateData.costPayableByMiles));
        accordionEl.find(".payable-cash").text(numberWithCommas(templateData.costPayableByCash));

        $(".payment-method-content").find(".total-price-paid").find(".unit").each(function() {
            $(this).text(`${templateData.currency} ${numberWithCommas(costBreakDownTotal)}`);
        });
    };

    var addCancelDealListener = function() {
        $(".accordion-items.passenger-flight")
            .find(".cancel-deal")
            .off().on("click", function(evt) {
                var _self = $(this),
                    paxId = _self.data("pax-id"),
                    pax = _.findWhere(templateData.paxDetails, {id: paxId}),
                    origin = _self.data("origin"),
                    originName = _self.data("origin-name"),
                    destination = _self.data("destination"),
                    destinationName = _self.data("destination-name"),
                    flightDetails = _self.data("flight-details"),
                    bundleSeat = _.findWhere(flightDetails[0].addonPerPax, {type: "PACK"});

                renderRemoveDealSeat(bundleSeat, flightDetails);
                renderRemoveDealGroup(origin, destination, flightDetails);
                renderRemoveDealModal(pax, bundleSeat.seatType, originName, destinationName);
                $("#remove-deal").off().on("click", function() {
                    var newFlightDetails = _.map(flightDetails, function(details) {
                        details.addonPerPax = _.map(details.addonPerPax, function(addOn) {
                            addOn.isPackService = false;
                            addOn.seatType = "";
                            addOn.seatNumber = "";
                            delete addOn.additionalBaggageWeight;
                            return addOn;
                        });
                        return details;
                    });

                    pax.detailsPerFlight = _.map(pax.detailsPerFlight, function(details) {
                        var newFlightDetail = _.findWhere(newFlightDetails, {id: details.id});
                        if (newFlightDetail) {
                            details.addonPerPax = newFlightDetail.addonPerPax;
                        }
                        return details;
                    });
                    for (var i = 0; i < templateData.paxDetails; i ++) {
                        if (templateData.paxDetails[i].id === pax.id) {
                            templateData.paxDetails[i] = pax;
                            break;
                        }
                    }

                    cleanTpl([".passenger-add-ons-tpl", ".passenger-flights-tpl", ".cost-break-down-tpl"]);
                    renderPassengerAddOnsGroup();
                    renderPassengerFlightsGroup();

                    renderCostBreakDownGroup();
                    reInitializeTooltips();

                    $(this).closest(".popup").Popup("hide");
                });
            });
    };

    var renderRemoveDealModal = function(pax, seatType, originName, destinationName) {
        $(".popup--remove-deal").find(".deal-name").text(pax.paxName);
        $(".popup--remove-deal").find(".deal-depart").text(originName);
        $(".popup--remove-deal").find(".deal-arrival").text(destinationName);
        $(".popup--remove-deal").find(".deal-note").text(`Your ${seatType} from ${originName} to ${destinationName} will be deselected if you remove this deal.`);
    };

    var renderRemoveDealSeat = function(bundleSeat, flightDetails) {
        var tpl = $(_.template(removeDealSeatTpl, {
            bundleSeat: bundleSeat.seatType,
            flightDetails: flightDetails
        }));
        cleanTpl([".remove-deal-seat-tpl"]);
        $(".popup--remove-deal").find(".remove-deal-seat-tpl").append(tpl);
    };

    var renderRemoveDealGroup = function(origin, destination, flightDetails) {
        var bundleSeat = _.findWhere(flightDetails[0].addonPerPax, {type: "PACK"});
        var tpl = $(_.template(removeDealBaggageTpl, {
            origin: origin,
            destination: destination,
            additionalBaggage: bundleSeat.additionalBaggageWeight
        }));
        cleanTpl([".remove-deal-baggage-tpl"]);
        $(".popup--remove-deal").find(".remove-deal-baggage-tpl").append(tpl);
    };

    var addCancelTripAddOnsListener = function() {
        $(".accordion-items.trip-add-ons")
            .find(".cancel-trip-add-ons")
            .off().on("click", function() {
                var _self = $(this),
                    tripType = _self.data("trip-type");
                
                $(".popup--remove-trip-addons").find(".deal-type").text(tripType);
                $("#remove-trip-addons").off().on("click", function() {
                    templateData.commonAddons = _.filter(templateData.commonAddons, function(addOn) {
                        if (addOn.type.toLowerCase() !== tripType.toLowerCase()) {
                            return addOn;   
                        }
                    });
    
                    cleanTpl([".cost-break-down-tpl", ".trip-add-ons-tpl"]);
                    renderTravelInsuranceGroup();
                    renderHotelGroup();
                    renderCarGroup();
                    renderTripAddOnsCostGroup();
    
                    renderCostBreakDownGroup();

                    $(this).closest(".popup").Popup("hide");
                });
            });
    };

    var addAddressListener = function() {
        $(".add-address").on("click", function() {
            var parent = $(this).closest(".address-name");
            parent.find(".grid-inner").each(function() {
                var _self = $(this);
                _self.addClass("grid-inner-addline1");
                _self.removeClass("grid-inner");
            });
        });
    };

    var renderCreditCardGroup = function() {
        var tpls = _.map(templateData.creditCards, function(card, idx) {
            var cardDate = card.expiryDate.split("-");
            var cardNumber = card.cardTokenNumber.match(/[0-9]+/)[0];
            var cardHiddenNumber = card.cardTokenNumber.match(/X+/)[0];
            cardHiddenNumber = cardHiddenNumber.replace(/X/g, "•").replace(/(.{4})/g, '$1 ');
            return $(_.template(creditCardOptionsTpl, {
                idx: idx + 1,
                name: card.cardHolderName,
                month: cardDate[0],
                year: cardDate[1],
                image: card.cardType.toLowerCase(),
                cardNumber: cardNumber,
                cardHiddenNumber: cardHiddenNumber,
                isDefault: card.defaultCard == "true",
                isExpired: card.expiryStatus == "expired"
            }));
        });
        $("#save-credit-debit").append(tpls);
        addCreditCardSelectListener();
        attachCustomImage();
    };

    var attachCustomImage = function() {
        setTimeout(function() {
            var parentUlBlock = $('li.select-card-empty').closest('ul');
            if (parentUlBlock.length) {
                addCustomImage(parentUlBlock);
            } else {
                attachCustomImage();
            }
        }, 500);
    };

    var addCustomImage = function(parentUlBlock) {
        var listcustomSelect = parentUlBlock.find('li');
		listcustomSelect.each(function(index) {
			var _this = $(this),
					thisDataImg = _this.data('value'),
					imgTemp = '<img src="images/' + thisDataImg + '" alt="credit card" data-format-card="master">';
			if(index === 0) { return; }
			_this.prepend(imgTemp);
			_this.find('img').css({
				'margin-top': '-5px',
				'margin-right': '10px'
			});
		});
    };

    var addCreditCardSelectListener = function() {
        var customSelect = $("#save-credit-debit").closest("[data-customselect]");
        var optionSelected = $("#save-credit-debit").find("option:selected");
        var creditDebitInfo = $(".credit-debit-info");
        var payWithCard = $(".pay-with_card-text");
        var payWithAnotherCard = $(".block-content-credit-debit");
        
        $("#save-credit-debit").off().on("change", function() {
            customSelect = $(this).closest("[data-customselect]");
            optionSelected = $(this).find("option:selected");
            payWithAnotherCard.addClass("hidden");
            if (optionSelected.data("novalue")) {
                creditDebitInfo.addClass("hidden");
                payWithCard.addClass("hidden");
                customSelect.find(".img-card-selected").addClass("hidden");
                return;
            }
            payWithCard.removeClass("hidden");
            creditDebitInfo.removeClass("hidden");
            customSelect.find(".img-card-selected").removeClass("hidden");
            changeCreditDebitInfo(customSelect, creditDebitInfo, optionSelected);
        });
        $("#pay-another-card-cta").off().on("click", function(evt) {
            evt.preventDefault();
            payWithAnotherCard.removeClass("hidden");
            payWithCard.removeClass("hidden");
            creditDebitInfo.addClass("hidden");
        });
        changeCreditDebitInfo(customSelect, creditDebitInfo, optionSelected);
    };

    var changeCreditDebitInfo = function(customSelect, creditDebitInfo, optionSelected) {
        customSelect.find(".img-card-selected").attr("src", `images/${optionSelected.val()}`);
        creditDebitInfo.find(".credit-debit-name").text(optionSelected.data("name"));
        creditDebitInfo.find(".credit-debit-number").text(optionSelected.data("hidden-card") + " " + optionSelected.data("card"));
        creditDebitInfo.find(".expiry-mon").text(optionSelected.data("month"));
        creditDebitInfo.find(".expiry-year").text(optionSelected.data("year"));
        creditDebitInfo.find("#input-cvv-1")[0].value = "";
    };

    var submitButtonState = function(){
        $("#accept-cb").on("change", function() {
            toggleButtonState($(this).is(":checked"));
        });
        toggleButtonState(false);
    };

    var toggleButtonState = function(state) {
        var buttonEl = $("#submit-mp-payment");
        buttonEl[state ? "removeAttr" : "attr"]("disabled", "true");
        buttonEl[state ? "removeClass" : "addClass"]("disabled");
    };

    // UTILITY
    var numberWithCommas = function(num) {
        return num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var calculateTable = function(paxData, additional) {
        return _.reduce(paxData, function(currentValue, pax) {
            return currentValue + pax.fareDetails.fareAmount + 
                _.reduce((pax.fareDetails.surcharge || []), function(currentValue, surcharge) {
                    return currentValue + surcharge.amount;
                }, 0) +
                _.reduce((pax.fareDetails.taxes || []), function(currentValue, tax) {
                    return currentValue + tax.amount;
                }, 0);
        }, 0) + (additional || 0);
    };

    var checkFlightSegmentIndex = function(paxFlightDetails) {
        var flightLegs = _.map(templateData.flights, function(flight) {
            return {
                title: `${flight.origin} (${flight.originAirportCode}) to ${flight.destination} (${flight.destinationAirportCode})`,
                originCode: flight.originAirportCode,
                destinationCode: flight.destinationAirportCode,
                originName: flight.origin,
                destinationName: flight.destination,
                flightDetails: []
            }
        });

        _.each(paxFlightDetails, function(details) {
            var flightNumber = details.flightNo.replace(new RegExp(details.operatingAirlineCode, "i"), "");
            var index = mapFlightIndex(flightNumber);
            if (index >= 0) {
                flightLegs[index].flightDetails.push(details);
            }
        });

        flightLegs = _.map(flightLegs, function(leg) {
            leg.seats = getSeats(leg.flightDetails);
            leg.baggage = getBaggage(leg.flightDetails);
            leg.totalAmount = calculateAddOns(leg.flightDetails);
            return leg;
        });

        return flightLegs;
    };

    var mapFlightIndex = function(flightNumber) {
        var flightIdx = -1;
        _.every(templateData.flights, function(flight, idx) {
            var find = _.findWhere(flight.flightSegments, {flightNumber: flightNumber});
            if (find) {
                flightIdx = idx;
            }

            return !find;
        });

        return flightIdx;
    };

    var getSeats = function(flightDetails) {
        var seats = [];
        _.each(flightDetails, function(leg) {
            var flightNumber = leg.flightNo.replace(new RegExp(leg.operatingAirlineCode, "i"), "");
            var seatData = {
                titleFrom: leg.from,
                titleTo: leg.to,
                titleFlightTracker: `${leg.operatingAirlineCode} ${flightNumber}`
            };
            _.every(leg.addonPerPax, function(addOn) {
                if (addOn.seatNumber) {
                    seatData.isPackService = addOn.isPackService;
                    seatData.seatType = addOn.seatType;
                    seatData.seatNumber = addOn.seatNumber;
                }
                return !addOn.seatNumber;
            });
            seats.push(seatData);
        });
        return seats;
    };

    var getBaggage = function(flightDetails) {
        var flightDetail = flightDetails[0],
            freeBaggage = 0,
            additionalBaggage = 0;
        
        _.each(flightDetail.addonPerPax, function(addOn) {
            freeBaggage += addOn.ffpFreeBaggageWeight ? parseInt(addOn.ffpFreeBaggageWeight) : 0;
            additionalBaggage += addOn.additionalBaggageWeight ? parseInt(addOn.additionalBaggageWeight) : 0;
        });

        return {
            freeBaggage: freeBaggage,
            additionalBaggage: additionalBaggage
        }
    };

    var getFlightLabel = function(flightDetails) {
        var label = "";
        var flightDetail = flightDetails[0];
        var packDetails = _.findWhere(flightDetail.addonPerPax, {isPackService: true});
        if (packDetails) {
            label = `Deal (${packDetails.seatType} ${packDetails.additionalBaggageWeight ? `+ ${packDetails.additionalBaggageWeight}kg baggage` : `` })`;
        } else {
            label = "-";
        }

        return label;
    };

    var calculateAddOns = function(flightDetails) {
        return _.reduce(flightDetails, function(currentValue, details) {
            return currentValue + _.reduce(details.addonPerPax, function($currentValue, addOn) {
                if (!addOn.isPackService) {
                    return 0;
                }
                return $currentValue + addOn.amount;
            }, 0)
        }, 0);
    };

    var calculateAddOnsSubTotal = function(flightLegs) {
        return _.reduce(flightLegs, function(currentValue, leg) {
            return currentValue + leg.totalAmount;
        }, 0);
    };

    var calculateBreakDownSubTotal = function(pax, flightLegs) {
        var total = 0;
        total += pax.fareDetails.fareAmount;
        total = _.reduce(pax.fareDetails.taxes, function(currentValue, tax) {
            return currentValue + tax.amount;
        }, total);
        total = _.reduce(pax.fareDetails.surcharge, function(currentValue, tax) {
            return currentValue + tax.amount;
        }, total);
        total = _.reduce(flightLegs, function(currentValue, leg) {
            return currentValue + leg.totalAmount;
        }, total);
        return total;
    };

    var formatHotelData = function(addOn) {
        var numberOfNights,
            isBreakfastIncluded,
            roomTypes;
        numberOfNights = _.reduce(addOn.rooms, function(currentValue, room) {
            return currentValue + room.numberOfNights;
        }, 0);
        isBreakfastIncluded = _.reduce(addOn.rooms, function(currentValue, room) {
            return currentValue && room.breakfast;
        }, true);
        roomTypes = _.map(addOn.rooms, function(room) {
            return `${room.roomCount} ${room.roomType}`;
        }).join(", ");

        return {
            numberOfNights: numberOfNights,
            isBreakfastIncluded: isBreakfastIncluded,
            roomType: roomTypes
        }
    };

    var cleanTpl = function(classes) {
        _.each(classes, function(className) {
            $(className).html('');
        });
    };

    var reInitializeTooltips = function() {
        $("[data-tooltip]").each(function() {
            $(this).kTooltip();
        });
    };

    var strickyScroll = function (className) {
        var stickyEl = $(className);
        stickyEl.each(function () {
            var _self = $(this);
            var top = _self.offset().top - 100;
            $(window).scroll(function (event) {
                var y = $(this).scrollTop();
                
                if (!_self.closest(".tab-content.active").length) {
                    return;
                }

                if (!_self.closest(".sticky-wrapper").length) {
                    top = _self.offset().top - 100;
                }
                if (y >= top) {
                    if (!_self.closest(".sticky-wrapper").length) {
                        _self.wrap(`<div class="sticky-wrapper"></div>`);
                        _self.closest(".sticky-wrapper").addClass("fixed");
                    }
                } else {
                    if (_self.parent().is(".sticky-wrapper")) {
                        _self.unwrap();
                    }
                }
            });
        });
    };

    return {
        init: init
    }
}();

SIA.AccordionJS = function() {
    var duration = 500;
    var init = function() {
        setAccordionContents();
    };

    var setAccordionContents = function() {
        $(".accordion-content").each(function() {
            var _self = $(this);
            var _parent = _self.closest("[data-accordion-state]");
            var accordionState = _parent.data("accordion-state");
            setAccordionTrigger(_parent, _self);
            _self[accordionState ? "slideDown" : "slideUp"](duration, function() {
                if (!accordionState) {
                    _self.css("display", "none");
                }
                _self.removeClass("hidden");
            });
        });
    };

    var setAccordionTrigger = function(parent, accordionContent) {
        var trigger = parent.find("[data-accordion-trigger]:first");
        var currentState = parent.data("accordion-state");
        var icon = trigger.find("em.ico-point-d:first");
        changeIconState(currentState, icon);
        trigger
            .off()
            .on("click", function(e) {
                e.preventDefault();
                currentState = !parent.data("accordion-state");
                closeSiblingAccordion(parent);
                changeIconState(currentState, icon);
                parent.data("accordion-state", currentState);
                accordionContent.slideToggle(duration, function() {
                    scrollAnimate(parent);
                });
            });
    };

    var closeSiblingAccordion = function(parent){
        var siblings = parent.siblings("[data-accordion-state]");
        siblings.each(function() {
            var _self = $(this);
            _self.find(".accordion-content").slideUp(duration);
            _self.data("accordion-state", false);
            changeIconState(false, _self.find("em.ico-point-d:first"));
        });
    };

    var scrollAnimate = function(scrollEl) {
        if ($('body').hasClass("no-scroll-animate")) {
            return;
        }

        $('html, body').animate({
            scrollTop: scrollEl.offset().top
        }, 300);
    };

    var changeIconState = function(currentState, icon) {
        if (currentState) {
            icon.css("transform", "rotate(180deg)");
            icon.css("margin-top", "5px");
        } else {
            icon.removeAttr("style");
        }
    };

    return {
        init: init
    }
}();

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof _ == "undefined") {
            waitForLibraries(fn);
        } else {
            SIA.mpPayments();
            fn.init();
            setTimeout(function() {
                SIA.payment();
                SIA.initAutocompleteCity();
            }, 500);
        }
    }, 50);
};

$(function() {
    waitForLibraries(SIA.Mp1Payments);
});