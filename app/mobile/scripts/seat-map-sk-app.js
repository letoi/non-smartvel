/**
 *
 * Seat Map Rendering APP
 *
 **/

 var passengerDetailStatus = '', listCol, currentSeatPrice;

 var setStatusLabel = function(ad, code, tabEl) {
 	var seatCharacteristic = code;
 		listLegend = globalJson.seatMap.legends,
 		legendObj = {};

 	seatCharacteristic.length && _.map(seatCharacteristic, function(character){
 		_.map(listLegend, function(legend){
 			(legend.code === character) && (legendObj = legend);
 		})
 	})

 	passengerDetailStatus = seatCharacteristic;
 	return passengerDetailStatus;

 	if(tabEl && tabEl.length) {
 		if(!_.isEmpty(legendObj)) {
 			switch(legendObj.code) {
 				case "S":
 					return tabEl.data('info-standard');
 				case "F":
 					return tabEl.data('info-forward');
 				case "P":
 					return tabEl.data('info-preferred');
 				default:
 					return;
 			}
 		}
 	}

 	return;
 }

var mergeJson = function(data){

		function mergeDeep(o1, o2) {
	    var tempNewObj = o1;

	    if (o1.length === undefined && typeof o1 !== "number") {
	        $.each(o2, function(key, value) {
	            if (o1[key] === undefined) {
	                tempNewObj[key] = value;
	            } else {
	                tempNewObj[key] = mergeDeep(o1[key], o2[key]);
	            }
	        });
	    }
	    else if (o1.length > 0 && typeof o1 !== "string") {
	        $.each(o2, function(index) {
	            if (JSON.stringify(o1).indexOf(JSON.stringify(o2[index])) === -1) {
	                tempNewObj.push(o2[index]);
	            }
	        });
	    }
	    else {
	        tempNewObj = o2;
	    }
	    return tempNewObj;
	};

	var currObj = {};

	for(var i = 0; i < data.row.length; i++) {
		if(!_.isEmpty(currObj) && currObj.number === data.row[i].number){
			currObj = mergeDeep(currObj, data.row[i]);
			data.row.splice(i, 1);
			i = i - 1;
		} else {
			currObj = data.row[i];
		}
	}

	return data;
}

var changeTab = function(nextFlightUrl) {
	$.ajax({
		url: nextFlightUrl,
		method: 'GET',
		dataType: 'json',
		beforeSend: function(){
			$('.overlay-loading').removeClass('hidden').show();
		},
		success: function(response){
			var nextJson = mergeJson(response);
			var flightSelectedIdx;
			var flightTabs;

			var totalFare = $('.total-cost').data('total-fare');
			$('.total-cost').find('.amount').text(totalFare.toLocaleString(undefined, { minimumFractionDigits: 2 }))
			$('.total-cost').attr('data-price', JSON.stringify({}));

			SIA.RenderSeat.init(nextJson);
			flightSelectedIdx = $('[data-click-through]').find('.tab-item.active').index();
			flightTabs = $('[data-click-through]').find('.tab-item:not(".disabled")');
			$('input[name="seat-3-submit"]').removeAttr('data-last-flight');
			$('input[name="seat-3-submit"]').val('next flight');
			if(flightSelectedIdx >= flightTabs.length - 1) {
				$('input[name="seat-3-submit"]').attr('data-last-flight', true);
				$('input[name="seat-3-submit"]').val('proceed to payment');
			}
			$('.overlay-loading').addClass('hidden').hide();
		}
	})
}

var changePage = function() {
	var url = 'sk-ut-payments-a.html'

	!$('body').is('.sk-ut-seatmap-a') && (url = 'sk-ut-payments-b.html')
     window.location.replace(url);
}

SIA.RenderSeat = (function() {
	// Declare private vars fs gfsd fkhasd
	var global = SIA.global;
	var body = global.vars.body;
	var container = $('.main-inner');
	var tabs = container.find('.seat-tabs>.tab');
	var select = container.find('#main-tab-select');
	var paxNav = container.find('.seat-tabs .tab-wrapper .booking-nav');

	var paxSelect, paxLabel;
	var paxPopup = $('#paxPopup .pax-group');

	var seatForm = container.find('#form-seatmap');
	var seatContainer = seatForm.children('.seatmap');
	var flightHeader = seatForm.siblings('.blk-heading');

	// Cache equipment container for upper and main deck
	var equipCont = seatForm.find('.seat-equipped');
	var equipContUD = equipCont.clone(false).attr('id','seat-ud').html('').addClass('hidden');
	equipCont.attr('id','seat-md').before(equipContUD);

	var seatsCont = seatForm.find('.seatmap-content');
	var deckNav = seatsCont.find('.seat-deck-nav');

	var mainDeck = $('<div id="main-deck" class="seat-deck"></div>');
	var upperDeck = $('<div id="upper-deck" class="seat-deck"></div>');

	var wingCont = seatForm.find('.seat-bg');
	var deckClass;
	var thisItemClick;

	// Declare templates
	var seatObjects = {
		template: {
			cabin: '<div class="seatmap-cabin"><div class="seatmap-cabin-row seatmap-toprow"></div><div class="seatmap-cabin-wrapper"></div></div>',
			blk: '<div class="seatmap-row-block"></div>',
			seatRow: '<div class="seatmap-cabin-row"><span class="seatmap-rownum left">{0}</span><span class="seatmap-rownum right">{0}</span></div>',
			sLabel: '<div class="seatmap-columnletter">{1}</div>',
			seatColumn: '<div class="seatmap-row-block"></div>',
			aisle: '<div class="seat-aisle"></div>',
			inforSeat: '<span class="passenger-detail__seat"></span>',
			space: '<div class="seatmap-cabin-separate"></div>',
			equipment: '<span class="seat-equipped-item">All seats are equipped with:</span>',
			equipmentLegend: '<div class="seat-legend"><div class="seat-legend-content"></div><a href="#" class="seat-legend__control">seat legend<em class="ico-down"></em></a></div>',
			wingStart: '<div class="seatmap-wings seatmap-wingstart"><div class="seatmap-wing-content"><span class="seatmap-wingtip left"><img src="images/wing-start-l.png" alt="" /></span><span class="seatmap-wingtip right"><img src="images/wing-start-r.png" alt="" /></span></div></div>',
			wingEnd: '<div class="seatmap-wings seatmap-wingend"><div class="seatmap-wing-content"><span class="seatmap-wingtip left"><img src="images/wing-end-l.png" alt="" /></span><span class="seatmap-wingtip right"><img src="images/wing-end-r.png" alt="" /></span></div></div>',
			genericMessage: '<p>Specific seat selection is not available on this flight, as it&rsquo;s operated by our partner airline. Please choose your preferred seat type, and we&rsquo;ll do our best to ensure that our partner airline fulfils your selection.</p>',
			genericSeats: '<div class="seatmap-content"><h4 class="sub-heading-2--grey">Legend</h4><div class="type-seatmap"><div class="type-seatmap-item"><div data-sia-rowblock="1" class="seatmap-row-block"><span class="seat seat-free">W</span><span class="seat seat-free">&nbsp;</span><span class="seat seat-free">A</span></div><div class="seat-aisle"></div><div data-sia-rowblock="2" class="seatmap-row-block"><span class="seat seat-free">A</span><span class="seat seat-free">&nbsp;</span><span class="seat seat-free">A</span></div><div class="seat-aisle"></div><div data-sia-rowblock="3" class="seatmap-row-block"><span class="seat seat-free">A</span><span class="seat seat-free">&nbsp;</span><span class="seat seat-free">W</span></div></div></div><p class="note-text">Aircraft types may vary. For details, get in touch with our partner airline operating this flight.</p></div>'
		},
		deckClass: null
	};

	// Declare JSON variable cache containers
	var compartment, row, cl, rl, actLeg, flight;
	var preselected = [],
		preselAttr = [],
		cabins = [],
		rows = [],
		paxGroups = [];

	var seatMapJson;
  var seatMapTravelPartyTpl = function(data1){
    var templateSeatMapTravelParty,
    		appendDiv = $('.travel-party-wrapper');
    $.get(global.config.url.seatMapTravelParty, function (data) {
      var template = window._.template(data, {
        data: data1
      });
      templateSeatMapTravelParty = $(template);
      appendDiv.empty().append(templateSeatMapTravelParty);
    });
  };
	var seatMapSeatSelectionTpl = function(data1){
    var templateSeatMapSeatSelection,
    		appendDiv = $('.accordion-seat-selection-wrapper');
    $.get(global.config.url.seatMapSeatSelection, function (data) {
      var template = window._.template(data, {
        data: data1
      });
      templateSeatMapSeatSelection = $(template);
      appendDiv.empty().append(templateSeatMapSeatSelection);
      addClassForSeatSelection();
    });
  };
	// Paint the flight tabs
  var seatMapNoteTpl = function(data1){
    var templateSeatMapNote,
    appendDiv = $('.seat-status');
      $.get(global.config.url.seatMapNote, function (data) {
        var template1 = window._.template(data, {
          data: data1
        });
        templateSeatMapNote = $(template1);
        appendDiv.empty().append(templateSeatMapNote);
        var seatForwardZone = $('.seatmap-row-block').find('.seat.forward-zone');
        seatForwardZone.closest('.seatmap-cabin-row').addClass('green-background');
        if($('[data-tooltip]')) {
          $('[data-tooltip]').kTooltip();
        }
        if($('body').hasClass('first-seatmap-page') || $('body').hasClass('business-seatmap-page')){
        	var changeTextSS = appendDiv.find('li').find('.standard-seat').next();
        	changeTextSS.text('Available seat');
        }
      });
   };
	var tabInfo = function() {
		var flightInfo = globalJson.seatMap.seatMapVO.flightDateInformationVO;
		var airCraftInfo = globalJson.seatMap.flight.equipment.aircraft;
		var l = flightInfo.length;

		tabs.empty();
		select.empty();

		for (var i = 0; i < l; i++) {
			var cinfo = flightInfo[i];
			var active = '';
			var disabled = '';
			var selected = '';

			if (cinfo.disabled === true) {
				disabled = ' disabled';
			} else {
			  if (cinfo.selected) {
			  	active = ' active';
			  	selected = ' selected="selected"'
			  }
			}

			if (!cinfo.disabled && cinfo.selected) {
				actLeg = cinfo;
			}
			var tab = null;
			var ct = '<div data-customselect="true" class="custom-select custom-select--2 hidden" data-replace-text-by-plane="to">' +
                '<label for="multi-select-limit-'+i+'" class="select__label">&nbsp;</label><span class="select__text">'+ cinfo.departureCityCode +' <em class="ico-plane"></em> ' + cinfo.arrivalCityCode + '</span><span class="ico-dropdown"></span>' +
                '<select id="multi-select-limit-'+i+'" name="multi-select-limit">' +
                '</select>'+
              '</div>';
			tab = $('<li class="tab-item' + active + disabled + '" data-nextflight="'+ cinfo.nextFlightUrl +'"><a href="javascript:void(0)" data-flightnumber="' + cinfo.marketingAirlineCode + cinfo.flightNumber + '" data-flightdate="' + cinfo.departureDate + '" data-departsegment="' + cinfo.departureCityCode + '" data-arrivalsegment="' + cinfo.arrivalCityCode + '">' + cinfo.departureCityCode + '<em class="ico-plane">&#xe601;</em>' + cinfo.arrivalCityCode + '<em class="ico-dropdown"></em></a>'+ ct +'</li>');

			var opt = $('<option value="' + (i + 1) + '"' + selected + disabled + ' data-flightnumber="' + cinfo.marketingAirlineCode + cinfo.flightNumber + '" data-flightdate="' + cinfo.departureDate + '" data-departsegment="' + cinfo.departureCityCode + '" data-arrivalsegment="' + cinfo.arrivalCityCode + '">' + cinfo.departureCityCode + ' to ' + cinfo.arrivalCityCode + '</option>');

			tabs.append(tab);
			select.append(opt);

			if(l < 2) {
				tabs.hide();
				select.hide();
			}

			if (!cinfo.disabled && cinfo.selected) {
				flightDetails(flightInfo[i].departureCity, flightInfo[i].arrivalCity, flightInfo[i].carrierNumber, airCraftInfo, flightInfo[i].classOfService);
			}
		}

		var moreTab = $('<li class="more-item"><a href="#">More<em class="ico-dropdown"></em></a></li>');

		tabs.append(moreTab)

		var tabWrapper = tabs.parent();
		var buildSelectForLongText = function(){
			var indexLimitTab = 0;
			var totalwid = 0;
			var tabItem = tabs.children();
			tabItem.each(function(i){
				var self = $(this);
				var ctselect = self.find('select');
				// ctselect.empty().append(select.children(':eq('+i+')').clone());
				ctselect.empty().append(select.children(':eq('+i+'),:gt('+i+')').clone());
				if(totalwid + self.outerWidth() <= global.config.tablet && !tabItem.eq(indexLimitTab).hasClass('limit-item')){
					totalwid +=(self.outerWidth() - (self.find('em.ico-dropdown').is(':hidden') ? 0 : self.find('em.ico-dropdown').outerWidth(true)));
					indexLimitTab = i;
				}
			});
			tabWrapper.addClass('multi-tabs');
			tabItem.filter(':gt('+indexLimitTab+')').not('.more-item').addClass('hidden');
			if(tabItem.eq(indexLimitTab).hasClass('limit-item')){
			}
		};

		buildSelectForLongText();

	};

	// Paint the flight details header
	var flightDetails = function(o, d, cn, ac, c) {
		if(typeof ac === 'undefined'){
			ac = '';
		}else{
			ac = ac + ' ';
		}
		flightHeader.find('h3').html(o + ' to ' + d);
		flightHeader.find('h4 .text-infor').css('display','block').html(cn + ' ' + ac + '&bull; ' + c);
	};

	var findSeat = function(ad, seatNumber, seatCharacter){
		var listRowSeat = globalJson.seatMap.row,
			seatCharacteristic = [];

		_.map(listRowSeat, function(rowSeat){
			if(rowSeat.number === seatNumber) {
				for(var pra = 0; pra < rowSeat.seat.length; pra++) {
					if(rowSeat.seat[pra].column === seatCharacter) {

							for(var pr = 0; pr < rowSeat.seat[pra].seatPrice.length; pr++) {
								if(rowSeat.seat[pra].seatPrice[pr].passengerID === ad.passengerID) {
									var detailSeatStatus;

										if(rowSeat.seat[pra].seatPrice[pr].amount < 1) {
											detailSeatStatus = 'Complimentary';
										} else {
											detailSeatStatus = rowSeat.seat[pra].seatPrice[pr].currency + ' ' + rowSeat.seat[pra].seatPrice[pr].amount;
										}
										seatCharacteristic = detailSeatStatus;


								}
							}
					}
				}
			}
		})

		setStatusLabel(ad, seatCharacteristic);
	}

	var paxDetails = function() {
		var paxAr = globalJson.seatMap.passenger;
		var start = globalJson.seatMap.seatMapVO.passengerStartingPoint;

		paxNav.html('<label class="tab-select_label hidden" for="sidebar-tab-select">Passenger tab</label><select name="sidebar-tab-select" id="sidebar-tab-select" class="tab-select"></select>');

		paxSelect = paxNav.find('#sidebar-tab-select');
		paxLabel = paxNav.find('.tab-select_label');

		// Reset popup
		paxPopup.html('<label class="tooltip__label">Select this seat for:</label>');

		var l = paxAr.length;

		var wChild = [];
		var paxTabs = [];
		var paxOptions = [];
		var paxRadios = [];

		for (var i = 0; i < l; i++) {
			// Cache var
			var po = paxAr[i];
			var active = (i + 1) === parseInt(start) ? ' active' : '';
			var seat = '';
			var disabled = '';
			// update seatmap popup
			var isPreferredSeat = '';

			// Check if infant then get adult partner

			var seatNo;
			var newSeatVal = '';
			if (po.seatSelected.seatNumber && po.seatSelected.seatNumber.toLowerCase() !== 'na') {
				seatNo = po.seatSelected.seatNumber;
				if (seatNo.charAt(0) === '0') {
					seatNo = seatNo.slice(1);
				}

				seat = '<span class="passenger-detail__seat">' + seatNo + '</span>';
				preselected.push([seatNo, po.passengerID, paxTabs.length + 1]);
				preselAttr.push(seatNo);

				disabled = ' disabled="disabled"';
				newSeatVal = seatNo;
			} else {
				seatNo = 'NA';
			}

			var child = po.passengerType.toLowerCase() === 'chd' ? '&nbsp;-&nbsp;Child' : '';

			var paxName = po.lastName.length > 0 ? po.title + ' ' + po.lastName + ' ' + po.firstName : 'Passenger ' + (paxTabs.length + 1) + ' - ' + po.passengerType;
			var infantName = po.infant ? po.infant.firstName + ' ' + po.infant.lastName + '<span>&nbsp;-&nbsp;Infant</span>' : '';

			var seatStatus = '';
			var disabledPassenger = '';

			if(typeof po.seatSelected.seatNumber === 'undefined' || po.seatSelected.seatNumber.toLowerCase() === 'na') {
			  seatStatus = 'No seats selected';
			} else {
				// find seat
				var seatNumber = parseInt(po.seatSelected.seatNumber),
					seatCharacter = po.seatSelected.seatNumber.split(seatNumber)[1];

				seatStatus = findSeat(po, seatNumber, seatCharacter) || passengerDetailStatus;
			}

			if(po.disableSeatSelection) {
				seatStatus = 'Not allowed';
				disabledPassenger = ' disabled';
			}

			// update seatmap popup
			var preferredSeatLast = po.isPreferredSeat;

			if($('body').hasClass('cib-seatsmap-page')){
				var paxTab = $('<a href="#" class="booking-nav__item' + active + disabledPassenger + '" data-preferred-seat='+ preferredSeatLast + ' data-info-preferred="'+ po.PreferredSeat+'" data-info-forward="'+ po.ForwardZoneSeat +'" data-info-standard="'+ po.StandardSeat + '" data-id="'+ po.passengerID + '"><span class="passenger-info"><span class="passenger-info__number">' + (paxTabs.length + 1) + '.</span><span class="passenger-info__text">' + paxName + child + '</span><span class="passenger-info__text">' + infantName + child + '</span><span class="passenger-detail__seat-status">'+ seatStatus +'</span><em class="ico-point-r"></em><input type="hidden" name="paxNew[]" value="' + newSeatVal + '"></span>' + seat + '</a>');
			}else{
				var paxTab = $('<a href="#" class="booking-nav__item' + active + disabledPassenger + '" data-info-preferred="'+ po.PreferredSeat+'" data-info-forward="'+ po.ForwardZoneSeat +'" data-info-standard="'+ po.StandardSeat +'"><span class="passenger-info"><span class="passenger-info__number">' + (paxTabs.length + 1) + '.</span><span class="passenger-info__text">' + paxName + child + '</span><span class="passenger-info__text">' + infantName + child + '</span><span class="passenger-detail__seat-status">'+ seatStatus +'</span><em class="ico-point-r"></em><input type="hidden" name="paxNew[]" value="' + newSeatVal + '"></span>' + seat + '</a>');
			}

			var option = $('<option value="' + (paxTabs.length + 1) + '">' + paxName + '</option>');

			if($('body').hasClass('cib-seatsmap-page')){
				var paxRadio = $('<div class="custom-radio custom-radio--1 ' + (po.disableSeatSelection ? 'disabled' : '') + '"><input name="search-radio" disabled id="seat-tooltip-radio-' + po.passengerID + '" type="radio" value="' + newSeatVal + '" data-preferred-seat ='+ preferredSeatLast + ' data-info-preferred="'+ po.PreferredSeat+'" data-info-forward="'+ po.ForwardZoneSeat +'" data-info-standard="'+ po.StandardSeat + '" data-id="'+ po.passengerID + '"><label for="seat-tooltip-radio-' + po.passengerID + '">' + paxName + '<br />' + infantName + '</label>'+seat+'<span class="passenger-detail__seat-status">'+ seatStatus +'</span></div>');
			}else{
				var paxRadio = $('<div class="custom-radio custom-radio--1 ' + (po.disableSeatSelection ? 'disabled' : '') + '"><input name="search-radio" disabled id="seat-tooltip-radio-' + po.passengerID + '" type="radio" value="' + newSeatVal + '" data-info-preferred="'+ po.PreferredSeat+'" data-info-forward="'+ po.ForwardZoneSeat +'" data-info-standard="'+ po.StandardSeat +'"><label for="seat-tooltip-radio-' + po.passengerID + '">' + paxName + '<br />' + infantName+ '</label>'+seat+'<span class="passenger-detail__seat-status">'+ seatStatus +'</span></div>');
			}

			// check if there is oldSeatNumber there
			if(po.oldSeatNumber){
				paxTab = $('<a href="#" class="booking-nav__item' + active + '" data-oldseatnumber="'+ po.oldSeatNumber +'" data-assignseatnumber="'+ po.seatNumber +'" data-info-preferred="'+ po.PreferredSeat+'" data-info-forward="'+ po.ForwardZoneSeat +'" data-info-standard="'+ po.StandardSeat +'"><span class="passenger-info"><span class="passenger-info__number">' + (paxTabs.length + 1) + '.</span><span class="passenger-info__text">' + paxName + child + '</span><span class="passenger-detail__seat-status">'+ seatStatus +'</span><em class="ico-point-r"></em><input type="hidden" name="paxNew[]" value="' + newSeatVal + '"></span>' + seat + '</a>');
				paxRadio = $('<div class="custom-radio custom-radio--1 ' + (po.disableSeatSelection ? 'disabled' : '') + '"><input name="search-radio" disabled="'+(po.disableSeatSelection ? true : false)+'" data-oldseatnumber="'+ po.oldSeatNumber +'" data-assignseatnumber="'+ po.seatNumber +'" id="seat-tooltip-radio-' + po.passengerID + '" type="radio" value="' + newSeatVal + '"  data-info-preferred="'+ ad.PreferredSeat+'" data-info-forward="'+ ad.ForwardZoneSeat +'" data-info-standard="'+ ad.StandardSeat +'"><label for="seat-tooltip-radio-' + po.passengerID + '">' + paxName + '</label>'+seat+'<span class="passenger-detail__seat-status">'+ seatStatus +'</span></div>');
			}

			if (po.passengerType.toLowerCase() === 'child') {
				paxTab.attr('data-ischild', true);
				option.attr('data-ischild', true);
			}

			if(po.infant) {
				paxTab.attr('data-hasinfant', true);
				option.attr('data-hasinfant', true);
			}

			// Add passenger in array to be used on seat selection
			paxGroups.push(po);

			paxTabs.push(paxTab);
			paxOptions.push(option);
			paxRadios.push(paxRadio);

			// Add form object to POST on form submit. eg:'paxOld1-31A'
			seatForm.prepend($('<input type="hidden" name="paxOld[]" value="paxOld' + po.passengerID + '-' + seatNo + '" data-paxindex="' + paxTabs.length + '">'));
		}

		// Add passengers to the DOM
		var pl = paxTabs.length;

		for (var i = 0; i < pl; i++) {
			paxLabel.before(paxTabs[i]);
			paxSelect.append(paxOptions[i]);
			paxPopup.append(paxRadios[i]);
		}

		// Add preselected attribute to container
		seatContainer.attr('data-preselected', preselAttr.toString());
	};

	// Update default hidden input fields
	var updateInputFields = function() {
		// Get selected flight block
		var curFlight;
		for (var i = globalJson.seatMap.seatMapVO.flightDateInformationVO.length - 1; i >= 0; i--) {
			if (globalJson.seatMap.seatMapVO.flightDateInformationVO[i].selected === true) {
				curFlight = globalJson.seatMap.seatMapVO.flightDateInformationVO[i];
				break;
			}
		}

		var flowIndicator = curFlight.FlowIndicator;
		var existingFlightDate = curFlight.departureDate;
		var existingFlightNumber = curFlight.flightNumber;
		var existingDepartureSegment = curFlight.departureCityCode;
		var existingArrivalSegment = curFlight.arrivalCityCode;
		var existingCabin = curFlight.existingCabin;
		var newFlightDate = curFlight.newFlightDate;
		var newFlightNumber = curFlight.newFlightNumber;
		var newDepartureSegment = curFlight.newDepartureSegment;
		var newArrivalSegment = curFlight.newArrivalSegment;
		var newCabin = curFlight.newCabin;
		var paymentRequired = curFlight.paymentRequired;

		if (typeof flowIndicator !== 'undefined') {
			$('input[name="FlowIndicator"]').val(flowIndicator);
		} else {
			$('input[name="FlowIndicator"]').val('NA');
		}

		if (typeof existingFlightDate !== 'undefined') {
			$('input[name="existingFlightDate"]').val(existingFlightDate);
		} else {
			$('input[name="existingFlightDate"]').val('NA');
		}

		if (typeof existingFlightNumber !== 'undefined') {
			$('input[name="existingFlightNumber"]').val(existingFlightNumber);
		} else {
			$('input[name="existingFlightNumber"]').val('NA');
		}

		if (typeof existingFlightNumber !== 'undefined') {
			$('input[name="existingFlightNumber"]').val(existingFlightNumber);
		} else {
			$('input[name="existingFlightNumber"]').val('NA');
		}

		if (typeof existingDepartureSegment !== 'undefined') {
			$('input[name="existingDepartureSegment"]').val(existingDepartureSegment);
		} else {
			$('input[name="existingDepartureSegment"]').val('NA');
		}

		if (typeof existingArrivalSegment !== 'undefined') {
			$('input[name="existingArrivalSegment"]').val(existingArrivalSegment);
		} else {
			$('input[name="existingArrivalSegment"]').val('NA');
		}

		if (typeof existingCabin !== 'undefined') {
			$('input[name="existingCabin"]').val(existingCabin);
		} else {
			$('input[name="existingCabin"]').val('NA');
		}

		if (typeof newFlightDate !== 'undefined') {
			$('input[name="newFlightDate"]').val(newFlightDate);
		} else {
			$('input[name="newFlightDate"]').val('NA');
		}

		if (typeof newFlightNumber !== 'undefined') {
			$('input[name="newFlightNumber"]').val(newFlightNumber);
		} else {
			$('input[name="newFlightNumber"]').val('NA');
		}

		if (typeof newDepartureSegment !== 'undefined') {
			$('input[name="newDepartureSegment"]').val(newDepartureSegment);
		} else {
			$('input[name="newDepartureSegment"]').val('NA');
		}

		if (typeof newArrivalSegment !== 'undefined') {
			$('input[name="newArrivalSegment"]').val(newArrivalSegment);
		} else {
			$('input[name="newArrivalSegment"]').val('NA');
		}

		if (typeof newCabin !== 'undefined') {
			$('input[name="newCabin"]').val(newCabin);
		} else {
			$('input[name="newCabin"]').val('NA');
		}

		if (typeof paymentRequired !== 'undefined') {
			$('input[name="paymentRequired"]').val(paymentRequired);
		} else {
			$('input[name="paymentRequired"]').val('NA');
		}

		// Update the paxType and oldSeatType input field by running through the passenger object

		var paxObj = globalJson.seatMap.passenger;
		var paxType = [];
		var oldSeatType = [];

		for (var j = paxObj.length - 1; j >= 0; j--) {
			paxType.push(paxObj[j].passengerID + '-' + paxObj[j].passengerType);
			oldSeatType.push(paxObj[j].passengerID + '-' + paxObj[j].seatType);
		}

		$('input[name="paxType"]').val(paxType);
		$('input[name="oldSeatType"]').val(oldSeatType);
	};

	// Render the seat map
	var renderMap = function() {
		// Cache the active class
		seatObjects.deckClass = actLeg.classOfService;
		// Add the class
		var seatClass = actLeg.classOfService.toLowerCase().replace(' class', '');

		seatsCont.addClass('seatmap--' + seatClass);

		// Setup the decks
		seatsCont.find('#upper-deck, #main-deck').remove();
		seatsCont.append(upperDeck.empty()).append(mainDeck.empty());
		// Loop through the cabins
		var rowcount = 0;

		cabins = [];

		for (var cbc = 0; cbc < cl; cbc++) {

			var cabin = $(seatObjects.template.cabin);
			var cabinWrap = cabin.find('.seatmap-cabin-wrapper');
			var cabinTop = cabin.find('.seatmap-toprow');

			// Add column groups as the cabinwrappers data
			cabinWrap.data('groupLabels', []);

			// Add reference data for the cabins column wrappers
			cabinWrap.data('groupWrapper', []);

			cabins.push([cabinWrap, cabinTop]);

			// Append to correct cabin container
			// Assign current equipment container
			var curEquipCont;
			if (compartment[cbc].location === 'U') {
				upperDeck.append(cabin);
				curEquipCont = equipContUD;
			} else {
				mainDeck.append(cabin);
				curEquipCont = equipCont;
			}

			// Cache cabin equipment values
			var seat = flight.cabinAmenities.seat;
			var bed = flight.cabinAmenities.bed;
			var power = flight.cabinAmenities.power;
			var screen = flight.cabinAmenities.screen;
			var eql = seatObjects.template.equipmentLegend;

			// Add the cabin description while in the loop for the first cabin
			// Check if any equipment is available then add them in to current container
			if (typeof seat !== 'undefined' || typeof bed !== 'undefined' || typeof power !== 'undefined' || typeof screen !== 'undefined' && curEquipCont.children().length) {
				var eq = seatObjects.template.equipment;
				curEquipCont.html('').append($(eq));

				// Check each seat charactersitics and render available ones
				if(typeof seat !== 'undefined') {
					curEquipCont.append($('<span class="seat-equipped-item"><em class="ico-1-flat-bed"></em>' + seat + '</span>'));
				}
				if(typeof bed !== 'undefined') {
					curEquipCont.append($('<span class="seat-equipped-item"><em class="ico-1-flat-bed"></em>' + bed + '</span>'));
				}
				if(typeof power !== 'undefined') {
					curEquipCont.append($('<span class="seat-equipped-item"><em class="ico-1-plug-in"></em>' + power + '</span>'));
				}
				if(typeof screen !== 'undefined') {
					curEquipCont.append($('<span class="seat-equipped-item"><em class="ico-1-screen"></em>' + screen + '</span>'));
				}
			}

			// Fix issue misses block Seat Legend on devices
			if(!curEquipCont.find('.seat-legend').length) {
				curEquipCont.append($(eql));
			}

			// Print the column labels
			var columnDetails = compartment[cbc].column;
			var colCount = columnDetails.length;
			// var to check if its right(true) or left(false) edge. If left then add new blk
			var bool = false;
			var grpCount = 0;
			var blk = $(seatObjects.template.blk);
			var curBlk = blk.clone();
			cabinTop.append(curBlk);

			// Add initial aisle
			cabinTop.append($(seatObjects.template.aisle));
			// Add reference to current group
			var curGrp = [];

			// Add column groups at the cabin-wrappers data
			cabinWrap.data('groupLabels').push(curGrp);
			cabinWrap.data('groupWrapper').push(curBlk);

			// Add the column container to the cabin and add the labels
			for (var i = 0; i < colCount; i++) {
				// Create new instance of colLabelCont
				var colLabelCont = $(seatObjects.template.sLabel);
				// var colLabel = columnDetails[i].seatColumn;
				colLabelCont.text(columnDetails[i].designator);
				curBlk.append(colLabelCont);

				curGrp.push(columnDetails[i].designator);

				// Check if Aisle to initiate new column group
				if (columnDetails[i].characteristics === 'A' || columnDetails[i].characteristics === 'W/A') {
					if (!bool) {
						curBlk = blk.clone();

						cabinTop.append(curBlk);

						bool = !bool;

						curGrp = [];
					} else {
						// Add aisle
						cabinTop.append($(seatObjects.template.aisle));
						bool = !bool;
						grpCount++;

						// Add column groups as the cabin-wrappers data
						cabinWrap.data('groupLabels').push(curGrp);
						cabinWrap.data('groupWrapper').push(curBlk);
					}
				}
			}

			// get total group count
			var tg = cabinTop.find('.seatmap-row-block').length;

			// Add colgrp attribute for later width reference
			var colRowBlks = cabinTop.find('.seatmap-row-block');
			for (var i = colRowBlks.length - 1; i >= 0; i--) {
				colRowBlks.eq(i).attr('data-colgrp', i);
			}

			// Remove extra aisle
			cabinTop.find('.seat-aisle').last().remove();

			// Print the row number, row group containers and the seats for each row
			var rowStart = compartment[cbc].startRow;
			var rowEnd = compartment[cbc].endRow;

			if(rowEnd === undefined ){
				rowEnd = rowStart;
			}

			// var totalRows = rowEnd - rowStart + 1;
			var crc = 0;

			// Start iterating the row information and start render
			for (var i = rowStart; i < rowEnd + 1; i++) {
				// Check if row exist
				var r = getRow(i);
				if (!r) {
					continue;
				}

				// Create a row container
				var row = $(seatObjects.template.seatRow);
				row.find('span').text(i);
				row.attr('data-row', i);
				cabinWrap.append(row);

				// Render rows
				mapRow(row, i, tg, cbc);

				// If rows has no seat nor galleys nor facilities, remove the row
				// Start check with rows with empty seat
				if(row.hasClass('row-has-empty-seat') && !row.hasClass('seat-inner-bassinet') && !row.hasClass('seat-inner-galley')) {
					var empty = checkRowEmpty(row);
					if(empty) {
						row.remove();
					}else {
						// Add the row to the array for reference
						rows.push(row);
					}
				}

				// Increment cabin row counter
				crc++;
				rowcount++;
			}

			// Check for cabin facility

			//-----------

			var cabinFacilityObj = compartment[cbc];
			// Check if cabin facility exists
			if(typeof cabinFacilityObj !== 'undefined') {
				// Assign direction based on rowLocation
				var cDir = true;
				if(cabinFacilityObj.location === 'F') {
					cDir = false;
				}
				if(cabinFacilityObj.location === 'R') {
					cDir = true;
				}

				cabinFacility(cabinWrap, cabinFacilityObj, cDir);
			}
		}

		// Temporarily add active to all decks so we can get a solid block width for the elements
		mainDeck.addClass('active');
		upperDeck.addClass('active');

		// Check for start wing and end wing info
		var rowWingStart = seatsCont.find('.seat-wingstart');
		var rowWingEnd = seatsCont.find('.seat-wingend');

		// Hide main deck if its empty
		if (upperDeck.children().length > 0 && mainDeck.children().length < 1) {
			mainDeck.removeClass('active');
			equipCont.addClass('hidden');
			equipContUD.removeClass('hidden');
		}

		// Hide upper deck if its empty
		if (mainDeck.children().length > 0 && upperDeck.children().length < 1) {
			upperDeck.removeClass('active');
			renderWings(rowWingStart, rowWingEnd);
		}

		// Hide the upperDeck if both have children and initiate the deck navi
		if (upperDeck.children().length > 0 && mainDeck.children().length) {
			upperDeck.removeClass('active');
			initDeckNav();
			// render wings after deck init's so the toggle is visible
			renderWings(rowWingStart, rowWingEnd);
		}
	};

	var mapRow = function(row, rowNo, totalGrp, cabinIndex) {
		// Get the row object matching the current row
		var crArr = getRow(rowNo);

		var curRow = crArr[0];
		var curRowObjIndex = crArr[1];

		// Assign current row details
		var rowFacilities = curRow.facility;

		var rowColDetails = curRow.seat;

		// From the total group count while creating the labels,
		// add the same column groups per row
		var cabinRow = row;
		for (var c = 0; c < totalGrp; c++) {
			var seatBlk = $(seatObjects.template.blk);
			seatBlk.attr('data-colgrp', c);

			// Append to the cabin row the row group
			cabinRow.append(seatBlk);

			// Add aisle
			if (c !== totalGrp - 1) {
				cabinRow.append($(seatObjects.template.aisle));
			}

			// Get the seat labels that belong to this row
			var curRow = cabins[cabinIndex][0].data('groupLabels')[c];

			// Render the seats in this row group
			rowColDetails && renderSeats(curRow, rowColDetails, c, seatBlk, rowNo, totalGrp);

			// Render Facilities, this will be called 3 times in one row
			// Check which location the group is in then render facility

			// ---------
			rowFacilities && renderFacilities(c, totalGrp, seatBlk, rowFacilities);

			// Add data-col attribute
			seatBlk.attr('data-col', curRow.length);

			// Once seats are added in, check if content has facility
			if (seatBlk.attr('data-replace')) {
				var f = $(getFacility(seatBlk.attr('data-replace')));
				seatBlk.html(f).parent().addClass('seat-has-facility');
			}

			// Check if has galley or any facility inside the row
			if (seatBlk.children('.seatmap-galley').length > 0 && !seatBlk.attr('data-replace')) {
				// Only remove empty seats if all the seats in the group are empty
				if (seatBlk.children('.seat').length === seatBlk.children('.seat.seat-empty').length) {
					seatBlk.children('.seat-empty').remove();
				}
			}

			// Check if column group has empty seats and facility in it
			// If true add galley-less class to parent
			if (seatBlk.children('.seat.seat-empty').length && seatBlk.children('.seatmap-galley').length) {
				if (seatBlk.children('.seat.seat-empty').length > 1) {
					seatBlk.addClass('galley-less-' + seatBlk.children('.seat.seat-empty').length);
				} else {
					seatBlk.addClass('galley-less');
				}
			}

			// Check the exits and add facility-lift class if necessary
			// If row has exit
			if (cabinRow.hasClass('seat-row-hasexit')) {
				cabinRow.find('.seatmap-exit').remove();
				cabinRow.append($('<span class="seatmap-exit left">Exit</span>'));
				cabinRow.append($('<span class="seatmap-exit right">Exit</span>'));
			}
			// If row has exit but no blank row before it
			if (cabinRow.hasClass('seat-row-hasexit') && !cabinRow.prev().hasClass('seatmap-row--empty')) {
				cabinRow.addClass('seatmap-facility-lift');
			}
			// If row has exit but no facility lift class
			if(cabinRow.hasClass('seat-row-hasexit') && !cabinRow.hasClass('seatmap-facility-lift')) {
				cabinRow.addClass('seatmap-facility-lift');
			}

			// If previous row has empty seat add class to the row, in case bassinet needs to adjust
			var prevRow = cabinRow.prev();
			if (prevRow.hasClass('row-has-empty-seat')) {
				var emptyCol = prevRow.find('.col-has-empty-seat');
				if (emptyCol.length > 1) {
					cabinRow.find('[data-colgrp="' + emptyCol.attr('data-colgrp') + '"]').addClass('galley-less-' + emptyCol.length);
				} else {
					cabinRow.find('[data-colgrp="' + emptyCol.attr('data-colgrp') + '"]').addClass('galley-less');
				}
			}

			// If current cabin has bassinet and previous has no empty colgrp's
			if (cabinRow.hasClass('seat-inner-bassinet') && !cabinRow.hasClass('seatmap-facility-lift')){
				// If previous column has no empty groups at all
				// Else get the parallel group and check if its empty
				if(!prevRow.hasClass('has-colgrp-noseats') && !prevRow.hasClass('has-colgrp-nofacil')) {
					prevRow.addClass('seatmap-facility-lift');
				}else {
					// Get all col groups with bassinet
					var curBasGrp = jqEachToArray(cabinRow.find('.seatcol-hasbassinet'));
					var p = curBasGrp.length;
					while(p--){
						// Get parallel col group and check if empty
						var pGrp = prevRow.find('[data-colgrp="' + curBasGrp[p].attr('data-colgrp') + '"]');
						if(!pGrp.hasClass('colgrp-noseats') && !pGrp.hasClass('colgrp-nofacil')){
							cabinRow.addClass('seatmap-facility-lift');
						}
					}
				}
			}

			var prevColGrp = prevRow.find('[data-colgrp="' + seatBlk.attr('data-colgrp') + '"]');

			// If current block has bassinet and previous column group can occupy
			if(seatBlk.hasClass('seatcol-hasbassinet')) {
				if(prevColGrp.hasClass('colgrp-noseats') && prevColGrp.hasClass('colgrp-nofacil') && !cabinRow.hasClass('has-nospace')) {
					cabinRow.addClass('has-space-forbass');
				}else {
					cabinRow.addClass('has-nospace');
				}
				// If current column group has bassinet add reference to previous row
				if(prevColGrp.length){
					prevColGrp.addClass('next-colgrp-hasbassinet');
				}
			}

			// reset the row class after adding all seats
			resetRowClass(cabinRow);

			// If row has exit and previous row is empty remove duplicate lifts
			if(cabinRow.hasClass('seat-row-hasexit') && prevRow.hasClass('has-colgrp-noseats') && prevRow.hasClass('has-colgrp-nofacil') && prevRow.hasClass('seatmap-facility-lift')) {
				prevRow.removeClass('seatmap-facility-lift');
			}

			// If row has exit and previous row has empty space for it
			if(cabinRow.hasClass('seat-row-hasexit') && prevRow.hasClass('has-colgrp-noseats') && prevRow.hasClass('has-colgrp-nofacil')) {
				if(prevColGrp.hasClass('colgrp-noseats') && prevColGrp.hasClass('colgrp-nofacil')) {
					if(c === 0 || c === totalGrp-1) {
						// To merge 2 consecutive rows with exits, check if it has the has-transfer-exit class before adding, else just let it be removed
						var prevExit = prevColGrp.find('.seatmap-exit');
						if(!prevColGrp.hasClass('has-transfer-exit')) {
							// Make sure there are no duplicates
							prevExit.remove();
							prevColGrp.append($('<div class="seatmap-exit"><span>Exit</span></div>')).addClass('has-transfer-exit').removeClass('colgrp-nofacil');
						}
					}

					cabinRow.removeClass('seatmap-facility-lift seat-row-hasexit').find('.seatmap-exit').remove();
				}
			}

			// If row contains a seat with a galley inside the column, add seat-inner-galley to adjust the seat row number on cases of seat-inner-bassinet + seat-inner-galley
			if (seatBlk.children('.seatmap-galley').length > 0) {
				cabinRow.addClass('seat-inner-galley');
			}

			// Add empty classes by checking empty rows
			checkForEmpty(seatBlk, cabinRow);
		}

		// If row has bassinet and previous row has transferred facility
		if (cabinRow.hasClass('seat-inner-bassinet') && cabinRow.prev().hasClass('has-transfer-facility') && !cabinRow.hasClass('seatmap-facility-lift')) {
			// Add transfer-facility-lift class
			cabinRow.prev().addClass('seatmap-lift-transfer-facility');
		}

		// Check if first row on rowVO object is a facility
		var firstSeatRowNo = compartment[cabinIndex].startRow;
		if (cabinIndex === 0 && rowNo === firstSeatRowNo) {
			// Check for previous facility and next facility if any
			// Pass cabinRow's wrapper to keep a reference to it, in case the row gets removed from DOM
			checkExternalFacilities(cabinRow, totalGrp, curRowObjIndex, false, cabinRow.parent());
			checkExternalFacilities(cabinRow, totalGrp, curRowObjIndex, true, cabinRow.parent());
		}

		// Check if previous or next row has facility and no seatRowNumber
		if (curRowObjIndex > 0) {
			// Loop for seatRowNumber 0 till you hit the next or last item on rowVO
			// Pass cabinRow's wrapper to keep a reference to it, in case the row gets removed from DOM
			checkExternalFacilities(cabinRow, totalGrp, curRowObjIndex, true, cabinRow.parent());
		}
	};

	var renderSeats = function(rowLabel, rowColDetails, curGroup, rowCont, rowNo, totalGrp) {
		var rcl = rowColDetails.length;
		var rc = rowLabel.length;

		// For each row column label, create a seat from the info
		// If the seat info does not exist create an empty seat
		for (var i = 0; i < rc; i++) {
			// Assign cur label
			var curLabel = rowLabel[i];

			// Find matching label in row from JSON
			var match = false;
			var details;
			for (var j = 0; j < rcl; j++) {
				// current seatOccupationDetail
				var col = rowColDetails[j];

				if (curLabel === col.column) {
					match = true;
					details = col;
					break;
				}
			}

			// If theres a match
			var seat;
			if (match) {
				var occ = 'free';
				if (details.occupied) {
					occ = 'occupied';
				}

				// Create a seat
				var seatLabel = rowNo + details.column;

				if($('body').hasClass('premium-seatmap-page')){
					seat = $('<div data-sia-seat="' + seatLabel + '" class="seat standard-seat seat-' + occ.toLowerCase() + '" data-seat-description="' + details.seatDescription + '"></div>');
				} else {
					seat = $('<div data-sia-seat="' + seatLabel + '" class="seat seat-' + occ.toLowerCase() + '" data-seat-description="' + details.seatDescription + '"></div>');
				}

				seat.attr('data-code', details.characteristics + details.zone);
				var seatEach = seat;
				seatEach.each(function() {
					var selfEl = $(this);
					if($(this).data('code').length){
          	var arrDataCode = $(this).data('code').split(",");
          	for(var i = 0 ; i < arrDataCode.length ; i++){
          		if(arrDataCode[i] === "S"){
          			selfEl.addClass('standard-seat');
          			selfEl.attr('data-background-color', arrDataCode[i]);
          		}
          		if(arrDataCode[i] === "F"){
          			selfEl.addClass('forward-zone');
          			selfEl.attr('data-background-color', arrDataCode[i]);
          		}
          	}

					}
        });

				if (details.seatPrice !== undefined && details.seatPrice !== 'NA' && details.seatPrice !== 'Not available') {
					currentSeatPrice = seat;
					var seatPriceList = details.seatPrice;
					var dataIdNavItem = $('.booking-nav__item.active').data('id');
					for(var pr = 0; pr < seatPriceList.length; pr++) {
						if(seatPriceList[pr].passengerID === dataIdNavItem) {
							seat.attr('data-seat-price',seatPriceList[pr].currency +' '+ seatPriceList[pr].amount);
							seat.attr('data-price', seatPriceList[pr].amount);
							seat.attr('data-seat-price-usd', seatPriceList[pr].amount);
						}
					}
				}
				// Adding USD price attribute
				if (details.seatPrice !== 'NA' && details.seatPrice !== 'Not available' && typeof details.seatUSDPrice !== 'undefined') {
					seat.attr('data-seat-price-usd', details.seatUSDPrice[0]);
				}

				// Get seat type prepared in JSON
				getSeatType(seat, details);

				// Check if its the preselected seat
				var p = preselected.length;
				while (p--) {
					if (preselected[p][0] === seatLabel) {
						seat.addClass('seat-preselected seat-selected').html(preselected[p][2]).data('preselected', preselected[p][2]);

						if (seat.hasClass('seat-char-bassinet')) {
							seat.data('hasbassinet', true);
						}

						break;
					}
				}
			}
			// Else create a blank seat
			else {
				seat = $('<div data-sia-seat="' + (rowNo + curLabel) + '" class="seat seat-empty"></div>');
			}

			// Add classes to float the seats to the edge
			if (curGroup === 0) {
				rowCont.addClass('leftWing');
			}
			if (curGroup === (totalGrp - 1)) {
				rowCont.addClass('rightWing');
			}

			// Append the seat to column group wrapper
			rowCont.append(seat);

			// If seat has exit add class to parent
			if (seat.hasClass('seat-char-hasexit')) {
				rowCont.parent().addClass('seat-row-hasexit');
			}

			// If seat has bassinet add class to parent and previous container
			if (seat.hasClass('seat-char-bassinet')) {
				// rowCont.parent().prev().addClass('seatmap-facility-lift');
				rowCont.parent().addClass('seat-inner-bassinet');
				rowCont.addClass('seatcol-hasbassinet');
			}

			// Check for wings
			if (seat.hasClass('seat-char-wingstart') && (rowCont.hasClass('leftWing') || rowCont.hasClass('rightWing'))) {
				rowCont.addClass('seat-wingstart');
			}

			if (seat.hasClass('seat-char-wingend') && (rowCont.hasClass('leftWing') || rowCont.hasClass('rightWing'))) {
				rowCont.addClass('seat-wingend');
			}

			// After adding to container, check seatCharacteristics so we have a reference of the parent
			if (details) {
				// Check seatCharacteristics since on ICE flow facilities are found in seatCharacteristics
				getSeatCharacteristic(seat, details);

				// Add the seat characteristics as data attribute for updating inputField
				seat.attr('data-seattype', details.seatType);

				// Check for additional alignemnt info
				details.alignemnt && getSeatAlignment(seat, details);
			}

			// If seat is empty
			if (seat.hasClass('seat-empty')) {
				rowCont.addClass('col-has-empty-seat');
				rowCont.parent().addClass('row-has-empty-seat');
			}
		}
	};

	var cabinFacility = function(cabinWrap, rowFacilities, direction) {
		// declare new row for the facility
		var row = $(seatObjects.template.seatRow);
		row.find('span').remove();

		// create the data label for reference

		// Add the row to the current cabin wrapper
		if (direction) {
			cabinWrap.append(row);
		}
		if (!direction) {
			cabinWrap.prepend(row);
		}

		// Get total facilities
		var totalGrp = cabinWrap.data('groupLabels').length;

		// create the column groups and check for facilities
		for (var c = 0; c < totalGrp; c++) {
			var seatBlk = $(seatObjects.template.blk);
			seatBlk.attr('data-colgrp', c);
			seatBlk.attr('data-col', cabinWrap.data('groupLabels')[c].length);

			// Append the column group to the cabin row
			row.append(seatBlk).addClass('seatmap-facility-row');

			// Add aisle
			if (c !== totalGrp - 1) {
				row.append($(seatObjects.template.aisle));
			}

			// Check which location the group is in then render facility
			var facilities;
			if (rowFacilities.length > 0 && typeof cabinWrap.data('groupLabels') !== 'undefined') {
				facilities = getExtRFacility(cabinWrap.data('groupLabels')[c], rowFacilities, c, totalGrp);
			}

			// Check if previous row has empty column
			if (facilities && facilities.length > 0) {
				// seatBlk.html(facilities[0]);
				appendFacility(facilities, seatBlk);
			}
		}
	};

	var checkExternalFacilities = function(cabinRow, totalGrp, curRowObjIndex, direction, cabinWrap) {
		var nextIndex = direction ? curRowObjIndex + 1 : curRowObjIndex - 1;
		var rowObj = row[nextIndex];

		if (typeof rowObj !== 'undefined') {
			if(rowObj.number === 0){
				createFacilityBlk(cabinRow, totalGrp, nextIndex, direction, cabinWrap);
			}
		}
	};

	var createFacilityBlk = function(cabinRow, totalGrp, nextIndex, direction, cabinWrap) {
		// declare new row for the facility
		var row = $(seatObjects.template.seatRow);
		row.find('span').remove();

		// create the data label for reference
		// current label is curRowIndex seat number
		var prevIndex = direction ? nextIndex -1 : nextIndex + 1;
		var crl = row[prevIndex].number;

		// Add row label
		var rowL = direction ? crl + 'b' : crl + 'a';
		row.attr('data-row', rowL).addClass('seatmap-row--empty has-colgrp-noseats');

		// Cache prevRow
		var cwChild = cabinWrap.children();
		var prevRow = direction ? cwChild.eq(cwChild.length-1) : cwChild.eq(0);

		// Add the row to the current cabin container
		// Append to cabinWrap instead of current row in case current row got removed
		if (direction) {
			cabinWrap.append(row);
		}
		if (!direction) {
			cabinWrap.prepend(row);
		}

		// Check if the row needs to lift
		if (!direction && cabinRow.hasClass('seat-inner-bassinet')) {
			cabinRow.addClass('seatmap-facility-lift');
		}

		// create the column groups and check for facilities
		for (var c = 0; c < totalGrp; c++) {
			// Add colgrp-noseats class by default since seatRow:0 has no seats
			var seatBlk = $(seatObjects.template.blk);
			seatBlk.attr('data-colgrp', c).addClass('colgrp-noseats');

			// Cache the current non-0 row's current column
			var curColGroup = cabinRow.children('.seatmap-row-block').eq(c);
			// Add data-col attribute
			var seatCount = curColGroup.attr('data-col');
			if (typeof seatCount !== 'undefined') {
				seatBlk.attr('data-col', seatCount);
			}

			// Append the column group to the cabin row
			row.append(seatBlk).addClass('seatmap-facility-row');

			// Add aisle
			if (c !== totalGrp - 1) {
				row.append($(seatObjects.template.aisle));
			}

			// cache the row information
			var rowFacilities = row[nextIndex].facility;

			// Check which location the group is in then render facility
			var facilities;
			if (rowFacilities.length > 0 && typeof cabinWrap.data('groupLabels') !== 'undefined') {
				// Assign the facility in a variable
				facilities = getExtRFacility(cabinWrap.data('groupLabels')[c], rowFacilities, c, totalGrp);
			}

			// Check if previous row has empty column, shift the facility if so
			var prevEmptyColGrp = prevRow.find('[data-colgrp="'+seatBlk.attr('data-colgrp')+'"]');
			if (typeof facilities !== 'undefined' && facilities.length > 0) {
				// Match current column group with previous column group
				// Check if the previous row has a parent to make sure it still exists
				if(prevRow.hasClass('has-colgrp-noseats') && prevRow.hasClass('has-colgrp-nofacil') && prevEmptyColGrp.hasClass('colgrp-noseats') && prevEmptyColGrp.hasClass('colgrp-nofacil') && !prevEmptyColGrp.hasClass('colgrp-exit')){
					// Add the facility in previous row
					// Remove all previous classes that prevents from adding
					prevEmptyColGrp.html('');
					appendFacility(facilities, prevEmptyColGrp);

					// Add indicator that galley is transfered
					prevRow.addClass('has-transfer-facility');
				}else {
					appendFacility(facilities, seatBlk);
				}

				// Reset the previous row's class
				resetRowClass(prevRow);
			}

			// If row contains a seat with galley inside the column, add seat-inner-galley to adjust the seat row number on cases of seat-inner-bassinet + seat-inner-galley
			if (seatBlk.children('.seatmap-galley').length > 0) {
				row.addClass('seat-inner-galley');
				// Check if previous column block is empty
				// Then shift the galley up
				var prevRowEmptyCol = cabinRow.find('[data-colgrp="'+seatBlk.attr('data-colgrp')+'"]');
				if(cabinRow.hasClass('has-colgrp-noseats') && prevRowEmptyCol.hasClass('colgrp-noseats') && !prevRowEmptyCol.hasClass('colgrp-exit')){
					// Add indicator that galley is transfered
					cabinRow.addClass('has-transfer-facility');
				}
			}

			// If seatRow:0 has exit, add indicator class on the row
			if (seatBlk.hasClass('colgrp-exit')) {
				row.addClass('has-colgrp-exit');
			}

			// Add empty classes by checking empty rows
			checkForEmpty(seatBlk, row);
		}

		// If previous row has empty seat add class to the row, in case bassinet needs to adjust
		if (prevRow.hasClass('row-has-empty-seat')) {
			var emptyCol = prevRow.find('.col-has-empty-seat');

			if (emptyCol.length > 1) {
				cabinRow.find('[data-colgrp="' + emptyCol.attr('data-colgrp') + '"]').addClass('galley-less-' + emptyCol.length);
			} else {
				cabinRow.find('[data-colgrp="' + emptyCol.attr('data-colgrp') + '"]').addClass('galley-less');
			}
		}

		// After adding facilities and checking for seats, check if the row is empty
		// Start check with rows with empty seat
		if(row.hasClass('has-colgrp-noseats') && row.hasClass('has-colgrp-nofacil') && !row.hasClass('has-colgrp-exit')) {
			var empty = checkRowEmpty(row);
			if(empty) {
				row.remove();
			}else {
				// Add the row to the array for reference
				rows.push(row);
			}
		}

		// If previous row has exit and current row already has exit, remove current row so it does not conflict with current lifts
		if(row.hasClass('has-colgrp-exit') && prevRow.hasClass('seat-row-hasexit')){
			row.remove();
		}

		// Check again for the next row with seatRow: 0
		var nIndex = direction ? (nextIndex + 1) : (nextIndex - 1);
		var rowObj;
		rowObj = row[nIndex];

		if (typeof rowObj !== 'undefined') {
			if(rowObj.number === 0){
				createFacilityBlk(row, totalGrp, nIndex, direction, cabinWrap);
			}
		}
	};

	var renderFacilities = function(curGroup, totalGrp, groupCont, rowFacilities) {
		var l = rowFacilities.length;
		// Check if trowfacilites have data
		if (l > 0) {
			// get center group
			var mid = Math.floor(totalGrp / 2);

			// Create array container for the facilities, this will be used to compress the facilities into 1 item
			var facilities = [];

			// Cache cabin wrapper labels
			var cabinWrapLbl = groupCont.parent().parent().data('groupLabels');

			// Iterate through the facilities to filter where the facility should go
			for (var i = 0; i < l; i++) {
				// Cache if has handicap
				var hf = (typeof rowFacilities[i].handicapSymbol !== 'undefined') && rowFacilities[i].handicapSymbol === 'true';
				// Cache the type of facility
				var tof = hf ? 'LAH' : rowFacilities[i].type;
				// cache location of facility
				var lof = rowFacilities[i].location;

				// Cache boolean for facilityFormat
				var hasFacFormat = (typeof rowFacilities[i].facilityFormat !== 'undefined' && rowFacilities[0].facilityFormat === 'COLUMN');
				// Cache cabin wrappers' column labels
				var curLbls = cabinWrapLbl[curGroup];

				// Check if facility format is present
				// If not do usual conditionals
				if(hasFacFormat) {
					// Loop through current labels
					for (var j = curLbls.length - 1; j >= 0; j--) {
						// If label matches location of facility then add the facility
						if(curLbls[j] === lof) {
							addValue(!hf, facilities, getFacility(tof));
						}
					}
				}else {
					var fac = getFacility(tof);
					if ((curGroup === 0) && (lof === 'L') && fac !== '') {
						addValue(!hf, facilities, fac);
					}
					if ((curGroup === 0) && (lof === 'A') && fac !== '') {
						addValue(!hf, facilities, fac);
					}
					if (curGroup === (totalGrp - 1) && lof === 'R' && fac !== '') {
						addValue(!hf, facilities, fac);
					}
					if (curGroup === mid && lof === 'C' && fac !== '') {
						addValue(!hf, facilities, fac);
					}
					if (curGroup === mid && lof === 'LC' && fac !== '') {
						addValue(!hf, facilities, fac);
					}
					if (curGroup === mid && lof === 'RC' && fac !== '') {
						addValue(!hf, facilities, fac);
					}
				}
			}

			appendFacility(facilities, groupCont);
			if(groupCont.find('.has-handicap').length) {
				groupCont.attr('data-handicap', 'true');
			}

		} else {
			return;
		}
	};

	var addValue = function(end, arr, val){
		if(end) {
			arr.push(val);
		}else {
			arr.unshift(val);
		}
	};

	var appendFacility = function(facArr, cont){
		// Check unique facilities then add to container
		var uniQueFacs = [];
		var l = facArr.length;

		// Loop through facility array
		for(var i = 0; i < l; i++) {
			var c = uniQueFacs.length;
			var unique = true;
			var f = facArr[i];

			if(c > 0) {
				// Iterate through uniQueFacs array
				while (c--) {
					// check if current value is already in the unique
					if(f === uniQueFacs[c]) {
						unique = false;
						break;
					}
				}

				if(unique) {
					uniQueFacs.push(f);
					cont.append(f);
					if($(f).hasClass('seatmap-exit')){
						cont.addClass('colgrp-exit');
					}
				}
			}else {
				uniQueFacs.push(f);
				cont.append(f);
				if($(f).hasClass('seatmap-exit')){
					cont.addClass('colgrp-exit');
				}
			}
		}

		// Merge lavatories if 1 is normal and 1 has handicap
		var la = cont.find('.icon-only');
		var lah = cont.find('.has-handicap');
		if(la.length > 0 && lah.length > 0){
			la.eq(0).remove();
		}
	};

	var renderWings = function(rws, rwe){
		// get deck nav offset
		var dno = 0;
		if(deckNav.hasClass('on')) {
			dno = deckNav.outerHeight() + 5;
		}

		if (rws.length) {
			var wingStart = $(seatObjects.template.wingStart);

			// Deduct the height of the wing container which is 118
			wingStart.css({
				'top': rws.offset().top - mainDeck.offset().top - 118 + dno
			});

			wingCont.append(wingStart);
		}

		if (rwe.length) {
			var wingEnd = $(seatObjects.template.wingEnd);
			// deduct 10px as the offset
			wingEnd.css({
				'height': mainDeck.offset().top + mainDeck.height() - rwe.offset().top - 10
			});

			wingCont.append(wingEnd);
		}
	};

	var initDeckNav = function() {
		// assign the 2 buttons on a variable
		var mNav = deckNav.find('.tab-item').eq(0);
		var uNav = deckNav.find('.tab-item').eq(1);
		var popupSeatSelect = $('[data-infomations-1]');
		var popupSeatChange = $('[data-infomations-2]');


		// Add listeners
		uNav.find('a').on({
			'click': function(e) {
				e.preventDefault();
				e.stopPropagation();
				popupSeatSelect.find('.tooltip__close').trigger('click');
				popupSeatChange.find('.tooltip__close').trigger('click');
				var t = $(this);
				if (!t.parent().hasClass('active')) {
					t.parent().addClass('active');
					mNav.removeClass('active');

					deckIn(upperDeck, false);
					deckOut(mainDeck, true);

					wingCont.hide();
					equipCont.addClass('hidden');
					equipContUD.removeClass('hidden');
				}
			}
		});

		mNav.find('a').on({
			'click': function(e) {
				e.preventDefault();
				e.stopPropagation();
				popupSeatSelect.find('.tooltip__close').trigger('click');
				popupSeatChange.find('.tooltip__close').trigger('click');
				var t = $(this);
				if (!t.parent().hasClass('active')) {
					t.parent().addClass('active');
					uNav.removeClass('active');

					deckIn(mainDeck, true);
					deckOut(upperDeck, false);

					setTimeout(function() {
						wingCont.show();
						equipContUD.addClass('hidden');
						equipCont.removeClass('hidden');
					}, 300);
				}
			}
		});

		// Show the deck navigation
		deckNav.addClass('on');
		mNav.addClass('active');
	};

	// var deckIn = function(deck, frmLeft) {
	var deckIn = function(deck) {
		// var animation = frmLeft ? 'fadeIn' : 'fadeInRight';

		deck.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$(this).off().removeClass('animated fadeIn').addClass('seat-deck active');
			seatsCont.removeAttr('style');
		});

		setTimeout(function() {
			deck.addClass('animated active fadeIn');
		}, 300);
	};

	// var deckOut = function(deck, toLeft) {
	var deckOut = function(deck) {
		deck.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$(this).off().removeClass('animated active fadeOut').addClass('seat-deck');
		});

		deck.addClass('animated fadeOut');
	};

	// Getters
	var getRow = function(rn) {
		for (var i = row.length - 1; i >= 0; i--) {
			// Search for row number match
			if (row[i].number === rn) {
				return [row[i], i];
			}
		}
	};

	var getExtRFacility = function(cabinLabelArr, rowFacilities, curGroup, totalGrp) {
		var facilities = [];
		// get center group
		var mid = Math.floor(totalGrp / 2);

		// Cache if has facility format in rowFacilities
		for (var i = rowFacilities.length - 1; i >= 0; i--) {
			// Cache current rowFacility
			var curRowFac = rowFacilities[i];

			// Check if the facilityFormat for current item is defined
			if(typeof curRowFac.facilityFormat !== 'undefined' && curRowFac.facilityFormat === 'COLUMN') {
				// Loop through each cabin letters in cabinLabelArr
				for (var j = cabinLabelArr.length - 1; j >= 0; j--) {
					// Check if the labels match
					if (cabinLabelArr[j] === curRowFac.column) {
						// Cache if has handicap
						var hf = (typeof curRowFac.handicapSymbol !== 'undefined') && curRowFac.handicapSymbol === 'true';
						// Cache the type of facility
						var tof = hf ? 'LAH' : curRowFac.type;
						var f = getFacility(tof);
						if (f!==''){
							facilities.push(getFacility(tof));
						}
					}
				}
			}else {
				var hf = (typeof curRowFac.handicapSymbol !== 'undefined') && curRowFac.handicapSymbol === 'true';
					// Cache the type of facility
				var tof = hf ? 'LAH' : rowFacilities[i].type;
				var lof = rowFacilities[i].column;
				var fac = getFacility(tof);

				if ((curGroup === 0) && (lof === 'L') && fac !== '') {
					facilities.push(fac);
				}
				if ((curGroup === 0) && (lof === 'A') && fac !== '') {
					facilities.push(fac);
				}
				if (curGroup === (totalGrp - 1) && lof === 'R' && fac !== '') {
					facilities.push(fac);
				}
				if (curGroup === mid && lof === 'C' && fac !== '') {
					facilities.push(fac);
				}
				if (curGroup === mid && lof === 'LC' && fac !== '') {
					facilities.push(fac);
				}
				if (curGroup === mid && lof === 'RC' && fac !== '') {
					facilities.push(fac);
				}
				if ((curGroup === 0 || curGroup === (totalGrp - 1)) && lof === 'NA' && tof === 'E' && fac !== '') {
					facilities.push(fac);
				}
			}
		}

		// Return facilities for current column group
		return facilities;
	};

	var getSeatType = function(seat, col) {
		var lengthCharacteristics;
		if(col.zone !== undefined) {
			lengthCharacteristics = col.characteristics + col.zone;
		} else {
			lengthCharacteristics = col.characteristics;
		}
		for(var i = 0; i < lengthCharacteristics.length; i++){
			switch (lengthCharacteristics[i].toLowerCase()) {
				case 'p':
					seat.addClass('seat-char-preferred').attr('data-preferred', 'P');
					break;
				case 'k':
					seat.addClass('seat-char-windowless');
					break;
				case 'b':
					seat.addClass('seat-char-bassinet');
					break;
				case 'e':
					seat.addClass('seat-char-hasexit');
					break;
				case 'w':
					seat.addClass('seat-char-window');
					break;
				case 'a':
					seat.addClass('seat-char-aisle');
					break;
				case 'h':
					seat.addClass('seat-char-handicap');
					break;
				case 's':
					seat.addClass('standard-seat');
					break;
				case 'f':
					seat.addClass('forward-zone');
					break;
				case 'c':
					seat.addClass('seat-char-chargeable');
					break;
			}
		}
	};

	var getSeatAlignment = function(seat, col) {
		switch (col.alignment.toLowerCase()) {
			case 'r':
				seat.parent().addClass('al-r');
				break;
			case 'l':
				seat.parent().addClass('al-l');
				break;
			case 'c':
				seat.parent().addClass('al-c');
				break;
			case 'cr':
				seat.parent().addClass('al-x');
				break;
		}
	};

	var getSeatCharacteristic = function(seat, schar) {
		for(var i = 0; i < schar.characteristics.length; i++) {
			switch (schar.characteristics[i]) {
				case 'GN':
					seat.parent().attr('data-replace', 'G');
					seat.removeClass().addClass('seat seat-free');

					break;

				case 'LA':
					if(seat.parent().attr('data-handicap') === 'true') {
						seat.parent().attr('data-replace', 'LAH');
					}else {
						seat.parent().attr('data-replace', 'LA');
					}
					seat.removeClass().addClass('seat seat-free');

					break;

				case 'LAE':
					if(seat.parent().attr('data-handicap') === 'true') {
						seat.parent().attr('data-replace', 'LAH');
					}else {
						seat.parent().attr('data-replace', 'LAE');
					}
					seat.removeClass().addClass('seat seat-free');

					break;

				case '8':
					seat.removeClass().addClass('seat seat-empty');

					break;

				case 'OW':
					seat.parent().parent().addClass('seat-overwing');

					break;
			}
		}
	};

	var getFacility = function(fc) {
		var t;

		switch (fc) {
			case 'LA':
				t = '<div class="seatmap-galley icon-only"><em class="ico-1-toilet"></em></div>';
				break;
			case 'LAE':
				t = '<div class="seatmap-galley"><em class="ico-1-toilet"></em><em class="ico-1-assistance"></em></div>';
				break;
			case 'LAH':
				t = '<div class="seatmap-galley has-handicap"><em class="ico-1-toilet"></em><em class="ico-1-assistance"></em></div>';
				break;
			case 'G':
				t = '<div class="seatmap-galley">Galley</div>';
				break;
			case 'GN':
				t = '<div class="seatmap-galley">Galley</div>';
				break;
			case 'E':
				t = '<div class="seatmap-exit"><span>Exit</span></div>';
				break;
			case 'D':
				t = '<div class="seatmap-exit left"><span>Exit</span></div>';
				break;
			default:
				t = '';
				break;
		}

		return t;
	};

	var genericSeatMap = function() {
		var bookingNavItem = $('.booking-nav__item');
		var holderActive = bookingNavItem.filter('.active').index();
		var passengerDropdowns = $('.seatmap__select[data-pax]');
		var seatMapNonClickable = $('.seatmap-content');
		var selectNextPass = function(el){
			if(body.hasClass('f-loop')){
				if(!withFLoop()){
					if(el.length){
						el.trigger('click.togglePaxDropdown');
					}
					else{
						bookingNavItemWithoutInfant.eq(0).trigger('click.togglePaxDropdown');
					}
				}
			}
			else{
				if(!withoutFLoop()){
					if(el.length){
						el.trigger('click.togglePaxDropdown');
					}
					else{
						bookingNavItemWithoutInfant.eq(0).trigger('click.togglePaxDropdown');
					}
				}
			}
		};

		var withoutFLoop = function(){
			var all = true;
			for (var i = 0; i < bookingNavItemWithoutInfant.length; i++) {
				if (!bookingNavItemWithoutInfant.eq(i).find('.passenger-detail__seat').length) {
					all = false;
					return all;
				}
			}
			return all;
		};

		var withFLoop = function(){
			var all = true;
			for (var i = 0; i < bookingNavItemWithoutInfant.length; i++) {
				if (!bookingNavItemWithoutInfant.eq(i).hasClass('chosen')) {
					all = false;
					return all;
				}
			}
			return all;
		};

		//detect assigned passenger to remove dropdown
		var detectAssignedPass = function(){
			for (var i = 0; i < bookingNavItem.length; i++) {
				if (bookingNavItem.eq(i).find('.passenger-detail__seat').length) {
					if(bookingNavItem.eq(i).find('.passenger-detail__seat').html() !=='W' && bookingNavItem.eq(i).find('.passenger-detail__seat').html() !=='A'){
						bookingNavItem.eq(i).addClass('ignore');
						passengerDropdowns.eq(i).find('[data-customselect]').addClass('disabled');
					}
					else{
						passengerDropdowns.eq(i).find('select').val(bookingNavItem.eq(i).find('.passenger-detail__seat').html());
						passengerDropdowns.eq(i).find('[data-customselect]').removeClass('default');
					}
				}
			}
		};

		if (passengerDropdowns.length) {
			bookingNavItem
				.off('click.togglePaxDropdown')
				.on('click.togglePaxDropdown', function(e) {
					e.preventDefault();
					bookingNavItem.removeClass('active');
					var index = bookingNavItem.index($(this).addClass('active'));
					passengerDropdowns.addClass('hidden-dt').eq(index).removeClass('hidden-dt');
				});
			detectAssignedPass();
		}

		var bookingNavItemWithoutInfant = bookingNavItem.filter(function(){
			return !$(this).find('.passenger-info__text > span').length && !$(this).hasClass('ignore');
		});

		var passengerDropdownsWithoutInfant = passengerDropdowns.filter(function(i){
			return !bookingNavItem.eq(i).find('.passenger-info__text > span').length && !bookingNavItem.eq(i).hasClass('ignore');
		});


		passengerDropdownsWithoutInfant.each(function(i, it) {
			var dropdown = $(it);
			dropdown
				.find('[data-customselect]')
				.off('change.changeSeatPosition')
				.on('change.changeSeatPosition', function(e, isTrigger) {
					var selectedValue = $(this).find('select').val();
					var seat = bookingNavItemWithoutInfant.eq(i).children('.passenger-detail__seat');
					var navPass = bookingNavItemWithoutInfant.eq(i);
					navPass.find('.passenger-info input').val(selectedValue);
					if(!isTrigger){
						navPass.addClass('chosen');
					}
					if(selectedValue.toLowerCase() !== '') {
						if (seat.length) {
							seat.text(selectedValue);
						} else {
							navPass.append('<span class="passenger-detail__seat">' + selectedValue + '</span>');
						}
					} else {
						if(navPass.find('.passenger-detail__seat').html() === 'W' || navPass.find('.passenger-detail__seat').html() === 'A'){
							navPass.find('.passenger-detail__seat').remove();
							navPass.removeClass('chosen');
						}
					}
					selectNextPass(bookingNavItemWithoutInfant.eq(i + 1));
				})
				.trigger('change.changeSeatPosition', [true]);
		});

		bookingNavItem.eq(holderActive).trigger('click.togglePaxDropdown');

		if (seatMapNonClickable.is('[data-non-clickable]')) {
			seatMapNonClickable.undelegate('.ico-seat', 'click.selectSeat');
		}
	};

	var startGenericRender = function() {
		// Add class to style generic seatmap
		seatContainer.addClass('seatmap--generic');

		// Init the message
		seatContainer.append($(seatObjects.template.genericMessage));

		// Hide the equipment container
		equipCont.remove();

		// Hide the seat map container
		seatsCont.hide();

		// Print out all details
		tabInfo();
		paxDetails();

		// Update hidden input fields
		updateInputFields();

		// populate select fields for generic seatmap
		var paxAr = globalJson.seatMap.passenger;
		var start = globalJson.seatMap.seatMapVO.passengerStartingPoint;
		var paxSelects = [];

		var l = paxAr.length;

		for (var i = 0; i < l; i++) {
			// Cache var
			var po = paxAr[i];
			var wChild = [];

			// Check if its the active passenger
			var hidden = (i + 1) === parseInt(start) ? '' : ' hidden-dt';

			// Check if infant then get adult partner
			if (po.passengerType.toLowerCase() === 'infant') {
				var ad;

				for (var j = l - 1; j >= 0; j--) {
					if (paxAr[j].passengerID === po.passengerID) {
						ad = paxAr[j];
						wChild.push(j);

						break;
					}
				}

				var seatNo;

				if (po.seatNumber.toLowerCase() !== 'not available') {
					seatNo = po.seatNumber;
					if (seatNo.charAt(0) === '0') {
						seatNo = seatNo.slice(1);
					}

					preselected.push([seatNo, po.passengerID, paxSelects.length + 1]);
					preselAttr.push(seatNo);

					// Add form object to POST on form submit. eg:'paxOld1-31A'
					seatForm.prepend($('<input type="hidden" name="paxOld[]" value="paxOld' + po.passengerID + '-' + seatNo + '" data-paxindex="' + paxSelects.length + '">'));
				}

				if (ad.seatNumber.toLowerCase() !== 'not available') {
					seatNo = ad.seatNumber;
					if (seatNo.charAt(0) === '0') {
						seatNo = seatNo.slice(1);
					}

					preselected.push([seatNo, po.passengerID, paxSelects.length + 1]);

					// Add form object to POST on form submit. eg:'paxOld1-31A'
					seatForm.prepend($('<input type="hidden" name="paxOld[]" value="paxOld' + po.passengerID + '-' + seatNo + '" data-paxindex="' + paxSelects.length + '">'));
				}

				if (ad.seatNumber.toLowerCase() === 'not available' || po.seatNumber.toLowerCase() === 'not available') {
					// Add form object to POST on form submit. eg:'paxOld1-31A'
					seatForm.prepend($('<input type="hidden" name="paxOld[]" value="paxOld' + po.passengerID + '-NA" data-paxindex="' + paxSelects.length + '">'));
				}

				var aSelected = '',
				wSelected = '',
				sSelected = '';

				if(typeof seatNo === 'undefined') {
					seatNo = 'NA';
				}

				if(seatNo.toLowerCase() === 'A' || seatNo.toLowerCase() === 'aisle') {
					aSelected = ' selected="selected"';
				}else if(seatNo.toLowerCase() === 'W' || seatNo.toLowerCase() === 'window') {
					wSelected = ' selected="selected"';
				}else {
					sSelected = ' selected="selected"';
				}

				var adultName = ad.lastName.length > 0 ? ad.title + ' ' + ad.lastName + ' ' + ad.firstName : 'Passenger ' + (paxSelects.length + 1) + ' - ' + ad.passengerType;
				var infantName = po.lastName.length > 0 ? po.title + ' ' + po.lastName + ' ' + po.firstName + '<span>&nbsp;-&nbsp;Infant</span>' : 'Passenger ' + (paxSelects.length + 1) + ' - ' + po.passengerType;

				var paxSelect;

				if(wChild.length > 0) {
					paxSelect = $('<div data-pax="' + paxSelects.length + '" class="seatmap__select' + hidden + '"><label for="preferred-seat-' + paxSelects.length + '" class="seatmap__select-label hidden-dt">' + (paxSelects.length + 1) + '. ' + adultName + ' <br/> ' + infantName + '</label><div class="alert-block checkin-alert error-message"><div class="inner"><div class="alert__icon"><em class="ico-alert"></em></div><div class="alert__message"><p>An adult passenger with an infant cannot select a specific seat type on this flight. For assistance, get in touch with the respective partner airline.</p></div></div></div></div>');
				}else {
					paxSelect = $('<div data-pax="' + paxSelects.length + '" class="seatmap__select' + hidden + '"><label for="preferred-seat-' + paxSelects.length + '" class="seatmap__select-label hidden-dt">' + (paxSelects.length + 1) + '. ' + adultName + ' <br/> ' + infantName + '</label><div data-customselect="true" class="custom-select custom-select--2 custom-select--seat default"><label for="preferred-seat" class="select__label">Seat preference</label><span class="select__text">Window (W)</span><span class="ico-dropdown">Window (W)</span><select id="preferred-seat-' + paxSelects.length + '" name="preferred-seat"><option value=""'+sSelected+'>Select</option><option value="W"'+wSelected+'>Window (W)</option><option value="A"'+aSelected+'>Aisle (A)</option></select></div></div>');
				}

				paxSelects.push(paxSelect);

				// Add to the popup used on tablet and mobile
			} else {
				if (!_.isEmpty(po.infant)) {
					continue;
				}

				var seatNo;
				if (po.seatNumber.toLowerCase() !== 'not available') {
					seatNo = po.seatNumber;
					if (seatNo.charAt(0) === '0') {
						seatNo = seatNo.slice(1);
					}

				} else {
					seatNo = 'NA';
				}

				var aSelected = '',
				wSelected = '',
				sSelected = '';

				if(seatNo.toLowerCase() === 'A' || seatNo.toLowerCase() === 'aisle') {
					aSelected = ' selected="selected"';
				}else if(seatNo.toLowerCase() === 'W' || seatNo.toLowerCase() === 'window') {
					wSelected = ' selected="selected"';
				}else {
					sSelected = ' selected="selected"';
				}

				var child = po.passengerType.toLowerCase() === 'child' ? '&nbsp;-&nbsp;Child' : '';

				var paxName = po.lastName.length > 0 ? po.title + ' ' + po.lastName + ' ' + po.firstName : 'Passenger ' + (paxSelects.length + 1) + ' - ' + po.passengerType;

				var paxSelect = $('<div data-pax="' + paxSelects.length + '" class="seatmap__select' + hidden + '"><label for="preferred-seat-' + paxSelects.length + '" class="seatmap__select-label hidden-dt">' + (paxSelects.length + 1) + '. ' + paxName + child + '</label><div data-customselect="true" class="custom-select custom-select--2 custom-select--seat default"><label for="preferred-seat" class="select__label">Seat preference</label><span class="select__text">Window (W)</span><span class="ico-dropdown">Window (W)</span><select id="preferred-seat-' + paxSelects.length + '" name="preferred-seat"><option value=""'+sSelected+'>Select</option><option value="W"'+wSelected+'>Window (W)</option><option value="A"'+aSelected+'>Aisle (A)</option></select></div></div>');

				paxSelects.push(paxSelect);

				// Add form object to POST on form submit. eg:'paxOld1-31A'
				seatForm.prepend($('<input type="hidden" name="paxOld[]" value="paxOld' + po.passengerID + '-' + seatNo + '" data-paxindex="' + paxSelect.length + '">'));
			}
		}

		// Add passengers to the DOM
		var pl = paxSelects.length;

		seatContainer.append($(seatObjects.template.genericSeats));
		for (var i = 0; i < pl; i++) {
			seatContainer.append(paxSelects[i]);
		}


		// Init the select functionality
		genericSeatMap();
	};

	var startRender = function() {
		// Cache values
		compartment = globalJson.seatMap.compartment;
		row = globalJson.seatMap.row;
		flight = globalJson.seatMap.flight;
		// Cache length
		cl = compartment.length;
		rl = row.length;

		// Print out all details
		tabInfo();
		paxDetails();

		// Update hidden input fields
		updateInputFields();

		// Start seat rendering
		renderMap();
	};

	// Conventions
	var jqEachToArray = function(jqobj){
		var arr = [];
		jqobj.each(function(){
			arr.push($(this));
		});

		return arr;
	};

	var checkForEmpty = function(colGroup, row){
		// Check if current colGroup has children after checking all attributes
		if(colGroup.children('.seat.seat-empty').length && colGroup.children('.seat.seat-empty').length === colGroup.children().length) {
			colGroup.addClass('colgrp-noseats');
			row.addClass('has-colgrp-noseats');
		}

		// Check if colGroup has no facility
		if(colGroup.children().length === 0 || colGroup.find('.seatmap-galley').length === 0) {
			colGroup.addClass('colgrp-nofacil');
			row.addClass('has-colgrp-nofacil').removeClass('seatmap-facility-row seat-inner-galley');

		}else {
			// Ensure proper classes are in
			row.addClass('seat-inner-galley');
			colGroup.removeClass('colgrp-nofacil');
		}
	};

	var resetRowClass = function(row){
		// After removing/adding classes of colGrp, check rowClasses
		var colGrps = row.find('.seatmap-row-block');
		var e = 0, f = 0, s = 0;
		var prevRow = row.prev();
		var bassinetHit = false;

		colGrps.each(function(){
			var t = $(this);

			// Check for seats
			var seats = t.find('.seat');
			var eseats = t.find('.seat.seat-empty');
			if(seats.length === eseats.length || seats.length < 1){
				t.addClass('colgrp-noseats');
				row.addClass('has-colgrp-noseats');
			}
			if(seats.length === 0){
				t.removeClass('colgrp-noseats');
				// Increment seat column counter
				s++;
			}

			var facilities = t.find('.seatmap-galley');
			if(facilities.length > 0){
				t.removeClass('colgrp-nofacil');
				// Increment facility column counter
				f++;
			}else{
				// If there is atleast 1 galley
				t.addClass('colgrp-nofacil');
				row.addClass('has-colgrp-nofacil seat-inner-galley');
			}

			var exits = t.find('.seatmap-exit');
			if(exits.length > 0){
				t.addClass('colgrp-exit');
				row.addClass('has-colgrp-exit');
			}else {
				t.removeClass('colgrp-exit');
				// Increment exit column counter
				e++;
			}

			// Check for bassinet placements and see if it hits any galley from previous
			var prevColGrp = prevRow.find('[data-colgrp="' + t.attr('data-colgrp') + '"]');
			if((t.hasClass('seatcol-hasbassinet') && !prevColGrp.hasClass('colgrp-nofacil')) || (t.hasClass('seatcol-hasbassinet') && !prevColGrp.hasClass('colgrp-noseats'))) {
				bassinetHit = true;
				t.addClass('bassinet-hit');
			}

			// If current block has bassinet and previous block has facility within a row with bassinet-hit, add class to give way to next galley because previous galley will align top
			var prevHasHit = (t.hasClass('seatcol-hasbassinet') && !prevColGrp.hasClass('colgrp-nofacil')) || (t.hasClass('seatcol-hasbassinet') && !prevColGrp.hasClass('colgrp-noseats'));
			if(prevHasHit && prevRow.hasClass('has-bassinet-hit')) {
				t.addClass('prev-col-has-bassinet-hit');
			}
		});

		if(e === colGrps.length){
			row.removeClass('has-colgrp-exit');
		}
		if(f === colGrps.length){
			row.removeClass('has-colgrp-nofacil');
		}
		if(s === colGrps.length){
			row.removeClass('has-colgrp-noseats');
		}

		if(bassinetHit && row.hasClass('seat-inner-bassinet')) {
			row.removeClass('has-space-forbass').addClass('has-bassinet-hit');
		}else{
			row.addClass('has-space-forbass').removeClass('has-bassinet-hit');
		}
	};

	var checkRowEmpty = function(row){
		var empty = true;

		// If at least one seat is found, row is not empty
		var noseat = true;
		row.find('.seat').each(function(){
			if(!$(this).hasClass('seat-empty')){
				noseat = false;
			}
		});

		// If at least 1 facility is found, row is not empty
		var nofac = true;
		if(row.find('.seatmap-galley').length > 0){
			nofac = false;
		}

		var noexit = true;
		if(row.find('.seatmap-exit').length > 0){
			noexit = false;
		}

		empty = noseat && nofac && noexit ? true : false;

		return empty;
	};

	var addClassForSeatSelection = function() {
		var tableSeatSelection = $('.table-seat-selection'),
				tableData = tableSeatSelection.find('tbody td:not(:first-child) span');
		tableData.each(function() {
			var _this = $(this),
					thisVal = _this.text();
			if(thisVal === 'Complimentary') {
				_this.addClass('complimentary');
			} else if (thisVal === 'Not allowed') {
				_this.addClass('not-allowed');
			} else {
				_this.addClass('fare-price');
			}
		});
	};

	var init = function(nextJson) {
		// Parse attached JSON in body
		var el = document.getElementById('seatMap');

		if ($(el).length) {
			var elBody = $.trim(el.innerHTML);
			var s = elBody.substr(2, (elBody.length - 4));

			SIA.ParseJSON(s);

			globalJson.seatMap = nextJson ? nextJson : mergeJson(globalJson.seatMap);

			// Check if its generic seatmap
			if (globalJson.seatMap.errorVO.errocode === 'ERROR_seat.selection.generic') {
				startGenericRender();
			} else {
				// Start normal render
				startRender();
			}

			$(document).find('a.booking-nav__item').on('click', function(){
				if (listCol.seatPrice !== 'NA' && listCol.seatPrice !== 'Not available') {
					var seatPriceCol = listCol.seatPrice;
					var dataIdNavItem = $('.booking-nav__item.active').data('id');
					for(var pr = 0; pr < seatPriceCol.length; pr++) {
						if(seatPriceCol[pr].passengerID === dataIdNavItem) {
							currentSeatPrice.attr('data-seat-price',seatPriceCol[pr].amount +' '+ seatPriceCol[pr].currency);
							currentSeatPrice.attr('data-price', seatPriceCol[pr].amount);
						}
					}
				}
			});

			SIA.SeatSelection.init();
			seatMapJson = globalJson.seatMap;
			setTimeout(function(){
				if($('.travel-party-wrapper').length){
					seatMapTravelPartyTpl(seatMapJson);
				}
				if($('.accordion-seat-selection-wrapper').length && $('body').hasClass('economy-seatmap-page')){
					seatMapSeatSelectionTpl(seatMapJson);
				}
				if($('.seat-legend').length){
					seatMapNoteTpl(seatMapJson);
				}
			}, 300);
		}

		// SIA.preloader.hide();
		$('.overlay-loading ').hide();

		var flightSelectedIdx = $('[data-click-through]').find('.tab-item.active').index();
			var tabFlights = $('[data-click-through]').find('.tab-item');

			for(var i = flightSelectedIdx + 1; i < tabFlights.length; i++) {
				if(!tabFlights.eq(i).is('.disabled')) {
					$('input[name="seat-3-submit"]').attr('data-nextflight', tabFlights.eq(i).data('nextflight'));
					break;
				}
			}
	};

	var oRenderer = {
		init: init,
		startRender: startRender,
		preselected: preselAttr,
		seatObjects: seatObjects,
		seatsCont: seatsCont,
		paxGroups: paxGroups,
		deckClass: deckClass
	};

	return oRenderer;
})();

SIA.ParseJSON = function(str) {
	eval(str);
};


/**
 * @name SIA
 * @description Define global seatMap functions
 * @version 1.0
 */

SIA.SeatSelection = (function() {
	// Globals
	var global,
		win;
	var body = SIA.global.vars.body;

	// Seat selection defaults
	var seatsCont, seatItems, templateInforSeat;

	var occupiedClass = 'seat-occupied';
	var blockedClass = 'ico-seat-blocked';
	var preselected = 'seat-preselected';
	// var emptyClass = 'ico-seat-empty';
	var emptyClass = 'seat-empty';
	var avaibleSeatClass = 'seat-free';
	var forwardZoneClass = 'forward-zone';
	var standardSeat = 'standard-seat';
	var selectedClass = 'seat-selected';
	var blankSeat = 'blank-seat';
	var deselectOn = 'deselect-on';
	var preferredSeatClass = 'seat-char-preferred';
	var timerPopup = null;
	var tablet = 988;
	var templateInforSeat = $(SIA.RenderSeat.seatObjects.template.inforSeat);

	var BSP = $('.booking-summary');
	// var triggerOpenBS = BSP.find('.booking-summary__control');
	var fare = BSP.find('[data-fare]');
	var headtotal = BSP.find('[data-headtotal]');
	var grandtotal = BSP.find('[data-tobepaid]');
	var templateBSPPreferredSeat = '<li><span>{0}</span><span class="price">{1}</span></li>';
	var item,
		getTotal;
	var texttemplatePrefer, texttemplateFeePrefer, texttemplateStandard, texttemplateForward;
	var currency = null;
	var priceCookie = {};

	var unformatNumber = function(number) {
		var unformat = window.accounting.unformat(number);
		return parseFloat(unformat);
	};

	var formatNumber = function(number, decimal) {
		if(decimal || decimal===0){
			return globalJson.seatMap.locale === 'de_DE' ? window.accounting.formatNumber(number, decimal, '.', ',') : window.accounting.formatNumber(number, decimal, ',', '.');
		}
		else{
			return globalJson.seatMap.locale === 'de_DE' ? window.accounting.formatNumber(number, 2, '.', ',') : window.accounting.formatNumber(number, 2, ',', '.');
		}
	};

	var precision = {
		currency: {
			'SGD':2,
			'JPY':0,
			'TWD':0,
			'HKD':0,
			'KRW':0,
			'INR':0,
			'KWD':3,
			'DKK':0,
			'CNY':0,
			'THB':0,
			'LKR':0,
			'ZAR':2,
			'SEK':0,
			'NOK':0,
			'AED':0,
			'SAR':0,
			'BND':0,
			'MYR':0
		},
		getPrecision: function(currency) {
			return this.currency[$.trim(currency)];
		}
	};

	var getCurrency = function(number) {
		return number.replace(/[\d*\.\,]/g,'');
	};


	var onSelected = function() {};
	var onUnSelected = function() {};
	var afterChoose = function() {};
	// var tooltip = false;

	var popupSeatSelect,
		popupSeatChange,
		popupSeatSelectRadio,
		peopleList,
		seat;

	var indexSeat = 0;

	var arrSeat = [];
	var number = 0;

	var isSelect = false;
	var filterPeopleList = $();
	var filterOccupied = $();

	// popup prefer
	var popupSeatPrefer = $('.popup--seat-prefer');
	var popupConfirmSeatBlank = $('.popup--confirm-seat-2');
	var popupConfirmModal = $('.popup--confirm-seat-4');
	var popupPreferredNotSelect = $('.popup--seat-not-select');

	var preventUpdate = false;
	var flyingFocus = $('#flying-focus');


	var checkAllEmptyChair = function() {
		for (var i = 0; i < peopleList.length; i++) {
			if (arrSeat[i].status) {
				return false;
			}
		}
		return true;
	};

	var checkLeftChair = function() {
		var c = 0;
		for (var i = 0; i < peopleList.length; i++) {
			if (!arrSeat[i].status) {
				c++;
			}
		}
		return c;
	};

	var checkEmptyChair = function(number) {
		// console.log(arrSeat, number);
		if (!arrSeat[number].status) {
			arrSeat[number].status = true;
			return arrSeat[number].chairNumber;
		}
		for (var i = 0; i < peopleList.length; i++) {
			if (!arrSeat[i].status) {
				arrSeat[i].status = true;
				return arrSeat[i].chairNumber;
			}
		}
		return -1;
	};

	var findEmptyChair = function(number) {
		if (arrSeat[number] && !arrSeat[number].status) {
			return arrSeat[number].chairNumber;
		}
		for (var ii = number; ii < peopleList.length; ii++) {
			if (arrSeat[ii] && !arrSeat[ii].status) {
				return arrSeat[ii].chairNumber;
			}
		}
		for (var i = 0; i < peopleList.length; i++) {
			if (!arrSeat[i].status) {
				return arrSeat[i].chairNumber;
			}
		}
		return -1;
	};

	var checkAllPassenger = function() {
		var all = true;
		for (var i = 0; i < peopleList.length; i++) {
			if (!peopleList.eq(i).find('.passenger-detail__seat').length && !peopleList.eq(i).is('.disabled')) {
				all = false;
				return all;
			}
		}
		return all;
	};

	var newCircleRule = function(){
		var all = true;
		for (var i = 0; i < peopleList.length; i++) {
			if (!peopleList.eq(i).hasClass('chosen')) {
				all = false;
				return all;
			}
		}
		return all;
	};

	var removeChair = function(number) {
		for (var i = 0; i < peopleList.length; i++) {
			if ((i + 1) === Number(number)) {
				arrSeat[i].status = false;
				arrSeat[i].occupied = $();
				arrSeat[i].renew = false;
				return false;
			}
		}
	};

	var revertSeat = function(s) {
		s.removeClass(avaibleSeatClass).addClass(selectedClass);
		s.html(filterPeopleList.index() + 1);
	};

	var showSeatmapTooltip = function(isPrefer) {
		switch(isPrefer) {
			case 'preferred':
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('.tooltip__preferred-text').hide();
				break;
			case 'both':
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('.tooltip__preferred-text').hide();
				popupSeatSelect.find('.tooltip__windowless[data-tooltip-seatmap="windowless"]').show();
				break;
			case 'windowLessEmergency':
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('[data-tooltip-seatmap="windowless-emergency"]').show();
				popupSeatSelect.find('.tooltip__preferred-text').show();
				break;
			case 'pWindowLessEmergency':
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('[data-tooltip-seatmap="windowless-emergency"]').show();
				popupSeatSelect.find('.tooltip__preferred-text').show();
				break;
			case 'pEmergency':
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('[data-tooltip-seatmap="emergency"]').show();
				popupSeatSelect.find('.tooltip__preferred-text').show();
				break;
			default:
				popupSeatSelect.find('.tooltip__text-1').hide();
				popupSeatSelect.find('.tooltip__windowless').hide();
				popupSeatSelect.find('[data-tooltip-seatmap="windowless"]').show();
		}
	};

	var flagClicked = false;
	var selectOnMobile = function() {
		var sInfo = templateInforSeat.clone();
		win = $(window);
		isSelect = true;
		popupSeatChange.hide();
		showConfirmSeatBlank();
		if (preventUpdate) {
			isSelect = false;

			return;
		}
		popupSeatSelect.show();
		popupSeatSelect.find('.tooltip__windowless').hide();
		popupSeatSelect.find('.tooltip__text-1').hide();
		popupSeatSelect.find('.tooltip__standard-1').hide();
		popupSeatSelect.find('.tooltip__forward-1').hide();
		popupSeatSelect.find('.tooltip__preferred-text').hide();

		if (seat.hasClass('seat-char-windowless') && seat.hasClass('seat-char-hasexit')) {
			showSeatmapTooltip('windowLessEmergency');
		}
		else if (seat.hasClass('seat-char-windowless')) {
			popupSeatSelect.find('.tooltip__preferred').hide();
			popupSeatSelect.find('[data-tooltip-seatmap="windowless"]').show();
		}

		if (seat.data('preferred')) {
			popupSeatSelect.find('.tooltip__standard-1').hide();
			popupSeatSelect.find('.tooltip__forward-1').hide();

			popupSeatSelect.find('.tooltip__text-1').show();
			if(seat.data('seat-price-usd') == undefined){
				popupSeatSelect.find('.tooltip__preferred').text('Preferred Seat').show();

			} else{
				popupSeatSelect.find('.tooltip__preferred').text(texttemplatePrefer.format(seat.data('preferred') ? seat.data('seat-price-usd') + ' USD' : seat.data('seat-price'))).show();
			}
			currency = getCurrency(seat.data('seat-price'));

			if (seat.hasClass('seat-char-windowless') && seat.hasClass('seat-char-hasexit')) {
				// preferred and windowLess and emergencyExit
				showSeatmapTooltip('pWindowLessEmergency');
			}
			else if (!seat.hasClass('seat-char-windowless') && seat.hasClass('seat-char-hasexit')) {
				// preferred and emergencyExit
				showSeatmapTooltip('pEmergency');
			}
			else if (seat.hasClass('seat-char-windowless')) {
				// preferred and windowLess
				showSeatmapTooltip('both');
			}
			else {
				// preferred
				showSeatmapTooltip('preferred');
			}
		}

		if(seat.hasClass('standard-seat')) {
			popupSeatSelect.find('.tooltip__forward-1').hide();
			popupSeatSelect.find('.tooltip__text-1').hide();

			popupSeatSelect.find('.tooltip__standard-1').show();
			popupSeatSelect.find('.tooltip__standard').text(seat.data('price') === 0 ? 'Standard Seat' : texttemplateStandard.format(seat.data('price') + ' USD')).show();
		}

		if(seat.hasClass('forward-zone')) {
			popupSeatSelect.find('.tooltip__standard-1').hide();
			popupSeatSelect.find('.tooltip__text-1').hide();

			popupSeatSelect.find('.tooltip__forward-1').show();
			popupSeatSelect.find('.tooltip__forward').text(seat.data('price') === 0 ? 'Forward Zone Seat' : texttemplateForward.format(seat.data('price') + ' USD')).show();
		}

		var total = popupSeatSelectRadio;
		var checkedRadio = total.filter(':checked');

		if (!checkedRadio.length) {
			checkedRadio = total.parent().not('.disabled').first().find(':radio').prop({
				'checked': true
			});
		}
		var leftP = seat.offset().left - popupSeatSelect.outerWidth(true) / 2 + seat.outerWidth() / 2;
		var leftArrow = 0;
		if (leftP < 0) {
			leftArrow = seat.offset().left + seat.outerWidth() / 2;
			leftP = 0;
		}

		if (leftP + popupSeatSelect.outerWidth(true) >= win.width()) {
			leftP = leftP - (leftP + popupSeatSelect.outerWidth(true) - win.width());
			leftArrow = seat.offset().left - leftP + seat.outerWidth() / 2;
		}

		popupSeatSelect.css({
			top: seat.offset().top - popupSeatSelect.outerHeight(true) - 12 - SIA.global.vars.container.offset().top,
			left: leftP
		}).find('.btn-full').off('click.choosePerson').on('click.choosePerson', function(e) {
			e.preventDefault();
			$('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
			$('#btn-pre-seat-back').addClass('confirm-clicked');
			if(!$('#btn-pre-seat-proceed').hasClass('proceed-clicked')){
				var dataFixed = $('[data-fixed]');
				var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
				if($('body').hasClass('cib-seatsmap-page')){
					if(dataPreferredSeat == true  ){
						if(thisItemClick.data('preferred') !== 'P' && !thisItemClick.hasClass('seat-occupied')){
							popupPreferredNotSelect.Popup('show');
							popupPreferredNotSelect.Popup('show');
							$('body').find('.fadeInOverlay').addClass('overlay');
						}
					}
				}
			}

			if(dataPreferredSeat == false || dataPreferredSeat == undefined) {
				flagClicked = true;
			} else {
				flagClicked = true;
			}

			preventUpdate = false;
			filterPeopleList = peopleList.filter('.active');
			checkedRadio = total.filter(':checked');
			var index = checkedRadio.length ? total.index(checkedRadio) : filterPeopleList.index();

			$('#btn-pre-seat-proceed').on('click', function(){
				$(this).addClass('proceed-clicked');
				preventUpdate = false;
				flagClicked = true;
				if(flagClicked)	{
					updateInfor();
				}
			});

			$('#btn-pre-seat-back').on('click', function(){
				var dataFixed = $('[data-fixed]');
				var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
				flagClicked = false;
				if($('body').hasClass('cib-seatsmap-page')  && dataPreferredSeat == true ){
					$(this).addClass('cancel-clicked');
				}
				popupSeatSelect.hide();
			});

			var updateInfor = function() {
				number = checkEmptyChair(index);
				if (arrSeat[filterPeopleList.index()].status) {
					filterOccupied = $(arrSeat[filterPeopleList.index()].occupied);
					var dataFixed = $('[data-fixed]');
					var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
					if(!$('body').hasClass('cib-seatsmap-page') || dataPreferredSeat !== true){
						filterOccupied.addClass(avaibleSeatClass).removeClass(selectedClass);
					}else{
						if($('#btn-pre-seat-proceed').hasClass('proceed-clicked') || thisItemClick.hasClass('preferred-trigger')){
							filterOccupied.addClass(avaibleSeatClass).removeClass(selectedClass);
						}
					}
					var dataFixed = $('[data-fixed]');
					var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
					preventUpdate = false;
					if(!$('body').hasClass('cib-seatsmap-page') || dataPreferredSeat !== true){
						seat.removeClass(avaibleSeatClass).addClass(selectedClass);
					}else{
						if($('#btn-pre-seat-proceed').hasClass('proceed-clicked') || thisItemClick.hasClass('preferred-trigger')){

							seat.removeClass(avaibleSeatClass).addClass(selectedClass);
						}
					}

					setTimeout(function() {
						if(filterOccupied.data('background-color') === 'FZ'){
							if(!filterOccupied.hasClass('seat-selected')){
								filterOccupied.addClass(forwardZoneClass);
							}
						} else if(filterOccupied.data('background-color') === 'SS'){
							if(!filterOccupied.hasClass('seat-selected')){
								filterOccupied.addClass(standardSeat);
							}
						}
					}, 100);

					setTimeout(function() {
						if(seat.data('background-color') === 'FZ' && seat.hasClass('seat-selected')){
							seat.removeClass(forwardZoneClass);
						} else if(seat.data('background-color') === 'SS' && seat.hasClass('seat-selected')){
							seat.removeClass(standardSeat);
						}
					}, 100);

					if(seat.data('background-color') === 'FZ' && seat.hasClass('seat-selected')){
						seat.removeClass(forwardZoneClass);
					} else if(seat.data('background-color') === 'SS' && seat.hasClass('seat-selected')){
						seat.removeClass(standardSeat);
					}

					showConfirmSeatBlank();

					if (!peopleList.eq(filterPeopleList.index()).data('hasinfant') && seat.hasClass('seat-char-bassinet') && SIA.RenderSeat.seatObjects.deckClass === 'Economy Class') {
						// arrSeat[number-1].status = false;
						if (!preventUpdate) {
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.noinfant);
							popupConfirmModal.Popup('show');
							preventUpdate = true;
						}
					}
					if ((peopleList.eq(filterPeopleList.index()).data('hasinfant') || peopleList.eq(filterPeopleList.index()).data('ischild')) && seat.hasClass('seat-char-hasexit')) {
						// arrSeat[number-1].status = false;
						if (!preventUpdate) {
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.withInfantAndChild);
							popupConfirmModal.Popup('show');
							preventUpdate = true;
						}
					}

					if (preventUpdate) {
						seat.addClass(avaibleSeatClass).removeClass(selectedClass);
						popupSeatSelect.find('.tooltip__close').trigger('click.choosePerson');
						revertSeat(filterOccupied);
						filterOccupied = $();
						// filterOccupied.removeClass(avaibleSeatClass).addClass(selectedClass);
						return;
					}
					var dataFixed = $('[data-fixed]');
					var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
					if(!$('body').hasClass('cib-seatsmap-page') || dataPreferredSeat !== true){
						filterOccupied.html('&nbsp;');
					}else{
						if($('#btn-pre-seat-proceed').hasClass('proceed-clicked') || thisItemClick.hasClass('preferred-trigger')){
							filterOccupied.html('&nbsp;');
						}
					}
					filterPeopleList.find('.passenger-detail__seat').remove();
					filterPeopleList.find('input').val('');
					number = filterPeopleList.index() + 1;
					if (filterOccupied.data('preselected')) {
						filterOccupied.addClass(preselected).html(filterOccupied.data('preselected'));
						filterOccupied.removeClass(occupiedClass);
						if (filterOccupied.data('preferred') && filterPeopleList.data('preferred')) {
							filterPeopleList.removeData('preferred');
						}
						if (filterOccupied.hasClass('seat-char-bassinet')) {
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.preAssignedBassinet);
							if(!seat.hasClass('seat-char-bassinet')){
								// update seatmap popup
								if(!$('#btn-pre-seat-proceed').hasClass('proceed-clicked')){
									if(popupPreferredNotSelect.is(':visible')){
										var interval = setInterval(doStuff, 500);
										function doStuff() {
											if(!$('#btn-pre-seat-back').hasClass('cancel-clicked')){
											  if(popupPreferredNotSelect.is(':hidden')){
											  	popupConfirmModal.Popup('show');
											  	clearInterval(interval);
											  }
											}
										}
									}
								}else{
									popupConfirmModal.Popup('show');
								}

								if(!$('body').hasClass('cib-seatsmap-page')){
									popupConfirmModal.Popup('show');
								}
							}
						}
						else if (filterOccupied.hasClass(preferredSeatClass) && globalJson.seatMap.flow === 'MB' && !filterOccupied.data('notified')) {
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.preAssignedPreferred);
							// if(!seat.hasClass('seat-char-preferred')){
							popupConfirmModal.Popup('show');
							filterOccupied.data('notified', true);
							// }
						}
					} else {
						if (filterOccupied.data('preferred') && filterPeopleList.data('preferred')) {
							filterPeopleList.removeData('preferred');
						} else if (filterOccupied.data('seat-characteristic')) {
							filterOccupied.html(filterOccupied.data('seat-characteristic'));
						} else {
							var dataFixed = $('[data-fixed]');
							var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
							if(!$('body').hasClass('cib-seatsmap-page') || dataPreferredSeat !== true){
								filterOccupied.html('&nbsp;');
							}else{
								if($('#btn-pre-seat-proceed').hasClass('proceed-clicked') || thisItemClick.hasClass('preferred-trigger')){
									filterOccupied.html('&nbsp;');
								}
							}
						}
					}

					if (filterOccupied.data('BSP')) {
						filterOccupied.data('BSP').item.remove();
						getTotal = unformatNumber(headtotal.text());
						headtotal.text(currency +' ' + formatNumber(getTotal - parseFloat(filterOccupied.data('BSP').price)));
						grandtotal.text(currency +' ' + formatNumber(getTotal - parseFloat(filterOccupied.data('BSP').price)));
						seat.removeData('BSP');
					}
					sInfo.appendTo(peopleList.eq((filterPeopleList.index()))).html(seat.data('sia-seat'));

					peopleList.eq(filterPeopleList.index()).find('input').val(seat.data('sia-seat'));

					filterOccupied.data('name', '');

					// update seatmap popup

					var dataFixed = $('[data-fixed]');
					var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
					if($('body').hasClass('cib-seatsmap-page')  && dataPreferredSeat == true ){
						filterOccupied.addClass(avaibleSeatClass).removeClass(selectedClass);
						setTimeout(function() {
							if(filterOccupied.data('background-color') === 'FZ'){
								if(!filterOccupied.hasClass('seat-selected')){
									filterOccupied.addClass(forwardZoneClass);
								}
							} else if(filterOccupied.data('background-color') === 'SS'){
								if(!filterOccupied.hasClass('seat-selected')){
									filterOccupied.addClass(standardSeat);
								}
							}
						}, 100);
						setTimeout(function() {
							if(seat.data('background-color') === 'FZ' && seat.hasClass('seat-selected')){
								seat.removeClass(forwardZoneClass);
							} else if(seat.data('background-color') === 'SS' && seat.hasClass('seat-selected')){
								seat.removeClass(standardSeat);
							}
						}, 100);

						seat.removeClass(avaibleSeatClass).addClass(selectedClass);

						seat.removeClass(avaibleSeatClass).addClass(selectedClass).html(number);
						$('#btn-pre-seat-back').removeClass('cancel-clicked');
						$('#btn-pre-seat-back').removeClass('confirm-clicked');
					}
					setTimeout(function() {
						if(filterOccupied.data('background-color') === 'FZ'){
							if(!filterOccupied.hasClass('seat-selected')){
								filterOccupied.addClass(forwardZoneClass);
							}
						} else if(filterOccupied.data('background-color') === 'SS'){
							if(!filterOccupied.hasClass('seat-selected')){
								filterOccupied.addClass(standardSeat);
							}
						}
					}, 100);
					setTimeout(function() {
						if(seat.data('background-color') === 'FZ' && seat.hasClass('seat-selected')){
							seat.removeClass(forwardZoneClass);
						} else if(seat.data('background-color') === 'SS' && seat.hasClass('seat-selected')){
							seat.removeClass(standardSeat);
						}
					}, 100);

					var dataFixed = $('[data-fixed]');
					var dataPreferredSeat = dataFixed.find('a.booking-nav__item.active').data('preferred-seat');
					preventUpdate = false;
					flagClicked = true;
					if(!$('body').hasClass('cib-seatsmap-page') || dataPreferredSeat !== true){
						seat.removeClass(avaibleSeatClass).addClass(selectedClass).html(number);
					}else{
						if($('#btn-pre-seat-proceed').hasClass('proceed-clicked') || thisItemClick.hasClass('preferred-trigger')){
							seat.removeClass(avaibleSeatClass).addClass(selectedClass).html(number);
						}
					}

					arrSeat[filterPeopleList.index()].occupied = seat;

					seat.data('name', popupSeatSelectRadio.eq(number - 1).siblings('label').html());
					if (seat.data('preferred')) {
						peopleList.eq(filterPeopleList.index()).data('preferred', {
							price: seat.data('seat-price').replace(/[a-z\s]/gi,''),
							priceUSD: seat.data('seat-price-usd'),
							seat: seat.data('sia-seat'),
							el: seat,
							info: peopleList.eq(filterPeopleList.index()).find('.passenger-info__text').html() +'<br>'+ peopleList.eq(filterPeopleList.index()).find('.passenger-info__text').next('.passenger-info__text').html()
						});
						currency = getCurrency(seat.data('seat-price'));
					}
					// total.eq(filterPeopleList.index()).prop({
					//  'disabled': true,
					//  'checked': false
					// }).parent().addClass('disabled');

					var etc = filterPeopleList.next('.booking-nav__item');
					total.eq(filterPeopleList.index()).siblings('.passenger-detail__seat').remove();
					total.eq(filterPeopleList.index()).parent().append($(templateInforSeat).clone().html(seat.data('sia-seat')));
					var doneCR = function(){
						total.eq(filterPeopleList.index()).prop({
							'checked': true
						}).parent().removeClass('disabled');
					};
					var firstCR = function(){
						peopleList.removeClass('active').eq(0).addClass('active');
						total.eq(0).prop({
							'checked': true
						}).parent().removeClass('disabled');
					};
					var nextCR = function(){
						filterPeopleList.removeClass('active').next().addClass('active');
						total.eq(etc.index()).prop({
							'checked': true
						}).parent().removeClass('disabled');
					};
					if (etc.length && !checkAllPassenger()) {
						nextCR();
						filterPeopleList.addClass('chosen');
					} else if (!etc.length) {
						filterPeopleList.addClass('chosen');
						if(body.hasClass('f-loop')){
							if(newCircleRule()){
								doneCR();
							}
							else{
								firstCR();
							}
						}
						else{
							// if (!filterPeopleList.data('last')) {
							if (!checkAllPassenger()) {
								// filterPeopleList.data('last', true);
								firstCR();
							}
						}
					} else if (checkAllPassenger()) {
						filterPeopleList.addClass('chosen');
						if(body.hasClass('f-loop')){
							if(newCircleRule()){
								doneCR();
							}
							else{
								nextCR();
							}
						}
						else{
							doneCR();
						}
					}
					// if (etc.length && !checkAllPassenger()) {
					// 	filterPeopleList.removeClass('active').next().addClass('active');
					// 	total.eq(etc.index()).prop({
					// 		'checked': true
					// 	}).parent().removeClass('disabled');
					// } else if (!etc.length) {
					// 	if (!checkAllPassenger()) {
					// 	// if (!filterPeopleList.data('last')) {
					// 		// filterPeopleList.data('last', true);
					// 		peopleList.removeClass('active').eq(0).addClass('active');
					// 		total.eq(0).prop({
					// 			'checked': true
					// 		}).parent().removeClass('disabled');
					// 	}
					// } else if (checkAllPassenger()) {
					// 	total.eq(filterPeopleList.index()).prop({
					// 		'checked': true
					// 	}).parent().removeClass('disabled');
					// }
				} else {
					if (number !== -1) {
						if (!peopleList.eq(number - 1).data('hasinfant') && seat.hasClass('seat-char-bassinet') && SIA.RenderSeat.seatObjects.deckClass === 'Economy Class') {
							arrSeat[number - 1].status = false;
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.noinfant);
							popupConfirmModal.Popup('show');
							popupSeatSelect.hide();
							win.off('resize.popupSeatSelect');
							return;
						}
						if ((peopleList.eq(number - 1).data('hasinfant') || peopleList.eq(number - 1).data('ischild')) && seat.hasClass('seat-char-hasexit')) {
							arrSeat[number - 1].status = false;
							popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.withInfantAndChild);
							popupConfirmModal.Popup('show');
							popupSeatSelect.hide();
							win.off('resize.popupSeatSelect');
							return;
						}
						peopleList.eq((number - 1)).find('.passenger-detail__seat').remove();
						sInfo.appendTo(peopleList.eq((number - 1))).html(seat.data('sia-seat'));
						peopleList.eq(number - 1).find('input').val(seat.data('sia-seat'));

						if (seat.find('.ico-bassinet').length) {
							seat.removeClass(avaibleSeatClass).addClass(selectedClass).html('<em class="ico-bassinet"></em>' + number);
						} else {
							seat.removeClass(avaibleSeatClass).addClass(selectedClass).html(number);
						}
						arrSeat[number - 1].occupied = seat;
						seat.data('name', popupSeatSelectRadio.eq(index).siblings('label').html());
						// total.eq(number - 1).prop({
						//  'disabled': true,
						//  'checked': false
						// }).parent().addClass('disabled');

						if (!total.eq(findEmptyChair(index) - 1).parent().hasClass('disabled')) {
							total.eq(findEmptyChair(index) - 1).prop({
								'checked': true
							});
						}

						if (findEmptyChair(number) !== -1) {
							peopleList.removeClass('active').eq((findEmptyChair(number) - 1)).addClass('active');
							total.filter('[disabled]').prop({
								'checked': false
							}).parent().addClass('disabled');
						} else {
							peopleList.removeClass('active').eq(0).addClass('active');
							total.eq(0).prop({
								'checked': true
							}).parent().removeClass('disabled');
						}

						if (seat.data('preferred')) {
							peopleList.eq(number - 1).data('preferred', {
								price: seat.data('seat-price').replace(/[a-z\s]/gi,''),
								priceUSD: seat.data('seat-price-usd'),
								seat: seat.data('sia-seat'),
								el: seat,
								info: peopleList.eq(number - 1).find('.passenger-info__text').html()
							});
							currency = getCurrency(seat.data('seat-price'));
						}
					}
				}

				popupSeatSelect.hide();
				win.off('resize.popupSeatSelect');
				if (seat.data('seat-price')) {
					// if (!BSP.hasClass('active')) {
					// 	triggerOpenBS.trigger('click.openBS');
					// }
					item = $(templateBSPPreferredSeat.format(L10n.label.preferred, formatNumber(seat.data('preferred') ? seat.data('seat-price-usd') : seat.data('seat-price')))).insertAfter(fare);
					getTotal = unformatNumber(headtotal.text());
					seat.data('BSP', {
						item: item,
						price: unformatNumber(seat.data('seat-price')),
						priceUSD: seat.data('seat-price-usd')
					});
					currency = getCurrency(seat.data('seat-price'));
					// headtotal.text(currency +' ' + formatNumber(getTotal + parseFloat(seat.data('preferred') ? seat.data('seat-price-usd') : seat.data('seat-price'))));
					// grandtotal.text(currency +' ' + formatNumber(getTotal + parseFloat(seat.data('preferred') ? seat.data('seat-price-usd') : seat.data('seat-price'))));
					// headtotal.text(currency +' ' + formatNumber(getTotal + parseFloat(seat.data('preferred') ? seat.data('seat-price-usd') : seat.data('seat-price'))));
					// grandtotal.text(currency +' ' + formatNumber(getTotal + parseFloat(seat.data('preferred') ? seat.data('seat-price-usd') : seat.data('seat-price'))));
					headtotal.text(currency +' ' + formatNumber(getTotal + unformatNumber(seat.data('seat-price'))));
					grandtotal.text(currency +' ' + formatNumber(getTotal + unformatNumber(seat.data('seat-price'))));
				}

				// var seatStatus = setStatusLabel(null, seat.data('code'), peopleList.eq((number - 1))) || '';
				var seatStatus = seat.data('price');
				var dataSeatPrice = seat.data('seat-price');
				var detailSeatStatus = seatStatus;
				if(seatStatus === 0) {
					detailSeatStatus = 'Complimentary';
				} else {
									detailSeatStatus = $('body').hasClass('sk-ut-workflow') ? 'USD ' + detailSeatStatus : dataSeatPrice;
				}

				popupSeatSelect.find('input[type="radio"]:checked').siblings('.passenger-detail__seat-status').text(detailSeatStatus);

				filterPeopleList.data('seattype', seat.data('seattype'));
			};

			if(flagClicked)	{
				updateInfor();
			}


			isSelect = false;
			popupSeatSelect.find('.tooltip__windowless').hide();
			popupSeatSelect.find('.tooltip__text-1').hide();

			if(!$('.booking-nav__item.active').data('hasinfant')) {
				priceCookie = $('.total-cost').data('price') ? $('.total-cost').data('price') : priceCookie;
				priceCookie[$('.booking-nav__item.active').index()] = parseFloat($(this).attr('data-seatprice'))*1.4;
				$('.total-cost').attr('data-price', JSON.stringify(priceCookie));
				var totalFare = $('.total-cost').data('total-fare');
				var d = new Date();
					d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
					var expires = "expires="+d.toUTCString();
					document.cookie = 'priceData' +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'priceData' + "=" + JSON.stringify(priceCookie) + ";" + expires + ";path=/";

				var priceSeat = 0;
				!_.isEmpty(priceCookie) && _.map(priceCookie, function(price){
					priceSeat += price
				})

				$('.total-cost').find('.amount').text((totalFare + priceSeat).toLocaleString(undefined, { minimumFractionDigits: 2 }));
				$('.sub-total').find('.price').text((totalFare + priceSeat).toLocaleString(undefined, { minimumFractionDigits: 2 }));
			}

			// What is this function for?
			if ($.isFunction(onSelected)) {
				onSelected.call(this, seat, index);
			}
		});

		popupSeatSelect.find('.tooltip__heading strong').text(seat.data('sia-seat'));
		popupSeatSelect.find('.tooltip__arrow').css('left', '').css({
			left: leftArrow ? leftArrow : ''
		});

		popupSeatSelect.find('.tooltip__close').off('click.choosePerson').on('click.choosePerson', function(e) {
			e.preventDefault();
			 $('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
			popupSeatSelect.hide();
			popupSeatSelect.find('.tooltip__windowless').hide();
			popupSeatSelect.find('.tooltip__text-1').hide();
			isSelect = false;
			win.off('resize.popupSeatSelect');
		});

		win.off('resize.popupSeatSelect').on('resize.popupSeatSelect', function() {
			clearTimeout(timerPopup);

			timerPopup = setTimeout(function() {
				var leftP = seat.offset().left - popupSeatSelect.outerWidth(true) / 2 + seat.outerWidth() / 2;
				var leftArrow = 0;
				if (leftP < 0) {
					leftArrow = seat.offset().left + seat.outerWidth() / 2;
					leftP = 0;
				} else if (leftP + popupSeatSelect.width() >= win.width()) {
					leftP = leftP - (leftP + popupSeatSelect.width() - win.width());
					leftArrow = seat.offset().left - leftP + seat.outerWidth() / 2;
				}

				popupSeatSelect.show().css({
					top: seat.offset().top - popupSeatSelect.outerHeight(true) - 12 - SIA.global.vars.container.offset().top,
					left: leftP
				});

				popupSeatSelect.find('.tooltip__arrow').css('left', '').css({
					left: leftArrow ? leftArrow : ''
				});
				if (win.width() >= tablet) {
					popupSeatSelect.hide();
					win.off('resize.popupSeatSelect');
					isSelect = false;
				}
			}, 150);
		});
	};

	var unselectedOnMobile = function() {
		isSelect = true;
		popupSeatChange.find('.tooltip__heading').html(seat.data('name'));
		popupSeatSelect.hide();
		popupSeatChange.show();

		var leftP = seat.offset().left - popupSeatChange.outerWidth(true) / 2 + seat.outerWidth() / 2;

		var leftArrow = 0;

		if (leftP < 0) {
			leftArrow = seat.offset().left + seat.outerWidth() / 2;
			leftP = 0;
		} else if (leftP + popupSeatChange.width() >= win.width()) {
			leftP = leftP - (leftP + popupSeatChange.width() - win.width());
			leftArrow = seat.offset().left - leftP + seat.outerWidth() / 2;
		}

		popupSeatChange.css({
			top: seat.offset().top - popupSeatChange.outerHeight(true) - 12 - SIA.global.vars.container.offset().top,
			left: leftP
		}).find('.btn-full').off('click.changeSeat').on('click.changeSeat', function(e) {
			e.preventDefault();
			$('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
			var seatNumberUnSelect = seat.html();

			popupSeatChange.hide();
			removeChair(seat.text());
			popupSeatSelectRadio.eq((seatNumberUnSelect - 1)).prop('disabled', false).parent().removeClass('disabled');
			popupSeatSelectRadio.eq((seatNumberUnSelect - 1)).siblings('.passenger-detail__seat').remove();

			peopleList.eq((seatNumberUnSelect - 1)).removeClass('chosen').find('.passenger-detail__seat').remove();

			peopleList.eq((seatNumberUnSelect - 1)).find('.input').val('');

			if (parseInt(seatNumberUnSelect === peopleList.length)) {
				// peopleList.eq(peopleList.length - 1).data('last', false);
				peopleList.eq(peopleList.length - 1);
			} else if (!peopleList.eq(peopleList.length - 1).find('.passenger-detail__seat').length) {
				// peopleList.eq(peopleList.length - 1).data('last', false);
				peopleList.eq(peopleList.length - 1);
			}

			peopleList.eq((seatNumberUnSelect - 1)).removeData('seattype');

			seat.removeClass(selectedClass).addClass(avaibleSeatClass);

			setTimeout(function() {
				if(seat.data('background-color') === 'FZ'){
					seat.addClass(forwardZoneClass);
				} else if(seat.data('background-color') === 'SS'){
					seat.addClass(standardSeat);
				}
			}, 100);

			if ($.isFunction(onUnSelected)) {
				onUnSelected.call(this, seat);
			}

			if (checkLeftChair() === 1) {
				popupSeatSelectRadio.filter('[disabled]').prop({
					'checked': false
				}).parent().addClass('disabled');
				if (!popupSeatSelectRadio.eq(findEmptyChair(seatNumberUnSelect - 1) - 1).parent().hasClass('disabled')) {
					popupSeatSelectRadio.eq(findEmptyChair(seatNumberUnSelect - 1) - 1).prop({
						'checked': true
					});
				}
				peopleList.removeClass('active').eq(seatNumberUnSelect - 1).addClass('active');
			}
			if (seat.data('preselected')) {
				if (seat.data('preferred') && peopleList.eq((seatNumberUnSelect - 1)).data('preferred')) {
					peopleList.eq((seat.html() - 1)).removeData('preferred');
				}
				seat.addClass(preselected).html(seatNumberUnSelect);
				seat.html(seat.data('preselected'));
			} else {
				if (seat.data('preferred') && peopleList.eq((seatNumberUnSelect - 1)).data('preferred')) {
					peopleList.eq((seatNumberUnSelect - 1)).removeData('preferred');
					seat.html(seat.data('preferred'));
				} else if (seat.data('seat-characteristic')) {
					seat.html(seat.data('seat-characteristic'));
				} else {
					seat.html('');
				}
			}
			isSelect = false;
			win.off('resize.popupSeatChange');

			// reset first passenger
			if (checkAllEmptyChair()) {
				peopleList.removeClass('active').eq(0).addClass('active');
			}

			if (seat.data('BSP')) {
				seat.data('BSP').item.remove();
				getTotal = unformatNumber(headtotal.text());
				headtotal.text(currency +' ' + formatNumber(getTotal - parseFloat(seat.data('BSP').price)));
				grandtotal.text(currency +' ' + formatNumber(getTotal - parseFloat(seat.data('BSP').price)));
				seat.removeData('BSP');
			}

			if(!$('.booking-nav__item.active').data('hasinfant')) {
				priceCookie = $('.total-cost').data('price') ? $('.total-cost').data('price') : priceCookie;
				priceCookie[$('.booking-nav__item.active').index()] = 0;
				$('.total-cost').attr('data-price', JSON.stringify(priceCookie));
				var totalFare = $('.total-cost').data('total-fare');
				var d = new Date();
					d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
					var expires = "expires="+d.toUTCString();
					document.cookie = 'priceData' +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'priceData' + "=" + JSON.stringify(priceCookie) + ";" + expires + ";path=/";

				var priceSeat = 0;
				!_.isEmpty(priceCookie) && _.map(priceCookie, function(price){
					priceSeat += price
				})

				$('.total-cost').find('.amount').text((totalFare + priceSeat).toLocaleString(undefined, { minimumFractionDigits: 2 }));
				$('.sub-total').find('.price').text((totalFare + priceSeat).toLocaleString(undefined, { minimumFractionDigits: 2 }));
			}
		});
		popupSeatChange.find('.tooltip__arrow').css('left', '').css({
			left: leftArrow ? leftArrow : ''
		});
		popupSeatChange.find('.tooltip__close').off('click.cancelChangeSeat').on('click.cancelChangeSeat', function(e) {
			e.preventDefault();
			$('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
			popupSeatChange.hide();
			popupSeatSelect.find('.tooltip__windowless').hide();
			popupSeatSelect.find('.tooltip__text-1').hide();
			win.off('resize.popupSeatChange');
			isSelect = false;
		});

		win.off('resize.popupSeatChange').on('resize.popupSeatChange', function() {
			clearTimeout(timerPopup);

			timerPopup = setTimeout(function() {
				var leftP = seat.offset().left - popupSeatChange.outerWidth(true) / 2 + seat.outerWidth() / 2;
				var leftArrow = 0;
				if (leftP < 0) {
					leftArrow = seat.offset().left + seat.outerWidth() / 2;
					leftP = 0;
				} else if (leftP + popupSeatChange.width() >= win.width()) {
					leftP = leftP - (leftP + popupSeatChange.width() - win.width());
					leftArrow = seat.offset().left - leftP + seat.outerWidth() / 2;
				}

				popupSeatChange.show().css({
					top: seat.offset().top - popupSeatChange.outerHeight(true) - 12 - SIA.global.vars.container.offset().top,
					left: leftP
				});
				popupSeatChange.find('.tooltip__arrow').css('left', '').css({
					left: leftArrow ? leftArrow : ''
				});
				if (win.width() >= tablet) {
					popupSeatChange.hide();
					win.off('resize.popupSeatChange');
					isSelect = false;
				}
			}, 150);
		});
	};

	var showConfirmSeatBlank = function() {
		if (body.hasClass(blankSeat)) {
			if (seat.siblings('.' + selectedClass).length) {
				var siblingsChair = seat.siblings('.' + selectedClass).last();
				if (seat.next('.' + selectedClass).length) {
					siblingsChair = seat.next('.' + selectedClass);
				}

				if (Math.abs(siblingsChair.index() - seat.index()) >= 2 && (!seat.prev().hasClass(occupiedClass) && !seat.prev().hasClass(emptyClass) && !seat.next().hasClass(occupiedClass) && !seat.next().hasClass(emptyClass))) {
					popupConfirmSeatBlank.Popup('show');

					preventUpdate = true;
				}
			}

			if (filterOccupied.length && filterOccupied.prev().hasClass(selectedClass) && filterOccupied.next().hasClass(selectedClass)) {
				popupConfirmSeatBlank.Popup('show');

				preventUpdate = true;
			}
		}
	};

	var init = function() {
		global = SIA.global;
		win = global.vars.win;

		seatsCont = SIA.RenderSeat.seatsCont;
		seatItems = seatsCont.find('.seat');

		templateInforSeat = $(SIA.RenderSeat.seatObjects.template.inforSeat);

		// popup prefer
		popupSeatPrefer = $('.popup--seat-prefer');
		popupConfirmSeatBlank = $('.popup--confirm-seat-2');
		popupConfirmModal = $('.popup--confirm-seat-4');

		preventUpdate = false;
		flyingFocus = $('#flying-focus');

		popupSeatSelect = $('[data-infomations-1]');
		popupSeatChange = $('[data-infomations-2]');
		texttemplatePrefer = popupSeatSelect.find('.tooltip__preferred').text();
		texttemplateStandard = popupSeatSelect.find('.tooltip__standard').text();
		texttemplateForward = popupSeatSelect.find('.tooltip__forward').text();
		texttemplateFeePrefer = popupSeatPrefer.find('div.popup__text-intro p').text();
		popupSeatSelectRadio = popupSeatSelect.find(':radio');
		peopleList = $('.sidebar .booking-nav > a');

		popupSeatSelect.removeClass('hidden').hide();
		popupSeatChange.removeClass('hidden').hide();

		popupSeatSelectRadio.each(function(idx) {
			// $(this).prop('disabled', false).parent().removeClass('disabled').off('change.changestatus').on('change.changestatus', function() {
			// 	peopleList.removeClass('active').eq(idx).addClass('active');
			// });
			!$(this).parent().is('.disabled') && $(this).prop('disabled', false);

			$(this).parent().off('change.changestatus').on('change.changestatus', function() {
				peopleList.removeClass('active').eq(idx).addClass('active');
			});
		});

		if (BSP.length) {
			BSP.find('.booking-summary__control').off('click.resizett').on('click.resizett', function() {
				win.trigger('resize.popupSeatChange');
				win.trigger('resize.popupSeatSelect');
			});
		}

		// var detectAllDisable = function(){
		//  var all = true;
		//  popupSeatSelectRadio.each(function(){
		//    if(!$(this).is('[disabled]')){
		//      all = false;
		//      return all;
		//    }
		//  });
		//  return all;
		// };

		var detectAllDisable = function() {
			var all = true;
			peopleList.each(function() {
				// if (idx === peopleList.length - 1 && $(this).find('.passenger-detail__seat').length) {
				// 	$(this).data('last', true);
				// }
				if (!$(this).find('.passenger-detail__seat')) {
					all = false;
					return all;
				}
			});
			return all;
		};

		if (detectAllDisable()) {
			popupSeatSelectRadio.eq(0).prop({
				'checked': true
			}).parent().removeClass('disabled');
		}

		if (!peopleList.filter('.active').length) {
			peopleList.eq(0).addClass('active');
			popupSeatSelectRadio.eq(0).prop({
				'checked': true
			}).parent().removeClass('disabled');
		}

		popupSeatSelectRadio.eq(peopleList.filter('.active').index()).prop({
			'checked': true
		}).parent().removeClass('disabled');

		var preferData = {
			heading: {
				passenger: L10n.preferModal.passenger,
				seat: L10n.preferModal.seat,
				price: L10n.preferModal.price.format($.trim(currency))
			},
			isICE: false,
			// isICE: BSP.length ? false : true,
			flightInfo: globalJson.seatMap.seatMapVO.flightDateInformationVO,
			total: {
				text: L10n.preferModal.total,
				number: 240
			}
		};

		var templateCustommerPreferSeat = '<div class="table-row">' +
			'<div class="table-col table-col-1">' +
			'<p class="text-dark">{0}</p>' +
			'</div>' +
			'<div class="table-col table-col-2"><span class="passenger-detail__seat">{1}</span></div>' +
			'<div class="table-col table-col-3"><span class="visible-mb">{2}</span><span class="price">{3}</span></div>' +
			'</div>';

		// seat = $();

		if (!popupSeatPrefer.data('Popup')) {
			popupSeatPrefer.Popup({
				overlayBGTemplate: '<div class="overlay"></div>',
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function() {
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
					win.off('resize.popupSeatSelect');
				},
				afterHide: function() {
					isSelect = false;
					win.off('resize.popupSeatSelect');
				}
			});
		}

		if (!popupConfirmSeatBlank.data('Popup')) {
			popupConfirmSeatBlank.Popup({
				overlayBGTemplate: '<div class="overlay"></div>',
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function() {
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
				}
			});
		}

		if (!popupConfirmModal.data('Popup')) {
			popupConfirmModal.Popup({
				overlayBGTemplate: '<div class="overlay"></div>',
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function() {
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
				}
			});
		}

		peopleList.off('click.selectPerson').on('click.selectPerson', function(e) {
			e.preventDefault();
			peopleList.removeClass('active');
			$(this).addClass('active');
		});

		for (var i = 1; i <= peopleList.length; i++) {
			if(typeof arrSeat[i - 1] === 'undefined'){
				arrSeat[i - 1] = {};
			}
			arrSeat[i - 1].chairNumber = i;
			arrSeat[i - 1].renew = false;
			arrSeat[i - 1].status =  false;
			arrSeat[i - 1].occupied = $();
		}

		seatItems.filter('.' + selectedClass).each(function(){
			var self = $(this);
			var index = parseInt(self.text()) - 1;
			arrSeat[index].status =  true;
			arrSeat[index].occupied = self;
		});

		$(document).mouseup(function(e) {
        var container = $("aside.tooltip--seat-1");
        var containerChangeSeat = $("aside.tooltip--seat-2");

        if (!containerChangeSeat.is(':visible')) {
	        // if the target of the click isn't the container nor a descendant of the container
	        if (!container.is(e.target) && container.has(e.target).length === 0)
	        {
	            container.hide();
	            isSelect = false;
	            $('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
	        } else {
	        	$('.seatsmap-sk-page').find('.seatmap-content').css('overflow', 'hidden');
	        }
        }

        if (!container.is(':visible')) {
	        // if the target of the click isn't the container nor a descendant of the container
	        if (!containerChangeSeat.is(e.target) && containerChangeSeat.has(e.target).length === 0)
	        {
	            containerChangeSeat.hide();
	            isSelect = false;
	            $('.seatsmap-sk-page').find('.seatmap-content').removeAttr('style');
	        } else {
	        	$('.seatsmap-sk-page').find('.seatmap-content').css('overflow', 'hidden');
	        }
        }

    });

		seatItems.each(function() {
			$(this).on('click.selectSeat', function() {
				if(!$(this).hasClass('seat-occupied')) {
          $('.seatsmap-sk-page').find('.seatmap-content').css('overflow', 'hidden');
        }
				thisItemClick = $(this);
				$(this).trigger('preselect');

				if($(this).data('preferred') == 'P'){
					$(this).addClass('preferred-trigger');
				}
				// var win = $(window);

				if (isSelect) {
					return;
				}

				seat = $(this);

				if (seat.hasClass(occupiedClass) || seat.hasClass(blockedClass) || seat.hasClass(emptyClass)) {
					return;
				}

				if (seat.hasClass(selectedClass)) {
					if(body.hasClass(deselectOn)){
						return;
					}
					if (seat.siblings('.' + selectedClass).length) {
						if (seat.next('.' + selectedClass).length && seat.prev('.' + selectedClass).length) {
							if (body.hasClass(blankSeat)) {
								popupConfirmSeatBlank.Popup('show');
								return;
							}
						}
					}
					// if(win.width() >= tablet) {// desktop
					//  unselectOnDeskTop(seat);
					// }
					// else{
					// }
					unselectedOnMobile();
				} else {
					preventUpdate = false;

					if (preventUpdate) {
						return;
					}

					// if(win.width() >= tablet) {
					//  selectOnDeskTop();
					// }
					// else{
					// }
					selectOnMobile();
				}

				indexSeat = seatsCont.find(seatItems).index(seat);

				$('input[name="seat-tooltip-submit"]').attr('data-seatprice', $(this).data('price'));
				$('[data-infomations-2] .btn-full').attr('data-seatprice', $(this).data('price'));
				if ($.isFunction(afterChoose)) {
					afterChoose.call(this, indexSeat);
				}
			});
		});

		// check logic
		var checkLogic = function() {
			var formSeatmap = $('#form-seatmap');
			var nextFlightBtn = $('.button-group-1 input', formSeatmap);
			var acceptPopBtn = popupSeatPrefer.find('#form-prefer-submit-1');
			var rdAcceptPop = popupSeatPrefer.find('#form-prefer-1');
			var preferContain = null;
			var newFlightDate = $('[name="newFlightDate"]');
			var newFlightNumber = $('[name="newFlightNumber"]');
			var newDepartureSegment = $('[name="newDepartureSegment"]');
			var newArrivalSegment = $('[name="newArrivalSegment"]');
			var isloading = false;

			// var validationFailureCheck = function() {
			// 	if ($('span.passenger-detail__seat', peopleList).length < peopleList.length) {
			// 		return true;
			// 	}
			// 	return false;
			// };

			// var checkChildHasSelected = function() {
			// 	var selectAll = true;
			// 	peopleList.each(function() {
			// 		var self = $(this);
			// 		if ((self.data('hasinfant') || self.data('ischild')) && !self.find('.passenger-detail__seat').length) {
			// 			selectAll = false;
			// 			return selectAll;
			// 		}
			// 	});
			// 	return selectAll;
			// };

			// add data preferred if there is no MB and ICE
			var checkAndDataPreferred = function(){
				seatItems.filter('.' + selectedClass).filter('.' + preferredSeatClass).each(function(){
					var self = $(this);
					var index = parseInt(self.html()) - 1;
					peopleList.eq(index).data('preferred', {
						price: self.data('seat-price').replace(/[a-z\s]/gi,''),
						priceUSD: self.data('seat-price-usd'),
						seat: self.data('sia-seat'),
						el: self,
						info: peopleList.eq(index).find('.passenger-info__text').html() +'<br>'+ peopleList.eq(index).find('.passenger-info__text').next('.passenger-info__text').html()
					});
					currency = getCurrency(self.data('seat-price'));
				});
			};

			if(!globalJson.seatMap.flow || globalJson.seatMap.flow !=='ICE' || globalJson.seatMap.flow !=='MB'){
				checkAndDataPreferred();
			}

			var checkChoosePrefer = function() {
				var preferInfor = [];
				peopleList.each(function(idx) {
					var passengerInforholder = $(this);
					var preferredInfor = passengerInforholder.data('preferred');
					if (preferredInfor) {
						if(globalJson.seatMap.flow === 'MB'){
							if(passengerInforholder.data('oldseatnumber')){
								if(passengerInforholder.data('oldseatnumber') !== preferredInfor.seat){
									// if(!preferredInfor.el.data('preselected')){
									// 	preferInfor.push(preferredInfor);
									// }
									// else if(preferredInfor.el.data('preselected') !== idx + 1){
									// 	preferInfor.push(preferredInfor);
									// }
									preferInfor.push(preferredInfor);
								}
								else if(passengerInforholder.data('oldseatnumber') === preferredInfor.seat && passengerInforholder.data('assignseatnumber') !== preferredInfor.seat){
									if(!preferredInfor.el.data('preselected')){
										preferInfor.push(preferredInfor);
									}
									else if(preferredInfor.el.data('preselected') !== idx + 1){
										preferInfor.push(preferredInfor);
									}
								}
							}
							else{
								if(!preferredInfor.el.data('preselected')){
									preferInfor.push(preferredInfor);
								}
								else if(preferredInfor.el.data('preselected') !== idx + 1){
									preferInfor.push(preferredInfor);
								}
							}
						}
						else if(globalJson.seatMap.flow !== 'ICE'){
							// if(!preferredInfor.el.data('preselected')){
							// 	preferInfor.push(preferredInfor);
							// }
							// else if(preferredInfor.el.data('preselected') !== idx + 1){
							// 	preferInfor.push(preferredInfor);
							// }
							preferInfor.push(preferredInfor);
						}
					}
				});
				return preferInfor;
			};
			var enableAccept = function() {
				if (rdAcceptPop.is(':checked')) {
					acceptPopBtn.removeClass('disabled').prop('disabled', false);
				} else {
					acceptPopBtn.addClass('disabled').prop('disabled', true);
				}
			};
			var numberOfSelectedSeats = popupSeatPrefer.find('.table-content');
			// popup validate
			var popupValidationCheck = $('.popup--confirm-seat-5');
			if (!popupValidationCheck.data('Popup')) {
				popupValidationCheck.Popup({
					overlayBGTemplate: '<div class="overlay"></div>',
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]',
					afterShow: function() {
						flyingFocus = $('#flying-focus');
						if (flyingFocus.length) {
							flyingFocus.remove();
						}
					}
				});
			}

			popupValidationCheck.find('#seatmap-cancel-1').off('click.cancelPopup').on('click.cancelPopup', function(e) {
				e.preventDefault();
				popupValidationCheck.Popup('hide');
			});
			popupValidationCheck.find('#seatmap-save').off('click.saveAndSubmit').on('click.saveAndSubmit', function(e) {
				e.preventDefault();
				if (preferContain.length) {
					popupValidationCheck.Popup('hide');
					renderPrefer();
				} else {
					if($('body').hasClass('sk-ut-workflow')) {
						changePage()
					} else {
						formSeatmap[0].submit();
					}
				}
			});

			var updateSeatNumber = function(info) {
				numberOfSelectedSeats.each(function() {
					var self = $(this);
					$.each(info, function() {
						var price = this.price;
						if($('body').hasClass('sk-ut-workflow')) {
							price = (parseFloat(price)*1.4).toLocaleString(undefined, { minimumFractionDigits: 2 });
						}
						$(templateCustommerPreferSeat.format(this.info, this.seat, L10n.preferModal.price.format(currency), price)).appendTo(self);
					});
				});
			};

			var calculateTotal = function(data, usd) {
				var tt = 0;
				if(usd){
					$.each(data, function() {
						// tt += parseFloat(this.priceUSD);
						tt += globalJson.seatMap.locale === 'de_DE' ? parseFloat(this.priceUSD.replace(/[.]/gi,'').replace(',','.')) : unformatNumber(this.priceUSD);
					});
				}
				else{
					$.each(data, function() {
						tt += globalJson.seatMap.locale === 'de_DE' ? parseFloat(this.price.replace(/[.]/gi,'').replace(',','.')) : unformatNumber(this.price);
					});
				}
				return formatNumber(parseFloat(tt), precision.getPrecision(usd ? 'USD' : currency));
			};

			rdAcceptPop.off('change.enableAccept').off('change.enableAccept').on('change.enableAccept', function() {
				enableAccept();
			});

			var updateNewSeatType = function() {
				var newSeatType = $('[name=newSeatType]');
				var st = '';
				var c = 0;
				peopleList.each(function(idx) {
					var seattype = $(this).data('seattype');
					if (seattype) {
						if (c) {
							st += (idx + 1) + '-' + seattype;
							c++;
						} else {
							st += ',' + (idx + 1) + '-' + seattype;
						}
					}
				});
				newSeatType.val(st);
			};

			var renderPrefer = function() {
				popupSeatPrefer.find('.table-default').empty();
				popupSeatSelect.find('.tooltip__close').trigger('click');
				popupSeatChange.find('.tooltip__close').trigger('click');
				isloading = true;
				$.get(SIA.global.config.url.preferSeatContent, function(data) {
					var template = window._.template(data, {
						'data': preferData
					});
					$(template).appendTo(popupSeatPrefer.find('.table-default'));
					numberOfSelectedSeats = popupSeatPrefer.find('.table-content');
					updateSeatNumber(preferContain);
					popupSeatPrefer.Popup('show');
					if ($(this).data('url')) {
						$(this).closest('form').attr('action', $(this).data('url'));
						popupSeatPrefer.find('form').attr('action', $(this).data('url'));
					}
					enableAccept();
					isloading = false;
				}, 'html');
			};

			var nextFlightBtnDataNext = nextFlightBtn.filter('[data-next="true"]');
			nextFlightBtn.filter('[data-next="true"]').off('click.showPrefer').off('click.showPrefer').on('click.showPrefer', function(e, urlFlight) {
				e.preventDefault();
				preferContain = checkChoosePrefer();
				updateNewSeatType();
				preferData = {
					heading: {
						passenger: L10n.preferModal.passenger,
						seat: L10n.preferModal.seat,
						price: L10n.preferModal.price.format($.trim(currency))
					},
					// isICE: BSP.length ? false : true,
					isICE: false,
					flightInfo: globalJson.seatMap.seatMapVO.flightDateInformationVO,
					total: {
						text: L10n.preferModal.total,
						number: currency + calculateTotal(preferContain)
					}
				};
				if (isloading) {
					return;
				}
				// popupSeatPrefer.find('p.popup__text-intro').html(L10n.preferModal.alert.format(calculateTotal(preferContain)));
				popupSeatPrefer.find('div.popup__text-intro p').html(texttemplateFeePrefer.format(calculateTotal(preferContain, true), currency));
				// if (!checkChildHasSelected()) {
				// 	e.preventDefault();
				// 	popupConfirmModal.find('.popup__text').html(L10n.seatEconomy.selectedChild);
				// 	popupConfirmModal.Popup('show');
				// } else if (validationFailureCheck()) {
				// 	e.preventDefault();
				// 	popupSeatSelect.find('.tooltip__close').trigger('click');
				// 	popupSeatChange.find('.tooltip__close').trigger('click');
				// 	// popupValidationCheck.find('.popup__text').text(L10n.seatEconomy.notSelected);
				// 	popupValidationCheck.Popup('show');
				// } else if (preferContain.length) {
				var url = urlFlight ? urlFlight : $(this).attr('data-nextflight');
				if (preferContain.length) {
					$('input[name="form-prefer-submit-1"]').attr('data-nextflight', url);
					e.preventDefault();
					renderPrefer();
				} else {
					if ($(this).data('url')) {
						$(this).closest('form').attr('action', $(this).data('url'));
					}
					if($('body').is('.sk-ut-workflow') && !nextFlightBtnDataNext.is('[data-last-flight]')) {
						changeTab(url)
					} else {
						if($('body').hasClass('sk-ut-workflow')) {
							changePage()
						} else {
							formSeatmap[0].submit();
						}
					}
				}
			});
			nextFlightBtn.filter(':not([data-next="true"])').off('click.skipThisStep').on('click.skipThisStep', function(e) {
				e.preventDefault();
				var self = $(this);
				if (self.data('url')) {
					formSeatmap.attr('action', self.data('url'));
				}
				if($('body').hasClass('sk-ut-workflow')) {
					changePage()
				} else {
					formSeatmap[0].submit();
				}
			});

			var updateFlightInforOnTab = function(el) {
				newFlightDate.val(el.data('flightdate'));
				newFlightNumber.val(el.data('flightnumber'));
				newDepartureSegment.val(el.data('departsegment'));
				newArrivalSegment.val(el.data('arrivalsegment'));
			};

			$('.tabs--1.seat-tabs').removeClass('hidden').on('click.switch-flight', '> .tab .tab-item a', function(e) {
				var self = $(this);
				var li = self.parent();
				var liIndex = self.closest('ul').children().index(li);
				var activeLiIndex = li.siblings('li.active').index();
				updateFlightInforOnTab(self);
				e.preventDefault();
				if (li.is('.active')) {
					return;
				}
				var url = li.attr('data-nextflight');
				if (liIndex > activeLiIndex) {
					nextFlightBtnDataNext.eq(nextFlightBtnDataNext.length - 1).trigger('click.showPrefer', url);
				} else {
					// nextFlightBtn.not('[data-next="true"]').trigger('click.showPrefer');
					if($('body').is('.sk-ut-workflow')) {
						changeTab(url);
					} else {
						if($('body').hasClass('sk-ut-workflow')) {
							changePage()
						} else {
							formSeatmap[0].submit();
						}
					}
				}

				$('input[name="form-prefer-submit-1"]').attr('data-nextflight', li.data('nextflight'));
			});
			var seatTabs = $('.tabs--1.seat-tabs');
			var selectFlight = seatTabs.children('.tab-select');
			var triggerTab = seatTabs.find('ul.tab .tab-item > a');
			// var smGeneric = $('.seatmap--generic');
			var selectFlightIndex = selectFlight.prop('selectedIndex');
			selectFlight.off('change.triggerSubmit').on('change.triggerSubmit', function(e) {
				e.preventDefault();
				// these code cause form not able to submit. so comment it
				// if ($(this).prop('selectedIndex') === 1) {
				// 	nextFlightBtn.filter('[data-next="true"]').trigger('click.showPrefer');
				// } else {
				// 	nextFlightBtn.not('[data-next="true"]').trigger('click.showPrefer');
				// }
				// setTimeout(function(){
				// 	if(smGeneric.length){
				// 		smGeneric.closest('form').submit();
				// 	}
				// 	else{
				// 		formSeatmap[0].submit();
				// 	}
				// }, 200);
				triggerTab.eq($(this).prop('selectedIndex')).trigger('click.switch-flight');
				$(this).prop('selectedIndex', selectFlightIndex);
			}).off('blur.resetValue').on('blur.resetValue', function() {
				$(this).children().eq(selectFlightIndex).prop('selected', true);
			});
		};

		// add legend
		var addLegend = function() {
			var seatLegend = $('.seat-legend');
			var seatLegendControl = seatLegend.find('.seat-legend__control');
			var seatLegendContent = seatLegend.find('.seat-legend-content');
			var seatStatus = $('.seat-status').clone().appendTo(seatLegendContent);
			seatStatus.find('[data-tooltip]').each(function() {
				if (!$(this).data('kTooltip')) {
					$(this).kTooltip();
				}
			});
			seatLegendControl.off('click.showlegend').on('click.showlegend', function(e) {
				e.preventDefault();

				// Add toggle animate for block Seat Legend
				seatLegendContent.slideToggle(400);

				if (seatLegendContent.hasClass('active')) {
					seatLegendContent.removeClass('active');
					seatLegendControl.removeClass('active');
				} else {
					seatLegendContent.addClass('active');
					seatLegendControl.addClass('active');
				}
				if (popupSeatSelect.is(':visible')) {
					popupSeatSelect.find('a.tooltip__close').trigger('click');
				}
				if (popupSeatChange.is(':visible')) {
					popupSeatChange.find('a.tooltip__close').trigger('click');
				}
			});
		};

		var showErrorMessage = function() {
			var errorMessage = $('.error-message');

			if (globalJson.seatMap.errorVO.errocode !== '' && globalJson.seatMap.errorVO.errocode !== 'Not available' && globalJson.seatMap.errorVO.errocode !== 'ERROR_seat.selection.generic') {
				errorMessage.attr('data-errormsg', globalJson.seatMap.errorVO.errocode).removeClass('hidden').find('p').text(globalJson.seatMap.errorVO.errodesc);
			}
		};

/*		var adjustHeightNavAndSeatMapContent = function(){
			var nav = $('[data-fixed]');
			var innerNav = nav.children('.inner');
			var seapContent = nav.next();
			if(seapContent.outerHeight(true) < innerNav.outerHeight(true)){
				seapContent.height(innerNav.height() + 100);
			}
		};*/

		// remove class seat-inner-galley
		// this code is temporary to solve case seat number and seat does not align
		var removeClassSeatInnerGaller = function(){
			$('.seat-inner-galley').each(function(){
				var self = $(this);
				if(!self.find('.seatmap-galley').length){
					self.removeClass('seat-inner-galley');
				}
			});
		};

		checkLogic();
		addLegend();
		showErrorMessage();
/*		adjustHeightNavAndSeatMapContent();*/
		removeClassSeatInnerGaller();
	};

	// init
	var oSeatSelection = {
		init: init
	};

	return oSeatSelection;
})();

SIA.RenderSeat.init();
// SIA.SeatSelection.seatMap();
// SIA.SeatSelection.init();x
if($('body').hasClass('seatsmap-sk-page')){
	var thisMainDeck = $('#main-deck');
	var seatmapCabin = thisMainDeck.children('.seatmap-cabin');
  seatmapCabin.find('div').first().addClass('countNum');
  var sumCountNum = 0;
  $('.countNum').children('div').each(function(){
    sumCountNum += $(this).width();
  });
  seatmapCabin.css('min-width', sumCountNum + 15 + 'px');
  var wSeatmapCabin = $('.seatmap-cabin').width() - $(window).width();
  $('.seatmap-content').scrollLeft(wSeatmapCabin/2 + 47);
}
$('body').is('.sk-ut-workflow') && $('input[name="form-prefer-submit-1"]').off('click.handleChangeTab').on('click.handleChangeTab', function(e){
	e.preventDefault();
	$(this).closest('.popup').Popup('hide');
	var url = $(this).attr('data-nextflight');

	if(!$('input[name="seat-3-submit"]').is('[data-last-flight]')) {
		changeTab(url)
	} else {
		changePage();
	}
})

if($('body').is('.sk-ut-workflow')) {
	$('input[name="seat-3-submit"]').attr('data-last-flight', true);
	$('input[name="seat-3-submit"]').val('proceed to payment');
}
