/**
 * @name SIA
 * @description Define global bookingWidget functions
 * @version 1.0
 */
SIA.bookingWidget = function(){
	var global = SIA.global;
	var bookingWidgetComponent = $('[data-booking-widget]');
	var bookingWidgetForm = bookingWidgetComponent.find('form');
	var bookingWidget = bookingWidgetComponent.find('.booking-widget.sticky');
	var bookingStatic = bookingWidgetComponent.find('.booking-widget-static');
	var bookingWidgetBlock = bookingWidget.find('.booking-widget-block');
	var stickyOpen = bookingWidgetComponent.find('[data-sticky-open]');
	var stickyReopen = bookingWidgetComponent.find('[data-sticky-reopen]');
	var stickyCloseEl = bookingWidget.find('.sticky__close');
	var isExpand = false;
	var hasAnimation = false;
	var isExpandAll = false;

	stickyOpen.off('click.openStciky').on('click.openStciky', function(){
		if(!isExpand){
			stickyOpen.addClass('hidden');
			stickyCloseEl.removeClass('hidden');
			bookingWidgetBlock.removeClass('hidden').addClass('animated');
			bookingWidget.removeClass('collapse').addClass('expand open-sticky');
			isExpand = true;
		}
		setTimeout(function(){
			if(!hasAnimation){
				bookingWidget.removeClass('expand');
				bookingWidgetBlock.removeClass('animated');
			}
		}, 500);
	});

	stickyReopen.off('click.reopen').on('click.reopen', function(e){
		e.preventDefault();
		if(!isExpandAll){
			bookingWidget.removeClass('hidden').addClass('animated expand-all');
			isExpandAll = true;
			isExpand = true;
		}
		else{
			stickyOpen.trigger('click.openStciky');
		}
	});

	stickyCloseEl.off('click.collapseSticky').on('click.collapseSticky', function(){
		if(isExpand){
			bookingWidgetBlock.addClass('animated');
			bookingWidget.removeClass('expand open-sticky').addClass('collapse');
			isExpand = false;
		}
		setTimeout(function(){
			if(!hasAnimation){
				bookingWidgetBlock.addClass('hidden').removeClass('animated');
				bookingWidget.removeClass('collapse');
				stickyCloseEl.addClass('hidden');
				stickyOpen.removeClass('hidden');
			}
		}, 500);
	});

	bookingWidgetBlock.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
		hasAnimation = true;
		if(isExpand){
			bookingWidget.removeClass('expand');
			bookingWidgetBlock.removeClass('animated');
		}else{
			bookingWidgetBlock.addClass('hidden').removeClass('animated');
			bookingWidget.removeClass('collapse');
			stickyCloseEl.addClass('hidden');
			stickyOpen.removeClass('hidden');
		}
	});

	bookingWidget.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
		if(isExpandAll){
			bookingWidget.removeClass('animated expand-all');
		}
	});

	bookingWidgetForm.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});

	if(bookingStatic.length){
		var toggleMbEl = bookingStatic.find('.slide-down-mb');

		bookingStatic.find('[data-toggle-mb]').each(function(){
			$(this).off('focus.toggleEl').on('focus.toggleEl', function(){
				toggleMbEl.addClass('active');
				toggleMbEl.addClass('animated slide-down-mb--expand');
			});
		});
	}
	var focusElementForm = function() {
		var classCustomEl = $('[data-class]'),
			adultCustomEl = $('[data-adult]'),
			childCustomEl = $('[data-child]');

			$(classCustomEl).off('change.saveClassData').on('change.saveClassData',function(){
				var AdultInputEl = $(this).closest('form').find('[data-adult]'),
					timeOut = null;
				timeOut && clearTimeout(timeOut);
				if(AdultInputEl.length) {
				  $('[data-customselect]').removeClass('focus');
				  timeOut = setTimeout(function(){
				    AdultInputEl.addClass('focus');
				  }, 100);
				}
			});
			$(adultCustomEl).off('change.saveAdultData').on('change.saveAdultData',function(event, flag){
				var ChildInputEl = $(this).closest('form').find('[data-child]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(ChildInputEl.length) {
				  	$('[data-customselect]').removeClass('focus');
					  timeOut = setTimeout(function(){
					    ChildInputEl.addClass('focus');
					  }, 100);
					}
			});
			$(childCustomEl).off('change.saveChildData').on('change.saveChildData',function(event, flag){
				var InfantInputEl = $(this).closest('form').find('[data-infant]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(InfantInputEl.length) {
				  	$('[data-customselect]').removeClass('focus');
					  timeOut = setTimeout(function(){
					    InfantInputEl.addClass('focus');
					  }, 100);
					}
			});			
		};
	focusElementForm();
};
