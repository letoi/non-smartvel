/**
 * @name SIA
 * @description Define global donatemiles functions
 * @version 1.0
 */

SIA.manageProgressBar = function () {
  var degMap = {
    60000: {
      from: 85,
      to: 0
    },
    75000: {
      from: 120,
      to: -5
    },
    100000: {
      from: 160,
      to: 15
    }
  };
  var $parent = $('.js-progressbar-mobile');
  var progressBar = $parent.find('[data-progress-bar]');
  var currentPPS = parseInt(progressBar.data('kf-points'));

  var $eliteAnimate = progressBar.find('.js-elite-animate');
  var $tooltip = $('.js-tooltip');

  var	tab = $('.js-tab');
  var	tabItem = tab.find('.tab-item');
  var	tabContent = tab.find('.tab-content');

  var isIE = function () {
    var userAgent = navigator.userAgent;
    return userAgent.indexOf('MSIE ') > -1 || userAgent.indexOf('Trident/') > -1 || userAgent.indexOf('Edge/') > -1;
  }

  var offsetMe = function (ele, val) {
    var animationInterval = setInterval(function () {
      if (!val) {
        clearInterval(animationInterval);
        return;
      }

      ele.style.strokeDashoffset = val;
      val--;
    }, 20);
  }

  var resetAnimation = function () {
    $('.js-checkmark').addClass('hidden');

    var maskEls = $parent.find('[data-point]');
    var point;
    var deg;
    maskEls.each(function(index, el) {
      point = $(el).data('point');
      deg = degMap[point];
      $(el).css({
        transform: 'rotate(' + deg.from + 'deg)'
      });
    });

    $tooltip.css('display', 'none');
  }

  var animateProgressBarByDeg = function(time, deg, showTooltip) {
    $(this).delay(time).animate({
      deg: deg,
    }, {
      duration: 300,
      easing: 'linear',
      step: function() {
        $(this).css({
          transform: 'rotate(' + deg + 'deg)'
        });
      },
      complete: function () {
        var index = parseInt($(this).data('index'));
        $($('.js-milestones-dot')[index]).find('.js-checkmark').removeClass('hidden');
        $($('.js-milestones-dot')[index]).find('.js-milestones-statistic').addClass('text-color-primary');

        if (isIE()) {
          fixAnimationIE(index);
        }

        if (showTooltip) {
          $('.js-current-statistic').text(currentPPS);
          setTimeout(function () {
            $('.js-statistic-box').removeClass('hidden');
          }, 1000);
        }
      }
    });
  };

  var animationProgressBar = function () {
    $eliteAnimate.addClass('elite-progress-bar');
    $tooltip.find('.current-number').text(currentPPS);

    var fixAnimationIE = function (idx) {
      var _selfCircle = document.querySelectorAll('.checkmark__circle'),
        _selfCheck = document.querySelectorAll('.checkmark__check');
      offsetMe(_selfCircle[idx], 166);
      offsetMe(_selfCheck[idx], 48);
    }

    var startAnimation = function () {
      var arr = [];
      var countAnimateEl;

      $eliteAnimate.each(function () {
        arr.push(parseInt($(this).data('point')));
      });

      for (var i = 0; i < arr.length; i++) {
        if (currentPPS <= arr[i]) {
          countAnimateEl = i + 1;

          break;
        }
      }

      var selfPoint;
      var deg;

      for (var i = 0; i < countAnimateEl; i++) {
        var self = $($eliteAnimate[i]);
        var delayTime = i * 700;

        selfPoint = parseInt(self.data('point'));

        if (i >= 1) {
          prePoint = parseInt($($eliteAnimate[i - 1]).data('point'));
        }

        deg = degMap[selfPoint];
        if (currentPPS >= selfPoint) {
          animateProgressBarByDeg.call(self, delayTime, deg.to, true);
        } else {
          var toDeg = (deg.from - deg.to) * (currentPPS - prePoint) / (selfPoint - prePoint);
          animateProgressBarByDeg.call(self, delayTime, toDeg, true);
        }
      }
    }

    startAnimation();
  };

  tabItem.each(function (idx) {
    $(this).off('click.initAnimation').on('click.initAnimation', function (e) {

      if ($(this).is('[data-tabprogressbar]') && !$(tabContent[idx]).is('.active')) {
        setTimeout(function () {
          animationProgressBar();
        }, 200);
      } else {
        $eliteAnimate.stop(true, true);
        resetAnimation();
      }

      $(this).addClass('active');
      $(this).siblings().removeClass('active');

      $(tabContent[idx]).addClass('active');
      $(tabContent[idx]).siblings().removeClass('active');

    });
  });

  return {
    animationProgressBar: animationProgressBar,
    resetAnimation: resetAnimation,
    offsetAnimation: offsetMe
  }
};

$(document).ready(function () {
  var manageProgressBar;
  var $vouchers = $('.js-elite-progress-bar');

  function initialize() {
    return SIA.AjaxCaller.template('elite-rewards-progress-bar.tpl').then(function (tplContent) {
      var template = window._.template(tplContent, {
        data: {},
      });

      $vouchers.html(template);

      setTimeout(function () {
        manageProgressBar = SIA.manageProgressBar();
        manageProgressBar.animationProgressBar();
      }, 100);
    }).fail(function (err) {
      console.log(err);
    });
  };

  initialize();
});


