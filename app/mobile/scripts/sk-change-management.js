var SIA = SIA || {};

SIA.skChangeManagement = function() {
    var p = {};

    p.overlayChangeManagementEl = $('.overlay-change-management');
    p.skChangeManagementSliderEl = $('.sk-change-management-slider');
    p.bodySkChangeManagementEl = $('.sk-change-management');

    // tablet images
    p.tabletImages = {
        '2': 'images/M-img-2-slider.png',
        '3': 'images/M-img-3-slider.png',
        '4': 'images/M-img-4-slider.png',
        '5': 'images/M-img-5-slider.png'
    };

    var init = function () {
        if (showOverlay()) {
            // remove scrolling on background
            p.bodySkChangeManagementEl.css('position', 'fixed');

            // show the overlay of change management
            p.overlayChangeManagementEl.removeClass('hidden');

            // change all images for mobile
            changeImagesForMobile();

            loadCloseBtnEvent();

            loadSlider();
        }
    };

    var showOverlay = function() {
        var cookie = document.cookie.replace(/(?:(?:^|.*;\s*)showCoachMarks\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        var val = false;

        if (cookie === '') {
            val = true;
        }

        return val;
    };

    var disableOverlay = function () {
        // don't show this overlay again
        document.cookie = 'showCoachMarks=false';
    };

    var changeImagesForMobile = function () {
        var imagesNum = 4;

        for (var i = 2; i <= imagesNum + 1; i++) {
            var el = $('.img-' + i + '-slider');
            var indexStr = i.toString();

            el.attr('src', p.tabletImages[indexStr]);
         }
    };

    var loadCloseBtnEvent = function() {
        p.overlayChangeManagementEl.find('.close-btn').on('click', function(e) {
            e.preventDefault();
            p.overlayChangeManagementEl.addClass('hidden');

            // add scrolling on background
            p.bodySkChangeManagementEl.css('position', '');

            // don't show this overlay again
            disableOverlay();
        });
    };

    var loadSlider = function() {
        p.nextBtnEl = $('.next-btn');

        p.skChangeManagementSliderEl.slick({
            dots: true,
            prevArrow: '',
            nextArrow: '',
            mobileFirst: true
        });

        function changeElemetStyle(currentSlide, nextSlide, el, className) {
            el.removeClass(className + currentSlide);
            el.addClass(className + nextSlide);
        }

        function changeSliderMainHeight(nextSlide) {
            var sliderMainEl = $('.sk-change-management-slider .slick-track');

            // get active slider item
            var activeSlide = p.skChangeManagementSliderEl.find('[data-slick-index="' + nextSlide + '"]')[0];

            // reset the height first to get an accurate measure
            sliderMainEl.css('height', 'auto');

            // change main slider height
            sliderMainEl.css('height', ($(activeSlide).height() + 20) + 'px');
        }

        function hideSlickDots() {
            p.skChangeManagementSliderEl.find('.slick-dots').addClass('hide');
        }

        // add default style
        var firstSlide = p.skChangeManagementSliderEl.slick('slickCurrentSlide');
        if (firstSlide == 0) {
            // dots styles
            changeElemetStyle('', 0, $('.slick-dots'), 'slick-dots-');

            changeSliderMainHeight(0);
        }

        // add on before change slider event
        p.skChangeManagementSliderEl.on({
            'beforeChange': function(event, slick, currentSlide, nextSlide){
                var currentSlideDot = p.skChangeManagementSliderEl.find('.slick-dots li');

                if (!currentSlideDot.hasClass('slick-active')) {
                    hideSlickDots();
                }

                // change dots style
                changeElemetStyle(currentSlide, nextSlide, $('.slick-dots'), 'slick-dots-');

                // add last button "start booking"
                if (nextSlide == 4) {
                    $('.start-booking').off().on('click', function (e) {
                        e.preventDefault();
                        p.overlayChangeManagementEl.addClass('hidden');

                        // add scrolling on background
                        p.bodySkChangeManagementEl.css('position', '');

                        // don't show this overlay again
                        disableOverlay();
                    });
                }

                changeSliderMainHeight(nextSlide);

                p.skChangeManagementSliderEl.find('.slick-dots').addClass('hide');
                p.curSlide = currentSlide;
            },
            'afterChange': function(event, slick, currentSlide){
                setTimeout(function(){
                    p.skChangeManagementSliderEl.find('.slick-dots').removeClass('hide');
                }, 1000);

                if(p.curSlide != currentSlide) $('.cm-wrapper').animate({scrollTop:0}, '200');
            }
        });

        $(window).on('touchmove', function() {
            hideSlickDots();
        });

        // add the next button event
        p.nextBtnEl.on('click', function() {
            p.skChangeManagementSliderEl.slick('slickNext');

            // put focus on top of the window
            $('.cm-wrapper').scrollTop(0);
        });
    };

    var oPublic = {
        init: init,
        public: p
    };

    return oPublic;
}();

$(function() {
    // wait for overlay loading to be finish
    var waitForOverlayLoading = function() {
       setTimeout(function() {
           if (! $('.overlay-loading').hasClass('hidden')) {
               waitForOverlayLoading();
           } else {
                SIA.skChangeManagement.init();
           }
       }, 100);
    };

    $('.overlay-loading').hasClass('hidden') ? SIA.skChangeManagement.init() : waitForOverlayLoading();
});
