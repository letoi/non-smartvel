/**
 * @name SIA
 * @description Define global initAutocompleteCity functions
 * @version 1.0
 */
SIA.initAutocompleteCity = function(){
	var global = SIA.global;
	var vars = global.vars;
	// var doc = global.vars.doc;
	var win = vars.win;
	var	config = global.config;
	var body = vars.body;
	//var isIETouch = vars.isIETouch();
	var originSelected;
	var destinationSelected;
	var countryOriginSelected;
	var countryDestinationSelected;

	var promotionObject = {};
	promotionObject.json = globalJson.promotionAamData;
	

	var getDestination = function(arrOforiginCode){
        var destinations = {};
        _.each(promotionObject.json.fareDeals.origin, function(origin){
            if(origin.originCode == arrOforiginCode[arrOforiginCode.length -1]){
                destinations = origin;
            }
        });
        return destinations;
	}
	
	var isExisted = function(destinationCityName,jsonDestinations){
		return _.chain(jsonDestinations)
			   .map(function(i){
				return i.destinationCityName.toLowerCase() == destinationCityName.toLowerCase();
			   })
			   .reduce(function(mem, i){
				return mem || i;
			   }, false).value();
	}

	var getSelectedOriginsDestination = function(wp){
		if($('body').hasClass('pass-through-aam-page') && wp.data('destination')){
			destinations = getDestination($('[name="search-fl-city-1"]').val().split(" "));
			$('ul#ui-id-5').addClass('drop-down-to-destination');
			$('ul#ui-id-5').find('li').each(function(){
				if(!isExisted($(this).data("value").substr(0,$(this).data("value").indexOf("*")),destinations.destination ) && $(this).data("value") != "No results found."){
					$(this).remove();
				}else if($(this).data("value") != "No results found."){
					var words = $(this).data("value").split("*");
					// console.log($(this).find('a').html('<span>'+words[0]+' - '+words[1]+' '+'</span> <span>'+words[3]+'</span><p>'+words[2]+'</p>'));
					// $(this).find('a').html('<span>'+words[0]+' - '+words[1]+' '+'</span> <span>'+words[3]+'</span><p>'+words[2]+'</p>');
					$(this).find('a').html('<div class="origin-city-list-container"><span class="origin-country-name">'+words[0]+' - '+words[1]+' '+'<p class="origin-airport-name">'+words[2]+'</p></div>' + '<div class="origin-code-container"><span class="origin-code">'+words[3]+'</span><div>');
				}
			});
		}else if($('body').hasClass('pass-through-aam-page') && wp.data('origin')){
			$('ul#ui-id-4').addClass('drop-down-from-destination');
			$('ul#ui-id-4').find('li').each(function(){
				if($(this).data("value") != "No results found."){
					var words = $(this).data("value").split("*");
					// console.log(words[3]);
					// console.log($(this).find('a').html('<span>'+words[0]+' - '+words[1]+' '+'</span> <span>'+words[3]+'</span><p>'+words[2]+'</p>'));
					// $(this).find('a').html('<span>'+words[0]+' - '+words[1]+' '+'</span> <span>'+words[3]+'</span><p>'+words[2]+'</p>');
					$(this).find('a').html('<div class="origin-city-list-container"><span class="origin-country-name">'+words[0]+' - '+words[1]+' '+'<p class="origin-airport-name">'+words[2]+'</p></div>' + '<div class="origin-code-container"><span class="origin-code">'+words[3]+'</span><div>');
				}
			});
		}
	};

	var _autoComplete = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			// containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		// that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.autocompleteFields = that.options.autocompleteFields;
		that.airportData = that.options.airportData;
		that.cityData = that.options.cityData;
		that.timer = null;
		that.timerResize = null;
		that.winW = win.width();

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('[data-autocomplete]');
			var inputEl = wp.find('input');
			var optionEl = wp.find('select option');

			if(wp.data('origin') && inputEl.val() !== '') {
				optionEl.each(function(){
					if($(this).data('text') === inputEl.val()) {
						countryOriginSelected = $(this).data('text');
						originSelected = $(this).data('country-exclusion');
					}
				});
			}

			if(wp.data('destination') && inputEl.val() !== '') {
				optionEl.each(function(){
					if($(this).data('text') === inputEl.val()) {
						countryDestinationSelected = $(this).data('text');
						destinationSelected = $(this).data('country-exclusion');
					}
				});
			}

			var bookingAutoComplete = field.autocomplete({
				minLength : wp.data("min-length") ? wp.data("min-length") : 0,
				open: that.options.open,
				change: that.options.change,
				select: that.options.select,
				close: that.options.close,
				search: that.options.search,
				response: that.options.response,
				// source: that.airportData,
				source: function(request, response) {
					/*// create a regex from ui.autocomplete for 'safe returns'
					 var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
					 // match the user's request against each destination's keys,

					 var match = $.grep(that.airportData, function(airport) {
					 var label = airport.label;
					 var value = airport.value;

					 return (matcher.test(label) || matcher.test(value)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
					 });

					 // ... return if ANY of the keys are matched
					 response(match);*/

					var listData = [],
						listCodeCheck = [];

					if(wp.data('origin')) {
						if(destinationSelected && destinationSelected !== '') {
							destinationSelected.indexOf(',') === -1 ? listCodeCheck.push(destinationSelected) : listCodeCheck = destinationSelected.split(',');
						}
						if(countryDestinationSelected && countryDestinationSelected !== '') {
							listCodeCheck.push(countryDestinationSelected);
						}
					}

					if(wp.data('destination')) {
						if(originSelected && originSelected !== '') {
							originSelected.indexOf(',') === -1 ? listCodeCheck.push(originSelected) : listCodeCheck = originSelected.split(',');
						}
						if(countryOriginSelected && countryOriginSelected !== '') {
							listCodeCheck.push(countryOriginSelected);
						}
					}

					if(!request.term) {
						listData = checkAvailableFlights(listCodeCheck, that.airportData);
						listData = checkGroupNoItem(listData);
						response(listData);
						getSelectedOriginsDestination(wp);
						return;
					}

					try {
						that.group = '';
						var matcher = new RegExp('(^' + $.ui.autocomplete.escapeRegex(request.term) + ')|(\\s' + $.ui.autocomplete.escapeRegex(request.term) + ')|([(]' + $.ui.autocomplete.escapeRegex(request.term) + ')', 'i');
						var match1 = [];
						if(that.cityData.length){
							match1 = $.grep(that.cityData, function(airport) {
								var label = airport.label;
								var valueOption = airport.valueOption;

								return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
							});
						}


						var match = $.grep(that.airportData, function(airport) {
							var label = airport.label || "";
							var valueOption = airport.valueOption || "";
							var value = airport.countrySelectedcity || "";
							if($("body").hasClass("cib-passenger-sk-page")){
								var link = airport.link;
								return (matcher.test(link) || matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
							}
							return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
						});

						listData = checkAvailableFlights(listCodeCheck, match);
						listData = checkGroupNoItem(listData);

						if(match1.length){
							match1 = checkAvailableFlights(listCodeCheck, match1);
							response(match1.concat(listData));
							getSelectedOriginsDestination(wp);
						}
						else{
							response(listData);
							getSelectedOriginsDestination(wp);
						}

					} catch(err) {
						response(that.airportData);
					}
				},
				position: that.options.position,
				appendTo : that.options.autoCompleteAppendTo
			}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function (ul, item) {
				if (item.city != "No results found.") {
					if (item.group) {
						that.group = item.group;
						return $('<li class="group-item">'+ item.group +'</li>')
							.appendTo(ul);
					} else {
						var link = item.link ? ' data-link="' + item.link + '"' : '';
						// since all cities are under a country/group, if item.parent does not belong to that.options.group, you can just prepend to ul
						if (item.parent === that.group) {
							return $('<li class="autocomplete-item"' + link + '>')
								.attr('data-value', item.city)
								.append('<a class="autocomplete-link">' + item.city +'</a>')
								.appendTo(ul);
						} else if (item.parent){
							return $('<li class="autocomplete-item redundancy"' + link + '>')
								.attr('data-value', item.city)
								.append('<a class="autocomplete-link">' + item.city +'</a>')
								.prependTo(ul);
						}
						return $('<li class="autocomplete-item"' + link + '>')
							.attr('data-value', item.city)
							.append('<a class="autocomplete-link">' + item.city +'</a>')
							.appendTo(ul);
					}

				} else if (item.city == "No results found.") {
					if($("body").hasClass("cib-passenger-sk-page")){
						return $('<li class="hidden"></li>').appendTo(ul);
					}	
					return $('<li class="autocomplete-item" data-value="No results found.">').append('<p style="padding-left: 10px;">No results found.</p>').appendTo(ul);
				}

			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			// bookingAutoComplete._move = function() {
			// 	that.group = '';
			// };


			/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
			 bookingAutoComplete._move = function( direction ) {
			 var item, previousItem,
			 last = false,
			 api = this.menu.element.data('jsp'),
			 li = $(),
			 minus = null,
			 currentPosition = api.getContentPositionY();
			 switch(direction){
			 case 'next':
			 if(this.element.val() === ''){
			 api.scrollToY(0);
			 li = this.menu.element.find('li:first');
			 item = li.addClass('active').data( 'ui-autocomplete-item' );
			 }
			 else{
			 previousItem = this.menu.element.find('li.active').removeClass('active');
			 li = previousItem.next();
			 item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
			 // console.log(currentPosition, previousItem.position().top);
			 }
			 if(!item){
			 last = true;
			 li = this.menu.element.find('li').removeClass('active').first();
			 item = li.addClass('active').data( 'ui-autocomplete-item' );
			 }
			 this.term = item.value;
			 this.element.val(this.term);
			 if(last){
			 api.scrollToY(0);
			 last = false;
			 }
			 else{
			 if(li.index() > that.options.itemShow - 1){
			 minus = Math.abs(previousItem.position().top - (this.menu.element.find('li:eq('+ (that.options.itemShow - 1) +')')).position().top);
			 if(Math.abs(currentPosition - minus) > 3){
			 api.scrollToY(minus);
			 currentPosition = api.getContentPositionY();
			 }
			 api.scrollToY(currentPosition + $(previousItem).outerHeight(true));
			 }
			 }
			 break;
			 case 'previous':
			 if(this.element.val() === ''){
			 last = true;
			 item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
			 }
			 else{
			 previousItem = this.menu.element.find('li.active').removeClass('active');
			 item = previousItem.prev().removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
			 }
			 if(!item){
			 last = true;
			 item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
			 }
			 this.term = item.value;
			 this.element.val(this.term);
			 if(last){
			 api.scrollToY(this.menu.element.find('.jspPane').height());
			 last = false;
			 }
			 else{
			 api.scrollToY(previousItem.position().top);
			 currentPosition = api.getContentPositionY();
			 api.scrollToY(currentPosition - $(previousItem).outerHeight(true));
			 }
			 break;
			 }
			 };
			 }*/

			field.autocomplete('widget').addClass('autocomplete-menu');
			/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
			 win.off('resize.blur'+index).on('resize.blur'+index, function(){
			 clearTimeout(that.timerResize);
			 that.timerResize = setTimeout(function(){
			 field.blur();
			 }, 100);
			 });
			 }*/

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function () {
				var self = $(this);
				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				// if(global.vars.isIE()){
				// 	doc.off('click.hideAutocompleteCity').on('click.hideAutocompleteCity', function(e){
				// 		if(!$(e.target).closest('.ui-autocomplete').length && !$(e.target).is('.ui-autocomplete-input')){
				// 			field.closest('.custom-select').removeClass('focus');
				// 			field.autocomplete('close');
				// 		}
				// 		if($('ul.ui-autocomplete:visible').length && $('ul.ui-autocomplete:visible').not(field.autocomplete('widget')).length){
				// 			$('ul.ui-autocomplete:visible').not(field.autocomplete('widget')).data('input').autocomplete('close');
				// 		}
				// 	});
				// }
				win.off('resize.repositionAutocompleteCity').on('resize.repositionAutocompleteCity', function(){
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function(){
						// if(field.autocomplete('widget').is(':visible')){
						// 	field.autocomplete('widget').css({
						// 		'left': field.closest('[data-autocomplete]').offset().left,
						// 		'top': field.closest('[data-autocomplete]').offset().top + field.closest('[data-autocomplete]').outerHeight(true)
						// 	});
						// }
						if(that.winW !== win.width()){
							field.trigger('blur.highlight');
							that.winW = win.width();
						}
					}, 100);
				});
			});

			field.off('blur.highlight').on('blur.highlight', function(){
				that.timer = setTimeout(function(){
					field.closest('.custom-select').removeClass('focus');
				}, 200);
				field.autocomplete('close');
				/*if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
				 win.off('resize.reposition');
				 }*/
				win.off('resize.repositionAutocompleteCity');

				if(field.val().length > 0) clearCloseBtn(field);
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
				e.stopPropagation();
			});

			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
				clearTimeout(that.timer);
				if(wp.hasClass('add-dest-to-favourites') && typeof SIA.FavouritesPage !== 'undefined') {
					SIA.FavouritesPage.addCityList(field);
				}
			});

			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					field.autocomplete('widget').find('li.active').trigger('click');
					e.preventDefault();
				}
			});

			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
		resetAvailableFlight();
	};

	var initAutocompleteCity = function(){
		// init autocomplete
		if ($("body").hasClass("flight-search")) {
			SIA.initAutocompleteCityNew();
			return;
		}
		
		var autocompleteEl = body.find('[data-autocomplete]');
		var timerAutocomplete = null;
		// var timerAutocompleteSearch = null;
		// var timerAutocompleteOpen = null;
		//var loadingStatus = config.template.loadingStatus;
		/*var initLoading = (global.vars.isIE() && global.vars.isIE() < 9);
		 if(initLoading){
		 loadingStatus = $(loadingStatus).appendTo(body);
		 }*/
		if(autocompleteEl.length){
			autocompleteEl.each(function(){
				var self = $(this),
					inputEl = self.find('input:text');

				if(self.data('init-automcomplete')){
					return;
				}
				if(self.find('input:text').is('[readonly]')){
					self.find('input:text').off('focus').off('click');
					return;
				}
				var select = self.find('select');
				var options = select.children();
				var data = [];
				var arrDataValue = [];
				var clearHideaddClear = null;

				// self.data('minLength', 2);
				self.data('init-automcomplete', true);
				self.find('input:text').focus(function() {
					// $(this).val('');
					/*var dataAutocomplete = inputEl.data('uiAutocomplete');
					 if(dataAutocomplete && inputEl.val()) {
					 var indexItem = $.inArray(inputEl.val(), arrDataValue);
					 clearTimeout(timerAutocompleteSearch);
					 timerAutocompleteSearch = setTimeout(function() {
					 if(indexItem === -1) {
					 dataAutocomplete.search();
					 } else {
					 inputEl.autocomplete('widget').find('li').eq(indexItem).addClass('active');
					 }
					 }, 200);
					 }*/
				});
				if(select.data('city-group')){
					var addData = function(els){
						els.each(function(){
							var indexOfAirport = $(this).text().indexOf('(');
							var valueOption = $(this).text().slice(indexOfAirport);
							data.push({
								city: $(this).text(),
								label: $(this).text(),
								value: $(this).data('text'),
								valueOption: valueOption,
								parent: $(this).parent().attr('label'),
								country: $(this).data('country'),
								link: $(this).data('link') || '',
								countryExclusion: $(this).data('country-exclusion')
							});
							arrDataValue.push({
								city: $(this).text(),
								label: $(this).data('text'),
								value: $(this).data('text'),
								valueOption: valueOption,
								country: $(this).data('country'),
								link: $(this).data('link') || '',
								countryExclusion: $(this).data('country-exclusion')
							});
						});
					};
					options.each(function(){
						var self = $(this);
						if(self.attr('label')){
							data.push({
								city: self.attr('label'),
								label: self.attr('label'),
								group: self.attr('label'),
								link: self.data('link') || '',
								// value: self.data('text')
								value: self.attr('label')
							});
						}
						else{
							data.push({
								city: self.text(),
								label: self.text(),
								value: self.data('text'),
								link: self.data('link') || '',
							});
						}
						if(self.children().length){
							addData(self.children());
						}
					});
				}
				else{
					options.each(function(){
						data.push({
							city: $(this).text(),
							label: $(this).text(),
							value: $(this).data('text'),
							link: $(this).data('link') || '',
						});
						// arrDataValue.push($(this).data('text'));
					});
				}
				select.off('change.selectCity').on('change.selectCity', function(){
					select.closest('[data-autocomplete]').find('input').val(select.find(':selected').data('text'));
				});

				var isFixed = false;
				self.parents().each(function() {
					isFixed = $(this).css('position') === 'fixed';
					return !isFixed;
				});

				_autoComplete({
					autocompleteFields : self.find('input:text'),
					autoCompleteAppendTo: body,
					airportData : data,
					cityData : arrDataValue,
					position: isFixed ? { collision: 'flip'} : { my: 'left top', at: 'left bottom', collision: 'none' },
					open: function(){
						var self = $(this);
						self.autocomplete('widget').find('.redundancy').remove();
						clearTimeout(clearHideaddClear);
						// self.addClear('hide');
						// self.autocomplete('widget').hide();
						if(isFixed){
							self.autocomplete('widget').css({
								position: 'fixed'
							});
						}
						// clearTimeout(timerAutocompleteOpen);
						// timerAutocompleteOpen = setTimeout(function(){
						// self.autocomplete('widget').show();
						// self.autocomplete('widget').show().css({
						// 	'left': self.closest('[data-autocomplete]').offset().left,
						// 	'top': self.closest('[data-autocomplete]').offset().top + self.closest('[data-autocomplete]').outerHeight(true)
						// });
						/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
						 self.autocomplete('widget')
						 .jScrollPane().off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
						 e.preventDefault();
						 });
						 }*/
						// }, 100);
					},
					response: function(event, ui){

						if (self.find(".select__text_styled").length) {
							$(event.target).css("opacity", "1");
							self.find(".select__text_styled").addClass("hidden");
						}

						if(ui.content.length ===1){
							// $(this).val(ui.content[0].value);
							// $(this).select();
						}
						if(!ui.content.length) {
							ui.content.push({
								city: L10n.globalSearch.noMatches,
								label: L10n.globalSearch.noMatches,
								value: null
							});
						}
					},
					search: function(){
						// var self = $(this);
						/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
						 self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
						 }*/
						// clearTimeout(timerAutocompleteSearch);
						// timerAutocompleteSearch = setTimeout(function(){
						// if(self.autocomplete('widget').find('li').length === 1){
						// self.autocomplete('widget').find('li').addClass('active');
						// }
						// }, 100);
					},
					close: function(){
						var self = $(this),
							selfAutoComplete = self.parents('[data-autocomplete]');
						/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
							$(this).autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
						}*/
						selfAutoComplete.removeClass('focus');
						
						var targetEl = event.target;
						var styledElement = selfAutoComplete.find(".select__text_styled");
						if (styledElement.length && self.val()) {
							var optionValue = _.map(selfAutoComplete.find("select").find("option"), function(el) {
								el = $(el);
								var value = el.text().split("*");
								return (value[0] + " - " + value[3]).toLowerCase();
							});
							var searchText = optionValue.indexOf(self.val().toLowerCase());
							if (searchText != -1) {
								self.css("opacity", "0");
								styledElement.removeClass("hidden");
							} else {
								self.val("");
							}
						} 

						// if(self.rules().required && this.defaultValue && !this.value) {
						// 	self.val(this.defaultValue).valid();
						// }

						if(!self.val() || self.val() === self.attr('placeholder')){
							self.closest('[data-autocomplete]').addClass('default');
						}

						// Fix bug for iPad
						if(/iPad/i.test(window.navigator.userAgent)) {
							setTimeout(function(){ $(document).scrollTop(win.scrollTop() + 1); }, 100);
						}
						clearHideaddClear = setTimeout(function(){
							self.blur();
							self.addClear('hide');
						}, 200);


						if(self.data('autopopulate')){
							$(self.data('autopopulate')).val(self.val()).closest('[data-autocomplete]').removeClass('default');
							if(self.parents('.grid-col').hasClass('error')){
								$(self.data('autopopulate')).valid();
							}
						}

						if(self.data('autopopulateholder')){
							self.val(self.val() || self.data('autopopulateholder')).closest('[data-autocomplete]').removeClass('default');
							self.valid();
						}
					},
					select: function (event, ui) {
						// $(this).addClear('hide');
						var self = $(this),
							selfAutoComplete = self.parents('[data-autocomplete]'),
							searchBox = $('input', selfAutoComplete);

						var targetEl = event.target;
						var styledElement = selfAutoComplete.find(".select__text_styled");
						var itemValue = [];
						if (styledElement.length) {
							$(targetEl).css("opacity", "0");
							styledElement.removeClass("hidden");
							itemValue = _.map(ui.item.city.split("*"), function (i) {
								return $.trim(i);
							});
							styledElement.find(".styled-city").text(itemValue[0]);
							styledElement.find(".styled-code").text(itemValue[3]);
						} 

						if (ui.item.link) {
							searchBox.data('link', ui.item.link);
						}

						if(!ui.item.value) {
							window.setTimeout(function() {
								self.trigger('blur.triggerByGroupValidate');
							}, 400);
							return;
						} else {
							if(self.closest('#travel-widget').data('widget-v1') || self.closest('#travel-widget').data('widget-v2')) {
								$('#travel-widget .from-select, #travel-widget .to-select').not(selfAutoComplete).removeClass('default');
								if(selfAutoComplete.is('.from-select')) {
									$('#travel-widget .from-select').not(selfAutoComplete).find('input').val(ui.item.value);
								} else if(selfAutoComplete.is('.to-select')) {
									$('#travel-widget .to-select').not(selfAutoComplete).find('input').val(ui.item.value);
								}
							};
						}

						self.closest('[data-autocomplete]').removeClass('default');
						//var wrapper = self.closest('div');
						/*if(initLoading){
						 loadingStatus.css({
						 'width': wrapper.outerWidth(true),
						 'height': wrapper.outerHeight(true),
						 'top': wrapper.offset().top,
						 'left': wrapper.offset().left,
						 'display': 'block'
						 });
						 }*/
						if(self.parents('.from-select').length){
							originSelected = ui.item.countryExclusion;
							countryOriginSelected = ui.item.value;
							self.closest('.form-group').data('change', true);
							if(self.parents('.from-to-container').find('.to-select').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									},100);
								}
							}
							else{
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										//self.blur();
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									}, 100);
								}
							}
						}
						if(self.parents('.to-select').length){
							destinationSelected = ui.item.countryExclusion;
							countryDestinationSelected = ui.item.value;
							if($('#travel-radio-4').length && $('#travel-radio-5').length && ($('#travel-radio-4').is(':visible') || $('#travel-radio-5').is(':visible'))){
								if(self.is('#city-2')){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										if($('#travel-radio-4').is(':checked')){
											self.blur();
											setTimeout(function(){
												// $('#travel-start-day').closest('.input-3').trigger('click.showDatepicker');
												$('#travel-start-day').focus();
											}, 300);
										}
										else if($('#travel-radio-5').is(':checked')){
											self.blur();
											setTimeout(function(){
												// self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
												self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').focus();
											}, 300);
										}
										// if(self.closest('form').data('validator')){
										// 	self.closest('.form-group').find('input').valid();
										// }
									}, 100);
								}
							}
							else if(self.closest('form').find('[data-return-flight]').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										if(self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')){
											self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
										}
										else{
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.blur();
										setTimeout(function(){
											// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
											if(self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')){
												self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
											}
											else{
												self.closest('form').find('.form-group').find('[data-oneway]').focus();
											}
										}, 300);
									}, 100);
								}
							}
							else if(self.closest('form').find('.form-group').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.blur();
										setTimeout(function(){
											// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
											if(!$('body').hasClass('multi-city-page')){
												self.closest('form').find('.form-group').find('[data-oneway]').focus();
											}else{
												self.closest('.form-group').find('[data-oneway]').focus();
											}
										}, 300);
									}, 100);
								}
							}
							else if(self.parents('.from-to-container').children('[data-trigger-date]').length) {
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 100);
								}
							}
							else if(self.closest('[data-flight-schedule]').length){
								var dtfs = self.closest('[data-flight-schedule]');
								if(dtfs.find('[data-return-flight]').length){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										dtfs.find('[data-start-date]').focus();
									}, 500);
								}
								else if(dtfs.find('[data-oneway]').length){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										dtfs.find('[data-oneway]').focus();
									}, 500);
								}
							}
						}
						setTimeout(function(){
							if(self.closest('form').data('validator')){
								self.valid();
							}
							/*if(initLoading){
							 loadingStatus.hide();
							 }*/
							self.closest('[data-autocomplete]').siblings('.mobile').find('.select__text').text(ui.city).siblings('select').val(ui.value);
							self.trigger('change.programCode');
							if (styledElement.length) {
								searchBox.val(itemValue[0] + " - " + itemValue[3]);
							}
						}, 200);
					},
					setWidth: 0
				});

				if(inputEl.val() === '' || inputEl.val() === inputEl.attr('placeholder')){
					self.closest('[data-autocomplete]').addClass('default');
				} else {
					self.closest('[data-autocomplete]').removeClass('default');
				}
			});

			win.off('touchmove.closeAutocomplete').on('touchmove.closeAutocomplete', function() {
				body.find('.focus[data-autocomplete] input').trigger('blur.highlight');
			});
		}
	};

	var checkAvailableFlights = function(listCode, match) {
		var listData = match;
		var filterDataOrigin = [];

		_.each(listCode, function(code, idx){
			filterDataOrigin = $.grep(listData, function(item){
				if(idx !== listCode.length - 1) {
					if(item.country) {
						return item.country !== code;
					}
				} else {
					if(item.value) {
						return item.value !== code;
					}
				}
				return item;
			});
			listData = filterDataOrigin;
		});

		return listData;
	};

	var checkGroupNoItem = function(listData){
		var isCity = true;

		_.each(listData, function(data){
			isCity = data.value ? true : false;
		})
		if(!isCity) listData = [];

		var newArr = [];
		_.each(listData, function(data){
			!data.value ? newArr.push(data.label) : '';
			// data.value === data.label ? newArr.push(data.label) : '';
		})

		_.each(newArr, function(country){
			var hasCity = false;
			for(var i = 0; i < listData.length; i ++) {
				if(listData[i].parent === country) {
					hasCity = true;
					break;
				}
			}
			if(!hasCity) {
				_.each(listData, function(data, i){
					if(data.label === country) {
						listData.splice(i, 1);
					}
				});
			}
		});

		return listData;

	}

	var resetAvailableFlight = function(){
		$('[data-autocomplete]').each(function(){
			var _self = $(this),
				inputEl = _self.find('input'),
				clearTextEl = _self.find('.add-clear-text'),
				timeout;

			if(timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function() {
				if(_self.data('origin')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e){
						if(e.keyCode === 8) {
							originSelected = '';
							countryOriginSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function(){
						originSelected = '';
						countryOriginSelected = '';
					});
				}
				if(_self.data('destination')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e){
						if(e.keyCode === 8) {
							destinationSelected = '';
							countryDestinationSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function(){
						destinationSelected = '';
						countryDestinationSelected = '';
					});
				}
			}, 1000);

		});
	};

	var clearCloseBtn = function(el) {
		var closeIconEl = $('<a href="#clear" class="ico-cancel-thin auto-complete-close add-clear-text" tabindex="-1"><span class="ui-helper-hidden-accessible">clear</span></a>');
		var parent = el.closest('[data-autocomplete="true"]');

		var closeIcon = parent.find('.auto-complete-close');
		
		if(parent.hasClass('add-dest-to-favourites')) {
			if (closeIcon.length >= 1) {
				closeIcon.removeClass('hidden');
				closeIconEl.off().on('click', function(e) {
					e.preventDefault();
	
					el.val('');
					
					$(this).addClass('hidden');
				});
			} else {
				closeIconEl.off().on('click', function(e) {
					e.preventDefault();
	
					el.val('');
					el.trigger('blur');

					$(this).addClass('hidden');
				});
	
				parent.find('.select__text').after(closeIconEl);
			}
		}
	};

	// init
	initAutocompleteCity();
};
