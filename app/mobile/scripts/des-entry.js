/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */
SIA.desEntry = function(){
	var global = SIA.global,
		win = global.vars.win,
		// config = global.config,
		btnSeeMore = $('.country-button [data-see-more]'),
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		isLandscape = win.width() > win.height(),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startMbLanNum = 0,
		startMbPosNum = 0,
		defaultMbPosNum = 9,
		seeMoreMbPosNum = 3,
		defaultMbLanNum = 8,
		seeMoreMbLanNum = 4,
		seeMoreCount = 0;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region-categories');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var changeRegion = function(regionVal) {
		initCity(res.cities, (regionVal != 1 ? regionVal : null));
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		cities = _.sortBy(cities, function(o) { return o.city; });
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg == region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container.removeAttr('style')
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.removeAttr('style')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region-categories') == region || region == "1") &&
					(!city || self.data('city').toLowerCase() == city.toLowerCase());
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);

		var state = (!isLandscape ? (startMbPosNum = 0) : (startMbLanNum = 0)) || fillContent(false);
		return state;
	};

	var generateTemplate = function(tpl, resLen) {
		var searchKey = $("#search-city").val();
		if (!searchKey) {
			_.each(tpl, function(el) {
				el = $(el);
				if (!el.hasClass("col-mb-3")) {
					el.addClass("col-mb-3");
				}
				if (!el.hasClass("shuffle-item")) { 
					el.addClass("shuffle-item");
				}
			});
			var canLoadMore = container.find(".static-item:not(.shuffle-item)").length;
			if (!canLoadMore) {
				SIA.desEntryFilter.canLoadMoreFn(false);
				$(".country-button").addClass("hidden");
			}
			SIA.desEntryFilter.arrangeTpl();
		} else {
			SIA.desEntryFilter.resetFilters();
		}

		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.addClass('hidden');
			$('.country-button-border').addClass('hidden');

			return !isLandscape ? (startMbPosNum = resLen) : (startMbLanNum = resLen);
		}
		else {
			// btnSeeMore.text(seeMoreCount === 2 ? L10n.kfSeemore.seeAll : L10n.kfSeemore.loadMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx, resLen) {
		var tpl = $(".static-item");
		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');
				if(!img.data('loaded')) {
					img.one('load.loadImg', function() {
						self.removeClass('hidden');
						img.data('loaded', true);
						if (!tpl.filter('.hidden').length) {
							generateTemplate(tpl, resLen);
						}
					}).attr('src', img.data('img-src'));
				}

				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(tpl, resLen);
					}
				}
			});
		}
	};

	var fillContent = function(isResize, setNumber) {
		if (staticItems.length) {
			var endIdx = 0;
			var resLen = staticItems.length;

			if (isResize) {
				container.removeAttr('style');
				generateClass(staticItems);
			}

			if (isLandscape) {
				endIdx = !startMbLanNum ?
					(seeMoreCount > 2 ?	resLen : (setNumber ? setNumber * 4 : startMbLanNum + defaultMbLanNum)) :
					(seeMoreCount > 2 ? resLen : startMbLanNum + seeMoreMbLanNum);
				startMbLanNum = endIdx;
			}
			else {
				endIdx = !startMbPosNum ?
					(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 2 : startMbPosNum + defaultMbPosNum)) :
					(seeMoreCount > 2 ? resLen : startMbPosNum + seeMoreMbPosNum);
				startMbPosNum = endIdx;
			}

			getTemplate(staticItems, endIdx, resLen);
		}
	};

	var generateClass	= function(res) {
		res.removeAttr('style').addClass('hidden col-mb-3');
	};

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		SIA.desEntryFilter.resetFilters();
	});

	win.off('resize.template').on('resize.template', function() {
		if(win.width() <= win.height()){
			if(isLandscape) {
				isLandscape = false;
				var setNumber =  Math.ceil(startMbLanNum / 4);
				startMbPosNum = 0;
				fillContent(true, setNumber);
			}
		}
		else {
			if (!isLandscape) {
				isLandscape = true;
				var setNumber = Math.ceil(startMbPosNum / 2);
				startMbLanNum = 0;
				fillContent(true, setNumber);
			}
		}
	});

	var canClick = function(status) {
		preventClick = false;
	};

	var initModule = function(){
		var public = {
			renderTemplate: renderTemplate,
			changeRegion: changeRegion,
			canClick: canClick
		};

		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);
		SIA.desEntryFilter.init(public);
	};

	initModule();
};

SIA.desEntryFilter = function() {
	var p, widthEl = 0, heightEl = 0, canLoadMore = true, mobileLimit = 6, mobileSeeMore = 6, step = 1;

	var addInspireMeListeners = function() {
		step = 1;
		addInspireFiltersListeners();
	};

	var addInspireFiltersListeners = function() {
		$(".destination-inspireme-category").find("[type='radio']").off().on("click", function(evt) {
			step = 1;
			filterTriggers();
			regionTriggers();
		});

		step = 1;
		filterTriggers();
		regionTriggers();
	};

	var addRegionListeners = function() {
		$(".destination-regions-categories").find("input").off().on("change", function(evt) {
			step = 1;
			
			filterTriggers();
			regionTriggers();
		});
	};

	var regionTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var regionFilters = getRegionSelected();
		regionFilters = regionFilters.length ? regionFilters : ["all"];
		var filteredElements = getAllFilterBy(regionFilters, "region-categories");
		var showElLimit = showLoadMore(filteredElements);
		if (showElLimit.hidden.length) {
			removeAllHidden($(".country-button"));
		} else {
			$(".country-button").addClass("hidden");
		}
		toggleElements(showElLimit.display);
		var displayLength = showElLimit.display.length;
		var totalLength = showElLimit.display.length + showElLimit.hidden.length;
		$(".no-search-found")[displayLength ? "addClass" : "removeClass"]("hidden");
		$(".search-found").find(".no-search").text(totalLength);
		$(".search-found").find(".no-search").next().text((displayLength < 2) ? "destination found" : "destinations found");
		$(".country-button").find(".display-item").text(displayLength);
		$(".country-button").find(".total-item").text(totalLength);
		removeSearchText();
	};

	var removeSearchText = function() {
		$("#search-city").val("");
	};

	var showLoadMore = function(filteredElements) {
		var mobileLoad = 1, stepValue = 0;
		if (step == 1) {
			mobileLoad = mobileLimit;
			stepValue = mobileLoad * step;
		} else {
			mobileLoad = mobileSeeMore;
			stepValue = mobileLimit + (mobileSeeMore * (step - 1));
		}
		var displayEl = filteredElements.splice(0, stepValue);
		_.each(filteredElements, function(el) {
			el = $(el);
			el.addClass("hidden");
		});

		return {
			display: displayEl,
			hidden: filteredElements
		};
	};

	var removeAllHidden = function(el) {
		el.removeClass("hidden");
		el.find(".hidden").each(function() {
			var $this = $(this);
			$this.removeClass("hidden");
		});
	};

	var filterTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var filteredElements;
		var inspireFilter = getInspiredClick();
		
		filteredElements = getAllFilterBy([inspireFilter], "inspire-categories");
		toggleElements(filteredElements);
		p.canClick(false);
	};

	var toggleElements = function(elementList) {
		removeStatic();
		var step = 0;
		var stepValue = heightEl;
		for (var i = 0 ; i < elementList.length ; i ++ ) {
			var firstEl = $(elementList[i]);
			
			var topValue = (stepValue * step);

			if (firstEl) {
				firstEl.css("left", "0px");
				firstEl.css("top", topValue + "px");
				if (i == 0) {
					firstEl.addClass("static-item--large col-mb-6");
				}
				firstEl.removeClass("hidden");
				if (!firstEl.hasClass("col-mb-3")) {
					firstEl.addClass("col-mb-3");
				}
			} 
			
			step ++;
		}
		// $(".static-block--item").css("height", (stepValue * step) + "px");
	};

	var removeStatic = function() {
		var staticEL = $(".static-block--item").find(".static-item--large");
		staticEL.removeClass("static-item--large col-mb-6");
		staticEL.addClass("hidden");
	}

	var getAllFilterBy = function(filters, dataAttr) {
		var regexString = _.map(filters, function(i) {
			return "(" + i + ")";
		}).join("|");
		var regex = new RegExp(regexString, "i");
		var allVisibleElements = $("[data-" + dataAttr + "]:visible");
		var hiddenElements = $("[data-" + dataAttr + "].hidden");
		if (dataAttr != "region-categories") {
			allVisibleElements = $.merge(allVisibleElements, hiddenElements);
		} 
		var filteredElements = _.filter(allVisibleElements, function(el) {
			el = $(el);
			
			var containsValue = regex.test(el.data(dataAttr));
			if (containsValue || filters[0] == "all") {
				return el;
			} else {
				el.addClass("hidden");
			}
		});
		
		return filteredElements;
	};

	var getRegionSelected = function() {
		var allCustomRegion = $(".custom--region");
		var clickedFilters = [];
		_.each(allCustomRegion, function(el, idx) {
			el = $(el);
			
			if (el.find("input").is(":checked")) {
				clickedFilters.push(el.data("custom-region"));
			}
		});
		
		return clickedFilters;
	};

	var getInspiredClick = function() {
		var checkedElement = $(".inspireme-radio-filter").find("[type='radio']:checked");
		return checkedElement.val();
	};

	var toggleCheckbox = function(status) {
		var allCustomRegion = $(".custom--region");
		_.each(allCustomRegion, function(el, idx) {
			el = $(el);

			if (el.find("input").is(":checked") == status) {
				el.find("label").trigger("click");
			}
		});
	};

	var arrangeTpl = function() {
		filterTriggers();
		regionTriggers();
	};

	var canLoadMoreFn = function(status) {
		canLoadMore = status;
	};

	var resetFilters = function() {
		var city = $("#search-city").val();
		$("#radio-inspireme-all").prop("checked", true);
		toggleCheckbox(true);
		if (city) {
			$(".block-search").css("opacity", "0");
			setTimeout(function() {
				$(".block-search").css("opacity", "1");
				displaySearchItem(city);
			}, 10);
		} else {
			displaySearchItem(city);
		}
	};

	var displaySearchItem = function(city) {
		var showSearch = [];
		$(".static-item[data-city]").each(function() {
			var $this = $(this);
			if ($this.data("city").toLowerCase() == city.toLowerCase()) {
				showSearch.push($this);
			} else {
				$this.addClass("hidden");
			}
		});
		toggleElements(showSearch);
		$(".no-search-found")[showSearch.length ? "addClass" : "removeClass"]("hidden");
		$(".search-found").find(".no-search").text(showSearch.length);
		$(".search-found").find(".no-search").next().text((showSearch.length < 2) ? "destination found" : "destinations found");
		$(".country-button").addClass("hidden");
	};

	var loadMoreButton = function() {
		$("[data-see-more]").off().on("click", function(evt) {
			evt.preventDefault();
			evt.stopPropagation();
			step ++;
			filterTriggers();
			regionTriggers();
		});
	};

	var resetSearchButtonListener = function() {
		$(".reset-search-button").off().on("click", function(evt) {
			evt.preventDefault();
			resetFilters();
			if($("#radio-inspireme-all").prop("checked")) {
				setTimeout(function(){
					filterTriggers();
					regionTriggers();
				}, 200);
			}else {
				$("#radio-inspireme-all").trigger("click");
			}
		});
	};

	var init = function(public) {
		p = public;
		addInspireMeListeners();
		addRegionListeners();
		loadMoreButton();
		resetSearchButtonListener();
		// p.renderTemplate();
	};

	return {
		init: init,
		arrangeTpl: arrangeTpl,
		canLoadMoreFn: canLoadMoreFn,
		resetFilters: resetFilters,
		displaySearchItem: displaySearchItem
	}
}();

