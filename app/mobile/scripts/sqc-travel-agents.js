/**
 * @name SIA
 * @description Define global passengerDetail functions
 * @version 1.0
 */
SIA.travelAgentData = {};

SIA.travelAgent = (function() {
	function getTravelAgentData() {
		SIA.AjaxCaller.json('high-flyer/landing-result-search.json').then(function(data) {
			// logic standard data
			SIA.travelAgentData.json = data;
		}).fail(function(err) {
			console.error(err);
		});
	}

	function getTravelAgentTemplate() {
		SIA.AjaxCaller.template('sqc-travel-agents-result.tpl').then(function(tplContent) {
			SIA.travelAgentData.template = tplContent;
		}).fail(function(err) {
			console.error(err);
		});
	}

	getTravelAgentData();
	getTravelAgentTemplate();
})();

$(document).ready(function() {
	var loadMoreClass = '.js-agents-loadmore';
	var agentSearchInputClass = '.js-agent-search-input';
	var agentItemClass = '.js-agent-item';
	var checkboxAgentNameClass = '.js-checkbox-agent-name';
	var labelContentClass = '.js-label-content';
	var travelAgentResultClass = '.js-travel-agents-result';
	var hasResultClass = '.js-has-result';
	var agentDataClass = '.js-agent-data';
	var agentBadgeClass = '.js-agent-badge';
	var addAgentBtnClass = '.js-add-agent';
	var noResultClass = '.js-no-result';
	var agentPopupClass = '.js-agent-popup';
	var yourTravelAgentClass = '.js-travel-agents';
	var removeTravelAgentClass = '.js-remove-agent';
	var sortTravelAgentClass = '.js-sort-travel-agent';
	var hiddenClassName = 'hidden';
	var agentsSelected = [];

	var ruleDigits;
	var messageRequired = $(agentSearchInputClass).data('msg-required');

	// find type of input related with radio button
	var cboAgentName = $(checkboxAgentNameClass).is(':checked');
	var $labelContent = $(labelContentClass);
	var $inputSearch = $(agentSearchInputClass);
	var matchedItems = [];

	$(agentSearchInputClass).on('focus', function () {
		$(this).parent().addClass('focus');
	});

	$(agentSearchInputClass).on('focusout', function () {
		$(this).parent().removeClass('focus');
	});
	
	$('input[type=radio][name="travel-agent"]').change(function() {
		clearAgentsSelected();
		$inputSearch.val('');
		removeError(this);

		$(travelAgentResultClass).addClass(hiddenClassName);

		ruleDigits = this.value === 'agentRegNum';

		messageRequired = 'Please enter a Travel agent’s name';

		var type = 'text';
		var text = 'Enter a Travel agent’s name';
    if (ruleDigits) {
			type = 'tel';
			text = 'Enter travel agent’s IATA/ARC number';
			messageRequired = 'Please enter a valid IATA/ARC number';
		}

		$labelContent.text(text);
		$inputSearch.attr('type', type);
	});

	$(agentSearchInputClass).on('keypress', function (event) {
		if (ruleDigits && event.which !== 13){
			inputNumberOnly();
		}

		var inputValue = $(this).val();

		if (!inputValue) {
			return;
		}

	});

	if (cboAgentName) {
		$labelContent.text('Enter Travel agent’s name');

	} else {
		$labelContent.text('Enter travel agent’s IATA/ARC number');
	}

	// form search submit
	$(document).on('submit', 'form[name="form-search-travel-agent"]',function(e) {
		e.preventDefault();
		clearAgentsSelected();
		removeError($(agentSearchInputClass));
		$(travelAgentResultClass).removeClass(hiddenClassName);
		// get value from text search
		// get type search
		var searchValue = $(agentSearchInputClass).val().toLowerCase();
		var searchType = $('input[type="radio"][name="travel-agent"]:checked').val();

		if (!searchValue) {
			// show error or warning
			showError($(this).find(agentSearchInputClass), messageRequired);
			$(travelAgentResultClass).addClass(hiddenClassName);
			resetPositionLightBox();
			return;
		}

		// get all items
		var data = SIA.travelAgentData.json.TravelAgents.TravelAgent;
		var template = window._.template(SIA.travelAgentData.template, {
			data: data
		});

		// light box result
		$(travelAgentResultClass).html(template);

		var $noResultBox = $(noResultClass);
		// hidden all iems
		$(agentItemClass).addClass(hiddenClassName);
		$noResultBox.addClass(hiddenClassName);
		$(loadMoreClass).parent().addClass(hiddenClassName);

		// search in SIA.travelAgent.data
		if (searchType === 'agentName') {
			matchedItems = data.filter(function(el) {
				return el.agentName.toLowerCase().indexOf(searchValue) > -1
			});
		}

		if (searchType === 'agentRegNum') {
			matchedItems = data.filter(function(el) {
				return el.agentRegNum.toLowerCase().indexOf(searchValue) > -1
			});
		}

		if (matchedItems.length) {
			$.each(matchedItems, function(index) {
				if (index >= 5) {
					$(loadMoreClass).parent().removeClass(hiddenClassName);
					return;
				}
				$('#'+ this.agentID).removeClass(hiddenClassName);
			});
		}

		if (!matchedItems.length) {
			$(hasResultClass).addClass(hiddenClassName);
			$('form[name="formAgentsSelected"]').addClass(hiddenClassName);
			$noResultBox.removeClass(hiddenClassName);
		}
		resetPositionLightBox();
	});

	// loadmore
	$(document).on('click', loadMoreClass, function() {
		var maxItem = $(agentDataClass).data('max-items');
		var flag = 0;

		$.each(matchedItems, function(index) {
			var isHiddenItem = $(agentItemClass + '.hidden').is('#' + this.agentID);
			if (flag >= maxItem) {
				return;
			};

			if (isHiddenItem) {
				flag++;
				$('#' + this.agentID).removeClass(hiddenClassName);
			};
		});

		var showItems = $(agentItemClass).not('.hidden');
		if (showItems.length == matchedItems.length) {
			$(loadMoreClass).parent().addClass(hiddenClassName);
		};
		// reset position lightBox
		resetPositionLightBox();
	});

	// reset position lightbox
	function resetPositionLightBox() {
		var $innerPopup = $('.js-popup-search');
		var heightWindow = $(window).outerHeight();
		var heightInnerPopup = $innerPopup.outerHeight();

		$innerPopup.css({
			top: heightInnerPopup > heightWindow ? 0 : (heightWindow - heightInnerPopup)/2 + 'px',
			transform: 'none'
		});
	}

	// choice agent item
	$(document).on('click', agentItemClass, function() {
		var $agentItem = $(this);
		var isBtnAdd = $agentItem.find(addAgentBtnClass).val() === 'Add';
		var agentId = $agentItem.attr('id');
		var indexOfAgent = agentsSelected.indexOf(agentId);
		
		if (agentsSelected.indexOf(agentId) > -1) {
			agentsSelected.splice(indexOfAgent,1);
		} else {
			agentsSelected.push(agentId);
		}
		$('input[name="agentsSelected"]').val(JSON.stringify(agentsSelected));

		$agentItem.toggleClass('selected');
		$agentItem.find(agentBadgeClass).toggleClass(hiddenClassName);
		$agentItem.find(addAgentBtnClass).val(isBtnAdd ? 'Remove' : 'Add');
	});

	// submit array agent item added
	$(document).on('submit', 'form[name="formAgentsSelected"]', function(e) {
		e.preventDefault();
		$('input[name="agentsSelected"]').val(JSON.stringify(agentsSelected));
		$(agentPopupClass).Popup('hide');
		return false;
	});

	// single choice
	function resetState(btnRef) {
		var $agentItems = $(addAgentBtnClass).not(btnRef).closest(agentItemClass);
		$agentItems.removeClass('selected');
		$agentItems.find(addAgentBtnClass).not(btnRef).val('Add');
		$agentItems.find(agentBadgeClass).addClass(hiddenClassName);
	};

	// your travel agent
	var $yourTravelAgents = $(yourTravelAgentClass);
	var tplYourTravelAgent;
	var dataYourTravelAgent;
	var sortKey = 'asc';

	// gen tpl of your travel agents
	function initialize() {
		return SIA.AjaxCaller.template('sqc-travel-agents.tpl').then(function(tplContent) {
			return 	SIA.AjaxCaller.json('high-flyer/landing.json').then(function(data) {
					tplYourTravelAgent = tplContent;
					dataYourTravelAgent = data.TravelAgents.TravelAgent;

					var template = window._.template(tplContent, {
						data: sortAgentsByName(dataYourTravelAgent, sortKey),
					});
					$yourTravelAgents.html(template);
					sortKey = 'desc';
				});
			}).fail(function(err) {
				console.log(err);
		});
	}

	initialize();
	// sort data in your travel agents
	$(document).on('click', sortTravelAgentClass, function() {
		var dataSort;

		if (sortKey === 'asc') {
			dataSort = sortAgentsByName(dataYourTravelAgent, sortKey);
			sortKey = 'desc';
		} else {
			dataSort = sortAgentsByName(dataYourTravelAgent, sortKey);
			sortKey = 'asc';
		}
		var template = window._.template(tplYourTravelAgent,{ data: dataSort });
		$yourTravelAgents.html(template);

		if (sortKey === 'asc') {
			$(sortTravelAgentClass).removeClass('active');
		} else {
			$(sortTravelAgentClass).addClass('active');
		}
	});

	// remove travel agent
	$(document).on('click', removeTravelAgentClass, function() {
		var agentID = $(this).data('agent-id');
		if (!agentID) {
			return;
		}

		var url = '/api/v1/travel-agents/' + agentID;

		SIA.AjaxCaller.api(url, 'DELETE', {}).then(function() {
			// TODO: implement logic after deleted travel agent
		}).fail(function(err) {
			console.error(err);
		});
	});

	function sortAgentsByName(arr, key) {
		return arr.sort(function(a,b) {
			var aName = a.agentName.toLowerCase();
			var bName = b.agentName.toLowerCase();
			var condition = (aName > bName);

			if (key === 'asc') {
				condition = !condition;
			}

			return condition ? -1 : 1;
		});
	}

	function inputNumberOnly(evt) {
		var theEvent = evt || window.event;
		var key = (theEvent.keyCode || theEvent.which || theEvent.charCode);
		key = String.fromCharCode(key);
	
		var regex = /[0-9]/;
		if (!regex.test(key)) {
			theEvent.returnValue = false;
			if (theEvent.preventDefault) theEvent.preventDefault();
		}
	};

	function removeError(elm) {
		var $agentWrapper = $(elm).closest('.sqc-travel-agent');
		$agentWrapper.removeClass('error');
		$agentWrapper.find('.set-error-message').addClass(hiddenClassName);
	};

	function showError(elm, message) {
		var $agentWrapper = $(elm).closest('.sqc-travel-agent');
		$agentWrapper.addClass('error');
		$agentWrapper.find('.set-error-message').removeClass(hiddenClassName).text(message);
	};

	function clearAgentsSelected() {
		$('input[name="agentsSelected"]').removeAttr('value');
		agentsSelected = [];
	}
});
