/**
 * @name SIA
 * @description Define global initPersonTitles functions
 * @version 1.0
 */
SIA.initPersonTitles = function(){
	/* Generate an <option> element */
	var generateOption = function(jsonData, isSelected, initValue) {
		var options = '';
		isSelected = isSelected || false;
		for(var i = 0; i < jsonData.length; i++) {
			options += '<option ' + ((isSelected && i === 0 && !initValue)
				|| (initValue && jsonData[i] === initValue) ?
					'selected="selected"' : '')
				+ ' value="' + (isSelected && i === 0 ? '' : jsonData[i]) + '">'
				+ jsonData[i] + '</option>';
		}
		return options;
	};

	if(globalJson.titles) {
		$('[data-person-title]').each(function() {
			var that = $(this);
			var initValue = that.find('option:selected').text();
			that.html(generateOption(globalJson.titles, true, initValue));
			if(that.parent().is('[data-customselect]')) {
				that.parent().customSelect('_createTemplate').customSelect('refresh');
			}
		});
	}

	if(globalJson.otherTitles) {
		$('[data-person-title]').each(function() {
			var self = $(this);
			self.parent().off('afterSelect.preventClose').on('afterSelect.preventClose', function(e, val) {
				if (val === 'Others') {
					self.parent().data('customSelect').options.preventClose = true;
				}
			});
			self.off('change.title').on('change.title', function() {
				if(this.value.toLowerCase() === 'others') {
					var that = $(this);
					that.children().last().remove();
					that.append(generateOption(globalJson.otherTitles, false));
					if(that.parent().is('[data-customselect]')) {
						var scroll = that.parent().data('customSelect').element.scroll;
						var scrollBar = scroll.children('.scroll-bar');
						var scrollBarSpan = scrollBar.find('span');
						var scrollContainerUl = scroll.children('.scroll-container').find('ul');
						var marginTop = scrollContainerUl.css('margin-top');

						that.parent()
							.customSelect('_createTemplate')
							.customSelect('_initCustomScroll')
							.customSelect('refresh');

						that.parent().data('customSelect').element.curItem = scrollContainerUl.children().eq(0);

						scrollContainerUl.css('margin-top', marginTop);
						var top = Math.abs(parseInt(marginTop) / scrollContainerUl.height()) * scrollBar.height();
						scrollBarSpan.css({'top': top});

						setTimeout(function() {
							self.parent().data('customSelect').options.preventClose = false;
						}, 200);
					}
				}
			});
		});
	}
};
