var SIA = SIA || {};

SIA.whereWeFly = function () {
    var onHoverElements = [
        ".region-wrapper"
    ];
    var imageDefault = "images/map-land.png";
    var imageBGElement = $(".map-display");

    var init = function() {
        onHoverListener();
        backButtonListener();
        viewPdfMapNetwork();
    };

    var toggleBack = function(state) {
        var classIds = [
            ".map-back-button"
        ];

        $(classIds.join(","))[state ? "removeClass" : "addClass"]("hidden");
    };

    var viewPdfMapNetwork = function() {
        $(".custom-tooltip").append('<a href="images/map-mobile.pdf" target="_blank" class="view-pdf-map-network">View a PDF of our network map</a>');
    };

    var openAccordion = function(code) {
        $("[data-map-accordion='" + code + "']").find("[data-accordion-trigger]").trigger("click");
    };

    var openDefaultAccordion = function(id) {
        $("[data-map-accordion='"+ id +"']").find(".accordion__control").trigger("click");
    };

    var onHoverListener = function() {
        $(onHoverElements.join(",")).off()
            .on("mouseenter", function(evt) {
                imageBGElement.attr("src", $(this).data("map-image-hover"));
            })
            .on("mouseleave", function() {
                imageBGElement.attr("src", imageDefault);
            });
        onClickListener();
    };

    var backButtonListener = function() {
        $(".map-back-button").off().on("click", function(evt) {
            evt.preventDefault();
            imageBGElement.attr("src", imageDefault);
            onHoverListener();
            // toggleTooltip(false);
            toggleBack(false);
            openDefaultAccordion("SEA");
            $(".top-text").removeClass("hidden");
        });
    };

    var onClickListener = function() {
        $("[data-map-image-onclick]").on("click", function(evt) {
            var imageOnClickData = $(this).data("map-image-onclick");
            if (imageOnClickData) {
                imageBGElement.attr("src", imageOnClickData);
                $(".top-text").addClass("hidden");
                $(onHoverElements.join(",")).off("mouseenter").off("mouseleave");
                $("[data-map-image-onclick]").off("click");
                // toggleTooltip(true);
                openAccordion($(this).data("map-accordion-trigger"));
                toggleBack(true);
            }
        });
    };

    return {
        init: init
    };
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.whereWeFly.init();
        }
    }, 100);
};

$(function() {
    waitForUnderscore();
});
