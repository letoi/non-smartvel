SIA.DesMastheadFareDeal = function () {
  var init = function () {
    Vue.component('masthead-fare-deal', {
      template: '#masthead-fare-deal',
      data() {
        return {
          masthead: Array,
          fareDeal: Array
        }
      },

      methods: {
        readMore: readMore
      },

      mounted: getData
    });

    new Vue({
      el: '#masthead-container'
    });
  };

  var readMore = function () {
    var element = $('.js-read-more').text();
    if (element == 'Read more') {
      $('.js-read-more').text('Read less').addClass('active');
      $('.js-text-content').slideDown();
    } else {
      $('.js-read-more').text('Read more').removeClass('active');
      $('.js-text-content').slideUp();
    }
  }

  var getData = function () {
    var self = this;

    var apiRequest = [
      axios.get('ajax/des-landing.json')
    ];

    axios.all(apiRequest).then(axios.spread(function (jsonData) {
      var data = jsonData.data;

      self.masthead = data['masthead'].items;
      self.fareDeal = data['fareDeal'];
    }));
  }

  return {
    init: init
  };
}();

SIA.DesLandingExplore = function () {
  var bus = new Vue();

  var initComponents = function () {
    Vue.component('des-landing-explore', {
      template: '#des-landing-explore',
      data() {
        return {
          categories: dataCategory,
          sumDestinationFound: 0,
          userRestaurants: Array,
          userHotels: Array,
          userSights: Array,
          userTours: Array,
          userMusicArtCulture: Array,
          TradeFairsConferences: Array,
          userEvents: Array,
          activeTab: false,
          currentTab: 'sight-attraction',
          tabs: ['Sight-Attraction', 'Hotel', 'Restaurant', 'Tour-Activities', 'Music-Art-Culture', 'Event'],
          startDateDefault: String,
          endDateDefault: String
        }
      },

      methods: {
        setRatedStars(score) {
          var html = '';
          for (var i = 0; i < score; i++) {
            html += '<li class="explore__rating-star rated"></li>';
          }
          return html;
        },

        setActiveTabContent() {
          this.tabId = this.categories.filter(function (obj) { obj.isActive === true });
        }
      },

      computed: {
        currentTabComponent() {
          return 'tab-' + this.currentTab.toLowerCase();
        }
      },
    });

    Vue.component('tab-hotel', {
      template: '#tab-hotel',
      data() {
        return {
          currentPage: 1,
          itemsPerPage: 0,
          data: Array,
          id: 'hotel',
          name: 'Hotels',
          itemsFound: 0,
          totalPages: 0,
          numberOfItems: 6,
          loading: true
        }
      },

      mounted() {
        var api = SIA.APIGenerator.hotel();

        this.getDataByAPI(api, true);
      },

      methods: {
        getDataByAPI(api, paging = false) {
          this.loading = true;

          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.data = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (paging) {
              this.itemsFound = dataResponse.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => this.loading = false);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        setPage(pageNumber) {
          this.currentPage = pageNumber;

          var api = SIA.APIGenerator.hotel(this.currentPage);
          this.getDataByAPI(api);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;

          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;

          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        }
      }
    });

    Vue.component('tab-restaurant', {
      props: ['dataRestaurant'],
      template: '#tab-restaurant',
      data() {
        return {
          currentPage: 1,
          itemsPerPage: 0,
          data: Array,
          id: 'restaurant',
          name: 'Restaurants',
          dataItem: Object,
          itemsFound: 0,
          loading: true,
          numberOfItems: 6,
          totalPages: 0
        }
      },

      mounted() {
        var api = SIA.APIGenerator.restaurant();

        this.getDataByAPI(api, true);
      },

      methods: {
        getDataByAPI(api, paging = false) {
          this.loading = true;

          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.data = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (paging) {
              this.itemsFound = dataResponse.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => this.loading = false);
        },

        setRatedStars(score) {
          var html = '';
          for (var i = 0; i < Math.round(score * 2) / 2; i++) {
            html += '<li class="explore__rating-star rated"></li>';
          }
          return html;
        },

        starsNotRate() {
          var html = '';
          for (var i = 0; i < 5; i++) {
            html += '<li class="explore__rating-star"></li>';
          }
          return html;
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        setPage(pageNumber) {
          this.currentPage = pageNumber;
          var api = SIA.APIGenerator.restaurant(this.currentPage);

          this.getDataByAPI(api)
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;

          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        }
      },

      filters: {
        roundScore(number) {
          return Math.round(number * 2) / 2;
        }
      }
    });

    Vue.component('tab-sight-attraction', {
      props: ['dataSight'],
      template: '#tab-sight-attraction',
      data() {
        return {
          currentPage: 1,
          id: 'sight-attraction',
          name: 'Sights & Attractions',
          dataItem: Object,
          itemsFound: 0,
          numberOfItems: 6,
          nextApi: '',
          prevApi: '',
          loading: true,
          dataSights: Array,
          itemsPerPage: null,
          totalPages: 0
        }
      },

      mounted() {
        var api = SIA.APIGenerator.sight();

        this.getDataByAPI(api, true);
      },

      methods: {
        getDataByAPI(api, paging = false) {
          this.loading = true;

          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.dataSights = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (paging) {
              this.itemsFound = dataResponse.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => this.loading = false);
        },

        setPage(pageNumber) {
          this.currentPage = pageNumber;
          var api = SIA.APIGenerator.sight(this.currentPage);

          this.getDataByAPI(api);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;
          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        }
      }
    });

    Vue.component('tab-tour-activities', {
      props: ['dataTour'],
      template: '#tab-tour-activities',
      data() {
        return {
          currentPage: 1,
          itemsPerPage: 0,
          id: 'tour-activities',
          name: 'Tours & Activities',
          sumDestinationFound: 0,
          dataFilter: Array,
          loading: true,
          errorMsg: 'There are no matches found.',
          itemsFound: 0,
          filterStartsDate: null,
          filterEndsDate: null,
          totalPages: 0,
          numberOfItems: 6
        }
      },
      mounted() {
        var api = SIA.APIGenerator.tour();
        // var self = this;
        this.getDataByAPI(api, true);

        bus.$on('dateSelected', (date) => {
          var self = this;

          if (!date.start) {
            self.errorMsg = '"To" date required';
          }

          if (!date.end) {
            self.errorMsg = '"From" date required';
          }

          if (!date || !date.start || !date.end) {
            self.dataFilter = [];

            return;
          }
          this.currentPage = 1;

          var startDateFilter = new Date(date.start);
          var endDateFilter = new Date(date.end);

          self.filterStartsDate = [startDateFilter.getMonth() + 1, startDateFilter.getDate(), startDateFilter.getFullYear()].join('-');
          self.filterEndsDate = [endDateFilter.getMonth() + 1, endDateFilter.getDate(), endDateFilter.getFullYear()].join('-');

          var api = SIA.APIGenerator.tour({
            start: self.filterStartsDate,
            end: self.filterEndsDate
          });

          this.getDataByAPI(api, true, true);
        });
      },
      methods: {
        getDataByAPI(api, paging = false, showError) {
          this.loading = true;

          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.dataFilter = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (!this.dataFilter.length && showError) {
              this.errorMsg = 'There are no matches found.';
            }

            if (paging) {
              this.itemsFound = response.data.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => { this.loading = false; });
        },
        setPage(pageNumber) {
          this.currentPage = this.filterStartsDate && this.filterEndsDate ? 1 : pageNumber;

          var api = SIA.APIGenerator.tour({
            start: this.filterStartsDate,
            end: this.filterEndsDate
          }, this.currentPage);

          this.getDataByAPI(api, true);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;

          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;
          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        },

        filterDataByDateInput(data, startDateFilter, endDateFilter) {
          if (!data) {
            return;
          }

          var dataFilter = [];
          var startDate;
          var endDate;
          data.forEach(function (obj) {
            startDate = new Date(obj.starts).getTime();
            endDate = new Date(obj.ends).getTime();
            if (!startDateFilter && !endDateFilter) {
              dataFilter = data;
            } else if (!startDateFilter && endDateFilter) {
              if (endDate <= endDateFilter) {
                dataFilter.push(obj);
              }
            } else if (startDateFilter && !endDateFilter) {
              if (startDate >= startDateFilter) {
                dataFilter.push(obj);
              }
            } else {
              if (startDate >= startDateFilter && endDate <= endDateFilter) {
                dataFilter.push(obj);
              }
            }
          });
          this.dataFilter = dataFilter;
        },

        resetSearch() {
          this.currentPage = 1;

          bus.$emit('resetSearch');
        },

        filterUserPlacesByLayerName(data, types) {
          var dataFilter = data.filter(function (obj) {
            return obj.layer && types.indexOf(obj.layer.type_name);
          });

          return dataFilter;
        }
      },

      filters: {
        subAddress(str) {
          return str = str.substring(0, 30) + '...';
        },

        formatDateFromISODate(date) {
          var humanDate = new Date(date);
          var m = humanDate.getMonth();
          var d = humanDate.getDate();

          // we define an array of the months in a year
          var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

          // we get the text name of the month by using the value of m to find the corresponding month name
          var displayMonth = months[m];

          if (d < 10) {
            d = '0' + d;
          }

          return date = d + ' ' + displayMonth;
        }
      }
    });

    Vue.component('tab-music-art-culture', {
      props: ['dataMusic'],
      template: '#tab-music-art-culture',
      data() {
        return {
          currentPage: 1,
          itemsPerPage: 0,
          data: Array,
          id: 'music-art-culture',
          name: 'Music, Art & Culture',
          itemsFound: 0,
          numberOfItems: 6,
          loading: true
        }
      },

      mounted() {
        var api = SIA.APIGenerator.music();

        this.getDataByAPI(api, true);
      },

      methods: {
        getDataByAPI(api, paging = false) {
          this.loading = true;

          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.data = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (paging) {
              this.itemsFound = dataResponse.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => this.loading = false);
        },
        setPage(pageNumber) {
          this.currentPage = pageNumber;

          var api = SIA.APIGenerator.music(this.currentPage);
          this.getDataByAPI(api);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;

          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;
          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        }
      },

      filters: {
        formatDateFromISODate(date) {
          var humanDate = new Date(date);
          var m = humanDate.getMonth();
          var d = humanDate.getDate();

          // we define an array of the months in a year
          var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

          // we get the text name of the month by using the value of m to find the corresponding month name
          var displayMonth = months[m];

          if (d < 10) {
            d = '0' + d;
          }

          return date = d + ' ' + displayMonth;
        }
      }
    });

    Vue.component('tab-event', {
      props: ['dataEvent'],
      template: '#tab-event',
      data() {
        return {
          currentPage: 1,
          itemsPerPage: 0,
          data: Array,
          id: 'event',
          name: 'Events',
          itemsFound: 0,
          numberOfItems: 6,
          loading: true
        }
      },

      mounted() {
        var api = SIA.APIGenerator.event();

        this.getDataByAPI(api, true);
      },

      methods: {
        getDataByAPI(api, paging= false) {
          this.loading =  true;
          axios.get(api).then((response) => {
            var dataResponse = response.data;

            this.data = dataResponse.results;
            this.nextApi = dataResponse.next;
            this.prevApi = dataResponse.previous;

            if (paging) {
              this.itemsFound = dataResponse.count;
              this.itemsPerPage = dataResponse.results.length;
              this.totalPages = Math.ceil(this.itemsFound / this.itemsPerPage);
            }
          }).finally(() => this.loading = false);
        },
        setPage(pageNumber) {
          this.currentPage = pageNumber;

          var api = SIA.APIGenerator.event(this.currentPage);

          this.getDataByAPI(api);
        },

        showDetails(data) {
          bus.$emit('openingModal', data);
        },

        nextPage(api) {
          this.currentPage++;

          this.getDataByAPI(api);
        },

        prevPage(api) {
          this.currentPage--;
          this.getDataByAPI(api);
        },

        loadMoreItems(api) {
          this.currentPage++;
          this.getDataByAPI(api);
        }
      },

      filters: {
        formatDateFromISODate(date) {
          var humanDate = new Date(date);
          var m = humanDate.getMonth();
          var d = humanDate.getDate();

          // define an array of the months in a year
          var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

          // get the text name of the month by using the value of m to find the corresponding month name
          var displayMonth = months[m];

          if (d < 10) {
            d = '0' + d;
          }

          return date = d + ' ' + displayMonth;
        }
      }
    });

    Vue.component('explore-no-result', {
      template: `<div class="static-block">
                  <div class="static-block--item">
                    <div class="explore__no-result">
                      <p class="explore__no-result-text">There are no matches found.</p>
                      <p class="explore__no-result-text">Reset your search to see more destination.</p>
                      <input class="explore__no-result-button reset-search-button btn-1" type="button" value="RESET SEARCH" @click="resetSearch">
                    </div>
                  </div>
                </div>`,

      methods: {
        resetSearch: function () {
          bus.$emit('resetSearch');
        }
      }
    });

    Vue.component('date-picker-explore', {
      props: {
        configs: {
          type: Object,
          default: () => defaultConfig
        },
        i18n: {
          type: String,
          default: defaultI18n
        },
        months: {
          type: Array,
          default: () => null
        },
        shortDays: {
          type: Array,
          default: () => null
        },
        // options for captions are: title, ok_button
        captions: {
          type: Object,
          default: () => defaultCaptions
        },
        format: {
          type: String,
          default: 'DD MMMM YYYY (dddd)'
        },
        styles: {
          type: Object,
          default: () => { }
        },
        initRange: {
          type: Object,
          default: () => null
        },
        startActiveMonth: {
          type: Number,
          default: parseInt(json.fareDeals.travelDurationStart.split('-')[1]) - 1
        },
        startActiveYear: {
          type: Number,
          default: parseInt(json.fareDeals.travelDurationStart.split('-')[2])
        },
        presetRanges: {
          type: Object,
          default: () => null
        },
        compact: {
          type: String,
          default: 'false'
        },
        righttoleft: {
          type: String,
          default: 'true'
        },
        startDate: {
          type: Date,
          default: ''
        },
        endDate: {
          type: Date,
          default: ''
        }
      },
      template: `<div class="calendar-root">
          <div class="grid-col one-half" ref="errorDiv1">
            <div class="grid-inner">
              <div class="input-3" ref="startDiv" :class="{'focus':key == 'first', 'has_selected':key == 'first' || startDate}" tabindex=0 @click.stop="toggleCalendar('first')">
                <label for="select-date-2" class="input-3__label">From</label>
                <div class="input-date" ><input readonly=true type="text" name="start-date" id="start-date"  aria-labelledby="start-date-error" v-model="startDate" value="" class="input-date-picker ui-autocomplete-input" data-rule-required="true" data-msg-required="Select depart date" data-toggle-mb="true" autocomplete="off" aria-required="true"></div>
                <button type="button" name="btn-search-fl-depart" id="btn-search-fl-depart">
                  <em v-if="!startDate" class="ico-date">
                    <span class="ui-helper-hidden-accessible">Calendar</span>
                  </em>
                </button>
              </div>
            </div>
          </div>
          <div class="grid-col one-half" ref="errorDiv2">
            <div class="grid-inner">
              <div class="input-3" :class="{'focus':key == 'second', 'has_selected':endDate}" @click.stop="toggleCalendar('second')">
                <label for="select-date-1" class="input-3__label">To</label>
                <div class="input-date" ><input readonly=true ref="endInput"  type="text" name="end-date" id="end-date"  aria-labelledby="start-end-error" v-model="endDate" value="" class="input-date-picker ui-autocomplete-input" data-rule-required="true" data-msg-required="Select return date" data-toggle-mb="true" autocomplete="off" aria-required="true"></div>
                <button type="button" name="btn-search-fl-return" id="btn-search-fl-return">
                  <em v-if="!endDate" class="ico-date">
                    <span class="ui-helper-hidden-accessible">Calendar</span>
                  </em>
                </button>
              </div>
            </div>
          </div>

          <div class="calendar" :class="{'calendar-mobile ': isCompact, 'calendar-right-to-left': isRighttoLeft}" v-if="isOpen">
            <div class="calendar-head" v-if="!isCompact">
            </div>
            <div class="calendar-wrap">
              <div class="calendar_month_left" :class="{'calendar-left-mobile': isCompact}" v-if="showMonth">
                <div class="months-text">
                  <span class="left ico-point-l" @click.stop="goPrevMonth"></span>
                  <span class="right" @click.stop="goNextMonth" v-if="isCompact"></span>
                  {{monthsLocale[activeMonthStart] +' '+ activeYearStart}}
                  <span class="return-date-flight">{{numberOfDays}} days</span></div>
                  <ul :class="s.daysWeeks">
                    <li v-for="item in shortDaysLocale" :key="item">{{item}}</li>
                  </ul>
                  <ul v-for="r in 6" :class="[s.days]" :key="r">
                    <li :class="[{[s.daysSelected]: isDateSelected(r, i, 'first', startMonthDay, endMonthDate),
                    [s.daysInRange]: isDateInRange(r, i, 'first', startMonthDay, endMonthDate),
                    [s.dateDisabled]: isDateDisabled(r, i, startMonthDay, endMonthDate),
                    [s.disabled]: checkInvalidDate(r, i, 'first', startMonthDay, endMonthDate)}]"
                     v-for="i in numOfDays" :key="i" v-html="getDayCell(r, i, startMonthDay, endMonthDate)"
                      @click.stop="selectFirstItem(r, i, checkInvalidDate(r, i, 'first', startMonthDay, endMonthDate))"><a></a></li>
                  </ul>
              </div>
              <div class="calendar_month_right" v-if="!isCompact">
                <div class="months-text">
                  {{monthsLocale[startNextActiveMonth] +' '+ activeYearEnd}}
                  <span class="right ico-point-r" @click.stop="goNextMonth"></span>
                </div>
                <ul :class="s.daysWeeks">
                    <li v-for="item in shortDaysLocale" :key="item">{{item}}</li>
                </ul>
                <ul v-for="r in 6" :class="[s.days]" :key="r">
                  <li :class="[{[s.daysSelected]: isDateSelected(r, i, 'second', startNextMonthDay, endNextMonthDate),
                  [s.daysInRange]: isDateInRange(r, i, 'second', startNextMonthDay, endNextMonthDate),
                  [s.dateDisabled]: isDateDisabled(r, i, startNextMonthDay, endNextMonthDate),
                  [s.disabled]: checkInvalidDate(r, i, 'second', startNextMonthDay, endNextMonthDate)}]"
                      v-for="i in numOfDays" :key="i" v-html="getDayCell(r, i, startNextMonthDay, endNextMonthDate)"
                        @click.stop="selectSecondItem(r, i, checkInvalidDate(r, i, 'second', startNextMonthDay, endNextMonthDate))"><a></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>`,
      data() {
        return {
          dateRange: {},
          numOfDays: 7,
          isFirstChoice: true,
          isOpen: false,
          presetActive: '',
          showMonth: false,
          activeMonthStart: this.startActiveMonth,
          activeYearStart: this.startActiveYear,
          activeYearEnd: this.startActiveYear,
          startDate: '',
          endDate: '',
          numberOfDays: '0',
          key: ''
        }
      },
      created() {
        if (this.isCompact) {
          this.isOpen = true
        }
        if (this.activeMonthStart === 11) this.activeYearEnd = this.activeYearStart + 1
      },
      watch: {
        startNextActiveMonth: function (value) {
          if (value === 0) this.activeYearEnd = this.activeYearStart + 1
        }
      },
      computed: {
        getTravelDurationStart: function () {
          var result = json.fareDeals.travelDurationStart.split('-');
          return new Date(result[2], parseInt(result[1]) - 1, result[0]);
        },
        getTravelDurationEnd: function () {
          var result = json.fareDeals.travelDurationEnd.split('-');
          return new Date(result[2], parseInt(result[1]) - 1, result[0]);
        },
        addAdvancePurchaseday: function () {
          var startingDate = new Date();
          startingDate.setDate(startingDate.getDate() + parseInt(json.fareDeals.advancePurchase));
          return this.getTravelDurationStart > startingDate ? this.getTravelDurationStart : startingDate;
        },
        monthsLocale: function () {
          return this.months || availableMonths[this.i18n]
        },
        shortDaysLocale: function () {
          return this.shortDays || availableShortDays[this.i18n]
        },
        s: function () {
          return Object.assign({}, defaultStyle, this.style)
        },
        startMonthDay: function () {
          return new Date(this.activeYearStart, this.activeMonthStart, 1).getDay()
        },
        startNextMonthDay: function () {
          if (this.activeMonthStart === 11) {
            return new Date(this.activeYearStart, this.startNextActiveMonth, 1).getDay() + 1;
          } else {
            return new Date(this.activeYearStart, this.startNextActiveMonth, 1).getDay();
          }
        },
        endMonthDate: function () {
          return new Date(this.activeYearEnd, this.startNextActiveMonth, 0).getDate()
        },
        endNextMonthDate: function () {
          return new Date(this.activeYearEnd, this.activeMonthStart + 2, 0).getDate()
        },
        startNextActiveMonth: function () {
          return this.activeMonthStart >= 11 ? 0 : this.activeMonthStart + 1
        },
        finalPresetRanges: function () {
          const tmp = {}
          const presets = this.presetRanges || defaultPresets(this.i18n)
          for (const i in presets) {
            const item = presets[i]
            let plainItem = item
            if (typeof item === 'function') {
              plainItem = item()
            }
            tmp[i] = plainItem
          }
          return tmp
        },
        isCompact: function () {
          return this.compact === 'true'
        },
        isRighttoLeft: function () {
          return this.righttoleft === 'true'
        }
      },
      methods: {
        convertToDate: function (stringDate) {
          return new Date(stringDate[2], availableMonths.SRT.indexOf(stringDate[1]), stringDate[0]);
        },
        getNumberOfDays: function (start, end) {
          var oneDay = 24 * 60 * 60 * 1000;
          var diffDays = Math.round(Math.abs((start.getTime() - end.getTime()) / (oneDay)));
          return diffDays + 1;
        },
        toggleCalendar: function (key) {
          this.key = key;
          if (this.isCompact) {
            this.showMonth = !this.showMonth
            return
          }
          this.isOpen = !this.isOpen
          if (!this.isOpen) {
            this.key = '';
          }
          this.showMonth = !this.showMonth
          return
        },
        getDateString: function (date, format = this.format) {
          if (!date) {
            return null
          }
          const dateparse = new Date(Date.parse(date))
          return fecha.format(new Date(dateparse.getFullYear(), dateparse.getMonth(), dateparse.getDate() - 1), format)
        },
        getDayIndexInMonth: function (r, i, startMonthDay) {
          const date = (this.numOfDays * (r - 1)) + i
          return date - startMonthDay
        },
        getDayCell(r, i, startMonthDay, endMonthDate) {
          const result = this.getDayIndexInMonth(r, i, startMonthDay)
          // bound by > 0 and < last day of month
          return result > 0 && result <= endMonthDate ? result : '&nbsp;'
        },
        getNewDateRange(result, activeMonth, activeYear) {
          const newData = {}
          let key = 'start'

          if (!this.isFirstChoice) {
            key = 'end'
          } else {
            newData['end'] = null
          }
          const resultDate = new Date(activeYear, activeMonth, result);

          if (!this.isFirstChoice && resultDate < this.dateRange.start) {
            this.isFirstChoice = false
            return { start: resultDate }
          }
          var max = json.fareDeals.maximumStayType.toLowerCase() == 'months' ? parseInt(json.fareDeals.maxStay) * 30 : parseInt(json.fareDeals.maxStay);
          if (!this.isFirstChoice && this.getNumberOfDays(this.dateRange.start, resultDate) < parseInt(2)) {
            return { end: null };
          }
          else if (!this.isFirstChoice && this.getNumberOfDays(this.dateRange.start, resultDate) > max) {
            return { end: null };
          }
          // toggle first choice
          this.isFirstChoice = !this.isFirstChoice
          newData[key] = resultDate
          return newData
        },
        checkInvalidDate(r, i, key, startMonthDay, endMonthDate) {
          const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
          if (result < 2 || result > endMonthDate + 1) return false

          let currDate = null
          if (key === 'first') {
            currDate = this.changeMonth(this.getDateString(new Date(this.activeYearStart, this.activeMonthStart, result)));
          } else {
            currDate = this.changeMonth(this.getDateString(new Date(this.activeYearEnd, this.startNextActiveMonth, result)));
          }
          if (this.getTravelDurationEnd <= new Date() ||
            this.getNumberOfDays(new Date(), this.getTravelDurationEnd) < parseInt(json.fareDeals.minStay) ||
            this.getNumberOfDays(this.addAdvancePurchaseday, this.getTravelDurationEnd) < parseInt(json.fareDeals.minStay)
          ) {
            return true;
          } else {
            thisValue = !(this.convertToDate(currDate.split(' ')) >= this.addAdvancePurchaseday && this.convertToDate(currDate.split(' ')) <= this.getTravelDurationEnd);
            return thisValue;
          }
        },
        changeDay(date) {
          if (date) {
            var splitDate = date.split(' ');
            if (splitDate[3] == '(Tues)') {
              splitDate[3] = '(Tue)';
            } else if (splitDate[3] == '(Thur)') {
              splitDate[3] = '(Thu)';
            }
            return splitDate;
          } else {
            return null;
          }
        },
        changeMonth(date) {
          if (date) {
            var splitDate = this.changeDay(date);
            splitDate[1] = availableMonths.SRT[availableMonths.EN.indexOf(splitDate[1])];
            return splitDate[0] + ' ' + splitDate[1] + ' ' + splitDate[2] + ' ' + splitDate[3];
          } else {
            return null;
          }

        },
        checkDate() {
          this.toggleCalendar('');
          if (!this.dateRange.end) {
            this.$refs.endInput.focus();
            this.toggleCalendar('second');
          }

          setTimeout(function () {
            !this.endDate ? $('li.calendar_days_selected').addClass('single-date') : $('li.single-date').removeClass('single-date');
          }, 100);
          this.startDate = this.changeMonth(this.getDateString(this.dateRange.start));
          this.endDate = this.changeMonth(this.getDateString(this.dateRange.end));
          if (this.startDate && this.$refs.errorDiv1.classList.contains('error')) {
            this.$refs.errorDiv1.classList.remove('error');
            $(this.$refs.errorDiv1).children('.text-error').remove();
          } else if (this.startDate && this.$refs.errorDiv2.classList.contains('error')) {
            this.$refs.errorDiv2.classList.remove('error');
            $(this.$refs.errorDiv2).children('.text-error').remove();
          }
          if (this.dateRange.end) {
            this.numberOfDays = this.getNumberOfDays(this.convertToDate(this.startDate.split(' ')), this.convertToDate(this.endDate.split(' ')))
          }
        },
        selectFirstItem(r, i, notValid) {
          if (notValid) {
            return;
          }
          if (this.key == 'second') {
            this.dateRange.end = null;
            this.isFirstChoice = false;
          }
          const result = this.getDayIndexInMonth(r, i, this.startMonthDay) + 1
          this.dateRange = Object.assign({}, this.dateRange, this.getNewDateRange(result, this.activeMonthStart,
            this.activeYearStart))
          if (this.dateRange.start && this.dateRange.end) {
            this.presetActive = ''
            if (this.isCompact) {
              this.showMonth = false
            }
          }

          this.checkDate();

          bus.$emit('dateSelected', {
            start: this.startDate,
            end: this.endDate
          });
        },
        selectSecondItem(r, i, notValid) {
          if (notValid) {
            return;
          }
          if (this.key == 'second') {
            this.dateRange.end = null;
            this.isFirstChoice = false;
          }
          const result = this.getDayIndexInMonth(r, i, this.startNextMonthDay) + 1
          this.dateRange = Object.assign({}, this.dateRange, this.getNewDateRange(result, this.startNextActiveMonth,
            this.activeYearEnd))
          if (this.dateRange.start && this.dateRange.end) {
            this.presetActive = ''
          }
          this.checkDate();

          bus.$emit('dateSelected', {
            start: this.startDate,
            end: this.endDate
          });
        },
        isDateSelected(r, i, key, startMonthDay, endMonthDate) {
          const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
          if (result < 2 || result > endMonthDate + 1) return false

          let currDate = null
          if (key === 'first') {
            currDate = new Date(this.activeYearStart, this.activeMonthStart, result)
          } else {
            currDate = new Date(this.activeYearEnd, this.startNextActiveMonth, result)
          }

          return (this.dateRange.start && this.dateRange.start.getTime() === currDate.getTime()) ||
            (this.dateRange.end && this.dateRange.end.getTime() === currDate.getTime())
        },
        isDateInRange(r, i, key, startMonthDay, endMonthDate) {
          const result = this.getDayIndexInMonth(r, i, startMonthDay) + 1
          if (result < 2 || result > endMonthDate + 1) return false

          let currDate = null
          if (key === 'first') {
            currDate = new Date(this.activeYearStart, this.activeMonthStart, result)
          } else {
            currDate = new Date(this.activeYearEnd, this.startNextActiveMonth, result)
          }
          return (this.dateRange.start && this.dateRange.start.getTime() < currDate.getTime()) &&
            (this.dateRange.end && this.dateRange.end.getTime() > currDate.getTime())
        },
        isDateDisabled(r, i, startMonthDay, endMonthDate) {
          const result = this.getDayIndexInMonth(r, i, startMonthDay)
          // bound by > 0 and < last day of month
          return !(result > 0 && result <= endMonthDate)
        },
        goPrevMonth() {
          const prevMonth = new Date(this.activeYearStart, this.activeMonthStart, 0)
          this.activeMonthStart = prevMonth.getMonth()
          this.activeYearStart = prevMonth.getFullYear()
          this.activeYearEnd = prevMonth.getFullYear()
        },
        goNextMonth() {
          const nextMonth = new Date(this.activeYearEnd, this.startNextActiveMonth, 1)
          this.activeMonthStart = nextMonth.getMonth()
          this.activeYearStart = nextMonth.getFullYear()
          this.activeYearEnd = nextMonth.getFullYear()
        },
        updatePreset(item) {
          this.presetActive = item.label
          this.dateRange = item.dateRange
          // update start active month
          this.activeMonthStart = this.dateRange.start.getMonth()
          this.activeYearStart = this.dateRange.start.getFullYear()
          this.activeYearEnd = this.dateRange.end.getFullYear()
        },
        setDateValue: function () {
          bus.$emit('dateSelected', {
            start: this.startDate,
            end: this.endDate
          });

          if (!this.isCompact) {
            this.toggleCalendar('')
          }
        }
      },
      mounted: function () {
        var self = this;
        document.onkeydown = function (e) {
          if (e.keyCode == 9 || e.which == 9 && this.$refs.startDiv.classList.contains('focus-outline')) {
            if (!self.startDate) {
              self.$refs.startDiv.classList.remove('focus-outline');
              self.toggleCalendar('first');
            } else if (!self.endDate) {
              self.$refs.startDiv.classList.remove('focus-outline');
              self.toggleCalendar('second');
            } else {
              self.$refs.startDiv.classList.remove('focus-outline');
              self.toggleCalendar('first');
            }
          }
        };

        document.onclick = function () {
          setTimeout(function () {
            if (self.$refs.errorDiv1.classList.contains('error')) {
              self.toggleCalendar('first');
            } else if (self.isOpen) {
              self.toggleCalendar(self.key)
            }
          }, 100);
        };

        bus.$on('resetSearch', () => {
          self.dateRange = {};
          self.numOfDays = 7;
          self.isFirstChoice = true;
          self.isOpen = false;
          self.presetActive = '';
          self.showMonth = false;
          self.activeMonthStart = this.startActiveMonth;
          self.activeYearStart = this.startActiveYear;
          self.activeYearEnd = this.startActiveYear;
          self.startDate = '';
          self.endDate = '';
          self.numberOfDays = '0';
          self.key = '';
        });
      }
    });
  };

  var bootstrapApp = function () {
    const Modal = {
      name: 'modal',
      template: '#modal',
      props: ['data'],
      data() {
        return {
          data: Array,
          map: null,
          marker: null,
          image: String,
          name: String,
          address: String,
          description: String,
          hasMap: true
        }
      },
      created() {
        var self = this;
        bus.$on('openedModal', function (dataChanged) {
          var coordinates = [];

          if (typeof(dataChanged.location) === 'undefined') {
            self.hasMap = false;
            coordinates = [1.351616 , 103.808053];

            if (typeof(dataChanged.place.location) === 'undefined') {
              coordinates = [1.351616 , 103.808053];
            } else {
              self.hasMap = true;
              coordinates = [dataChanged.place.location.coordinates[1], dataChanged.place.location.coordinates[0]];
            }
          } else {
            self.hasMap = true;
            coordinates = [dataChanged.location.coordinates[1], dataChanged.location.coordinates[0]];
          }

          setTimeout(function () {
            self.data = dataChanged;
            self.initMap(coordinates);
            self.image = (Array.isArray(dataChanged.cc_photo) ? dataChanged.cc_photo[0].cc_photo : dataChanged.pictures[0].cc_photo);
            self.name = dataChanged.translations.en.name;
            self.address = (dataChanged.address ? dataChanged.address : dataChanged.place.name);
            self.description = dataChanged.translations.en.description.split('\n');

            $('html, body').addClass('no-scroll');
            $('.js-modal').addClass('showing');
          });
        })
      },
      updated: function () {
        var self = this;
        this.$nextTick(function () {
          $('.js-backdrop').on('click', function (event) {
            event.stopPropagation();
            if (event.target === this) {
              self.close();
            }
          });
        })
      },
      methods: {
        close() {
          if (this.map && this.map.remove) {
            this.map.remove();
          };

          this.map = undefined;
          this.$emit('close');
          $('html, body').removeClass('no-scroll');
          $('.js-modal').removeClass('showing');
        },
        setRatedStars(score) {
          var html = '';
          for (var i = 0; i < Math.round(score * 2) / 2; i++) {
            html += '<li class="explore__rating-star rated"></li>';
          }
          return html;
        },
        starsNotRate() {
          var html = '';
          for (var i = 0; i < 5; i++) {
            html += '<li class="modal__rating-star"></li>';
          }
          return html;
        },
        initMap(coordinates) {
          var myIcon = L.icon({
            iconUrl: 'images/marker-icon.png',
            iconSize: [24, 34]
          });

          this.map = L.map('map', {
            center: coordinates,
            zoom: 15,
            reset: true,
            animate: false
          })

          L.tileLayer(
            'https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
            {
              maxZoom: 18
            }
          ).addTo(this.map);

          this.marker = L.marker(coordinates, { icon: myIcon }).addTo(this.map);
        }
      },
      filters: {
        roundScore(number) {
          return Math.round(number * 2) / 2;
        }
      }
    };

    new Vue({
      el: '#container-explore',
      data() {
        return {
          isModalVisible: false,
          data: Object
        }
      },
      components: {
        modal: Modal
      },
      created() {
        bus.$on('openingModal', (data) => {
          this.isModalVisible = true;

          bus.$emit('openedModal', data);
        });
      },
      methods: {
        closeModal() {
          this.isModalVisible = false;
        }
      }
    });
  }

  var dataCategory = [{
    'id': 'sight-attraction',
    'name': 'Sights & Attraction',
    'value': 'sight-attraction',
  }, {
    'id': 'hotel',
    'name': 'Hotels',
    'value': 'hotel',
    'isActive': true
  }, {
    'id': 'restaurant',
    'name': 'Restaurants',
    'value': 'restaurant'
  }, {
    'id': 'tour-activities',
    'name': 'Tours & Activities',
    'value': 'tour-activities'
  }, {
    'id': 'music-art-culture',
    'name': 'Music, Art & Culture',
    'value': 'music-art-culture'
  }, {
    'id': 'event',
    'name': 'Events',
    'value': 'event'
  }];

  var init = function (fn) {
    initComponents();
    bootstrapApp();
  };

  return {
    init: init
  };
}();

SIA.DesWeather = function () {

  var init = function () {
    Vue.component('des-weather', {
      template: '#des-weather',
      data() {
        return {
          userWeather: Array
        }
      },

      mounted: getData
    });

    new Vue({
      el: '#weather'
    });
  };

  var getData = function () {
    var self = this;

    var apiRequest = [
      axios.get('ajax/des-landing.json')
    ];

    axios.all(apiRequest).then(axios.spread(function (jsonData) {
      var data = jsonData.data;

      self.userWeather = data['airportInformation'];

    }));
  }

  return {
    init: init
  };
}();

SIA.DesMoreDestinations = function () {

  var init = function () {
    Vue.component('des-more-destinations', {
      template: '#des-more-destinations',
      data() {
        return {
          userDestinations: Array
        }
      },

      mounted: getData
    });

    new Vue({
      el: '#destinations'
    });
  };

  var getData = function () {
    var self = this;

    var apiRequest = [
      axios.get('ajax/des-landing.json')
    ];

    axios.all(apiRequest).then(axios.spread(function (jsonData) {
      var data = jsonData.data;

      self.userDestinations = data['citiesMoreDestinations'];

    }));
  }

  return {
    init: init
  };
}();

var waitForLibraries = function (fn) {
  setTimeout(function () {
    if (typeof _ == 'undefined' ||
      typeof Vue == 'undefined' ||
      typeof axios == 'undefined'
    ) {
      waitForLibraries(fn);
    } else {
      fn.init();
    }
  }, 100);
};

$(document).ready(function () {
  waitForLibraries(SIA.DesMastheadFareDeal);
  waitForLibraries(SIA.DesLandingExplore);
  waitForLibraries(SIA.DesWeather);
  waitForLibraries(SIA.DesMoreDestinations);

  function setCityFilterFly(el, data) {
    var selfAutoComplete = el.parent();

    var styledElement = selfAutoComplete.find('.select__text_styled');
    if (!styledElement.length) {
      return;
    }

    styledElement.removeClass('hidden');
    styledElement.find('.styled-city').text(data[0]);
    styledElement.find('.styled-code').text(data[1]);

    el.val(data.join(' - '));
    el.css('opacity', 0);
  }

  // set value default for search fly
  setCityFilterFly($('#search-fl-city-1'), ['Singapore', 'SIN']);
  setCityFilterFly($('#search-fl-city-2'), ['London', 'LCY']);

  var initializeSliderDestination = function () {
    const $slide = $('.js-destination-slide');
    const option = {
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slideToScroll: 1,
      arrows: false
    };

    $slide.slick(option);
  };

  var initializeSliderMasthead = function () {
    const $slide = $('.js-fare-deal-slide');
    const option = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slideToScroll: 1,
      arrows: false
    };
    $slide.slick(option);
  };

  setTimeout(function () {
    initializeSliderMasthead();
    initializeSliderDestination();
  }, 1000);
});

SIA.APIGenerator = function (global) {
  var config = global.smartvel;

  function generateHotelAPI(page) {
    page = page ? page : 1;
    return config.url + '/user-places?key=' + config.key + '&starts=' + config.start + '&ends=' + config.end + '&fallback_lang=' + config.lang + '&lang=en&page=' + page + '&page_size=6&show_on_map=True&region=' + config.region.id + '&layer=hotel';
  }

  function generateSighsAPI(page) {
    page = page ? page : 1;
    return config.url + '/user-places?key=' + config.key + '&starts=' + config.start + '&ends=' + config.end + '&fallback_lang=en&lang=' + config.lang + '&page=' + page + '&page_size=6&show_on_map=True&region=' + config.region.id + '&layer=place';
  }

  function generateRestaurantAPI(page) {
    page = page ? page : 1;
    return config.url + '/user-restaurants?key=' + config.key + '&starts=' + config.start + '&ends=' + config.end + '&fallback_lang=en&lang=' + config.lang + '&page=' + page + '&page_size=6&show_on_map=True&region=' + config.region.id;
  }

  function generateToursAPI(dateFilter, page) {
    page = page ? page : 1;
    dateFilter = dateFilter && dateFilter.start && dateFilter.end
                  ? dateFilter
                  : {start: config.start, end: config.end};

    return config.url + '/user-events?key=' + config.key + '&starts=' + dateFilter.start + '&ends=' + dateFilter.end + '&fallback_lang=en&lang=' + config.lang + '&page=' + page + '&page_size=6&taxonomies=tours_and_activities&region=' + config.region.name;
  }

  function generateMusicAPI(page) {
    page = page ? page : 1;
    return config.url + '/events/local?key=' + config.key + '&starts=' + config.start + '&ends=' + config.end + '&fallback_lang=en&lang=' + config.lang + '&page=' + page + '&page_size=6&taxonomies=art_and_culture&taxonomies=performing_arts&taxonomies=music&region=' + config.region.name;
  }

  function generateEventsAPI(page) {
    page = page ? page : 1;
    return config.url + '/events/local?key=' + config.key + '&starts=' + config.start + '&ends=' + config.end + '&fallback_lang=en&lang=' + config.lang + '&page=' + page + '&page_size=6&taxonomies=trade_fairs_and_conferences&taxonomies=sports&taxonomies=other_interesting_events&region=' + config.region.name;
  }

  return {
    hotel: generateHotelAPI,
    restaurant: generateRestaurantAPI,
    sight: generateSighsAPI,
    tour: generateToursAPI,
    music: generateMusicAPI,
    event: generateEventsAPI
  }
}(window);