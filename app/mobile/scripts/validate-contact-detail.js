SIA.contactDetailValidator = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
    var contactForm = $('#form--contact-details');
    var preCountryCode = $("#pre-country-code");
    var phoneCountry = $("#phone-country");
    var contactEmailAddress = $("#contact--email-address");

	function init() {
		contactForm.validate({
            highlight: function(element, errorClass) {
                $(element).closest('span.input-1').addClass(errorClass);
                $(element).closest('span.select__text').addClass(errorClass);
            },
            success: function(element) {
                $(element).closest('span.input-1').removeClass('error');
                $(element).closest('span.select__text').removeClass('error');
            }
        });

        preCountryCode.off().on('blur', function () {
            contactForm.valid();
        });

        phoneCountry.off().on('blur', function () {
            contactForm.valid();
        });

        contactEmailAddress.off().on('blur', function () {
            contactForm.valid();
        });
	}

	return {
		init: init
	};
}();

var waitForJquery = function () {
    setTimeout(function () {
    if (typeof $.validator == 'undefined') {
            waitForJquery();
        } else {
            SIA.contactDetailValidator.init();
        }
    }, 100);
};

$(function () {
    typeof $.validator == 'undefined' ? waitForJquery() : SIA.contactDetailValidator.init();
});
