<%var precision=preCodeData;%>
<%
	fareFamiliesGroup = [],  familiesCabinGroup = {}, arrWrap = [];
	var totalCabin = [];
	_.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) {
		familiesCabinGroup[fareFamilies.cabinClassName] = fareFamilies.cabinClassName;

		if (totalCabin.indexOf(fareFamilies.cabinClassName) == -1) {
			totalCabin.push(fareFamilies.cabinClassName);
		}
	});
%>

<%
	function groupBy(arr, key) {
		var newArr = [],
				family = {},
				newItem, i, j, cur;
		for (i = 0, j = arr.length; i < j; i++) {
				cur = arr[i];
				 if (!(cur[key] in family)) {
						family[cur[key]] = { type: cur[key], data: [] };
						newArr.push(family[cur[key]]);
				}
				family[cur[key]].data.push(cur);
		}
		return newArr;
	}
%>

<% var arr = [] %>
<% _.each(data.flights, function(flights, flightIdx1){ %>

	<% var newArray = [] %>
			<% _.each(flights.segments, function(segments, flightSegmentIdx){ %>

				<% var newArr1 = [] %>


						<% _.each(data.recommendations, function(recommendation, reIdx){ %>
						 <%var fareFamilyNow=""%>
						 <% if( data.tripType ==='M') { %>
							<%fareFamilyNow=recommendation.fareFamily%>

								<% }  else {%>
							<%fareFamilyNow=recommendation.segmentBounds[flightIdx1].fareFamily%>

								<%}%>
								<% _.each(data.fareFamilies, function(fareFamiliesGroup, fareFamiliesGroupIdx){ %>
										<% _.each(recommendation.segmentBounds[flightIdx1].segments, function(segmentItem, segmentItemIdx){ %>
												<% if(segmentItem.segmentID === segments.segmentID && fareFamiliesGroup.fareFamily == fareFamilyNow ) { %>
											<%var tooltipFlag=labels.disabled%>
													<%if(segments.legs[0].codeShareFlight ===false && segments.legs[0].operatingAirline.code ==='SQ') {
																tooltipFlag=labels.enabled
														}%>

														<% newArr1.push({
																"family" : fareFamilyNow,
																"familyGroup" : fareFamiliesGroup.cabinClassName,
																"price" : recommendation.fareSummary.fareTotal.totalAmount,
																"pricePerAdult" : recommendation.fareSummary.fareDetailsPerAdult.totalAmount,
																"recommendationId" : recommendation.recommendationID,
																"displayLastSeat" : segmentItem.displayLastSeat,
																"numOfLastSeats" : segmentItem.numOfLastSeats,
																"eligibleOCRecommendationIDs" : segmentItem.eligibleOCRecommendationIDs,
																"fareFamilyCode" :fareFamiliesGroup.fareFamilyCode,"RBD":segmentItem.legs[0].sellingClass,
																"tooltipFlag":tooltipFlag,
																"fareFamilyName":fareFamiliesGroup.fareFamilyName})
														%>

													<% return false %>
												<% } %>
										<% }) %>
								<% }) %>
						<% }); %>
						<% newArray[segments.segmentID] =  newArr1 %>
				<% }); %>
		<% arr.push(newArray) %>
<% }); %>

<%
	_.each(arr, function(arr1, idx1){
		_.each(arr1, function(arr2, idx2){
			arr[idx1][idx2] = groupBy(arr2, 'family');
		})
	});

%>

<%
		var ffArray =[];

		_.each(data.fareFamilies, function(sortFF, sortFFIdx) {
		 ffArray.push(sortFF.fareFamily);
		});

		var sortObj = _.invert(_.object(_.pairs(ffArray)));
%>

<%

function formatFlightTime(str) {
	return str.slice(11, 16);
}

function formatFlightDate(str) {
	return $.datepicker.formatDate('dd M (D)', new Date(str[0].replace(/-/g,"/")))
}

function formatFlightDuration(time) {
	var secondsTotal = parseInt(time);
	var hh2 = Math.floor(secondsTotal / 3600);
	var mm2 = Math.floor((secondsTotal - (hh2 * 3600)) / 60);
	if (hh2 < 10) {hh2 = "0"+hh2;}
	if (mm2 < 10) {mm2 = "0"+mm2;}

	return {
		"timeTotal" : hh2+"hr "+mm2+ "mins",
		"timeTotalStation" : hh2+"h "+mm2+ "m"
	}
}

function formatFlightLayOvers(time) {
	var layoverTime = parseInt(time);
	var hour = Math.floor(layoverTime / 3600);
	var min = Math.floor((layoverTime - (hour * 3600)) / 60);
	if (hour < 10) {hour = "0"+hour;}
	if (min < 10) {min = "0"+min;}
	return totalLayoverTime = hour+"hr "+min+ "mins";
}

%>

<% _.each(flight.segments, function(segments, segmentsIdx) { %>
	<%

	var railsData = {};
	for (var railsIdx = 0, railsLen = segments.legs.length; railsIdx < railsLen; railsIdx++) {
		if (segments.legs[railsIdx].aircraft.code == 'TRN') {
			railsData  = {
				"valid" : true,
				"index" :  railsIdx
			};

			break;
		}
	}

	var segmentIds=[];
		var departureTime = segments.departureDateTime,
				arrivalDateTime = segments.arrivalDateTime,
				newDepartureTime = departureTime.slice(11, 16),
				newArrivalDateTime = arrivalDateTime.slice(11, 16),
				departureSplit = departureTime.split(' '),
				departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
				arrivalDateTimeSplit = arrivalDateTime.split(' '),
				arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));

		var layoverTime = parseInt(segments.legs[0].layoverDuration);
		var hour = Math.floor(layoverTime / 3600);
		var min = Math.floor((layoverTime - (hour * 3600)) / 60);
		if (hour < 10) {hour = "0"+hour;}
		if (min < 10) {min = "0"+min;}
		var totalLayoverTime = hour+"hr "+min+ "mins";

		var secondsTotal = parseInt(segments.tripDuration);
		var hh2 = Math.floor(secondsTotal / 3600);
		var mm2 = Math.floor((secondsTotal - (hh2 * 3600)) / 60);
		if (hh2 < 10) {hh2 = "0"+hh2;}
		if (mm2 < 10) {mm2 = "0"+mm2;}
		var timeTotal = hh2+"hr "+mm2+ "mins";
		var timeTotalStation = hh2+"h "+mm2+ "m";

		// get all operating airlines info
		var opAirlinesInfo = [];
		_.each(segments.legs, function(leg, legIdx) {
			opAirlinesInfo.push({
				code : leg.operatingAirline.code,
				name : leg.operatingAirline.name,
				flightnum: leg.flightNumber,
				aircraft: leg.aircraft.code
			});
		});
	%>

		<% _.each(arr[flightIdx], function(segment, segmentID) { %>
				<%
						segment = _.sortBy(segment, function(x) {
								return sortObj[x.type];
						});
				%>

				<% if(segments.segmentID == segmentID) { %>
						<% if(segment && segment.length > 0) { %>
								<%
										var parseTimeToHour = function(layoverDuration){
											var secondsStop = parseInt(layoverDuration);
											var stopHr = Math.floor(secondsStop / 3600);
											var stopMM = Math.floor((secondsStop - (stopHr * 3600)) / 60);
											if (stopHr < 10) {stopHr = "0"+stopHr;}
											if (stopMM < 10) {stopMM = "0"+stopMM;}
											return stopHr+"h "+stopMM+ "m";
										}
								%>

								<%
										var nonStop = false, oneStop = false, multiStop = false, arr = [];

										var oneStopDetail = {},
												multiStopDetail = [];

										var layovers = 0;

										var countLayover = function(segment) {
												var count = [];

												_.map(segment.legs, function(leg){
														var stop = 0,
														stopTime = 0,
														duration = 0;

														leg.layoverDuration > 0 ? duration += 1 : duration = duration;
														leg.stops.length && _.map(leg.stops, function(stopObj){
																if(stopObj.layoverDuration > 0) {
																		stop += 1;
																		stopTime += stopObj.layoverDuration;
																} else {
																		stop = stop;
																		stopTime = stopTime;
																}
														});

														count.push({
															"aircraft": leg.aircraft.code,
															"leg" : duration,
															"stop" : stop,
															"stopObj" : {
																"aircraft": leg.aircraft.code,
																"layover" : stopTime,
																"code" : leg.stops.length && leg.stops[0].airportCode || '',
																"airlinenumber": leg.flightNumber,
																"departure" : leg.stops.length && leg.stops[0].departureDateTime || '',
																"arrival" : leg.stops.length && leg.stops[0].arrivalDateTime || ''
															},
															"legObj" : {
																"aircraft": leg.aircraft.code,
																"layover": leg.layoverDuration,
																"code" : leg.destinationAirportCode,
																"airlinenumber": leg.flightNumber,
																"departure" : leg.departureDateTime,
																"arrival" : leg.arrivalDateTime
															},
															"destination": segments.destinationAirportCode,
															"wcag" : [
																{
																	"origin" : leg.originAirportCode,
																	"depart" : leg.departureDateTime,
																	"destination" : leg.stops.length && leg.stops[0].airportCode || '',
																	"arrival": leg.stops.length && leg.stops[0].departureDateTime || '',
																	"flightNumber" : leg.flightNumber,
																	"layover" : leg.stops.length && leg.stops[0].layoverDuration
																},
																{
																	"origin" : leg.stops.length ? leg.stops[0].airportCode : leg.originAirportCode,
																	"depart" : leg.stops.length ? leg.stops[0].arrivalDateTime : leg.departureDateTime,
																	"destination" : leg.destinationAirportCode,
																	"arrival" : leg.arrivalDateTime,
																	"flightNumber" : leg.flightNumber,
																	"layover" : leg.layoverDuration
																}
															]
														})
												});

												return count;
										}

										!segments.legs.length ? (nonStop = true) : (arr = countLayover(segments));

										_.each(arr, function(flight, i) {

											if (flight.aircraft != 'TRN') {
												// count layovers
												if (flight.legObj.layover != 0) { layovers += 1; }
												if (flight.stopObj.layover != 0) { layovers += 1; }
											}
										});

										if(layovers == 1) {
											_.map(arr, function(data){
												if(data.legObj.code === data.destination) {
													data.stopObj.layover && (oneStopDetail = data.stopObj);
												} else {
													oneStopDetail = data.legObj;
												}
											});

											oneStop = true;
										}

										if(layovers >= 1) {
											_.map(arr, function(data){
												if(data.stopObj.layover) {
													multiStopDetail.push(data.stopObj)
												}
												if(data.legObj.layover && data.legObj.code !== data.destination) {
													multiStopDetail.push(data.legObj)
												}
											})

											multiStop = true;
										}
								%>
						<%
								var matchName = function(code) {
									var name;
									_.map(data.airports, function(airport){
										if(airport.airportCode === code) {
											name = airport.cityName;
											return false;
										}
									})
									return name;
								}
						%>

						<%
								var checkPre = false;
								var info = {}, arrCabin = [];

								_.map(data.recommendations, function(recommendation, reidx){
									_.map(recommendation.segmentBounds[flightIdx].segments, function(boundSegment, boundSegmentID){
										if(boundSegment.segmentID === segments.segmentID) {
											var cabinClass1,
													cabinClass2;
											_.map(boundSegment.legs, function(leg, legidx){
												if(legidx === 0) {
													cabinClass1 = leg.cabinClass;
												} else {
													cabinClass2 = leg.cabinClass;
												}
											})
											if(cabinClass1 && cabinClass2 && (cabinClass1 !== cabinClass2)) {
												checkPre = true;
												arrCabin.push({"cabinClass1" : cabinClass1, "cabinClass2" : cabinClass2})
											}
										}
									})
								})

								if(checkPre) {
									if(arrCabin[0].cabinClass1 === 'Y') {
												info['header'] = labels.economy;
												info['flightNumber'] = segments.legs[0].flightNumber;
												info['origin'] = segments.legs[0].originAirportCode;
												info['destination'] = segments.legs[0].destinationAirportCode;
										}

										if(arrCabin[0].cabinClass2 === 'S') {
												info['header'] = labels.premiumEconomy;
												info['flightNumber'] = segments.legs[1].flightNumber;
												info['origin'] = segments.legs[1].originAirportCode;
												info['destination'] = segments.legs[1].destinationAirportCode;
										}
								}
						%>

				<%
					var totalRailFlyTime = null;
					if (railsData.valid) {
						var railflyTime = 0;
						for(var railsFlyTimeIdx = 0, railsFlyTimeLen = segments.legs.length; railsFlyTimeIdx < railsFlyTimeLen; railsFlyTimeIdx++) {
							var segmentLeg  = segments.legs[railsFlyTimeIdx];
							railflyTime += segmentLeg.flightDuration;
						}

						var totalRailFlyTime = formatFlightDuration(railflyTime);
					}
				%>
				<div class="flight-list-item economy-flight-bgd <%- ((railsData.valid) ? 'rail-fly' : '') %>" data-timetotal="<%- ((railsData.valid) ? totalRailFlyTime.timeTotal : timeTotal) %>" aria-describedby="flight-segment-<%- flightIdx %>-<% segmentsIdx %>" tabindex="0">
					<span class="says" id="flight-segment-<%- flightIdx %>-<% segmentsIdx %>">
					Flight <%- segments.legs[0].flightNumber %> , Departing from <% _.each(data.airports, function(airports, airportsIdx) { %><% if( airports.airportCode === segments.originAirportCode ) { %><%- airports.cityName %> at <%- airports.countryName %> <% if(segments.legs[0].departureTerminal) { %>Terminal <%- segments.legs[0].departureTerminal %> <% } %> on <%- departureDatepicker %> at <%- newDepartureTime %><% } %><% }); %>. Arriving in <% _.each(data.airports, function(airports, airportsIdx) { %><% if( airports.airportCode === segments.destinationAirportCode ) { %><%- airports.cityName %> at <%- airports.countryName %><% if(segments.legs[0].arrivalTerminal) { %>Terminal <%- segments.legs[0].arrivalTerminal %> <% } %> on <%- arrivalDateTimeDatepicker %> at <%- newArrivalDateTime %> <% } %><% }); %>, with layover time: <%- totalLayoverTime %> , total flight duration: <%- timeTotal %> ”
					</span>
					<span class="segment-id hidden"> <%- segmentID %> </span>
					<div class="recommended-table rails-flights-table" data-stop="<%- nonStop ? '0' : (oneStop ? '1' : '2') %>">
						<div data-flight-item class="col-info recommended-flight-item">
							<div class="flight-station">
								<% _.each(segments.legs, function(leg) { %>
									<span class="hidden" data-sort-leg="<%-leg.legID%>" data-sort-departure="<%-leg.departureDateTime%>" data-sort-arrival="<%-leg.arrivalDateTime%>"></span>
								<% }) %>
								
								<span data-sort-duration="<%- railflyTime %>"></span>
								<% if(railsData.valid) { %>
										<span class="stop-time">Rail-Fly • <%- totalRailFlyTime.timeTotal %></span>
								<% } %>

								<% if (railsData.valid && (parseInt(railsData.index) == 0)) { %>
									<%
										var railsTimeTotal = null;
										var railsTime = 0;
										for(var railsTimeIdx = 0, railsTimeLen = segments.legs.length; railsTimeIdx < railsTimeLen; railsTimeIdx++) {
											var segmentLeg  = segments.legs[railsTimeIdx];
											if (segmentLeg.aircraft.code == 'TRN') {
												railsTime += segmentLeg.flightDuration;
											}
										}

										railsTimeTotal = formatFlightDuration(railsTime);


									%>
									<div class="rail-fly-station">
										<span class="rail-time">
		                  <em class="ico-5-rail"></em>
		                  <span class="title">RAIL</span>
		                  <span class="time-stop"><%- railsTimeTotal.timeTotal %></span>
		                </span>
										<div class="control-flight-station anim-all" data-first-wrap-flight>
											<div class="flight-station-item">
												<div class="flight-station--inner">
													<div class="flight-station-info">
													<% var cabinCount = (totalCabin.length == 1) ? 'single-cabin' : '' ;%>
														<div class="station-stop ">
		                          <span class="station-stop-detail">
		                            <em class="ico-5-rail"></em>
		                          </span>
		                        </div>
														<div class="flights-station__info--detail">
															<%
																var originAirportCode = null;
																var departureTime = null;
																var departureDate = null;
																for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		originAirportCode = segmentLegs.originAirportCode;
																		departureTime = formatFlightTime(segmentLegs.departureDateTime);
																		departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																		break;
																	}
																}
															%>
															<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<%
																var arrivalAirportCode = null;
																var arrivalTime = null;
																var ArrivalDate = null;
																var arrivalTerminal = null;
																var flightDuration = null;
																for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		arrivalAirportCode = segmentLegs.destinationAirportCode;
																		arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																		ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																		arrivalTerminal = segmentLegs.arrivalTerminal;
																		flightDuration = formatFlightDuration(segmentLegs.flightDuration);
																		break;
																	}
																}
															%>
															<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																	if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>
															<%
																	return;
																	}
																});
															%>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="wrap-flight-station anim-all wrap-rail-fly-station">
									   <div class="flight-station-item flight-result-leg anim-all">
									      <div class="flight-station--inner">
									         <div class="flight-station-info">
									            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-5-rail"></em></span></div>
									            <div class="flights-station__info--detail">
									               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
									               	<%- originAirportCode %> <%- departureTime %>
								               	</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === originAirportCode ) { %>
																		<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-origin-date="<%- departureDate %>"
																			data-origin-airportname="<%- airports.airportName %>"
																			data-origin-terminal="">
																			<%- departureDate %>
																			<br><%- airports.airportName %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									            <div class="flights-station__info--detail return-flight">
							               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
							               			<%- arrivalAirportCode %> <%- arrivalTime %>
						               			</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === arrivalAirportCode ) { %>
																		<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-destination-date="<%- ArrivalDate %>"
																			data-destination-airportname="<%- airports.airportName %>"
																			data-destination-terminal="<%- arrivalTerminal %>">
																			<%- ArrivalDate %>
																			<br><%- airports.airportName %>
																			<br>Terminal <%- arrivalTerminal %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									         </div>
									         <div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta"
																	data-operationname="<%- info.name %>"
																	data-flightnumber="<%- info.code %>"
																	data-planename="TRAIN <%- info.flightnum %>">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>
															<%
																return;
																	}
																});
															%>
														</div>
													</div>
									      </div>

									      <%
													var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

													railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
												%>

												<% if (railLayOversTime) { %>
													<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
					                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
					                </span>
												<% } %>

									   	</div>
										</div>

									</div>
								<% } %>

								<%
									var txt = null;

									if (nonStop) { txt = 'Non-stop •'; }
									else if (oneStop) { txt = 'One-stop •'; }
									else if (layovers == 2) { txt = 'Two-stop •'; }
									else if (layovers >= 3) { txt = layovers.toString() + ' stops •'; }
								%>

								<% if(railsData.valid) { %>
									<%
										var flyTime = 0;
										for(var flyTimeIdx = 0, flyTimeLen = segments.legs.length; flyTimeIdx < flyTimeLen; flyTimeIdx++) {
											var segmentLeg  = segments.legs[flyTimeIdx];
											if (segmentLeg.aircraft.code != 'TRN') {
												flyTime += segmentLeg.flightDuration;
											}
										}

										flyTimeTotal = formatFlightDuration(flyTime);
									%>
									<span class="flight-time">
	                  <em class="ico-airplane-2"></em>
	                  <span class="title">FLIGHTS</span>
	                  <span class="time-stop"><%- layovers %> stop • <%- flyTimeTotal.timeTotal %></span>
	                </span>
								<% } else { %>
									<span class="stop-time"><%- txt %> <%- timeTotal %></span>
								<% } %>

								<div class="control-flight-station anim-all" data-first-wrap-flight>
									<div class="flight-station-item">
										<div class="flight-station--inner">
											<div class="flight-station-info">
											<% var cabinCount = (totalCabin.length == 1) ? 'single-cabin' : '' ;%>
												<div class="station-stop <%- cabinCount %>">
													<% if(nonStop) { %>
														<span class="station-stop-detail">
															<em class="ico-airplane-2"></em>
														</span>
													<% } %>
													<% if (oneStop) { %>
														<span class="station-stop-detail one-stop-station">
															<span class="time time--1">
																<strong><%- oneStopDetail.code %> </strong><%- parseTimeToHour(oneStopDetail.layover) %>
															</span>
														</span>
													<% } %>
													<% if(layovers >= 2) { %>
															<% var stops = (layovers == 2) ? 'stop' : 'multistop' ; %>
															<% for(var i = multiStopDetail.length - 1, j = 0; i >= 0; i--, j++) { %>
																<% if(multiStopDetail[j].aircraft != 'TRN') { %>
																	<span class="two-stop-station <%- stops %>--<%- j + 1 %>">
																		<span class="time">
																			<strong><%- multiStopDetail[j].code %></strong>

																			<% if (layovers == 2) { %>
																				<%- parseTimeToHour(multiStopDetail[j].layover) %>
																			<% } %>

																		</span>
																	</span>
																<% } %>
															<% } %>
													<% } %>
												</div>
												<div class="flights-station__info--detail">
													<%
														var originAirportCode = null;
														var departureTime = null;
														var departureDate = null;
														for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
															var segmentLegs = segments.legs[legsIdx];
															if (segmentLegs.aircraft.code != 'TRN') {
																originAirportCode = segmentLegs.originAirportCode;
																departureTime = formatFlightTime(segmentLegs.departureDateTime);
																departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																break;
															}
														}
													%>
													<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
													<% _.each(data.airports, function(airports, airportsIdx) { %>
														<% if( airports.airportCode === originAirportCode ) { %>
															<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
														<% } %>
													<% }); %>
												</div>
												<div class="flights-station__info--detail return-flight">
													<%
														var arrivalAirportCode = null;
														var arrivalTime = null;
														var ArrivalDate = null;
														for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
															var segmentLegs = segments.legs[legsIdx];
															if (segmentLegs.aircraft.code != 'TRN') {
																arrivalAirportCode = segmentLegs.destinationAirportCode;
																arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																break;
															}
														}
													%>
													<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
													<% _.each(data.airports, function(airports, airportsIdx) { %>
														<% if( airports.airportCode === arrivalAirportCode ) { %>
															<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
														<% } %>
													<% }); %>
												</div>
											</div>
											<div class="airline-info">
												<% var msAirlineInfo = (layovers >= 3) ? 'multistops-airline-info': ''; %>
												<div class="inner-info <%- msAirlineInfo %>">
													<% if(layovers >= 3 || railsData.valid) { %>
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft != 'TRN') {
														%>
															<%
																var operatedBy = (info.code != 'SQ' || info.code == 'MI') ?
																	'Operated by' : '';
																var airlineImgClss = (airlineImg != '') ? 'airline-detail' : '' ;
																var airlineImg = '';
																var airlineImgPos = ''

																if (info.code == 'SQ') {
																	airlineImg = 'sq';
																	airlineImgPos = 'sq-img';
																}

																if (info.code == 'MI') {
																	airlineImg =  'si';
																	airlineImgPos = 'si-img';
																}
															%>
															<span class="airline-deta <%- airlineImgClss %>">
																<% if (airlineImg != '') { %>
																	<img class="<%- airlineImgPos %>" src="images/svg/<%- airlineImg %>.svg" alt="<%- airlineImg %> Logo" longdesc="img-desc.html">
																<% } %>
																<strong><%- operatedBy %> <%- info.name %></strong> • <%- info.code %> <%- info.flightnum %><br>
															</span>
														<%
																}
															});
														%>
													<% } else { %>
													<span class="airline-detail <% if(segments.legs[0].operatingAirline.code == "SQ") { %> singapore-logo <% } %> <% if(segments.legs[0].operatingAirline.code == "TR") { %> scoot-logo <% } %>">
														<% if(segments.legs[0].operatingAirline.code == "SQ" || segments.legs[0].operatingAirline.code == "SI" || segments.legs[0].operatingAirline.code == "TR") { %>
															<img src="saar5/images/svg/<%- segments.legs[0].operatingAirline.code.toLowerCase() %>.svg" alt="<%- segments.legs[0].operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
														<% } %>
														<% if(segments.legs[0].operatingAirline.code == "MI") { %>
															<img src="saar5/images/svg/si.svg" longdesc="img-desc.html">
														<% } %>
														<%if(segments.legs[0].flightNumber.length ==4){ %>
														<strong class="no-image"><span><%=labels.operatedBy%></span> <%- segments.legs[0].operatingAirline.name %><span> •</span><span class="carrier carrier-no-image" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span></strong>
														<%}else{%>
														<strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong> <span class="carrier" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span>
														<%}%>
														</span>
													<% } %>
													<% if (railsData.valid && (parseInt(railsData.index) == 0)) { %>
														<a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.moreDetails%></a>
													<% } %>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div data-wrap-flight class="wrap-flight-station anim-all">
									<% if(railsData.valid && (parseInt(railsData.index) == 0)) { %>
										<%
											var originAirportCode = null;
											var departureTime = null;
											var departureDate = null;
											for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													originAirportCode = segmentLegs.originAirportCode;
													departureTime = formatFlightTime(segmentLegs.departureDateTime);
													departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
													break;
												}
											}
										%>
										<%
											var arrivalAirportCode = null;
											var arrivalTime = null;
											var ArrivalDate = null;
											var arrivalTerminal = null;
											var flightDuration = null;
											for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													arrivalAirportCode = segmentLegs.destinationAirportCode;
													arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
													ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
													arrivalTerminal = segmentLegs.arrivalTerminal;
													flightDuration = formatFlightDuration(segmentLegs.flightDuration);
													break;
												}
											}
										%>
							    	<div class="flight-station-item flight-result-leg anim-all hidden">
								      <div class="flight-station--inner">
								         <div class="flight-station-info">
								            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
								              <div class="flights-station__info--detail">
								               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
								               	<%- originAirportCode %> <%- departureTime %>
							               	</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-origin-date="<%- departureDate %>"
																		data-origin-airportname="<%- airports.airportName %>"
																		data-origin-terminal="">
																		<%- departureDate %>
																		<br><%- airports.airportName %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								            <div class="flights-station__info--detail return-flight">
						               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
						               			<%- arrivalAirportCode %> <%- arrivalTime %>
					               			</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-destination-date="<%- ArrivalDate %>"
																		data-destination-airportname="<%- airports.airportName %>"
																		data-destination-terminal="<%- arrivalTerminal %>">
																		<%- ArrivalDate %>
																		<br><%- airports.airportName %>
																		<br>Terminal <%- arrivalTerminal %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								         </div>
								         <div class="airline-info">
													<div class="inner-info">
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
															if (info.aircraft == 'TRN') {
														%>
															<span class="airline-deta"
																data-operationname="<%- info.name %>"
																data-flightnumber="<%- info.code %>"
																data-planename="TRAIN <%- info.flightnum %>">
																<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
															</span>
														<%
															return;
																}
															});
														%>
													</div>
												</div>
								      </div>

								      <%
												var railLayoversTotalTime = null;
												var railLayOversTime = 0;
												for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
													var railLayOvers = segments.legs[railLayoversTimeIDx];
													var layoverDuration = railLayOvers.layoverDuration;
													if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
														railLayOversTime = layoverDuration;
													}
												}

												railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
											%>

											<% if (railLayOversTime) { %>
												<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
				                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
				                </span>
											<% } %>

							  	 	</div>
									<% } %>

									<%
										var segFlightLegs = [];
										for (var segFlightIdx = 0, segFlightLen = segments.legs.length; segFlightIdx < segFlightLen; segFlightIdx++) {
											var segFlight = segments.legs[segFlightIdx];
											if (segFlight.aircraft.code != 'TRN') {
												segFlightLegs.push(segFlight);
											}
									 	}
									%>
									<% _.each(segFlightLegs, function(segmentsLegs, legsIdx) { %>
										<%

											var departureTime = segmentsLegs.departureDateTime,
												arrivalDateTime = segmentsLegs.arrivalDateTime,
												newDepartureTime = departureTime.slice(11, 16),
												newArrivalDateTime = arrivalDateTime.slice(11, 16),
												departureSplit = departureTime.split(' '),
												departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
												arrivalDateTimeSplit = arrivalDateTime.split(' '),
												arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));
												var secondsLayover = parseInt(segmentsLegs.layoverDuration);
												var secondsFlight = parseInt(segmentsLegs.flightDuration);
												var hh2 = Math.floor(secondsLayover / 3600);
												var mm2 = Math.floor((secondsLayover - (hh2 * 3600)) / 60);
												if (hh2 < 10) {hh2 = "0"+hh2;}
												if (mm2 < 10) {mm2 = "0"+mm2;}
												var timeTotalLayover = hh2+"hr "+mm2+ "mins";
												var hh3 = Math.floor(secondsFlight / 3600);
												var mm3 = Math.floor((secondsFlight - (hh3 * 3600)) / 60);
												if (hh3 < 10) {hh3 = "0"+hh3;}
												if (mm3 < 10) {mm3 = "0"+mm3;}
												var timeTotalFlight = hh3+"h "+mm3+ "m";
												if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) {
													var departureStop = segmentsLegs.stops[0].departureDateTime,
													arrivalStop = segmentsLegs.stops[0].arrivalDateTime,
													newDepartureStop = departureStop.slice(11, 16),
													newArrivalStop = arrivalStop.slice(11, 16),
													departureSplitStop = departureStop.split(' '),
													arrivalSplitStop = arrivalStop.split(' '),
													departureDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(departureSplitStop[0].replace(/-/g,"/"))),
													arrivalDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(arrivalSplitStop[0].replace(/-/g,"/")));
													var secondsLayoverStop = parseInt(segmentsLegs.stops[0].layoverDuration);
													var hh4 = Math.floor(secondsLayoverStop / 3600);
													var mm4 = Math.floor((secondsLayoverStop - (hh4 * 3600)) / 60);
													if (hh4 < 10) {hh4 = "0"+hh4;}
													if (mm4 < 10) {mm4 = "0"+mm4;}
													var timeTotalLayoverStop = hh4+"hr "+mm4+ "mins";
												}
											%>
										<% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
											<div class="flight-station-item flight-result-leg anim-all">
												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></div>
														<div class="flights-station__info--detail">
															<span class="hour" data-origin-hour="<%- segmentsLegs.originAirportCode %> <%- newDepartureTime %>"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
																<span class="country-name" data-origin-countryname="<%- airports.cityName %>"><%- airports.cityName %></span>
																<span class="date" data-origin-date="<%- departureDatepicker %>" data-origin-airportname="<%- airports.airportName %>" data-origin-terminal="<%- segmentsLegs.departureTerminal %>"><%- departureDatepicker %><br>
																	<%- airports.airportName %>
									<%airportName=airports.airportName%>
																<% } %>
															<% }); %>
														<br>

			<% if(segmentsLegs.departureTerminal !== '0' && segmentsLegs.departureTerminal !=undefined) { %>

				<% if(segmentsLegs.departureTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.departureTerminal %>
				<%}%>
			<%}%>

										</span>
														</div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour" data-destination-hour="<%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %>"><%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
																<span class="country-name" data-destination-countryname="<%- airports.cityName %>"><%- airports.cityName
																 %></span>
																 <span class="date" data-destination-date="<%- departureDatepickerStop %>" data-destination-airportname="<%- airports.airportName %>"><%- departureDatepickerStop %><br>
																<%- airports.airportName %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.flightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
										<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>

						 <% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg"  Logo" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "Scoot TigerAir" ) { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>
				<%var noImage=""%>
			 <%if(segments.legs[0].flightNumber.length ==4){ %>
				<%noImage="no-image"%>
				<strong class="no-image"><span><%=labels.operatedBy%></span> <%- segments.legs[0].operatingAirline.name %><span> •</span><span class="carrier carrier-no-image" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span></strong>
				<%}else{%>
				<strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong> <span class="carrier" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span>
				<%}%>

															</span>
															<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
								 <% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
											<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
												<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																						<span class="economy cabin-color hidden"><%- saar5.ccd.ccdSuites %></span>
																					<% } else { %>
																						<span class="economy cabin-color hidden"><%- familiesCabinGroup %></span>
																					<% } %>
																				<% }); %>
								 <% } %>
								</div>
													</div>
												</div>

												<% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
													<span class="layover-time--1" data-layovertime="<%- timeTotalLayoverStop %>"><em class="ico-flight-history"></em><%=labels.layOver%> <% if(timeTotalLayoverStop.slice(5,7) == "00") {%>
							<%- timeTotalLayoverStop.slice(0,4) %>
							<% } else { %>
							<%- timeTotalLayoverStop %>
							<% } %>
							</span>
												<% } %>

												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
														<div class="flights-station__info--detail">
															<span class="hour" data-origin-hour="<%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %>"><%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
																<span class="country-name" data-origin-countryname="<%- airports.cityName %>"><%- airports.cityName %></span>
																<span class="date" data-origin-date="<%- arrivalDatepickerStop %>" data-origin-airportname="<%- airports.airportName %>"><%- arrivalDatepickerStop %><br>
																	<%- airports.airportName %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour" data-destination-hour="<%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %>"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
																<span class="country-name" data-destination-countryname="<%- airports.cityName %>"><%- airports.cityName
																 %></span><span class="date" data-destination-date="<%- arrivalDateTimeDatepicker %>" data-destination-airportname="<%- airports.airportName %>" data-destination-terminal="<%- segmentsLegs.arrivalTerminal %>"><%- arrivalDateTimeDatepicker %><br>
																<%- airports.airportName %>
								 <%airportName=airports.airportName%>
																<% } %>
															<% }); %>
														<br>
															<% if(segmentsLegs.arrivalTerminal !== '0' && segmentsLegs.arrivalTerminal!=undefined) { %>
				<% if(segmentsLegs.arrivalTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.arrivalTerminal %>
				<%}%>
			<%}%>
											</span>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.flightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
										<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>

						 <% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg"  Logo" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "Scoot TigerAir" ) { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>

				<%var noImage=""%>
			 <%if(segments.legs[0].flightNumber.length ==4){ %>
				<%noImage="no-image"%>
				<strong class="no-image"><span><%=labels.operatedBy%></span> <%- segments.legs[0].operatingAirline.name %><span> •</span><span class="carrier carrier-no-image" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span></strong>
				<%}else{%>
				<strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong> <span class="carrier" data-carrier-code="<%- segments.legs[0].marketingAirline.code.toUpperCase() %>"> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %></span>
				<%}%>

															</span>
															<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
															<% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
																<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
																	<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																		<span class="economy cabin-color hidden"><%- saar5.ccd.ccdSuites %></span>
																	<% } else { %>
																		<span class="economy cabin-color hidden"><%- familiesCabinGroup %></span>
																	<% } %>
																<% }); %>
																<% } %><a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a></div>
													</div>
												</div>
												<% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
													<span class="layover-time--1" data-layovertime="<%- timeTotalLayover %>"><em class="ico-flight-history"></em>Layover time: <% if(timeTotalLayoverStop.slice(5,7) == "00") {%>
							<%- timeTotalLayoverStop.slice(0,4) %>
							<% } else { %>
							<%- timeTotalLayoverStop %>
							<% } %></span>
												<% } %>
											</div>
										<% } else { %>
											<div class="flight-station-item flight-result-leg anim-all">
												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
														<div class="flights-station__info--detail">
														<span class="hour" data-origin-hour="<%- segmentsLegs.originAirportCode %> <%- newDepartureTime %>"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
							<%var airportName%>
														<% _.each(data.airports, function(airports, airportsIdx) { %>
															<% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
															<span class="country-name" data-origin-countryname="<%- airports.cityName %>"><%- airports.cityName %></span><span class="date" data-origin-date="<%- departureDatepicker %>" data-origin-airportname="<%- airports.airportName %>" data-origin-terminal="<%- segmentsLegs.departureTerminal %>"><%- departureDatepicker %><br>
															<%- airports.airportName %>
								 <%airportName=airports.airportName%>
															<% } %>
														<% }); %>
														<br>
					<% if(segmentsLegs.departureTerminal !== '0'  && segmentsLegs.departureTerminal !=undefined) { %>
				<% if(segmentsLegs.departureTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.departureTerminal %>
				<%}%>
				<%}%>
				</span>
									</div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour" data-destination-hour="<%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %>"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
																<span class="country-name" data-destination-countryname="<%- airports.cityName %>"><%- airports.cityName
																 %></span><span class="date" data-destination-date="<%- arrivalDateTimeDatepicker %>" data-destination-airportname="<%- airports.airportName %>" data-destination-terminal="<%- segmentsLegs.arrivalTerminal %>"><%- arrivalDateTimeDatepicker %><br>
																<%- airports.airportName %>
								 <%airportName=airports.airportName%>
																<% } %>
															<% }); %>
															<br>
				<% if(segmentsLegs.arrivalTerminal !== '0' && segmentsLegs.arrivalTerminal!=undefined) { %>
				<% if(segmentsLegs.arrivalTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.arrivalTerminal %>
				<%}%>
				<%}%>
											</span>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.flightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
															<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "SC" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg"  Logo" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "TR" ||segmentsLegs.operatingAirline.code == "TZ") { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>
							 <%var noImage=""%>
							 <%if(segmentsLegs.flightNumber.length ==4){ %>
							<%noImage="no-image"%>
							<strong class="no-image"><span><%=labels.operatedBy%></span> <%- segmentsLegs.operatingAirline.name %><span> •</span><span class="carrier carrier-no-image" data-carrier-code="<%- segmentsLegs.marketingAirline.code.toUpperCase() %>"> <%- segmentsLegs.marketingAirline.code.toUpperCase() %> <%- segmentsLegs.flightNumber %></span></strong>
							<%}else{%>
							<strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong> <span class="carrier" data-carrier-code="<%- segmentsLegs.marketingAirline.code.toUpperCase() %>"> <%- segmentsLegs.marketingAirline.code.toUpperCase() %> <%- segmentsLegs.flightNumber %></span>
							<%}%>

															</span>
															<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
								<% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
															<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
																	<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																			<span class="economy cabin-color hidden"><%- saar5.ccd.ccdSuites %></span>
																	<% } else { %>
																		<span class="economy cabin-color hidden"><%- familiesCabinGroup %></span>
																	<% } %>
															<% }); %>
								<% } %>
															<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.lessDetails%></a></div>
													</div>
												</div>
												<% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
													<span class="layover-time--1" data-layovertime="<%- timeTotalLayover %>"><em class="ico-flight-history"></em><%=labels.layOver%> <% if(timeTotalLayover.slice(5,7) == "00") {%>
							<%- timeTotalLayover.slice(0,4) %>
							<% } else { %>
							<%- timeTotalLayover %>
							<% } %>
							</span>
												<% } %>
											</div>
										<% } %>
									<% }); %>

									<% if(railsData.valid && (parseInt(railsData.index) > 0)) { %>
										<%
											var originAirportCode = null;
											var departureTime = null;
											var departureDate = null;
											for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													originAirportCode = segmentLegs.originAirportCode;
													departureTime = formatFlightTime(segmentLegs.departureDateTime);
													departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
													break;
												}
											}
										%>
										<%
											var arrivalAirportCode = null;
											var arrivalTime = null;
											var ArrivalDate = null;
											var arrivalTerminal = null;
											var flightDuration = null;
											for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													arrivalAirportCode = segmentLegs.destinationAirportCode;
													arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
													ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
													arrivalTerminal = segmentLegs.arrivalTerminal;
													flightDuration = formatFlightDuration(segmentLegs.flightDuration);
													break;
												}
											}
										%>
							    	<div class="flight-station-item flight-result-leg anim-all hidden">
								      <div class="flight-station--inner">
								         <div class="flight-station-info">
								            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
								              <div class="flights-station__info--detail">
								               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
								               	<%- originAirportCode %> <%- departureTime %>
							               	</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-origin-date="<%- departureDate %>"
																		data-origin-airportname="<%- airports.airportName %>"
																		data-origin-terminal="">
																		<%- departureDate %>
																		<br><%- airports.airportName %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								            <div class="flights-station__info--detail return-flight">
						               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
						               			<%- arrivalAirportCode %> <%- arrivalTime %>
					               			</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-destination-date="<%- ArrivalDate %>"
																		data-destination-airportname="<%- airports.airportName %>"
																		data-destination-terminal="<%- arrivalTerminal %>">
																		<%- ArrivalDate %>
																		<br><%- airports.airportName %>
																		<br>Terminal <%- arrivalTerminal %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								         </div>
								         <div class="airline-info">
													<div class="inner-info">
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
															if (info.aircraft == 'TRN') {
														%>
															<span class="airline-deta"
																data-operationname="<%- info.name %>"
																data-flightnumber="<%- info.code %>"
																data-planename="TRAIN <%- info.flightnum %>">
																<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
															</span>

															<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.lessDetails%></a>
														<%
															return;
																}
															});
														%>
													</div>
												</div>
								      </div>

								      <%
												var railLayoversTotalTime = null;
												var railLayOversTime = 0;
												for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
													var railLayOvers = segments.legs[railLayoversTimeIDx];
													var layoverDuration = railLayOvers.layoverDuration;
													if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
														railLayOversTime = layoverDuration;
													}
												}

												railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
											%>

											<% if (railLayOversTime) { %>
												<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
				                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
				                </span>
											<% } %>

							  	 	</div>
									<% } %>

								</div>

								<% if (railsData.valid && (parseInt(railsData.index) > 0)) { %>
									<%
										var railsTimeTotal = null;
										var railsTime = 0;
										for(var railsTimeIdx = 0, railsTimeLen = segments.legs.length; railsTimeIdx < railsTimeLen; railsTimeIdx++) {
											var segmentLeg  = segments.legs[railsTimeIdx];
											if (segmentLeg.aircraft.code == 'TRN') {
												railsTime += segmentLeg.flightDuration;
											}
										}

										railsTimeTotal = formatFlightDuration(railsTime);


									%>
									<div class="rail-fly-station railsfly-return-station">
										<span class="rail-time">
		                  <em class="ico-5-rail"></em>
		                  <span class="title">RAIL</span>
		                  <span class="time-stop"><%- railsTimeTotal.timeTotal %></span>
		                </span>
										<div class="control-flight-station anim-all rail-fly-control-last" data-first-wrap-flight>
											<div class="flight-station-item">
												<div class="flight-station--inner">
													<div class="flight-station-info">
													<% var cabinCount = (totalCabin.length == 1) ? 'single-cabin' : '' ;%>
														<div class="station-stop ">
		                          <span class="station-stop-detail">
		                            <em class="ico-5-rail"></em>
		                          </span>
		                        </div>
														<div class="flights-station__info--detail">
															<%
																var originAirportCode = null;
																var departureTime = null;
																var departureDate = null;
																for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		originAirportCode = segmentLegs.originAirportCode;
																		departureTime = formatFlightTime(segmentLegs.departureDateTime);
																		departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																		break;
																	}
																}
															%>
															<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<%
																var arrivalAirportCode = null;
																var arrivalTime = null;
																var ArrivalDate = null;
																var arrivalTerminal = null;
																var flightDuration = null;
																for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		arrivalAirportCode = segmentLegs.destinationAirportCode;
																		arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																		ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																		arrivalTerminal = segmentLegs.arrivalTerminal;
																		flightDuration = formatFlightDuration(segmentLegs.flightDuration);
																		break;
																	}
																}
															%>
															<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																	if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>

																<a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.moreDetails%></a>
															<%
																	return;
																	}
																});
															%>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="wrap-flight-station anim-all wrap-rail-fly-station">
									   <div class="flight-station-item flight-result-leg anim-all">
									      <div class="flight-station--inner">
									         <div class="flight-station-info">
									            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-5-rail"></em></span></div>
									            <div class="flights-station__info--detail">
									               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
									               	<%- originAirportCode %> <%- departureTime %>
								               	</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === originAirportCode ) { %>
																		<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-origin-date="<%- departureDate %>"
																			data-origin-airportname="<%- airports.airportName %>"
																			data-origin-terminal="">
																			<%- departureDate %>
																			<br><%- airports.airportName %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									            <div class="flights-station__info--detail return-flight">
							               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
							               			<%- arrivalAirportCode %> <%- arrivalTime %>
						               			</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === arrivalAirportCode ) { %>
																		<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-destination-date="<%- ArrivalDate %>"
																			data-destination-airportname="<%- airports.airportName %>"
																			data-destination-terminal="<%- arrivalTerminal %>">
																			<%- ArrivalDate %>
																			<br><%- airports.airportName %>
																			<br>Terminal <%- arrivalTerminal %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									         </div>
									         <div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta"
																	data-operationname="<%- info.name %>"
																	data-flightnumber="<%- info.code %>"
																	data-planename="TRAIN <%- info.flightnum %>">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>

																<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a>
															<%
																return;
																	}
																});
															%>
														</div>
													</div>
									      </div>

									      <%
													var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

													railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
												%>

												<% if (railLayOversTime) { %>
													<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
					                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
					                </span>
												<% } %>

									   	</div>
										</div>

									</div>
								<% } %>

							</div>
						</div>
			<% var ffExists = false %>
						<% if(segments.segmentID == segmentID) { %>

			<%segmentIds.push(segments);%>

							<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
								<% if(familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') { %>
									<% if(segment && segment.length > 0) { %>
				<% ffExists = true %>
										<div data-trigger-animation="<%- flightIdx %>-<%- segmentsIdx %>-0" data-trigger-popup=".popup-table-flight" data-content-popup="block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-0" class="col-info col-info-left col-info-select economy-flight--green column-trigger-animation anim-all cabin-class-<%-familiesCabinGroup%> fareFamily-trigger data-trigger-animation-<%- flightIdx %>-<%- segmentsIdx %>-0" tabindex="0">
					<span class="head-col"><span class="text-head">
						<% if(familiesCabinGroup == "First") { %>
							<%=labels.first%><%=labels.suites%>
						<% } else { %>
							<%- familiesCabinGroup %>
						<% } %>
					</span></span>
								<div class="flight-result-opt-row primary anim-all" data-result-opt-row-v1>
												<div class="flight-price"><span class="from">From</span>
													<% var arrayItem = [] %>
								<span class="price" data-sort-price="<%- familiesCabinGroup %>">
								<% _.each(segment, function(segmentFlight){ %>
								<% segmentFlight.data.sort(function(a, b) {
									return parseFloat(a.price) - parseFloat(b.price);
								}); %>
								<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
									<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business") && segmentFlight.data.length > 0 ) { %>
									<% arrayItem.push({"priceSort" : segmentFlight.data[0].pricePerAdult }) %>
									<% return false %>
								<% } %>
								<% }); %>
								<% }); %>
								<% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
								<% arrayItem.sort(function(a, b) {
									return parseFloat(a.priceSort) - parseFloat(b.priceSort);
								}); %><% var priceStyleSmall = arrayItem[0].priceSort.toFixed(precision).indexOf(".") ;%>
								<%if(precision > 0){%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><small><%- arrayItem[0].priceSort.toFixed(precision).slice(priceStyleSmall) %></small>
								<%}else{%>
								<%- parseFloat(arrayItem[0].priceSort.toFixed(precision)).toLocaleString() %>
								<%}%>
								<% } %>
													</span>
							<% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
							 <% arrayItem.sort(function(a, b) {
															return parseFloat(a.priceSort) - parseFloat(b.priceSort);
														}); %>
														<div class="price-cheapest-outbound hidden"></div>
														<div class="price-cheapest-colum hidden"><%- arrayItem[0].priceSort %></div>
													<% } %>
													<span class="adult"><%=labels.oneAdult%></span><em class="ico-point-d"></em>
												</div>
									<span class="not-available"></span>
											</div>
								<div class="flight-result-opt-row optional anim-all hidden-mb-small" data-result-opt-row-v2>
												<% if(segment && segment.length > 0) { %>
										<div class="flight-price">
											<span class="select"><%=labels.selected%></span>
													<% var arrayItem = [] %>
													<span class="price">
														<% _.each(segment, function(segmentFlight){ %>
															<% segmentFlight.data.sort(function(a, b) {
																return parseFloat(a.price) - parseFloat(b.price);
															}); %>
															<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business") && segmentFlight.data.length > 0 ) { %>
																<% arrayItem.push({"priceSort" : segmentFlight.data[0].pricePerAdult})%>
																	<% return false %>
															<% } %>
															<% }); %>
														<% }); %>
														<% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
															<% arrayItem.sort(function(a, b) {
																return parseFloat(a.priceSort) - parseFloat(b.priceSort);
															}); %><% var priceStyleSmall = arrayItem[0].priceSort.toFixed(precision).indexOf(".") ;
															 %><%if(precision > 0){%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><small><%- arrayItem[0].priceSort.toFixed(precision).slice(priceStyleSmall) %></small><%}else{%>
								 <%- parseFloat(arrayItem[0].priceSort.toFixed(precision)).toLocaleString() %><%}%>
														<% } %>
													</span>
							<span class="adult"><%=labels.oneAdult%></span><em class="ico-point-u"></em></div><span class="not-available"><span>
												<% } %>
											</div>
										</div>
									<% } %>
								<% } else if(familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') { %>
									<% if(segment && segment.length > 0) { %>
					<% if(ffExists == true) { %>
										<div data-trigger-animation="<%- flightIdx %>-<%- segmentsIdx %>-1" data-trigger-popup=".popup-table-flight" data-content-popup="block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-1" class="col-info col-info-right col-info-select economy-flight--pey column-trigger-animation-1 anim-all fareFamily-trigger cabin-class-<%-familiesCabinGroup%>" tabindex="0">
					<span class="head-col">
						<span class="text-head">
							<% if(familiesCabinGroup == "First") { %>
								<%=labels.first%><%=labels.suites%>
							<% } else { %>
								<%- familiesCabinGroup %>
							<% } %>
						</span>
					</span>
					 <%}else{%>
					 <div data-trigger-animation="<%- flightIdx %>-<%- segmentsIdx %>-1" data-trigger-popup=".popup-table-flight" data-content-popup="block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-1" class="col-info col-info-left col-info-select economy-flight--pey column-trigger-animation-1 anim-all fareFamily-trigger cabin-class-<%-familiesCabinGroup%>" tabindex="0">
					 <span class="head-col">
						<span class="text-head">
							<% if(familiesCabinGroup == "First") { %>
								<%=labels.first%><%=labels.suites%>
							<% } else { %>
								<%- familiesCabinGroup %>
							<% } %>
						</span>
					 </span>
					 <% } %>
									<div class="flight-result-opt-row primary anim-all" data-result-opt-row-v1>
										<div class="flight-price">
											<span class="from">From</span>
							<% var arrayItem = [] %>
													<span class="price" data-sort-price="<%- familiesCabinGroup %>">
														<% _.each(segment, function(segmentFlight){ %>
															<% segmentFlight.data.sort(function(a, b) {
																return parseFloat(a.price) - parseFloat(b.price);
															}); %>
															<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First") && segmentFlight.data.length > 0 ) { %>
									<% arrayItem.push({"priceSort" : segmentFlight.data[0].pricePerAdult }) %>
																	<% return false %>
																<% } %>
															<% }); %>
														<% }); %><% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
															<% arrayItem.sort(function(a, b) {
																return parseFloat(a.priceSort) - parseFloat(b.priceSort);
															}); %><% var priceStyleSmall = arrayItem[0].priceSort.toFixed(precision).indexOf(".") ;
															 %><%if(precision > 0){%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString() %><small><%- arrayItem[0].priceSort.toFixed(precision).slice(priceStyleSmall) %></small><%}else{%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision)).toLocaleString()%><%}%><%}%>
													</span>
													<% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
															<% arrayItem.sort(function(a, b) {
																return parseFloat(a.priceSort) - parseFloat(b.priceSort);
															}); %>
														<div class="price-cheapest-outbound-1 hidden"></div>
														<div class="price-cheapest-colum hidden"><%- arrayItem[0].priceSort %></div>
													<% } %>
													<span class="adult"><%=labels.oneAdult%></span><em class="ico-point-d"></em></div><span class="not-available"><span>
												</div>
									<div class="flight-result-opt-row optional anim-all hidden-mb-small" data-result-opt-row-v2>
												<div class="flight-price"><span class="select"><%=labels.selected%></span>
													<% var arrayItem = [] %>
													<span class="price">
													<% _.each(segment, function(segmentFlight){ %>
														<% segmentFlight.data.sort(function(a, b) {
															return parseFloat(a.price) - parseFloat(b.price);
														}); %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First") && segmentFlight.data.length > 0 ) { %>
																<% arrayItem.push({"priceSort" : segmentFlight.data[0].pricePerAdult}) %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
													<% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
														<% arrayItem.sort(function(a, b) {
															return parseFloat(a.priceSort) - parseFloat(b.priceSort);
														}); %>
														<% var priceStyleSmall = arrayItem[0].priceSort.toFixed(precision).indexOf(".")%><%if(precision > 0){%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><small><%- arrayItem[0].priceSort.toFixed(precision).slice(priceStyleSmall)%></small>
							<%}else{%><%- parseFloat(arrayItem[0].priceSort.toFixed(precision)).toLocaleString() %>
							<%}%><% } %>
													</span>
													<span class="adult"><%=labels.oneAdult%></span>
													<em class="ico-point-u"></em>
												</div>
										<span class="not-available"></span>
											</div>
										</div>
									<% } %>
								<% } %>
							<% }); %>
						<% } %>
					</div>
					<% if(segments.segmentID == segmentID) { %>
						<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
							<% if(familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') { %>
				<div data-hidden-recommended data-segmentid="<%- segmentID %>" data-col-index="<%- flightIdx %>-<%- segmentsIdx %>-0" class="select-fare-block block-show-popup-mobile block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-0 anim-all">
									<div class="block-content-flight">
										<div class ="price-cheapest-colum hidden"></div>
										<div class="select-fare--inner">
											<div class="title-popup-mb"><span class="sub-heading-2--blue">Select fare</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible"><%=labels.close%></span><span class="text"></span></a>
												<a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em><%=labels.privileges%></a>
											</div>
											<div class="description-fare-mb">
												<span class="title-5--dark"><%= matchName(segments.originAirportCode) %> to <%= matchName(segments.destinationAirportCode) %></span>
												<%
													var txt = null;

													if (nonStop) { txt = 'Non-stop •'; }
													else if (oneStop) { txt = 'One-stop •'; }
													else if (layovers == 2) { txt = 'Two-stop •'; }
													else if (layovers >= 3) { txt = layovers.toString() + ' stops •'; }
												%>
												<span class="stop-time"><%- txt %> <%- timeTotal %></span>
											</div>
											<% _.each(segments.legs, function(segmentsLegs, legsIdx) { %>
												<div class="scoot-info  <% if(segmentsLegs.operatingAirline.code !== "TR" ) { %> hidden <% } %>">
													<div data-accordion-wrapper="1">
														<div data-accordion-wrapper-content="1">
															<div data-accordion="1" data-accordion-append>
																<a href="#" class="group-title" data-accordion-trigger="1" aria-expanded="false">
																<div class="scoot-thumb">
																	<% if(!$('html').hasClass('ie8')){%>
																		<img src="saar5/images/svg/tr.svg" alt="SC Logo" longdesc="img-desc.html">
																	<% }else{%>
																		<img src="images/scoot-img-large.jpg" alt="SC Logo" longdesc="img-desc.html">
																	<% }%>
																	</div>
																	<span class="scoot-title hidden-tb-dt"><%=labels.scootBenefits%></span>
																	<em class="ico-point-d hidden-tb-dt"></em>
																</a>
																<div class="scoot-content" data-accordion-content="1">
																	<div class="content"><%=labels.scoot%> </div>
																	<div class="list-items">
																		<div class="item">
																		<div class="img-thumb"><img src="saar5/images/icon-fly-bag-eat.png" alt="Icon fly bag eat" longdesc="img-desc.html">
																		</div>
																		<ul><li><%=labels.baggageAllowance%></li><li><%=labels.hotMeal%></li></ul>
																	</div>
																</div>
																<div class="more-info">
																	<% if(checkPre && !_.isEmpty(info)) { %>
																	<a href="#" class="link-4" data-trigger-popup=".popup-view-our-partner-airlines"><em class="ico-point-r"></em><%=labels.fareMsg5%></a>
																	<% } else { %>
																			<a href="#" class="link-4" data-trigger-popup=".popup-view-partner-airlines"><em class="ico-point-r"></em><%=labels.fareMsg6%></a>
																	<% } %>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
										 <% }); %>
										<% if($('body').hasClass('sk-ut-flight-search-b') && segments.segmentID === 3) { %>
										<div class="alert-block checkin-alert" style="margin: 20px; padding-left: 10px; padding-right: 10px; background-color: #E4E9EF;">
											<div class="inner">
												<div class="alert__icon" style="padding-right: 10px;"><em class="ico-alert" style="color: #00266b; font-size: 20px; width: 20px; height: 20px;"></em></div>
												<div class="alert__message" style="color: #00266b;"><%=labels.fareMsg7%><a href="#" data-trigger-popup=".popup-view-our-partner-airlines" tabindex="0"><%=labels.fareMsg8%> </a>
												</div>
											</div>
										</div>
										<% } %>
											<div class="select-fare-table multy-column hidden-mb-small <% _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) { %> <% if(fareFamiliesIdx > 4) { %> five-fare <% } %> <% }); %> ">
												<div class="row-head-select">
													<div class="col-select fare-condition"><span><%=labels.conditions%></span></div>
													<% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% if(fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') { %>
																	<div class="col-select economy-fs--green-<%- fareFamilyGroupIdx + 1 %>">
																		<% if(fareFamilyGroupIdx == 0) { %>
																			<span class="label-status"></span>
																		<% } %>
																		<span class="name-header-family"><%if(fareFamilyGroup.fareFamilyName!=null){%><%- fareFamilyGroup.fareFamilyName.replace("-","") %><% } %></span>
																		<span class="name-header-hidden hidden"><%if(fareFamilyGroup.fareFamilyName!=null){%><%- fareFamilyGroup.fareFamilyName.replace("-","") %><% } %></span>
																	</div>
																<% } %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select first-row">
													<div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
													<% var arrayStack=[]%>
						 <% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								 <% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
								 <% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																		 <% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																		 <% } else if(fareTypes.isBaggage == 'Yes') { %>
																				<span class="fare-price baggage "><%- fareTypes.baggage %></span>
																			<% } else { %>
																				<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																			<% } %>
																		</div>
								 <%}%>
																	<% } %>
									<% } %>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select last-row">
													<div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%></span></span></div>
							 <% var arrayStack=[]%>
													 <% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								 <% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
								<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																	 <div class="col-select">
										<% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>

																		</div>
								 <%}%>
																	<% } %>
								<%}%>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select first-row">
													<div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
							 <% var arrayStack=[]%>
													 <% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																			<% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %>
																		</div>
									<%}%>
																	<% } %>
								<%}%>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select last-row">
													<div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
														<% var arrayStack=[]%>
							 <% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																			<% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %>
																		</div>
									<%}%>
									<%}%>
																	<% } %>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select first-row">
													<div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
							 <% var arrayStack=[]%>
													<% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																			<% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %>
																		</div>
									<%}%>
																	<% } %>
								<%}%>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select">
													<div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
							 <% var arrayStack=[]%>
													<% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																			<% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %>
																		</div>
									<%}%>
																	<% } %>
								<%}%>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select">
													<div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
														<% var arrayStack=[]%>
							 <% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																		<div class="col-select">
																			<% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %>
																		</div>
									<%}%>
																	<% } %>
								<%}%>
																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
												<div class="row-select row-footer-select">
													<div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em><%=labels.privileges%></a>
													</div>
													<% _.each(segment, function(segmentFlight){ %>
														<% segmentFlight.data.sort(function(a, b) {
															return parseFloat(a.price) - parseFloat(b.price);
														}); %>
														 <% var arrayStack=[]%>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
																	<% if(rebookVal == 'false') { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
																		<div class="col-select">
																			<a href="#" class="btn-price column-left " data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																				<span class="rcmid-corresponding hidden"></span>
																				<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																				<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																				<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																				<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																				 %>
																				<span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																				<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span></span>
										<%}else{%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																				<span class="header-family hidden"><%- fareFamilyGroup.fareFamilyName %></span>
																			</a>
																			<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																			<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																				<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																		<% } %>
																		<% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
					<% if( (segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="") ||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="")) { %>
								 <% if( (segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode) || (segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>
								<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
								<% } %>
					<% } %>
																		<% }); %>
																		</div>
																	<% } %>
									<%}%>
																	 <% }else{ %>

																	 <% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>

																		<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') ) { %>

																	 <% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																	 <div class="col-select">
																			<a href="#" class="btn-price column-left " data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																				<span class="rcmid-corresponding hidden"></span>
																				<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																				<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																				<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																				<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																				 %>
																				<span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																				<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span></span>
										<%}else{%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																				<span class="header-family hidden"><%- fareFamilyGroup.fareFamilyName %></span>
																			</a>
																			<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																			<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																				<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																		<% } %>
																		<% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
					<% if((segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="") ||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="")) { %>
								 <% if( (segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode) || (segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>
								<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
								<% } %>
					<% } %>
																		<% }); %>
																		</div>
																		<% } %>
																			<% } %>
																		<% } %>

																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %>
												</div>
											</div>
											<div class="one-column hidden">
												<div class="select-fare-table one-fare">
													<div class="row-head-select">
														<div class="col-select economy-fs--green-1"><span><% _.each(segment, function(segmentFlight){ %>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% if(fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') { %>
																		<% if(fareFamilyGroupIdx == 0) { %>
																		<% } %>
																		<span class="name-header-family"><%if(fareFamilyGroup.fareFamilyName!=null){%><%- fareFamilyGroup.fareFamilyName.replace("-"," ") %> <%=labels.fareCondtns%><% }%></span>
																		<span class="name-header-hidden hidden"><%- fareFamilyGroup.fareFamilyName %></span>
																<% } %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %></span></div>
													</div>
													<div class="row-select">
														<div class="col-select">
															<div class="col-item col-item--1">
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
											<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
											<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																					<% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																				<% } else if(fareTypes.isBaggage == 'Yes') { %>
																						<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																					<% } else { %>
																						<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																					<% } %>
											<%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %>
								</div>
																</div>
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																			 <% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>
									<%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
										<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				 <% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %>
										<%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
										<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																					<% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %>
										<%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
															</div>
															<div class="col-item col-item--2">
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				 <% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %>
									<%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
																<div class="item item-booking-change">
																	<div class="item--left"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
										<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																					<% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %>
										<% } %>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
																<div class="item">
																	<div class="item--left"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
																	<div class="item--right">
									 <% var arrayStack=[]%>
									<% _.each(segment, function(segmentFlight){ %>
																	<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																		<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																			<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
										<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																				<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																				 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									 <% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																					 <% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %>
									 <%}%>
																				<% } %>
										<%}%>
																			<% }); %>
																			<% return false %>
																		<% } %>
																	<% }); %>
																<% }); %></div>
																</div>
															</div>
															<div class="col-item col-item--3"><% _.each(segment, function(segmentFlight){ %>
														<% segmentFlight.data.sort(function(a, b) {
															return parseFloat(a.price) - parseFloat(b.price);
														}); %>
														<% var arrayStack=[]%>
														<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
															<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business')&& fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<% if(rebookVal == 'false') { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																	 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
																			<a href="#" class="btn-price column-left" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																			<span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																				<span class="rcmid-corresponding hidden"></span>
																				<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																				<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																				<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																				<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																				 %>
																				<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span></span>
										<%}else{%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																			</a>
																			<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																			<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																				<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																			<% } %>
																		<% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
					<% if((segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
					 ||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>

																			<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
																			<% } %>

																		<% }); %>
																		<% } %>
								<%}%>
																	<% }else{ %>
																	<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business')) { %>
																		<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																			<a href="#" class="btn-price column-left" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																			<span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																				<span class="rcmid-corresponding hidden"></span>
																				<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																				<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																				<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																				<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																				 %>
																				<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span></span>
										<%}else{%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																			</a>
																			<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																			<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																				<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																			<% } %>
																		<% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
					<% if((segmentFlight.data[0].fareFamilyCode!= null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
					||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>

																			<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>

							<% } %>
																		<% }); %>
																		 <% } %>
																			<% } %>
																			<% } %>

																<% }); %>
																<% return false %>
															<% } %>
														<% }); %>
													<% }); %></div>
														</div>
													</div>
													<div class="row-select last-row hidden-mb-small">
														<div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em><%=labels.privileges%></a>
														</div>
													</div>
												</div>
											</div>
						<% var arrayStack1=[]%>
											<% _.each(segment, function(segmentFlight){ %>
												<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
													<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
														<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
							<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
															<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
															|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								<% if(arrayStack1.indexOf(segmentFlight.data[0].fareFamilyName) == -1){%>
								 <% arrayStack1.push(segmentFlight.data[0].fareFamilyName)%>
																<div class="hidden-tb-dt table-economy-green" data-table-mobile>
																	<!-- table same desktop: will have tables width col in desktop so for each it -->
																	<div class="select-fare-table multy-column">
																		<div class="row-head-select">
																			<div class="col-select economy-fs--green-<%- fareFamilyGroupIdx + 1 %>"><% if(fareFamilyGroupIdx == 0) { %><% } %>

										<span class="name-header-family"><%- segmentFlight.data[0].fareFamilyName.replace("-","") %></span>
										</div> </div><div class="row-select"><div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
																			<div class="col-select">
																				<% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																				<% } else if(fareTypes.isBaggage == 'Yes') { %>
																					<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																				<% } else { %>
																					<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																				<% } %>
																			</div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%></span></span></div>
																		<div class="col-select">
																		<% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>
																		</div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
																			<div class="col-select"> <% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %>
																			</div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
																			<div class="col-select"> <% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %></div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
																			<div class="col-select"><% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %></div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
																			<div class="col-select"> <% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %></div>
																		</div>
																		<div class="row-select">
																			<div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
																			<div class="col-select"> <% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %></div>
																		</div>
																		<div class="row-footer-select">
																			<div class="col-select full">
																				<% segmentFlight.data.sort(function(a, b) {
																					return parseFloat(a.price) - parseFloat(b.price);
																				}); %>
										<% var arrayStackEco=[]%>
																				<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																					<% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
																						<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
											<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																							<% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																							|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
												<% if(arrayStackEco.indexOf(segmentFlight.data[0].fareFamilyName) == -1){%>
												 <% arrayStackEco.push(segmentFlight.data[0].fareFamilyName)%>
																								<a href="#" class="btn-price column-left " data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																									<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																									 %>
																									 <span class="rcmid-corresponding hidden"></span>
																									 <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																									 <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																									 <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																									 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																									<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>
																									<%if(precision > 0){%>
							 <span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%></span><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span><%}else{%><span class="btn-price-cheapest"><%-parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString()%></span>
							<%}%>
																									<em class="ico-check-thick"></em>
																								</a>
																								<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																								<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																									<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																								<% } %>
																							 <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
						<% if( (segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
						||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>
																								<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
																								<% } %>
																							<% }); %>
												<%}%>
																							<% } %>
											<%}%>
																						<% }); %>
																						<% return false %>
																					<% } %>
																				<% }); %>
																			</div>
																		</div>
																	</div>
																	<!-- end -->
																</div>
								<%}%>
															<% } %>
							<%}%>
														<% }); %>
														<% return false %>
													<% } %>
												<% }); %>
											<% }); %>
											<div class="button-group-1 hidden-tb-dt">
												<input name="proceed-fare" value="Proceed" id="proceed-fare" type="button" class="btn-1 has-disabled" data-close>
												<input name="close-fare" value="Close" id="close-fare" type="button" class="btn-7" data-close>
											</div>
										</div>
									</div>
									<div class="upsell hidden hidden-mb-small">
										<div class="title-popup-mb"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible"><%=labels.close%></span><span class="text"></span></a>
										</div>
										<div class="content"><span class="price-selected hidden"></span><%=labels.upgradeTo%> <strong class="name-family"> <%=labels.economyGood%></strong> <%=labels.additional%><strong class="currency-code"></strong><strong class="price-sgd"></strong><%=labels.enjoy%></div>
										<div class="list-items">
						<% if(!$('body').hasClass('sk-ut-workflow')) { %>
								<div class="item"><span class="thumb"><em class="ico ico-business-1"></em><strong class="item-info"><%=labels.fiveKg%></strong></span><span class="des"><%=labels.additionalBaggagePerPax%></span></div>
								<div class="item item-2"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info"><%=labels.fiftyPcnt%></strong></span><span class="des"><%=labels.earnMoreMiles%></span></div>
								<div class="item item-3"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info"><%=labels.flexibility%></strong></span><span class="des"><%=labels.saveBooking%></span></div>
								<div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info"><%=labels.seats%></strong></span><span class="des"><%=labels.paySeatSelection%></span></div>
						<% } else { %>
							<div class="item"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info"><%=labels.fiftyPcnt%></strong></span><span class="des"><%=labels.earnFiftyMiles%></span></div>
							<div class="item"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info"><%=labels.flexibility%></strong></span><span class="des"><%=labels.saveUsd%></span></div>
							<div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info"><%=labels.seats%></strong></span><span class="des"><%=labels.paySeatSelection%></span></div>
						<% } %>
										</div>
										<div class="form-group group-btn">
											<input type="submit" name="btn-keep-selection" id="btn-keep-selection" value="Keep my selection" style="margin-top: 15px;" class="btn-4">
											<input type="submit" name="btn-upgrade" id="btn-upgrade" value="Upgrade" class="btn-1">
										</div>
									</div>
								</div>
							<% } %>
							<% if(familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') { %>
				<div data-hidden-recommended-1 data-segmentid="<%- segmentID %>" data-col-index="<%- flightIdx %>-<%- segmentsIdx %>-1" class="select-fare-block block-show-popup-mobile block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-1 anim-all">
									<div class ="price-cheapest-colum hidden"></div>
									<div class="select-fare--inner">
										<div class="title-popup-mb"><span class="sub-heading-2--blue">Select fare</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible"><%=labels.close%></span><span class="text"></span></a>
											<a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-premium-benefit--krisflyer"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.privileges%></a>
										</div>
										<div class="description-fare-mb">
											<span class="title-5--dark"><%= matchName(segments.originAirportCode) %> to <%= matchName(segments.destinationAirportCode) %></span>
											<%
												var txt = null;

												if (nonStop) { txt = 'Non-stop •'; }
												else if (oneStop) { txt = 'One-stop •'; }
												else if (layovers == 2) { txt = 'Two-stop •'; }
												else if (layovers >= 3) { txt = layovers.toString() + ' stops •'; }
											%>
											<span class="stop-time"><%- txt %> <%- timeTotal %></span>
										</div>

		<% if(familiesCabinGroup == 'Premium Economy' || !(familiesCabinGroup == 'First')) { %>
										<div class="additional-privileges">
											<% if(checkPre && !_.isEmpty(info)) { %>
												<div class="notification-panel">
													<div class="inner">
														<div class="alert__icon"><em class="ico-info-round-fill"></em></div>
														<div class="alert__message"><%= info.header %> <%=labels.isNotOffered%> <%= info.flightNumber %> from <%= info.origin %> to <%= info.destination %></div>
													</div>
												</div>
											<% } %>
	<div class="news-item">
		<h4 class="title-4--blue"><%=labels.inflight%></h4>
		<div>
			<div class="col-item">
				<div class="col-inner">
					<img src="saar5/images/greater_comfort.png" alt="images 1"/>
					<div class="text-content">
						<h5 class="title-5--blue"><%=labels.comfort%></h5>
						<p class="desc"><%=labels.sit%></p>
					</div>
				</div>
			</div>
			<div class="col-item">
				<div class="col-inner">
					<img src="saar5/images/greater_choices.png" alt="images 1"/>
					<div class="text-content">
						<h5 class="title-5--blue"><%=labels.choice%></h5>
						<p class="desc"><%=labels.cook%></p>
					</div>
				</div>
			</div>
			<div class="col-item">
				<div class="col-inner">
					<img src="saar5/images/priority_treatment.png" alt="images 1"/>
					<div class="text-content">
						<h5 class="title-5--blue"><%=labels.priority%></h5>
						<p class="desc"><%=labels.checkin%></p>
					</div>
				</div>
			</div>
		</div>
											</div>
										</div>
<%}%>
										<div class="select-fare-table multy-column hidden-mb-small third-fare">
											<div class="row-head-select">
												<div class="col-select fare-condition"><span><%=labels.conditions%></span></div>
						<%var arrayStack=[]%>
												<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
							<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% if(fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') { %>
								<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																<div class="col-select economy-fs--pey-<%- fareFamilyGroupIdx + 1 - fareFamilyGroupIdx %>">
				<span class="name-header-family"><%if(fareFamilyGroup.fareFamilyName!=null){%><%- fareFamilyGroup.fareFamilyName.replace("-","") %> <% }%></span>
																</div>
															<% } %>
															<% return false %>
														<% } %>
							<%}%>
													<% }); %>
												<% }); %>
											</div>
											<div class="row-select first-row">
												<div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
						<%var arrayStack=[]%>
												<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
							<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																		<% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																		<% } else if(fareTypes.isBaggage == 'Yes') { %>
																			<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																		<% } else { %>
																			<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																		<% } %>
																	</div>
							<%}%>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %>
											</div>
											<div class="row-select last-row">
												<div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%></span></span></div>
						<%var arrayStack=[]%>
												<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First')  &&  fareFamilyGroup.fareFamily == segmentFlight.type &&(segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
							<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
																	<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">

								<% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>

																	</div>
							<%}%>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %>
											</div>
											<div class="row-select first-row">
												<div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
						<%var arrayStack=[]%>
												<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First')  && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									 <%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																		 <% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %>
																	</div>
									<% } %>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %>
											</div>
											<div class="row-select last-row">
												<div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
												<%var arrayStack=[]%>
						<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
							<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
							<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
																	<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																		 <% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %>
																	</div>
									<% } %>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>

													<% }); %>
												<% }); %>
											</div>
											<div class="row-select first-row">
												<div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
												<%var arrayStack=[]%>
							<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
							<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="")||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																 || (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
							 <%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																	 <% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %>
																	</div>
									<% } %>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>

													<% }); %>
												<% }); %>
											</div>
											<div class="row-select">
												<div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
												<%var arrayStack=[]%>
						<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>

							<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																		<% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %>
																	</div>
									<% } %>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>

													<% }); %>
												<% }); %>
											</div>
											<div class="row-select">
												<div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
												<%var arrayStack=[]%>
						<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
							<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
								<%if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<%arrayStack.push(fareFamilyGroup.fareFamilyName)%>
									<div class="col-select">
																		 <% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %>
																	</div>
									<% } %>
																<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>

													<% }); %>
												<% }); %>
											</div>
											<div class="row-select row-footer-select">
												<div class="col-select"><a href="#" class="link-4" data-trigger-popup=".popup-view-premium-benefit--krisflyer"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em><%=labels.privileges%></a>
												</div>
												<% _.each(segment, function(segmentFlight){ %>
													<% segmentFlight.data.sort(function(a, b) {
														return parseFloat(a.price) - parseFloat(b.price);
													}); %>

						 <%var arrayStack=[]%>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First')  && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && segmentFlight.data[0].fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
								<%if((arrayStack.indexOf(segmentFlight.data[0].fareFamilyName) == -1) && (segmentFlight.data[0].fareFamilyCode == fareFamilyGroup.fareFamilyCode)){%>
																<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
																	<%arrayStack.push(segmentFlight.data[0].fareFamilyName)%>
									<div class="col-select">
																		<a href="#" class="btn-price col-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																		<span class="rcmid-corresponding hidden"></span>
																		<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																		<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																		<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																			<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																			 %>
																			 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																			 <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%></span><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span><%}else{%><span class="btn-price-cheapest-1"><%-parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString()%></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																		</a>
																		<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																		<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																			<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																		<% } %>
								 <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
		 <% if( (segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
			||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD)) { %>
								 <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
			 <% } %>
								<% }); %>
																	</div>
																<% } %>
								<% } %>
								<%}%>
															<% }); %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %>
											</div>
										</div>
										<div class="one-column hidden">
											<div class="select-fare-table one-fare">
												<div class="row-head-select">
													<div class="col-select economy-fs--green-1"><span>

							<% _.each(segment, function(segmentFlight){ %>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First')  && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% if(fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') { %>
																	<% if(fareFamilyGroupIdx == 0) { %>
																	<% } %>
																	<span class="name-header-family"><%if(fareFamilyGroup.fareFamilyName!=null){%><%- fareFamilyGroup.fareFamilyName.replace("-","") %> <% }%> <%=labels.fareCondtns%></span>
																	<span class="name-header-hidden hidden"><%- fareFamilyGroup.fareFamilyName %></span>
															<% } %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %></span></div>
												</div>
												<div class="row-select">
													<div class="col-select">
														<div class="col-item col-item--1">
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
																<div class="item--right">
								 <% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																			 <% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																				<% } else if(fareTypes.isBaggage == 'Yes') { %>
																					<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																				<% } else { %>
																					<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																				<% } %>
									<%}%>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%> </span></span></div>
																<div class="item--right">
								 <% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																	<% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>
										<% } %>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
																<div class="item--right">
								 <% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				 <% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %>
									<%}%>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
																<div class="item--right">
								<% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
										<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				 <% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %>
										<% } %>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
														</div>
														<div class="col-item col-item--2">
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
																<div class="item--right">
								 <% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																			 <% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %>
									<%}%>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
															<div class="item item-booking-change">
																<div class="item--left"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
																<div class="item--right">
								<% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				<% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %>
										 <% } %>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
															<div class="item">
																<div class="item--left"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
																<div class="item--right">
								 <% var arrayStack=[]%>
								<% _.each(segment, function(segmentFlight){ %>
																<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																	<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																		<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
									<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																			<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																			|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
									<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
									<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																				 <% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %>
									<%}%>
																			<% } %>
									<%}%>
																		<% }); %>
																		<% return false %>
																	<% } %>
																<% }); %>
															<% }); %></div>
															</div>
														</div>
														<div class="col-item col-item--3">
							<% _.each(segment, function(segmentFlight){ %>
													<% segmentFlight.data.sort(function(a, b) {
														return parseFloat(a.price) - parseFloat(b.price);
													}); %>
													 <% var arrayStack=[]%>
													<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
														<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
															<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
															<% if(rebookVal == 'false') { %>
								<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
								<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
								|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
																		<a href="#" class="btn-price column-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																			<span class="rcmid-corresponding hidden"></span>
																			<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																			<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																			<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																			<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																			 %>
																			 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																			 <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest-1"><%-parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%></span><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span>
										<%}else{%>
										<span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																		</a>
																		<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																		<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																			<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																		<% } %>
								 <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
			<% if((segmentFlight.data[0].fareFamilyCode!=null &&  fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
			 ||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>
									<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
			<% } %>
								<% }); %>
																<% } %>
								<%}%>
																	<% }else{ %>
																	<% if(arrayStack.indexOf(fareFamilyGroup.fareFamilyName) == -1){%>
																	<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type ) { %>
																		<% arrayStack.push(fareFamilyGroup.fareFamilyName)%>
																			<a href="#" class="btn-price column-left" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																			<span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																				<span class="rcmid-corresponding hidden"></span>
																				<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																				<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																				<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																				<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																				 %>
																				<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

										<%if(precision > 0){%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span></span>
										<%}else{%>
										<span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
										<%}%>
																				<em class="ico-check-thick"></em>
																			</a>
																			<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																			<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																				<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																			<% } %>
																		<% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
																			 <% if((segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
																				||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>

																			<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>

							 <% } %>
																		<% }); %>
																		 <% } %>
																			<% } %>
																			<% } %>
															<% }); %>
															<% return false %>
														<% } %>
													<% }); %>
												<% }); %></div>
													</div>
												</div>
												<div class="row-select last-row hidden-mb-small">
													<div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-premium-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em><%=labels.privileges%></a>
													</div>
												</div>
											</div>
										</div>

					<% var arrayStackPey=[]%>
										<% _.each(segment, function(segmentFlight){ %>
											<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
		<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
													<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
							<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
														<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
														|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
				 <% if(arrayStackPey.indexOf(segmentFlight.data[0].fareFamilyName) == -1){%>

				<% arrayStackPey.push(segmentFlight.data[0].fareFamilyName)%>
															<div class="hidden-tb-dt table-economy-green" data-table-mobile>
																<div class="select-fare-table multy-column">
																	<div class="row-head-select">
																		<div class="col-select economy-fs--pey-1"><span><%- segmentFlight.data[0].fareFamilyName.replace("-","") %></span></div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span><%=labels.baggage%></span></span></div>
																		<div class="col-select">
																		<% if(fareTypes.isBaggage == 'No') { %>
																					 <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
																		<% } else if(fareTypes.isBaggage == 'Yes') { %>
																				<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																			<% } else { %>
																				<span class="fare-price baggage"><%- fareTypes.baggage %></span>
																			<% } %>
																		</div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span><%=labels.seat%></span></span></div>
										 <div class="col-select">
										<% if(null != fareTypes.seatSelection) { %>
																<% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");%>
															<%  } else { %>
																<% var seatSelectionIndexNote = -1; %>
															<% } %>
																		<% if(seatSelectionIndexNote > 0) { %>
																			 <% if(fareTypes.isSeatSelection == 'No') { %>
																					<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else if(fareTypes.isSeatSelection == 'Yes') { %>
																					<span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } else { %>
																					<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																				<% } %>
										<% } else { %>
											<% if(fareTypes.isSeatSelection == 'No') { %>
																				<span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } else { %>
																				<span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
																			<% } %>
											<% } %>
																			</div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span><%=labels.earnMiles%></span></span></div>
																		<div class="col-select"> <% if(fareTypes.isEarnMiles == 'No') { %>
																				<span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } else if(fareTypes.isEarnMiles == 'Yes') { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																				<span class="loading loading--small hidden"></span>
<span class="ico-right miles-tooltipEarn  hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
								<em tabindex="0" data-content="<p class='tooltip__text-3'>data here</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
		</span>
																			<% } else { %>
																				<span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
																			<% } %></div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span><%=labels.upgrade%></span></span></div>
																		<div class="col-select"> <% if(fareTypes.isUpgradeAward == 'No') { %>
																				<span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } else if(fareTypes.isUpgradeAward == 'Yes') { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																						<span class="loading loading--small hidden"></span>
											<span class="ico-right miles-tooltip <%-segmentFlight.data[0].tooltipFlag%> hidden-mi" farefamily="<%-fareFamilyGroup.fareFamilyName%>">
																	<em tabindex="0"  data-content="<p class='tooltip__text-2'>data here</p>"
											data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill" aria-label="View more information"></em>
											</span>
																	<% } else { %>
																				<span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
																			<% } %></div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
																		<div class="col-select"><% if(fareTypes.isCancellation == 'No') { %>
																				<span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
																			<% } else if(fareTypes.isCancellation == 'Yes' && !(/\d/.test(fareTypes.cancellation))) { %>
																				<span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
																			<% } else { %>
																				<span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
																			<% } %></div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
																		<div class="col-select"> <% if(fareTypes.isItineraryChangeFee == 'No') { %>
																				<span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else if(fareTypes.isItineraryChangeFee == 'Yes' && !(/\d/.test(fareTypes.bookingChange))) { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } else { %>
																				<span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
																			<% } %></div>
																	</div>
																	<div class="row-select">
																		<div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
																		<div class="col-select"> <% if(fareTypes.isNoshow == 'No') { %>
																				<span class="not-allowed no-show"><%- fareTypes.noShow %></span>
																			<% } else if(fareTypes.isNoshow == 'Yes') { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } else { %>
																				<span class="fare-price no-show"><%- fareTypes.noShow %></span>
																			<% } %></div>
																	</div>
																	<div class="row-footer-select">
																		<div class="col-select full">
																			<% segmentFlight.data.sort(function(a, b) {
																				return parseFloat(a.price) - parseFloat(b.price);
																			}); %>
										<% var arrayStack1=[]%>
																			<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
																				<% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
																					<% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
											<%if((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="") ||(fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="")){%>
																						<% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && ((fareTypes.fareFamilyCode!=null && fareFamilyGroup.fareFamilyCode!=null && fareTypes.fareFamilyCode!="" && fareFamilyGroup.fareFamilyCode == fareTypes.fareFamilyCode)
																						|| (fareTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareTypes.RBD!="" && segmentFlight.data[0].RBD == fareTypes.RBD))) { %>
										<% if(arrayStack1.indexOf(segmentFlight.data[0].fareFamilyName) == -1){%>
												 <% arrayStack1.push(segmentFlight.data[0].fareFamilyName)%>
																							<a href="#" class="btn-price column-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
																							<span class="rcmid-corresponding hidden"></span>
																							<span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
																							<span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.pricePerAdult %>-<% }); %></span>
																							<span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.pricePerAdult %> <% }); %></span>
																								<% var priceStyleSmall = segmentFlight.data[0].pricePerAdult.toFixed(precision).indexOf(".") ;
																								 %>
																								 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
																								<span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].pricePerAdult %></span>

												<%if(precision > 0){%>
												<span class="btn-price-cheapest-1"><%-parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(0, priceStyleSmall)).toLocaleString()%></span><span class="unit-small"><%-segmentFlight.data[0].pricePerAdult.toFixed(precision).slice(priceStyleSmall)%></span>
												<%}else{%>
												<span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].pricePerAdult.toFixed(precision)).toLocaleString() %></span>
												<%}%>
																								<em class="ico-check-thick"></em>
																							</a>
																							<span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
																							<% if(segmentFlight.data[0].displayLastSeat == true) { %>
																								<span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> <%=labels.seatLeft%></span>
																							<% } %>
													 <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
				 <% if((segmentFlight.data[0].fareFamilyCode!=null && fareDataTypes.fareFamilyCode!=null && fareDataTypes.fareFamilyCode!="" && segmentFlight.data[0].fareFamilyCode == fareDataTypes.fareFamilyCode)
					||(fareDataTypes.RBD!=null && segmentFlight.data[0].RBD!=null && fareDataTypes.RBD!="" && segmentFlight.data[0].RBD == fareDataTypes.RBD)) { %>

														<span class="index-of hidden"> <%- fareDataTypesIdx %> </span>

					<% } %>
													<% }); %>
											<%}%>
																						<% } %>
											<%}%>
																					<% }); %>
																					<% return false %>
																				<% } %>
																			<% }); %>
																		</div>
																	</div>
																</div>
															</div>
														<%}%>
													<% } %>
													<%}%>
													<% }); %>
													<% return false %>
												<% } %>
											<% }); %>
										<% }); %>
										<div class="button-group-1 hidden-tb-dt">
											<input name="proceed-fare-1" value="Proceed" id="proceed-fare-1" type="button" class="btn-1 has-disabled" data-close>
											<input name="close-fare-1" value="Close" id="close-fare-1" type="button" class="btn-7" data-close>
										</div>
									</div>
								</div>
							<% } %>
						<% }); %>
					<% } %>

					<%
						var railsflyValid = '';
						if (railsData.valid) {
							railsflyValid = 'rail-fly';
						}
					%>

					<div class="change-flight-item bgd-white hidden <%= railsflyValid %>">
						<div data-flight-item class="col-info recommended-flight-item">
							<div class="flight-station">
								<% if(railsData.valid) { %>
										<span class="stop-time">Rail-Fly • <%- totalRailFlyTime.timeTotal %></span>
								<% } %>

								<% if (railsData.valid && (parseInt(railsData.index) == 0)) { %>
									<%
										var railsTimeTotal = null;
										var railsTime = 0;
										for(var railsTimeIdx = 0, railsTimeLen = segments.legs.length; railsTimeIdx < railsTimeLen; railsTimeIdx++) {
											var segmentLeg  = segments.legs[railsTimeIdx];
											if (segmentLeg.aircraft.code == 'TRN') {
												railsTime += segmentLeg.flightDuration;
											}
										}

										railsTimeTotal = formatFlightDuration(railsTime);


									%>
									<div class="rail-fly-station">
										<span class="rail-time">
		                  <em class="ico-5-rail"></em>
		                  <span class="title">RAIL</span>
		                  <span class="time-stop"><%- railsTimeTotal.timeTotal %></span>
		                </span>
										<div class="control-flight-station anim-all" data-first-wrap-flight>
											<div class="flight-station-item">
												<div class="flight-station--inner">
													<div class="flight-station-info">
													<% var cabinCount = (totalCabin.length == 1) ? 'single-cabin' : '' ;%>
														<div class="station-stop ">
		                          <span class="station-stop-detail">
		                            <em class="ico-5-rail"></em>
		                          </span>
		                        </div>
														<div class="flights-station__info--detail">
															<%
																var originAirportCode = null;
																var departureTime = null;
																var departureDate = null;
																for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		originAirportCode = segmentLegs.originAirportCode;
																		departureTime = formatFlightTime(segmentLegs.departureDateTime);
																		departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																		break;
																	}
																}
															%>
															<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<%
																var arrivalAirportCode = null;
																var arrivalTime = null;
																var ArrivalDate = null;
																var arrivalTerminal = null;
																var flightDuration = null;
																for (var legsIdy = (segments.legs.length - 1); legsIdy >= 0; legsIdy--) {
																	var segmentLegs = segments.legs[legsIdy];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		arrivalAirportCode = segmentLegs.destinationAirportCode;
																		arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																		ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																		arrivalTerminal = segmentLegs.arrivalTerminal;
																		flightDuration = formatFlightDuration(segmentLegs.flightDuration);
																		break;
																	}
																}
															%>
															<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																	if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>
															<%
																	return;
																	}
																});
															%>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="wrap-flight-station anim-all wrap-rail-fly-station">
									   <div class="flight-station-item flight-result-leg anim-all">
									      <div class="flight-station--inner">
									         <div class="flight-station-info">
									            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-5-rail"></em></span></div>
									            <div class="flights-station__info--detail">
									               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
									               	<%- originAirportCode %> <%- departureTime %>
								               	</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === originAirportCode ) { %>
																		<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-origin-date="<%- departureDate %>"
																			data-origin-airportname="<%- airports.airportName %>"
																			data-origin-terminal="">
																			<%- departureDate %>
																			<br><%- airports.airportName %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									            <div class="flights-station__info--detail return-flight">
							               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
							               			<%- arrivalAirportCode %> <%- arrivalTime %>
						               			</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === arrivalAirportCode ) { %>
																		<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-destination-date="<%- ArrivalDate %>"
																			data-destination-airportname="<%- airports.airportName %>"
																			data-destination-terminal="<%- arrivalTerminal %>">
																			<%- ArrivalDate %>
																			<br><%- airports.airportName %>
																			<br>Terminal <%- arrivalTerminal %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									         </div>
									         <div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta"
																	data-operationname="<%- info.name %>"
																	data-flightnumber="<%- info.code %>"
																	data-planename="TRAIN <%- info.flightnum %>">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>
															<%
																return;
																	}
																});
															%>
														</div>
													</div>
									      </div>

									      <%
													var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

													railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
												%>

												<% if (railLayOversTime) { %>
													<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
					                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
					                </span>
												<% } %>

									   	</div>
										</div>

									</div>
								<% } %>


									<%
										var txt = null;

										if (nonStop) { txt = 'Non-stop •'; }
										else if (oneStop) { txt = 'One-stop •'; }
										else if (layovers == 2) { txt = 'Two-stop •'; }
										else if (layovers >= 3) { txt = layovers.toString() + ' stops •'; }
									%>

										<% if(railsData.valid) { %>
									<%
										var flyTime = 0;
										for(var flyTimeIdx = 0, flyTimeLen = segments.legs.length; flyTimeIdx < flyTimeLen; flyTimeIdx++) {
											var segmentLeg  = segments.legs[flyTimeIdx];
											if (segmentLeg.aircraft.code != 'TRN') {
												flyTime += segmentLeg.flightDuration;
											}
										}

										flyTimeTotal = formatFlightDuration(flyTime);
									%>
									<span class="flight-time">
	                  <em class="ico-airplane-2"></em>
	                  <span class="title">FLIGHTS</span>
	                  <span class="time-stop"><%- layovers %> stop • <%- flyTimeTotal.timeTotal %></span>
	                </span>
								<% } else { %>
									<span class="stop-time"><%- txt %> <%- timeTotal %></span>
								<% } %>

								<div class="control-flight-station anim-all" data-first-wrap-flight>
									<div class="flight-station-item">
										<div class="flight-station--inner">
											<div class="flight-station-info">
												 <div class="station-stop">
													<% if(nonStop) { %>
														<span class="station-stop-detail">
															<em class="ico-airplane-2"></em>
														</span>
													<% } %>
													<% if (oneStop) { %>
														<span class="station-stop-detail one-stop-station">
															<span class="time time--1">
																<strong><%- oneStopDetail.code %> </strong><%- parseTimeToHour(oneStopDetail.layover) %>
															</span>
														</span>
													<% } %>
													<% if(layovers >= 2) { %>
															<% var stops = (layovers == 2) ? 'stop' : 'multistop' ; %>
															<% for(var i = multiStopDetail.length - 1, j = 0; i >= 0; i--, j++) { %>
																<% if(multiStopDetail[j].aircraft != 'TRN') { %>
																	<span class="two-stop-station <%- stops %>--<%- j + 1 %>">
																		<span class="time">
																			<strong><%- multiStopDetail[j].code %></strong>

																			<% if (layovers == 2) { %>
																				<%- parseTimeToHour(multiStopDetail[j].layover) %>
																			<% } %>

																		</span>
																	</span>
																<% } %>
															<% } %>
													<% } %>
												</div>
												<div class="flights-station__info--detail">
													<%
														var originAirportCode = null;
														var departureTime = null;
														var departureDate = null;
														for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
															var segmentLegs = segments.legs[legsIdx];
															if (segmentLegs.aircraft.code != 'TRN') {
																originAirportCode = segmentLegs.originAirportCode;
																departureTime = formatFlightTime(segmentLegs.departureDateTime);
																departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																break;
															}
														}
													%>
													<div class="code-origin-airport hidden"><%- segments.originAirportCode %></div>
													<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
													<% _.each(data.airports, function(airports, airportsIdx) { %>
														<% if( airports.airportCode === originAirportCode ) { %>
															<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
														<% } %>
													<% }); %>
												</div>
												<div class="flights-station__info--detail return-flight">
													<%
														var arrivalAirportCode = null;
														var arrivalTime = null;
														var ArrivalDate = null;
														for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
															var segmentLegs = segments.legs[legsIdx];
															if (segmentLegs.aircraft.code != 'TRN') {
																arrivalAirportCode = segmentLegs.destinationAirportCode;
																arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																break;
															}
														}
													%>
													<div class="code-destination-airport hidden"><%- segments.destinationAirportCode %></div>
													<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
													<% _.each(data.airports, function(airports, airportsIdx) { %>
														<% if( airports.airportCode === arrivalAirportCode ) { %>
															<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
														<% } %>
													<% }); %>
												</div>
											</div>
											<div class="airline-info">
												<% var msAirlineInfo = (layovers >= 3) ? 'multistops-airline-info': ''; %>
												<div class="inner-info <%- msAirlineInfo %>">
													<% if(layovers >= 3 || railsData.valid) { %>
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft != 'TRN') {
														%>
															<%
																var operatedBy = (info.code != 'SQ' || info.code == 'MI') ?
																	'Operated by' : '';
																var airlineImgClss = (airlineImg != '') ? 'airline-detail' : '' ;
																var airlineImg = '';
																var airlineImgPos = ''

																if (info.code == 'SQ') {
																	airlineImg = 'sq';
																	airlineImgPos = 'sq-img';
																}

																if (info.code == 'MI') {
																	airlineImg =  'si';
																	airlineImgPos = 'si-img';
																}
															%>
															<span class="airline-deta <%- airlineImgClss %>">
																<% if (airlineImg != '') { %>
																	<img class="<%- airlineImgPos %>" src="images/svg/<%- airlineImg %>.svg" alt="<%- airlineImg %> Logo" longdesc="img-desc.html">
																<% } %>
																<strong><%- operatedBy %> <%- info.name %></strong> • <%- info.code %> <%- info.flightnum %><br>
															</span>
														<%
																}
															});
														%>
													<% } else { %>
													<span class="airline-detail <%-noImage%>">
													<% if(segments.legs[0].operatingAirline.code == "SQ" || segments.legs[0].operatingAirline.code == "SI" || segments.legs[0].operatingAirline.code == "TR") { %>
														<img src="saar5/images/svg/<%- segments.legs[0].operatingAirline.code.toLowerCase() %>.svg" alt="<%- segments.legs[0].operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
													<% } %>

				 <% if(segments.legs[0].operatingAirline.code == "MI") { %>
														<img src="saar5/images/svg/si.svg" longdesc="img-desc.html">
				<% } %>


														<strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong> <%- segments.legs[0].marketingAirline.code.toUpperCase() %> <%- segments.legs[0].flightNumber %>
													</span>
													<% } %>

													<% if (railsData.valid && (parseInt(railsData.index) == 0)) { %>
														<a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.moreDetails%></a>
													<% } %>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div data-wrap-flight class="wrap-flight-station anim-all">
										<% if(railsData.valid && (parseInt(railsData.index) == 0)) { %>
										<%
											var originAirportCode = null;
											var departureTime = null;
											var departureDate = null;
											for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													originAirportCode = segmentLegs.originAirportCode;
													departureTime = formatFlightTime(segmentLegs.departureDateTime);
													departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
													break;
												}
											}
										%>
										<%
											var arrivalAirportCode = null;
											var arrivalTime = null;
											var ArrivalDate = null;
											var arrivalTerminal = null;
											var flightDuration = null;
											for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													arrivalAirportCode = segmentLegs.destinationAirportCode;
													arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
													ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
													arrivalTerminal = segmentLegs.arrivalTerminal;
													flightDuration = formatFlightDuration(segmentLegs.flightDuration);
													break;
												}
											}
										%>
							    	<div class="flight-station-item flight-result-leg anim-all hidden">
								      <div class="flight-station--inner">
								         <div class="flight-station-info">
								            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
								              <div class="flights-station__info--detail">
								               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
								               	<%- originAirportCode %> <%- departureTime %>
							               	</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-origin-date="<%- departureDate %>"
																		data-origin-airportname="<%- airports.airportName %>"
																		data-origin-terminal="">
																		<%- departureDate %>
																		<br><%- airports.airportName %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								            <div class="flights-station__info--detail return-flight">
						               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
						               			<%- arrivalAirportCode %> <%- arrivalTime %>
					               			</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-destination-date="<%- ArrivalDate %>"
																		data-destination-airportname="<%- airports.airportName %>"
																		data-destination-terminal="<%- arrivalTerminal %>">
																		<%- ArrivalDate %>
																		<br><%- airports.airportName %>
																		<br>Terminal <%- arrivalTerminal %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								         </div>
								         <div class="airline-info">
													<div class="inner-info">
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
															if (info.aircraft == 'TRN') {
														%>
															<span class="airline-deta"
																data-operationname="<%- info.name %>"
																data-flightnumber="<%- info.code %>"
																data-planename="TRAIN <%- info.flightnum %>">
																<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
															</span>
														<%
															return;
																}
															});
														%>
													</div>
												</div>
								      </div>

								      <%
												var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

												railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
											%>

											<% if (railLayOversTime) { %>
												<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
				                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
				                </span>
											<% } %>

							  	 	</div>
									<% } %>

									<%
										var segFlightLegs = [];
										for (var segFlightIdx = 0, segFlightLen = segments.legs.length; segFlightIdx < segFlightLen; segFlightIdx++) {
											var segFlight = segments.legs[segFlightIdx];
											if (segFlight.aircraft.code != 'TRN') {
												segFlightLegs.push(segFlight);
											}
									 	}
									%>
									<% _.each(segFlightLegs, function(segmentsLegs, legsIdx) { %>

										<%
											var departureTime = segmentsLegs.departureDateTime,
												arrivalDateTime = segmentsLegs.arrivalDateTime,
												newDepartureTime = departureTime.slice(11, 16),
												newArrivalDateTime = arrivalDateTime.slice(11, 16),
												departureSplit = departureTime.split(' '),
		departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
												arrivalDateTimeSplit = arrivalDateTime.split(' '),
		arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));
												var secondsLayover = parseInt(segmentsLegs.layoverDuration);
												var secondsFlight = parseInt(segmentsLegs.flightDuration);
												var hh2 = Math.floor(secondsLayover / 3600);
												var mm2 = Math.floor((secondsLayover - (hh2 * 3600)) / 60);
												if (hh2 < 10) {hh2 = "0"+hh2;}
												if (mm2 < 10) {mm2 = "0"+mm2;}
												var timeTotalLayover = hh2+"hr "+mm2+ "mins";
												var hh3 = Math.floor(secondsFlight / 3600);
												var mm3 = Math.floor((secondsFlight - (hh3 * 3600)) / 60);
												if (hh3 < 10) {hh3 = "0"+hh3;}
												if (mm3 < 10) {mm3 = "0"+mm3;}
												var timeTotalFlight = hh3+"h "+mm3+ "m";
												if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) {
													var departureStop = segmentsLegs.stops[0].departureDateTime,
													arrivalStop = segmentsLegs.stops[0].arrivalDateTime,
													newDepartureStop = departureStop.slice(11, 16),
													newArrivalStop = arrivalStop.slice(11, 16),
													departureSplitStop = departureStop.split(' '),
													arrivalSplitStop = arrivalStop.split(' '),
			departureDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(departureSplitStop[0].replace(/-/g,"/"))),
			arrivalDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(arrivalSplitStop[0].replace(/-/g,"/")));
													var secondsLayoverStop = parseInt(segmentsLegs.stops[0].layoverDuration);
													var hh4 = Math.floor(secondsLayoverStop / 3600);
													var mm4 = Math.floor((secondsLayoverStop - (hh4 * 3600)) / 60);
													if (hh4 < 10) {hh4 = "0"+hh4;}
													if (mm4 < 10) {mm4 = "0"+mm4;}
													var timeTotalLayoverStop = hh4+"hr "+mm4+ "mins";
												}
										%>
										<% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
											<div class="flight-station-item flight-result-leg anim-all">
												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
														<div class="flights-station__info--detail">
															<span class="hour"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
																<span class="country-name"><%- airports.cityName %></span>
																<span class="date"><%- departureDatepicker %><br>
																	<%- airports.airportName %>
									 <%airportName=airports.airportName%>
																<% } %>
															<% }); %>
														<br>
														<% if(segmentsLegs.departureTerminal !== '0') { %>
				<% if(segmentsLegs.departureTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.departureTerminal %>
				<%}%>
			<%}%>

										</span>
														</div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour"><%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
																<span class="country-name"><%- airports.cityName
																 %></span>
																 <span class="date"><%- departureDatepickerStop %><br>
																<%- airports.airportName %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
							<%var noImage=''%>
							<%if(segmentsLegs.flightNumber.length ==4){ %>
							<%noImage='no-image'%><%}%>
															<span class="airline-detail <%-noImage%>">
					<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>
						 <% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "TR" ) { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>
																<strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong> <%- segmentsLegs.marketingAirline.code.toUpperCase() %> <%- segmentsLegs.flightNumber %>
															</span>
															<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
															<% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
																<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
																	<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																		<span class="economy"><%- saar5.ccd.ccdSuites %></span>
																	<% } else { %>
																		<span class="economy"><%- familiesCabinGroup %></span>
																	<% } %>
																<% }); %>
															<% } %></div>
													</div>
												</div>

												<% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
													<span class="layover-time--1"><em class="ico-flight-history"></em>Layover time: <% if(timeTotalLayoverStop.slice(5,7) == "00") {%>
														<%- timeTotalLayoverStop.slice(0,4) %>
														<% } else { %>
														<%- timeTotalLayoverStop %>
														<% } %>
													 </span>
												<% } %>

												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
														<div class="flights-station__info--detail">
															<span class="hour"><%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
																<span class="country-name"><%- airports.cityName %></span>
																<span class="date"><%- arrivalDatepickerStop %><br>
																	<%- airports.airportName %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
																<span class="country-name"><%- airports.cityName
																 %></span><span class="date"><%- arrivalDateTimeDatepicker %><br>
																<%- airports.airportName %>
								 <%airportName=airports.airportName%>
																<% } %>
															<% }); %>
														<br>
				<% if(segmentsLegs.arrivalTerminal !== '0' && segmentsLegs.arrivalTerminal!=undefined) { %>
				<% if(segmentsLegs.arrivalTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.arrivalTerminal %>
				<%}%>
			<%}%>
						</span>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
							<%var noImage=''%>
							<%if(segmentsLegs.flightNumber.length ==4){ %>
							<%noImage='no-image'%>
							<%}%>
															<span class="airline-detail <%-noImage%>">
					<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>

						 <% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "Scoot TigerAir" ) { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>


																<strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong> <%- segmentsLegs.marketingAirline.code.toUpperCase() %> <%- segmentsLegs.flightNumber %>
															</span>
															<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
															<% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
																<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
																	<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																		<span class="economy"><%- saar5.ccd.ccdSuites %></span>
																	<% } else { %>
																		<span class="economy"><%- familiesCabinGroup %></span>
																	<% } %>
																<% }); %>
															<% } %>
															<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.lessDetails%></a></div>
													</div>
												</div>
												<% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
													<span class="layover-time--1"><em class="ico-flight-history"></em>Layover time: <% if(timeTotalLayover.slice(5,7) == "00") {%>
							<%- timeTotalLayover.slice(0,4) %>
							<% } else { %>
							<%- timeTotalLayover %>
							<% } %>
							</span>
												<% } %>
											</div>
										<% } else { %>
											<div class="flight-station-item flight-result-leg anim-all">
												<div class="flight-station--inner">
													<div class="flight-station-info">
														<div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
														<div class="flights-station__info--detail"><span class="hour"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
														<%var airportName%>
							<% _.each(data.airports, function(airports, airportsIdx) { %>
															<% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
															<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDatepicker %><br>
															<%- airports.airportName %>
								 <%airportName=airports.airportName%>
															<% } %>
														<% }); %>
														<br>

					<% if(segmentsLegs.departureTerminal !== '0' && segmentsLegs.departureTerminal !=undefined) { %>
				<% if(segmentsLegs.departureTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.departureTerminal %>
				<%}%>
			<%}%> </span></div>
														<div class="flights-station__info--detail return-flight">
															<span class="hour"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
								<%var airportName%>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
																<span class="country-name"><%- airports.cityName
																 %></span><span class="date"><%- arrivalDateTimeDatepicker %><br>
																<%- airports.airportName %>
								<%airportName=airports.airportName%>
																<% } %>
															<% }); %>
															<br>
				<% if(segmentsLegs.arrivalTerminal !== '0') { %>
				<% if(segmentsLegs.arrivalTerminal === 'I'){ %>
					<%=labels.flightMsg6%> <%=labels.international%>
				<%}else{%>
					<%=labels.flightMsg6%> <%- segmentsLegs.arrivalTerminal %>
				<%}%>
			<%}%>
						</span>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
							<%var noImage=''%>
							<%if(segmentsLegs.flightNumber.length ==4){ %>
							<%noImage='no-image'%>
							<%}%>
															<span class="airline-detail <%-noImage%>">
					<% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
																<img src="saar5/images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
															<% } %>

						 <% if(segmentsLegs.operatingAirline.code == "MI" ) { %>
				<img src="saar5/images/svg/si.svg" longdesc="img-desc.html">
															<% } %>

						<% if(segmentsLegs.operatingAirline.code == "Scoot TigerAir" ) { %>
				<img src="saar5/images/svg/tr.svg"  Logo" longdesc="img-desc.html">
															<% } %>

																<strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong> <%- segmentsLegs.marketingAirline.code.toUpperCase() %> <%- segmentsLegs.flightNumber %>
															</span>
											<span class="name-plane <%-noImage%>"><%- segmentsLegs.aircraft.name %></span>
											<% if(segmentsLegs.aircraft.code!="TRN" && segmentsLegs.aircraft.code!="BUS") { %>
												<% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
													<% if(segmentsLegs.aircraft.code == "388" && segmentsLegs.operatingAirline.code === "SQ" && familiesCabinGroup == "First") { %>
																			<span class="economy"><%- saar5.ccd.ccdSuites %></span>
																		<% } else { %>
																			<span class="economy"><%- familiesCabinGroup %></span>
																		<% } %>
																	<% }); %>
																<% } %><a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.lessDetails%></a></div>
													</div>
												</div>
												<% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
													<span class="layover-time--1"><em class="ico-flight-history"></em><%=labels.layOver%> <% if(timeTotalLayover.slice(5,7) == "00") {%>
							<%- timeTotalLayover.slice(0,4) %>
							<% } else { %>
							<%- timeTotalLayover %>
							<% } %>
							</span>
												<% } %>
											</div>
										<% } %>
									<% }); %>

								<% if(railsData.valid && (parseInt(railsData.index) > 0)) { %>
										<%
											var originAirportCode = null;
											var departureTime = null;
											var departureDate = null;
											for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													originAirportCode = segmentLegs.originAirportCode;
													departureTime = formatFlightTime(segmentLegs.departureDateTime);
													departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
													break;
												}
											}
										%>
										<%
											var arrivalAirportCode = null;
											var arrivalTime = null;
											var ArrivalDate = null;
											var arrivalTerminal = null;
											var flightDuration = null;
											for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
												var segmentLegs = segments.legs[legsIdx];
												if (segmentLegs.aircraft.code == 'TRN') {
													arrivalAirportCode = segmentLegs.destinationAirportCode;
													arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
													ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
													arrivalTerminal = segmentLegs.arrivalTerminal;
													flightDuration = formatFlightDuration(segmentLegs.flightDuration);
													break;
												}
											}
										%>
							    	<div class="flight-station-item flight-result-leg anim-all hidden">
								      <div class="flight-station--inner">
								         <div class="flight-station-info">
								            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
								              <div class="flights-station__info--detail">
								               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
								               	<%- originAirportCode %> <%- departureTime %>
							               	</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-origin-date="<%- departureDate %>"
																		data-origin-airportname="<%- airports.airportName %>"
																		data-origin-terminal="">
																		<%- departureDate %>
																		<br><%- airports.airportName %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								            <div class="flights-station__info--detail return-flight">
						               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
						               			<%- arrivalAirportCode %> <%- arrivalTime %>
					               			</span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																		<%- airports.cityName %>
																	</span>
																	<span class="date"
																		data-destination-date="<%- ArrivalDate %>"
																		data-destination-airportname="<%- airports.airportName %>"
																		data-destination-terminal="<%- arrivalTerminal %>">
																		<%- ArrivalDate %>
																		<br><%- airports.airportName %>
																		<br>Terminal <%- arrivalTerminal %>
																	</span>
																<% } %>
															<% }); %>
								            </div>
								         </div>
								         <div class="airline-info">
													<div class="inner-info">
														<% _.each(opAirlinesInfo, function(info, infoIdx) {
															if (info.aircraft == 'TRN') {
														%>
															<span class="airline-deta"
																data-operationname="<%- info.name %>"
																data-flightnumber="<%- info.code %>"
																data-planename="TRAIN <%- info.flightnum %>">
																<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
															</span>

															<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.lessDetails%></a>
														<%
															return;
																}
															});
														%>
													</div>
												</div>
								      </div>

								      <%
												var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

												railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
											%>

											<% if (railLayOversTime) { %>
												<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
				                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
				                </span>
											<% } %>

							  	 	</div>
									<% } %>

								</div>

								<% if (railsData.valid && (parseInt(railsData.index) > 0)) { %>
									<%
										var railsTimeTotal = null;
										var railsTime = 0;
										for(var railsTimeIdx = 0, railsTimeLen = segments.legs.length; railsTimeIdx < railsTimeLen; railsTimeIdx++) {
											var segmentLeg  = segments.legs[railsTimeIdx];
											if (segmentLeg.aircraft.code == 'TRN') {
												railsTime += segmentLeg.flightDuration;
											}
										}

										railsTimeTotal = formatFlightDuration(railsTime);


									%>
									<div class="rail-fly-station railsfly-return-station">
										<span class="rail-time">
		                  <em class="ico-5-rail"></em>
		                  <span class="title">RAIL</span>
		                  <span class="time-stop"><%- railsTimeTotal.timeTotal %></span>
		                </span>
										<div class="control-flight-station anim-all rail-fly-control-last" data-first-wrap-flight>
											<div class="flight-station-item">
												<div class="flight-station--inner">
													<div class="flight-station-info">
													<% var cabinCount = (totalCabin.length == 1) ? 'single-cabin' : '' ;%>
														<div class="station-stop ">
		                          <span class="station-stop-detail">
		                            <em class="ico-5-rail"></em>
		                          </span>
		                        </div>
														<div class="flights-station__info--detail">
															<%
																var originAirportCode = null;
																var departureTime = null;
																var departureDate = null;
																for (var legsIdx = 0, legsLen = segments.legs.length; legsIdx < legsLen; legsIdx++) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		originAirportCode = segmentLegs.originAirportCode;
																		departureTime = formatFlightTime(segmentLegs.departureDateTime);
																		departureDate = formatFlightDate(segmentLegs.departureDateTime.split(' '));
																		break;
																	}
																}
															%>
															<span class="hour"><%- originAirportCode %> <%- departureTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === originAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- departureDate %></span>
																<% } %>
															<% }); %>
														</div>
														<div class="flights-station__info--detail return-flight">
															<%
																var arrivalAirportCode = null;
																var arrivalTime = null;
																var ArrivalDate = null;
																var arrivalTerminal = null;
																var flightDuration = null;
																for (var legsIdx = (segments.legs.length - 1); legsIdx >= 0; legsIdx--) {
																	var segmentLegs = segments.legs[legsIdx];
																	if (segmentLegs.aircraft.code == 'TRN') {
																		arrivalAirportCode = segmentLegs.destinationAirportCode;
																		arrivalTime = formatFlightTime(segmentLegs.arrivalDateTime);
																		ArrivalDate = formatFlightDate(segmentLegs.arrivalDateTime.split(' '));
																		arrivalTerminal = segmentLegs.arrivalTerminal;
																		flightDuration = formatFlightDuration(segmentLegs.flightDuration);
																		break;
																	}
																}
															%>
															<span class="hour"><%- arrivalAirportCode %> <%- arrivalTime %></span>
															<% _.each(data.airports, function(airports, airportsIdx) { %>
																<% if( airports.airportCode === arrivalAirportCode ) { %>
																	<span class="country-name"><%- airports.cityName %></span><span class="date"><%- ArrivalDate %></span>
																<% } %>
															<% }); %>
														</div>
													</div>
													<div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																	if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>

																<a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible"><%=labels.moreInfo%></span></em><%=labels.moreDetails%></a>
															<%
																	return;
																	}
																});
															%>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="wrap-flight-station anim-all wrap-rail-fly-station">
									   <div class="flight-station-item flight-result-leg anim-all">
									      <div class="flight-station--inner">
									         <div class="flight-station-info">
									            <div class="station-stop" data-timeflight="<%- flightDuration.timeTotalStation %>"><span class="station-stop-detail"><em class="ico-5-rail"></em></span></div>
									            <div class="flights-station__info--detail">
									               <span class="hour" data-origin-hour="<%- originAirportCode %> <%- departureTime %>">
									               	<%- originAirportCode %> <%- departureTime %>
								               	</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === originAirportCode ) { %>
																		<span class="country-name" data-origin-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-origin-date="<%- departureDate %>"
																			data-origin-airportname="<%- airports.airportName %>"
																			data-origin-terminal="">
																			<%- departureDate %>
																			<br><%- airports.airportName %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									            <div class="flights-station__info--detail return-flight">
							               		<span class="hour" data-destination-hour="<%- arrivalAirportCode %> <%- arrivalTime %>">
							               			<%- arrivalAirportCode %> <%- arrivalTime %>
						               			</span>
																<% _.each(data.airports, function(airports, airportsIdx) { %>
																	<% if( airports.airportCode === arrivalAirportCode ) { %>
																		<span class="country-name" data-destination-countryname="<%- airports.cityName %>">
																			<%- airports.cityName %>
																		</span>
																		<span class="date"
																			data-destination-date="<%- ArrivalDate %>"
																			data-destination-airportname="<%- airports.airportName %>"
																			data-destination-terminal="<%- arrivalTerminal %>">
																			<%- ArrivalDate %>
																			<br><%- airports.airportName %>
																			<br>Terminal <%- arrivalTerminal %>
																		</span>
																	<% } %>
																<% }); %>
									            </div>
									         </div>
									         <div class="airline-info">
														<div class="inner-info">
															<% _.each(opAirlinesInfo, function(info, infoIdx) {
																if (info.aircraft == 'TRN') {
															%>
																<span class="airline-deta"
																	data-operationname="<%- info.name %>"
																	data-flightnumber="<%- info.code %>"
																	data-planename="TRAIN <%- info.flightnum %>">
																	<strong><%- info.name %></strong> • TRAIN <%- info.code %> <%- info.flightnum %><br>
																</span>

																<a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a>
															<%
																return;
																	}
																});
															%>
														</div>
													</div>
									      </div>

									      <%
													var railLayoversTotalTime = null;
													var railLayOversTime = 0;
													for (var railLayoversTimeIDx = 0, railLayoversLen = segments.legs.length; railLayoversTimeIDx < railLayoversLen; railLayoversTimeIDx++) {
														var railLayOvers = segments.legs[railLayoversTimeIDx];
														var layoverDuration = railLayOvers.layoverDuration;
														if (railLayOvers.aircraft.code == 'TRN' && layoverDuration) {
															railLayOversTime = layoverDuration;
														}
													}

													railLayoversTotalTime = formatFlightLayOvers(railLayOversTime);
												%>

												<% if (railLayOversTime) { %>
													<span class="layover-time--2 rail-layover" data-layovertime="<%- railLayoversTotalTime %>">
					                  <em class="ico-flight-history"></em>Layover time:  <%- railLayoversTotalTime %>
					                </span>
												<% } %>

									   	</div>
										</div>

									</div>
								<% } %>

							</div>
							<div class="right-flight">
								<div class="hidden-tb-dt flight-price economy-flight--green">
									<span class="cabin-flight">
										<span class="name-cabin"><%=labels.economy%></span>
										<span class="adult"><%=labels.oneAdult%></span>
										<span class="from">from</span>
									</span>
									<span class="price">888.<small>00</small></span>
								</div>
								<div class="button-group-1">
									<input type="button" name="change-button" id="change-button" value="Change" class="btn-8 button-change"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			<% } %>
		<% } %>
	<% }); %>
<% }); %>
