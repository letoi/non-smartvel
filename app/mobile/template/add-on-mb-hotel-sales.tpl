<div class="block-2 add-ons-item">
    <h5 class="sub-heading-3--dark">Travel insurance</h5>
    <div class="add-ons-item__content item-row">
      <div class="item-content-head item-content-head--top0 style">
        <div class="item-col-1 item-col-1--25"><figure><img src="images/aig-logo.png" alt="Travel insurance" longdesc="img-desc.html"></figure></div>
        <div class="item-col-2">
          <div class="content">
            <div class="desc" >
              <p>Let Travel Insurance by Travel Guard® take care of the unexpected so you can travel worry-free!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="add-ons-footer">
      <p>SGD 150.00 for all passengers</p><a href="#" class="link"><em class="ico-point-r"></em>Add</a>
    </div>
</div>
<% if(data.agodaBookingList !== void 0 && data.agodaBookingList.length > 0){ %>
    <% _.each(data.agodaBookingList, function(agodaBookingList, agodaBookingListIdx) { %>
        <div class="block-2 add-ons-item block-hotel">
            <h5 class="sub-heading-3--dark hotel-title">Hotel</h5>
            <div class="hotel-infor addon-hotel-sales">
                <div class="item-content-head">
                    <div class="item-col-1">
                        <figure>
                            <img src="<%- agodaBookingList.hotelImage %>" data-hotel alt="<%- agodaBookingList.hotelName %>" longdesc="">
                        </figure>
                    </div>
                    <div class="item-col-2">
                        <div class="content">
                            <h3 class="title-5--blue hotel-name"><%- agodaBookingList.hotelName %></h3>
                            <ul class="rating-block" aria-label="<%- agodaBookingList.starRating %> Star">
                                <% if (agodaBookingList.starRating === "0.5" || agodaBookingList.starRating === 0.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "1" || agodaBookingList.starRating === 1) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "1.5" || agodaBookingList.starRating === 1.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "2" || agodaBookingList.starRating === 2) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "2.5" || agodaBookingList.starRating === 2.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "3" || agodaBookingList.starRating === 3) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "3.5" || agodaBookingList.starRating === 3.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "4" || agodaBookingList.starRating === 4) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "4.5" || agodaBookingList.starRating === 4.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "5" || agodaBookingList.starRating === 5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } %>
                            </ul>
                            <div class="desc">
                                <p><%=agodaBookingList.addresLineOne%>, <%=agodaBookingList.city%>, <%=agodaBookingList.country%> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="add-ons-item__content item-row">
                <div class="item">
                    <ul class="info-details-3">
                        <li> <span>Check-in date:</span><p class="checkin-date"><%=agodaBookingList.checkInDate%></p></li>
                        <li><span>Check-out date:</span><p class="checkout-date"><%=agodaBookingList.checkOutDate%></p></li>
                        <li>
                            <span>Guests:</span>
                            <% if(parseInt(agodaBookingList.numberOfGuests) <= 1)  { %>
                                <%=agodaBookingList.numberOfGuests%> Adult
                            <% } else { %>
                                 <%=agodaBookingList.numberOfGuests%> Adults
                            <% } %>

                        </li>
                        <li><span>Room types:</span><%=agodaBookingList.noOfRooms%> <%=agodaBookingList.roomTypes%></li>
                        <li><span>No. of nights:</span><%=agodaBookingList.numberOfNights%></li>
                        <li><span>Breakfast:</span>Included for all rooms</li>
                    </ul>
                </div>
                <div class="item">
                    <div class="price-room">
                        <div class="total-title">Amount charged by Agoda</div>
                        <div class="total-cost">SGD 800.90</div>
                    </div>
                    <ul class="info-details-2">
                        <li><span>Agoda Reference ID:</span>12345678</li>
                        <li><span>Booking status:</span>Booking received</li>
                    </ul>
                </div>
            </div>
            <div class="add-on-cantact">
                <figure class="img-block"><img src="images/agoda-logo.png" alt="photo-car" longdesc="img-desc.html">
                </figure>
                <div class="add-on__content">
                    <div class="desc">
                        <p>To make changes to your booking, contact</p><a href="<%=agodaBookingList.selfServiceURL%>">Agoda Customer Support</a>
                        <p>Tel:&nbsp;<strong>+65 61234567</strong></p>
                    </div>
                </div>
            </div>
        </div>
    <% }) %>
<% } %>
<div class="block-2 add-ons-item">
    <h5 class="sub-heading-3--dark">Car rental</h5>
    <div class="add-ons-item__content item-row">
      <div class="item-content-head item-content-head--top0 style">
        <div class="item-col-1 item-col-1--25"><figure><img src="images/addon-rentalcars-logo.png" alt="Car rental" longdesc="img-desc.html"></figure></div>
        <div class="item-col-2">
          <div class="content">
            <div class="desc" >
              <p>Off on a road trip? Or just getting from A to B? A wide range of vehicles is available with our partner.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="add-ons-footer">
        <a href="#" class="link"><em class="ico-point-r"></em>Add</a>
    </div>
</div>
