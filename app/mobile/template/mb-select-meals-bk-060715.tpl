<% _.each(data.sectorMealListInformationVO, function(flight, flightIdx) { %>
  <div data-accordion="1" class="block-2 accordion accordion--2" data-select-meal="true">
    <div data-accordion-trigger="1" class="accordion__control active">
      <h3 class="sub-heading-3--dark accordion__heading-redemp"><%= (flightIdx + 1)%>. <%= flight.departureCityCode%> to <%= flight.arrivalCityCode%></h3><em class="ico-point-d"></em>
    </div>
    <div data-accordion-content="1" class="accordion__content">
      <% if (flight.mealService && flight.mealService.length === 1) { %>
        <% var trip = flight.mealService[0] %>
        <% var tripIdx = 0 %>
        <div class="select-meal-item" data-meal-portion="true">
          <h4 class="text-dark"><%= trip.type%></h4>
          <div class="form-group grid-row">
            <div class="grid-col one-half">
              <div class="grid-inner">
                <span class="item-label">Category</span>
                <div data-customselect="true" class="custom-select custom-select--2">
                  <label for="category-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= trip.mealList[0].mealCategoryName%></span><span class="ico-dropdown">Select</span>
                  <select id="category-<%= flightIdx%>-<%= tripIdx%>" name="category-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Category." data-select-category="true">
                    <% _.each(trip.mealList, function(category, categoryIdx) { %>
                      <%
                        var listMeal = _.map(category.mealServiceList, function(num, key) {return num.mealName}).toString();
                        var listCode = _.map(category.mealServiceList, function(num, key) {return num.mealCode}).toString();
                      %>
                      <option<%= categoryIdx==0 ? ' selected="selected" ' : ''%> value="<%= category.mealCategoryName%>" data-code="<%= category.mealCategoryCode%>" data-service-list-meal='<%= listMeal%>' data-service-list-code='<%= listCode%>' data-populate="<%= category.mealCategoryCode==='SPM' ? true : false%>"><%= category.mealCategoryName%></option>
                    <%});%>
                  </select>
                </div>
              </div>
            </div>
            <div class="grid-col one-half">
              <div class="grid-inner">
                <span class="item-label">Meal</span>
                <div data-customselect="true" class="custom-select custom-select--2 default">
                  <label for="meal-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= trip.mealList[0].mealServiceList[0].mealName%></span><span class="ico-dropdown">Select</span>
                  <select  id="meal-<%= flightIdx%>-<%= tripIdx%>" name="meal-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Meal." data-option-meal="true">
                    <% _.each(trip.mealList[0].mealServiceList, function(meal, mealIdx) { %>
                      <option <%= mealIdx==0 ? ' selected="selected" ' : ''%> value="<%= meal.mealName%>" data-code="<%= meal.mealCode%>"><%= meal.mealName%></option>
                    <%});%>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="alert-block checkin-alert" style="display: none;">
          <div class="inner">
            <div class="alert__icon"><em class="ico-alert"></em></div>
            <div class="alert__message"><span>By selecting a ((({0}))) meal, all your ((({1}))) meal selections from (((<%= flight.departureCityCode%>))) to (((<%= flight.arrivalCityCode%>))) have been changed to ((({0}))) meal selections.</span> <a href="#" data-undo="true">Undo</a></div>
          </div>
        </div>
      <% } else { %>
        <% _.each(flight.mealService, function(trip, tripIdx) { %>
          <div class="select-meal-item" data-meal-portion="true">
            <h4 class="text-dark"><%= trip.type%><span class="flight-schedule"><%= trip.origin%><em class="ico-plane"></em><%= trip.destination%></span></h4>
            <div class="form-group grid-row">
              <div class="grid-col one-half">
                <div class="grid-inner">
                  <span class="item-label">Category</span>
                  <div data-customselect="true" class="custom-select custom-select--2">
                    <label for="category-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= trip.mealList[0].mealCategoryName%></span><span class="ico-dropdown">Select</span>
                    <select id="category-<%= flightIdx%>-<%= tripIdx%>" name="category-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Category." data-select-category="true">
                      <% _.each(trip.mealList, function(category, categoryIdx) { %>
                        <%
                          var listMeal = _.map(category.mealServiceList, function(num, key) {return num.mealName}).toString();
                          var listCode = _.map(category.mealServiceList, function(num, key) {return num.mealCode}).toString();
                        %>
                        <option<%= categoryIdx==0 ? ' selected="selected" ' : ''%> value="<%= category.mealCategoryName%>" data-code="<%= category.mealCategoryCode%>" data-service-list-meal='<%= listMeal%>' data-service-list-code='<%= listCode%>' data-populate="<%= category.mealCategoryCode==='SPM' ? true : false%>"><%= category.mealCategoryName%></option>
                      <%});%>
                    </select>
                  </div>
                </div>
              </div>
              <div class="grid-col one-half">
                <div class="grid-inner">
                  <span class="item-label">Meal</span>
                  <div data-customselect="true" class="custom-select custom-select--2 default">
                    <label for="meal-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= trip.mealList[0].mealServiceList[0].mealName%></span><span class="ico-dropdown">Select</span>
                    <select  id="meal-<%= flightIdx%>-<%= tripIdx%>" name="meal-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Meal." data-option-meal="true">
                      <% _.each(trip.mealList[0].mealServiceList, function(meal, mealIdx) { %>
                        <option <%= mealIdx==0 ? ' selected="selected" ' : ''%> value="<%= meal.mealName%>" data-code="<%= meal.mealCode%>"><%= meal.mealName%></option>
                      <%});%>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="alert-block checkin-alert" style="display: none;">
            <div class="inner">
              <div class="alert__icon"><em class="ico-alert"></em></div>
              <div class="alert__message"><span>By selecting a ((({0}))) meal, all your ((({1}))) meal selections from (((<%= flight.departureCityCode%>))) to (((<%= flight.arrivalCityCode%>))) have been changed to ((({0}))) meal selections.</span> <a href="#" data-undo="true">Undo</a></div>
            </div>
          </div>
        <%});%>
      <% } %>
    </div>
  </div>
<%});%>
