<aside class="popup popup--gdpr-lightbox hidden">
  <div class="popup__inner">
    <div class="popup__content"><a href="#" data-close="true" class="popup__close hidden"></a>
        <h2 class="popup__heading"><span>Manage your privacy settings</span></h2>
        <p class="popup__heading-paragraph">Apart from technical cookies which are necessary for our website to function properly, you can select the cookies you wish to allow below. More information about the different types of cookies are available in our <span class="text"><a href="#">Cookie Policy</a></span>.</p>

        <div class="popup__gdpr__settings">
          <h4 class="popup__gdpr__settings__heading"><span>Technical</span></h4>
          <p class="popup__gdpr__settings__heading-paragraph">These cookies are necessary for our website to function at its most efficient, such as remembering your language and country preferences to enable us to present relevant content to you.</p>
        </div>

        <div class="popup__gdpr__settings">
          <h4 class="popup__gdpr__settings__heading"><span>Ease of use</span></h4>
          <div class="popup__gdpr__settings__desc">
            <p class="popup__gdpr__settings__heading-paragraph">Our website is made easier for you to use with these cookies as we are able to keep track of your KrisFlyer membership number when you've logged in, provide the most relevant departure airports for your location when you're booking a flight and many more.</p>
            <div class="popup__gdpr__settings__checkbox">
              <label class="ease-of-use-label">
                <input type="checkbox" name="gdpr-settings" class="popup__gdpr__checkbox" value="ALLOWED" id="ease_of_use" checked>
                <span></span>
              </label>
            </div>
          </div>
        </div>

        <div class="popup__gdpr__settings">
          <h4 class="popup__gdpr__settings__heading"><span>Tracking</span></h4>
          <div class="popup__gdpr__settings__desc">
            <p class="popup__gdpr__settings__heading-paragraph">We gather statistics on how you navigate and perform transactions on our website and use them for our analysis to continuously improve and optimise our services.</p>
            <div class="popup__gdpr__settings__checkbox">
              <label>
                <input type="checkbox" name="gdpr-settings" class="popup__gdpr__checkbox" value="ALLOWED" id="tracking" checked>
                <span></span>
              </label>
            </div>
          </div>
        </div>

        <div class="popup__gdpr__settings">
          <h4 class="popup__gdpr__settings__heading"><span>Marketing</span></h4>
          <div class="popup__gdpr__settings__desc">
            <p class="popup__gdpr__settings__heading-paragraph">While planning for your upcoming trip, these cookies allow us to present personalised marketing messages such as relevant and up-to-date offers which best suit your interest</p>
            <div class="popup__gdpr__settings__checkbox">
              <label>
                <input type="checkbox" name="gdpr-settings" class="popup__gdpr__checkbox" value="ALLOWED" id="marketing" checked>
                <span></span>
              </label>
            </div>
          </div>
        </div>
  
        <div class="popup__gdpr__save">
          <input type="button" value="SAVE SETTINGS" class="popup__gdpr__save__settings">
        </div>
    </div>
  </div>
</aside>
