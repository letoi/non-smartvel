<aside class="popup popup--intro-lightbox-comingsoon hidden" >
  <div class="popup__inner">
    <a href="#" class="popup__close" data-close="true"></a>
    <figure>
      <div class="img-wrapper"><img src="images/image-retina-comingsoon.jpg" alt="" longdesc="img-desc.html">
      </div>
      <div class="intro-block">
        <h2 class="popup__heading"><span>More improvements coming your way soon on the enhanced singaporeair.com</span></h2>
        <p>Making changes to your booking and upgrading flights with KrisFlyer miles will now be faster. This enhanced site also gives you an easier way to look for information and manage the planning of your upcoming trip.</p>
        <div class="button-group"><a href="#" class="btn-1" data-close="true">Continue</a>
        </div>
        <div class="custom-checkbox custom-checkbox--1" data-remember-cookie="true">
          <input name="remember-cb" id="remember-cb" type="checkbox">
          <label for="remember-cb">Don't show this to me again</label>
        </div>
      </div>
    </figure>
  </div>
</aside>
