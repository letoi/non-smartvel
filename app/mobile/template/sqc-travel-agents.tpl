<% if(data.length) { %>
<hr>
<br>
<br>
<h4 class="title-4--blue">Your travel agents</h4>
<br>
<table class="table-1 table-responsive" data-table-history="true">
	<thead>
		<tr>
		<th class="table-width-0" colspan="2">
			<a href="javascript:;" class="active travel-agents__sort-link js-sort-travel-agent">Travel agents
				<em class="ico-point-u js-icon-sort"></em>
			</a>
		</th>
		</tr>
	</thead>
	<tbody>
	<% _.each(data, function(agent, index) { %>
		<tr class="<%- index % 2 == 0 ? 'odd' : 'even' %>">
			<td>
				<div class="title-5--blue"><%- agent.agentName %></div>
				<div><span>IATA/ARC number:</span> <%- agent.agentRegNum %></div>
			</td>
			<td>
				<% if(agent.agentLinked == "true") { %>
					<div><a href="javascript:;" class="view-tickets js-remove-agent" data-agent-id="<%- agent.agentID %>" tabindex="0" ><span>Remove</span> </a></div>
					<div class="hidden-mb" >&nbsp;</div>
				<% } else { %>
					-
				<% } %>
			</td>
		</tr>
	<% }); %>
	</tbody>
</table>
<% }; %>
