<div class="blk-heading">
  <h3 class="sub-heading-1--dark"><%- data.depart %> to <%- data.destination %></h3>
</div>
<p class="text-note"><%- data.textNote %></p>
<!-- desktop -->
<table class="table-2 table-flight-status">
  <thead>
    <tr>
      <th class="table-width-0 hidden-mb"><%- data.heading.flight %></th>
      <th class="table-width-1 hidden-tablet"><%- data.heading.scheduled %></th>
      <th class="table-width-2 hidden-tablet"><%- data.heading.estimated %></th>
      <th class="table-width-3 hidden-tablet"><%- data.heading.actual %></th>
    </tr>
  </thead>
  <tbody>
    <% _.each(data.results, function(result, resultIdx) { %>
      <% _.each(result.flight, function(flight, idx){ %>
        <% _.each(flight.departs, function(depart, departIdx) { %>
          <tr>
            <td>
              <div class="table-2__info-top">
                <strong class="left">Flight <%- result.flightNumber %></strong>
                <strong class="right"><%- depart.status.arrived %></strong>
              </div>
              <div class="table-2__content">
                <span class="flight">Depart:&nbsp;</span>
                <%- depart.place %>
                <% if (depart.terminal && depart.terminal.trim() && depart.terminal !== 'Not available') {%>
                  <span class="dash">-</span>
                  <%- depart.terminal %>
                <% } %>
              </div>
            </td>

            <td>
              <div class="data-title"><%- data.heading.scheduled %></div>
              <div class="table-2__content">
                <% if (depart.scheduled && depart.scheduled.time && depart.scheduled.time.trim() && depart.scheduled.date && depart.scheduled.date.trim()) { %>
                  <%- depart.scheduled.time %>
                  -
                  <%- depart.scheduled.date %>
                <% } else { %>
                  <span class="dash">-</span>
                <% } %>
              </div>
            </td>

            <td>
              <div class="data-title"><%- data.heading.estimated %></div>
              <div class="table-2__content">
                <% if (depart.estimated && depart.estimated.time && depart.estimated.time.trim() && depart.estimated.date && depart.estimated.date.trim()) { %>
                  <%- depart.estimated.time %>
                  -
                  <%- depart.estimated.date %>
                <% } else { %>
                  <span class="dash">-</span>
                <% } %>
              </div>
            </td>

            <td>
              <div class="data-title"><%- data.heading.actual %></div>
              <div class="table-2__content">
                <% if (depart.actual && depart.actual.time && depart.actual.time.trim() && depart.actual.date && depart.actual.date.trim()) { %>
                  <%- depart.actual.time %>
                  -
                  <%- depart.actual.date %>
                <% } else { %>
                  <span class="dash">-</span>
                <% } %>
              </div>
            </td>
          </tr>

          <% if(result.stops && flight.departs.length > 1){%>
            <tr class="multi-flight-<%- departIdx + 1 %>">
          <% } else { %>
            <% if(!result.stops && flight.departs.length > 1){%>
              <tr class="no-layover multi-flight-<%- departIdx + 1 %>">
            <% } else { %>
              <tr class="one-flight">
            <% } %>
          <% } %>
              <% if (flight.arrives && flight.arrives.length && flight.arrives[departIdx]) { %>
                <td>
                  <div class="table-2__content"><span class="flight">Arrive:&nbsp;</span>
                    <%- flight.arrives[departIdx].place %>
                    <% if (flight.arrives[departIdx].terminal && flight.arrives[departIdx].terminal.trim() && flight.arrives[departIdx].terminal !== 'Not available') { %>
                      <span class="dash">-</span>
                      <%- flight.arrives[departIdx].terminal %>
                    <% } %>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.scheduled %></div>
                  <div class="table-2__content">
                    <% if(flight.arrives[departIdx].scheduled && flight.arrives[departIdx].scheduled.time && flight.arrives[departIdx].scheduled.time.trim() && flight.arrives[departIdx].scheduled.date && flight.arrives[departIdx].scheduled.date.trim()) { %>
                        <%- flight.arrives[departIdx].scheduled.time %>
                        -
                        <%- flight.arrives[departIdx].scheduled.date %>
                    <% } else {%>
                      <span class="dash">-</span>
                    <% } %>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.estimated %></div>
                  <div class="table-2__content">
                    <% if(flight.arrives[departIdx].estimated && flight.arrives[departIdx].estimated.time && flight.arrives[departIdx].estimated.time.trim() && flight.arrives[departIdx].estimated.date && flight.arrives[departIdx].estimated.date.trim()) { %>
                        <%- flight.arrives[departIdx].estimated.time %>
                        -
                        <%- flight.arrives[departIdx].estimated.date %>
                    <% } else {%>
                      <span class="dash">-</span>
                    <% } %>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.actual %></div>
                  <div class="table-2__content">
                    <% if(flight.arrives[departIdx].actual && flight.arrives[departIdx].actual.time && flight.arrives[departIdx].actual.time.trim() && flight.arrives[departIdx].actual.date && flight.arrives[departIdx].actual.date.trim()) { %>
                        <%- flight.arrives[departIdx].actual.time %>
                        -
                        <%- flight.arrives[departIdx].actual.date %>
                    <% } else {%>
                        <span class="dash">-</span>
                    <% } %>
                  </div>
                </td>
              <% } else { %>
                <td>
                  <div class="table-2__content">
                    <span class="flight">Arrive:&nbsp;</span>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.scheduled %></div>
                  <div class="table-2__content">
                    <span class="dash">-</span>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.estimated %></div>
                  <div class="table-2__content">
                    <span class="dash">-</span>
                  </div>
                </td>
                <td>
                  <div class="data-title"><%- data.heading.actual %></div>
                  <div class="table-2__content">
                    <span class="dash">-</span>
                  </div>
                </td>
              <% } %>
            </tr>

            <% if(result.stops && departIdx === 0){%>
              <tr class="table-2__layover">
                <td>
                  <div class="table-2__content"><%-result.stops + ' Stopover' + ((result.stops && parseInt(result.stops) > 1) ? 's' : '') %></div>
                </td>
                <td colspan="3"></td>
              </tr>
            <% } %>

        <% }); %>
      <% }); %>
    <% }); %>
  </tbody>
</table>
<!-- mobile -->

</div>
