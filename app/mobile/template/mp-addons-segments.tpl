<% if(data) {
  segmentsGroup = {};
  _.each(data.packPaxAssociations, function(pax1Data, pax1Idx) {
    _.each(pax1Data.segments, function(segments1Data, segments1Idx) {
      segmentsGroup[segments1Data.segmentId] = segments1Data;
    });
  });
  %>
  <h3 class="sub-title--blue">For your flight</h3>
  <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-simple-tab">
    <ul class="tab">
      <li class="tab-item active"><a href="#">Bundle<em class="ico-dropdown"></em></a>
        <div data-customselect="true" class="custom-select custom-select--2 hidden">
          <label for="multi-select-0" class="select__label"><span class="ui-helper-hidden-accessible">Select</span>
          </label><span class="select__text"></span><span class="ico-dropdown"></span>
          <select id="multi-select-0" name="multi-select-0">
            <option value="0" selected="selected">Bundle</option>
            <option value="1">A la carte</option>
          </select>
        </div>
      </li>
      <li class="tab-item"><a href="#">A la carte<em class="ico-dropdown"></em></a>
        <div data-customselect="true" class="custom-select custom-select--2 hidden">
          <label for="multi-select-1" class="select__label"><span class="ui-helper-hidden-accessible">Select</span>
          </label><span class="select__text"></span><span class="ico-dropdown"></span>
          <select id="multi-select-1" name="multi-select-1">
            <option value="1">A la carte</option>
          </select>
        </div>
      </li>
    </ul>
    <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">
      <label for="multi-select" class="select__label"><span class="ui-helper-hidden-accessible">Select</span>
      </label><span class="select__text"></span><span class="ico-dropdown"></span>
      <select id="multi-select" name="multi-select">
        <option value="0" selected="selected">Bundle</option>
        <option value="1">A la carte</option>
      </select>
    </div>
    <div class="tab-wrapper">
      <div class="tab-content active">
        <div data-accordion-wrapper="1">
          <div data-accordion-wrapper-content="1">
            <div data-accordion="1">
              <div class="addons-your-flight">
                <% if(data.bundle.length > 0) { %>
                  <div class="your-flight-item block-2">
                    <div class="addons-your-flight-content check-preferred">
                      <div data-accordion-wrapper="2">
                        <div class="description">
                          <figure>
                            <% if(data.bundle[0].isRecommended == "true") {
                              if(data.bundle[0].packImageUrl !== "") { %>
                                <img src="<%- data.bundle[0].packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                              <% } else { %>
                                <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                              <% }
                            } %>
                          </figure>
                          <div class="content">
                            <div class="detail-content">
                              <% if(data.bundle[0].isRecommended == "true") { %>
                                <span class="selected-orange">Recommended</span>
                              <% } %>
                              <h4 class="title-5--blue">
                                <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                                  Preferred Seat + Additional baggage
                                <% } %>
                              </h4>
                              <% if(data.bundle[0].packDescription !== "") { %>
                                <p><% data.bundle[0].packDescription %></p>
                              <% } %>
                            </div>
                            <div class="from-price-flight"><span class="note">From</span>
                              <span class="sgd-price">
                                <% arrBundleId = [];
                                  arrBundle = [];
                                  arrBundleSegmentPacks = [];
                                  _.each(data.bundle, function(bundleData, bundleIdx) {
                                    arrBundleId[bundleData.packId] = bundleData.packId;
                                    arrBundle[bundleData.packId] = {
                                      packId : bundleData.packId,
                                      ancillaries : bundleData.ancillaries
                                    }
                                  });
                                  _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                                    _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                      _.each(segmentsData.packs, function (packsData, packsIdx) {
                                        if (arrBundleId[packsData.packId]) {
                                          arrBundleSegmentPacks.push(packsData);
                                        }
                                      });
                                    });
                                  });
                                  arrBundleSegmentPacks.sort(function(a, b) {
                                    return parseFloat(a.price) - parseFloat(b.price);
                                  }); %>
                                  <%- arrBundleSegmentPacks[0].currency %> <%- Number(arrBundleSegmentPacks[0].price).toFixed(2) %>
                              </span>
                              <span class="miles">Per passenger per flight</span><span class="selected-orange">save up to <%- data.bundle[0].percentageSaved + "%" %></span>
                              <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                                <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="Select" class="btn-8" data-select-item="false"/>
                                <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Selected" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                                <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
                          <div class="block-flight-details">
                            <div class="title-popup-mb">
                              <span class="sub-heading-2--blue">
                                <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                                  Preferred Seat + Additional baggage
                                <% } %>
                              </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                            </div>
                            <% _.each(segmentsGroup, function(itemData, itemIdx) { %>
                              <div class="block-flight-details--inner">
                                <div class="addons-landing--inner">
                                  <div data-accordion="2" class="addons-landing-content ">
                                    <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                      <span class="title-5--blue"><%- itemData.segmentDescription %></span>
                                      <span class="bundle-selected hidden"><span></span></span><em class="ico-point-d"></em>
                                    </a>
                                    <div data-accordion-content="2" class="accordion__content">
                                      <% _.each(data.packPaxAssociations, function(paxData, paxIdx) { %>
                                      <div class="bundle-flight-block data-pss-xbag">
                                        <span class="title-5--blue"><%- paxData.paxName %></span>
                                        <div class="bundle-flight--inner">
                                          <% _.each(itemData.packs, function(packsData, packsIdx) {
                                            if (arrBundle[packsData.packId]) { %>
                                              <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                                </div>
                                                <div class="bundle-flight--inner">
                                              <% } %>
                                              <div class="bundle-flight-item">
                                                <div class="bundle-flight-item--inner"><span class="title-5--blue">Bundle <%- packsIdx + 1 %></span>
                                                  <ul class="bundle-baggage two-bundle-baggage">
                                                    <li><em class="ico-1-preferred"></em>Preferred Seat</li>
                                                    <li>
                                                      <% _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                                        if (ancillariesData.ancillaryType == "xBag") { %>
                                                          <em class="ico-business-1"></em>Additional baggage <%- ancillariesData.units.numberOfUnits%><%- ancillariesData.units.unitOfMeasurement.toLowerCase() %>
                                                        <% }
                                                      }); %>
                                                    </li>
                                                  </ul>
                                                  <div class="select-price"><span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span>
                                                    <input type="button" name="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false" data-total-fare/>
                                                    <input type="button" name="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false"/>
                                                  </div>
                                                </div>
                                              </div>
                                            <% }
                                          }); %>
                                        </div>
                                      </div>
                                      <% }); %>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <% }); %>
                            <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <% } %>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-content">
        <div data-accordion-wrapper="1">
          <div data-accordion-wrapper-content="1">
            <div data-accordion="1">
              <div class="addons-your-flight">
                <% if(data.alacarte.length > 0) { %>
                <% arrPSS = [];
                  arrPSSId = [];
                  arrxBAG = [];
                  arrxBAGId = [];
                  arrItemxBAG = [];
                  arrPiece = [];
                  arrPieceId = [];
                  _.each(data.alacarte, function(alacarteData, alacarteIdx) {
                    if(alacarteData.ancillaryType == 'PSS') {
                      arrPSS.push(alacarteData);
                      arrPSSId[alacarteData.packId] = alacarteData.packId;
                    } else if (alacarteData.ancillaryType == 'xBag') {
                      arrxBAGId[alacarteData.packId] = alacarteData.packId;
                      arrItemxBAG[alacarteData.packId] = {
                        packId : alacarteData.packId,
                        units : alacarteData.units
                      }
                      arrxBAG.push(alacarteData);
                    } else if (alacarteData.ancillaryType == 'HEAVY' || alacarteData.ancillaryType == 'BULK' || alacarteData.ancillaryType == 'PIECE') {
                      arrPiece.push(alacarteData);
                      arrPieceId[alacarteData.packId] = alacarteData.packId;
                    }
                  });
                  if(arrPSS.length > 0 ) { %>
                    <div class="your-flight-item block-2">
                      <div class="addons-your-flight-content check-preferred">
                        <div data-accordion-wrapper="2">
                          <div class="description">
                            <figure>
                              <% if(arrPSS[0].isRecommended == "true") {
                                if(arrPSS[0].packImageUrl !== "") { %>
                                  <img src="<%- arrPSS[0].packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                                <% } else { %>
                                  <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                                <% }
                              } %>
                            </figure>
                            <div class="content">
                              <div class="detail-content">
                                <% if(arrPSS[0].isRecommended == "true") { %>
                                  <span class="selected-orange">Recommended</span>
                                <% } %>
                                <h4 class="title-5--blue">Preferred Seat</h4>
                                <% if(arrPSS[0].packDescription !== "") { %>
                                  <p><% arrPSS[0].packDescription %></p>
                                <% } %>
                              </div>
                              <div class="from-price-flight"><span class="note">From</span>
                                <span class="sgd-price">
                                  <% arrSegmentPacks = [];
                                  _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                                    _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                      _.each(segmentsData.packs, function (packsData, packsIdx) {
                                        if (arrPSSId[packsData.packId]) {
                                          arrSegmentPacks.push(packsData);
                                        }
                                      });
                                    });
                                  });
                                  arrSegmentPacks.sort(function(a, b) {
                                    return parseFloat(a.price) - parseFloat(b.price);
                                  }); %>
                                  <%- arrSegmentPacks[0].currency %> <%- Number(arrSegmentPacks[0].price).toFixed(2) %>
                                </span>
                                <span class="miles">Per passenger per flight</span>
                                <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                                  <input type="button" name="select-seat-btn" id="select-seat-btn" value="Select" class="btn-8" data-select-item="false"/>
                                  <input type="button" name="selected-seat-btn" id="selected-seat-btn" value="Selected" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                                  <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true">
                            <div class="block-flight-details">
                              <div class="title-popup-mb"><span class="sub-heading-2--blue">Preferred Seat</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                              </div>
                              <% _.each(segmentsGroup, function(itemData, itemIdx) { %>

                                <div class="block-flight-details--inner">
                                  <div class="addons-landing--inner">
                                    <div data-accordion="2" class="addons-landing-content"><a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                      <span class="title-5--blue"><%- itemData.segmentDescription %></span>
                                      <span class="bundle-selected hidden preferreds-seat"><span></span>Preferreds Seat selected</span><em class="ico-point-d"></em></a>
                                      <div data-accordion-content="2" class="accordion__content">
                                        <div class="preferred-flight-block">
                                          <% _.each(data.packPaxAssociations, function(paxData, paxIdx) { %>
                                            <div class="preferred-flight-item pref-enjoy-item">
                                              <div class="title-item"><span class="title-5--blue"><%- paxData.paxName %></span></div><span class="text-item">Preferred Seat</span>
                                              <div class="pref-enjoy-choose">
                                                <div class="content-right">
                                                  <div class="select-price">
                                                    <% _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                                      if (segmentsData.segmentId == itemData.segmentId) {
                                                        _.each(segmentsData.packs, function(packsData, packsIdx) {
                                                           if (arrPSSId[packsData.packId]) {
                                                            if(packsData.price == '0') {%>
                                                              <span class="selected-orange">Complimentary</span>
                                                            <% } %>
                                                            <span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span>
                                                          <% }
                                                        })
                                                      }
                                                    }); %>
                                                  </div>
                                                  <div class="select-price-button">
                                                    <input type="button" name="select-baggage-<%- itemIdx %>-<%- paxIdx + 1 %>" id="select-baggage-<%- itemIdx %>-<%- paxIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false" data-total-fare />
                                                    <input type="button" name="selected-baggage-<%- itemIdx %>-<%- paxIdx + 1 %>" id="selected-baggage-<%- itemIdx %>-<%- paxIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false"/>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          <% }); %>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <% }); %>
                              <input type="button" name="confirm-pss-btn" id="confirm-pss-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <% }
                  if(arrxBAG.length > 0 ) { %>
                    <div class="your-flight-item block-2">
                      <div class="addons-your-flight-content">
                        <div data-accordion-wrapper="2">
                          <div class="description">
                            <figure><img src="images/baggage-add-ons.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                            </figure>
                            <div class="content">
                              <div class="detail-content">
                                <h4 class="title-5--blue">Additional baggage</h4>
                                <% if(arrxBAG[0].packDescription !== "") { %>
                                  <p><% arrxBAG[0].packDescription %></p>
                                <% } %>
                              </div>
                              <div class="from-price-flight"><span class="note">From</span>
                                <span class="sgd-price">
                                  <% arrSegmentBAG = [];
                                  _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                                    _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                      _.each(segmentsData.packs, function (packsData, packsIdx) {
                                        if (arrxBAGId[packsData.packId]) {
                                          arrSegmentBAG.push(packsData);
                                        }
                                      });
                                    });
                                  });
                                  arrSegmentBAG.sort(function(a, b) {
                                    return parseFloat(a.price) - parseFloat(b.price);
                                  }); %>
                                  <%- arrSegmentBAG[0].currency %> <%- Number(arrSegmentBAG[0].price).toFixed(2) %>
                                </span>
                                <span class="miles">Per passenger per flight</span>
                                <div data-accordion-trigger="1" aria-expanded="false"  data-trigger-mobile-popup class="button-group-3">
                                  <input type="button" name="select-baggage-piece-btn" id="select-baggage-piece-btn" value="Select" class="btn-8" data-select-item="false"/>
                                  <input type="button" name="selected-baggage-piece-btn" id="selected-baggage-piece-btn" value="Selected" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                                  <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true">
                            <div class="block-flight-details">
                              <div class="title-popup-mb"><span class="sub-heading-2--blue">Additional baggage</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                              </div>
                              <% _.each(segmentsGroup, function(itemData, itemIdx) { %>
                                <div class="block-flight-details--inner">
                                  <div class="addons-landing--inner">
                                    <div data-accordion="2" class="addons-landing-content"><a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                      <span class="title-5--blue"><%- itemData.segmentDescription %></span>
                                      <span class="bundle-selected hidden additional-baggages"><span></span>Additional baggages selected</span><em class="ico-point-d"></em></a>
                                      <div data-accordion-content="2" class="accordion__content">
                                        <% _.each(data.packPaxAssociations, function(paxData, paxIdx) { %>
                                          <div class="preferred-flight-block">
                                            <div class="preferred-flight-item weight-flight-item">
                                              <div class="title-baggage-item"><span class="title-5--blue"><%- paxData.paxName %></span>
                                                <% _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                                  if (segmentsData.segmentId == itemData.segmentId) {
                                                    _.each(segmentsData.packs, function(packsData, packsIdx) {
                                                      if (arrItemxBAG[packsData.packId]) { %>
                                                        <span class="free-bagage">Your free baggage: <strong><%- arrItemxBAG[packsData.packId].units.numberOfUnits %><%- arrItemxBAG[packsData.packId].units.unitOfMeasurement %></strong></span>
                                                      <% }
                                                    });
                                                  }
                                                }); %>
                                                <!-- <ul class="list-baggage-member">
                                                  <li><strong>30 kg</strong></li>
                                                  <li><strong>Additional 20 kg</strong>as a KrisFlyer Elite Gold member*</li>
                                                </ul> -->
                                              </div>
                                              <div class="weight-baggage">
                                                <div class="wrap-baggage"><span class="text-item"><em class="ico-business-1"></em><strong>Add weight</strong></span>
                                                  <div class="full-flight">
                                                    <div data-customselect="true" class="custom-select custom-select--2">
                                                      <label for="add-weight-<%- itemIdx %>-<%- paxIdx + 1 %>" class="select__label"><span class="ui-helper-hidden-accessible">Label</span>
                                                      </label><span class="select__text">5kg ($15.00)</span><span class="ico-dropdown">5kg ($15.00)</span>
                                                      <select id="add-weight-<%- itemIdx %>-<%- paxIdx + 1 %>" name="add-weight-<%- itemIdx %>-<%- paxIdx + 1 %>" data-dropdpown-selected="true">
                                                        <option value="1" selected="selected">5kg ($15.00)</option>
                                                        <option value="2">10kg ($30.00)</option>
                                                        <option value="3">15kg ($45.00)</option>
                                                        <option value="4">20kg ($60.00)</option>
                                                        <option value="5">20kg ($75.00)</option>
                                                        <option value="6">25kg ($90.00)</option>
                                                        <option value="7">30kg ($105.00)</option>
                                                      </select>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        <% }); %>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <% }); %>
                              <input type="button" name="confirm-baggage-btn" id="confirm-baggage-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <% }
                  if(arrPiece.length > 0 ) { %>
                    <div class="your-flight-item block-2">
                      <div class="addons-your-flight-content">
                        <div data-accordion-wrapper="2">
                          <div class="description">
                            <figure><img src="images/baggage-add-ons.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                            </figure>
                            <div class="content">
                              <div class="detail-content">
                                <h4 class="title-5--blue">Additional baggage</h4>
                                <% if(arrPiece[0].packDescription !== "") { %>
                                  <p><% arrPiece[0].packDescription %></p>
                                <% } %>
                              </div>
                              <div class="from-price-flight"><span class="note">From</span>
                                <span class="sgd-price">
                                  <% arrSegmentPiece = [];
                                  _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                                    _.each(paxData.segments, function(segmentsData, segmentsIdx) {
                                      _.each(segmentsData.packs, function (packsData, packsIdx) {
                                        if (arrPieceId[packsData.packId]) {
                                          arrSegmentPiece.push(packsData);
                                        }
                                      });
                                    });
                                  });
                                  arrSegmentPiece.sort(function(a, b) {
                                    return parseFloat(a.price) - parseFloat(b.price);
                                  }); %>
                                  <%- arrSegmentPiece[0].currency %> <%- Number(arrSegmentPiece[0].price).toFixed(2) %>
                                </span>
                                <span class="miles">Per passenger per flight</span>
                                <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                                  <input type="button" name="select-add-heavy-btn" id="select-add-heavy-btn" value="Select" class="btn-8" data-select-item="false"/>
                                  <input type="button" name="selected-add-heavy-btn" id="selected-add-heavy-btn" value="Selected" class="hidden btn-1" data-select-item="false"/>
                                  <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true">
                            <div class="block-flight-details">
                              <div class="title-popup-mb"><span class="sub-heading-2--blue">Additional baggage</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                              </div>
                              <% _.each(data.packPaxAssociations, function(paxData, paxIdx) {
                                _.each(paxData.segments, function(segmentsData, segmentsIdx) { %>
                                  <div class="block-flight-details--inner">
                                    <div class="addons-landing--inner">
                                      <div data-accordion="2" class="addons-landing-content"><a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                        <span class="title-5--blue"><%- segmentsData.segmentDescription %></span>
                                        <span class="bundle-selected hidden additional-baggages"><span></span>Additional baggages selected</span><em class="ico-point-d"></em></a>
                                        <div data-accordion-content="2" class="accordion__content">
                                          <% _.each(segmentsData.packs, function(packsData, packsIdx) {
                                            if (arrPieceId[packsData.packId]) { %>
                                              <div class="preferred-flight-block">
                                                <div class="preferred-flight-item weight-flight-item">
                                                  <div class="title-baggage-item"><span class="title-5--blue"><%- paxData.paxName %></span><span class="free-bagage">Your free baggage:<strong>30kg</strong></span></div>
                                                  <div class="weight-baggage">
                                                    <div class="wrap-baggage">
                                                      <div class="select-price"><span class="from-text">from</span><span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span></div>
                                                      <div class="select-price-button">
                                                        <input type="button" name="select-price-btn-1" id="select-price-btn-1" value="Select" class="btn-8" data-selected-button="false" data-total-fare />
                                                        <input type="button" name="selected-price-btn-1" id="selected-price-btn-1" value="Selected" class="hidden btn-1" data-selected-button="false"/>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="preferred-flight--info hidden">
                                                  <div class="review-baggage--item">
                                                    <span class="review-baggage-text" data-unit="80"><em class="ico-business-1"></em><span><strong>Add 9kg to free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 80.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Each bag cannot weigh more than 32kg.&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span></span>
                                                    <ul data-plus-or-minus-number="4" class="add-baggage-list">
                                                      <li>
                                                        <button type="button" class="btn-minus">-</button>
                                                      </li>
                                                      <li>
                                                        <input type="tel" name="number-baggage" value="2" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-1" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                      </li>
                                                      <li>
                                                        <button type="button" class="btn-plus">+</button>
                                                      </li>
                                                    </ul>
                                                    <span class="sgd-price">SGD <span>160.00</span></span>
                                                  </div>
                                                  <div class="review-baggage--item">
                                                    <span class="review-baggage-text" data-unit="60"><em class="ico-business-1"></em><span><strong>Oversized free bag <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 60.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small"  data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                                  </span>
                                                    <ul data-plus-or-minus-number="4" class="add-baggage-list">
                                                      <li>
                                                        <button type="button" class="btn-minus">-</button>
                                                      </li>
                                                      <li>
                                                        <input type="tel" name="number-baggage" value="" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-2" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                      </li>
                                                      <li>
                                                        <button type="button" class="btn-plus">+</button>
                                                      </li>
                                                    </ul>
                                                    <span class="sgd-price" style="display:none;">SGD <span>0.00</span></span>
                                                  </div>
                                                  <div class="review-baggage--item">
                                                    <span class="review-baggage-text" data-unit="80"><em class="ico-business-1"></em><span><strong>Add extra bag, 32kg <em tabindex="0" class="ico-info-round-fill hidden-tb-dt" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></strong> (SGD 80.00 each) <em tabindex="0" class="ico-info-round-fill hidden-mb-small" data-tooltip="true" data-type="2" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Length + Width + Height must not exceed 158cm (62 inches).&lt;/p&gt;"><span class="ui-helper-hidden-accessible">View more information</span></em></span>
                                                    </span>
                                                    <ul data-plus-or-minus-number="2" class="add-baggage-list">
                                                      <li>
                                                        <button type="button" class="btn-minus">-</button>
                                                      </li>
                                                      <li>
                                                        <input type="tel" name="number-baggage" value="" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-3" data-rule-digits="true" maxlength="1" class="number-baggage">
                                                      </li>
                                                      <li>
                                                        <button type="button" class="btn-plus">+</button>
                                                      </li>
                                                    </ul>
                                                    <span class="sgd-price" style="display:none;">SGD <span>0.00</span></span>
                                                  </div>
                                                </div>
                                              </div>
                                            <% }
                                          }); %>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <% });
                              }); %>
                              <input type="button" name="confirm-baggage-piece-btn" id="confirm-baggage-piece-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <% }
                } %>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<% } %>
