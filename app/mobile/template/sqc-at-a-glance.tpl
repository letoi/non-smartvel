<% _.each(data, function(flight, flightIdx) { %>
  <% if(flightIdx < 3) { %>
    <% if(flight.delay) { %>
      <div class="alert-block" data-upcoming-flight-alert="<%= flight.referenceNumber %>">
        <div class="inner">
          <div class="alert__icon"><em class="ico-alert"></em></div>
          <div class="alert__message"><%= flight.delay %></div>
        </div>
      </div>
    <% } %>
    <div data-accordion="1" class="block-2 accordion accordion--1 booking--style-2" data-flight data-url="<%= flight.url %>">
      <div class="accordion__control-inner">
        <a href="#" data-accordion-trigger="1" class="accordion__control accordion__control--1">
          <h4 class="sub-heading-2--blue">Booking Reference <%= flight.referenceNumber %></h4>
          <div class="sub-text-1">
            <%= flight.from.name %> to <%= flight.to.name %>
            <span class="hidden-mb">&nbsp;-&nbsp;</span>
            <span><%= flight.departureDate %></span>
          </div>
          <em class="ico-point-d hidden"></em>
        </a>
        <span class="loading loading--small">Loading...</span>
        <div class="booking-form hidden">
          <a href="#" class="btn-1">
            <!-- Manage check-in -->
          </a>
        </div>
      </div>
      <div data-accordion-content="1" class="accordion__content" style="display: none;">
        <div class="booking-info-group">
          <!--Details-->
        </div>
      </div>
      <div class="accordion__content-bottom"><strong class="sub-title">Passengers:</strong>
        <ul class="inline-list-style" data-passengers="<%= flight.passengers %>"><% _.each(flight.passengers, function(pass, passIdx) { %><% if(passIdx < 3) { %><li <%= passIdx === 2 ? 'class="last"' : '' %>><%=pass%><% if(passIdx < 2) { print(','); } %></li><% } %><% }); %></ul>
        <% if(flight.passengers && flight.passengers.length > 3) { %>
          <a href="javascript:void(0);" class="link-4" data-toggle-passengers-list="true">
            <span class="more"><em class="ico-point-r"></em>See more</span>
            <span class="less"><em class="ico-point-r"></em>See less</span>
          </a>
        <% } %>
      </div>
    </div>
  <% } %>
<% }); %>
