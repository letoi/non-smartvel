<% if(data) { %>
  <%
    var numberPassengers = ''
    numberPassengers = data.adultCount;
    if(data.adultCount) {
      numberPassengers += data.adultCount > 1 ? ' Adults' : ' Adult';
    } 
    if(data.childCount) {
      numberPassengers += ' - ' + data.childCount 
      numberPassengers += data.childCount > 1 ? ' Children' : ' Child' 
    } 
    if(data.infantCount) {
      numberPassengers = ' - ' + data.infantCount
      numberPassengers = data.infantCount > 1 ? ' Infants' : ' Infant'
    }
  %>
  <div class="booking-summary__heading" id="booking-summary__heading" aria-describedby="aria-bsh">
    <a href="#" class="booking-summary__control focus-outline" aria-describedby="aria-bsh" data-tabindex="true" role="button" aria-expanded="true" aria-controls="booking-summary__content" tabindex="0">
      Booking summary
      <em class="ico-point-d"></em>
    </a>
    <div class="booking-summary__info" tabindex="0"><span class="says">Booking summary, Selected Fare:</span>
      <p class="number-passengers">
        <%= numberPassengers %>
      </p>
      <span class="refund-label">total to be refunded</span>
      <p data-headtotal="true" class="total-cost"><span class="unit"><%= data.currency %> <%= data.totalToBeRefunded.toLocaleString('en', { minimumFractionDigits: 2 }) %></span></p>
      <p class="fare-notice"></p>
    </div>
    <div id="booking-summary__info-copy" aria-live="polite" role="status" tabindex="10" class="says"></div>
    <span class="loading loading--medium-2 hidden" aria-hidden="true">Loading...</span>
  <span id="aria-bsh" class="says">You are now at the Booking Summary Panel. The total amount of your fare is: <%= data.currency %> <%= data.totalToBeRefunded %> and  for <%= numberPassengers %>. The total fare includes taxes and surcharges. </span></div>
  <div class="booking-summary__content" id="booking-summary__content">
    <div class="booking-group">
      <div class="booking-heading">
        <h3>REFUND BREAKDOWN</h3><a href="#" class="link-4" data-trigger-popup=".popup--flights-details" data-popup-anchor="cost" data-is-disabled-stop-im="true"><em class="ico-point-r">&nbsp;</em><span class="text">More details</span></a>
      </div>
      <div tabindex="0" class="booking-group__content">
        <div class="flights-cost">
          <ul class="flights-cost__details">
            <% _.each(data.sshPackageDetails, function(packageDetail){ %>
              <li>
              <span><%= packageDetail.sshPkgName%></span>
              <span class="price">
                <span class="unit"><%= data.currency %> </span>
                <%= packageDetail.sshPrice.toLocaleString('en', { minimumFractionDigits: 2 }) %>
              </span>
              </li> 
            <% }) %>
          </ul>
        <div class="flights-cost">
          <ul class="flights-cost__details">
            <li class="grand-price"><span>Grand total: </span><span data-grandtotal="true" class="price"><span class="unit"><%= data.currency %> </span><%= data.totalToBeRefunded.toLocaleString('en', { minimumFractionDigits: 2 }) %></span></li>
          </ul>
        </div>
        <div class="grand-total">
          <p data-aria-text="Total to be refunded" class="total-title">TOTAL TO BE REFUNDED</p>
          <p data-tobepaid="true" class="total-info"><span class="unit"><%= data.currency %> </span><%= data.totalToBeRefunded.toLocaleString('en', { minimumFractionDigits: 2 }) %></p>
          <p class="info-charge">Credit card ending in - <%= data.refundedToCard %></p>
        </div>
      </div>
    </div>
  </div>
<% } %>