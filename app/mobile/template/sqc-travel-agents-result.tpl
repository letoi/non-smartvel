<% if(data.length) { %>
<div class="agent-result__none js-no-result">
	<b><i> No results found</i></b>
</div>
<div class="agent-result--wrapper js-has-result">
	<h4 class="title-4--blue">Your search results</h4>
	<div class="agents-result__content js-agent-data" data-max-items="5">
		<% _.each(data, function(agent, index) { %>
		<div class="agent-result__content-info js-agent-item" id="<%- agent.agentID %>">
			<span class="agent-result__content-info__badge js-agent-badge hidden">Added</span>
			<div class="title-5--blue"><%- agent.agentName %></div>
			<div><span>IATA/ARC number:</span> <%- agent.agentRegNum %></div>
			<input type="button" name="Add" value="Add" class="btn js-add-agent agent-result__content-info__button">
		</div>
		<% }); %>
	</div>
	<div class="agent-result__load-more--wrapper">
		<input type="button" class="agent-result__load-more__btn js-agents-loadmore" name="agent-load-more-btn" value="Load More">
	</div>
</div>
<form class="js-form-add-agents" name="formAgentsSelected">
	<input type="hidden" id="agentsSelected" name="agentsSelected">
	<div class="form-group">
		<!-- done button for modal dismiss-->
		<input type="submit" class="btn-1 right agent-result__btn-done" value="Done">
	</div>
</form>
<% }; %>
