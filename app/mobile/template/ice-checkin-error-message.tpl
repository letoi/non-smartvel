<aside class="popup popup-6 popup--checkin-error-message hidden">
    <div class="popup__inner" tabindex="-1">
      <div class="popup__content focus-outline" role="document" tabindex="0">
        <p class="popup__text-intro--2">You must cancel check-in for your latest flights on this itinerary before you can cancel this one.</p>
        <div class="button-group-1"><a href="#" class="btn-1 btn-back-booking">
            <text>Back</text></a>
        </div>
      <a href="#" class="popup__close" aria-label="Close button"></a></div>
    </div>
</aside>