<%

 var toDate = function(epoch, format, locale) {
      var date = new Date(epoch),
      format = format || 'dd/mmm/YY',
      locale = locale || 'en'
      dow = {};

      dow.en = ['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];

      var monthNames = ["Jan","Feb", "Mar", "Apr", "May", "June ","July", "Aug", "Sept", "Oct", "Nov", "Dec"];

      var formatted = format
      .replace('D', dow[locale][date.getDay()])
      .replace('d', ("0" + date.getDate()).slice(-2))
      .replace('mmm', ((monthNames[date.getMonth()])))
      .replace('yyyy', date.getFullYear())
      .replace('yy', (''+date.getFullYear()).slice(-2))
      .replace('hh',  ("0" + date.getHours()).slice(-2))
      .replace('mn', date.getMinutes())

      return formatted
   }

%>
<aside class="popup popup--checkin-cancel-confirm animated" role="dialog" tabindex="-1">
        <div class="popup__inner">
          <div class="popup__content focus-outline" role="document" tabindex="0">
            <h2 class="popup__heading" aria-hidden="false">Check-in successfully cancelled</h2>
            <p class="popup__text-intro">You have successfully cancelled check-in for the passengers on the following flights:</p>
            <form action="#" method="post" class="form--checkin-cancel-confirm">
              <fieldset>
               <%
                    var flight_count = 1;
                    _.each(paxRecordVO.flights, function(flight) {
                    var checkedInLeg = [];
                %>
                          <h3 class="sub-heading-3--dark"><%- flight_count %>. <%= flight.origin.cityName %> (<%= flight.origin.airportCode %>) to <%= flight.destination.cityName %> (<%= flight.destination.airportCode %>)</h3>
                          <h4 class="popup__text-intro--2"><%= toDate(flight.scheduledDepartureDateTime, "d mmm (D)") %> – <%= toDate(flight.scheduledDepartureDateTime, 'hh:mn') %> – <%= flight.operatingAirline.airlineCode %> <%= flight.operatingAirline.flightNumber %></h4>
                          <div class="table-default">
                            <div class="table-row table-row--heading">
                              <div class="table-col table-col__full success-header">Passenger(s)</div>
                            </div>
                            <div class="table-row">
                                <%
                                  // set pax id for the loop
                                  var i = 1;
                                  var pi = 0;
                                  // loop through all flights
                                  _.each(allPax, function(pax) {
                                    // loop through services of each pax
                                    _.each(pax.services, function(service){
                                      _.each(flight.flightIDs, function(flID){
                                        if(service.flightID == flID && service.dcsStatus.checkedIn == true){
                                          // assign new obj for checkedin pax
                                 %>
                                    <div class="table-col table-col-1 success-text"><span><%- i++ %>.&nbsp;</span>
                                            <span><%- pax.paxDetails.firstName %> <%- pax.paxDetails.lastName %> 
                                              <% if(pax.paxDetails.passengerType == "INF"){ %>
                                                  <span class="default">&nbsp;(INFANT)</span>
                                              <% } %> 
                                            </span>
                                    </div>
                                  <% 
                                        }
                                      });
                                  });
                                });
                              %>    
                            </div>
                          </div>

                       <% flight_count++ }); %>     
                  <div class="button-group-1"><a href="#" class="btn-1 btn-back-booking">
                      <text>Back</text></a>
                  </div>
              </fieldset>
            </form>
          <a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span></a></div>
        </div>
</aside>