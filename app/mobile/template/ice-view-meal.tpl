<aside class="popup popup--view-meal hidden"><p id="popup--help" class="says">Beginning of dialog window. Press escape to cancel and close this window.</p>
  <div class="popup__inner" tabindex="-1">
    <div class="popup__content focus-outline" aria-hidden="false" tabindex="0" aria-describedby="popup--help">
      <h2 class="popup__heading" aria-hidden="false">Selected meals</h2>
      <h3 class="sub-heading-3--dark">1. Copenhagen to Singapore</h3>
      <div class="table--3">
        <div class="table-row">
          <div class="table-inner">
            <p class="first">CPH TO FRA</p>
            <p>Breakfast: Book The Cook - Roasted Japanese Pumpkin and Oriental Mushroom Salad</p>
          </div>
        </div>
        <div class="table-row">
          <div class="table-inner">
            <p class="first">FRA TO SIN</p>
            <p>Lunch: Book The Cook - Roasted Japanese Pumpkin and Oriental Salad</p>
          </div>
        </div>
      </div>
    <a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span></a></div>
  </div>
</aside>