<% if(data && data.length) { %>
	<% _.each(data, function(day, dayIdx){ %>
	<% var price = parseFloat(day.price).toLocaleString() %>
		<a href="#" data-date="<%= day.month %>" class="slide-date">
			<div class="slide-item">
			  <div class="content"><span class="time"><%- $.datepicker.formatDate('D dd M', new Date(day.month.replace(new RegExp('-', 'g'),'\/'))) %></span><span class="from">from</span>
			    <div class="price"><span class="large-price"><%- price %></span>.00</div>
			  </div>
			</div>
			<span class="text"></span>
		</a>
	<% }) %>
<% } %>