<div class="table-default">
	<div class="table-row table-row--heading">
		<div class="table-col table-col-1"><%- data.description %></div>
		<div class="table-col table-col-2 table-col-2__row">
			<p class="table-col-2__left"><%- data.baggageType %></p>
			<span class="table-col-2__right hidden-mb"><%- data.currencyCode %></span>
		</div>
	</div>
	<% _.each(data.segmentDetails, function(segmentItem){ %>
		<div class="table-row">
			<% _.each(segmentItem.infoFlight, function(infoFlightItem, idx){ %>
				<% if (segmentItem.infoFlight.length > 1) { %>
					<% if ((segmentItem.infoFlight.length - 1)!== idx) { %>
						<div class="prefer_staus prefer_staus-1"><span><%- infoFlightItem.from %></span><em class="ico-1-plane"></em><span><%- infoFlightItem.to %></span></div>
					<% } else { %>
						<div class="prefer_staus"><span><%- infoFlightItem.from %></span><em class="ico-1-plane"></em><span><%- infoFlightItem.to %></span></div>
					<% } %>
				<% } else { %>
					<div class="prefer_staus"><span><%- infoFlightItem.from %></span><em class="ico-1-plane"></em><span><%- infoFlightItem.to %></span></div>
				<% } %>
			<% }) %>
		</div>
		<div class="table-content">
			<% _.each(segmentItem.baggage, function(baggageItem){ %>
				<div class="table-row">
					<div class="table-col table-col-1"><p class="name-customer"><%- baggageItem.baggageDetails.paxName %></p></div>
					<div class="table-col table-col-2">
						<% _.each(baggageItem.baggageDetails.infoBaggage, function(infoBaggageItem){ %>
							<div class="table-col-2__row">
								<p class="table-col-2__left"><%- infoBaggageItem.baggageType %></p>
								<span class="table-col-2__right"><strong class="hidden-tb-dt"><%- data.currencyCode %></strong><span class="price"><%- infoBaggageItem.price %></span></span>
							</div>
						<% }) %>
					</div>
				</div>
			<% }) %>
		</div>
	<% }) %>
	<div class="table-row">
		<div class="prefer-result"><span class="sub">total</span><span><%- data.total %></span></div>
	</div>
</div>
