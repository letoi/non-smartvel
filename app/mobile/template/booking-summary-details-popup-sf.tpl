 <% if(!confirmationPage) { %>
  <h2 data-anchor="flights" class="popup__heading">Secure fare</h2>
  <p class="popup__text-intro">Need more time to think? Secure your fare for up to 48 hours for just <strong>SGD [price].</strong> During this time, your fare will be locked in.</p>
  <h2 class="popup__heading">Flight info</h2>
  <div class="flights-target booking--style-3__main">
    <% _.each(data.bookingSummary.flight, function(flight, flightIdx) { %>
      <div class="heading-wrap">
        <h3 class="sub-heading-3--dark">
          <%= (flightIdx + 1) %>. <%= flight.origin %> to <%= flight.destination %>
        </h3>
      </div>
      <div class="booking-info-group booking-info-group-sf">
        <% _.each(flight.flightSegments, function(segment, segmentIdx) { %>
          <% if(segmentIdx == 0) { %>
            <div class="flights__info--group">
              <div class="flights--detail" data-flight-number="<%= segment.flightNumber %>"
                data-carrier-code="<%= segment.carrierCode %>"
                data-date="<%= segment.deparure.date %>"
                data-origin="<%= segment.deparure.airportCode %>">
                <span>Flight <%= segment.carrierCode %> <%= segment.flightNumber %>
                  <em class="ico-point-d"></em>
                  <span class="loading loading--small hidden"></span>
                </span>
                <div class="details hidden">
                  <% if(segment.airCraftType) { %>
                    <p>Aircraft type: <%= segment.airCraftType %></p>
                  <% } %>
                </div>
              </div>
              <span class="flights-type"><%= segment.cabinClassDesc %></span>
            </div>
          <% } %>
          <div class="booking-info booking-info-sf no-border">
            <div class="booking-info-item one-half">
              <div class="booking-desc">
                <span class="hour">
                  <%= segment.deparure.airportCode %> <%= segment.deparure.time %>
                </span>
                <span class="country-name">
                  <%= segment.deparure.cityName %>
                </span>
                <div class="booking-content">
                  <span>
                    <%= segment.deparure.date %>,<br> <%= segment.deparure.airportName %>
                  </span>
                  <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                </div>
              </div>
            </div>
            <div class="booking-info-item one-half">
              <div class="booking-desc">
                <span class="hour">
                  <%= segment.arrival.airportCode %> <%= segment.arrival.time %>
                </span>
                <span class="country-name">
                  <%= segment.arrival.cityName %>
                </span>
                <div class="booking-content">
                  <span>
                    <%= segment.arrival.date %>,<br> <%= segment.arrival.airportName %>
                  </span>
                </div>
              </div>
            </div>
            <p class="operated"><%= L10n.oparated %></p>
          </div>
          <% if(segment.layoverTime) { %>
            <div class="booking-info booking-info-sf booking-info-row">
              <div class="booking-content">
                <span>Layover time: <%= segment.layoverTime %></span>
              </div>
            </div>
          <% } %>
        <% }); %>
        <div class="booking-info booking-info-sf booking-info-row">
          <div class="booking-content">
            <span>Total travel time: <%= flight.totalTravelTime %></span>
          </div>
        </div>
      </div>
    <% }); %>
  </div>
<% } %>
  <div class="terms-conditions terms-conditions-sf">
    <h3 class="sub-heading-2--dark">Important information</h3>
    <ul class="list-tsc">
      <li>Your entire itinerary will be held; this includes the origin, destination, travel dates, number of passengers and cabin class</li>
      <li>The holding fee is non-refundable</li>
      <li>The fare amount is not inclusive of the holding fee</li>
      <li>You may only purchase add-ons once you have confirmed your booking</li>
    </ul>
    <p>For more infomation, visit our
      <a class="" href="#" title="FAQs"> FAQs</a>
    </p>
  </div>
  <div class="grand-total">
    <span class="total-title">Full fare to be paid</span>
    <p class="total-info">
      <span><%= data.bookingSummary.currency %>
        <span data-need-format="2"><%= data.bookingSummary.fareTotal %></span>
      </span>
    </p>
  </div>
  <div class="grand-total">
    <span class="total-title">Secure this fare for up to 48 hours at</span>
    <p class="total-info">
      <span><%= data.bookingSummary.currency %>
        <span data-need-format="2"><%= data.tttVO.tttFare %></span>
      </span>
    </p>
  </div>
  <div class="button-group-1">
    <input type="submit" name="alert-input-2" id="alert-input-2" value="Proceed" class="btn-1">
    <input type="button" name="alert-input-1" id="alert-input-1" value="Cancel" data-close="true" class="btn-2">
  </div>
