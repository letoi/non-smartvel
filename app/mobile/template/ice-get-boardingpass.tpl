<%

 var toDate = function(epoch, format, locale) {
      var date = new Date(epoch),
      format = format || 'dd/mmm/YY',
      locale = locale || 'en'
      dow = {};

      dow.en = ['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];

      var monthNames = ["Jan","Feb", "Mar", "Apr", "May", "June ","July", "Aug", "Sept", "Oct", "Nov", "Dec"];

      var formatted = format
      .replace('D', dow[locale][date.getDay()])
      .replace('d', ("0" + date.getDate()).slice(-2))
      .replace('mmm', ((monthNames[date.getMonth()])))
      .replace('yyyy', date.getFullYear())
      .replace('yy', (''+date.getFullYear()).slice(-2))
      .replace('hh',  ("0" + date.getHours()).slice(-2))
      .replace('mn', date.getMinutes())

      return formatted
   }
%>
<aside class="popup popup--boarding-3 hidden">
        <div class="popup__inner" tabindex="-1">
          <div class="popup__content focus-outline" role="document" tabindex="0">
            <div class="boarding">
              <h2 class="popup__heading" aria-hidden="false">Get boarding passes</h2>
              <div class="alert-block checkin-alert">
                <div class="inner">
                  <div class="alert__icon"><em class="ico-alert"></em></div>
                  <div class="alert__message">Once a boarding pass is generated, check-in cancellation and seat change <strong>will not be available.</strong></div>
                </div>
              </div>
              <form action="#" method="post" class="form--boarding-3" novalidate="novalidate">
                <fieldset>
                  <div data-tab="true" class="tabs--1">
                    <ul class="tab style-1">
                      <li class="tab-item active"><a href="mailto:#"><em class="ico-mail"><span class="ui-helper-hidden-accessible">Email</span></em>Email</a>
                      </li>
                      <li class="tab-item"><a href="#"><em class="ico-sms"><span class="ui-helper-hidden-accessible">SMS</span></em>SMS</a>
                      </li>
                    </ul>
                    <ul class="list-1 hidden-tablet">
                      <li><a href="pdf/adlafjlxjewfasd89asd8f.pdf" class="link-print" target="_blank"><em class="ico-print"><span class="ui-helper-hidden-accessible">Print</span></em>Print all boarding passes</a>
                      </li>
                    </ul>
                    <div class="tab-wrapper">
                        <div class="tab-content email active">
                            <div class="table-default">
                               <%
                               var i = 1;
                                  _.each(paxRecordVO.flights, function(flight) {
                                    var checkedInLeg = [];
                                  _.each(allPax, function(pax) {
                                      _.each(pax.services, function(service){
                                          _.each(flight.flightIDs, function(flID) {
                                             if(service.flightID == flID && service.dcsStatus.checkedIn == true && service.dcsStatus.boardingPassIssued == true ){
                                %> 
                                      <div data-validate-row="true" class="table-row">
                                          <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number"><%- i++ %>.</span><span class="passenger-info__text">
                                               <%- pax.paxDetails.firstName %> <%- pax.paxDetails.lastName %>
                                              </span></span></div>
                                              <div data-validate-col="true" class="table-col table-col-2">
                                                <div class="table-inner"><span class="input-1">
                                                    <label for="boarding-<%- i %>-input-<%- i %>" class="ui-helper-hidden-accessible">Email address</label>
                                                    <input type="email" name="boarding-<%- i %>-input-<%- i %>" id="boarding-<%- i %>-input-<%- i %>" value="" placeholder="Email address" aria-labelledby="boarding-<%- i %>-input-<%- i %>-error" data-rule-validateemail="true" data-msg-validateemail="Enter a valid email address." data-email="true" class="default"></span>
                                                </div>
                                              </div>
                                        </div>
                                  <%
                                        }
                                      });
                                    });
                                  });
                               });    
                             %>        
                             </div>
                       </div>
                      <div class="tab-content sms">
                        <div class="table-default">
                          <div data-validate-row="true" class="table-row">
                            <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">1.</span><span class="passenger-info__text">
                                  SHIH SING LIM <br>
                                  PRENTICE PORTER<span>&nbsp;(Infant)</span></span></span></div>
                            <div data-validate-col="true" class="table-col table-col-2">
                              <div class="table-inner">
                                <div data-autocomplete="true" class="custom-select custom-select--2 from-select default">
                                  <label for="check-country" class="select__label"><span class="ui-helper-hidden-accessible">Country number</span>
                                  </label><span class="select__text">
                                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" name="check-country" id="check-country" placeholder="Country" value="" aria-labelledby="check-country-error" data-rule-required="true" data-msg-required="Select a country code." data-country="true" class="ui-autocomplete-input" autocomplete="off" aria-required="true"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span><span class="ico-dropdown">arrow</span>
                                  <label for="check-country-label" class="hidden">&nbsp;
                                    <select id="check-country-label" data-city-group="false">
                                      <option value="1" data-text="Afghanistan">Afghanistan</option>
                                      <option value="2" data-text="Albania">Albania</option>
                                      <option value="3" data-text="Algeria">Algeria</option>
                                      <option value="4" data-text="American Samoa">American Samoa</option>
                                      <option value="5" data-text="Andorra">Andorra</option>
                                      <option value="6" data-text="Angola">Angola</option>
                                      <option value="7" data-text="Antigua And Barbuda">Antigua And Barbuda</option>
                                      <option value="8" data-text="Argentina">Argentina</option>
                                      <option value="9" data-text="Armenia">Armenia</option>
                                      <option value="10" data-text="Aruba">Aruba</option>
                                      <option value="11" data-text="Australia">Australia</option>
                                      <option value="12" data-text="Austria">Austria</option>
                                      <option value="13" data-text="Azerbaijan Republic">Azerbaijan Republic</option>
                                      <option value="14" data-text="Bahamas">Bahamas</option>
                                      <option value="15" data-text="Bahrain">Bahrain</option>
                                      <option value="16" data-text="Bangladesh">Bangladesh</option>
                                      <option value="17" data-text="Barbados">Barbados</option>
                                      <option value="18" data-text="Belarus">Belarus</option>
                                      <option value="19" data-text="Belgium">Belgium</option>
                                      <option value="20" data-text="Belize">Belize</option>
                                      <option value="21" data-text="Benin">Benin</option>
                                      <option value="22" data-text="Bermuda">Bermuda</option>
                                      <option value="23" data-text="Bhutan">Bhutan</option>
                                      <option value="24" data-text="Bolivia">Bolivia</option>
                                      <option value="25" data-text="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                      <option value="26" data-text="Bosnia Herzegovina">Bosnia Herzegovina</option>
                                      <option value="27" data-text="Botswana">Botswana</option>
                                      <option value="28" data-text="Brazil">Brazil</option>
                                      <option value="29" data-text="Brunei Darussalam">Brunei Darussalam</option>
                                      <option value="30" data-text="Bulgaria">Bulgaria</option>
                                      <option value="31" data-text="Burkina Faso">Burkina Faso</option>
                                      <option value="32" data-text="Burundi">Burundi</option>
                                      <option value="33" data-text="Cambodia">Cambodia</option>
                                      <option value="34" data-text="Cameroon">Cameroon</option>
                                      <option value="35" data-text="Canada">Canada</option>
                                      <option value="36" data-text="Cape Verde">Cape Verde</option>
                                      <option value="37" data-text="Cayman Islands">Cayman Islands</option>
                                      <option value="38" data-text="Central African Republic">Central African Republic</option>
                                      <option value="39" data-text="Chad">Chad</option>
                                      <option value="40" data-text="Chagos Archipelago">Chagos Archipelago</option>
                                      <option value="41" data-text="Chile">Chile</option>
                                      <option value="42" data-text="Cocos Island">Cocos Island</option>
                                      <option value="43" data-text="Colombia">Colombia</option>
                                      <option value="44" data-text="Congo (Kinshasa)">Congo (Kinshasa)</option>
                                      <option value="45" data-text="Cook Islands">Cook Islands</option>
                                      <option value="46" data-text="Costa Rica">Costa Rica</option>
                                      <option value="47" data-text="Cote D'Ivoire">Cote D'Ivoire</option>
                                      <option value="48" data-text="Croatia">Croatia</option>
                                      <option value="49" data-text="Cuba">Cuba</option>
                                      <option value="50" data-text="Curacao">Curacao</option>
                                      <option value="51" data-text="Cyprus">Cyprus</option>
                                      <option value="52" data-text="Czech Republic">Czech Republic</option>
                                      <option value="53" data-text="Denmark">Denmark</option>
                                      <option value="54" data-text="Djibouti">Djibouti</option>
                                      <option value="55" data-text="Dominican Republic">Dominican Republic</option>
                                      <option value="56" data-text="Ecuador">Ecuador</option>
                                      <option value="57" data-text="Egypt">Egypt</option>
                                      <option value="58" data-text="El Salvador">El Salvador</option>
                                      <option value="59" data-text="Equatorial Guinea">Equatorial Guinea</option>
                                      <option value="60" data-text="Estonia">Estonia</option>
                                      <option value="61" data-text="Ethiopia">Ethiopia</option>
                                      <option value="62" data-text="Falkland Islands">Falkland Islands</option>
                                      <option value="63" data-text="Faroe Islands">Faroe Islands</option>
                                      <option value="64" data-text="Fiji">Fiji</option>
                                      <option value="65" data-text="Finland">Finland</option>
                                      <option value="66" data-text="France">France</option>
                                      <option value="67" data-text="French Guiana">French Guiana</option>
                                      <option value="68" data-text="French Polynesia">French Polynesia</option>
                                      <option value="69" data-text="Gambia">Gambia</option>
                                      <option value="70" data-text="Georgia">Georgia</option>
                                      <option value="71" data-text="Germany">Germany</option>
                                      <option value="72" data-text="Ghana">Ghana</option>
                                      <option value="73" data-text="Gibraltar">Gibraltar</option>
                                      <option value="74" data-text="Greece">Greece</option>
                                      <option value="75" data-text="Greenland">Greenland</option>
                                      <option value="76" data-text="Grenadines">Grenadines</option>
                                      <option value="77" data-text="Guadeloupe">Guadeloupe</option>
                                      <option value="78" data-text="Guam">Guam</option>
                                      <option value="79" data-text="Guatemala">Guatemala</option>
                                      <option value="80" data-text="Guinea">Guinea</option>
                                      <option value="81" data-text="Guinea-Bissau">Guinea-Bissau</option>
                                      <option value="82" data-text="Guyana">Guyana</option>
                                      <option value="83" data-text="Haiti">Haiti</option>
                                      <option value="84" data-text="Honduras">Honduras</option>
                                      <option value="85" data-text="Hong Kong SAR">Hong Kong SAR</option>
                                      <option value="86" data-text="Hungary">Hungary</option>
                                      <option value="87" data-text="Iceland">Iceland</option>
                                      <option value="88" data-text="India">India</option>
                                      <option value="89" data-text="Indonesia">Indonesia</option>
                                      <option value="90" data-text="Iran">Iran</option>
                                      <option value="91" data-text="Iraq">Iraq</option>
                                      <option value="92" data-text="Ireland">Ireland</option>
                                      <option value="93" data-text="Israel">Israel</option>
                                      <option value="94" data-text="Italy">Italy</option>
                                      <option value="95" data-text="Jamaica">Jamaica</option>
                                      <option value="96" data-text="Japan">Japan</option>
                                      <option value="97" data-text="Jordan">Jordan</option>
                                      <option value="98" data-text="Kazakhstan">Kazakhstan</option>
                                      <option value="99" data-text="Kenya">Kenya</option>
                                      <option value="100" data-text="Kingdom Of Lesotho">Kingdom Of Lesotho</option>
                                      <option value="101" data-text="Kiribati">Kiribati</option>
                                      <option value="102" data-text="Korea (North)">Korea (North)</option>
                                      <option value="103" data-text="Kuwait">Kuwait</option>
                                      <option value="104" data-text="Kyrgyzstan">Kyrgyzstan</option>
                                      <option value="105" data-text="Laos">Laos</option>
                                      <option value="106" data-text="Latvia">Latvia</option>
                                      <option value="107" data-text="Lebanon">Lebanon</option>
                                      <option value="108" data-text="Liberia">Liberia</option>
                                      <option value="109" data-text="Libya">Libya</option>
                                      <option value="110" data-text="Liechtenstein">Liechtenstein</option>
                                      <option value="111" data-text="Lithuania">Lithuania</option>
                                      <option value="112" data-text="Luxembourg">Luxembourg</option>
                                      <option value="113" data-text="Macau">Macau</option>
                                      <option value="114" data-text="Macedonia">Macedonia</option>
                                      <option value="115" data-text="Madagascar">Madagascar</option>
                                      <option value="116" data-text="Malawi">Malawi</option>
                                      <option value="117" data-text="Malaysia">Malaysia</option>
                                      <option value="118" data-text="Maldives">Maldives</option>
                                      <option value="119" data-text="Malta">Malta</option>
                                      <option value="120" data-text="Marshall Islands">Marshall Islands</option>
                                      <option value="121" data-text="Martinique">Martinique</option>
                                      <option value="122" data-text="Mauritania">Mauritania</option>
                                      <option value="123" data-text="Mauritius">Mauritius</option>
                                      <option value="124" data-text="Mexico">Mexico</option>
                                      <option value="125" data-text="Micronesia">Micronesia</option>
                                      <option value="126" data-text="Moldova">Moldova</option>
                                      <option value="127" data-text="Monaco">Monaco</option>
                                      <option value="128" data-text="Mongolia">Mongolia</option>
                                      <option value="129" data-text="Montenegro">Montenegro</option>
                                      <option value="130" data-text="Morocco">Morocco</option>
                                      <option value="131" data-text="Mozambique">Mozambique</option>
                                      <option value="132" data-text="Myanmar">Myanmar</option>
                                      <option value="133" data-text="Namibia">Namibia</option>
                                      <option value="134" data-text="Nauru">Nauru</option>
                                      <option value="135" data-text="Nepal">Nepal</option>
                                      <option value="136" data-text="Netherlands">Netherlands</option>
                                      <option value="137" data-text="New Caledonia">New Caledonia</option>
                                      <option value="138" data-text="New Zealand">New Zealand</option>
                                      <option value="139" data-text="Nicaragua">Nicaragua</option>
                                      <option value="140" data-text="Niger">Niger</option>
                                      <option value="141" data-text="Nigeria">Nigeria</option>
                                      <option value="142" data-text="Niue">Niue</option>
                                      <option value="143" data-text="Norfolk Islan">Norfolk Islan</option>
                                      <option value="144" data-text="Northern Marianas Islands">Northern Marianas Islands</option>
                                      <option value="145" data-text="Norway">Norway</option>
                                      <option value="146" data-text="Oman">Oman</option>
                                      <option value="147" data-text="Pakistan">Pakistan</option>
                                      <option value="148" data-text="Palau">Palau</option>
                                      <option value="149" data-text="Panama">Panama</option>
                                      <option value="150" data-text="Papua New Guinea">Papua New Guinea</option>
                                      <option value="151" data-text="Paraguay">Paraguay</option>
                                      <option value="152" data-text="People's Republic Of China">People's Republic Of China</option>
                                      <option value="153" data-text="Peru">Peru</option>
                                      <option value="154" data-text="Philippines">Philippines</option>
                                      <option value="155" data-text="Poland">Poland</option>
                                      <option value="156" data-text="Portugal">Portugal</option>
                                      <option value="157" data-text="Puerto Rico">Puerto Rico</option>
                                      <option value="158" data-text="Qatar">Qatar</option>
                                      <option value="159" data-text="Republic Of Seychelles">Republic Of Seychelles</option>
                                      <option value="160" data-text="Romania">Romania</option>
                                      <option value="161" data-text="Russian Federation">Russian Federation</option>
                                      <option value="162" data-text="Rwanda">Rwanda</option>
                                      <option value="163" data-text="Saint Lucia">Saint Lucia</option>
                                      <option value="164" data-text="Samoa">Samoa</option>
                                      <option value="165" data-text="Sao Tome">Sao Tome</option>
                                      <option value="166" data-text="Saudi Arabia">Saudi Arabia</option>
                                      <option value="167" data-text="Senegal">Senegal</option>
                                      <option value="168" data-text="Serbia">Serbia</option>
                                      <option value="169" data-text="Sierra Leone">Sierra Leone</option>
                                      <option value="SG" data-text="Singapore">Singapore</option>
                                      <option value="171" data-text="Sint Maarten">Sint Maarten</option>
                                      <option value="172" data-text="Slovakia">Slovakia</option>
                                      <option value="173" data-text="Slovenia">Slovenia</option>
                                      <option value="174" data-text="Solomon Islands">Solomon Islands</option>
                                      <option value="175" data-text="Somalia">Somalia</option>
                                      <option value="176" data-text="South Africa">South Africa</option>
                                      <option value="177" data-text="South Korea">South Korea</option>
                                      <option value="178" data-text="Spain">Spain</option>
                                      <option value="179" data-text="Sri Lanka">Sri Lanka</option>
                                      <option value="180" data-text="St Vincent And The Grenadines">St Vincent And The Grenadines</option>
                                      <option value="181" data-text="St. Kills and Nevis">St. Kills and Nevis</option>
                                      <option value="182" data-text="Sudan">Sudan</option>
                                      <option value="183" data-text="Suriname">Suriname</option>
                                      <option value="184" data-text="Swaziland">Swaziland</option>
                                      <option value="185" data-text="Sweden">Sweden</option>
                                      <option value="186" data-text="Switzerland">Switzerland</option>
                                      <option value="187" data-text="Syria">Syria</option>
                                      <option value="188" data-text="Taiwan">Taiwan</option>
                                      <option value="189" data-text="Tajlkistan">Tajlkistan</option>
                                      <option value="190" data-text="Tanzania">Tanzania</option>
                                      <option value="191" data-text="Thailand">Thailand</option>
                                      <option value="192" data-text="Timor Leste">Timor Leste</option>
                                      <option value="193" data-text="Togo">Togo</option>
                                      <option value="194" data-text="Tonga">Tonga</option>
                                      <option value="195" data-text="Trinidad">Trinidad</option>
                                      <option value="196" data-text="Tunisia">Tunisia</option>
                                      <option value="197" data-text="Turkey">Turkey</option>
                                      <option value="198" data-text="Turkmenistan">Turkmenistan</option>
                                      <option value="199" data-text="Turks and Caios Islands">Turks and Caios Islands</option>
                                      <option value="200" data-text="Tuvalu">Tuvalu</option>
                                      <option value="201" data-text="Uganda">Uganda</option>
                                      <option value="202" data-text="Ukraine">Ukraine</option>
                                      <option value="203" data-text="United Arab Emirates">United Arab Emirates</option>
                                      <option value="204" data-text="United Kingdom">United Kingdom</option>
                                      <option value="205" data-text="United States Of America">United States Of America</option>
                                      <option value="206" data-text="Uruguay">Uruguay</option>
                                      <option value="207" data-text="Uzbekistan">Uzbekistan</option>
                                      <option value="208" data-text="Vanuatu">Vanuatu</option>
                                      <option value="209" data-text="Venezuela">Venezuela</option>
                                      <option value="210" data-text="Vietnam">Vietnam</option>
                                      <option value="211" data-text="Virgin Islands (GB)">Virgin Islands (GB)</option>
                                      <option value="212" data-text="Virgin Islands(U.S.)">Virgin Islands(U.S.)</option>
                                      <option value="213" data-text="Walls Islands">Walls Islands</option>
                                      <option value="214" data-text="Yemen">Yemen</option>
                                      <option value="215" data-text="Zambia">Zambia</option>
                                      <option value="216" data-text="Zimbabwe">Zimbabwe</option>
                                    </select>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div data-validate-col="true" class="table-col table-col-3">
                              <div class="table-inner"><span class="input-1">
                                  <label for="boarding-3-input-3" class="ui-helper-hidden-accessible">Area number</label>
                                  <input type="tel" name="boarding-3-input-3" id="boarding-3-input-3" value="" placeholder="Area" aria-labelledby="boarding-3-input-3-error" maxlength="4" data-rule-digits="true" data-msg-digits="Enter numbers only." data-area="true" class="default"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span>
                              </div>
                            </div>
                            <div data-validate-col="true" class="table-col table-col-4">
                              <div class="table-inner"><span class="input-1">
                                  <label for="boarding-3-input-4" class="ui-helper-hidden-accessible">Phone number</label>
                                  <input type="tel" name="boarding-3-input-4" id="boarding-3-input-4" value="912345678" placeholder="Phone number" aria-labelledby="boarding-3-input-4-error" maxlength="16" data-rule-required="true" data-msg-required="Enter a valid phone number." data-rule-digits="true" data-msg-digits="Enter numbers only." data-phone="true" aria-required="true"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span>
                              </div>
                            </div>
                          </div>
                          <div data-validate-row="true" class="table-row">
                            <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">2.</span><span class="passenger-info__text">DONGNI GUO</span></span></div>
                            <div data-validate-col="true" class="table-col table-col-2">
                              <div class="table-inner">
                                <div data-autocomplete="true" class="custom-select custom-select--2 from-select default">
                                  <label for="check1-country" class="select__label"><span class="ui-helper-hidden-accessible">Country number</span>
                                  </label><span class="select__text">
                                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><input type="text" name="check1-country" id="check1-country" placeholder="Country" value="" aria-labelledby="check1-country-error" data-rule-required="true" data-msg-required="Select a country code." data-country="true" class="ui-autocomplete-input" autocomplete="off" aria-required="true"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span><span class="ico-dropdown">arrow</span>
                                  <label for="check1-country-label" class="hidden">&nbsp;
                                    <select id="check1-country-label" data-city-group="false">
                                      <option value="1" data-text="Afghanistan">Afghanistan</option>
                                      <option value="2" data-text="Albania">Albania</option>
                                      <option value="3" data-text="Algeria">Algeria</option>
                                      <option value="4" data-text="American Samoa">American Samoa</option>
                                      <option value="5" data-text="Andorra">Andorra</option>
                                      <option value="6" data-text="Angola">Angola</option>
                                      <option value="7" data-text="Antigua And Barbuda">Antigua And Barbuda</option>
                                      <option value="8" data-text="Argentina">Argentina</option>
                                      <option value="9" data-text="Armenia">Armenia</option>
                                      <option value="10" data-text="Aruba">Aruba</option>
                                      <option value="11" data-text="Australia">Australia</option>
                                      <option value="12" data-text="Austria">Austria</option>
                                      <option value="13" data-text="Azerbaijan Republic">Azerbaijan Republic</option>
                                      <option value="14" data-text="Bahamas">Bahamas</option>
                                      <option value="15" data-text="Bahrain">Bahrain</option>
                                      <option value="16" data-text="Bangladesh">Bangladesh</option>
                                      <option value="17" data-text="Barbados">Barbados</option>
                                      <option value="18" data-text="Belarus">Belarus</option>
                                      <option value="19" data-text="Belgium">Belgium</option>
                                      <option value="20" data-text="Belize">Belize</option>
                                      <option value="21" data-text="Benin">Benin</option>
                                      <option value="22" data-text="Bermuda">Bermuda</option>
                                      <option value="23" data-text="Bhutan">Bhutan</option>
                                      <option value="24" data-text="Bolivia">Bolivia</option>
                                      <option value="25" data-text="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                      <option value="26" data-text="Bosnia Herzegovina">Bosnia Herzegovina</option>
                                      <option value="27" data-text="Botswana">Botswana</option>
                                      <option value="28" data-text="Brazil">Brazil</option>
                                      <option value="29" data-text="Brunei Darussalam">Brunei Darussalam</option>
                                      <option value="30" data-text="Bulgaria">Bulgaria</option>
                                      <option value="31" data-text="Burkina Faso">Burkina Faso</option>
                                      <option value="32" data-text="Burundi">Burundi</option>
                                      <option value="33" data-text="Cambodia">Cambodia</option>
                                      <option value="34" data-text="Cameroon">Cameroon</option>
                                      <option value="35" data-text="Canada">Canada</option>
                                      <option value="36" data-text="Cape Verde">Cape Verde</option>
                                      <option value="37" data-text="Cayman Islands">Cayman Islands</option>
                                      <option value="38" data-text="Central African Republic">Central African Republic</option>
                                      <option value="39" data-text="Chad">Chad</option>
                                      <option value="40" data-text="Chagos Archipelago">Chagos Archipelago</option>
                                      <option value="41" data-text="Chile">Chile</option>
                                      <option value="42" data-text="Cocos Island">Cocos Island</option>
                                      <option value="43" data-text="Colombia">Colombia</option>
                                      <option value="44" data-text="Congo (Kinshasa)">Congo (Kinshasa)</option>
                                      <option value="45" data-text="Cook Islands">Cook Islands</option>
                                      <option value="46" data-text="Costa Rica">Costa Rica</option>
                                      <option value="47" data-text="Cote D'Ivoire">Cote D'Ivoire</option>
                                      <option value="48" data-text="Croatia">Croatia</option>
                                      <option value="49" data-text="Cuba">Cuba</option>
                                      <option value="50" data-text="Curacao">Curacao</option>
                                      <option value="51" data-text="Cyprus">Cyprus</option>
                                      <option value="52" data-text="Czech Republic">Czech Republic</option>
                                      <option value="53" data-text="Denmark">Denmark</option>
                                      <option value="54" data-text="Djibouti">Djibouti</option>
                                      <option value="55" data-text="Dominican Republic">Dominican Republic</option>
                                      <option value="56" data-text="Ecuador">Ecuador</option>
                                      <option value="57" data-text="Egypt">Egypt</option>
                                      <option value="58" data-text="El Salvador">El Salvador</option>
                                      <option value="59" data-text="Equatorial Guinea">Equatorial Guinea</option>
                                      <option value="60" data-text="Estonia">Estonia</option>
                                      <option value="61" data-text="Ethiopia">Ethiopia</option>
                                      <option value="62" data-text="Falkland Islands">Falkland Islands</option>
                                      <option value="63" data-text="Faroe Islands">Faroe Islands</option>
                                      <option value="64" data-text="Fiji">Fiji</option>
                                      <option value="65" data-text="Finland">Finland</option>
                                      <option value="66" data-text="France">France</option>
                                      <option value="67" data-text="French Guiana">French Guiana</option>
                                      <option value="68" data-text="French Polynesia">French Polynesia</option>
                                      <option value="69" data-text="Gambia">Gambia</option>
                                      <option value="70" data-text="Georgia">Georgia</option>
                                      <option value="71" data-text="Germany">Germany</option>
                                      <option value="72" data-text="Ghana">Ghana</option>
                                      <option value="73" data-text="Gibraltar">Gibraltar</option>
                                      <option value="74" data-text="Greece">Greece</option>
                                      <option value="75" data-text="Greenland">Greenland</option>
                                      <option value="76" data-text="Grenadines">Grenadines</option>
                                      <option value="77" data-text="Guadeloupe">Guadeloupe</option>
                                      <option value="78" data-text="Guam">Guam</option>
                                      <option value="79" data-text="Guatemala">Guatemala</option>
                                      <option value="80" data-text="Guinea">Guinea</option>
                                      <option value="81" data-text="Guinea-Bissau">Guinea-Bissau</option>
                                      <option value="82" data-text="Guyana">Guyana</option>
                                      <option value="83" data-text="Haiti">Haiti</option>
                                      <option value="84" data-text="Honduras">Honduras</option>
                                      <option value="85" data-text="Hong Kong SAR">Hong Kong SAR</option>
                                      <option value="86" data-text="Hungary">Hungary</option>
                                      <option value="87" data-text="Iceland">Iceland</option>
                                      <option value="88" data-text="India">India</option>
                                      <option value="89" data-text="Indonesia">Indonesia</option>
                                      <option value="90" data-text="Iran">Iran</option>
                                      <option value="91" data-text="Iraq">Iraq</option>
                                      <option value="92" data-text="Ireland">Ireland</option>
                                      <option value="93" data-text="Israel">Israel</option>
                                      <option value="94" data-text="Italy">Italy</option>
                                      <option value="95" data-text="Jamaica">Jamaica</option>
                                      <option value="96" data-text="Japan">Japan</option>
                                      <option value="97" data-text="Jordan">Jordan</option>
                                      <option value="98" data-text="Kazakhstan">Kazakhstan</option>
                                      <option value="99" data-text="Kenya">Kenya</option>
                                      <option value="100" data-text="Kingdom Of Lesotho">Kingdom Of Lesotho</option>
                                      <option value="101" data-text="Kiribati">Kiribati</option>
                                      <option value="102" data-text="Korea (North)">Korea (North)</option>
                                      <option value="103" data-text="Kuwait">Kuwait</option>
                                      <option value="104" data-text="Kyrgyzstan">Kyrgyzstan</option>
                                      <option value="105" data-text="Laos">Laos</option>
                                      <option value="106" data-text="Latvia">Latvia</option>
                                      <option value="107" data-text="Lebanon">Lebanon</option>
                                      <option value="108" data-text="Liberia">Liberia</option>
                                      <option value="109" data-text="Libya">Libya</option>
                                      <option value="110" data-text="Liechtenstein">Liechtenstein</option>
                                      <option value="111" data-text="Lithuania">Lithuania</option>
                                      <option value="112" data-text="Luxembourg">Luxembourg</option>
                                      <option value="113" data-text="Macau">Macau</option>
                                      <option value="114" data-text="Macedonia">Macedonia</option>
                                      <option value="115" data-text="Madagascar">Madagascar</option>
                                      <option value="116" data-text="Malawi">Malawi</option>
                                      <option value="117" data-text="Malaysia">Malaysia</option>
                                      <option value="118" data-text="Maldives">Maldives</option>
                                      <option value="119" data-text="Malta">Malta</option>
                                      <option value="120" data-text="Marshall Islands">Marshall Islands</option>
                                      <option value="121" data-text="Martinique">Martinique</option>
                                      <option value="122" data-text="Mauritania">Mauritania</option>
                                      <option value="123" data-text="Mauritius">Mauritius</option>
                                      <option value="124" data-text="Mexico">Mexico</option>
                                      <option value="125" data-text="Micronesia">Micronesia</option>
                                      <option value="126" data-text="Moldova">Moldova</option>
                                      <option value="127" data-text="Monaco">Monaco</option>
                                      <option value="128" data-text="Mongolia">Mongolia</option>
                                      <option value="129" data-text="Montenegro">Montenegro</option>
                                      <option value="130" data-text="Morocco">Morocco</option>
                                      <option value="131" data-text="Mozambique">Mozambique</option>
                                      <option value="132" data-text="Myanmar">Myanmar</option>
                                      <option value="133" data-text="Namibia">Namibia</option>
                                      <option value="134" data-text="Nauru">Nauru</option>
                                      <option value="135" data-text="Nepal">Nepal</option>
                                      <option value="136" data-text="Netherlands">Netherlands</option>
                                      <option value="137" data-text="New Caledonia">New Caledonia</option>
                                      <option value="138" data-text="New Zealand">New Zealand</option>
                                      <option value="139" data-text="Nicaragua">Nicaragua</option>
                                      <option value="140" data-text="Niger">Niger</option>
                                      <option value="141" data-text="Nigeria">Nigeria</option>
                                      <option value="142" data-text="Niue">Niue</option>
                                      <option value="143" data-text="Norfolk Islan">Norfolk Islan</option>
                                      <option value="144" data-text="Northern Marianas Islands">Northern Marianas Islands</option>
                                      <option value="145" data-text="Norway">Norway</option>
                                      <option value="146" data-text="Oman">Oman</option>
                                      <option value="147" data-text="Pakistan">Pakistan</option>
                                      <option value="148" data-text="Palau">Palau</option>
                                      <option value="149" data-text="Panama">Panama</option>
                                      <option value="150" data-text="Papua New Guinea">Papua New Guinea</option>
                                      <option value="151" data-text="Paraguay">Paraguay</option>
                                      <option value="152" data-text="People's Republic Of China">People's Republic Of China</option>
                                      <option value="153" data-text="Peru">Peru</option>
                                      <option value="154" data-text="Philippines">Philippines</option>
                                      <option value="155" data-text="Poland">Poland</option>
                                      <option value="156" data-text="Portugal">Portugal</option>
                                      <option value="157" data-text="Puerto Rico">Puerto Rico</option>
                                      <option value="158" data-text="Qatar">Qatar</option>
                                      <option value="159" data-text="Republic Of Seychelles">Republic Of Seychelles</option>
                                      <option value="160" data-text="Romania">Romania</option>
                                      <option value="161" data-text="Russian Federation">Russian Federation</option>
                                      <option value="162" data-text="Rwanda">Rwanda</option>
                                      <option value="163" data-text="Saint Lucia">Saint Lucia</option>
                                      <option value="164" data-text="Samoa">Samoa</option>
                                      <option value="165" data-text="Sao Tome">Sao Tome</option>
                                      <option value="166" data-text="Saudi Arabia">Saudi Arabia</option>
                                      <option value="167" data-text="Senegal">Senegal</option>
                                      <option value="168" data-text="Serbia">Serbia</option>
                                      <option value="169" data-text="Sierra Leone">Sierra Leone</option>
                                      <option value="SG" data-text="Singapore">Singapore</option>
                                      <option value="171" data-text="Sint Maarten">Sint Maarten</option>
                                      <option value="172" data-text="Slovakia">Slovakia</option>
                                      <option value="173" data-text="Slovenia">Slovenia</option>
                                      <option value="174" data-text="Solomon Islands">Solomon Islands</option>
                                      <option value="175" data-text="Somalia">Somalia</option>
                                      <option value="176" data-text="South Africa">South Africa</option>
                                      <option value="177" data-text="South Korea">South Korea</option>
                                      <option value="178" data-text="Spain">Spain</option>
                                      <option value="179" data-text="Sri Lanka">Sri Lanka</option>
                                      <option value="180" data-text="St Vincent And The Grenadines">St Vincent And The Grenadines</option>
                                      <option value="181" data-text="St. Kills and Nevis">St. Kills and Nevis</option>
                                      <option value="182" data-text="Sudan">Sudan</option>
                                      <option value="183" data-text="Suriname">Suriname</option>
                                      <option value="184" data-text="Swaziland">Swaziland</option>
                                      <option value="185" data-text="Sweden">Sweden</option>
                                      <option value="186" data-text="Switzerland">Switzerland</option>
                                      <option value="187" data-text="Syria">Syria</option>
                                      <option value="188" data-text="Taiwan">Taiwan</option>
                                      <option value="189" data-text="Tajlkistan">Tajlkistan</option>
                                      <option value="190" data-text="Tanzania">Tanzania</option>
                                      <option value="191" data-text="Thailand">Thailand</option>
                                      <option value="192" data-text="Timor Leste">Timor Leste</option>
                                      <option value="193" data-text="Togo">Togo</option>
                                      <option value="194" data-text="Tonga">Tonga</option>
                                      <option value="195" data-text="Trinidad">Trinidad</option>
                                      <option value="196" data-text="Tunisia">Tunisia</option>
                                      <option value="197" data-text="Turkey">Turkey</option>
                                      <option value="198" data-text="Turkmenistan">Turkmenistan</option>
                                      <option value="199" data-text="Turks and Caios Islands">Turks and Caios Islands</option>
                                      <option value="200" data-text="Tuvalu">Tuvalu</option>
                                      <option value="201" data-text="Uganda">Uganda</option>
                                      <option value="202" data-text="Ukraine">Ukraine</option>
                                      <option value="203" data-text="United Arab Emirates">United Arab Emirates</option>
                                      <option value="204" data-text="United Kingdom">United Kingdom</option>
                                      <option value="205" data-text="United States Of America">United States Of America</option>
                                      <option value="206" data-text="Uruguay">Uruguay</option>
                                      <option value="207" data-text="Uzbekistan">Uzbekistan</option>
                                      <option value="208" data-text="Vanuatu">Vanuatu</option>
                                      <option value="209" data-text="Venezuela">Venezuela</option>
                                      <option value="210" data-text="Vietnam">Vietnam</option>
                                      <option value="211" data-text="Virgin Islands (GB)">Virgin Islands (GB)</option>
                                      <option value="212" data-text="Virgin Islands(U.S.)">Virgin Islands(U.S.)</option>
                                      <option value="213" data-text="Walls Islands">Walls Islands</option>
                                      <option value="214" data-text="Yemen">Yemen</option>
                                      <option value="215" data-text="Zambia">Zambia</option>
                                      <option value="216" data-text="Zimbabwe">Zimbabwe</option>
                                    </select>
                                  </label>
                                </div>
                              </div>
                            </div>
                            <div data-validate-col="true" class="table-col table-col-3">
                              <div class="table-inner"><span class="input-1">
                                  <label for="boarding-3-input-6" class="ui-helper-hidden-accessible">Area number</label>
                                  <input type="tel" name="boarding-3-input-6" id="boarding-3-input-6" value="" placeholder="Area" aria-labelledby="boarding-3-input-6-error" maxlength="4" data-rule-digits="true" data-msg-digits="Enter numbers only." data-area="true" class="default"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span>
                              </div>
                            </div>
                            <div data-validate-col="true" class="table-col table-col-4">
                              <div class="table-inner"><span class="input-1 default">
                                  <label for="boarding-3-input-7" class="ui-helper-hidden-accessible">Phone number</label>
                                  <input type="tel" name="boarding-3-input-7" id="boarding-3-input-7" value="" placeholder="Phone number" aria-labelledby="boarding-3-input-7-error" maxlength="16" data-rule-required="true" data-msg-required="Enter a valid phone number." data-rule-digits="true" data-msg-digits="Enter numbers only." data-phone="true" class="default" aria-required="true"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="button-group-1">
                    <input type="submit" name="boarding-10-submit" id="boarding-10-submit" value="Send" class="btn-1">
                  </div>
                </fieldset>
              </form>
            </div>
            <div class="boarding-1" style="display: none;">
              <h2 class="popup__heading" aria-hidden="false">Boarding pass successfully sent</h2>
              <p class="popup__text-intro">You have successfully sent your boarding pass to:<a href="pdf/adlafjlxjewfasd89asd8f.pdf" target="_blank"><em class="ico-print"><span class="ui-helper-hidden-accessible">Print</span></em>Print all boarding passes</a>
              </p>
              <form action="#" method="post" class="form--boarding-1">
                <fieldset>
                  <div class="table-default">
                    <div class="table-row">
                      <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">1.</span><span class="passenger-info__text">
                            SHIH SING LIM <br>
                            PRENTICE PORTER<span>&nbsp;(Infant)</span></span></span></div>
                      <div class="table-col table-col-2">
                        <div class="table-inner"><span data-email="true" class="input-1 disabled">
                            <label for="boarding-1-input-1" class="ui-helper-hidden-accessible">Email address</label>
                            <input type="email" name="boarding-1-input-1" value="shihsing.lim@gmail.com" id="" readonly="readonly" aria-labelledby="boarding-1-input-1-error"></span>
                        </div>
                      </div>
                    </div>
                    <div class="table-row">
                      <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">2.</span><span class="passenger-info__text">DONGNI GUO</span></span></div>
                      <div class="table-col table-col-2">
                        <div class="table-inner"><span data-email="true" class="input-1 disabled">
                            <label for="boarding-1-input-2" class="ui-helper-hidden-accessible">Email address</label>
                            <input type="email" name="boarding-1-input-2" value="dongni.guo@gmail.com" id="" readonly="readonly" aria-labelledby="boarding-1-input-2-error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="button-group-1">
                    <input type="submit" name="boarding-01-submit" id="boarding-01-submit" value="Back to check-in summary" class="btn-1 btn-back-booking">
                  </div>
                </fieldset>
              </form>
            </div>
            <div class="boarding-2" style="display: none;">
              <h2 class="popup__heading" aria-hidden="false">Boarding pass successfully sent</h2>
              <p class="popup__text-intro">You have successfully sent your boarding pass to:<a href="#"><em class="ico-print"><span class="ui-helper-hidden-accessible">Print</span></em>Print all boarding passes</a>
              </p>
              <form action="#" method="post" class="form--boarding-2">
                <fieldset>
                  <div class="table-default">
                    <div class="table-row">
                      <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">1</span><span class="passenger-info__text">
                            SHIH SING LIM <br>
                            PRENTICE PORTER<span>&nbsp;(Infant)</span></span></span></div>
                      <div class="table-col table-col-2">
                        <div class="table-inner">
                          <div data-customselect="true" class="custom-select custom-select--2 from-select disabled from-select disabled" data-autocomplete="true" data-country="true">
                            <label for="customSelect-1-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Country number</span>
                            </label><span class="select__text" aria-hidden="true">Singapore - SIN-2</span><span class="ico-dropdown" aria-hidden="true">Country</span>
                            <select id="check2country" name="check2country" style="visibility: hidden;">
                              <option value="1" selected="selected">Singapore - SIN-2</option>
                              <option value="2">Melbourne-2</option>
                              <option value="3">Brisbane-2</option>
                              <option value="4">Adelaide-4</option>
                            </select><input class="input-overlay" id="customSelect-1-combobox" name="customSelect-1-combobox" aria-expanded="false" aria-autocomplete="none" aria-owns="customSelect-1-listbox" role="combobox" readonly="readonly" aria-disabled="true">
                          <p class="select__tips ui-helper-hidden-accessible" id="customSelect-1-customSelectUsage" data-description-host="#customSelect-1-combobox" data-description-category="tip" aria-hidden="true">Press space to open combobox, and up, down, and enter keys, to select an option</p></div>
                        </div>
                      </div>
                      <div class="table-col table-col-3">
                        <div class="table-inner"><span data-area="true" class="input-1 disabled">
                            <input type="tel" name="boarding-2-input-1" value="Area" id="" readonly="readonly" aria-labelledby="boarding-2-input-1-error"></span>
                        </div>
                      </div>
                      <div class="table-col table-col-4">
                        <div class="table-inner"><span data-phone="true" class="input-1 disabled">
                            <input type="" name="boarding-2-input-2" value="912345678" id="" readonly="readonly" aria-labelledby="boarding-2-input-2-error"></span>
                        </div>
                      </div>
                    </div>
                    <div class="table-row">
                      <div class="table-col table-col-1"><span class="passenger-info"><span class="passenger-info__number">2.</span><span class="passenger-info__text">DONGNI GUO</span></span></div>
                      <div class="table-col table-col-2">
                        <div class="table-inner">
                          <div data-customselect="true" class="custom-select custom-select--2 from-select disabled from-select disabled" data-autocomplete="true" data-country="true">
                            <label for="customSelect-2-combobox" class="select__label"><span class="ui-helper-hidden-accessible">Country number</span>
                            </label><span class="select__text" aria-hidden="true">Singapore - SIN-2</span><span class="ico-dropdown" aria-hidden="true">Country</span>
                            <select id="check3country" name="check3country" style="visibility: hidden;">
                              <option value="1" selected="selected">Singapore - SIN-2</option>
                              <option value="2">Melbourne-2</option>
                              <option value="3">Brisbane-2</option>
                              <option value="4">Adelaide-4</option>
                            </select><input class="input-overlay" id="customSelect-2-combobox" name="customSelect-2-combobox" aria-expanded="false" aria-autocomplete="none" aria-owns="customSelect-2-listbox" role="combobox" readonly="readonly" aria-disabled="true">
                          <p class="select__tips ui-helper-hidden-accessible" id="customSelect-2-customSelectUsage" data-description-host="#customSelect-2-combobox" data-description-category="tip" aria-hidden="true">Press space to open combobox, and up, down, and enter keys, to select an option</p></div>
                        </div>
                      </div>
                      <div class="table-col table-col-3">
                        <div class="table-inner"><span data-area="true" class="input-1 disabled">
                            <input type="tel" name="boarding-2-input-3" value="Area" id="" readonly="readonly" aria-labelledby="boarding-2-input-3-error"></span>
                        </div>
                      </div>
                      <div class="table-col table-col-4">
                        <div class="table-inner"><span data-phone="true" class="input-1 disabled">
                            <input type="" name="boarding-2-input-4" value="912345678" id="" readonly="readonly" aria-labelledby="boarding-2-input-4-error"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="button-group-1">
                    <input type="submit" name="boarding-02-submit" id="boarding-02-submit" value="Back to check-in summary" class="btn-1 btn-back-booking">
                  </div>
                </fieldset>
              </form>
            </div>
          <a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span></a><a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span></a><a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span></a></div>
        </div>
      </aside>