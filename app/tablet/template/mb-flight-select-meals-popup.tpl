<%
  var flighInfo = data.flightInfo;
  var flightSector = flighInfo.flightSectorInfo;
  var menuInfo = flightSector.cabinClassInfo.menuInfo;
  if($.isArray(flightSector.cabinClassInfo.menuInfo.mealInfo) === false) {
    var selectionDetails = flightSector.cabinClassInfo.menuInfo.mealInfo.selectionDetails;
  }
  var getItemWriteup = function(str) {
    var tmpArr = str.split('-');
    if (tmpArr.length > 1 && tmpArr[0] === '') {
      return $.parseHTML(str.replace(/^(-)/,"").replace(new RegExp("\r?\n","g'"), "<br />"));
    }


    return str;
  };
%>
<h2 class="popup__heading" aria-hidden="true" aria-label="Inflight Menu">Inflight Menu</h2>
<p class="title-1"><%=flighInfo.flightSectorInfo.departureCityName%> to <%=flighInfo.flightSectorInfo.arrivalCityName%></p>
<p class="popup__text-intro">The inflight menu shown here is for your preview. If our inflight meal offerings do not suit your preference, you may consider opting for a Book the Cook meal instead.</p>
<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs--1 multi-tabs addons-tab">
  <ul role="tablist" class="tab tab-level-1">
    <% if(menuInfo.mealInfo && menuInfo.mealInfo.length > 1) {
      _.each(menuInfo.mealInfo, function(menu, menuIdx) { %>
        <% if(menuIdx === 0) { %>
          <li role="presentation" class="tab-item active"><a href="#" tabindex="0" role="tab" aria-controls="section-<%=menu.mealServiceCode%>"> <%= menu.mealServiceName%></a>
          </li>
        <% } else { %>
          <li role="presentation" class="tab-item"><a href="#" tabindex="0" role="tab" aria-controls="section-<%=menu.mealServiceCode%>"> <%= menu.mealServiceName%></a>
          </li>
        <% } %>
      <% }); %>
      <li role="presentation" class="more-item hidden-tb-dt">
        <a href="#" role="tab" aria-controls="section-more">More<em class="ico-dropdown"></em></a>
      </li>
      <li class="tab-item limit-item" data-keep-limit>
        <div data-customselect="true" class="custom-select custom-select--2">
          <label for="customSelect-0-combobox" class="select__label">&nbsp;</label>
          <span class="select__text" aria-hidden="true">More</span>
          <span class="ico-dropdown" aria-hidden="true"></span>
          <select id="multi-select-limit-1" name="multi-select-limit-1">
          </select>
        </div>
      </li>
    <% } else { %>
      <li role="presentation" class="tab-item active"><a href="#" tabindex="0" role="tab" aria-controls="section-<%=menuInfo.mealInfo.mealServiceCode%>"> <%= menuInfo.mealInfo.mealServiceName%></a>
      </li>
    <% } %>
  </ul>
  <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">
    <label for="multi-select-1" class="select__label">&nbsp;</label><span class="select__text"></span><span class="ico-dropdown"></span>
    <select id="multi-select-1" name="multi-select-1">
    <% if(menuInfo.mealInfo && menuInfo.mealInfo.length > 1) {
      _.each(menuInfo.mealInfo, function(menu, menuIdx) { %>
        <% if(menuIdx === 0) { %>
          <option value="<%- menuIdx%>" selected="selected"><%= menu.mealServiceName %></option>
        <% } else { %>
          <option value="<%- menuIdx%>"><%= menu.mealServiceName%></option>
        <% } %>
      <% }); %>
      <% }; %>
    </select>
  </div>
  <div class="tab-wrapper">
  <% if(menuInfo.mealInfo && menuInfo.mealInfo.length > 1) {
    _.each(menuInfo.mealInfo, function(menu, menuIdx) {
      var isMoretab = menu.selectionDetails.length > 2;
      var isOnetab = menu.selectionDetails.length === 1;
      %>
      <% if(menuIdx === 0) { %>
        <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tab-content active tabs--4 multi-tabs">
      <% } else { %>
        <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tab-content tabs--4 multi-tabs">
      <% } %>
        <% if(isMoretab) { %>
          <div class="time-meal">
          <div data-customselect="true" class="custom-select custom-select--2" data-dropdown-tab='true'>
            <label for="customSelect-0-combobox" class="select__label">&nbsp;</label>
            <span class="select__text" aria-hidden="true"><%= menu.selectionDetails[0].selectionName%></span>
            <span class="ico-dropdown" aria-hidden="true"></span>
            <select id="multi-select-level2-<%= menuIdx %>" name="multi-select-level2-<%= menuIdx %>">
              <% _.each(menu.selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
                <% if(selectionDetailIdx === 0) { %>
                  <option value="<%= selectionDetail.selectionName%>" selected="selected"><%= selectionDetail.selectionName%></option>
                <% } else { %>
                   <option value="<%= selectionDetail.selectionName%>"><%= selectionDetail.selectionName%></option>
                <% } %>
              <% }); %>
            </select>
            </div>
            </div>
        <% } else if(!isOnetab) { %>
          <ul role="tablist" class="tab seat-deck-tabs main--tabs">
            <% _.each(menu.selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
              <% if(selectionDetailIdx === 0) { %>
                <li role="tab" aria-selected="true" class="tab-item tab-item-level--2 active"><a href="#" tabindex="0"><%= selectionDetail.selectionName%></a></li>
              <% } else { %>
                 <li role="tab" class="tab-item tab-item-level--2"><a href="#" tabindex="-1"><%= selectionDetail.selectionName%></a></li>
              <% } %>
            <% }); %>
          </ul>
        <% } %>
      <div class="tab-wrapper-1">
        <% _.each(menu.selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
          <%
              var getIcon = function(icon){
                switch(icon){
                  case 'ICP':
                    return 'ico-4-salmon';
                    break;
                  case 'DWH':
                    return 'ico-4-fork-1';
                    break;
                  case 'WHS':
                    return 'ico-4-fork';
                    break;
                  case 'EPG':
                    return 'ico-4-amet';
                    break;
                  case 'PLF':
                    return 'ico-4-cook';
                    break;
                  case 'MTL':
                    return 'ico-4-fork';
                    break;
                  case 'VGT':
                    return 'ico-4-leaf';
                    break;
                  case 'LCL':
                    return 'ico-4-heart';
                    break;
                  case 'LCA':
                    return 'ico-4-sandwich';
                    break;
                  case 'LCH':
                    return 'ico-4-fork';
                    break;
                  case 'CNY':
                    return 'ico-4-info';
                    break;
                  case 'XMAS':
                    return 'ico-4-pine';
                    break;
                  case 'DEP':
                    return 'ico-4-coffee';
                    break;
                  case 'TWG':
                    return 'ico-4-fork';
                    break;
                  default:
                    return '';
                }
              };
              var itemInfoList = [],
                  itemInfoListTmp = [],
                  hasLegend = false;

              if(_.isArray(selectionDetail.itemCategoryInfo)) {

                _.each(selectionDetail.itemCategoryInfo, function(itemCategoryInfo, itemCategoryInfoIdx){

                  var itemInfoListTmp1 = [];
                  if(_.isArray(itemCategoryInfo.itemInfo)) {

                    _.each(itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx){
                      if(itemInfo.specialityInfo.dishIconId) {
                        itemInfoListTmp1.push(itemInfo);
                      }
                    });
                    itemInfoListTmp = itemInfoListTmp.concat(itemInfoListTmp1);
                  } else {
                    if(itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {
                      itemInfoListTmp.push(itemCategoryInfo.itemInfo);

                    }

                  }
                });
                itemInfoList = _.uniq(itemInfoListTmp, function(info) {
                  if(info.specialityInfo.dishIconId) {
                    return info.specialityInfo.dishIconId;
                  }
                });

              } else {
                var itemInfoListTmp2 = [];
                if(_.isArray(selectionDetail.itemCategoryInfo.itemInfo)) {

                    _.each(selectionDetail.itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx){
                      if(itemInfo.specialityInfo.dishIconId) {
                        itemInfoListTmp2.push(itemInfo);
                      }
                    });
                    itemInfoListTmp = itemInfoListTmp.concat(itemInfoListTmp2);
                  } else {
                    if(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {
                      itemInfoListTmp.push(selectionDetail.itemCategoryInfo.itemInfo);

                    }

                  }
                itemInfoList = _.uniq(itemInfoListTmp, function(info) {
                  if(info.specialityInfo.dishIconId) {
                    return info.specialityInfo.dishIconId;
                  }
                });
              }

              hasLegend = itemInfoList.length > 0 ? true : false;
            %>
          <% if(selectionDetailIdx === 0) { %>
            <div class="tab-content-1 active">
          <% } else { %>
            <div class="tab-content-1">
          <% } %>
            <% if(hasLegend) { %>
              <div class="col-main">
            <% } else { %>
              <div class="col-main no-legend">
            <% } %>
                <% if(selectionDetail.selectionDescription) { %>
                  <p class="name-customer"><%= selectionDetail.selectionDescription %></p>
                <% } %>
                <% if(_.isArray(selectionDetail.itemCategoryInfo)) {%>
                  <% _.each(selectionDetail.itemCategoryInfo, function(itemCategoryInfo, itemCategoryInfoIdx) { %>

                    <p class="title-main"><%=itemCategoryInfo.itemCategory %></p>
                    <ul class="list-meals">
                      <% if(_.isArray(itemCategoryInfo.itemInfo)) {%>
                        <% _.each(itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx) { %>
                          <li class="<%= itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                            <% if(itemInfo.itemMenuScriptHeader) {%>
                            <span>
                              <% if(itemInfo.specialityInfo.dishIconId) {%>
                                <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= itemInfo.itemMenuScriptHeader%>
                            </span>
                            <% } %>
                            <% if(itemInfo.itemMenuScriptwriteup) {%>
                              <p class="desc">
                                <% if(!itemInfo.itemMenuScriptHeader && itemInfo.specialityInfo && itemInfo.specialityInfo.dishIconId) {%>
                                - <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                                <% } %>
                                <%= getItemWriteup(itemInfo.itemMenuScriptwriteup)%>
                              </p>
                            <% } %>
                          </li>
                        <% }); %>
                      <% } else {%>
                         <li class="<%= itemCategoryInfo.itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                            <span>
                            <em class="<%= getIcon(itemCategoryInfo.itemInfo.specialityInfo.dishIconId) %>"></em><%= itemCategoryInfo.itemInfo.itemMenuScriptHeader%></span>
                            <% if(itemCategoryInfo.itemInfo.itemMenuScriptwriteup) {%>
                                <p class="desc"><%= getItemWriteup(itemCategoryInfo.itemInfo.itemMenuScriptwriteup)%></p>
                            <% } %>
                          </li>
                      <% } %>
                    </ul>
                  <% }); %>
                <% } else {%>
                  <p class="title-main"><%=selectionDetail.itemCategoryInfo.itemCategory %></p>
                  <ul class="list-meals">
                    <% if(_.isArray(selectionDetail.itemCategoryInfo.itemInfo)) {%>
                      <% _.each(selectionDetail.itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx) { %>
                        <li class="<%= itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                          <% if(itemInfo.itemMenuScriptHeader) {%>
                            <span>
                              <% if(itemInfo.specialityInfo.dishIconId) {%>
                                <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= itemInfo.itemMenuScriptHeader%>
                            </span>
                          <% } %>
                          <% if(itemInfo.itemMenuScriptwriteup) {%>
                            <p class="desc">
                              <% if(!itemInfo.itemMenuScriptHeader && itemInfo.specialityInfo && itemInfo.specialityInfo.dishIconId) {%>
                              - <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= getItemWriteup(itemInfo.itemMenuScriptwriteup)%>
                            </p>
                          <% } %>
                        </li>
                      <% }); %>
                    <% } else {%>
                      <li class="<%= selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                        <% if(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptHeader) {%>
                          <span>
                          <% if(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {%>
                            <em class="<%= getIcon(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) %>"></em>
                          <% } %>
                          <%= selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptHeader%></span>
                        <% } %>
                        <% if(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup) {%>
                          <p class="desc"><%= getItemWriteup(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup)%></p>
                        <% } %>
                      </li>
                    <% } %>
                  </ul>
                <% } %>
              </div>
              <% if(hasLegend) {%>
                <div class="col-legend">
                  <h3 class="title-main">legend</h3>
                  <ul class="list-meals list-main-legend">
                    <% _.each(itemInfoList, function(itemInfo, itemInfoIdx) { %>
                      <li>
                        <% if(itemInfo.specialityInfo.dishIconId) {%>
                          <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId)%>"></em>
                        <% } %>
                        <% if(itemInfo.specialityInfo.specialityFootNote) { %>
                          <%= itemInfo.specialityInfo.specialityFootNote%>
                        <% } %>
                      </li>
                    <% }); %>
                  </ul>
                </div>
              <% } %>
              <div class="main-note">
                <p>Menu items are subject to availability and change.</p>
                <p>Information is correct as of [date].</p>
              </div>
            </div>
        <% });%>
      </div>
    </div>
    <% }); %>
  <% } else { %>
     <%
      var isMoretab = selectionDetails.length > 2;
      var isOnetab = selectionDetails.length === 1;
    %>
    <% if(isMoretab) { %>
      <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tab-content active tabs--4 multi-tabs">
    <% } else { %>
      <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tab-content active multi-tabs  tabs--4">
    <% } %>

    <% if(isMoretab) { %>
      <div class="time-meal">
        <div data-customselect="true" class="custom-select custom-select--2" data-dropdown-tab='true'>
          <span class="select__text" aria-hidden="true"><%= selectionDetails[0].selectionName%></span>
          <span class="ico-dropdown" aria-hidden="true"></span>
          <select id="multi-select" name="multi-select">
            <% _.each(selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
            <% if(selectionDetailIdx === 0) { %>
              <option value="<%= selectionDetail.selectionName%>" selected="selected"><%= selectionDetail.selectionName%></option>
            <% } else { %>
               <option value="<%= selectionDetail.selectionName%>"><%= selectionDetail.selectionName%></option>
            <% } %>
          <% }); %>
          </select>
        </div>
      </div>
    <% } else if(!isOnetab) { %>
      <ul role="tablist" class="tab seat-deck-tabs main--tabs">
        <% _.each(selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
          <% if(selectionDetailIdx === 0) { %>
            <li role="tab" aria-selected="true" class="tab-item active"><a href="#" tabindex="0"><%= selectionDetail.selectionName%></a></li>
          <% } else { %>
             <li role="tab" class="tab-item"><a href="#" tabindex="-1"><%= selectionDetail.selectionName%></a></li>
          <% } %>
        <% }); %>
<!--         <% if(isMoretab) { %>
          <li role="presentation" class="more-item "><a href="#" role="tab" aria-controls="section-more">More<em class="ico-dropdown"></em></a></li>
        <% } %> -->
      </ul>
      <% } %>

      <div class="tab-wrapper-1">
        <% _.each(selectionDetails, function(selectionDetail, selectionDetailIdx) { %>
          <%
              var getIcon = function(icon){
                switch(icon){
                  case 'ICP':
                    return 'ico-4-salmon';
                    break;
                  case 'DWH':
                    return 'ico-4-fork-1';
                    break;
                  case 'WHS':
                    return 'ico-4-fork';
                    break;
                  case 'EPG':
                    return 'ico-4-amet';
                    break;
                  case 'PLF':
                    return 'ico-4-cook';
                    break;
                  case 'MTL':
                    return 'ico-4-fork';
                    break;
                  case 'VGT':
                    return 'ico-4-leaf';
                    break;
                  case 'LCL':
                    return 'ico-4-heart';
                    break;
                  case 'LCA':
                    return 'ico-4-sandwich';
                    break;
                  case 'LCH':
                    return 'ico-4-fork';
                    break;
                  case 'CNY':
                    return 'ico-4-info';
                    break;
                  case 'XMAS':
                    return 'ico-4-pine';
                    break;
                  case 'DEP':
                    return 'ico-4-coffee';
                    break;
                  case 'TWG':
                    return 'ico-4-fork';
                    break;
                  default:
                    return '';
                }
              };
              var itemInfoList = [],
                  itemInfoListTmp = [],
                  hasLegend = false;

              if(_.isArray(selectionDetail.itemCategoryInfo)) {

                _.each(selectionDetail.itemCategoryInfo, function(itemCategoryInfo, itemCategoryInfoIdx){

                  var itemInfoListTmp1 = [];
                  if(_.isArray(itemCategoryInfo.itemInfo)) {

                    _.each(itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx){
                      if(itemInfo.specialityInfo.dishIconId) {
                        itemInfoListTmp1.push(itemInfo);
                      }
                    });
                    itemInfoListTmp = itemInfoListTmp.concat(itemInfoListTmp1);
                  } else {
                    if(itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {
                      itemInfoListTmp.push(itemCategoryInfo.itemInfo);

                    }

                  }
                });
                itemInfoList = _.uniq(itemInfoListTmp, function(info) {
                  if(info.specialityInfo.dishIconId) {
                    return info.specialityInfo.dishIconId;
                  }
                });

              } else {
                var itemInfoListTmp2 = [];
                if(_.isArray(selectionDetail.itemCategoryInfo.itemInfo)) {

                    _.each(selectionDetail.itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx){
                      if(itemInfo.specialityInfo.dishIconId) {
                        itemInfoListTmp2.push(itemInfo);
                      }
                    });
                    itemInfoListTmp = itemInfoListTmp.concat(itemInfoListTmp2);
                  } else {
                    if(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {
                      itemInfoListTmp.push(selectionDetail.itemCategoryInfo.itemInfo);

                    }

                  }
                itemInfoList = _.uniq(itemInfoListTmp, function(info) {
                  if(info.specialityInfo.dishIconId) {
                    return info.specialityInfo.dishIconId;
                  }
                });
              }

              hasLegend = itemInfoList.length > 0 ? true : false;
            %>
          <% if(selectionDetailIdx === 0) { %>
            <div class="tab-content-1 active">
          <% } else { %>
            <div class="tab-content-1">
          <% } %>
            <% if(hasLegend) { %>
              <div class="col-main">
            <% } else { %>
              <div class="col-main no-legend">
            <% } %>
                <% if(selectionDetail.selectionDescription) { %>
                  <p class="name-customer"><%= selectionDetail.selectionDescription %></p>
                <% } %>
                <% if(_.isArray(selectionDetail.itemCategoryInfo)) {%>
                  <% _.each(selectionDetail.itemCategoryInfo, function(itemCategoryInfo, itemCategoryInfoIdx) { %>

                    <p class="title-main"><%=itemCategoryInfo.itemCategory %></p>
                    <ul class="list-meals">
                      <% if(_.isArray(itemCategoryInfo.itemInfo)) {%>
                        <% _.each(itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx) { %>
                          <li class="<%= itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                            <% if(itemInfo.itemMenuScriptHeader) {%>
                            <span>
                              <% if(itemInfo.specialityInfo.dishIconId) {%>
                                <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= itemInfo.itemMenuScriptHeader%>
                            </span>
                            <% } %>
                            <% if(itemInfo.itemMenuScriptwriteup) {%>
                              <p class="desc">
                              <% if(!itemInfo.itemMenuScriptHeader && itemInfo.specialityInfo && itemInfo.specialityInfo.dishIconId) {%>
                              - <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= getItemWriteup(itemInfo.itemMenuScriptwriteup)%>
                            </p>
                            <% } %>
                          </li>
                        <% }); %>
                      <% } else {%>
                         <li class="<%= itemCategoryInfo.itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>" >
                            <span>
                            <em class="<%= getIcon(itemCategoryInfo.itemInfo.specialityInfo.dishIconId) %>"></em><%= itemCategoryInfo.itemInfo.itemMenuScriptHeader%></span>
                            <% if(itemCategoryInfo.itemInfo.itemMenuScriptwriteup) {%>
                                <p class="desc"><%= getItemWriteup(itemCategoryInfo.itemInfo.itemMenuScriptwriteup)%></p>
                            <% } %>
                          </li>
                      <% } %>
                    </ul>
                  <% }); %>
                <% } else {%>
                  <p class="title-main"><%=selectionDetail.itemCategoryInfo.itemCategory %></p>
                  <ul class="list-meals">
                    <% if(_.isArray(selectionDetail.itemCategoryInfo.itemInfo)) {%>
                      <% _.each(selectionDetail.itemCategoryInfo.itemInfo, function(itemInfo, itemInfoIdx) { %>
                        <li class="<%= itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                          <% if(itemInfo.itemMenuScriptHeader) {%>
                            <span>
                              <% if(itemInfo.specialityInfo.dishIconId) {%>
                                <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= itemInfo.itemMenuScriptHeader%>
                            </span>
                          <% } %>
                          <% if(itemInfo.itemMenuScriptwriteup) {%>
                            <p class="desc">
                              <% if(!itemInfo.itemMenuScriptHeader && itemInfo.specialityInfo && itemInfo.specialityInfo.dishIconId) {%>
                              -  <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId) %>"></em>
                              <% } %>
                              <%= getItemWriteup(itemInfo.itemMenuScriptwriteup)%>
                            </p>
                          <% } %>
                        </li>
                      <% }); %>
                    <% } else {%>
                      <li class="<%= selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup ? '' : 'none-info' %>">
                        <% if(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptHeader) {%>
                          <span>
                          <% if(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) {%>
                            <em class="<%= getIcon(selectionDetail.itemCategoryInfo.itemInfo.specialityInfo.dishIconId) %>"></em>
                          <% } %>
                          <%= selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptHeader%></span>
                        <% } %>
                        <% if(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup) {%>
                          <p class="desc"><%= getItemWriteup(selectionDetail.itemCategoryInfo.itemInfo.itemMenuScriptwriteup)%></p>
                        <% } %>
                      </li>
                    <% } %>
                  </ul>
                <% } %>
              </div>
              <% if(hasLegend) {%>
                <div class="col-legend">
                  <h3 class="title-main">legend</h3>
                  <ul class="list-meals list-main-legend">
                    <% _.each(itemInfoList, function(itemInfo, itemInfoIdx) { %>
                      <li>
                        <% if(itemInfo.specialityInfo.dishIconId) {%>
                          <em class="<%= getIcon(itemInfo.specialityInfo.dishIconId)%>"></em>
                        <% } %>
                        <% if(itemInfo.specialityInfo.specialityFootNote) { %>
                          <%= itemInfo.specialityInfo.specialityFootNote%>
                        <% } %>
                      </li>
                    <% }); %>
                  </ul>
                </div>
              <% } %>
              <div class="main-note">
                <p>Menu items are subject to availability and change.</p>
                <p>Information is correct as of [date].</p>
              </div>
            </div>
        <% });%>
      </div>
    </div>
  <% } %>
  </div>
</div>
<a href="#" class="popup__close" aria-label="Close button"></a>
