<a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
<h3 class="popup__heading">PPS Club / KrisFlyer member privileges on Economy Class</h3>
<p class="des">As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each Economy Class fare family.</p>
<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">
  <ul class="tab" role="tablist">
    <li class="tab-item active" aria-selected="true" role="tab"><a href="#">PPS Club</a>
    </li>
    <li class="tab-item" role="tab"><a href="#">KrisFlyer Elite Gold</a>
    </li>
    <li class="tab-item" role="tab"><a href="#">KrisFlyer Elite Silver</a>
    </li>
    <li class="tab-item" role="tab"><a href="#">KrisFlyer</a>
    </li>
    <li class="more-item"><a href="#">More<em class="ico-dropdown"></em></a>
    </li>
  </ul>
  <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">
    <label for="multi-select" class="select__label">&nbsp;</label><span class="select__text">Rooms and rates</span><span class="ico-dropdown"></span>
    <select id="multi-select" name="multi-select">
      <option value="0" selected="selected">PPS Club</option>
      <option value="1">KrisFlyer Elite Gold</option>
      <option value="2">KrisFlyer Elite Silver</option>
      <option value="3">KrisFlyer</option>
    </select>
  </div>
  <div class="tab-wrapper">
    <div class="tab-content active", role="tabpanel">
      <div class="tab-heading">
        PPS Club member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Economy Promo</th>
            <th>Economy Good</th>
            <th>Economy Better</th>
            <th>Economy Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-business-1"></em>
              <span>Baggage*</span>
            </td>
            <td>
              <div class="data-title">Economy Promo</div><span>Additional 30kg</span>
            </td>
            <td>
              <div class="data-title">Economy Good</div><span>Additional 30kg</span>
            </td>
            <td>
              <div class="data-title">Economy Better</div><span>Additional 30kg</span>
            </td>
            <td>
              <div class="data-title">Economy Best</div><span>Additional 30kg</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Standard seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Economy Promo</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Premium seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Economy Promo</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-preferred"></em>
              <span>Forward Zone selection at booking</span>
            </td>
            <td>
              <div class="data-title">Economy Promo</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Economy Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Economy Promo</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Economy Good</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Economy Better</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Economy Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="tab-content" role="tabpanel">
      <div class="tab-heading">
        KrisFlyer Elite Gold member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Promo</th>
            <th>Good</th>
            <th>Better</th>
            <th>Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-business-1"></em>
              <span>Baggage*</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Additional 20kg</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Additional 20kg</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Additional 20kg</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Additional 20kg</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Standard seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Premium seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-preferred"></em>
              <span>Forward Zone selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="tab-content" role="tabpanel">
      <div class="tab-heading">
        KrisFlyer Elite Silver member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Promo</th>
            <th>Good</th>
            <th>Better</th>
            <th>Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Standard seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Premium seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-preferred"></em>
              <span>Forward Zone selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-icons-42"></em>
              <span>Earn KrisFlyer miles</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Additional 25%</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Additional 25%</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="tab-content" role="tabpanel">
      <div class="tab-heading">
        KrisFlyer member benefits
      </div>
      <table class="table-1 table-responsive">
        <thead class="hidden-mb-small">
          <tr>
            <th>Fare condition</th>
            <th>Promo</th>
            <th>Good</th>
            <th>Better</th>
            <th>Best</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Standard seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Complimentary^</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr class="even">
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-change-seat"></em>
              <span>Premium seat selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
          <tr>
            <td>
              <div class="data-title">Fare condition</div>
              <em class="ico-preferred"></em>
              <span>Forward Zone selection at booking</span>
            </td>
            <td>
              <div class="data-title">Promo</div><span>Not allowed</span>
            </td>
            <td>
              <div class="data-title">Good</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Better</div><span>Discounted</span>
            </td>
            <td>
              <div class="data-title">Best</div><span>Complimentary^</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="text-content">
      <div class="title">Terms and conditions</div>
      <div class="text-heading">Complimentary seat selection privileges for expired memberships</div>
      <div class="desc-1">If your PPS Club / KrisFlyer membership expires on or before the date of seat selection during booking, your complimentary seat selection privileges will remain unchanged.</div>
      <div class="text-heading">Complimentary additional baggage privileges for expired memberships</div>
      <div class="desc-1">If your PPS Club or KrisFlyer Elite Gold membership expires on or before the counter check-In date, you and your travelling party will no longer enjoy that exprired tier's complimentary baggage allowance.</div>
    </div>
  </div>
</div>
