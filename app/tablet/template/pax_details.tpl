<% if(data) { %>
	<% var i = 0, j = 0; %>
	<% _.each(data.passengers, function(passengers, idx){ %>
		<%
			 var passengerType;
			 if(passengers.passengerType == 'A'){
					passengerType = "- Adult";
			 } else if(passengers.passengerType == 'I'){
			 		passengerType = "- INFANT";
			 } else if(passengers.passengerType == 'C'){
			 		passengerType = "- CHILD";
			 }
		%>
		<% if(passengers.passengerType !== 'I') { %>
			<a href="#" class="booking-nav__item <% if(passengers.activePassenger == true) {%> active <% } %> <% if(passengers.passengerSelected == true) {%> passenger-selected-true <% } %>" role="tab" data-idx = "<%- i %>" data-passengerID="<%- passengers.passengerID %>">
				<span class="passenger-info"><span class="passenger-info__number"></span>
				<span class="passenger-info__text"><%- passengers.firstName %> <%- passengers.lastName %> <%- passengerType %> <br> <span class="passenger-type-infant"></span></span><em class="ico-point-r"></em><em class="ico-check-thick"></em>
				<input type="hidden" name="passenger-info-1"></span>
			</a>
			<% i = i + 1; %>
		<% } %>
	<% }); %>
	<select name="sidebar-tab-select" id="sidebar-tab-select" class="tab-select">
		<% _.each(data.passengers, function(passengersMb, idxPassengers){ %>
			<%
				 var passengerType;
				 if(passengersMb.passengerType == 'A'){
						passengerType = "- Adult";
				 } else if(passengersMb.passengerType == 'I'){
				 		passengerType = "- INFANT";
				 } else if(passengersMb.passengerType == 'C'){
				 		passengerType = "- CHILD";
				 } 
			%>
			<% if(passengersMb.passengerType !== 'I') { %>
				<option data-passengerID="<%- passengersMb.passengerID %>" value="" <% if(passengersMb.activePassenger == true) {%> selected="selected" <% } %> class=""><%- passengersMb.firstName %> <%- passengersMb.lastName %> <%- passengerType %>
				</option>
				<% j = j + 1; %>
			<% } %>
		<% }); %>
	</select>
<% } %>
