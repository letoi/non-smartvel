<% if(data) { %>
  <div class="bsp-booking-summary__heading">
    <div class="bsp-booking-summary__heading-wrap">
      <% if(data.flight.length > 2) { %>
        <% _.each(data.flight, function(itemFly, idx){ %>
          <span class="bsp-flight hidden-mb-small hidden-tb"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
        <% }) %>
        <% _.each(data.flight.slice(0 , 4), function(itemFly, idx4){ %>
          <span class="bsp-flight hidden-mb-small hidden-dt"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
        <% }) %>
        <span class="plus-more-flight hidden-mb-small hidden-dt">
          <% if(data.flight.length > 4) { %>
            <% var numberTablet = data.flight.length - 4 %>
          + <%- numberTablet %> more flights</span>
          <% } %>
        <span class="plus-more-flight hidden-tb-dt"><% var numberApplyMobile = data.flight.length %> <%- numberApplyMobile %> more flights</span><span class="adults">· <%- data.adultCount %> Adults</span><a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
      <% } else { %>
        <% if(data.flight.length < 2) { %>
            <span class="bsp-flight"><%- data.flight[0].originAirportCode %> - <%- data.flight[0].destinationAirportCode %></span>
            <% if(data.adultCount) { %>
              <span class="adult"><%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %></span>
            <% } %>
            <a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
        <% } else { %>
          <% if(data.flight.length = 2) { %>
            <span class="bsp-flight hidden-mb-small">
            <%- !_.isEmpty(dataDepart) ? dataDepart.data[0].originHour.split(' ')[0] : data.flight[0].originAirportCode %> -
            <%- !_.isEmpty(dataDepart) ? dataDepart.data[dataDepart.data.length - 1].destinationHour.split(' ')[0] : data.flight[0].destinationAirportCode %>
            <em class="ico-return-arrows"></em>
            <%- !_.isEmpty(dataReturn) ? dataReturn.data[0].originHour.split(' ')[0] : data.flight[1].originAirportCode %> -
            <%- !_.isEmpty(dataReturn) ? dataReturn.data[dataReturn.data.length - 1].destinationHour.split(' ')[0] : data.flight[1].destinationAirportCode %>
            </span>
            <span class="bsp-flight hidden-tb-dt">
            <%- !_.isEmpty(dataDepart) ? dataDepart.data[0].originHour.split(' ')[0] : data.flight[0].originAirportCode %> -
            <%- !_.isEmpty(dataDepart) ? dataDepart.data[dataDepart.data.length - 1].destinationHour.split(' ')[0] : data.flight[0].destinationAirportCode %>
            <em class="ico-return-arrows"></em>
            <%- !_.isEmpty(dataReturn) ? dataReturn.data[0].originHour.split(' ')[0] : data.flight[1].originAirportCode %> -
            <%- !_.isEmpty(dataReturn) ? dataReturn.data[dataReturn.data.length - 1].destinationHour.split(' ')[0] : data.flight[1].destinationAirportCode %>
            </span>
            <span class="adults">· <%- data.adultCount %> Adults</span><a href="#" class="search-link trigger-popup-edit-search"><em class="ico-edit"><span class="ui-helper-hidden-accessible"></span></em>Edit search</a>
          <% } %>
        <% } %>
      <% } %>
    </div>
  </div>
  <div class="bsp-booking-summary__content bsp-booking-summary__content-1 all-transition">
    <div class="bsp-booking-summary__content-wrap <% if(data.flight.length < 2) { %> one-way <% } %> <%- _.isEmpty(dataDepart) && 'none-select' %>">
      <div class="bsp-booking-summary-generality <% if(data.flight.length >= 2 ) { %> multi-city <% } %> <%- (!_.isEmpty(dataDepart) || !_.isEmpty(dataReturn)) ? 'hidden' : '' %>">
        <div class="bsp-booking-summary--group none-line-flight">
          <% if(data.flight.length >= 2) { %>
            <% _.each(data.flight, function(itemFly, idx){ %>
              <div class="bsp-flights__info--group <% if(idx % 2 == 0 && data.flight.length >= 2) {%> position-even <% } %>">
                <div class="bsp-flights__info--inner"><span class="flight-date-title">
                  <% if(data.flight.length < 2) { %>
                    One way
                  <% } %>
                  <% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
                    <% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
                  <% } %>
                  <% if(data.flight.length > 2) { %>
                    Flight <%- idx + 1 %>
                  <% } %></span>
                   <div class="flight-station-info">
                      <div class="station-stop">
                        <span class="station-stop-detail">
                          <em class="ico-airplane-2"></em>
                        </span>
                      </div>
                      <div class="flights-station__info--detail">
                        <span class="time"><%- itemFly.flightSegments[0].deparure.date %></span>
                        <span class="hour hidden-mb-small"><%- itemFly.origin %> (<%- itemFly.originAirportCode %>) </span>
                        <span class="hour hidden-tb-dt"><%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
                      </div>
                      <div class="flights-station__info--detail return-flight hidden-mb-small">
                        <span class="time"><%- itemFly.flightSegments[0].arrival.date %></span>
                        <span class="hour"><%- itemFly.destination %> (<%- itemFly.destinationAirportCode %>)</span>
                      </div>
                    </div>
                </div>
              </div>
            <% }) %>
          <% } else { %>
              <% if(data.flight.length < 2) { %>
                <div class="bsp-flights__info--group one-way flight-result-leg-wrap all-transition">
                <div class="bsp-flights__info--inner"><span class="flight-date-title">One way</span>
                  <div class="flight-station-info">
                    <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em></span></div>
                    <div class="flights-station__info--detail"><span class="time"><%- data.flight[0].flightSegments[0].deparure.date %></span><span class="hour"><%- data.flight[0].origin %> (<%- data.flight[0].originAirportCode %>) </span></div>
                    <div class="flights-station__info--detail return-flight"><span class="time"><%- data.flight[0].flightSegments[0].arrival.date %></span><span class="hour"><%- data.flight[0].destination %> (<%- data.flight[0].destinationAirportCode %>)</span></div>
                  </div>
                </div>
              </div>
              <% } %>
          <% } %>
        </div>
      </div>
      <div class="bsp-booking-summary__content-control <%- (!_.isEmpty(dataDepart) || !_.isEmpty(dataReturn)) ? '' : 'hidden' %>">
        <div class="bsp-total-fare">
          <div class="total-fare--inner"><span class="flight-date-title">Total fare</span>
            <div class="flights__info">
              <% if(data.adultCount) { %>
                <span class="number-passengers" data-travel-party="<%- data.childCount > 0 ? true : false %>">
                <%= data.adultCount %> Adult<%= data.adultCount > 1 ? 's' : '' %>
                <% } %>
              </span>
              <%
                var priceSeat = 0;
                var numberPass = data.adultCount + data.childCount + data.infantCount;
                if(priceData && typeof priceData != 'undefined') {
                  _.map(priceData, function(price){
                    priceSeat += price;
                  })
                }
               %>
              <p class="total-cost" data-total-fare="<%- !_.isEmpty(dataReturn) ? (parseFloat(dataDepart.price) + parseFloat(dataReturn.price)) * numberPass + parseFloat(priceSeat) : 0 %>">
                <span class="unit">
                  <%- data.currency %>
                    <span class="amount">
                    <% if(_.isEmpty(dataDepart)) { %>
                      <%- data.costPayableByCash.toLocaleString(undefined, { minimumFractionDigits: 2 }) %>
                    <% } else { %>
                      <% if(!_.isEmpty(dataReturn)) { %>
                        <%- ((parseFloat(dataDepart.price) + parseFloat(dataReturn.price)) * numberPass + parseFloat(priceSeat)).toLocaleString(undefined, { minimumFractionDigits: 2 }) %>
                      <% } else { %>
                        <%- (dataDepart.price * numberPass).toLocaleString(undefined, { minimumFractionDigits: 2 }) %>
                      <% } %>
                    <% } %>
                    </span>
                  </span>
                </p>
              <span class="fare-notice">Total fare includes discounts, taxes and surcharges</span>
            </div>
            <%
              var fare = data.fareTotal.toFixed(2);
              var tax = data.taxTotal.toFixed(2);
              if(!_.isEmpty(dataReturn)) {
                fare = dataReturn.fare ? dataReturn.fare.toFixed(2) : fare;
                tax = dataReturn.tax ? dataReturn.tax.toFixed(2) : tax;
              } else if(!_.isEmpty(dataDepart)) {
                fare = dataDepart.fare ? dataDepart.fare.toFixed(2) : fare;
                tax = dataDepart.tax ? dataDepart.tax.toFixed(2) : tax;
              }
            %>
            <div class="bsp-flights-cost"><span class="bsp-flights-cost-title"><span class="text-left">FLIGHTS</span><span class="text-right"><%- data.currency %></span></span>
              <ul class="bsp-flights-cost__details">
                <li><span>Fare</span><span class="price"><%- fare %></span></li>
                <li><span>Airport/Government taxes</span><span class="price"><%- tax %></span></li>
                <li><span>Carrier surcharges</span><span class="price">
                <%- data.surchargeTotal.toFixed(2) %> </span></li>
                <li class="sub-total"><span>Subtotal</span><span class="price"><% if(_.isEmpty(dataDepart)) { %><%- data.costPayableByCash.toLocaleString(undefined, { minimumFractionDigits: 2 }) %><% } else { %><% if(!_.isEmpty(dataReturn)) { %><%- ((parseFloat(dataDepart.price) + parseFloat(dataReturn.price)) * numberPass + parseFloat(priceSeat)).toLocaleString(undefined, { minimumFractionDigits: 2 }) %><% } else { %><%- (dataDepart.price * numberPass).toLocaleString(undefined, { minimumFractionDigits: 2 }) %><% } %><% } %></span></li>
              </ul>
            </div>
            <div class="cta-group">
              <li><a href="#" class="search-link trigger-popup-flights-details" data-trigger-popup=".popup--flights-details"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Cost breakdown by passengers</a>
              </li>
              <li><a href="#" class="search-link trigger-popup-add-ons-baggage" data-trigger-popup=".popup--add-ons-summary"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Full fare condition</a>
              </li>
              <li><a href="#" class="search-link trigger-popup-add-ons-summary" data-trigger-popup=".popup--add-ons-baggage"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Baggage allowance</a>
              </li>
            </div><a href="#" class="link-4 more-detail" data-more-details="true">More details<em class="ico-point-d"><span class="ui-helper-hidden-accessible"></span></em></a><a href="#" class="link-4 less-detail hidden-mb-small " data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a>
          </div>
        </div>
        <div class="bsp-booking-summary--group <% if(data.flight.length >= 2) { %> multi-city <% } else { %> one-way <% } %> hidden-mb-small" data-info>
            <% if(data.flight.length >= 2) { %>
              <% _.each(data.flight, function(itemFly, idx){ %>
                <div class="bsp-flights__info--group <% if(idx % 2 == 0) {%> position-even <% } %> <% if(idx >= 2) {%> hidden <% } %> flight-result-leg-wrap all-transition">
                  <div class="bsp-flights__info--inner"><span class="flight-date-title">
                  <% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
                    <% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
                  <% } %>
                  <% if(data.flight.length > 2) { %>
                    Flight <%- idx + 1 %>
                  <% } %>
                  </span>
                    <% if(idx === 0) { %>
                      <% if(!_.isEmpty(dataDepart)) { %>
                        <div class="flight-station-info">
                          <div class="station-stop">
                            <span class="station-stop-detail">
                              <em class="ico-airplane-2"></em>
                              <span class="time"><% dataDepart.totalTime.split('ins')[0] %></span>
                            </span>
                          </div>
                          <div class="flights-station__info--detail">
                            <span class="time"><%- dataDepart.data[0].originDate %></span>
                            <span class="hour"><%- dataDepart.data[0].originHour %></span>
                            <span class="country-name"><%- dataDepart.data[0].originCountryname %></span>
                          </div>
                          <div class="flights-station__info--detail return-flight">
                            <span class="time"><%- dataDepart.data[dataDepart.data.length - 1].destinationDate %></span>
                            <span class="hour"><%- dataDepart.data[dataDepart.data.length - 1].destinationHour %></span>
                            <span class="country-name"><%- dataDepart.data[dataDepart.data.length - 1].destinationCountryname %></span>
                          </div>
                        </div>
                      <% }  else { %>
                          <% if(!_.isEmpty(dataReturn)) { %>
                            <input type="button" data-select-outbound name="select-outbound" id="select-inbound" value="Select your outbound flight" class="btn-scroll btn-1">
                          <% } else { %>
                            <div class="flight-station-info">
                              <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time">15h 15m</span></span></div>
                              <div class="flights-station__info--detail"><span class="time"><%- itemFly.flightSegments[0].deparure.date %></span><span class="hour"><%- itemFly.originAirportCode %> <%- itemFly.flightSegments[0].deparure.time %></span><span class="country-name"><%- itemFly.origin %></span></div>
                              <div class="flights-station__info--detail return-flight"><span class="time"><%- itemFly.flightSegments[0].arrival.date %></span><span class="hour"><%- itemFly.destinationAirportCode %> <%- itemFly.flightSegments[0].arrival.time %></span><span class="country-name"><%- itemFly.destination %></span></div>
                          </div>
                          <% } %>
                      <% } %>
                    <% } else if(idx === 1) { %>
                      <% if(!_.isEmpty(dataReturn)) { %>
                        <div class="flight-station-info">
                          <div class="station-stop">
                            <span class="station-stop-detail">
                              <em class="ico-airplane-2"></em>
                              <span class="time"><% dataReturn.totalTime.split('ins')[0] %></span>
                            </span>
                          </div>
                          <div class="flights-station__info--detail">
                            <span class="time"><%- dataReturn.data[0].originDate %></span>
                            <span class="hour"><%- dataReturn.data[0].originHour %></span>
                            <span class="country-name"><%- dataReturn.data[0].originCountryname %></span>
                          </div>
                          <div class="flights-station__info--detail return-flight">
                            <span class="time"><%- dataReturn.data[dataReturn.data.length - 1].destinationDate %></span>
                            <span class="hour"><%- dataReturn.data[dataReturn.data.length - 1].destinationHour %></span>
                            <span class="country-name"><%- dataReturn.data[dataReturn.data.length - 1].destinationCountryname %></span>
                          </div>
                        </div>
                      <% }  else { %>
                          <% if(!_.isEmpty(dataDepart)) { %>
                            <input type="button" data-select-inbound name="select-inbound" id="select-inbound" value="Select your inbound flight" class="btn-scroll btn-1">
                          <% } else { %>
                            <div class="flight-station-info">
                              <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time">15h 15m</span></span></div>
                              <div class="flights-station__info--detail"><span class="time"><%- itemFly.flightSegments[0].deparure.date %></span><span class="hour"><%- itemFly.originAirportCode %> <%- itemFly.flightSegments[0].deparure.time %></span><span class="country-name"><%- itemFly.origin %></span></div>
                              <div class="flights-station__info--detail return-flight"><span class="time"><%- itemFly.flightSegments[0].arrival.date %></span><span class="hour"><%- itemFly.destinationAirportCode %> <%- itemFly.flightSegments[0].arrival.time %></span><span class="country-name"><%- itemFly.destination %></span></div>
                            </div>
                          <% } %>
                      <% } %>
                    <% } %>
                  </div>
                </div>
                <% }) %>
              <% } else { %>
                  <% if(data.flight.length < 2) { %>
                    <div class="bsp-flights__info--group one-way flight-result-leg-wrap all-transition">
                    <div class="bsp-flights__info--inner"><span class="flight-date-title">One way</span>
                      <div class="flights__info">
                        <div class="bsp-flights__info--detail"><span class="time">
                            <%- data.flight[0].flightSegments[0].deparure.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- data.flight[0].originAirportCode %> <%- data.flight[0].flightSegments[0].deparure.time %></span><span class="country-name"><%- data.flight[0].origin %></span>
                        </div>
                        <div class="bsp-flights__info--detail"><span class="time">
                            <%- data.flight[0].flightSegments[0].arrival.date %></span><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%- data.flight[0].destinationAirportCode %> <%- data.flight[0].flightSegments[0].arrival.time %></span><span class="country-name"><%- data.flight[0].destination %></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <% } %>
              <% } %>
          <% if(data.flight.length > 2) { %>
            <% var numberLength = data.flight.length - 2 %>
            <div class="plus-more-detail"><a href="#" class="link-4 hidden-mb-small"><span class="text">+ <%- numberLength %> more flights</span></a>
            </div>
          <% } %>
        </div>
        <div class="bsp-booking-summary__content-detail hidden " data-more-content>
          <div class="bsp-booking-summary--group <% if(data.flight.length > 2) { %> multi-city <% } %><% if(data.flight.length >= 2) { %> return <% } %>">
            <% _.each(data.flight, function(itemFly, idx){ %>
              <div class="bsp-flights__info--group <% if(idx % 2 == 0 && data.flight.length >= 2) {%> position-even <% } %> flight-result-leg-wrap all-transition">
                <div class="bsp-flights__info--inner"><span class="flight-date-title">
                <% if(data.flight.length < 2) { %>
                  One way
                <% } %>
                <% if(data.flight.length < 3 && data.flight.length > 1 ) { %>
                  <% if(idx % 2 == 0 ) {%> Departing <% } else { %> Returning <% } %>
                <% } %>
                <% if(data.flight.length > 2) { %>
                  Flight <%- idx + 1 %>
                <% } %>
                  </span>
                  <% if(idx === 0) { %>
                    <% if(!_.isEmpty(dataDepart)) { %>
                        <% _.each(dataDepart.data, function(item, itemidx){ %>
                          <div class="flight-station-info">
                            <div class="station-stop">
                          		<% if(item.operationName.toLowerCase() == 'accesrail') { %>
                        				<span class="station-stop-detail rail-fly">
                                	<em class="ico-rail rail-middle"></em>
	                                <span class="time"><% item.timeFlight %></span>
	                              </span>
                          		<% } else { %>
                          			<span class="station-stop-detail">
	                              	<em class="ico-airplane-2"></em>
	                                <span class="time"><% item.timeFlight %></span>
	                              </span>
                          		<% } %>
                            </div>
                            <div class="flights-station__info--detail">
                              <span class="time"><%- item.originDate %></span>
                              <span class="hour"><%- item.originHour %></span>
                              <span class="country-name"><%- item.originCountryname %></span>
                              <span class="date"></span>
                                <%- item.originAirportname %>
                                <br>
                                <% if(item.originterminal) { %>
                                  Terminal <%= item.originterminal %>
                                <% } %>
                            </div>
                            <div class="flights-station__info--detail return-flight">
                              <span class="time"><%- item.destinationDate %></span>
                              <span class="hour"><%- item.destinationHour %></span>
                              <span class="country-name"><%- item.destinationCountryname %></span>
                              <span class="date"></span>
                                <%- item.destinationAirportname %>
                                <br>
                                <% if(item.destinationTerminal) { %>
                                  Terminal <%= item.destinationTerminal %>
                                <% } %>
                            </div>
                          </div>
                          <p class="flying-details">
                            <strong><%- item.operationName %></strong>
                            <span class="hidden-tb show-mb-inline">·</span>
                            	<% if(item.operationName.toLowerCase() == 'accesrail') { %>
                            		<%
                            			var railPlaneName = item.planeName.split(' ');
                            		%>
                          		 	TRAIN <%- item.flightNumber %> <%- railPlaneName[1] %>
                            	<% } else { %>
                            		<%- item.flightNumber %> · <%- item.planeName %>
                            	<% } %>

                             <% if(item.operationName.toLowerCase() != 'accesrail') { %>
                             	<span class="economy"><%= item.headerClass %></span>
                             <% } %>
                           </p>
                          <% if(item.layover) { %>
                          	<% if (item.operationName.toLowerCase() == 'accesrail') { %>
                          		<div class="rail-fly">
	                          		<span class="layover-time--2 rail-layover">
								                  <em class="ico-flight-history"></em>Layover time: <%- item.layover %>
								                </span>
							                </div>
                          	<% } else { %>
                          		<span class="layover-time--1">
	                              <em class="ico-flight-history"></em>
	                              Layover time: <%- item.layover %>
	                            </span>
                          	<% } %>
                          <% } %>
                        <% }) %>
                        <div class="flights__info border"><span>Total travel time: <%- dataDepart.totalTime %></span></div>
                    <% }  else { %>
                       <% if(!_.isEmpty(dataReturn)) { %>
                            <input type="button" data-select-outbound name="select-inbound" id="select-outbound" value="Select your outbound flight" class="btn-scroll btn-1">
                        <% }  else { %>
                            <% _.each(itemFly.flightSegments, function(itemSegments, idx1){ %>
                              <div class="flight-station-info">
                                <div class="station-stop">
                                  <span class="station-stop-detail">
                                    <em class="ico-airplane-2"></em>
                                    <span class="time">15h 15m</span>
                                  </span>
                                </div>
                                <div class="flights-station__info--detail">
                                  <span class="time"><%- itemSegments.deparure.date %></span>
                                  <span class="hour"><%- itemFly.originAirportCode %> <%- itemSegments.deparure.time %></span>
                                  <span class="country-name"><%- itemFly.origin %></span>
                                  <span class="date"></span><%- itemSegments.deparure.airportName %><br><% if(itemSegments.deparure.terminal !== "" && itemSegments.deparure.terminal) { %> Terminal <% } %> <%- itemSegments.deparure.terminal %>
                                </div>
                                <div class="flights-station__info--detail return-flight">
                                  <span class="time"><%- itemSegments.arrival.date %></span>
                                  <span class="hour"><%- itemFly.destinationAirportCode %> <%- itemSegments.arrival.time %></span>
                                  <span class="country-name"><%- itemFly.destination %></span>
                                  <span class="date"></span><%- itemSegments.arrival.airportName %><br><% if(itemSegments.arrival.terminal !== "" && itemSegments.arrival.terminal) { %> Terminal <% } %> <%- itemSegments.arrival.terminal %>
                                </div>
                              </div>
                              <p class="flying-details">
                                <strong><%- itemSegments.carrierName %></strong>
                                <span class="hidden-tb show-mb-inline">·</span>
                                 <%- itemSegments.carrierCode %> <%- itemSegments.flightNumber %> · <%- itemSegments.airCraftType %>
                                 <span class="economy"><%- itemSegments.cabinClassDesc %></span>
                               </p>
                              <% if(itemSegments.layoverTime) { %>
                                <span class="layover-time--1">
                                  <em class="ico-flight-history"></em>
                                  Layover time: <%- itemSegments.layoverTime %>
                                </span>
                              <% } %>
                            <% }) %>
                            <div class="flights__info border"><span>Total travel time: <%- itemFly.totalTravelTime %></span></div>
                        <% } %>
                    <% } %>
                  <% } else if(idx === 1) { %>
                    <% if(!_.isEmpty(dataReturn)) { %>
                        <% _.each(dataReturn.data, function(item, itemidx){ %>
                          <div class="flight-station-info">
                            <div class="station-stop">
                              <% if(item.operationName.toLowerCase() == 'accesrail') { %>
                        				<span class="station-stop-detail rail-fly">
                                	<em class="ico-rail rail-middle"></em>
	                                <span class="time"><% item.timeFlight %></span>
	                              </span>
                          		<% } else { %>
                          			<span class="station-stop-detail">
	                              	<em class="ico-airplane-2"></em>
	                                <span class="time"><% item.timeFlight %></span>
	                              </span>
                          		<% } %>
                            </div>
                            <div class="flights-station__info--detail">
                              <span class="time"><%- item.originDate %></span>
                              <span class="hour"><%- item.originHour %></span>
                              <span class="country-name"><%- item.originCountryname %></span>
                              <span class="date"></span>
                                <%- item.originAirportname %>
                                <br>
                                <% if(item.originterminal) { %>
                                  Terminal <%= item.originterminal %>
                                <% } %>
                            </div>
                            <div class="flights-station__info--detail return-flight">
                              <span class="time"><%- item.destinationDate %></span>
                              <span class="hour"><%- item.destinationHour %></span>
                              <span class="country-name"><%- item.destinationCountryname %></span>
                              <span class="date"></span>
                                <%- item.destinationAirportname %>
                                <br>
                                <% if(item.destinationTerminal) { %>
                                  Terminal <%= item.destinationTerminal %>
                                <% } %>
                            </div>
                          </div>
                         	<p class="flying-details">
                            <strong><%- item.operationName %></strong>
                            <span class="hidden-tb show-mb-inline">·</span>
                            	<% if(item.operationName.toLowerCase() == 'accesrail') { %>
                            		<%
                            			var railPlaneName = item.planeName.split(' ');
                            		%>
                          		 	TRAIN <%- item.flightNumber %> <%- railPlaneName[1] %>
                            	<% } else { %>
                            		<%- item.flightNumber %> · <%- item.planeName %>
                            	<% } %>

                             <% if(item.operationName.toLowerCase() != 'accesrail') { %>
                             	<span class="economy"><%= item.headerClass %></span>
                             <% } %>
                           </p>
                          <% if(item.layover) { %>
                          	<% if (item.operationName.toLowerCase() == 'accesrail') { %>
                          		<div class="rail-fly">
	                          		<span class="layover-time--2 rail-layover">
								                  <em class="ico-flight-history"></em>Layover time: <%- item.layover %>
								                </span>
							                </div>
                          	<% } else { %>
                          		<span class="layover-time--1">
	                              <em class="ico-flight-history"></em>
	                              Layover time: <%- item.layover %>
	                            </span>
                          	<% } %>
                          <% } %>
                        <% }) %>
                        <div class="flights__info border"><span>Total travel time: <%- dataReturn.totalTime %></span></div>
                    <% }  else { %>
                        <% if(!_.isEmpty(dataDepart)) { %>
                            <input type="button" data-select-inbound name="select-inbound" id="select-inbound" value="Select your inbound flight" class="btn-scroll btn-1">
                        <% }  else { %>
                            <% _.each(itemFly.flightSegments, function(itemSegments, idx1){ %>
                              <div class="flight-station-info">
                                <div class="station-stop">
                                  <span class="station-stop-detail">
                                    <em class="ico-airplane-2"></em>
                                    <span class="time">15h 15m</span>
                                  </span>
                                </div>
                                <div class="flights-station__info--detail">
                                  <span class="time"><%- itemSegments.deparure.date %></span>
                                  <span class="hour"><%- itemFly.originAirportCode %> <%- itemSegments.deparure.time %></span>
                                  <span class="country-name"><%- itemFly.origin %></span>
                                  <span class="date"></span><%- itemSegments.deparure.airportName %><br><% if(itemSegments.deparure.terminal !== "" && itemSegments.deparure.terminal) { %> Terminal <% } %> <%- itemSegments.deparure.terminal %>
                                </div>
                                <div class="flights-station__info--detail return-flight">
                                  <span class="time"><%- itemSegments.arrival.date %></span>
                                  <span class="hour"><%- itemFly.destinationAirportCode %> <%- itemSegments.arrival.time %></span>
                                  <span class="country-name"><%- itemFly.destination %></span>
                                  <span class="date"></span><%- itemSegments.arrival.airportName %><br><% if(itemSegments.arrival.terminal !== "" && itemSegments.arrival.terminal) { %> Terminal <% } %> <%- itemSegments.arrival.terminal %>
                                </div>
                              </div>
                              <p class="flying-details">
                                <strong><%- itemSegments.carrierName %></strong>
                                <span class="hidden-tb show-mb-inline">·</span>
                                 <%- itemSegments.carrierCode %> <%- itemSegments.flightNumber %> · <%- itemSegments.airCraftType %>
                                 <span class="economy"><%- itemSegments.cabinClassDesc %></span>
                               </p>
                              <% if(itemSegments.layoverTime) { %>
                                <span class="layover-time--1">
                                  <em class="ico-flight-history"></em>
                                  Layover time: <%- itemSegments.layoverTime %>
                                </span>
                              <% } %>
                            <% }) %>
                            <div class="flights__info border"><span>Total travel time: <%- itemFly.totalTravelTime %></span></div>
                        <% } %>
                    <% } %>
                  <% } %>
                </div>
              </div>
            <% }) %>
          </div><div class="edit-search-mb-tb"><a href="#" class="link-4 less-detail hidden-tb-dt" data-less-details="true">Less details<em class="ico-point-u"><span class="ui-helper-hidden-accessible"></span></em></a></div>
        </div>
      </div>
    </div>
  </div>
<% } %>
