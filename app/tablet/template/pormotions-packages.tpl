<% _.each(data, function(packages){ %>
  <div class="flight-item">
    <div class="flight-item__inner">
      <a href="#">
        <img src="<%- packages.srcImage %>" alt="<%- packages.title %>" longdesc="img-desc.html">
        <span class="flight-item__vignette"></span>
        <div class="flight-item__info-1">
          <p class="info-promotions"><%- packages.typeFlight %><span><%- packages.title %></span></p>
        </div></a>
      <div class="flight-item__details">
        <h3 class="packages-heading"><%- packages.duration %> from <%- packages.currency %> <%- packages.price %></h3>
        <p><%- packages.description %></p><a href="#" class="link-4"><em class="ico-point-r"></em>Find out more</a>
      </div>
    </div>
  </div>
<% }) %>