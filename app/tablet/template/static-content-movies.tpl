<% if (data && data.length) { %>
  <% _.each(data, function(movie, idx) { %>
    <article class="ent-item ent-item--2">
      <div class="ent-item__inner">
        <figure class="ent-item__thumb">
          <img src="<%- movie.src %>" alt="<%- movie.name %>" longdesc="<%- movie.longdesc %>">
        </figure>
        <section class="ent-item__content">
          <header class="ent-item__title">
            <strong><%- movie.name %></strong>
          </header>
          <footer class="ent-item__footer">
            <a href="<%- movie.link %>" class="info-link-1">
              <em class="ico-movie"></em>
              <%- movie.country %>
            </a>
          </footer>
        </section>
      </div>
    </article>
  <% }); %>
<% } %>
