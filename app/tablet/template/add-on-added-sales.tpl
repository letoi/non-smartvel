<% if(agodaBooking){ %>
    <div class="add-ons-item add-ons-item-added">
      <div class="hotel-infor">
        <div class="item-content-head">
          <div class="item-col-1"><figure><img src="<%=agodaBooking.hotelImage%>" alt="photo-Hotel" longdesc="img-desc.html"></figure></div>
          <div class="item-col-2">
            <div class="content">
              <h3 data-tabindex="true" class="sub-heading-2--blue head"><%=agodaBooking.hotelName %></h3>
              <ul class="rating-block" data-tabindex="true" aria-label="<%-agodaBooking.starRating%> Star">
                <% if (agodaBooking.starRating === "0.5" || agodaBooking.starRating === 0.5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "1" || agodaBooking.starRating === 1) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "1.5" || agodaBooking.starRating === 1.5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "2" || agodaBooking.starRating === 2) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "2.5" || agodaBooking.starRating === 2.5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "3" || agodaBooking.starRating === 3) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "3.5" || agodaBooking.starRating === 3.5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "4" || agodaBooking.starRating === 4) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "4.5" || agodaBooking.starRating === 4.5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } else if (agodaBooking.starRating === "5" || agodaBooking.starRating === 5) { %>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                  <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                  </li>
                <% } %>
              </ul>
              <div data-tabindex="true" class="desc">
                <p><%=agodaBooking.addresLineOne%>, <%=agodaBooking.city%>, <%=agodaBooking.country%>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="add-ons-item__content item-row">
        <figure class="img-block"><img src="images/agoda-added.jpg" alt="photo-car" longdesc="img-desc.html">
        </figure>
        <ul class="info-details-3">
          <li><span>Check-in date:</span><%= checkinDate %></li>
          <li><span>Check-out date:</span><%= checkoutDate %></li>
          <li>
            <span>Guests:</span><%= guest %>
          </li>
          <li><span>Room types:</span><%= roomType %></li>
          <li><span>No. of nights:</span><%=agodaBooking.numberOfNights%></li>
          <li><span>Breakfast:</span>Included for all rooms</li>
        </ul>
      </div>
    </div>
<% } %>
