<%
if(data) {
    segmentsGroup = {};
    _.each(data.packPaxAssociations, function(pax1Data, pax1Idx) {
        _.each(pax1Data.segments, function(segments1Data, segments1Idx) {
              segmentsGroup[segments1Data.segmentId] = segments1Data;
        });
    });

    var getPackData = function(packId) {
        for (var i = 0; i < data.packPaxAssociations.length; i++) {
            var segment = data.packPaxAssociations[i].segments;
            for (var j = 0; j < segment.length; j++) {
                var packs = segment[j].packs;
                for (var k = 0; k < packs.length; k++) {
                    if (packs[k].packId === packId) {
                        return packs[k];
                    }
                }
            }
        }
    };

    var getFirstBundle = function(cat){
        for (var i = 0; i < data.bundle.length; i++) {
            if(data.bundle[i].packCategory === cat) return data.bundle[i];
        }
    };

    var arrBundleId = [];
    var arrBundle = [];
    var arrBundleSegmentPacks = [];
    var fwdSeatsArr = [];
    _.each(data.bundle, function(bundleData, bundleIdx) {
        arrBundleId[bundleData.packId] = bundleData.packId;
        arrBundle[bundleData.packId] = {
          packId : bundleData.packId,
          ancillaries : bundleData.ancillaries
        };

        _.each(bundleData.ancillaries, function (anc, id) {
            if(anc.ancillaryType === 'FWD') {
                fwdSeatsArr.push({
                    anc: bundleData.ancillaries[id],
                    packId: bundleData.packId,
                    pack: getPackData(bundleData.packId)
                });
            }
        });

    });
    _.each(data.packPaxAssociations, function(paxData, paxIdx) {
        _.each(paxData.segments, function(segmentsData, segmentsIdx) {
            _.each(segmentsData.packs, function (packsData, packsIdx) {
                if (arrBundleId[packsData.packId]) {
                  arrBundleSegmentPacks.push(packsData);
                }
            });
        });
    });

    arrBundleSegmentPacks.sort(function(a, b) {
        return parseFloat(a.price) - parseFloat(b.price);
    });

    fwdSeatsArr.sort(function(a, b) {
        return parseFloat(a.pack.price) - parseFloat(a.pack.price);
    });
%>
  <h3 class="sub-title--blue">For your flight</h3>
  <div data-accordion-wrapper="1" data-bundle>
    <div data-accordion-wrapper-content="1">
      <div data-accordion="1">
        <div class="addons-your-flight">
          <% if(data.bundle.length > 0) { %>
            <div class="your-flight-item block-2">
              <div class="addons-your-flight-content check-preferred">
                <div data-accordion-wrapper="2">
                  <div class="description">
                    <figure>
                      <% if(data.bundle[0].isRecommended == "true") {
                        if(data.bundle[0].packImageUrl !== "") { %>
                          <img src="<%- data.bundle[0].packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } else { %>
                          <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% }
                      } %>
                    </figure>
                    <div class="content">
                      <div class="detail-content">
                        <% if(data.bundle[0].isRecommended == "true") { %>
                          <span class="selected-orange">Recommended Deal</span>
                        <% } %>
                        <h4 class="title-5--blue">
                          <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                            Preferred Seat + Additional baggage
                          <% } %>
                        </h4>
                        <% if(data.bundle[0].packDescription) { %>
                          <p><%= data.bundle[0].packDescription %></p>
                        <% } %>
                      </div>
                      <div class="from-price-flight"><span class="note">From</span>
                        <span class="sgd-price">
                          <%
                            // Get highest discount
                            var dc = 0;
                            _.each(arrBundleSegmentPacks, function(pack, paxIdx) {
                                if(typeof pack.originalPrice !== 'undefined') {
                                    var pdc = parseInt(pack.originalPrice) - parseInt(pack.price);
                                    dc = pdc > dc ? pdc : dc;
                                }
                            });
                            %>
                            <%- arrBundleSegmentPacks[0].currency %> <%- Number(arrBundleSegmentPacks[0].price).toFixed(2) %>
                        </span>
                        <span class="miles">Per passenger per flight</span>
                        <% if(dc > 0) { %>
                        <span class="selected-orange">save up to <%- arrBundleSegmentPacks[0].currency %> <%- dc %></span>
                        <% } %>
                        <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                          <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="Select" class="btn-8" data-select-item="false"/>
                          <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Selected" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                          <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
                    <div class="block-flight-details">
                      <div class="title-popup-mb">
                        <span class="sub-heading-2--blue">
                          <% if(data.bundle[0].packCategory == "PSS_XBAG_B") { %>
                            Preferred Seat + Additional baggage
                          <% } %>
                        </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                      </div>
                      <% _.each(segmentsGroup, function(itemData, itemIdx) { %>
                        <div class="block-flight-details--inner">
                          <div class="addons-landing--inner">
                            <div data-accordion="2" class="addons-landing-content ">
                              <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                <span class="title-5--blue"><%- itemData.segmentDescription %></span>
                                <span class="bundle-selected hidden"><span></span></span><em class="ico-point-d"></em>
                              </a>
                              <div data-accordion-content="2" class="accordion__content">
                                <% _.each(data.packPaxAssociations, function(paxData, paxIdx) { %>
                                <div class="bundle-flight-block data-pss-xbag">
                                  <span class="title-5--blue"><%- paxData.paxName %></span>
                                  <div class="bundle-flight--inner">
                                    <%
                                        // Get correct segment data for current pax
                                        var paxItemData;
                                        for (var i = 0; i < paxData.segments.length; i++) {
                                            if(paxData.segments[i].segmentId === itemIdx) {
                                                paxItemData = paxData.segments[i];
                                                break;
                                            }
                                        }
                                    %>
                                    <% _.each(paxItemData.packs, function(packsData, packsIdx) {
                                      if (arrBundle[packsData.packId]) { %>
                                        <%
                                          var numBaggage;
                                          // Get number of units left in the package
                                          var curSegment = itemIdx;

                                          _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                            if (ancillariesData.ancillaryType == "XBAG") {
                                              numBaggage = ancillariesData.units.numberOfUnits + ancillariesData.units.unitOfMeasurement.toLowerCase()
                                            }
                                          });
                                        %>
                                        <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                          </div>
                                          <div class="bundle-flight--inner">
                                        <% } %>
                                        <div class="bundle-flight-item">
                                          <div class="bundle-flight-item--inner"><span class="title-5--blue">Deal <%- packsIdx + 1 %></span>
                                            <ul class="bundle-baggage two-bundle-baggage">
                                              <li><em class="ico-1-preferred"></em>Preferred Seat</li>
                                              <li>
                                                <% _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                                  if (ancillariesData.ancillaryType == "XBAG") { %>
                                                    <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                  <% }
                                                }); %>
                                              </li>
                                            </ul>
                                            <div class="select-price"><span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span>
                                              <%
                                                var fPackData = getPackData(packsData.packId);
                                                var savings = parseInt(fPackData.originalPrice) - parseInt(fPackData.price);

                                                if(typeof fPackData.originalPrice !== 'undefined') {
                                              %>
                                              <div class="discount-wrapper">
                                                <span class="selected-orange">save SGD <%= savings %></span>
                                              </div>
                                              <% } %>
                                              <input type="button" name="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false"  aria-label="Select Deal <%- packsIdx + 1 %> <%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                              <input type="button" name="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false" aria-label="De-select Deal <%- packsIdx + 1 %> <%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                            </div>
                                          </div>
                                        </div>
                                      <% }
                                    }); %>
                                  </div>
                                </div>
                                <% }); %>
                              </div>
                            </div>
                          </div>
                        </div>
                        <% }); %>
                      <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <% var fwdBundle = getFirstBundle('BAGGAGE_FWD'); %>

            <div class="your-flight-item block-2">
              <div class="addons-your-flight-content check-preferred">
                <div data-accordion-wrapper="2">
                  <div class="description">
                    <figure>
                      <% if(fwdBundle.isRecommended == "true") {
                        if(fwdBundle.packImageUrl !== "") { %>
                          <img src="<%- data.bundle[0].packImageUrl %>" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% } else { %>
                          <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                        <% }
                      } else { %>
                        <img src="images/add-ons-your-flight.jpg" alt="Preferred Seat" longdesc="img-desc.html"/>
                      <% } %>
                    </figure>
                    <div class="content">
                      <div class="detail-content">
                        <% if(fwdBundle.isRecommended == "true") { %>
                          <span class="selected-orange">Recommended Deal</span>
                        <% } %>
                        <h4 class="title-5--blue">
                          <% if(fwdBundle.packCategory == "BAGGAGE_FWD") { %>
                            Forward zone seat + Additional baggage
                          <% } %>
                        </h4>
                        <% if(fwdBundle.packDescription) { %>
                          <p><%= fwdBundle.packDescription %></p>
                        <% } %>
                      </div>
                      <div class="from-price-flight"><span class="note">From</span>
                        <span class="sgd-price">
                          <%
                            var dc = 0;
                            for (var i = 0; i < fwdSeatsArr.length; i++) {
                                var pack = getPackData(fwdSeatsArr[i].packId);
                                if(typeof pack.originalPrice !== 'undefined') {
                                    var pdc = parseInt(pack.originalPrice) - parseInt(pack.price);
                                    dc = pdc > dc ? pdc : dc;
                                }
                            }

                            %>
                            <%- fwdSeatsArr[0].pack.currency %> <%- Number(fwdSeatsArr[0].pack.price).toFixed(2) %>
                        </span>
                        <span class="miles">Per passenger per flight</span>
                        <% if(dc > 0) { %>
                        <span class="selected-orange">save up to <%- arrBundleSegmentPacks[0].currency %> <%- dc %></span>
                        <% } %>
                        <div data-accordion-trigger="1" aria-expanded="false" data-trigger-mobile-popup class="button-group-3">
                          <input type="button" name="select-bundle-seat-btn" id="select-bundle-seat-btn" value="Select" class="btn-8" data-select-item="false"/>
                          <input type="button" name="selected-bundle-seat-btn" id="selected-bundle-seat-btn" value="Selected" class="hidden btn-1 selected-button-1" data-select-item="false"/>
                          <a href="#" class="icon-down"><em class="ico-point-d hidden" data-select-item="false"><span class="ui-helper-hidden-accessible">Select</span></em></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div data-accordion-content="1" data-accordion-wrapper-content="2" class="block-show-popup-mobile" data-return-flight="true" >
                    <div class="block-flight-details">
                      <div class="title-popup-mb">
                        <span class="sub-heading-2--blue">
                            <% if(fwdBundle.packCategory == "BAGGAGE_FWD") { %>
                              Forward zone seat + Additional baggage
                            <% } %>
                        </span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text">&#xe60d;</span></a>
                      </div>
                      <% _.each(segmentsGroup, function(itemData, itemIdx) { %>
                        <div class="block-flight-details--inner">
                          <div class="addons-landing--inner">
                            <div data-accordion="2" class="addons-landing-content ">
                              <a href="#" data-accordion-trigger="2" aria-expanded="false" aria-label="Add-ons" class="accordion__control accordion__control-flight trigger-mobile-resize">
                                <span class="title-5--blue"><%- itemData.segmentDescription %></span>
                                <span class="bundle-selected hidden"><span></span></span><em class="ico-point-d"></em>
                              </a>
                              <div data-accordion-content="2" class="accordion__content">
                                <% _.each(data.packPaxAssociations, function(paxData, paxIdx) { %>
                                <div class="bundle-flight-block data-pss-xbag">
                                  <span class="title-5--blue"><%- paxData.paxName %></span>
                                  <div class="bundle-flight--inner">
                                    <%
                                        // Get correct segment data for current pax
                                        var paxItemData;
                                        for (var i = 0; i < paxData.segments.length; i++) {
                                            if(paxData.segments[i].segmentId === itemIdx) {
                                                paxItemData = paxData.segments[i];
                                                break;
                                            }
                                        }
                                    %>
                                    <% var dealCount = 1;
                                    for (var i = 0; i < paxItemData.packs.length; i++) {
                                      var packsData = paxItemData.packs[i];
                                      var packsIdx = i;

                                      if (arrBundle[packsData.packId]) {
                                          var numBaggage;
                                          // Get number of units left in the package
                                          var curSegment = itemIdx;

                                          var hasForwardSeat = false;
                                          for (var k = 0; k < arrBundle[packsData.packId].ancillaries.length; k++) {
                                              var ancillariesData = arrBundle[packsData.packId].ancillaries[k];
                                              if (ancillariesData.ancillaryType == "FWD") {
                                                  hasForwardSeat = true;
                                              }
                                          }

                                          if(!hasForwardSeat) continue;

                                          _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                            if (ancillariesData.ancillaryType == "XBAG") {
                                              numBaggage = ancillariesData.units.numberOfUnits + ancillariesData.units.unitOfMeasurement.toLowerCase()
                                            }
                                          });
                                        %>
                                        <% if(packsIdx % 2 == 0 && packsIdx != 0) {%>
                                          </div>
                                          <div class="bundle-flight--inner">
                                        <% } %>
                                        <div class="bundle-flight-item">
                                          <div class="bundle-flight-item--inner"><span class="title-5--blue">Deal <%= dealCount %></span>
                                            <ul class="bundle-baggage two-bundle-baggage">
                                              <li>
                                              <% _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                                if (ancillariesData.ancillaryType == "FWD") { %>
                                                  <em class="ico-1-preferred"></em>Forward seat
                                                <% }
                                              }); %>
                                              </li>
                                              <li>
                                                <% _.each(arrBundle[packsData.packId].ancillaries, function(ancillariesData, ancillariesIdx) {
                                                  if (ancillariesData.ancillaryType == "XBAG") { %>
                                                    <em class="ico-business-1"></em>Additional baggage <%- numBaggage %>
                                                  <% }
                                                }); %>
                                              </li>
                                            </ul>
                                            <div class="select-price"><span class="sgd-price"><%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %></span>
                                              <%
                                                var fPackData = getPackData(packsData.packId);
                                                var savings = parseInt(fPackData.originalPrice) - parseInt(fPackData.price);

                                                if(typeof fPackData.originalPrice !== 'undefined') {
                                              %>
                                              <div class="discount-wrapper">
                                                <span class="selected-orange">save <%- packsData.currency %> <%= savings %></span>
                                              </div>
                                              <% } %>
                                              <input type="button" name="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="select-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Select" class="btn-8" data-selected-button="false"  aria-label="Select Deal <%- packsIdx + 1 %> <%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                              <input type="button" name="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" id="selected-bundle-<%- itemData.segmentId %>-<%- packsIdx + 1 %>" value="Selected" class="hidden btn-1" data-selected-button="false" aria-label="De-select Deal <%- packsIdx + 1 %> <%- packsData.currency %> <%- Number(packsData.price).toFixed(2) %> Preferred Seat and Additional baggage <%- numBaggage %> for <%- paxData.paxName %>"/>
                                            </div>
                                          </div>
                                        </div>
                                      <% }
                                      dealCount++;
                                    } %>
                                  </div>
                                </div>
                                <% }); %>
                              </div>
                            </div>
                          </div>
                        </div>
                        <% }); %>
                      <input type="button" name="confirm-bundle-btn" id="confirm-bundle-btn" value="Confirm" class="btn-1 hidden-tb-dt confirm-price-button">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <% } %>
        </div>
      </div>
    </div>
  </div>
<% } %>
