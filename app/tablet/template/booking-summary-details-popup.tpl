<% if(!confirmationPage) { %>
  <h2 data-anchor="flights" class="popup__heading hidden">Flight details</h2>
  <div class="flights-target flights-target--2">
    <% _.each(data.bookingSummary.flight, function(flight, flightIdx) { %>
      <h3 class="sub-heading-3--dark">
        <%= (flightIdx + 1) %>. <%= flight.origin %> to <%= flight.destination %>
      </h3>
      <div class="flights-target__content">
        <div class="flights__info--group">
          <% _.each(flight.flightSegments, function(segment, segmentIdx) { %>
            <% if(segmentIdx == 0) { %>
              <div class="flights__info none-border">
                <div class="flights--detail">
                  <span>Flight <%= segment.carrierCode %> <%= segment.flightNumber %></span>
                </div>
                <% if(segment.airCraftType) { %>
                  <p class="aircraft-type">Aircraft type: <%= segment.airCraftType %></p>
                <% } %>
                <span class="class-flight"><%= segment.cabinClassDesc %></span>
              </div>
            <% } %>
            <div class="flights__info">
              <div class="flights__info--detail">
                <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                <span class="hour">
                  <%= segment.deparure.airportCode %> <%= segment.deparure.time %>
                </span>
                <span class="country-name">
                  <%= segment.deparure.cityName %>
                </span>
                <span class="date">
                  <%= segment.deparure.date %>,<br> <%= segment.deparure.airportName %>
                  <span class="terminal">
                    <% if(segment.deparure.terminalName) { %>
                      <%= segment.deparure.terminalName %>
                    <% } %>
                    <%= segment.deparure.terminal %>
                  </span>
                </span>
              </div>
              <div class="flights__info--detail">
                <span class="hour">
                  <%= segment.arrival.airportCode %> <%= segment.arrival.time %>
                </span>
                <span class="country-name">
                  <%= segment.arrival.cityName %>
                </span>
                <span class="date">
                  <%= segment.arrival.date %>,<br> <%= segment.arrival.airportName %>
                  <span class="terminal">
                    <% if(segment.arrival.terminalName) { %>
                      <%= segment.arrival.terminalName %>
                    <% } %>
                    <%= segment.arrival.terminal %>
                  </span>
                </span>
              </div>
              <p class="operated"><%= L10n.oparated %></p>
            </div>
            <% if(segment.layoverTime) { %>
              <div class="flights__info travel-time none-border">
                <span>Layover time: <%= segment.layoverTime %></span>
              </div>
            <% } %>
          <% }); %>
          <div class="flights__info travel-time none-border">
            <span>Total travel time: <%= flight.totalTravelTime %></span>
          </div>
        </div>
      </div>
    <% }); %>
  </div>

  <%
    var renderSeat = function(detail, seat){
      print('<div class="booking-details booking-details--2">' +
        '<div class="booking-col col-1">' +
          '<em class="ico-change-seat"></em>' +
        '</div>' +
        '<div class="booking-col col-2">' + L10n.bookingSummary.texts.seat + '</div>' +
        '<div class="booking-col col-3">' +
          '<div class="align-wrapper">' +
            '<div class="align-inner">' +
              '<div class="has-cols">' +
                '<p class="target-info">' + detail.from + ' to ' + detail.to + '</p>' +
                '<p class="seat-info">' + seat.description + '</p>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>');
    };
    var renderBags = function(bags){
      var bagHtml = '';
      bagHtml += '<div class="booking-details booking-details--2">' +
        '<div class="booking-col col-1">' +
          '<em class="ico-business-1"></em>' +
        '</div>' +
        '<div class="booking-col col-2">' + L10n.bookingSummary.texts.baggage + '</div>' +
        '<div class="booking-col col-3">' +
          '<div class="align-wrapper">' +
            '<div class="align-inner">';

      for(var i = 0; i < bags.length; i++) {
        bagHtml += '<p>' + bags[i].description + '</p>';
      }

      bagHtml += '</div></div></div></div>';
      print(bagHtml);
    };
    var renderSubHeading = function(detail) {
      _.each(data.bookingSummary.flight, function(f, fIdx) {
        _.each(f.flightSegments, function(s, sIdx) {
          if(s.deparure.airportCode == detail.from && s.arrival.airportCode == detail.to) {
            print('<strong>' + s.carrierCode + ' ' + s.flightNumber + '</strong> - ' + s.deparure.cityName + ' to ' + s.arrival.cityName);
          }
        });
      });
    };
  %>

<!--   <h2 class="popup__heading"><%-L10n.bookingSummary.texts.passengers%></h2>
  <div class="flights-target">
    <% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
      <% if(pax.detailsPerFlight) { %>
        <%var arrBags = [];%>
        <div class="booking-info--2">
          <h3 class="sub-heading-3--dark"><%= (paxIdx + 1) %>. <%= pax.paxName %></h3>
          <% _.each(pax.detailsPerFlight, function(detail, detailIndex) { %>
            <div class="booking-info--2__item">
              <h4 class="sub-heading">
                <span>
                  <%renderSubHeading(detail);%>
                </span>
              </h4>
              <div class="booking-details__group">
                <%if(detail.addonPerPax && detail.addonPerPax.length){%>
                  <%arrBags = [];%>
                  <%_.each(detail.addonPerPax, function(addOn, adIdx){%>
                    <%addOn.type === 'Seat' ? (renderSeat(detail, addOn)) :
                      addOn.type === 'Excess baggage' ? (arrBags.push(addOn)) : ''%>
                  <%})%>
                  <%renderBags(arrBags);%>
                <%}%>
              </div>
            </div>
          <% }); %>
        </div>
      <% } %>
    <% }); %>
  </div> -->
<% } %>

<h2 data-anchor="cost" class="popup__heading">Cost breakdown by passenger</h2>
<h4 data-anchor="cost" class="sub-heading-3--dark">Flight</h2>
<p></p>
<div class="flights-target">
  <% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
    <table class="table-cost <%= (paxIdx % 2 > 0) ? 'table-cost' : '' %>">
      <thead>
        <tr>
          <th>
            <span class="text-bold-1">PASSENGER <%= (paxIdx + 1) %> - <%= pax.paxName %></span>
          </th>
          <th class="hidden-mb">
            <span class="detail">SGD</span>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="first">
          <td>
            <span class="text-bold indent">Fare (<%= pax.paxType %>)</span>
            <span class="detail detail-1">SGD</span>
          </td>
          <td data-need-format="2"><%= pax.fareDetails.fareAmount %></td>
        </tr>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold indent">Airport / Government taxes:</span>
          </td>
        </tr>
        <% _.each(pax.fareDetails.taxes, function(tax, taxIdx) { %>
          <tr>
            <td class="indent"><%= tax.description %> (<%= tax.code %>)</td>
            <td data-need-format="2"><%= tax.amount %></td>
          </tr>
        <% }); %>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold indent">Carrier Surcharges:</span>
          </td>
        </tr>
        <% _.each(pax.fareDetails.surcharge, function(charge, chargeIdx) { %>
          <tr>
            <td><%= charge.description %> (<%= charge.code %>)</td>
            <td data-need-format="2"><%= charge.amount %></td>
          </tr>
        <% }); %>
        <% if(pax.detailsPerFlight) { %>
          <%
            var seat = 0, excessBaggage = 0, meal = 0;
            _.each(pax.detailsPerFlight, function(detail, detailIndex) {
              _.each(detail.addonPerPax, function(paxAdd, paxAddIdx) {
                if(paxAdd.amount) {
                  if(paxAdd.type == 'Seat') {
                    seat += paxAdd.amount;
                  }
                  else if (paxAdd.type == 'Excess baggage') {
                    excessBaggage += paxAdd.amount;
                  }
                  else if (paxAdd.type == 'Meal') {
                    meal += paxAdd.meal;
                  }
                }
              });
            });
          %>
          <% if (seat > 0) { %>
            <tr class="row-title">
              <td>
                <span class="text-bold">Preferred Seat</span>
                <span class="detail">
                  <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                </span>
                <span class="detail detail-1">Cost in SGD</span>
              </td>
              <td data-need-format="2"><%= seat %></td>
            </tr>
          <% } %>
          <% if (excessBaggage > 0) { %>
            <tr class="row-title">
              <td>
                <span class="text-bold">Excess baggage</span>
                <span class="detail">
                  <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                </span>
                <span class="detail detail-1">Cost in SGD</span>
              </td>
              <td data-need-format="2"><%= excessBaggage %></td>
            </tr>
          <% } %>
          <% if (meal > 0) { %>
            <tr class="row-title">
              <td>
                <span class="text-bold">Meal</span>
                <span class="detail">
                  <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                </span>
                <span class="detail detail-1">Cost in SGD</span>
              </td>
              <td data-need-format="2"><%= meal %></td>
            </tr>
          <% } %>
        <% } %>
      </tbody>
      <tfoot>
        <tr>
          <td>
            <span class="text-bold">Sub-total</span>
            <span class="detail detail-1">Cost in SGD</span>
          </td>
          <td class="total-price" data-need-format="2">
            <%= pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount %>
          </td>
        </tr>
      </tfoot>
    </table>
  <% }); %>
<!--   <table class="table-cost table-addons">
    <thead>
      <tr>
        <th>
          <span class="text-bold-1">ADD-ONS:</span>
        </th>
        <th class="hidden-mb">
          <span class="detail">Cost in SGD</span>
        </th>
      </tr>
    </thead>
    <tbody>
      <% _.each(data.bookingSummary.commonAddons, function(addon, addonIdx) { %>
        <tr class="<%= addonIdx > 0 ? 'row-title' : '' %>">
          <td>
            <span class="text-bold"><%= addon.type %></span>
            <span class="detail"><%= addon.description %></span>
          </td>
          <td data-need-format="2"><%= addon.amount %></td>
        </tr>
      <% }); %>
    </tbody>
    <tfoot>
      <tr>
        <td>
          <span class="text-bold">Sub-total</span>
          <span class="detail detail-1">Cost in SGD</span>
        </td>
        <td class="total-price" data-need-format="2">
          <%= data.bookingSummary.addonSubTotal %>
        </td>
      </tr>
    </tfoot>
  </table> -->

  <!--STATIC DISPLAY-->
<h3 class="sub-heading-3--dark">Flight add-ons</h3>
<p></p>
<div class="flights-target">
    <table class="table-cost table-addons-mp">
      <thead>
        <tr>
          <th>
            <span class="text-bold-1">PASSENGER 1 - JOHN TAN</span>
          </th>
          <th class="hidden-mb">
            <span class="detail">SGD</span>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="first">
          <td>
            <span class="text-bold indent">Bundle 3 -  Preferred Seat + Additional 10kg baggage</span>
          </td>
        </tr>
        <tr class="first">
          <td>
            <span class="indent">San Francisco to Singapore - 23 Feb 2017 (Thu)</span>
          </td>
          <td data-need-format="2">120</td>
        </tr>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold indent">WiFi - Full flight</span>
          </td>
        </tr>
          <tr>
            <td class="indent">San Francisco to Singapore - 23 Feb 2017 (Thu)</td>
            <td data-need-format="2">40</td>
          </tr>
      </tbody>
      <tfoot>
        <tr>
          <td>
            <span class="text-bold">Sub-total</span>
            <span class="detail detail-1">Cost in SGD</span>
          </td>
          <td class="total-price" data-need-format="2">160</td>
        </tr>
      </tfoot>
    </table>

    <table class="table-cost table-addons-mp">
      <thead>
        <tr>
          <th>
            <span class="text-bold-1">PASSENGER 2 - BILL TAN</span>
          </th>
          <th class="hidden-mb">
            <span class="detail">SGD</span>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="first">
          <td>
            <span class="text-bold indent">Bundle 2 -  Preferred Seat + Additional 10kg baggage</span>
          </td>
        </tr>
        <tr class="first">
          <td>
            <span class="indent">San Francisco to Singapore - 23 Feb 2017 (Thu)</span>
          </td>
          <td data-need-format="2">90</td>
        </tr>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold indent">WiFi - Full flight</span>
          </td>
        </tr>
          <tr>
            <td class="indent">San Francisco to Singapore - 23 Feb 2017 (Thu)</td>
            <td data-need-format="2">50</td>
          </tr>
      </tbody>
      <tfoot>
        <tr>
          <td>
            <span class="text-bold">Sub-total</span>
            <span class="detail detail-1">Cost in SGD</span>
          </td>
          <td class="total-price" data-need-format="2">140</td>
        </tr>
      </tfoot>
    </table>
</div>

<h3 class="sub-heading-3--dark">Trip add-ons</h3>
<div class="flights-target">
    <table class="table-cost table-addons-mp trip-addons">
      <thead>
        <tr>
          <th>
            <span class="text-bold-1"></span>
          </th>
          <th class="hidden-mb">
            <span class="detail">SGD</span>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="first">
          <td>
            <span class="text-bold">Travel insurance</span>
          </td>
        </tr>
        <tr class="first">
          <td>
            <span class="">From 23 Apr to 15 May 2017</span>
          </td>
          <td data-need-format="2">80</td>
        </tr>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold ">Hotel</span>
          </td>
        </tr>
        <tr>
          <td class="">The Fairmont San Francisco Hotel</td>
        </tr>
        <tr>
          <td class="">14 Feb 2017 (Tue) to 16 Feb 2017 (Thu) - 2 nights</td>
          <td data-need-format="2">582.40</td>
        </tr>
        <tr class="row-title">
          <td colspan="2">
            <span class="text-bold ">Car</span>
          </td>
        </tr>
        <tr>
          <td class="">14 Feb 2017 (Tue) to 16 Feb 2017 (Thu)</td>
          <td data-need-format="2">231.50</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td>
            <span class="text-bold">Sub-total</span>
          </td>
          <td class="total-price" data-need-format="2">893.80</td>
        </tr>
      </tfoot>
    </table>
</div>

<div class="grand-total">
  <span class="total-title">Grand total payable</span>
  <p class="total-info">
    <span>
      SGD <span data-need-format="2"> 3,971.80</span>
    </span>
  </p>
</div>
