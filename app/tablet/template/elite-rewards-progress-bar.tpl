<h2 class="main-heading dials__title">Elite Rewards</h2>
<div class="elite-rewards elite-rewards--desktop hidden">
  <div class="group-progress elite-rewards__group-progress">
    <div data-progress-bar data-kf-points="75000" data-total-points="100000" data-silver="0" class="progress-bar">
      <div class="milestones elite-rewards__milestones js-milestones">
        <span class="time-start">
          Start of qualification period
          <span>1 Jan 2018</span>
        </span>
        <div class="milestones-item milestones-item-1 js-elite-item">
          <span class="milestones-item__info">
            <span class="icon-elite-rewards"></span>
            <span data-double-miles class="desc">
              <strong>60,000 Elite Rewards</strong>
              Earn double KrisFlyer miles
            </span>
          </span>
        </div>
        <div class="milestones-item milestones-item-2 js-elite-item">
          <span class="milestones-item__info">
            <span class="icon-elite-rewards"></span>
            <span data-double-miles class="desc">
              <strong>75,000 Elite Rewards</strong>
              Complimentary seat upgrade (Short-haul)
            </span>
          </span>
        </div>
        <div class="milestones-item milestones-item-3 last-milestones-item js-elite-item">
          <span class="milestones-item__info">
            <span class="icon-elite-rewards"></span>
            <span data-airport-upgrade class="desc">
              <strong>100,000 Elite Rewards</strong>
              Complimentary upgrade to<br/>Premium Economy Class
            </span>
          </span>
        </div>
        <div class="milestones-bar--silver js-silver"></div>
        <div class="milestones-bar--gold js-gold"></div>
        <div class="milestones-animate-wrapper milestones-animate-wrapper-1">
          <div data-index="0" data-point="60000" class="milestones-animate milestones-animate-1 elite-animate js-elite-animate"></div>
          <span class="tooltip-progress tooltip js-elite-tooltip">
            <em class="tooltip__arrow"></em>Current Elite Rewards:
            <span class="current-number">60,000</span>
          </span>
        </div>
        <div class="milestones-animate-wrapper milestones-animate-wrapper-2">
          <div data-index="1" data-point="75000" class="milestones-animate milestones-animate-2 elite-animate js-elite-animate"></div>
          <span class="tooltip-progress tooltip js-elite-tooltip">
            <em class="tooltip__arrow"></em>Current Elite Rewards:
            <span class="current-number">75,000</span>
          </span>
        </div>
        <div class="milestones-animate-wrapper milestones-animate-wrapper-3">
          <div data-index="2" data-point="100000" class="milestones-animate milestones-animate-3 elite-animate js-elite-animate"></div>
          <span class="tooltip-progress tooltip js-elite-tooltip">
            <em class="tooltip__arrow"></em>Current Elite Rewards:
            <span class="current-number">100,000</span>
          </span>
        </div>
        <span class="time-end">
          End of qualification period
          <span>31 Dec 2018</span>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="elite-rewards elite-rewards--mobile js-progressbar-mobile elite-circle hidden">
  <span class="elite-circle__time-start">
    Start of qualification<br/>period 1 Jan 2018
  </span>

  <div data-chart-old="3" class="elite-circle-wrapper">
    <div data-progress-bar data-kf-points="75000" data-total-points="100000" data-silver="0" class="js-progress-bar">
      <div class="elite-circle__mask elite-circle__mask--first">
        <div class="elite-circle__fill elite-circle__fill--grey elite-circle__fill--first"></div>
        <div class="elite-circle__fill elite-circle--first-path elite-circle__fill--blue js-elite-animate" data-index="0" data-point="60000"></div>
      </div>

      <div class="elite-circle__mask elite-circle__mask--second">
        <div class="elite-circle__fill elite-circle__fill--grey elite-circle__fill--second"></div>
        <div class="elite-circle__fill elite-circle--second-path elite-circle__fill--blue js-elite-animate" data-index="1" data-point="75000"></div>
      </div>

      <div class="elite-circle__mask elite-circle__mask--third">
        <div class="elite-circle__fill elite-circle__fill--grey elite-circle__fill--third"></div>
        <div class="elite-circle__fill elite-circle--third-path elite-circle__fill--blue js-elite-animate" data-index="2" data-point="100000"></div>
      </div>
    </div>

    <div class="elite-circle__dot js-milestones-dot dot-1">
      <span class="elite-circle__dot__info">
        <div class="js-checkmark hidden">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52" class="checkmark">
            <circle cx="26" cy="26" r="25" fill="none" class="checkmark--circle"></circle>
            <path fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" class="checkmark--checked"></path>
          </svg>
        </div>
        <span class="icon-dot"></span>
        <span data-double-miles class="desc">
          <strong class="js-milestones-statistic">60,000</strong><br/>
          Earn double KrisFlyer miles
        </span>
      </span>
    </div>
    <div class="elite-circle__dot js-milestones-dot dot-2">
      <span class="elite-circle__dot__info">
        <div class="js-checkmark hidden">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52" class="checkmark">
            <circle cx="26" cy="26" r="25" fill="none" class="checkmark--circle"></circle>
            <path fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" class="checkmark--checked"></path>
          </svg>
          </div>
        <span class="icon-dot"></span>
        <span data-double-miles class="desc">
          <strong class="js-milestones-statistic">75,000</strong><br/>
          Complimentary seat upgrade (Short-haul)
        </span>
      </span>
    </div>
    <div class="elite-circle__dot js-milestones-dot dot-3">
      <span class="milestones-item__info">
        <div class="js-checkmark hidden">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52" class="checkmark">
            <circle cx="26" cy="26" r="25" fill="none" class="checkmark--circle"></circle>
            <path fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" class="checkmark--checked"></path>
          </svg>
        </div>
        <span class="icon-dot"></span>
        <span data-airport-upgrade class="desc">
          <strong class="js-milestones-statistic">100,000</strong><br/>
          Complimentary upgrade to
          <br/>Premium Economy Class
        </span>
      </span>
    </div>
  </div>

  <span class="elite-circle__time-end">
    End of qualification<br/>period 31 Dec 2018
  </span>

  <div class="elite-circle__desc">
    <p class="elite-circle__info">
      <span class="elite-circle__info__heading">Elite Rewards</span>
      <span class="elite-circle__info__current-statistic js-statistic-box hidden">
        Current
        <br/>Elite Rewards:
        <br/><span class="js-current-statistic"></span>
    </p>
  </div>
</div>

<p class="elite-rewards__notice">
  <span>
    <a href="#" class="js-lightbox" data-trigger-popup=".popup--elite-rewards">Elite Rewards</a> are earned on Singapore Airlines and SilkAir flights only.
  </span>
</p>

<aside class="popup popup--elite-rewards hidden">
  <div class="popup__inner">
    <div class="popup__content"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>
      <h2 class="popup__heading">Elite rewards</h2>
      <div class="popup__description">
        This is content of "Elite Rewards are earned on Singapore Airlines and SilkAir flights only."
      </div>
    </div>
  </div>
</aside>
