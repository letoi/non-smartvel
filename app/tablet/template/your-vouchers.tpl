<% if(data) { %>
  <div class="voucher-list voucher-list--1">
    <% _.each(data, function(voucherData, idx){ %>
      <% if(voucherData.rewardStatus !== 'Expired') { %>
        <div class="item">
          <div class="item-row <% if(voucherData.rewardStatus === 'Available') { %> available-row <% } %>">
            <div class="item-cell rewardType"><%- voucherData.rewardType %></div>
            <div class="item-cell promoCode"><%- voucherData.promoCode %></div>
            <div class="item-cell rewardExpiryDate"><%- voucherData.rewardExpiryDate %></div>
            <% if(voucherData.rewardStatus === 'Available') { %>
              <div class="item-cell rewardStatus"><span class="status available"><%- voucherData.rewardStatus %></span>
                  <em data-tooltip="true" data-type="2" data-max-width="188" data-content="<p class=&quot;tooltip__text-2&quot;>You have earned this voucher, and it’s ready to be used</p>" class="ico-tooltips" aria-label="View more information" tabindex="0"></em>
              </div>
              <div class="group-link">
                <% if(voucherData.rewardType == "50,000 KrisFlyer Miles Redemption Discount"){ %>
                  <span class="full-link"><a href="#" class="link-4"><em class="ico-point-r"></em>Redeem flights</a></span>
                <% } %>
                <a href="#" class="link-4"><em class="ico-point-r"></em>View your upcoming flights</a>
              </div>
            <% } else if(voucherData.rewardStatus === 'Applied') { %>
              <div class="item-cell rewardStatus"><span class="status"><%- voucherData.rewardStatus %></span>
                  <em data-tooltip="true" data-type="2" data-max-width="188" data-content="<p class=&quot;tooltip__text-2&quot;>You have recently used this voucher, and it’s being processed</p>" class="ico-tooltips" aria-label="View more information" tabindex="0"></em>
              </div>
            <% } else if(voucherData.rewardStatus === 'Used') { %>
              <div class="item-cell rewardStatus"><span class="status"><%- voucherData.rewardStatus %></span>
                  <em data-tooltip="true" data-type="2" data-max-width="188" data-content="<p class=&quot;tooltip__text-2&quot;>You have successfully used this voucher</p>" class="ico-tooltips" aria-label="View more information" tabindex="0"></em>
              </div>
            <% } else if(voucherData.rewardStatus === 'Expired') { %>
              <div class="item-cell rewardStatus"><span class="status"><%- voucherData.rewardStatus %></span>
                <em data-tooltip="true" data-type="2" data-max-width="188" data-content="<p class=&quot;tooltip__text-2&quot;>This voucher has expired, and can no longer be used</p>" class="ico-tooltips" aria-label="View more information" tabindex="0"></em>
              </div>
            <% } else if(voucherData.rewardStatus === 'Pending') { %>
              <div class="item-cell rewardStatus"><span class="status"><%- voucherData.rewardStatus %></span>
              </div>
            <% } %>
          </div>
        </div>
      <% } %>
    <% }); %>
  </div>
<% } %>
