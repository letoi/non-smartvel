<% if(data) { %>
  <%
    fareFamiliesGroup = [],  familiesCabinGroup = {}, arrWrap = [];
    _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) {
      familiesCabinGroup[fareFamilies.cabinClassName] = fareFamilies.cabinClassName;
    });
  %>

  <%
    function groupBy(arr, key) {
      var newArr = [],
          family = {},
          newItem, i, j, cur;
      for (i = 0, j = arr.length; i < j; i++) {
          cur = arr[i];
           if (!(cur[key] in family)) {
              family[cur[key]] = { type: cur[key], data: [] };
              newArr.push(family[cur[key]]);
          }
          family[cur[key]].data.push(cur);
      }
      return newArr;
    }
  %>
  <% var arr = [] %>
  <% _.each(data.flights, function(flights, flightIdx){ %>
    <% var newArray = [] %>
        <% _.each(flights.segments, function(segments, flightSegmentIdx){ %>
          <% var newArr1 = [] %>
              <% _.each(data.recommendations, function(recommendation, reIdx){ %>
                <% _.each(data.fareFamilies, function(fareFamiliesGroup, fareFamiliesGroupIdx){ %>
                  <% _.each(recommendation.segmentBounds[flightIdx].segments, function(segmentItem, segmentItemIdx){ %>
                    <% if(segmentItem.segmentID === segments.segmentID && fareFamiliesGroup.fareFamily == recommendation.segmentBounds[flightIdx].fareFamily ) { %>
                      <% newArr1.push({"family" : recommendation.segmentBounds[flightIdx].fareFamily,"familyGroup" : fareFamiliesGroup.cabinClassName, "price" : recommendation.fareSummary.fareTotal.totalAmount, "recommendationId" : recommendation.recommendationID, "displayLastSeat" : segmentItem.displayLastSeat, "numOfLastSeats" : segmentItem.numOfLastSeats }) %>
                      <% return false %>
                    <% } %>
                  <% }) %>
                <% }) %>
              <% }); %>
              <% newArray[segments.segmentID] =  newArr1 %>
        <% }); %>
        <% arr.push(newArray) %>
  <% }); %>
  <%
    _.each(arr, function(arr1, idx1){
      _.each(arr1, function(arr2, idx2){
        arr[idx1][idx2] = groupBy(arr2, 'family');
      })
    });
  %>

  <% _.each(data.flights, function(flights, flightIdx) { %>
    <div class="wrap-content-fs">
      <div class="slider-group">
          <div class="title-slider">
            <h3 class="sub-heading-3--dark">
            <% _.each(data.airports, function(airports, airportsIdx) { %>
              <% if( airports.airportCode === flights.originAirportCode ) { %>
                <%- flightIdx + 1 %>. <%- airports.cityName %>
              <% } %>
            <% }); %> to
            <% _.each(data.airports, function(airports, airportsIdx) { %>
              <% if( airports.airportCode === flights.destinationAirportCode ) { %>
                <%- airports.cityName %>
              <% } %>
            <% }); %>
            </h3>
            <a href="cib-fare-calendar.html" class="monthly-view"><em class="ico-chart"><span class="ui-helper-hidden-accessible"></span></em>Monthly view</a>
          </div>
          <div class="economy-slider flexslider" data-fs-slider="true", data-current-date="2017-01-15" >
            <button type="button" class="btn-3 btn-slider btn-prev slick-prev"></button>
            <div class="slides"></div>
            <button type="button" class="btn-3 btn-slider btn-next slick-next"></button>
          </div>
          <div class="sub-logo">
            <img src="images/svg/sq.svg" alt="SQ Logo" longdesc="img-desc.html">
            <img src="images/svg/si.svg" alt="SI Logo" longdesc="img-desc.html">
            <img src="images/svg/sc.svg" alt="SC Logo" longdesc="img-desc.html">
            <span class="text">Singapore Airlines Group</span>
            <span class="ico-right">
              <em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2'&gt;Hotel bookings allow for a maximum length of stay of 14 days.&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em>
            </span>
          </div>
      </div>
      <% if(flights.segments.length > 1 && filterArr && filterArr.length) { %>
        <% var filter = filterArr[flightIdx] %>
        <div class="flight-search-filter-economy" data-flight-filter="<%- flightIdx %>">
          <div class="link-show"><em class="ico-filters">&nbsp;</em><a href="#" class="link-4"><span class="text">Show filters</span></a>
          </div>
          <div class="content">
            <div class="heading-filter">
              <div class="title">Filters</div>
              <div class="link-hide"><a href="#" class="link-4"><em class="ico-point-r">&nbsp;</em><span class="text">Hide filters</span></a>
              </div>
            </div>
            <div class="block-1">
              <div class="content-filter-search">
                <div class="content-inner">
                  <div class="left-content">
                    <p class="title-6--dark title">Stopover</p>
                    <ul class="list">
                      <% if(filter.nonStop) { %>
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="non-stop-<%= flightIdx %>" id="non-stop-<%= flightIdx %>" aria-labelledby="non-stop-error" type="checkbox" aria-label="Non-stop">
                            <label for="non-stop-<%= flightIdx %>">Non-stop</label>
                          </div>
                        </li>
                      <% } %>
                      <% if(filter.oneStop) { %>
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="one-stop-<%= flightIdx %>" id="one-stop-<%= flightIdx %>" aria-labelledby="one-stop-error" type="checkbox" aria-label="1-stop">
                            <label for="one-stop-<%= flightIdx %>">1-stop</label>
                          </div>
                        </li>
                      <% } %>
                      <% if(filter.twoStop) { %>
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="two-stop-<%= flightIdx %>" id="two-stop-<%= flightIdx %>" aria-labelledby="two-stop-error" type="checkbox" aria-label="2-stop">
                            <label for="two-stop-<%= flightIdx %>">2-stop</label>
                          </div>
                        </li>
                      <% } %>
                    </ul>
                  </div>
                  <div class="right-content">
                    <p class="title-6--dark title">Operating carrier</p>
                    <ul class="list">
                      <% if(filter.SAgroup) { %>
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="sa-group-<%= flightIdx %>" id="sa-group-<%= flightIdx %>" aria-labelledby="singapore-airlines-group-error" type="checkbox" aria-label="Singapore Airlines Group">
                            <label for="sa-group-<%= flightIdx %>">Singapore Airlines Group <em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2'&gt;Hotel bookings allow for a maximum length of stay of 14 days.&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em></label>
                          </div>
                        </li>
                      <% } %>
                      <% if(filter.codeShare) { %>
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="codeshare-<%= flightIdx %>" id="codeshare-<%= flightIdx %>" aria-labelledby="parter-codeshare-error" type="checkbox" aria-label="partner / codeshare airlines">
                            <label for="codeshare-<%= flightIdx %>">partner / codeshare airlines</label>
                          </div>
                        </li>
                      <% } %>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="content-filter-search filter-travel">
                <div class="content-inner">
                  <p class="title-6--dark title">Travel duration</p>
                  <div class="slider slider--1">
                    <div class="slider-range" data-range-slider="tripDuration" data-min="<%- filter.minTripDuration %>" data-max="<%- filter.maxTripDuration %>" data-step="3600"></div>
                  </div>
                  <p class="title-6--dark title">Layover time</p>
                  <div class="slider slider--1">
                    <div class="slider-range" data-range-slider="layover" data-min="<%- filter.minLayover %>" data-max="<%- filter.maxLayover %>" data-step="1800"></div>
                  </div>
                </div>
              </div>
              <div class="content-filter-search filter-time">
                <div class="content-inner">
                  <p class="title-6--dark title">Departure time</p>
                  <div class="slider slider--1">
                    <div class="slider-range" data-range-slider="departure" data-min="<%- filter.minDeparture %>" data-max="<%- filter.maxDeparture %>" data-step="1800000"></div>
                  </div>
                  <p class="title-6--dark title">Arrival time</p>
                  <div class="slider slider--1">
                    <div class="slider-range" data-range-slider="arrival" data-min="<%- filter.minArrival %>" data-max="<%- filter.maxArrival %>" data-step="1800000"></div>
                  </div>
                </div>
              </div>
              <input type="button" name="btn-filter" value="apply filter" class="btn-1 btn-filter hidden-tb-dt">
            </div>
          </div>
        </div>
      <% } %>
      <div class="recommended-flight-block">
        <div class="head-recommended"><span>Recommended flight for you</span></div>
      </div>
      <div class="loadmore-block hidden" data-loadmore>
        <span class="show-result">Showing <span data-loaded-flight>5</span> out of <span data-total-flight>10</span> flights</span>
        <a href="#" title="btn-loadmore" class="btn-loadmore hidden-mb-small">Load more flights</a>
        <a href="#" title="btn-loadmore" class="btn-loadmore-mb hidden-tb-dt">Show all flights</a>
      </div>
    </div>
  <% }); %>
<% } %>
