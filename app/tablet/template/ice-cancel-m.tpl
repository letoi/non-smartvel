<%

 var toDate = function(epoch, format, locale) {
      var date = new Date(epoch),
      format = format || 'dd/mmm/YY',
      locale = locale || 'en'
      dow = {};

      dow.en = ['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];

      var monthNames = ["Jan","Feb", "Mar", "Apr", "May", "June ","July", "Aug", "Sept", "Oct", "Nov", "Dec"];

      var formatted = format
      .replace('D', dow[locale][date.getDay()])
      .replace('d', ("0" + date.getDate()).slice(-2))
      .replace('mmm', ((monthNames[date.getMonth()])))
      .replace('yyyy', date.getFullYear())
      .replace('yy', (''+date.getFullYear()).slice(-2))
      .replace('hh',  ("0" + date.getHours()).slice(-2))
      .replace('mn', date.getMinutes())

      return formatted;
   }

%>
<aside class="popup popup--checkin-cancel hidden<% if(curLeg < 1){ %> first-flight<% } %>" data-leg="<%- curLeg %>">
  <div class="popup__inner" tabindex="-1">
    <div class="popup__content focus-outline" role="document" tabindex="0">
      <h2 class="popup__heading" aria-hidden="false">Cancel check-in</h2>
     <form action="#" method="post" class="form--cancel-flight">
        <fieldset>
             <%
                  var flight = flights[curLeg];
                  var checkedInLeg = [];
              %>
              <h3 class="sub-heading-3--dark"><%= curLeg+1 %>. <%= flight.origin.cityName %> (<%= flight.origin.airportCode %>) to <%= flight.destination.cityName %> (<%= flight.destination.airportCode %>)</h3>
              <h4 class="popup__text-intro--2"><%= toDate(flight.scheduledDepartureDateTime, "d mmm (D)") %> – <%= toDate(flight.scheduledDepartureDateTime, 'hh:mn') %> – <%= flight.operatingAirline.airlineCode %> <%= flight.operatingAirline.flightNumber %></h4>
             

              <div class="table-default">
                <div class="table-row table-row--heading">
                  <div class="table-col table-col__full">
                    <div class="table-inner">
                      <div class="custom-checkbox custom-checkbox--1 custom-checkbox-all">
                        <input name="cancelChekin<%- curLeg %>" id="cancelChekin<%- curLeg %>" type="checkbox" aria-labelledby="cancel-checkbox-0-error" aria-label="Select all" checked>
                        <label for="cancelChekin<%- curLeg %>">Select all</label>
                      </div>
                    </div>
                  </div>
                </div>
                      <div class="table-row">
                        <%
                          // set pax id for the loop
                          var pi = 0;
                          // loop through all flights
                          _.each(allPax, function(pax) {
                            // loop through services of each pax
                            _.each(pax.services, function(service){
                              _.each(flight.flightIDs, function(flID){
                                if(service.flightID == flID && service.dcsStatus.checkedIn == true){
                                  // assign new obj for checkedin pax
                         %>
                                    <div class="table-col table-col-1">
                                      <div class="table-inner">
                                        <div class="custom-checkbox custom-checkbox--<%- pi %>">
                                          <input name="cancelCheckin-<%= service.flightID %>" id="cancelCheckin-<%= service.flightID %>" type="checkbox" aria-labelledby="cancel-checkbox-<%- pi %>-error" <% if(service.dcsStatus.checkedIn){ %> checked <% } %> aria-label="<%= pax.paxDetails.firstName %> <%= pax.paxDetails.lastName %>">
                                          <label for="cancelCheckin-<%= service.flightID %>">
                                            <%- pax.paxDetails.firstName %> <%- pax.paxDetails.lastName %>
                                              <% if(pax.paxDetails.passengerType == "INF"){ %>
                                                <span>(INFANT)</span>
                                              <% } %>
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                             <% 
                                  }
                                });
                            });
                          });
                        %>       
                      </div>
                    </div>
          <div class="button-group-1">
            <input type="submit" name="cancel-checkbox-cancel-submit" id="cancel-checkbox-cancel-submit" value="Confirm" class="btn-1">
          </div>
        </fieldset>
      </form>
    <a href="#" class="popup__close" aria-label="Close button"></a></div>
  </div>
</aside>