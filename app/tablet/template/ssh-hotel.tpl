<% _.each( data, function( hotel, idx ){ %>
  <div class="hotel-item hotel-item--1">
    <figure class="hotel-item__thumb">
      <div class="wrap-img"><img src="<%- hotel.imgUrl%>" alt="<%- hotel.hotel%>" longdesc="img-desc.html">
      </div>
    </figure>
    <div class="hotel-item__inner">
      <div class="hotel-item__heading">
        <h4 class="sub-heading-2--blue"><%- hotel.hotel%></h4>
        <ul class="rating-block">
          <% for(var i = 0; i < hotel.rate; i++){ %>
            <% if(i === Math.floor(hotel.rate)) { %>
              <li class="half-rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em></li>
            <% } else { %>
              <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to unfavourite</span></em></li>
            <% } %>
          <%}%>
        </ul>
      </div>
      <div class="hotel-item__content" data-accordion="1">
        <p class="address"><%- hotel.address%></p>
        <div class="hotel-item__price"><span class="price-info"><%- hotel.currency%>  <%- hotel.price%></span><small><%- hotel.roomDetail%></small>
          <div class="choose-button">
            <a href="#" class="btn-1">Select</a>
          </div>
          <div class="accordion-point visible-mb" data-accordion-trigger="1">
            <em class="ico-point-d"></em>
          </div>
        </div>
        <div class="hotel-item__info" data-accordion-content="1">
          <dl class="info-list">
            <dt class="text">Package:</dt>
            <dd class="info"><%- hotel.package%>
            </dd>
            <dt class="text">Price includes:</dt>
            <dd class="info">
              <% _.each( hotel.includes, function( item, idx ){ %><% if(idx < hotel.includes.length) {%><%= item%><br><%}else{%><%= item%><%}%><%})%>
            </dd>
            <dt class="text text-icons">Hotel facilities:</dt>
            <dd class="info info-icons"><% _.each( hotel.facilities, function( facility, idx ){ %><em class="<%- facility%>"></em><%})%>
            </dd>
          </dl>
          <p class="desc"><%- hotel.desc%></p>
        </div>
      </div>
    </div>
  </div>
<%})%>
