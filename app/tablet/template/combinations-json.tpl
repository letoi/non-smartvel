<% if(data) { %>
	<%
		function groupBy(arr, key) {
      var newArr = [],
      		family = {},
          newItem, i, j, cur;
      for (i = 0, j = arr.length; i < j; i++) {
          cur = arr[i];
           if (!(cur[key] in family)) {
              family[cur[key]] = { type: cur[key], data: [] };
              newArr.push(family[cur[key]]);
          }
          family[cur[key]].data.push(cur);
      }
      return newArr;
		}
	%>
	<% var arr = [] %>
	<% _.each(data.flights, function(flights, flightIdx){ %>
		<% var newArray = [] %>
				<% _.each(flights.segments, function(segments, flightSegmentIdx){ %>
					<% var newArr1 = [] %>
							<% _.each(data.recommendations, function(recommendation, reIdx){ %>
								    <% _.each(recommendation.segmentBounds[flightIdx].segments, function(segmentItem, segmentItemIdx){ %>
		 									<% if(segmentItem.segmentID === segments.segmentID) { %>
		 							 			<% newArr1.push({"family" : recommendation.segmentBounds[flightIdx].fareFamily, "price" : recommendation.segmentBounds[flightIdx].fareSummary.fareTotal.totalAmount, "price2" : recommendation.fareSummary.fareTotal.totalAmount, "recommendationId" : recommendation.recommendationID}) %>
		 							 			<% return false %>
		 							 		<% } %>
		 								<% }) %>
							<% }); %>
							<% newArray[segments.segmentID] =  newArr1 %>
							<% console.log(newArr1) %>
				<% }); %>
				<% arr.push(newArray) %>
	<% }); %>
	<%
		_.each(arr, function(arr1, idx1){
			_.each(arr1, function(arr2, idx2){
				arr[idx1][idx2] = groupBy(arr2, 'family');
			})
		});
	%>

	<% _.each(data.flights, function(flights, arrIdx){ %>
		<p class="sub-heading-3--blue title"><%- flights.originAirportCode %> to <%- flights.destinationAirportCode %></p>
		<table class = "combinations-son">
		<% _.each(arr[arrIdx], function(segment, segmentID) { %>
			<tr>
				<th class="title-6--dark title" >segmentID</th>
				<td><%- segmentID %></td>
					<% _.each(flights.segments, function(flightsSegments, flightsSegmentsIdx){ %>
						<% if(flightsSegments.segmentID == segmentID) { %>
							<td><%- flightsSegments.departureDateTime.slice(10, 16) %> --- <%- flightsSegments.arrivalDateTime.slice(10, 16) %></td>
						<% } %>
				<% }) %>
				<% _.each(segment, function(segmentFlight){ %>
					<td>
						<span class="title-6--dark title" >
							<%- segmentFlight.type %>
						</span>
						<% _.each(segmentFlight.data, function(price){ %>
							<p>rcmId <%- price.recommendationId %> : <%- price.price2 %></p>
						<% }) %>
					</td>
				<% }) %>
			</tr>
		<% }) %>
		</table>
		<br><br>
	<% }); %>
<% } %>
