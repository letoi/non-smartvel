<%
    fareFamiliesGroup = [], familiesCabinGroup = {}, arrWrap = [];
    _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) {
       familiesCabinGroup[fareFamilies.cabinClassName] = fareFamilies.cabinClassName;
    });
%>

<%
    function groupBy(arr, key) {
     var newArr = [],
         family = {},
         newItem, i, j, cur;
     for (i = 0, j = arr.length; i < j; i++) {
         cur = arr[i];
         if (!(cur[key] in family)) {
             family[cur[key]] = {
                 type: cur[key],
                 data: []
             };
             newArr.push(family[cur[key]]);
         }
         family[cur[key]].data.push(cur);
     }
     return newArr;
    }
%>

<% var arr = [] %>
<% _.each(data.flights, function(flights, flightIdx1){ %>
  <% var newArray = [] %>
    <% _.each(flights.segments, function(segments, flightSegmentIdx){ %>
    <% var newArr1 = [] %>
      <% _.each(data.recommendations, function(recommendation, reIdx){ %>
        <% _.each(data.fareFamilies, function(fareFamiliesGroup, fareFamiliesGroupIdx){ %>
          <% _.each(recommendation.segmentBounds[flightIdx1].segments, function(segmentItem, segmentItemIdx){ %>
            <% if(segmentItem.segmentID === segments.segmentID && fareFamiliesGroup.fareFamily == recommendation.segmentBounds[flightIdx1].fareFamily ) { %>
              <% newArr1.push({
                "family" : recommendation.segmentBounds[flightIdx1].fareFamily,
                "familyGroup" : fareFamiliesGroup.cabinClassName,
                "price" : recommendation.fareSummary.fareTotal.totalAmount,
                "recommendationId" : recommendation.recommendationID,
                "displayLastSeat" : segmentItem.displayLastSeat,
                "numOfLastSeats" : segmentItem.numOfLastSeats,
                "eligibleOCRecommendationIDs" : segmentItem.eligibleOCRecommendationIDs
              }) %>
              <% return false %>
            <% } %>
          <% }) %>
        <% }) %>
      <% }); %>
      <% newArray[segments.segmentID] =  newArr1 %>
    <% }); %>
  <% arr.push(newArray) %>
<% }); %>

<%
    console.log('test');
    console.table(arr);
    _.each(arr, function(arr1, idx1) {
        _.each(arr1, function(arr2, idx2) {
            arr[idx1][idx2] = groupBy(arr2, 'family');
        })
    });
%>

<% _.each(flight.segments, function(segments, segmentsIdx) { %>
<%
  var departureTime = segments.departureDateTime,
  arrivalDateTime = segments.arrivalDateTime,
  newDepartureTime = departureTime.slice(11, 16),
  newArrivalDateTime = arrivalDateTime.slice(11, 16),
  departureSplit = departureTime.split(' '),
  departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
  arrivalDateTimeSplit = arrivalDateTime.split(' '),
  arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));

  var layoverTime = parseInt(segments.legs[0].layoverDuration);
  var hour = Math.floor(layoverTime / 3600);
  var min = Math.floor((layoverTime - (hour * 3600)) / 60);
  if (hour < 10) {hour = "0"+hour;}
  if (min < 10) {min = "0"+min;}
  var totalLayoverTime = hour+"hr "+min+ "mins";

  var secondsTotal = parseInt(segments.tripDuration);
  var hh2 = Math.floor(secondsTotal / 3600);
  var mm2 = Math.floor((secondsTotal - (hh2 * 3600)) / 60);
  if (hh2 < 10) {hh2 = "0"+hh2;}
  if (mm2 < 10) {mm2 = "0"+mm2;}
  var timeTotal = hh2+"hr "+mm2+ "mins";
  var timeTotalStation = hh2+"h "+mm2+ "m";
%>

<% _.each(arr[flightIdx], function(segment, segmentID) { %>
  <% if(segments.segmentID == segmentID) { %>
    <% if(segment && segment.length > 0) { %>
      <%
      var parseTimeToHour = function(layoverDuration){
      var secondsStop = parseInt(layoverDuration);
      var stopHr = Math.floor(secondsStop / 3600);
      var stopMM = Math.floor((secondsStop - (stopHr * 3600)) / 60);
      if (stopHr < 10) {stopHr = "0"+stopHr;}
      if (stopMM < 10) {stopMM = "0"+stopMM;}
      return stopHr+"h "+stopMM+ "m";
    }
%>

<%
    var nonStop = false, oneStop = false, twoStop = false, arr = [], check = [];

    var oneStopDetail = {},
    twoStopDetail = [];

    var countLayover = function(segment) {
      var count = [];

      _.map(segment.legs, function(leg){
        var stop = 0,
          stopTime = 0,
          duration = 0;

          leg.layoverDuration > 0 ? duration += 1 : duration = duration;
          leg.stops.length && _.map(leg.stops, function(stopObj){
              if(stopObj.layoverDuration > 0) {
                stop += 1;
                stopTime += stopObj.layoverDuration;
              } else {
                stop = stop;
                stopTime = stopTime;
              }
          });

        count.push({
          "leg" : duration,
          "stop" : stop,
          "stopObj" : {
            "layover" : stopTime,
            "code" : leg.stops.length && leg.stops[0].airportCode || '',
            "airlinenumber": leg.airlineFlightNumber,
            "departure" : leg.stops.length && leg.stops[0].departureDateTime || '',
            "arrival" : leg.stops.length && leg.stops[0].arrivalDateTime || ''
          },
          "legObj" : {
            "layover": leg.layoverDuration,
            "code" : leg.destinationAirportCode,
            "airlinenumber": leg.airlineFlightNumber,
            "departure" : leg.departureDateTime,
            "arrival" : leg.arrivalDateTime
          },
          "destination": segments.destinationAirportCode,
          "wcag" : [
          {
            "origin" : leg.originAirportCode,
            "depart" : leg.departureDateTime,
            "destination" : leg.stops.length && leg.stops[0].airportCode || '',
            "arrival": leg.stops.length && leg.stops[0].departureDateTime || '',
            "flightNumber" : leg.airlineFlightNumber,
            "layover" : leg.stops.length && leg.stops[0].layoverDuration
          },
          {
            "origin" : leg.stops.length ? leg.stops[0].airportCode : leg.originAirportCode,
            "depart" : leg.stops.length ? leg.stops[0].arrivalDateTime : leg.departureDateTime,
            "destination" : leg.destinationAirportCode,
            "arrival" : leg.arrivalDateTime,
            "flightNumber" : leg.airlineFlightNumber,
            "layover" : leg.layoverDuration
          }
          ]
        })
      })

      return count;
    }

    !segments.legs.length ? (nonStop = true) : (arr = countLayover(segments));

    if (arr.length === 1) {
      if (arr[0].leg && arr[0].stop) {
          twoStop = true;
      } else {
          if (arr[0].leg || arr[0].stop) {
              oneStop = true
          } else {
              nonStop = true
          };
      }
    } else {
      _.map(arr, function(data, idx) {
          if (data.leg && data.stop) {
              twoStop = true;
              return false
          }
          return check.push((data.leg || data.stop) ? true : false);
      });

      if (check.length > 1) {
          if (check[0] === true && check[1] === true) {
              twoStop = true;
          } else if (check[0] === false && check[1] === false) {
              nonStop = true;
          } else {
              oneStop = true;
          }
      }
    }

    if (oneStop) {
    var oneStopData = countLayover(segments);
    _.map(oneStopData, function(data) {
        if (data.legObj.code === data.destination) {
            data.stopObj.layover && (oneStopDetail = data.stopObj);
        } else {
            oneStopDetail = data.legObj;
        }
    })
    }

    if (twoStop) {
    var twoStopData = countLayover(segments);

    _.map(twoStopData, function(data) {
        if (data.stopObj.layover) {
            twoStopDetail.push(data.stopObj)
        }
        if (data.legObj.layover && data.legObj.code !== data.destination) {
            twoStopDetail.push(data.legObj)
        }
    })
    }
%>

<%
    var matchName = function(code) {
        var name;
         _.map(data.airports, function(airport) {
             if (airport.airportCode === code) {
                 name = airport.cityName;
                 return false;
             }
         });
         return name;
    }
%>

<%
    var checkPre = false;
    var info = {},
    arrCabin = [];

    _.map(data.recommendations, function(recommendation, reidx) {
        _.map(recommendation.segmentBounds[flightIdx].segments, function(boundSegment, boundSegmentID) {
          if (boundSegment.segmentID === segments.segmentID) {
            var cabinClass1,
              cabinClass2;
            _.map(boundSegment.legs, function(leg, legidx) {
              if (legidx === 0) {
                  cabinClass1 = leg.cabinClass;
              } else {
                  cabinClass2 = leg.cabinClass;
              }
            })
            if (cabinClass1 && cabinClass2 && (cabinClass1 !== cabinClass2)) {
              checkPre = true;
              arrCabin.push({
                  "cabinClass1": cabinClass1,
                  "cabinClass2": cabinClass2
              });
            }
          }
        })
    })

    if (checkPre) {
        if (arrCabin[0].cabinClass1 === 'Y') {
            info['header'] = 'Economy';
            info['flightNumber'] = segments.legs[0].airlineFlightNumber;
            info['origin'] = segments.legs[0].originAirportCode;
            info['destination'] = segments.legs[0].destinationAirportCode;
        }
        if (arrCabin[0].cabinClass2 === 'S') {
            info['header'] = 'Premium Economy'
            info['flightNumber'] = segments.legs[1].airlineFlightNumber;
            info['origin'] = segments.legs[1].originAirportCode;
            info['destination'] = segments.legs[1].destinationAirportCode;
        }
    }
%>
<div class="flight-list-item economy-flight-bgd" data-timetotal="<%- timeTotal %>" aria-describedby="flight-segment-<%- flightIdx %>-<% segmentsIdx %>" tabindex="0">
  <span class="says" id="flight-segment-<%- flightIdx %>-<% segmentsIdx %>">
    Flight <%- segments.legs[0].airlineFlightNumber %>,
    Departing from <% _.each(data.airports, function(airports, airportsIdx) { %>
    <% if( airports.airportCode === segments.originAirportCode ) { %>
      <%- airports.cityName %>
      at <%- airports.countryName %>
      <% if(segments.legs[0].departureTerminal) { %>
        Terminal <%- segments.legs[0].departureTerminal %>
      <% } %>
        on <%- departureDatepicker %>
        at <%- newDepartureTime %>
      <% } %>
    <% }); %>
    .Arriving in
    <% _.each(data.airports, function(airports, airportsIdx) { %>
      <% if( airports.airportCode === segments.destinationAirportCode ) { %>
        <%- airports.cityName %>
        at <%- airports.countryName %>
        <% if(segments.legs[0].arrivalTerminal) { %>
          Terminal <%- segments.legs[0].arrivalTerminal %>
        <% } %>
        on <%- arrivalDateTimeDatepicker %>
        at <%- newArrivalDateTime %>
      <% } %>
    <% }); %>,
    with layover time: <%- totalLayoverTime %>,
    total flight duration: <%- timeTotal %> ”
  </span>
  <span class="segment-id hidden"> <%- segmentID %> </span>
  <div class="recommended-table" data-stop="<%- nonStop ? '0' : (oneStop ? '1' : '2') %>">
    <div data-flight-item class="col-info recommended-flight-item">
      <div class="flight-station">
        <% if (nonStop) { %>
          <span class="stop-time">Non-stop • <%- timeTotal %></span>
        <% } %>
        <% if (oneStop) { %>
          <span class="stop-time">One-stop • <%- timeTotal %></span>
        <% } %>
        <% if (twoStop) { %>
          <span class="stop-time">Two-stop • <%- timeTotal %></span>
        <% } %>
        <div class="control-flight-station anim-all" data-first-wrap-flight>
          <div class="flight-station-item">
            <div class="flight-station--inner">
              <div class="flight-station-info">
                <div class="station-stop">
                  <% if(nonStop) { %>
                    <span class="station-stop-detail">
                      <em class="ico-airplane-2"></em>
                      <span class="time"><%- parseTimeToHour(segments.tripDuration) %></span>
                    </span>
                  <% } %>
                  <% if (oneStop) { %>
                    <span class="station-stop-detail one-stop-station">
                      <span class="time time--1">
                        <strong><%- oneStopDetail.code %> </strong><%- parseTimeToHour(oneStopDetail.layover) %>
                      </span>
                    </span>
                  <% } %>
                  <% if(twoStop) { %>
                    <% _.each(twoStopDetail, function(stopDetail, stopIdx){ %>
                      <span class="two-stop-station stop--<%- stopIdx + 1 %>">
                        <span class="time">
                          <strong><%- stopDetail.code %></strong>
                          <span><%- parseTimeToHour(stopDetail.layover) %></span>
                        </span>
                      </span>
                    <% }) %>
                  <% } %>
                </div>
                <div class="flights-station__info--detail">
                  <span class="hour"><%- segments.originAirportCode %> <%- newDepartureTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segments.originAirportCode ) { %>
                      <span class="country-name"><%- airports.countryName %></span><span class="date"><%- departureDatepicker %></span>
                    <% } %>
                  <% }); %>
                </div>
                <div class="flights-station__info--detail return-flight">
                  <span class="hour"><%- segments.destinationAirportCode %> <%- newArrivalDateTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segments.destinationAirportCode ) { %>
                      <span class="country-name"><%- airports.countryName %></span><span class="date"><%- arrivalDateTimeDatepicker %></span>
                    <% } %>
                  <% }); %>
                </div>
              </div>
              <div class="airline-info">
                <div class="inner-info">
                  <span class="airline-detail <% if(segments.legs[0].operatingAirline.code == "SQ") { %> singapore-logo <% } %> <% if(segments.legs[0].operatingAirline.code == "TR") { %> scoot-logo <% } %>">
                  <% if(segments.legs[0].operatingAirline.code == "SQ" || segments.legs[0].operatingAirline.code == "SI" || segments.legs[0].operatingAirline.code == "TR") { %>
                    <img src="images/svg/<%- segments.legs[0].operatingAirline.code.toLowerCase() %>.svg" alt="<%- segments.legs[0].operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
                  <% } %>
                    <strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong><%- segments.legs[0].airlineFlightNumber %>
                  </span>
                  <a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>More details</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div data-wrap-flight class="wrap-flight-station anim-all">
          <% _.each(segments.legs, function(segmentsLegs, legsIdx) { %>
          <%
          var departureTime = segmentsLegs.departureDateTime,
            arrivalDateTime = segmentsLegs.arrivalDateTime,
            newDepartureTime = departureTime.slice(11, 16),
            newArrivalDateTime = arrivalDateTime.slice(11, 16),
            departureSplit = departureTime.split(' '),
            departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
            arrivalDateTimeSplit = arrivalDateTime.split(' '),
            arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));
            var secondsLayover = parseInt(segmentsLegs.layoverDuration);
            var secondsFlight = parseInt(segmentsLegs.flightDuration);
            var hh2 = Math.floor(secondsLayover / 3600);
            var mm2 = Math.floor((secondsLayover - (hh2 * 3600)) / 60);
            if (hh2 < 10) {hh2 = "0"+hh2;}
            if (mm2 < 10) {mm2 = "0"+mm2;}
            var timeTotalLayover = hh2+"hr "+mm2+ "mins";
            var hh3 = Math.floor(secondsFlight / 3600);
            var mm3 = Math.floor((secondsFlight - (hh3 * 3600)) / 60);
            if (hh3 < 10) {hh3 = "0"+hh3;}
            if (mm3 < 10) {mm3 = "0"+mm3;}
            var timeTotalFlight = hh3+"h "+mm3+ "m";
            if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) {
              var departureStop = segmentsLegs.stops[0].departureDateTime,
              arrivalStop = segmentsLegs.stops[0].arrivalDateTime,
              newDepartureStop = departureStop.slice(11, 16),
              newArrivalStop = arrivalStop.slice(11, 16),
              departureSplitStop = departureStop.split(' '),
              arrivalSplitStop = arrivalStop.split(' '),
              departureDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(departureSplitStop[0].replace(/-/g,"/"))),
              arrivalDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(arrivalSplitStop[0].replace(/-/g,"/")));
              var secondsLayoverStop = parseInt(segmentsLegs.stops[0].layoverDuration);
              var hh4 = Math.floor(secondsLayoverStop / 3600);
              var mm4 = Math.floor((secondsLayoverStop - (hh4 * 3600)) / 60);
              if (hh4 < 10) {hh4 = "0"+hh4;}
              if (mm4 < 10) {mm4 = "0"+mm4;}
              var timeTotalLayoverStop = hh4+"hr "+mm4+ "mins";
            }
          %>
          <% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
          <div class="flight-station-item flight-result-leg anim-all">
            <div class="flight-station--inner">
              <div class="flight-station-info">
                <div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
                <div class="flights-station__info--detail">
                  <span class="hour" data-origin-hour="<%- segmentsLegs.originAirportCode %> <%- newDepartureTime %>"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
                    <span class="country-name" data-origin-countryname="<%- airports.countryName %>"><%- airports.countryName %></span>
                    <span class="date" data-origin-date="<%- departureDatepicker %>" data-origin-airportname="<%- airports.airportName %>" data-origin-terminal="<%- segmentsLegs.departureTerminal %>"><%- departureDatepicker %><br>
                      <%- airports.airportName %>
                    <% } %>
                  <% }); %>
                <br>
                  <% if(segmentsLegs.departureTerminal) { %>
                    Terminal <%- segmentsLegs.departureTerminal %>
                  <% } %>
                  </span>
                </div>
                <div class="flights-station__info--detail return-flight">
                  <span class="hour" data-destination-hour="<%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %>"><%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
                    <span class="country-name" data-destination-countryname="<%- airports.countryName %>"><%- airports.countryName
                     %></span>
                     <span class="date" data-destination-date="<%- departureDatepickerStop %>" data-destination-airportname="<%- airports.airportName %>"><%- departureDatepickerStop %><br>
                    <%- airports.airportName %></span>
                    <% } %>
                  <% }); %>
                </div>
              </div>
              <div class="airline-info">
                <div class="inner-info">
                  <span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.airlineFlightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
                  <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
                    <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
                  <% } %>
                    <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
                  </span>
                  <span class="name-plane"><%- segmentsLegs.aircraft.name %></span><% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
                    <span class="economy cabin-color hidden"><%- familiesCabinGroup %></span><% }); %>
                </div>
              </div>
            </div>

            <% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
              <span class="layover-time--1" data-layovertime="<%- timeTotalLayoverStop %>"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayoverStop %></span>
            <% } %>

            <div class="flight-station--inner">
              <div class="flight-station-info">
                <div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
                <div class="flights-station__info--detail">
                  <span class="hour" data-origin-hour="<%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %>"><%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
                    <span class="country-name" data-origin-countryname="<%- airports.countryName %>"><%- airports.countryName %></span>
                    <span class="date" data-origin-date="<%- arrivalDatepickerStop %>" data-origin-airportname="<%- airports.airportName %>"><%- arrivalDatepickerStop %><br>
                      <%- airports.airportName %></span>
                    <% } %>
                  <% }); %>
                </div>
                <div class="flights-station__info--detail return-flight">
                  <span class="hour" data-destination-hour="<%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %>"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
                    <span class="country-name" data-destination-countryname="<%- airports.countryName %>"><%- airports.countryName
                     %></span><span class="date" data-destination-date="<%- arrivalDateTimeDatepicker %>" data-destination-airportname="<%- airports.airportName %>" data-destination-terminal="<%- segmentsLegs.arrivalTerminal %>"><%- arrivalDateTimeDatepicker %><br>
                    <%- airports.airportName %>
                    <% } %>
                  <% }); %>
                <br>
                  <% if(segmentsLegs.arrivalTerminal) { %>
                    Terminal <%- segmentsLegs.arrivalTerminal %>
                    <% } %>
                    </span>
                </div>
              </div>
              <div class="airline-info">
                <div class="inner-info">
                  <span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.airlineFlightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
                  <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
                    <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
                  <% } %>
                    <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
                  </span>
                  <span class="name-plane"><%- segmentsLegs.aircraft.name %></span><% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
                    <span class="economy cabin-color hidden"><%- familiesCabinGroup %></span><% }); %><a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a>
                </div>
              </div>
            </div>
            <% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
              <span class="layover-time--1" data-layovertime="<%- timeTotalLayover %>"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayover %></span>
            <% } %>
          </div>
          <% } else { %>
          <div class="flight-station-item flight-result-leg anim-all">
            <div class="flight-station--inner">
              <div class="flight-station-info">
                <div class="station-stop" data-timeFlight="<%- timeTotalFlight %>"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
                <div class="flights-station__info--detail">
                  <span class="hour" data-origin-hour="<%- segmentsLegs.originAirportCode %> <%- newDepartureTime %>"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
                    <span class="country-name" data-origin-countryname="<%- airports.countryName %>"><%- airports.countryName %></span><span class="date" data-origin-date="<%- departureDatepicker %>" data-origin-airportname="<%- airports.airportName %>" data-origin-terminal="<%- segmentsLegs.departureTerminal %>"><%- departureDatepicker %><br>
                    <%- airports.airportName %>
                    <% } %>
                  <% }); %>
                  <br>
                  <% if(segmentsLegs.departureTerminal) { %>
                    Terminal <%- segmentsLegs.departureTerminal %> <% } %></span>
                </div>
                <div class="flights-station__info--detail return-flight">
                  <span class="hour" data-destination-hour="<%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %>"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
                  <% _.each(data.airports, function(airports, airportsIdx) { %>
                    <% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
                    <span class="country-name" data-destination-countryname="<%- airports.countryName %>"><%- airports.countryName
                     %></span>
                    <span class="date" data-destination-date="<%- arrivalDateTimeDatepicker %>" data-destination-airportname="<%- airports.airportName %>" data-destination-terminal="<%- segmentsLegs.arrivalTerminal %>"><%- arrivalDateTimeDatepicker %><br>
                      <%- airports.airportName %>
                      <% } %>
                    <% }); %>
                    <br>
                    <% if(segmentsLegs.arrivalTerminal) { %>
                      Terminal <%- segmentsLegs.arrivalTerminal %>  <% } %>
                    </span>
                </div>
              </div>
              <div class="airline-info">
                <div class="inner-info">
                  <span class="airline-detail" data-operationname="<%- segmentsLegs.operatingAirline.name %>" data-flightnumber="<%- segmentsLegs.airlineFlightNumber %>" data-planename="<%- segmentsLegs.aircraft.name %>">
                  <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
                    <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
                  <% } %>
                    <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
                  </span>
                  <span class="name-plane"><%- segmentsLegs.aircraft.name %></span>
                  <% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
                    <span class="economy cabin-color hidden"><%- familiesCabinGroup %></span>
                  <% }); %>
                  <a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a>
                </div>
              </div>
            </div>
            <% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
              <span class="layover-time--1" data-layovertime="<%- timeTotalLayover %>"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayover %></span>
            <% } %>
          </div>
          <% } %>
          <% }); %>
        </div>
      </div>
    </div>
    <% if(segments.segmentID == segmentID) { %>
      <% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
        <% if(familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') { %>
          <% if(segment && segment.length > 0) { %>
            <div data-trigger-animation="<%- flightIdx %>-<%- segmentsIdx %>-0" data-trigger-popup=".popup-table-flight" data-content-popup="block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-0" class="col-info col-info-left col-info-select economy-flight--green column-trigger-animation anim-all" tabindex="0">
              <span class="head-col">
                <span class="text-head"><%- familiesCabinGroup %></span>
              </span>
              <div class="flight-result-opt-row primary anim-all" data-result-opt-row-v1>
                <div class="flight-price"><span class="from">From</span>
                  <% var arrayItem = [] %>
                  <span class="price">
                    <% _.each(segment, function(segmentFlight){ %>
                      <% segmentFlight.data.sort(function(a, b) {
                        return parseFloat(a.price) - parseFloat(b.price);
                      }); %>
                      <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                        <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business") && segmentFlight.data.length > 0 ) { %>
                        <% arrayItem.push({"priceSort" : segmentFlight.data[0].price }) %>
                          <% return false %>
                      <% } %>
                      <% }); %>
                    <% }); %>
                    <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                      <% arrayItem.sort(function(a, b) {
                        return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                      }); %>
                      <% var priceStyleSmall = arrayItem[0].priceSort.toFixed(2).indexOf(".") ;
                       %>
                      <%- parseFloat(arrayItem[0].priceSort.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %>
                      <small><%- arrayItem[0].priceSort.toFixed(2).slice(priceStyleSmall) %></small>
                    <% } %>
                  </span>
                  <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                    <% arrayItem.sort(function(a, b) {
                      return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                    }); %>
                    <div class="price-cheapest-outbound hidden"></div>
                    <div class="price-cheapest-colum hidden"><%- arrayItem[0].priceSort %></div>
                  <% } %>
                  <span class="adult">1 adult</span><em class="ico-point-d"></em>
                </div>
                <span class="not-available"></span>
              </div>
              <div class="flight-result-opt-row optional anim-all hidden-mb-small" data-result-opt-row-v2>
                <% if(segment && segment.length > 0) { %>
                  <div class="flight-price">
                    <span class="select">Selected</span>
                    <% var arrayItem = [] %>
                    <span class="price">
                      <% _.each(segment, function(segmentFlight){ %>
                        <% segmentFlight.data.sort(function(a, b) {
                          return parseFloat(a.price) - parseFloat(b.price);
                        }); %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business") && segmentFlight.data.length > 0 ) { %>
                          <% arrayItem.push({"priceSort" : segmentFlight.data[0].price }) %>
                            <% return false %>
                        <% } %>
                        <% }); %>
                      <% }); %>
                      <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                        <% arrayItem.sort(function(a, b) {
                          return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                        }); %>
                        <% var priceStyleSmall = arrayItem[0].priceSort.toFixed(2).indexOf(".") ;
                         %>
                        <%- parseFloat(arrayItem[0].priceSort.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %><small><%- arrayItem[0].priceSort.toFixed(2).slice(priceStyleSmall) %></small>
                      <% } %>
                    </span>
                    <span class="adult">1 adult</span>
                    <em class="ico-point-u"></em>
                  </div>
                  <span class="not-available"></span>
                <% } %>
              </div>
            </div>
          <% } %>
        <% } else if(familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') { %>
            <% if(segment && segment.length > 0) { %>
              <div data-trigger-animation="<%- flightIdx %>-<%- segmentsIdx %>-1" data-trigger-popup=".popup-table-flight" data-content-popup="block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-1" class="col-info col-info-right col-info-select economy-flight--pey column-trigger-animation-1 anim-all" tabindex="0">
                <span class="head-col">
                  <span class="text-head"><%- familiesCabinGroup %></span>
                </span>
                <div class="flight-result-opt-row primary anim-all" data-result-opt-row-v1>
                  <div class="flight-price">
                    <span class="from">From</span>
                    <% var arrayItem = [] %>
                    <span class="price">
                      <% _.each(segment, function(segmentFlight){ %>
                        <% segmentFlight.data.sort(function(a, b) {
                          return parseFloat(a.price) - parseFloat(b.price);
                        }); %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First") && segmentFlight.data.length > 0 ) { %>
                            <% arrayItem.push({"priceSort" : segmentFlight.data[0].price }) %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                      <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                        <% arrayItem.sort(function(a, b) {
                          return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                        }); %>
                        <% var priceStyleSmall = arrayItem[0].priceSort.toFixed(2).indexOf(".") ;
                         %>
                        <%- parseFloat(arrayItem[0].priceSort.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %><small><%- arrayItem[0].priceSort.toFixed(2).slice(priceStyleSmall) %></small>
                      <% } %>
                    </span>
                    <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                        <% arrayItem.sort(function(a, b) {
                          return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                        }); %>
                      <div class="price-cheapest-outbound-1 hidden"></div>
                      <div class="price-cheapest-colum hidden"><%- arrayItem[0].priceSort %></div>
                    <% } %>
                    <span class="adult">1 adult</span>
                    <em class="ico-point-d"></em>
                  </div>
                  <span class="not-available"></span>
                </div>
                <div class="flight-result-opt-row optional anim-all hidden-mb-small" data-result-opt-row-v2>
                  <div class="flight-price">
                    <span class="select">Selected</span>
                    <% var arrayItem = [] %>
                    <span class="price">
                    <% _.each(segment, function(segmentFlight){ %>
                      <% segmentFlight.data.sort(function(a, b) {
                        return parseFloat(a.price) - parseFloat(b.price);
                      }); %>
                      <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                        <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First") && segmentFlight.data.length > 0 ) { %>
                          <% arrayItem.push({"priceSort" : segmentFlight.data[0].price }) %>
                          <% return false %>
                        <% } %>
                      <% }); %>
                    <% }); %>
                    <% if(arrayItem && arrayItem.length > 0 &&  arrayItem[0].priceSort) { %>
                      <% arrayItem.sort(function(a, b) {
                        return parseFloat(a.priceSort) - parseFloat(b.priceSort);
                      }); %>
                      <% var priceStyleSmall = arrayItem[0].priceSort.toFixed(2).indexOf(".") ;
                       %>
                      <%- parseFloat(arrayItem[0].priceSort.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %><small><%- arrayItem[0].priceSort.toFixed(2).slice(priceStyleSmall) %></small>
                    <% } %>
                    </span>
                    <span class="adult">1 adult</span>
                    <em class="ico-point-u"></em>
                  </div>
                  <span class="not-available"></span>
                </div>
              </div>
            <% } %>
        <% } %>
      <% }); %>
    <% } %>
  </div>
<% if(segments.segmentID == segmentID) { %>
  <% _.each(familiesCabinGroup, function(familiesCabinGroup, familiesCabinGroupIdx) { %>
    <% if(familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') { %>
      <div data-hidden-recommended data-segmentid="<%- segmentID %>" data-col-index="<%- flightIdx %>-<%- segmentsIdx %>-0" class="select-fare-block block-show-popup-mobile block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-0 anim-all">
            <div class="block-content-flight">
              <div class ="price-cheapest-colum hidden"></div>
              <div class="select-fare--inner">
                  <div class="title-popup-mb"><span class="sub-heading-2--blue">Select fare</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
                    <a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em>View PPS Club / KrisFlyer privileges</a>
                  </div>
                  <div class="description-fare-mb">
                    <span class="title-5--dark"><%= matchName(segments.originAirportCode) %> to <%= matchName(segments.destinationAirportCode) %></span>
                    <% if(nonStop) { %>
                      <span class="stop-time">Non-stop • <%- timeTotal %></span>
                    <% } %>
                    <% if(oneStop) { %>
                      <span class="stop-time">One-stop • <%- timeTotal %></span>
                    <% } %>
                    <% if(twoStop) { %>
                      <span class="stop-time">Two-stop • <%- timeTotal %></span>
                    <% } %>
                  </div>
                  <% _.each(segments.legs, function(segmentsLegs, legsIdx) { %>
                    <div class="scoot-info  <% if(segmentsLegs.operatingAirline.code !== "TR" ) { %> hidden <% } %>">
                      <div data-accordion-wrapper="1">
                        <div data-accordion-wrapper-content="1">
                          <div data-accordion="1" data-accordion-append>
                            <a href="#" class="group-title" data-accordion-trigger="1" aria-expanded="false">
                              <div class="scoot-thumb">
                                <% if(!$('html').hasClass('ie8')){%>
                                  <img src="images/svg/tr.svg" alt="SC Logo" longdesc="img-desc.html">
                                <% }else{%>
                                  <img src="images/scoot-img-large.jpg" alt="SC Logo" longdesc="img-desc.html">
                                <% }%>
                              </div>
                              <span class="scoot-title hidden-tb-dt">About scoot benefits</span>
                              <em class="ico-point-d hidden-tb-dt"></em>
                            </a>
                            <div class="scoot-content" data-accordion-content="1">
                              <div class="content">You'll be flying from Singapore to Seoul on Scoot, the low-cost carrier of the Singapore Airlines Group. On your flight, you'll enjoy:</div>
                              <div class="list-items">
                                <div class="item">
                                  <div class="img-thumb"><img src="images/icon-fly-bag-eat.png" alt="Icon fly bag eat" longdesc="img-desc.html">
                                  </div>
                                  <ul><li>Check-in baggage allowance</li><li>Complimentary hot meal on board</li></ul>
                                </div>
                              </div>
                              <div class="more-info">
                                <% if(checkPre && !_.isEmpty(info)) { %>
                                <a href="#" class="link-4" data-trigger-popup=".popup-view-our-partner-airlines"><em class="ico-point-r"></em>View fare conditions when flying with our partner airlines</a>
                                <% } else { %>
                                    <a href="#" class="link-4" data-trigger-popup=".popup-view-partner-airlines"><em class="ico-point-r"></em>View fare conditions when flying with Scoot</a>
                                <% } %>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <% }); %>
                  <% if($('body').hasClass('sk-ut-flight-search-b') && segments.segmentID === 3) { %>
                  <div class="alert-block checkin-alert" style="margin: 20px; padding-left: 10px; padding-right: 10px; background-color: #E4E9EF;">
                    <div class="inner">
                      <div class="alert__icon" style="padding-right: 10px;"><em class="ico-alert" style="color: #00266b; font-size: 20px; width: 20px; height: 20px;"></em></div>
                      <div class="alert__message" style="color: #00266b;">SQ 2627 is a flight operated by Scandinavian Airlines. Fare conditions for baggage allowance, seat selection, earning of miles and upgrading with miles indicated below are only for Singapore Airlines and SilkAir flights.<a href="#" data-trigger-popup=".popup-view-our-partner-airlines" tabindex="0"> View conditions for flights operated by partner airlines</a>
                      </div>
                    </div>
                  </div>
                  <% } %>
                  <div class="select-fare-table multy-column hidden-mb-small <% _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) { %> <% if(fareFamiliesIdx > 4) { %> five-fare <% } %> <% }); %> ">
                    <div class="row-head-select">
                      <div class="col-select fare-condition"><span>Fare conditions</span></div>
                      <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% if(fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') { %>
                              <div class="col-select economy-fs--green-<%- fareFamilyGroupIdx + 1 %>">
                                <% if(fareFamilyGroupIdx == 0) { %>
                                  <span class="label-status"></span>
                                <% } %>
                                <span class="name-header-family"><%- fareFamilyGroup.fareFamilyName %></span>
                                <span class="name-header-hidden hidden"><%- fareFamilyGroup.fareFamilyName %></span>
                              </div>
                            <% } %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select first-row">
                      <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
                     <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.baggage == 'Not allowed') { %>
                                    <span class="not-allowed baggage "><%- fareTypes.baggage %></span>
                                  <% } else if(fareTypes.baggage == 'Complimentary') { %>
                                    <span class="complimentary baggage "><%- fareTypes.baggage %></span>
                                  <% } else { %>
                                    <span class="fare-price baggage"><%- fareTypes.baggage %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select last-row">
                      <div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking </span></span></div>
                       <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                <% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");
                                %>
                                  <% if(seatSelectionIndexNote > 0) { %>
                                    <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                      <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } else if($.trim(fareTypes.seatSelection.slice(0, seatSelectionIndexNote )) == 'Complimentary') { %>
                                      <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } else { %>
                                      <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } %>
                                  <% } else if(seatSelectionIndexNote < 0) { %>
                                    <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                      <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                                      <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } else { %>
                                      <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                    <% } %>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select first-row">
                      <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
                       <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                                    <span class="not-allowed earn-krisFlyer "><%- fareTypes.earnKrisflyerMiles %></span>
                                  <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                                    <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                  <% } else { %>
                                    <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select last-row">
                      <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles</span></span></div>
                       <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                                    <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                  <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                                    <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                  <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                                      <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                          <em data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                                  <% } else { %>
                                    <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select first-row">
                      <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
                      <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.cancellation == 'Not allowed') { %>
                                    <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
                                  <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                                    <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
                                  <% } else { %>
                                    <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select">
                      <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
                      <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.bookingChange == 'Not allowed') { %>
                                    <span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
                                  <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                                    <span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
                                  <% } else { %>
                                    <span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select">
                      <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
                       <% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <% if(fareTypes.noShow == 'Not allowed') { %>
                                    <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
                                  <% } else if(fareTypes.noShow == 'Complimentary') { %>
                                    <span class="complimentary no-show"><%- fareTypes.noShow %></span>
                                  <% } else { %>
                                    <span class="fare-price no-show"><%- fareTypes.noShow %></span>
                                  <% } %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                    <div class="row-select row-footer-select">
                      <div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em>View PPS Club / KrisFlyer privileges</a>
                      </div>
                      <% _.each(segment, function(segmentFlight){ %>
                        <% segmentFlight.data.sort(function(a, b) {
                          return parseFloat(a.price) - parseFloat(b.price);
                        }); %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                <div class="col-select">
                                  <a href="#" class="btn-price column-left" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
                                    <span class="rcmid-corresponding hidden"></span>
                                    <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
                                    <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
                                    <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                                    <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                                     %>
                                    <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                                    <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                                    <span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                                    <em class="ico-check-thick"></em>
                                    <span class="header-family hidden"><%- fareFamilyGroup.fareFamilyName %></span>
                                  </a>
                                  <span class="fare-family-id hidden"><%-
                                  segmentFlight.data[0].family %></span>
                                  <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                                    <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
                                  <% } %>
                                  <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                                     <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                                    <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                                    <% } %>
                                  <% }); %>
                                </div>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %>
                    </div>
                  </div>
                  <div class="one-column hidden">
                    <div class="select-fare-table one-fare">
                      <div class="row-head-select">
                        <div class="col-select economy-fs--green-1"><span><% _.each(segment, function(segmentFlight){ %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% if(fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') { %>
                                <% if(fareFamilyGroupIdx == 0) { %>
                                <% } %>
                                <span class="name-header-family"><%- fareFamilyGroup.fareFamilyName %> fare conditions</span>
                                <span class="name-header-hidden hidden"><%- fareFamilyGroup.fareFamilyName %></span>
                            <% } %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %></span></div>
                      </div>
                      <div class="row-select">
                        <div class="col-select">
                          <div class="col-item col-item--1">
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.baggage == 'Not allowed') { %>
                                        <span class="not-allowed baggage"><%- fareTypes.baggage %></span>
                                      <% } else if(fareTypes.baggage == 'Complimentary') { %>
                                        <span class="complimentary baggage"><%- fareTypes.baggage %></span>
                                      <% } else { %>
                                        <span class="fare-price baggage"><%- fareTypes.baggage %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>

                                      <% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");

                                      %>

                                      <% if(seatSelectionIndexNote > 0) { %>
                                        <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                          <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } else if($.trim(fareTypes.seatSelection.slice(0, seatSelectionIndexNote )) == 'Complimentary') { %>
                                          <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } else { %>
                                          <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } %>
                                      <% } else if(seatSelectionIndexNote < 0) { %>
                                        <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                          <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                                          <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } else { %>
                                          <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                        <% } %>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                                        <span class="not-allowed earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                      <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                                        <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                      <% } else { %>
                                        <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                                        <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                      <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                                        <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                      <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                                    <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                          <em  data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                                      <% } else { %>
                                        <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                          </div>
                          <div class="col-item col-item--2">
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.cancellation == 'Not allowed') { %>
                                        <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
                                      <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                                        <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
                                      <% } else { %>
                                        <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                            <div class="item item-booking-change">
                              <div class="item--left"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.bookingChange == 'Not allowed') { %>
                                        <span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
                                      <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                                        <span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
                                      <% } else { %>
                                        <span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                            <div class="item">
                              <div class="item--left"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
                              <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
                              <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                  <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                    <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                      <% if(fareTypes.noShow == 'Not allowed') { %>
                                        <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
                                      <% } else if(fareTypes.noShow == 'Complimentary') { %>
                                        <span class="complimentary no-show"><%- fareTypes.noShow %></span>
                                      <% } else { %>
                                        <span class="fare-price no-show"><%- fareTypes.noShow %></span>
                                      <% } %>
                                    <% } %>
                                  <% }); %>
                                  <% return false %>
                                <% } %>
                              <% }); %>
                            <% }); %></div>
                            </div>
                          </div>
                          <div class="col-item col-item--3"><% _.each(segment, function(segmentFlight){ %>
                        <% segmentFlight.data.sort(function(a, b) {
                          return parseFloat(a.price) - parseFloat(b.price);
                        }); %>
                        <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                          <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                            <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                              <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                  <a href="#" class="btn-price column-left" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
                                  <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                                    <span class="rcmid-corresponding hidden"></span>
                                    <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
                                    <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
                                    <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                                    <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                                     %>
                                    <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                                    <span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                                    <em class="ico-check-thick"></em>
                                  </a>
                                  <span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
                                  <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                                    <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
                                  <% } %>
                                  <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                                     <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                                    <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                                    <% } %>
                                  <% }); %>
                              <% } %>
                            <% }); %>
                            <% return false %>
                          <% } %>
                        <% }); %>
                      <% }); %></div>
                        </div>
                      </div>
                      <div class="row-select last-row hidden-mb-small">
                        <div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em>View PPS Club / KrisFlyer privileges</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <% _.each(segment, function(segmentFlight){ %>
                    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                      <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                          <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode ) { %>
                            <div class="hidden-tb-dt table-economy-green" data-table-mobile>
                              <!-- table same desktop: will have tables width col in desktop so for each it -->
                              <div class="select-fare-table multy-column">
                                <div class="row-head-select">
                                  <div class="col-select economy-fs--green-<%- fareFamilyGroupIdx + 1 %>"><% if(fareFamilyGroupIdx == 0) { %><% } %><span class="name-header-family"><%- fareFamilyGroup.fareFamilyName %></span></div> </div><div class="row-select"><div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
                                  <div class="col-select">
                                    <% if(fareTypes.baggage == 'Not allowed') { %>
                                      <span class="not-allowed baggage"><%- fareTypes.baggage %></span>
                                    <% } else if(fareTypes.baggage == 'Complimentary') { %>
                                      <span class="complimentary baggage"><%- fareTypes.baggage %></span>
                                    <% } else { %>
                                      <span class="fare-price baggage"><%- fareTypes.baggage %></span>
                                    <% } %>
                                  </div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking</span></span></div>
                                  <div class="col-select">
                                    <% var seatSelectionIndexNote = fareTypes.seatSelection.indexOf("(");
                                    %>
                                    <% if(seatSelectionIndexNote > 0) { %>
                                      <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                        <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } else if($.trim(fareTypes.seatSelection.slice(0, seatSelectionIndexNote )) == 'Complimentary') { %>
                                        <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } else { %>
                                        <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } %>
                                    <% } else if(seatSelectionIndexNote < 0) { %>
                                      <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                                        <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                                        <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } else { %>
                                        <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                                      <% } %>
                                    <% } %>
                                  </div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
                                  <div class="col-select"><% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                                      <span class="not-allowed earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                    <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                                      <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                    <% } else { %>
                                      <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                                    <% } %></div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles </span></span></div>
                                  <div class="col-select"><% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                                      <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                    <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                                      <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                    <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                                    <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                          <em data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                                  <% } else { %>
                                      <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                                    <% } %></div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
                                  <div class="col-select"><% if(fareTypes.cancellation == 'Not allowed') { %>
                                      <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
                                    <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                                      <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
                                    <% } else { %>
                                      <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
                                    <% } %></div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
                                  <div class="col-select"><% if(fareTypes.bookingChange == 'Not allowed') { %>
                                      <span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
                                    <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                                      <span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
                                    <% } else { %>
                                      <span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
                                    <% } %></div>
                                </div>
                                <div class="row-select">
                                  <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
                                  <div class="col-select"><% if(fareTypes.noShow == 'Not allowed') { %>
                                      <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
                                    <% } else if(fareTypes.noShow == 'Complimentary') { %>
                                      <span class="complimentary no-show"><%- fareTypes.noShow %></span>
                                    <% } else { %>
                                      <span class="fare-price no-show"><%- fareTypes.noShow %></span>
                                    <% } %></div>
                                </div>
                                <div class="row-footer-select">
                                  <div class="col-select full">
                                    <% segmentFlight.data.sort(function(a, b) {
                                      return parseFloat(a.price) - parseFloat(b.price);
                                    }); %>
                                    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                                      <% if((familiesCabinGroup == 'Economy' || familiesCabinGroup == 'Business') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Economy" || segmentFlight.data[0].familyGroup == "Business")) { %>
                                        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                                          <% if((fareFamilyGroup.cabinClassName == 'Economy' || fareFamilyGroup.cabinClassName == 'Business') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                                            <a href="#" class="btn-price column-left " data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
                                              <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                                               %>
                                               <span class="rcmid-corresponding hidden"></span>
                                               <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
                                               <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
                                               <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                                               <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                                              <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                                              <span class="btn-price-cheapest"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                                              <em class="ico-check-thick"></em>
                                            </a>
                                            <span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
                                            <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                                              <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
                                            <% } %>
                                             <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                                               <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                                              <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                                              <% } %>
                                            <% }); %>
                                          <% } %>
                                        <% }); %>
                                        <% return false %>
                                      <% } %>
                                    <% }); %>
                                  </div>
                                </div>
                              </div>
                              <!-- end -->
                            </div>
                          <% } %>
                        <% }); %>
                        <% return false %>
                      <% } %>
                    <% }); %>
                  <% }); %>
                  <div class="button-group-1 hidden-tb-dt">
                    <input name="proceed-fare" value="Proceed" id="proceed-fare" type="button" class="btn-1 has-disabled" data-close>
                    <input name="close-fare" value="Close" id="close-fare" type="button" class="btn-7" data-close>
                  </div>
              </div>
            </div>
        <div class="upsell hidden hidden-mb-small">
          <div class="title-popup-mb"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
          </div>
          <div class="content"><span class="price-selected hidden"></span>Upgrade to <strong class="name-family"> Economy good</strong> for an additional <strong>SGD </strong><strong class="price-sgd"></strong>, and you'll enjoy:</div>
          <div class="list-items">
          <% if(!$('body').hasClass('sk-ut-workflow')) { %>
              <div class="item"><span class="thumb"><em class="ico ico-business-1"></em><strong class="item-info">+ 5kg</strong></span><span class="des">Additional baggage per passenger</span></div>
              <div class="item item-2"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info">50%</strong></span><span class="des">Earn more KrisFlyer miles with this flight</span></div>
              <div class="item item-3"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info">Flexibility</strong></span><span class="des">Save SGD 100 on booking changes</span></div>
              <div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info">Seats</strong></span><span class="des">Pay for seat selection at booking</span></div>
          <% } else { %>
            <div class="item"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info">50%</strong></span><span class="des">Earn 50% KrisFlyer miles with this flight</span></div>
            <div class="item"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info">Flexibility</strong></span><span class="des">Save USD 100 on booking changes</span></div>
            <div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info">Seats</strong></span><span class="des">Pay for seat selection at booking</span></div>
          <% } %>
          </div>
          <div class="form-group group-btn">
          <input type="submit" name="btn-keep-selection" id="btn-keep-selection" value="Keep my selection" class="btn-4">
          <input type="submit" name="btn-upgrade" id="btn-upgrade" value="Upgrade" class="btn-1">
          </div>
        </div>
      </div>
    <% } %>
    <% if(familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') { %>
      <div data-hidden-recommended-1 data-segmentid="<%- segmentID %>" data-col-index="<%- flightIdx %>-<%- segmentsIdx %>-1" class="select-fare-block block-show-popup-mobile block-show-popup-mobile-<%- flightIdx %>-<%- segmentsIdx %>-1 anim-all">
        <div class ="price-cheapest-colum hidden"></div>
        <div class="select-fare--inner">
        <div class="title-popup-mb"><span class="sub-heading-2--blue">Select fare</span><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
          <a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-premium-benefit--krisflyer"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>View KrisFlyer members benefits</a>
</div>
<div class="description-fare-mb">
<span class="title-5--dark"><%= matchName(segments.originAirportCode) %> to <%= matchName(segments.destinationAirportCode) %></span>
<% if(nonStop) { %>
  <span class="stop-time">Non-stop • <%- timeTotal %></span>
<% } %>
<% if(oneStop) { %>
  <span class="stop-time">One-stop • <%- timeTotal %></span>
<% } %>
<% if(twoStop) { %>
  <span class="stop-time">Two-stop • <%- timeTotal %></span>
<% } %>
</div>
<div class="additional-privileges">
<% if(checkPre && !_.isEmpty(info)) { %>
  <div class="notification-panel">
    <div class="inner">
      <div class="alert__icon"><em class="ico-info-round-fill"></em></div>
      <div class="alert__message"><%= info.header %> is not offered on flight <%= info.flightNumber %> from <%= info.origin %> to <%= info.destination %></div>
    </div>
  </div>
<% } %>
<div class="news-item">
  <h4 class="title-4--blue">Your Premium Economy Class inflight experience</h4>
  <div>
    <div class="col-item">
      <div class="col-inner">
        <img src="images/greater_comfort.png" alt="images 1"/>
        <div class="text-content">
          <h5 class="title-5--blue">Greater comfort</h5>
          <p class="desc">Sit back. All the way back, with a generous 38" seat pitch and 19.5" seat width.</p>
        </div>
      </div>
    </div>
    <div class="col-item">
      <div class="col-inner">
        <img src="images/greater_choices.png" alt="images 1"/>
        <div class="text-content">
          <h5 class="title-5--blue">Greater choices</h5>
          <p class="desc">Savour more meal choices from our Premium Economy Book the Cook menu.</p>
        </div>
      </div>
    </div>
    <div class="col-item">
      <div class="col-inner">
        <img src="images/priority_treatment.png" alt="images 1"/>
        <div class="text-content">
          <h5 class="title-5--blue">Priority treatment</h5>
          <p class="desc">Enjoy priority check-in, boarding and baggage handling.</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="select-fare-table multy-column hidden-mb-small third-fare">
<div class="row-head-select">
  <div class="col-select fare-condition"><span>Fare conditions</span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% if(fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') { %>
          <div class="col-select economy-fs--pey-<%- fareFamilyGroupIdx + 1 - fareFamilyGroupIdx %>"><span class="name-header-family"><%- fareFamilyGroup.fareFamilyName %></span>
          </div>
        <% } %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select first-row">
  <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.baggage == 'Not allowed') { %>
                <span class="not-allowed baggage"><%- fareTypes.baggage %></span>
              <% } else if(fareTypes.baggage == 'Complimentary') { %>
                <span class="complimentary baggage"><%- fareTypes.baggage %></span>
              <% } else { %>
                <span class="fare-price baggage"><%- fareTypes.baggage %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select last-row">
  <div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
              <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
              <% } else { %>
                <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select first-row">
  <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                <span class="not-allowed earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
              <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
              <% } else { %>
                <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select last-row">
  <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
              <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
              <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                  <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                        <em  data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                <% } else { %>
                <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select first-row">
  <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.cancellation == 'Not allowed') { %>
                <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
              <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
              <% } else { %>
                <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select">
  <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.bookingChange == 'Not allowed') { %>
                <span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
              <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                <span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
              <% } else { %>
                <span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select">
  <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
  <% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <% if(fareTypes.noShow == 'Not allowed') { %>
                <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
              <% } else if(fareTypes.noShow == 'Complimentary') { %>
                <span class="complimentary no-show"><%- fareTypes.noShow %></span>
              <% } else { %>
                <span class="fare-price no-show"><%- fareTypes.noShow %></span>
              <% } %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
<div class="row-select row-footer-select">
  <div class="col-select"><a href="#" class="link-4" data-trigger-popup=".popup-view-premium-benefit--krisflyer"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>View KrisFlyer members benefits</a>
  </div>
  <% _.each(segment, function(segmentFlight){ %>
    <% segmentFlight.data.sort(function(a, b) {
      return parseFloat(a.price) - parseFloat(b.price);
    }); %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
            <div class="col-select">
              <a href="#" class="btn-price col-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
              <span class="rcmid-corresponding hidden"></span>
              <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
              <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
              <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                 %>
                 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                 <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                <span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                  <em class="ico-check-thick"></em>
              </a>
              <span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
              <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
              <% } %>
               <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                 <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                <% } %>
              <% }); %>
            </div>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %>
</div>
</div>
<div class="one-column hidden">
<div class="select-fare-table one-fare">
  <div class="row-head-select">
    <div class="col-select economy-fs--green-1"><span><% _.each(segment, function(segmentFlight){ %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% if(fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') { %>
            <% if(fareFamilyGroupIdx == 0) { %>
            <% } %>
            <span class="name-header-family"><%- fareFamilyGroup.fareFamilyName %> fare conditions</span>
            <span class="name-header-hidden hidden"><%- fareFamilyGroup.fareFamilyName %></span>
        <% } %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %></span></div>
  </div>
  <div class="row-select">
    <div class="col-select">
      <div class="col-item col-item--1">
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.baggage == 'Not allowed') { %>
                    <span class="not-allowed baggage"><%- fareTypes.baggage %></span>
                  <% } else if(fareTypes.baggage == 'Complimentary') { %>
                    <span class="complimentary baggage"><%- fareTypes.baggage %></span>
                  <% } else { %>
                    <span class="fare-price baggage"><%- fareTypes.baggage %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking </span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                    <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                  <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                    <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                  <% } else { %>
                    <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                    <span class="not-allowed earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                  <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                    <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                  <% } else { %>
                    <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                    <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
                  <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                    <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
                  <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                  <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                        <em data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                  <% } else { %>
                    <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
      </div>
      <div class="col-item col-item--2">
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.cancellation == 'Not allowed') { %>
                    <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
                  <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                    <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
                  <% } else { %>
                    <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
        <div class="item item-booking-change">
          <div class="item--left"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.bookingChange == 'Not allowed') { %>
                    <span class="not-allowed booking-change"><%- fareTypes.bookingChange %></span>
                  <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                    <span class="complimentary booking-change"><%- fareTypes.bookingChange %></span>
                  <% } else { %>
                    <span class="fare-price booking-change"><%- fareTypes.bookingChange %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
        <div class="item">
          <div class="item--left"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
          <div class="item--right"><% _.each(segment, function(segmentFlight){ %>
          <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
            <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
              <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                  <% if(fareTypes.noShow == 'Not allowed') { %>
                    <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
                  <% } else if(fareTypes.noShow == 'Complimentary') { %>
                    <span class="complimentary no-show"><%- fareTypes.noShow %></span>
                  <% } else { %>
                    <span class="fare-price no-show"><%- fareTypes.noShow %></span>
                  <% } %>
                <% } %>
              <% }); %>
              <% return false %>
            <% } %>
          <% }); %>
        <% }); %></div>
        </div>
      </div>
      <div class="col-item col-item--3"><% _.each(segment, function(segmentFlight){ %>
    <% segmentFlight.data.sort(function(a, b) {
      return parseFloat(a.price) - parseFloat(b.price);
    }); %>
    <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
      <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
        <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
          <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
              <a href="#" class="btn-price column-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
                <span class="rcmid-corresponding hidden"></span>
                <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
                <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
                <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                 %>
                 <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                 <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                <span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                  <em class="ico-check-thick"></em>
              </a>
              <span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
              <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
              <% } %>
               <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                 <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                <% } %>
              <% }); %>
          <% } %>
        <% }); %>
        <% return false %>
      <% } %>
    <% }); %>
  <% }); %></div>
    </div>
  </div>
  <div class="row-select last-row hidden-mb-small">
    <div class="col-select"><a href="#" class="link-4 link-view-benefit" data-trigger-popup=".popup-view-premium-benefit--krisflyer" tabindex="0"><em class="ico-bow"></em>View PPS Club / KrisFlyer privileges</a>
    </div>
  </div>
</div>
</div>
<% _.each(segment, function(segmentFlight){ %>
<% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
  <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
    <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
      <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode ) { %>
        <div class="hidden-tb-dt table-economy-green" data-table-mobile>
          <div class="select-fare-table multy-column">
            <div class="row-head-select">
              <div class="col-select economy-fs--pey-1"><span><%- fareFamilyGroup.fareFamilyName %></span></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em><span>Baggage</span></span></div>
              <div class="col-select">
                <% if(fareTypes.baggage == 'Not allowed') { %>
                  <span class="not-allowed baggage"><%- fareTypes.baggage %></span>
                <% } else if(fareTypes.baggage == 'Complimentary') { %>
                  <span class="complimentary baggage"><%- fareTypes.baggage %></span>
                <% } else { %>
                  <span class="fare-price baggage"><%- fareTypes.baggage %></span>
                <% } %>
              </div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-1-preferred"></em><span>Seat selection at booking</span></span></div>
              <div class="col-select"><% if((fareTypes.seatSelection == 'Not allowed') || (fareTypes.seatSelection == 'Only available during online check-in')) { %>
                  <span class="not-allowed seat-selection"><%- fareTypes.seatSelection %></span>
                <% } else if(fareTypes.seatSelection == 'Complimentary') { %>
                  <span class="complimentary seat-selection"><%- fareTypes.seatSelection %></span>
                <% } else { %>
                  <span class="fare-price seat-selection"><%- fareTypes.seatSelection %></span>
                <% } %></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em><span>Earn KrisFlyer miles</span></span></div>
              <div class="col-select"><% if(fareTypes.earnKrisflyerMiles == 'Not allowed') { %>
                  <span class="not-allowed earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                <% } else if(fareTypes.earnKrisflyerMiles == 'Complimentary') { %>
                  <span class="complimentary earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                <% } else { %>
                  <span class="fare-price earn-krisFlyer"><%- fareTypes.earnKrisflyerMiles %></span>
                <% } %></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em><span>Upgrade with miles</span></span></div>
              <div class="col-select"><% if(fareTypes.upgradeWithMiles == 'Not allowed') { %>
                  <span class="not-allowed upgrade"><%- fareTypes.upgradeWithMiles %></span>
                <% } else if(fareTypes.upgradeWithMiles == 'Complimentary') { %>
                  <span class="complimentary upgrade"><%- fareTypes.upgradeWithMiles %></span>
                <% } else if(fareTypes.upgradeWithMiles == 'Allowed') { %>
                  <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                        <em data-cabin-code = "<%- fareTypes.fareFamilyCode %>" data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong></strong><p class='tooltip__text-2'></p></div>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade" aria-label="View more information"></em>
                  <% } else { %>
                  <span class="fare-price upgrade"><%- fareTypes.upgradeWithMiles %></span>
                <% } %></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
              <div class="col-select"><% if(fareTypes.cancellation == 'Not allowed') { %>
                  <span class="not-allowed cancellation"><%- fareTypes.cancellation %></span>
                <% } else if(fareTypes.cancellation == 'Complimentary') { %>
                  <span class="complimentary cancellation"><%- fareTypes.cancellation %></span>
                <% } else { %>
                  <span class="fare-price cancellation"><%- fareTypes.cancellation %></span>
                <% } %></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
              <div class="col-select"><% if(fareTypes.bookingChange == 'Not allowed') { %>
                  <span class="not-allowed"><%- fareTypes.bookingChange %></span>
                <% } else if(fareTypes.bookingChange == 'Complimentary') { %>
                  <span class="complimentary"><%- fareTypes.bookingChange %></span>
                <% } else { %>
                  <span class="fare-price"><%- fareTypes.bookingChange %></span>
                <% } %></div>
            </div>
            <div class="row-select">
              <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
              <div class="col-select"><% if(fareTypes.noShow == 'Not allowed') { %>
                  <span class="not-allowed no-show"><%- fareTypes.noShow %></span>
                <% } else if(fareTypes.noShow == 'Complimentary') { %>
                  <span class="complimentary no-show"><%- fareTypes.noShow %></span>
                <% } else { %>
                  <span class="fare-price no-show"><%- fareTypes.noShow %></span>
                <% } %></div>
            </div>
            <div class="row-footer-select">
              <div class="col-select full">
                <% segmentFlight.data.sort(function(a, b) {
                  return parseFloat(a.price) - parseFloat(b.price);
                }); %>
                <% _.each(data.fareFamilies, function(fareFamilyGroup, fareFamilyGroupIdx) { %>
                  <% if((familiesCabinGroup == 'Premium Economy' || familiesCabinGroup == 'First') && fareFamilyGroup.fareFamily == segmentFlight.type && (segmentFlight.data[0].familyGroup == "Premium Economy" || segmentFlight.data[0].familyGroup == "First")) { %>
                    <% _.each(familyData.fareTypes, function(fareTypes, fareTypesIdx) { %>
                      <% if((fareFamilyGroup.cabinClassName == 'Premium Economy' || fareFamilyGroup.cabinClassName == 'First') && fareFamilyGroup.fareFamilyCode.slice(0, -1) == fareTypes.fareFamilyCode) { %>
                        <a href="#" class="btn-price column-right" data-price-segment="<%- segmentFlight.data[0].price %>" data-header-class="<%- fareFamilyGroup.fareFamilyName %>">
                        <span class="rcmid-corresponding hidden"></span>
                        <span class="rcmid-out hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %> <% }); %></span>
                        <span class="rcmid hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.recommendationId %>   <%- segmentFlightData.price %>-<% }); %></span>
                        <span class="list-price hidden"><% _.each(segmentFlight.data, function(segmentFlightData){ %><%- segmentFlightData.price %> <% }); %></span>
                          <% var priceStyleSmall = segmentFlight.data[0].price.toFixed(2).indexOf(".") ;
                           %>
                           <span class="eligible-oc-recommendation-ids hidden"><%- segmentFlight.data[0].eligibleOCRecommendationIDs[0] %></span>
                          <span class="btn-price-cheapest-colum hidden"><%- segmentFlight.data[0].price %></span>
                          <span class="btn-price-cheapest-1"><%- parseFloat(segmentFlight.data[0].price.toFixed(2).slice(0, priceStyleSmall)).toLocaleString() %></span><span class="unit-small"><%- segmentFlight.data[0].price.toFixed(2).slice(priceStyleSmall) %></span>
                          <em class="ico-check-thick"></em>
                        </a>
                        <span class="fare-family-id hidden"><%- segmentFlight.data[0].family %></span>
                        <% if(segmentFlight.data[0].displayLastSeat == true) { %>
                          <span class="seat-left"><%- segmentFlight.data[0].numOfLastSeats %> seats left</span>
                        <% } %>
                         <% _.each(familyData.fareTypes, function(fareDataTypes, fareDataTypesIdx) { %>
                           <% if( segmentFlight.data[0].family.slice(0,3) == fareDataTypes.fareFamilyCode) { %>
                          <span class="index-of hidden"> <%- fareDataTypesIdx %> </span>
                          <% } %>
                        <% }); %>
                      <% } %>
                    <% }); %>
                    <% return false %>
                  <% } %>
                <% }); %>
              </div>
            </div>
          </div>
        </div>
      <% } %>
    <% }); %>
    <% return false %>
  <% } %>
<% }); %>
<% }); %>
<div class="button-group-1 hidden-tb-dt">
<input name="proceed-fare-1" value="Proceed" id="proceed-fare-1" type="button" class="btn-1 has-disabled" data-close>
<input name="close-fare-1" value="Close" id="close-fare-1" type="button" class="btn-7" data-close>
</div>
</div>
</div>
<% } %>
<% }); %>
<% } %>
<div class="change-flight-item bgd-white hidden">
<div data-flight-item class="col-info recommended-flight-item">
<div class="flight-station">
<% if (nonStop) { %>
<span class="stop-time">Non-stop • <%- timeTotal %></span>
<% } %>
<% if (oneStop) { %>
<span class="stop-time">One-stop • <%- timeTotal %></span>
<% } %>
<% if (twoStop) { %>
<span class="stop-time">Two-stop • <%- timeTotal %></span>
<% } %>
<div class="control-flight-station anim-all" data-first-wrap-flight>
<div class="flight-station-item">
<div class="flight-station--inner">
<div class="flight-station-info">
  <div class="station-stop">
    <% if(nonStop) { %>
      <span class="station-stop-detail">
        <em class="ico-airplane-2"></em>
        <span class="time"><%- parseTimeToHour(segments.tripDuration) %></span>
      </span>
    <% } %>
    <% if (oneStop) { %>
      <span class="station-stop-detail one-stop-station">
        <span class="time time--1">
          <strong><%- oneStopDetail.code %> </strong><%- parseTimeToHour(oneStopDetail.layover) %>
        </span>
      </span>
    <% } %>
    <% if(twoStop) { %>
      <% _.each(twoStopDetail, function(stopDetail, stopIdx){ %>
        <span class="two-stop-station stop--<%- stopIdx + 1 %>">
          <span class="time">
            <strong><%- stopDetail.code %></strong>
            <span><%- parseTimeToHour(stopDetail.layover) %></span>
          </span>
        </span>
      <% }) %>
    <% } %>
  </div>
  <div class="flights-station__info--detail">
    <div class="code-origin-airport hidden"><%- segments.originAirportCode %></div>
    <span class="hour"><%- segments.originAirportCode %> <%- newDepartureTime %></span>
    <% _.each(data.airports, function(airports, airportsIdx) { %>
      <% if( airports.airportCode === segments.originAirportCode ) { %>
        <span class="country-name"><%- airports.countryName %></span><span class="date"><%- departureDatepicker %></span>
      <% } %>
    <% }); %>
  </div>
  <div class="flights-station__info--detail return-flight">
    <div class="code-destination-airport hidden"><%- segments.destinationAirportCode %></div>
    <span class="hour"><%- segments.destinationAirportCode %> <%- newArrivalDateTime %></span>
    <% _.each(data.airports, function(airports, airportsIdx) { %>
      <% if( airports.airportCode === segments.destinationAirportCode ) { %>
        <span class="country-name"><%- airports.countryName %></span><span class="date"><%- arrivalDateTimeDatepicker %></span>
      <% } %>
    <% }); %>
  </div>
</div>
<div class="airline-info">
  <div class="inner-info">
    <span class="airline-detail">
    <% if(segments.legs[0].operatingAirline.code == "SQ" || segments.legs[0].operatingAirline.code == "SI" || segments.legs[0].operatingAirline.code == "TR") { %>
      <img src="images/svg/<%- segments.legs[0].operatingAirline.code.toLowerCase() %>.svg" alt="<%- segments.legs[0].operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
    <% } %>
      <strong><%- segments.legs[0].operatingAirline.name %><span> •</span></strong><%- segments.legs[0].airlineFlightNumber %>
    </span>
    <a href="#" class="link-4 more-detail" data-more-details-table="true"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>More details</a>
  </div>
</div>
</div>
</div>
</div>
<div data-wrap-flight class="wrap-flight-station anim-all">
<% _.each(segments.legs, function(segmentsLegs, legsIdx) { %>
<%
var departureTime = segmentsLegs.departureDateTime,
  arrivalDateTime = segmentsLegs.arrivalDateTime,
  newDepartureTime = departureTime.slice(11, 16),
  newArrivalDateTime = arrivalDateTime.slice(11, 16),
  departureSplit = departureTime.split(' '),
  departureDatepicker = $.datepicker.formatDate('dd M (D)', new Date(departureSplit[0].replace(/-/g,"/"))),
  arrivalDateTimeSplit = arrivalDateTime.split(' '),
  arrivalDateTimeDatepicker = $.datepicker.formatDate('dd M (D)', new Date(arrivalDateTimeSplit[0].replace(/-/g,"/")));
  var secondsLayover = parseInt(segmentsLegs.layoverDuration);
  var secondsFlight = parseInt(segmentsLegs.flightDuration);
  var hh2 = Math.floor(secondsLayover / 3600);
  var mm2 = Math.floor((secondsLayover - (hh2 * 3600)) / 60);
  if (hh2 < 10) {hh2 = "0"+hh2;}
  if (mm2 < 10) {mm2 = "0"+mm2;}
  var timeTotalLayover = hh2+"hr "+mm2+ "mins";
  var hh3 = Math.floor(secondsFlight / 3600);
  var mm3 = Math.floor((secondsFlight - (hh3 * 3600)) / 60);
  if (hh3 < 10) {hh3 = "0"+hh3;}
  if (mm3 < 10) {mm3 = "0"+mm3;}
  var timeTotalFlight = hh3+"h "+mm3+ "m";
  if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) {
    var departureStop = segmentsLegs.stops[0].departureDateTime,
    arrivalStop = segmentsLegs.stops[0].arrivalDateTime,
    newDepartureStop = departureStop.slice(11, 16),
    newArrivalStop = arrivalStop.slice(11, 16),
    departureSplitStop = departureStop.split(' '),
    arrivalSplitStop = arrivalStop.split(' '),
    departureDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(departureSplitStop[0].replace(/-/g,"/"))),
    arrivalDatepickerStop = $.datepicker.formatDate('dd M (D)', new Date(arrivalSplitStop[0].replace(/-/g,"/")));
    var secondsLayoverStop = parseInt(segmentsLegs.stops[0].layoverDuration);
    var hh4 = Math.floor(secondsLayoverStop / 3600);
    var mm4 = Math.floor((secondsLayoverStop - (hh4 * 3600)) / 60);
    if (hh4 < 10) {hh4 = "0"+hh4;}
    if (mm4 < 10) {mm4 = "0"+mm4;}
    var timeTotalLayoverStop = hh4+"hr "+mm4+ "mins";
  }
%>
<% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
<div class="flight-station-item flight-result-leg anim-all">
  <div class="flight-station--inner">
    <div class="flight-station-info">
      <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
      <div class="flights-station__info--detail">
        <span class="hour"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
        <% _.each(data.airports, function(airports, airportsIdx) { %>
          <% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
          <span class="country-name"><%- airports.countryName %></span>
          <span class="date"><%- departureDatepicker %><br>
            <%- airports.airportName %>
          <% } %>
        <% }); %>
      <br>
        <% if(segmentsLegs.departureTerminal) { %>
          Terminal <%- segmentsLegs.departureTerminal %> </span>
        <% } %>
      </div>
      <div class="flights-station__info--detail return-flight">
        <span class="hour"><%- segmentsLegs.stops[0].airportCode %> <%- newDepartureStop %></span>
        <% _.each(data.airports, function(airports, airportsIdx) { %>
          <% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
          <span class="country-name"><%- airports.countryName
           %></span>
           <span class="date"><%- departureDatepickerStop %><br>
          <%- airports.airportName %></span>
          <% } %>
        <% }); %>
      </div>
    </div>
    <div class="airline-info">
      <div class="inner-info">
        <span class="airline-detail">
        <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
          <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
        <% } %>
          <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
        </span>
        <span class="name-plane"><%- segmentsLegs.aircraft.name %></span><span class="economy">Economy</span></div>
    </div>
  </div>

  <% if(segmentsLegs.stops && segmentsLegs.stops.length !== 0) { %>
    <span class="layover-time--1"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayoverStop %></span>
  <% } %>

  <div class="flight-station--inner">
    <div class="flight-station-info">
      <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
      <div class="flights-station__info--detail">
        <span class="hour"><%- segmentsLegs.stops[0].airportCode %> <%- newArrivalStop %></span>
        <% _.each(data.airports, function(airports, airportsIdx) { %>
          <% if( airports.airportCode === segmentsLegs.stops[0].airportCode ) { %>
          <span class="country-name"><%- airports.countryName %></span>
          <span class="date"><%- arrivalDatepickerStop %><br>
            <%- airports.airportName %></span>
          <% } %>
        <% }); %>
      </div>
      <div class="flights-station__info--detail return-flight">
        <span class="hour"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
        <% _.each(data.airports, function(airports, airportsIdx) { %>
          <% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
          <span class="country-name"><%- airports.countryName
           %></span><span class="date"><%- arrivalDateTimeDatepicker %><br>
          <%- airports.airportName %>
          <% } %>
        <% }); %>
      <br>
        <% if(segmentsLegs.arrivalTerminal) { %>
          Terminal <%- segmentsLegs.arrivalTerminal %>
          <% } %>
          </span>
      </div>
    </div>
    <div class="airline-info">
      <div class="inner-info">
        <span class="airline-detail">
        <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
          <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
        <% } %>
          <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
        </span>
        <span class="name-plane"><%- segmentsLegs.aircraft.name %></span><span class="economy">Economy</span><a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a></div>
    </div>
  </div>
  <% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
    <span class="layover-time--1"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayover %></span>
  <% } %>
</div>
<% } else { %>
<div class="flight-station-item flight-result-leg anim-all">
  <div class="flight-station--inner">
    <div class="flight-station-info">
      <div class="station-stop"><span class="station-stop-detail"><em class="ico-airplane-2"></em><span class="time"><%- timeTotalFlight %></span></span></div>
      <div class="flights-station__info--detail"><span class="hour"><%- segmentsLegs.originAirportCode %> <%- newDepartureTime %></span>
      <% _.each(data.airports, function(airports, airportsIdx) { %>
        <% if( airports.airportCode === segmentsLegs.originAirportCode ) { %>
        <span class="country-name"><%- airports.countryName %></span><span class="date"><%- departureDatepicker %><br>
        <%- airports.airportName %>
        <% } %>
      <% }); %>
      <br>
      <% if(segmentsLegs.departureTerminal) { %>
        Terminal <%- segmentsLegs.departureTerminal %> </span><% } %></div>
      <div class="flights-station__info--detail return-flight">
        <span class="hour"><%- segmentsLegs.destinationAirportCode %> <%- newArrivalDateTime %></span>
        <% _.each(data.airports, function(airports, airportsIdx) { %>
          <% if( airports.airportCode === segmentsLegs.destinationAirportCode ) { %>
          <span class="country-name"><%- airports.countryName
           %></span><span class="date"><%- arrivalDateTimeDatepicker %><br>
          <%- airports.airportName %>
          <% } %>
        <% }); %>
        <br>
        <% if(segmentsLegs.arrivalTerminal) { %>
          Terminal <%- segmentsLegs.arrivalTerminal %> </span> <% } %>
      </div>
    </div>
    <div class="airline-info">
      <div class="inner-info">
        <span class="airline-detail">
        <% if(segmentsLegs.operatingAirline.code == "SQ" || segmentsLegs.operatingAirline.code == "SI" || segmentsLegs.operatingAirline.code == "TR" ) { %>
          <img src="images/svg/<%- segmentsLegs.operatingAirline.code.toLowerCase() %>.svg" alt="<%- segmentsLegs.operatingAirline.code.toLowerCase() %> Logo" longdesc="img-desc.html">
        <% } %>
          <strong><%- segmentsLegs.operatingAirline.name %><span> •</span></strong><%- segmentsLegs.airlineFlightNumber %>
        </span>
        <span class="name-plane"><%- segmentsLegs.aircraft.name %></span><span class="economy">Economy</span><a href="#" class="link-4 less-detail" data-less-details-table="true" tabindex="-1"><em class="ico-point-r"><span class="ui-helper-hidden-accessible">More info</span></em>Less details</a></div>
    </div>
  </div>
  <% if(segmentsLegs.layoverDuration && segmentsLegs.layoverDuration !== 0) { %>
    <span class="layover-time--1"><em class="ico-flight-history"></em>Layover time: <%- timeTotalLayover %></span>
  <% } %>
</div>
<% } %>
<% }); %>
</div>
</div>
<div class="right-flight">
<div class="hidden-tb-dt flight-price economy-flight--green">
<span class="cabin-flight">
<span class="name-cabin">Economy</span>
<span class="adult">1 Adult</span>
<span class="from">from</span>
</span>
<span class="price">888.<small>00</small></span>
</div>
<div class="button-group-1">
<input type="button" name="change-button" id="change-button" value="Change" class="btn-8 button-change"/>
</div>
</div>
</div>
</div>
<!--   <% if(!$('html').hasClass('ie8')) { %> -->
</div>
  <!-- <% } %> -->
<% } %>
<% } %>
<% }); %>
<% }); %>
