<% if(data) { %>
  <div class="add-ons-item add-ons-item-added">
    <div class="car-infor">
      <div class="item-content-head">
        <div class="item-col-1">
          <figure><img src="<%- data.largeImageURL %>" alt="<- data.vehicleName %>" longdesc="img-desc.html">
          </figure>
        </div>
        <div class="item-col-2">
          <div class="content">
            <h3 class="sub-heading-2--blue head"><%= data.vehicleName %></h3>
            <div class="desc">
              <p class="sub-head"><%= data.carSize.description %></p>
            </div>
            <ul class="list-furnished">
              <li><%= data.seats %> Seats</li>
              <li><%= data.doors %> Doors</li>
              <li><%= data.bigSuitcase %> Large bags</li>
            </ul>
            <ul class="list-furnished">
              <% if(data.aircon === 'Yes') { %>
                <li>Air Conditioning</li>
              <% } %>
              <% if(data.automatic === 'Automatic tranmission') { %>
                <li>Automatic Tranmission</li>
              <% } else { %>
                <li>Manual Tranmission</li>
              <% } %>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="add-ons-item__content item-row">
      <figure class="img-block"><img src="images/rental-car-added.jpg" alt="photo-hotel" longdesc="img-desc.html">
      </figure>
      <ul class="info-details-1 style">
        <li><span>Pick up:</span><%= pickupDate %></li>
        <li><span>Location:</span><%= data.route.pickUp.locationName %></li>
      </ul>
      <ul class="info-details-1 style">
        <li><span>Drop off:</span><%= dropoffDate %></li>
        <li><span>Location:</span><%= data.route.dropOff.locationName %></li>
      </ul>
    </div>
  </div>
<% } %>
