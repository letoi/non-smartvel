<% if(data) {%>
  <% if(data.paxDetails) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title active">
          <h3 class="title-4--blue main-title">Passengers and flight add-ons</h3>
          <h3 class="title-5--dark sub-total">Grand total <%- data.currency %> <span>300.00</span></h3>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
          <div class="booking-info-group">
            <% if(idxPaxDetails == 0) {%>
              <div aria-expanded="true" class="group-title active">
            <% } else { %>
               <div aria-expanded="true" class="group-title">
            <% } %>
              <h4 class="title-5--blue"><%- flightPaxDetails.paxName %></h4>
            </div>
            <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
              <div class="booking-info-content">
                <h5 class="title-5--blue"><%- flightDetailsPerFlight.flightNo %> - Singapore to San Francisco</h5>
                <% if(flightDetailsPerFlight.addonPerPax) {%>
                  <div class="bundle-info"><span><strong>Bundle added - </strong></span><span data-remove-last-add><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %><%- flightAddonPerPax.description.concat(' + ') %><% }); %></span></div>
                <% } %>
                <div class="booking-info--2">
                  <div class="booking-info--1__item">
                    <div class="booking-details__group">
                      <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                        <% if(flightAddonPerPax.type == "Seat") {%>
                          <div class="booking-details booking-detail--1">
                            <div class="booking-col col-1"><em class="ico-change-seat"></em>
                            </div>
                            <div class="booking-col col-2">
                              <div aria-label="Seats" class="Seats">Seats</div>
                            </div>
                            <div class="booking-col col-3">
                              <div class="align-wrapper">
                                <div class="align-inner">
                                  <div class="has-cols">
                                    <p class="target-info"><%- flightDetailsPerFlight.from %> TO <%- flightDetailsPerFlight.to %></p>
                                    <p class="seat-info"><%- flightAddonPerPax.seatNumber %> <% if(flightAddonPerPax.isPreferredSeat == true) {%>(Preferred Seat)<% } %></p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <% } %>
                        <% if(flightAddonPerPax.type == "Additional Baggage") {%>
                          <div class="booking-details booking-detail--1">
                            <div class="booking-col col-1"><em class="ico-business-1"></em>
                            </div>
                            <div class="booking-col col-2">
                              <div aria-label="Baggage" class="Baggage">Baggage</div>
                            </div>
                            <div class="booking-col col-3">
                              <div class="align-wrapper">
                                <div class="align-inner">
                                  <div class="has-cols">
                                    <p class="target-info">Included</p>
                                    <p class="seat-info"><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                                      <% if(flightAddonPerPax.freeBaggagePiece != null) {%>
                                       <%- flightAddonPerPax.freeBaggagePiece %>kg
                                      <% } %>
                                      <% if(flightAddonPerPax.ffpAllowanceDesc != null) {%><br>
                                        <%- flightAddonPerPax.freeBaggageWeight %>kg
                                        as <%- flightAddonPerPax.ffpAllowanceDesc %> member
                                      <% } %>
                                    <% }); %>
                                  </div>
                                  <div class="has-cols">
                                    <p class="target-info">Purchased</p>
                                    <p class="seat-info"><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %> <%- flightAddonPerPax.additionalBaggageWeight %><% }); %>kg</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <% } %>
                      <% }); %>
                    </div>
                  </div>
                </div>
              </div>
            <% }); %>
          </div>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total has-link">
          <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
            <div data-tabindex="true" class="flights-cost">
              <h4 class="flights-cost-title"><span class="text-left">Flights add-ons</span><span class="text-right"><%- data.currency %></span></h4>
              <ul class="flights-cost__details">
                <li><span><%- flightPaxDetails.paxName %></span><span class="price"></span></li>
                <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                  <% if(flightDetailsPerFlight.addonPerPax) {%>
                    <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                      <li><span>․ Bundle for <%- flightDetailsPerFlight.from %> - <%- flightDetailsPerFlight.to %></span>
                      <span class="price"><%- flightAddonPerPax.amount.toFixed(2) %></span>
                      </li>
                    <% }); %>
                  <% } %>
                <% }); %>
                <li class="sub-total"><span>Sub-total</span><span class="price">160.00</span>
                </li>
              </ul>
              <div class="grand-total"><span>Grand total</span>
                <div class="grand-total-content">
                  <div class="text-total">SGD <span>3,971.80</span></div>
                  <div class="des">Includes discounts</div>
                </div>
              </div>
            </div>
          <% }); %>
        </div>
      </div>
    </div>
  <% } %>
<% } %>
