<aside class="toolbar toolbar--language">
	<div class="toolbar__content">
		<p class="toolbar__text">This page is in&nbsp;<span class="toolbar__language">Simplified Chinese</span>. Would you like to display it in&nbsp;<span class="toolbar__language">English</span>?</p>
		<ul class="toolbar__control">
			<li><a href="#" data-lang-toolbar="true">Yes</a><span>|</span>
			</li>
			<li><a href="#" data-lang-toolbar="false">No</a>
			</li>
		</ul>
		<a class="toolbar__close" href="#"></a>

	</div>
</aside>
