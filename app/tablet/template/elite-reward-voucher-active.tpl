<% if(data !== void 0){ %>
	<div class="voucher">
		<span class="voucher__heading">Active Vouchers</span>
		<% _.each(data, function(data, dataIdx){ %>
				<div class="voucher__group js-voucher-item">
					<div class="voucher__group__detail">
						<div class="voucher__group__detail__voucher">
              <div class="voucher__group__detail__title">
                <span class="voucher__icon voucher__icon--<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>double<%} else { %>seat<% } %>"></span>
                <span>
                  <%= data.rewardType %>
                </span>
              </div>
							<span class="voucher__group__detail__voucher-code">voucher code <%- data.rewardNumber%></span>
						</div>
						<div class="voucher__group__detail__use-voucher">
							<p class="voucher__group__detail__voucher-time"><%- data.rewardExpiryDate %></p>
							<a href="#" class="btn-1">use voucher</a>
						</div>
						<div class="voucher__group__detail__link">
							<% if(data.type == "EarnDoubleKrisFlyerMiles") { %>
							<p class="voucher__group-how">
								<a href="#" class="ico-point-r js-lightbox" data-trigger-popup=".popup--how-to-use">How to use</a>
							</p>
							<% } %>
							<p class="voucher__group-term <% if(data.type != "EarnDoubleKrisFlyerMiles") { %>no-float<% } %>">
								<a href="javascript:void(0);" class="ico-point-r js-lightbox" data-trigger-popup=".popup--term-and-condition">Terms and conditions</a>
							</p>
						</div>
					</div>
				</div>
		<% }) %>
	</div>
<% } %>

<aside class="popup popup--how-to-use hidden">
	<div class="popup__inner">
		<div class="popup__content"><a href="#" class="popup__close"><span class="ui-helper-hidden-accessible">Close</span>&#xe60d;</a>
			<h2 class="popup__heading">How to use double KrisFlyer miles voucher</h2>
			<div class="popup__description">
				Simply select this Elite Reward and apply it to the flight segment of your choice in your bookings. This Elite Reward can only be used by the principal Elite Gold member. Please allow 2-4 weeks for the KrisFlyer miles to be reflected in your account.
			</div>

			<div class="popup__sub-heading">How miles are calculated</div>
			<div class="popup__description">As an example, if a PPS Club member travels on Business Class from Singapore to London:</d>
			<table class="popup__table">
				<tr>
					<td class="popup__table-text">Base miles earned</td>
					<td class="popup__table-value">6,761 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Cabin bonus (1.25X)</td>
					<td class="popup__table-value">6.761 x 1.25 = 8,452 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Double miles accrual voucher</td>
					<td class="popup__table-value">8,452 x 2 = 16,904 miles</td>
				</tr>
				<tr>
					<td class="popup__table-text">Tier bonus (0.25X)</td>
					<td class="popup__table-value">16,904 x 0.25 = 18,595 miles</td>
				</tr>
			</table>
			<div class="popup__description">The double miles accrual bonus is applied after the cabin bonus and before the tier bonus.</div>
		</div>
	</div>
</aside>
