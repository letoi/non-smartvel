<% if(data) { %>
  <% var showBlock = false; %>
  <% _.each(data.passenger, function(passengers, passengerIDx){ %>
    <%
      if(typeof passengers.packSeat === 'undefined' || passengers.packSeat === '') {
        showBlock = true;
      }
    %>
  <% }); %>
	<div class="accordion-seat-selection <% if(showBlock) { %> show <% } %>" data-block-accordion-item="">
		<a href="#" data-accordion-trigger="1" aria-expanded="true" class="group-title active">
	  <h4 class="title-5--blue main-title">Passenger privileges</h4><em class="ico-point-d"></em></a>
	  <div data-accordion-content="1" class="table-seat-selection" style="display: block;">
	    <table class="table-responsive">
	      <thead class="hidden-mb">
	        <tr>
	          <th>Passengers</th>
	          <th>Standard Seat</th>
	          <th>Forward Zone Seat</th>
	          <th>Extra Legroom Seat</th>
	        </tr>
	      </thead>
	      <tbody>
					<% _.each(data.passenger, function(passengers){ %>
						<%
							var passengerID = passengers.passengerID,
									passengerType = passengers.passengerType,
									passengerName = passengers.title + ' ' + passengers.firstName + ' ' + passengers.lastName,
									standardSeat = passengers.StandardSeat,
									forwardZoneSeat = passengers.ForwardZoneSeat,
									preferredSeat = passengers.PreferredSeat,
									KFTierCode = passengers.KFTierCode,
   								classTypeSeat, classTypeSeatColor, textTypeSeat,
   								passengerInfantName;

              if(passengers.isKrisflyer == true) {
								classTypeSeat = 'type-seat';
								if(passengers.isSupplementary == true) {
									textTypeSeat = 'SUPP.PPS CLUB';
								} else {
									if (KFTierCode === 'L' || KFTierCode === 'T' || KFTierCode === 'Q') {
										textTypeSeat = 'PPS Club';
									} else if (KFTierCode === 'G') {
										classTypeSeatColor = 'gold';
										textTypeSeat = 'Elite Gold';
									} else if (KFTierCode === 'S') {
										classTypeSeatColor = 'silver';
										textTypeSeat = 'Elite Silver';
									} else if (KFTierCode === 'K') {
										classTypeSeatColor = 'kris';
										textTypeSeat = 'KrisFlyer';
									}
								}
							}
							if(passengers.isSupplementary == true) {
								classTypeSeat = 'type-seat';
								textTypeSeat = 'SUPP.PPS CLUB';
							}
							if(passengers.infant) {
                	var passengerInfantId = passengers.infant.passengerID;
                	if (passengerInfantId === passengerID) {
                		passengerInfantName = passengers.infant.firstName + ' ' + passengers.infant.lastName;
                	}
							}
						%>
			        <tr>
			          <td>
			            <div class="data-title">Passengers</div>
                  <span class="name"><%- passengerName %></span>
                  <% if(passengers.isKrisflyer == true || passengers.isSupplementary == true) { %>
                  <span class="<%- classTypeSeat %> <%- classTypeSeatColor %>"><%- textTypeSeat %></span>
                  <% } %>
			          </td>
			          <td>
			            <div class="data-title">Standard Seat</div><span><%- standardSeat %></span>
			          </td>
			          <td>
			            <div class="data-title">Forward Zone Seat</div><span><%- forwardZoneSeat %></span>
			          </td>
			          <td>
			            <div class="data-title">Extra Legroom Seat</div><span><%- preferredSeat %></span>
			          </td>
			        </tr>
					<% }); %>
	      </tbody>
	    </table>
	  </div>
	</div>
<% } %>
