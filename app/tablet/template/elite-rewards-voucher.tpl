<% if(data){ %>
  <div class="elite-rewards__vouchers-box">
    <span class="elite-rewards__vouchers__heading">
      Active / Used Voucher
    </span>
    <% _.each(data, function(data, dataIdx){ %>
    <% if(data.type == "ComplimentarySeatUpgrade" || data.type == "ComplimentaryUpgradeToPremiumEconomy"){ %>
      <div class="manage-booking__group">
        <div class="manage-booking__group__detail">
          <div class="manage-booking__icon ico-change-seat manage-booking__group__detail__voucher">
            <span class="manage-booking__group__detail__title">
              <%- data.rewardType %>
            </span>
            <span class="manage-booking__group__detail__voucher-code">
              voucher code
              <%- data.rewardNumber%>
            </span>
            <div class="manage-booking__group__detail__link">
              <a href="javascript:void(0);" class="ico-point-r" data-trigger-popup=".popup--term-and-condition">Terms and conditions</a>
            </div>
          </div>
          <div class="manage-booking__group__detail__user-voucher">
            <a class="btn-2 manage-booking__btn--disabled">
              <% if(data.type == "ComplimentarySeatUpgrade") { %>Applied<%} else { %>used<% } %>
                <span data-tooltip="true" data-type="2" data-max-width="188" data-content="&lt;p class=tooltip__text-2&gt;You may recently used this &lt;br&gt; vouchers,and it's being&lt;br&gt; processed.&lt;/p&gt;"
                class="<% if (data.type == "ComplimentarySeatUpgrade") { %> ico-info-round-fill <% } %>"> </span>
            </a>
          </div>
        </div>
      </div>
      <% } %>
      <% }) %>
  </div>
  <% } %>
