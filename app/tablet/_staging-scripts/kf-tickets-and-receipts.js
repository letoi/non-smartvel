SIA.KFTicketReceipt = function() {
	var vars = SIA.global.vars;
	var formTicketReceipt = $('[data-form-ticket-receipt]');
	var fieldFirstName = formTicketReceipt.find('[data-field-firstname]');
	var fieldLastName = formTicketReceipt.find('[data-field-lastname]');
	var receiptType = formTicketReceipt.find('[data-choice="true"]');

	var formTicketReceiptValidate = function() {
		formTicketReceipt.validate({
			focusInvalid: true,
			errorPlacement: vars.validateErrorPlacement,
			success: vars.validateSuccess,
			invalidHandler: vars.invalidHandler
		});
	};

	formTicketReceiptValidate();

	var chooseReceiptType = function() {
		receiptType.off('change.receiptType').on('change.receiptType', function() {
			var selectedOption = $(this).children(':selected');
			var index = $(this).children().index(selectedOption);
			switch(index) {
				case 0:
				case 4:
					fieldFirstName.addClass('hidden');
					fieldLastName.removeClass('hidden');
					break;
				case 1:
				case 2:
				case 3:
					fieldFirstName.addClass('hidden');
					fieldLastName.addClass('hidden');
					break;
				case 5:
					fieldFirstName.removeClass('hidden');
					fieldLastName.removeClass('hidden');
					break;
				default:
					break;
			}
			formTicketReceipt.resetForm();
			$('[data-ticket-number]').children().addClass('hidden').eq(index).removeClass('hidden');
		});
	};

	chooseReceiptType();

	$.validator.addMethod('not_special_characters', function(value) {
		if(!value.length){
			return true;
		}
		return /^[a-zA-Z0-9- ]+$/i.test(value);
	}, L10n.validator.notSpecialCharacters);
};
