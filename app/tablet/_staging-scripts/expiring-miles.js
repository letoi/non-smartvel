/**
 * @name SIA
 * @description Define global KFMileExpiring functions
 * @version 1.0
 */

SIA.KFMileExpiring = function(){
	var global = SIA.global;
	var expiringMilesTable = $('.expiring-miles__table');
	var checkbox = $('input:checkbox', expiringMilesTable);
	var extendMileBtn = expiringMilesTable.siblings('.button-group-1').find('a');
	var popupExtendMiles = $('.popup--extend-miles'),
			formExtendMiles = popupExtendMiles.find('form');

	var loadCostData = function() {
		var milesFieldEl = $('[data-field-miles]'),
				cost1El = $('[data-field-cost-1]'),
				cost2El = $('[data-field-cost-2]'),
				maxMile = milesFieldEl.data('ruleMax'),
				isDelay = false;

		var callAjax = function() {
			$.ajax({
				url: global.config.url.kfExtendMileJSON,
				type: global.config.ajaxMethod,
				dataType: 'json',
				data: formExtendMiles.serialize(),
				success: function(res) {
					cost1El.val(res['cost-1'] || '');
					cost2El.val(res['cost-2'] || '');
				},
				error: function(jqXHR, textStatus) {
					console.log(textStatus);
				}
			});
		};

		milesFieldEl.off('keypress.loadCost').on('keypress.loadCost', function() {
			if(!isDelay) {
				isDelay = true;
				setTimeout(function() {
					if(!milesFieldEl.hasClass('error') && milesFieldEl.val() <= maxMile) {
						callAjax();
					}
					isDelay = false;
				}, 1000);
			}
		});
	};

	var fixtwoPopup = function(){
		var termAndCondition = $('.popup--tcs');
		var popupextendmiles = $('.popup--extend-miles');
		termAndCondition.off('afterHide.fixtwoPopup').on('afterHide.fixtwoPopup', function(){
			popupextendmiles.Popup('addResize');
			popupextendmiles.Popup('reset');
		});
	};

	var enableMileBtn = function(){
		var valid = false;
		checkbox.each(function(){
			if($(this).data('eligible') && $(this).is(':checked')){
				valid = true;
				return valid;
			}
		});
		return valid;
	};

	formExtendMiles.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});

	checkbox.off('change.enable').on('change.enable', function(){
		if(enableMileBtn()){
			extendMileBtn.removeClass('disabled');
		}
		else{
			extendMileBtn.addClass('disabled');
		}
	});

	fixtwoPopup();

	loadCostData();
};
