/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.plusOrMinusNumber = function() {
	var plusOrMunisModule = $('[data-plus-or-minus-number]');

	var changeValueInput = function(){
		plusOrMunisModule.each(function(){
			var self = $(this),
					limitInputVL = self.data('plus-or-minus-number'),
					plusBtn = self.find('.btn-plus'),
					minusBtn = self.find('.btn-minus'),
					input = self.find('[data-updated-value]'),
					unit = self.siblings('.review-baggage-text').attr('data-unit'),
					price = self.siblings('.sgd-price');

			var updatePrice = function(inputVal) {
				var formatPrice = Number(inputVal*unit).toFixed(2);
				price.find('span').text(formatPrice);
			};

			var showHidePrice = function(inputVal) {
				if(inputVal === 0) {
					price.fadeOut();
				} else {
					price.fadeIn();
				}
			};

			input.off('change.value').on('change.value', function(){
				var inputVL = input.val();
				var inputVLFinal = isNaN(inputVL) ? 0 : inputVL;
				if(parseInt(input.val()) > limitInputVL){
					inputVLFinal = limitInputVL;
				}
				input.val(inputVLFinal);
				if($('body').hasClass('mp-addons-piece-page')) {
					updatePrice(inputVLFinal);
					showHidePrice(inputVLFinal);
				}
			});

			plusBtn.off('click.plus').on('click.plus', function(){
				var inputVL = parseInt(input.val());
				var inputVLFinal = isNaN(inputVL) ? 0 : inputVL;
				if(inputVLFinal < limitInputVL){
					input.val(inputVLFinal + 1);
					if($('body').hasClass('mp-addons-piece-page')) {
						updatePrice(inputVLFinal + 1);
						showHidePrice(inputVLFinal + 1);
					}
				}
			});

			minusBtn.off('click.minus').on('click.minus', function(){
				var inputVL = parseInt(input.val());
				if(inputVL > 0){
					input.val(inputVL - 1);
					if($('body').hasClass('mp-addons-piece-page')) {
						updatePrice(inputVL - 1);
						showHidePrice(inputVL - 1);
					}
				}
			});
		});
	};

	var initModule = function() {
		changeValueInput();
	};

	initModule();
};
