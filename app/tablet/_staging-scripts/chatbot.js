var SIA = SIA || {};
SIA.ChatBot = function() {

    var p = {};
    var validation = false; 

    var init = function() {
        p.askBotBtn = $('#askBot');
        $('.chatbot-prompt-dialog-box').addClass('hidden');

        openChatBotWindow();
        // acceptButton();
        closeChatBotWindow();
        isTermsAccepted();
        acceptTermsBtn();
        closeTermMessage();
    };

    var openChatBotWindow = function() {
        p.askBotBtn.click(function() {
            $('#chatPopupDialog').removeClass('pull-down').addClass('show');
            $('.chatbot-prompt-dialog-box').addClass('hidden');

            setTimeout(function() {
                $('#chatPopupDialog').addClass('pull-up');
            }, 100);

            loadChatBot();
            //removeDefaultInputFocus();
        });
    };

    
    var isTermsAccepted = function() {
        if(sessionStorage.getItem("chatbot_window_agreed_TimeStamp") != null) {
            hideLaunchWindow();
        } else {
            acceptTermsBtn();
        }
    };
  
    var acceptTermsBtn = function(){
        $('#acceptBtn').click(function() {
            sessionStorage.setItem("chatbot_window_agreed_TimeStamp", String(new Date()));
            hideLaunchWindow();
        });
    };

    var wireUpWindowUnloadEvents = function(validation) {
        $(document).on('keypress', function(e) {
            if (e.keyCode == 116){
                validation = true;
            }
        });
         
        $(document).on("click", "a" , function() {
            validation = true;
        });
         

        $(document).on("submit", "form" , function() {
            validation = true;
        });
         
        $(document).bind("click", "input[type=submit]" , function() {
           validation = true;
        });
          
        $(document).bind("click", "button[type=submit]" , function() {
           validation = true;
        });
    };

    var closingWindow = function(validation) {
        $(window).on('beforeunload', function(){
            if (!wireUpWindowUnloadEvents){
                sessionStorage.removeItem("chatbot_window_agreed_TimeStamp");
                window.close();
            }
        });
    };

    var hideLaunchWindow = function() {
        $('.launch-window').addClass('hidden');
    };

    // close chatbot window
    var closeChatBotWindow = function() {
        $(document).on('click', '.close_icon', function() {
            $('#chatPopupDialog').removeClass('pull-up').addClass('pull-down');
            $('.chatbot-prompt-dialog-box').removeClass('hidden');
        });
    };

    var closeTermMessage = function() {
        $(document).on('click', '#closeMessage', function() {
            $('.privacy-terms').addClass('hidden');
        });
    };

    var ConnectionStatus = BotChat.ConnectionStatus;

    var directLine = new BotChat.DirectLine({
        secret: 'uu-tPuit1b8.cwA.iWM.fPIBoiiPlMSrfYnCiBtAOuUwwcWqChC6r2QeQlRFDLs'
    });

    directLine.activity$
        .subscribe(
            function(activity) {
                console.log("received activity ", activity);
            }
        );

    directLine.connectionStatus$
        .subscribe(function(connectionStatus) {
            switch (connectionStatus) {
                case ConnectionStatus.Uninitialized:
                    console.log("Uninitialized!")
                    break
                case ConnectionStatus.Connecting:
                    console.log("Connecting...")
                    break
                case ConnectionStatus.Online:
                    directLine.postActivity({
                        from: {
                            id: 'wYN8MsEuv63sERORyse2BNW_BakBgh1HEnQZv0_TDd_BOZaZLC8F!-966462018',
                            name: 'Customer'
                        },
                        type: 'message',
                        text: '~~initWeb~~',
                        channelData: {
                            loginID: 'qwerty'
                        }
                    }).subscribe(
                        function id() {
                            console.log("Posted activity, assigned ID ", id)
                            if (id === 'retry') {
                                // Error 50x
                            } else {
                                // Bot is online, enable ASK CHATBOT panel
                                $('.chatbot-prompt-dialog-box').removeClass('hidden');
                            }
                        },
                        function error() {
                            // Error 40x
                            console.log("Error posting activity", error)
                        }
                    );
                    break
                case ConnectionStatus.ExpiredToken:
                    console.log("Expired token!")
                    break
                case ConnectionStatus.FailedToConnect:
                    console.log("Totally failed to connect!")
                    break
                case ConnectionStatus.Ended:
                    console.log("Conversation ended.")
                    break
            }
        });

    // load chatbot
    var loadChatBot = function() {
        BotChat.App({
            botConnection: directLine,
            user: {
                id: 'wYN8MsEuv63sERORyse2BNW_BakBgh1HEnQZv0_TDd_BOZaZLC8F!-966462018',
                name: 'Customer'
            },
            bot: {
                id: '3cd93ce0-11ba-4219-add7-5fddfdf6b4df'
            },
            resize: 'detect'
        }, document.getElementById("siaBot"));
    };

    var oPublic = {
        init: init,
    };
    return oPublic;
}();

$(function() {
    SIA.ChatBot.init();
});
