
/**
 * @name SIA
 * @description Define global autocompleteCountryCity functions
 * @version 1.0
 */
SIA.autocompleteCountryCity = function(){
	var global = SIA.global;
	var config = global.config;
	var componentCountryCity = $('[data-component-country-city]');

	// Get data from JSON
	var getDataFromJson = function(){
		$.ajax({
			url: config.url.countryCityAutocomplete,
			type: global.config.ajaxMethod,
			dataType: 'json',
			success: function(data){
				if(data){
					renderSelectAndAutoComplete(data);
				}
			},
			error: function(err) {
				window.alert(err);
			}
		});
	};

	// Render html for autocomplete
	var renderSelectAndAutoComplete = function(data){
		var initAutocompleteSL = function(selectDefault, json){
			var html = '';

			var gernderHTML = function(arr){
				for (var i = 0; i < arr.length; i++) {
					html += '<option value="' + (arr[i].value) + '" data-text="' +
						arr[i].text + '">' + arr[i].name + '</option>';
				}
			};

			gernderHTML(json);

			selectDefault.empty().append(html);
			selectDefault
				.closest('[data-autocomplete]')
				.removeData('init-automcomplete');
			SIA.initAutocompleteCity();
		};

		componentCountryCity.each(function(){
			var component = $(this);
			var countryItem = component.find('[data-country-city]');

			countryItem.each(function(){
				var countryCity = $(this);
				var countryExpand = countryCity.find('[data-country-expand]');
				var cityExpand = countryCity.find('[data-city-expand]');
				var countryInput = countryExpand.find(':text');
				var cityInput = cityExpand.find(':text');
				var autocompleteCountryDefault = countryExpand.find('select');
				var autocompleteCity = countryCity.find('[data-city-expand]');
				var autocompleteCityDefault = autocompleteCity.find('select');

				var expandJson = function(){
					var countryArr = [];
					var cityArr = [];
					var locations = data.location;
					var valueBeforeChange = '';
					var diffVL = false;

					var getCityJSON = function(cityItem, arrCityItem){
						for(var j = 0; j < cityItem.length; j++){
							var cityObj = {name: cityItem[j].name, value: cityItem[j].value, text: cityItem[j].text};
							arrCityItem.push(cityObj);
						}
					};

					for(var i = 0; i < locations.length; i++){
						var countryItem = locations[i].country;
						var cityItem = locations[i].cities;
						var countryObj = {};
						var arrCityItem = [];

						countryObj = {name: countryItem.name, value: countryItem.value, text: countryItem.text};
						countryArr.push(countryObj);
						getCityJSON(cityItem, arrCityItem);
						cityArr.push(arrCityItem);
					}

					initAutocompleteSL(autocompleteCountryDefault, countryArr);

					countryInput.off('focusin.fromSL').on('focusin.fromSL', function(){
						valueBeforeChange = $(this).val();
					});

					var clearCity = function(val){
						if(!diffVL && !!val && val !== valueBeforeChange){
							valueBeforeChange = val;
							cityInput.val('');
							diffVL = true;
						}else{
							diffVL = false;
						}
					};

					countryInput.off('blur.fromSL').on('blur.fromSL', function(){
						clearCity($(this).val());
					});

					countryInput.off('change.fromSL autocompleteselect.fromSL').on('change.fromSL autocompleteselect.fromSL', function(){
						var input = $(this);

						setTimeout(function(){
							if(!!input.val()){
								if(cityExpand.length){
									for(var i = 0; i < countryArr.length; i++){
										if(input.val() === countryArr[i].text){
											initAutocompleteSL(autocompleteCityDefault, cityArr[i]);
											break;
										}
									}
									clearCity(input.val());
									cityExpand.removeClass('hidden');
								}
							}else{
								cityExpand.addClass('hidden');
							}
						});
					});
				};

				expandJson();
			});
		});
	};

	var initModule = function(){
		SIA.initAutocompleteCity();
		getDataFromJson();
	};

	initModule();
};
