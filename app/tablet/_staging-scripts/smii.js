SIA.FormValidator = function() {
	var p = {};

	p.errElClss = '.gigya-error-msg';

	var init = function() {

	};

	var attachEvnt = function(obj) {
		var evnts = ['keypress', 'blur', 'click'];

		if (typeof obj != 'undefined' && typeof obj.el != 'undefined') {
			for (var i = 0, iLen = evnts.length; i < iLen; i++) {
				var evntType = evnts[i];
				var evntFunc = obj[evntType];

				if (typeof evntFunc != 'undefined') {
					obj.el.on(evntType, evntFunc);
				}
			}
		}
	};

	var validateEmail = function(val) {
		var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

		return {
			state: emailReg.test(val),
			msg: 'Please enter a valid email address.'
		};
	};

	var validateEmpty = function(val) {
		return {
			state: (val != '') ? true : false,
			msg: 'This field is required'
		};
	};

	var isNotNumeric = function(val) {
		var value = val;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			var str = value[i];
			if (isNaN(str)) {
				return true;
			}
		}

		return false;
	};

	var isValRepetitive = function(val) {
		var value = val;
		var repetitiveTotal = parseInt(value[0]) * value.length;
		var valueTotal = 0;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == repetitiveTotal) ? true : false;
	};

	var isValSequential = function(val) {
		var value = val;
		var sequentialTotal = 0;
		var valueTotal = 0;

		for (var j = parseInt(value[0]), jLen = parseInt(value[value.length - 1]); j <= jLen; j++) {
			sequentialTotal += j;
		}

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == sequentialTotal) ? true : false;
	};

	var validatePassword = function(val) {
		if (val.length < 6) {
			return {
				state: false,
				msg: 'Please enter a 6 digit PIN.'
			};
		} else if (isValRepetitive(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that has different digits.'
			};
		} else if (isValSequential(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that is not formed by sequential digits.'
			};
		} else if (isNotNumeric(val)) {
			return {
				state: false,
				msg: 'Enter numbers only'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validateLegalAge = function(val) {
		var dateFormat = validateDateFormat(val);

		if (dateFormat.state) {
			var date = parseDate(val);

			// first month starts at 0
			var birthDate = new Date(date.year, date.month - 1, date.day);
			var nowDate = new Date();

			var age =  nowDate.getFullYear() - birthDate.getFullYear();
			if (age < 16) {
				return {
					state: false,
					msg: 'You must be at least 16 years old to be a basic account holder.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var parseDate = function(val) {
		var value = val;
		var fSeparator = value.indexOf('/');
		var lSeparator = value.lastIndexOf('/');

		return {
			day: value.slice(0, fSeparator).trim(),
			month: value.slice(fSeparator + 1, lSeparator).trim(),
			year: value.slice(lSeparator + 1).trim()
		};
	};

	var validateDateFormat = function(val) {
		var date = parseDate(val);

		if (date.day == 'DD' && date.month == 'MM' && date.year == 'YYYY') {
			return {
				state: false,
				msg: 'This date is required'
			};
		} else if (parseInt(date.day) < 1 || parseInt(date.day) > 31) {
			return {
				state: false,
				msg: 'This day is not valid'
			};
		} else if (parseInt(date.month) < 0 || parseInt(date.month) > 12) {
			return {
				state: false,
				msg: 'This month is not valid'
			};
		} else if (isNaN(date.day) || isNaN(date.month) || isNaN(date.year)) {
			return {
				state: false,
				msg: 'This date is not valid'
			};
		}else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var addErrState = function(el, inputclss, msgClss, errMsg) {
		el.addClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.addClass(msgClss);
		msgEl.html(errMsg);
	};

	var addValidState = function(el, inputclss, msgClss) {
		el.addClass(inputclss);
	};

	var removeInputState = function(el, inputclss, msgClss) {
		el.removeClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.removeClass(msgClss);
		msgEl.html('');
	};

	var inputValidator = function(el, result, errClss, validClss) {
		if (!result.state) {
			addErrState(el, errClss.input, errClss.msg, result.msg);
		} else {
			removeInputState(el, errClss.input, errClss.msg);
			addValidState(el, validClss);
		}
	};

	return {
		init: init,
		attachEvnt : attachEvnt,
		validateEmail : validateEmail,
		validateEmpty : validateEmpty,
		validatePassword : validatePassword,
		addErrState : addErrState,
		addValidState : addValidState,
		removeInputState : removeInputState,
		inputValidator : inputValidator,
		validateDateFormat: validateDateFormat,
		validateLegalAge: validateLegalAge
	};
}();

SIA.TogglePassword = function(appendEl, input) {
	var p = {};

	p.appendEl = appendEl;
	p.input = input;
	p.iconShow  = 'images/show-eye.png';
 	p.iconHide = 'images/hidden-eye.png';
 	p.icon = '<img data-togglePass class="pin-eye-icon" src="">';

	var init = function() {
		p.appendEl.css('position', 'relative');

		var icon = $(p.icon);
		p.appendEl.append(icon);
		icon.attr('src', p.iconShow);

		evnt(p.appendEl.find('[data-togglePass]'), p.input);
	};

	var evnt = function(el, input) {
		el.on('click', function() {
			var inputType = input.attr('type');
			var iconEl = input.parent().find('[data-togglePass]');

			if (inputType == 'password') {
				input.attr('type', 'text');
				iconEl.attr('src', p.iconHide);
			} else if (inputType == 'text') {
				input.attr('type', 'password');
				iconEl.attr('src', p.iconShow);
			}
		});
	};

	return { init: init };
};

SIA.RadioTab = function() {
    var p = {};

    p.gigyaEl = $('#ruLoginContainer');
    p.gigyaTabs = 'data-radiotab';
    p.gigyaContents = 'data-radiotab-content';

    p.ruErrorCont = $('#ruLoginError');

    var init = function() {
        // show default checked radio
        showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + p.gigyaEl.find('[checked="checked"]').attr(p.gigyaTabs) + '"]'));

        evnt(p.gigyaEl.find('[' + p.gigyaTabs + ']'));
    };

    var evnt = function(el) {
        el.on('click', function() {
            var t = $(this);

            // hide all contents
            p.gigyaEl.find('[' + p.gigyaContents + ']').addClass('hidden');

            // show current select tab content
            showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + t.attr(p.gigyaTabs) + '"]'));

            t.attr('data-radiotab') === '2' ? p.ruErrorCont.removeClass('hidden') : p.ruErrorCont.addClass('hidden')

        });
    };

    var showContent = function(el) {
        el.removeClass('hidden');
    };

    return {
        init: init,
        p: p
    };
}();

SIA.DatePicker = function(){
	var p = {};

	p.datePlaceholder = '<div style="position:absolute;top: 25px;left: 5px;height: 40px;width: 100%;">\
		<input type="text" value="DD" data-day style="width: 31px;border: none;text-align: center;height:100%;color: #999999;">\
		<span style="color: #999999;">/</span>\
		<input type="text" value="MM" data-month style="width: 31px;border: none;text-align: center;height:100%;color: #999999;">\
		<span style="color: #999999;">/</span>\
		<input type="text" value="YYYY" data-year style="width: 47px;border: none;text-align: center;height:100%;color: #999999;">\
	</div>';

	p.dateFormats = ['DD', 'MM', 'YYYY'];

	p.inputDay = '[data-day]';
	p.inputMonth = '[data-month]';
	p.inputYear = '[data-year]';

	var init = function(elParent, el) {
		p.dateParentEl = elParent;
		p.dateEl = el;

		customize(p.dateParentEl, p.dateEl);
		evnt(p.dateParentEl);
	};

	var customize = function(elParent, el) {
		elParent.css('position', 'relative');
		elParent.find(el).after($(p.datePlaceholder));
	};

	var resetFormat = function(el) {
		if (el[0].hasAttribute('data-day')) { el.val(p.dateFormats[0]); }
		else if (el[0].hasAttribute('data-month')) { el.val(p.dateFormats[1]); }
		else if (el[0].hasAttribute('data-year')) { el.val(p.dateFormats[2]); }
	};

	var transferVal = function() {
		var day = p.dateParentEl.find(p.inputDay).val();
		var month = p.dateParentEl.find(p.inputMonth).val();
		var year = p.dateParentEl.find(p.inputYear).val();
		var separator = '  /  ';

		p.dateEl.val(day + separator + month + separator + year);
	};

	var evnt = function(elParent) {
		elParent.on('blur', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			var t = $(this);

			// reset to default format
			if (t.val() == '') {
				resetFormat(t);
			}

			if (p.dateFormats.indexOf(t.val().toUpperCase()) >= 0) {
				t.css('color', '#999999');
				resetFormat(t);
			} else {
				t.css('color', 'black');
			}

			// transfer to actual input field
			transferVal();
		});

		elParent.on('click', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			var t = $(this);

			t.css('color', 'black');
		});
	};

	return {
		init: init
	};
};

SIA.RULogin = function() {
    var p = {};

    p.formValidator = SIA.FormValidator;

    p.gigyaErrClss = {
        input: 'gigya-error',
        msg: 'gigya-error-msg-active'
    };
    p.gigyaValidClss = 'gigya-valid';

    p.containerName = 'loginContainer';
    p.containerId = $('#' + p.containerName);

    var init = function() {
        //  find email field attached by gigya, save html and remove
        p.emailInput = p.containerId.find('.gigya-login-form .user-email-address input');

        // remove clear button
        p.emailInput.parent().find('.add-clear-text').remove();

         p.formValidator.attachEvnt({
            el: p.emailInput,
            keypress: function() {
                var t = $(this);

                p.formValidator.inputValidator(
                    t,
                    p.formValidator.validateEmail(t.val()),
                    p.gigyaErrClss,
                    p.gigyaValidClss);
            },
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmail(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
         });

        p.passInput = p.containerId.find('.gigya-login-form .user-password input');

     		// add max characters length
			p.passInput.attr('maxlength', '6');

        p.formValidator.attachEvnt({
            el: p.passInput,
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validatePassword(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
        });

        p.ruLoginContainer = $('#ruLoginContainer');
    };

     var onError = function(e){
     	$('#gigya-login-screen .gigya-error-display').prepend('<em class="ico-close-round-fill"></em>');
    };

    var onFieldChanged = function(e){
    	if(e.field === 'loginID') {
    		p.ruLoginError.html('');
    	}
    };

    return {
        init: init,
        p: p,
        onError: onError,
        onFieldChanged: onFieldChanged
    };
}();

SIA.RURegistration = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function(containerID) {
		p.containerName = containerID;
		p.containerId = $('#' + p.containerName);

		p.registrationForm = p.containerId.find('.gigya-register-form');

		// attach show/hide mask password
	 	p.passInput = p.registrationForm.find('.gigya-input-password');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '6');

		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		validateEmptyFields();

		// date
		p.dateBirthInput = p.registrationForm.find('[data-gigya-name="data.birthDate"]');

		disableDate();

		// customize placeholder
		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.dateBirthInput.parent(), p.dateBirthInput);

		validateDateBirth();

		initTabs();
	};

	var initTabs = function(){
		p.tabContainer = $('#ruTabs');
		p.tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					p.tabContainer.find('.tab-item').removeClass('active');
					p.tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					p.tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	};

	// remove placeholder
	var disableDate = function() {
		p.dateBirthInput.attr('placeholder', '');
		p.dateBirthInput.attr('readonly', true);
		p.dateBirthInput.css('color', 'white');
	};

	// firstname, lastname, password, country & email
	var validateEmptyFields = function() {
		var inputs = [
			p.registrationForm.find('[data-gigya-name="profile.firstName"]'),
			p.registrationForm.find('[data-gigya-name="profile.lastName"]'),
			p.registrationForm.find('[data-gigya-name="email"]'),
			p.registrationForm.find('[data-gigya-name="profile.country"]'),
			p.passInput
		];

		for (var i = 0, iLen = inputs.length; i < iLen; i++) {
			p.formValidator.attachEvnt({
				el: inputs[i],
				blur: function() {
				var t = $(this);

	 			if(t.val() == '') {
	 				p.formValidator.inputValidator(
						t,
						p.formValidator.validateEmpty(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	 				}
				}
			});
		}
	};

	// date
	var validateDateBirth = function() {
		var dateBirthFields = [
			p.registrationForm.find('[data-day]'),
			p.registrationForm.find('[data-month]'),
			p.registrationForm.find('[data-year]')
		];

		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			p.formValidator.attachEvnt({
				el: dateBirthFields[j],
				blur: function() {
					p.formValidator.inputValidator(
						p.dateBirthInput,
						p.formValidator.validateLegalAge(p.dateBirthInput.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
		}
	};

	return {
		init: init
	};
}();

SIA.RURegIntermediate = function() {
	var p = {};

	p.formValidator = SIA.FormValidator;

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	var init = function () {

	};

	var onAfterScreenLoad = function() {
		p.container = $('#gigyaRegistrationIntermediate');

		// move container position to top
		p.container.closest('.ru-reg-intermediate .popup__inner').css('top', '319px');

		// email
		p.formValidator.attachEvnt({
         el: p.container.find('[data-gigya-name="profile.email"]'),
         keypress: function() {
       		var t = $(this);

          	p.formValidator.inputValidator(
           		t,
           		p.formValidator.validateEmail(t.val()),
           		p.gigyaErrClss,
              	p.gigyaValidClss);
         },
         blur: function() {
             var t = $(this);

             if (t.val() == '') {
                 	p.formValidator.inputValidator(
                     t,
                     p.formValidator.validateEmpty(t.val()),
                     p.gigyaErrClss,
                     p.gigyaValidClss);
             } else {
              	p.formValidator.inputValidator(
                  t,
                  p.formValidator.validateEmail(t.val()),
                  p.gigyaErrClss,
                  p.gigyaValidClss);
             }
         }
   	});

		// attach show/hide mask password
	 	p.passInput = p.container.find('[data-gigya-name="password"]');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '6');

		// password
		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		// add max characters length
		p.passInput.attr('maxlength', '6');
	};

	var onError = function() {
		var errorMsgEl = p.container.find('.gigya-error-display');
		p.container.find('.gigya-error-display').remove();

		errorMsgEl.prepend('<em class="ico-close-round-fill"></em>');
		p.container.find('.gigya-profile-form h2').after(errorMsgEl);
	};

	return {
		init: init,
		onAfterScreenLoad: onAfterScreenLoad,
		onError: onError
	};
}();

function gigyaRegistrationCompletion() {

}

$(function() {
	var body = SIA.global.vars.body;

	if (body.hasClass('gigya-login')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'loginContainer',
	        startScreen: "gigya-login-screen",
	        onAfterScreenLoad: SIA.RULogin.init,
	        onError: SIA.RULogin.onError
	    });

	    SIA.RadioTab.init();
	}

	if (body.hasClass('gigya-registration')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTab',
	  		startScreen: "gigya-register-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTab');
	  		}
	   });
	}

	if (body.hasClass('gigya-reg-intermediate')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'gigyaRegistrationIntermediate',
	  		startScreen: "gigya-register-intermediate-screen",
	  		width: "100%",
	  		onAfterScreenLoad: SIA.RURegIntermediate.onAfterScreenLoad,
  			onError: SIA.RURegIntermediate.onError
	   });
	}

	if (body.hasClass('gigya-reg-completion')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTabCompletion',
	  		startScreen: "gigya-complete-registration-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTabCompletion');
	  		}
	   });
	}

	var tabContainer = $('#ruTabs');
	if(tabContainer.length) {
		tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					tabContainer.find('.tab-item').removeClass('active');
					tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	}
});
