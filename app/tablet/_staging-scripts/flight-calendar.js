/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.flightCalendar = function(){
	var global = SIA.global;
	var win = global.vars.win;

	if(!$('.fare-calendar-page').length){
		return;
	}

	var $calendar = $('.calendar__table'),
			$calendarRowColumn = $('td', $calendar);
	var controls = $('.control-horizontal, .control-vertical');
	var nextControl = $('.next', controls);
	var prevControl = $('.prev', controls);
	var btnNext = $('.flight__calendar .btn-1');
	var dataChecked = $calendar.data('checked');
	var disableNextBtn = function(e) {
		e.preventDefault();
		return;
	};
	var getPositionInTable = function(el) {
		var cellIndex = el.cellIndex,
				rowIndex = el.parentNode.rowIndex;
		return rowIndex + ',' + cellIndex;
	};
	var resetCalendarTable = function() {
		$calendarRowColumn.removeClass('active');
		$('th', $calendar).removeClass('active');
		$('td input[type="radio"]', $calendar).prop('checked', false);
		$calendar.removeData('checked');
		btnNext.addClass('disabled').on('click.disabled', disableNextBtn);
	};
	var calcCellDisplay = function(calendarTable) {
		var totalCellDisplay = calendarTable.find('tr:eq(0) th:visible').length - 1;
		controls.find('.number').each(function(){
			$(this).html(totalCellDisplay);
		});
		return totalCellDisplay;
	};
	var changeUrlControl = function() {
		if(window.innerWidth >= global.config.tablet) {
			nextControl.attr('href', nextControl.data('url-desktop'));
			prevControl.attr('href', prevControl.data('url-desktop'));
		}
		else if(window.innerWidth < global.config.tablet && window.innerWidth > global.config.mobile) {
			nextControl.attr('href', nextControl.data('url-tablet'));
			prevControl.attr('href', prevControl.data('url-tablet'));
		}
		else {
			nextControl.attr('href', nextControl.data('url-mobile'));
			prevControl.attr('href', prevControl.data('url-mobile'));
		}

	};
	var checkNextBtn = function(data, isTrigger) {
		if(data) {
			var position = data.split(','),
					rowIndex = parseInt(position[0]),
					cellIndex = parseInt(position[1]) - 1,
					checkedTd = $calendar.find('tr:eq(' + rowIndex + ') td:eq(' + cellIndex + ')');

			if(checkedTd.is(':hidden')) {
				resetCalendarTable();
			} else {
				if(isTrigger) {
					checkedTd.trigger('click.activeHead');
				}
				if(btnNext.hasClass('disabled')) {
					btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
				}
			}
		} else {
			resetCalendarTable();
		}
	};

	$calendarRowColumn.off('click.activeHead').on('click.activeHead',function(){
		if($(this).hasClass('not-available')){ return; }
		var col = $(this).parent().children().index($(this)),
				$headerColumn = $calendar.find('tr').eq(0).find('th');

		resetCalendarTable();
		$(this).addClass('active');
		$(this).parent().find('th').addClass('active');
		$headerColumn.eq(col).addClass('active');

		// event checked radio button and set data table
		$(this).find('input[type="radio"]').prop('checked', true);
		$calendar.data('checked', getPositionInTable(this));
		if(btnNext.hasClass('disabled')) {
			btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
		}
	});

	$calendar.data('currentCellDisplay', calcCellDisplay($calendar));
	checkNextBtn(dataChecked, true);
	changeUrlControl();
	win.off('resize.calculateCellDisplay').on('resize.calculateCellDisplay', function(){
		var currentCellDisplay = calcCellDisplay($calendar);
		if($calendar.data('currentCellDisplay') !== currentCellDisplay) {
			checkNextBtn($calendar.data('checked'), false);
			changeUrlControl();
			$calendar.data('currentCellDisplay', currentCellDisplay);
		}
	});
};
