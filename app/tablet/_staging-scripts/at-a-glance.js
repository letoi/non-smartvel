/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.atAGlance = function(){
	if(!$('.at-a-glance-page').length){
		return;
	}
	var global = SIA.global;
	// var body = global.vars.body;
	var vars = global.vars;
	var config = global.config;
	var win = $(window);
	var body = global.vars.body;
	var tabWrapper = $('.tab-wrapper');
	var tabContentActive = $('.tab-content.active');
	var tabContentUnActive = $('.tab-content:not(.active)');
	var chartItemActive = tabContentActive.find('.dials-chart__item');
	var chartItemUnactive = tabContentUnActive.find('.dials-chart__item');
	var dialsChart = $('.dials-chart__item');
	var responsiveTable = $('.table-responsive');
	var twoColsWrapper = $('.two-cols-wrapper');
	var delayChart = 200;
	var timeChart = null;
	var windowWidth = 0;
	var windowHeight = 0;
	var flexslider2 = $('.flexslider-2');
	var highlightSlider = $('#highlight-slider');
	var highlightSlides = highlightSlider.find('.slides');
	var customiseLinkForm = $('.popup--customise-link form');
	var customiseLinkChkbs = customiseLinkForm.find('input:checkbox');
	var totalCustome = customiseLinkForm.data('totalCustome');
	var duration = 600;

	// var isSSTab4NativeBrowser = function() {
	// 	// var ssTab4Models = ['SM-T230', 'SM-T230NU', 'SM-T230NT', 'SM-T231', 'SM-T235', 'SM-T235Y', 'SM-T330', 'SM-T330NU', 'SM-T331', 'SM-T335', 'SM-T337A', 'SM-T337T', 'SM-T337V', 'SM-T530NU', 'SM-T530', 'SM-T531', 'SM-T535', 'SM-T537A', 'SM-T537V'];

	// 	var userAgent = window.navigator.userAgent;
	// 	var isNativeBrowser = (userAgent.indexOf('Mozilla/5.0') > -1 && userAgent.indexOf('Android ') > -1 && userAgent.indexOf('AppleWebKit') > -1) && (userAgent.indexOf('Version') > -1);

	// 	// ssTab4Models.filter(function(model){
	// 	// 	return userAgent.indexOf(model) > -1;
	// 	// }).length === 1;

	// 	return isNativeBrowser && userAgent.indexOf('SM-T231') > -1;
	// };

	// Set min-height for the "Quick links" block
	var balanceHeight = function(wrapper){
		var blkContentWrap = wrapper.find('.block--expiring-miles .blk-content-wrap');
		var blkContent = wrapper.find('.block--quick-links .blk-content');
		blkContentWrap.removeAttr('style');
		blkContent.removeAttr('style');
		var contentWrapHeight = blkContentWrap.height();
		var contentHeight = blkContent.height();
		var maxLength = (contentWrapHeight > contentHeight ? contentWrapHeight : contentHeight);
		blkContent.css('min-height',maxLength);
		blkContentWrap.css('min-height',maxLength);
	};


	// Show all Dials
	var showAllDials = function(self){
		self.addClass('animated fadeIn');
		self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn').on('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn' , function(){
			setTimeout(function(){
				dialsChart = $('.dials-chart__item', flexslider2);

				dialsChart.each(function(){
					if(!$.data(this, 'chart')){
						dialsChart.chart({
							startVal: 0,
							endVal: 4000,
							increment: 180 / 100,
							incrementVal: 70,
							duration: duration
						}).chart('show');
					}
				});
				self.removeClass('animated fadeIn');
				self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn');
			},0);
		});
	};

	// var delegateEventForToolbar = function(self){
	// 	var init = function(){
	// 		body.on('transitionend.fadeIn oTransitionEnd.fadeIn webkitTransitionEnd.fadeIn MSTransitionEnd.fadeIn', '.toolbar--language', function(){
	// 			showAllDials(self);
	// 			body.off('transitionend.fadeIn oTransitionEnd.fadeIn webkitTransitionEnd.fadeIn MSTransitionEnd.fadeIn', '.toolbar--language');
	// 		});
	// 	};
	// 	init();
	// };

	// var getDrawData = function(jsonUrl,templateUrl,appendToElement){
	// 	var drawContent = function(res,apElement) {
	// 		var appendElement = $(apElement);
	// 		if(appendElement.length){
	// 			$.get(templateUrl, function (data) {
	// 				appendElement.empty();
	// 				var template = window._.template(data, {
	// 					data: res
	// 				});
	// 				$(template).appendTo(appendElement);
	// 				$('[data-accordion-wrapper]').accordion('refresh');
	// 			}, 'html');
	// 		}
	// 	};


	// 	var ajaxSuccess = function(res) {
	// 		drawContent(res,appendToElement);
	// 	};
	// 	$.ajax({
	// 			url: jsonUrl,
	// 			type: global.config.ajaxMethod,
	// 			dataType: 'json',
	// 			success: ajaxSuccess,
	// 			error: function(xhr, status) {
	// 				if(status !== 'abort') {
	// 					window.alert(L10n.flightSelect.errorGettingData);
	// 				}
	// 			}
	// 		});
	// };

	// getDrawData(config.url.kfAtAGlanceJSON ,config.url.kfAtAGlanceTemplate,'[data-accordion-wrapper-content=1]');

	win.off('resize.balanceHeight').on('resize.balanceHeight',function(){
		if(win.width() !== windowWidth || win.height() !== windowHeight){
			windowWidth = win.width();
			windowHeight = win.height();
			balanceHeight(twoColsWrapper);
		}
		if(win.width() < 751){
			dialsChart.removeClass('visibility-hidden');
			if(!highlightSlides.hasClass('slick-initialized')){
				highlightSlides.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					accessibility: false,
					arrows: false
				});
			}
			flexslider2.each(function(idx){
				var self = $(this);
				var slide = self.find('.slides');
				if(!slide.hasClass('slick-initialized')){
					slide
						.on('init', function() {
							if(!idx){
								showAllDials(self);
							}
						})
						.on('afterChange', function() {
							self.find('.slide-item').eq(0).css('display', '');
						});

					slide.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: 1,
						slidesToScroll: 1,
						accessibility: false,
						arrows: false,
						useCSS: false/*,
						onInit: function(){
							if(!idx){
								// if(body.data('showTBar')){
								// 	delegateEventForToolbar(self);
								// }
								// else{
								showAllDials(self);
								// }
							}
						},
						onAfterChange: function(){
							self.find('.slide-item').eq(0).css('display', '');
						}*/
					});
				}
			});
		}
		else {
			if(highlightSlides.hasClass('slick-initialized')){
				highlightSlides.slick('unslick');
			}
			flexslider2.each(function(){
				var self = $(this);
				var slide = self.find('.slides');
				if(slide.hasClass('slick-initialized')){
					slide.slick('unslick');

				}
			});
		}
	}).trigger('resize.balanceHeight');

	customiseLinkChkbs.off('change.customiseLinkChkbs').on('change.customiseLinkChkbs',function(){
		var unCheckedChkbxs = customiseLinkForm.find('input:checkbox:not(:checked)');
		if(customiseLinkForm.find('input:checkbox:checked').length >= totalCustome){
			unCheckedChkbxs.prop('disabled', true);
			unCheckedChkbxs.parent('.custom-checkbox').addClass('disabled');
		}
		else {
			unCheckedChkbxs.prop('disabled', false);
			unCheckedChkbxs.parent('.custom-checkbox').removeClass('disabled');
		}
	}).trigger('change.customiseLinkChkbs');

	$('.dials-tab').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active',
		beforeChange: function(tab) {
			if(win.width() < 751){
				var sld = tab.filter(':not(.active)').find('.flexslider-2 .slides');
				var items = sld.find('.slide-item');
				if(sld.length){
					if(!items.width()){
						sld.find('.slick-track').width(100000);
						items.width(tabWrapper.width()).eq(0).css('display', 'none');
						sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));
					}
				}
			}
		}
	});

	$('.slide-item').css('-webkit-transform', 'translate3d(0px,0px,0px)'); // fix ipad
	if(win.width() >= 751){
		if (vars.isSSTab4NativeBrowser()) {
			body.addClass('tab4-native-browser');
			duration = 0;
		}

		dialsChart.chart({
			startVal: 0,
			endVal: 4000,
			increment: 180 / 100,
			incrementVal: 70,
			duration: duration
		});
		chartItemActive.eq(0).chart('show');
		clearTimeout(timeChart);
		timeChart = setTimeout(function(){
			chartItemActive.eq(1).chart('show');
		}, delayChart);
		chartItemUnactive.chart('show');
	}


	// This function uses the sort order for the table.
	var sortOrderTable = function(resTable){
		var trs = resTable.find('tbody tr');
		var tbody = resTable.find('tbody');
		var elems = $.makeArray(trs);
		var ascending = false;
		var sortTrigger = resTable.find('thead th:first a');
		var evenOrder = function(table){
			table.find('tbody tr:odd').removeClass().addClass('even');
			table.find('tbody tr:even').removeClass().addClass('old');
		};

		sortTrigger.off('click.sortTrigger').on('click.sortTrigger',function(){
			var self = $(this);
			// var emElem = $(this).find('em');
			ascending = !ascending;
			if(ascending){
				// emElem.removeClass().addClass('ico-point-d');
				self.removeClass().addClass('active');
			}
			else{
				self.removeClass();
				// emElem.removeClass().addClass('ico-point-u');
			}
			sortDate(elems,tbody);
			evenOrder(resTable);
		});

		var sortDate = function(elems,tbody){
			var els = elems.sort(function(tr1, tr2) {
				var date1 = new Date($.trim($(tr1).find('td:first span').text()));
				var date2 = new Date($.trim($(tr2).find('td:first span').text()));
				if (ascending) {
					return ((date1 > date2) ? 1 : -1 );
				} else {
					return ((date1 > date2) ? -1 : 1 );
				}
				return 0;
			});
			tbody.html(els);
		};
	};

	sortOrderTable(responsiveTable);

	// This function uses render for the Your bookings block.
	var renderYourBookings = function(){
		var dataAccordionWrapperContent = $('[data-accordion-wrapper-content="1"]');
		if (dataAccordionWrapperContent.length > 1) {
			dataAccordionWrapperContent = $(dataAccordionWrapperContent[1]);
		}
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.children('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfUpcomingFlights[idx];
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			dataAccordionWrapperContent.children('[data-accordion]').remove();
			$.get(config.url.kfAtAGlanceTemplate, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfUpcomingFlights
				});
				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};

	//this function used render for the Booking to be confirm block.
	var renderYourConfirmBookings = function(){
		var dataAccordionWrapperContent = $('.block--confirm-booking [data-accordion-wrapper-content="1"]');
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.find('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfBookingConfirm[idx];
									res.bookingConfirmUrl = dataAccordionWrapperContent.data('payment-url');
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			$.get(config.url.kfAtAGlanceTemplateSf, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfBookingConfirm
				});

				dataAccordionWrapperContent.children().remove();

				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};
	renderYourBookings();
	renderYourConfirmBookings();

	var renderInfoBox = function(){
		var infoBox = $('.info-box');

		infoBox.each(function(){
			var self = $(this);
			var infoBoxButton = $('.info__button', self);

			infoBoxButton.off('click').on('click', function(){
				self.remove();
			});
		});
	};

	renderInfoBox();
	
};
