/**
 * @name SIA
 * @description Define global addon functions
 * @version 1.0
 */

SIA.addon = function(){
	var global = SIA.global,
		config = global.config,
		win = $(window),
		htmlBody = $('html,body'),
		isNewAddonPage = htmlBody.is('.add-ons-1-landing-page'),
		isAddInsurance = htmlBody.is('.mp-1-add-insurance'),
		isBspPanel = $('[data-booking-summary-panel]').is('.bsp-booking-summary'),
		sortName = 'price-desc',
		arrSupplierChecked = [],
		arrLocalChecked = [],
		arrFuelChecked = [],
		arrAutomaticChecked = [],
		AirChecked,
		DoorChecked,
		partySize,
		sortBy,
		arrCarSizeChecked = null,
		initCarData = [],
		currentCarData = [],
		pickupDateObj,
		dropoffDateObj,
		hotelData = [];

	var bookingSummary = $('.booking-summary');
	var trigger = bookingSummary.find('.booking-summary__heading .booking-summary__control');
	var bookingSummaryContent = bookingSummary.find('.booking-summary__content');

	// var btnAdd = $('#insurance-1');
	var addBookingSummary = $('[data-add-booking-summary]');
	// var acceptIn = $('#checkbox-insurance');

	var baggage = $('.table-baggage');
	var insuranceLeave = $('[data-insurance-leave]');

	var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;

	var allTriggerAddon = addBookingSummary.add(baggage.find('tr .baggage-1 :checkbox'));

	var detectAdded = function(){
		var isAdded = false;
		allTriggerAddon.each(function(){
			var that = $(this);
			console.log(that, that.data('added'));
			if(that.data('added')){
				isAdded = true;
				return !isAdded;
			}
		});
		return isAdded;
	};

	// var checlAcceptInStatus = function(){
	// 	if(acceptIn.is(':checked')){
	// 		btnAdd.prop('disabled', false).removeClass('disabled');
	// 	}
	// 	else{
	// 		btnAdd.prop('disabled', true).addClass('disabled');
	// 	}
	// };

	// enable and diable oversive
	baggage.find('tr').each(function(){
		var self = $(this);
		var cb = self.find('.baggage-1 :checkbox');
		var os = self.find('.baggage-3 :checkbox');
		// var os = self.find('[data-block-baggage] :checkbox');
		var addOversize = function(){
			if(cb.is(':checked')){
				os.prop('disabled', false);
				os.closest('.custom-checkbox').removeClass('disabled');
				os.closest('.custom-checkbox').siblings('[data-tooltip]').removeClass('disabled');
				if(!bookingSummary.hasClass('active')){
					trigger.trigger('click.openBS');
				}
			}
			else{
				os.prop({
					'disabled': true,
					'checked': false
				});
				os.closest('.custom-checkbox').addClass('disabled');
				os.closest('.custom-checkbox').siblings('[data-tooltip]').addClass('disabled');
			}
			if(!cb.data('added')){
				cb.data('added', true);
			}
			else{
				cb.data('added', false);
			}
		};
		addOversize();
		cb.data('added', false);
		cb.off('change.addOversize').on('change.addOversize', addOversize);
	});

	// Confirm continue without add on page.
	insuranceLeave.off('click.leave').on('click.leave', function(e){
		e.preventDefault();

		var self = $(this);
		var popupInsuranceLeave = $('.popup--insurance-leave');

		if(detectAdded()){
			if(!popupInsuranceLeave.data('Popup')) {
				popupInsuranceLeave.Popup({
					overlayBGTemplate: SIA.global.config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]'
				});
			}
		}else{
			window.location.href = self.attr('href');
		}

		$('body').off('click.leave-page').on('click.leave-page', '[data-insurance-leave]', function(e) {
			e.preventDefault();
			var btn = $(this);
			var href = btn.attr('href');
			var target = btn.attr('target');

			popupInsuranceLeave
				.find('[data-confirm]')
				.off('click.unload')
				.on('click.unload', function() {
					window.onbeforeunload = null;
					if(href && href.indexOf('#') === 0) {
						if($(href).length) {
							$('html, body').scrollTop($(href).offset().top);
						}
					}
					else {
						if (target && target === '_blank') {
							window.open(href, target);
						}
						else {
							window.location.href = href;
						}
					}
					popupInsuranceLeave.Popup('hide');
				});
			if(detectAdded()){
				popupInsuranceLeave.Popup('show');
			}
		});
	});

	// booking nav
	trigger.off('click.openBS').on('click.openBS', function(e){
		e.preventDefault();
		if(bookingSummary.hasClass('active')){
			bookingSummaryContent.css('display', 'none');
			bookingSummary.removeClass('active');
		}
		else{
			bookingSummaryContent.css('display', 'block');
			bookingSummary.addClass('active');
		}
	});

	// checlAcceptInStatus();
	// acceptIn.off('change.acceptIn').on('change.acceptIn', function(){
	// 	checlAcceptInStatus();
	// });

	// 	Trigger click on link has data-toggle-value
	var synchronizeClone = function(el){
		var originEl = el.closest('.slide-item');
		if(originEl.length){
			$(originEl.data('slick-cloned')).find('a[data-toggle-value]').trigger('click.toggleLabel');
		}
	};

	addBookingSummary.off('click.add').on('click.add', function(e){
		e.preventDefault();
		var formEl = $('.accordion__content').find('[data-form-validate]');
		if(formEl.length > 0 && !formEl.valid()) {
			return;
		}
		var addedData = false,
				addOnAdded = $(this).closest('.addon-item').find('.addon-added'),
				accordionTrigger = $(this).closest('.addon-item').find('[data-accordion-trigger="1"]');

		addOnAdded.addClass('hidden');
		if($(this).next().val() === '' || $(this).next().val() === 'false'){
			addedData = true;
			if(addOnAdded.is('.hidden')) {
				addOnAdded.removeClass('hidden');
			}
		}
		$(this).data('added', addedData);
		synchronizeClone($(this));
		setTimeout(function(){
			if(!bookingSummary.hasClass('active')){
				trigger.trigger('click.openBS');
			}
		}, 200);
		if(accordionTrigger.is('.active')) {
			accordionTrigger.trigger('click.accordion');
			htmlBody.animate({ scrollTop: $(this).closest('.addon-item').offset().top}, 'slow');
		}
	});

	// This function use for init slick slide.
	var initSlickSlide = function(isNewAddon){
		isNewAddon = isNewAddon === undefined ? false : isNewAddon;
		// var win = global.vars.win;
		// var config = global.config;
		var flexSlider = $('.flexslider');
		// var slides = flexSlider.find('.slides');
		// var setAccordition;
		// var wrapperHLS = flexSlider.parent();

		// var loadBackgroundHighlight = function(self, parentSelt, idx, totalImage, fl){
		var init = function(fl){
			var next = fl.parent().siblings('.ico-point-r'),
				prev = fl.parent().siblings('.ico-point-l'),
				newNext = fl.children('.slick-next'),
				newPrev = fl.children('.slick-prev'),
				gl = fl.find('.slides'),
				hotelBlock = fl.closest('.hotel-block'),
				options = {},
				defaultOptions = {},
				customOptions = hotelBlock.data('slider-option') ? $.parseJSON(hotelBlock.data('slider-option').replace(/\'/gi, '"')) : {};

			defaultOptions = {
				dots: false,
				speed: 300,
				draggable: true,
				prevArrow: '',
				nextArrow: '',
				slidesToShow: 2,
				slidesToScroll: 2,
				accessibility: false,
				onAfterChange: function(){
					gl.css('visibility', 'visible');
				},
				onInit: function(){
					if(isMac){
						gl.find('.slick-slide img').css({
							'-webkit-transform': 'none'
						});
					}
				}
			};
			options = $.extend({}, defaultOptions, customOptions);
			// fl.width(wrapperHLS.width());
			fl.css('visibility', 'visible');
			if(!gl.is('.slick-initialized')) {
				gl.slick(options);
			}
			if(!isNewAddon) {

				next.off('click.next').on('click.next', function(e){
					e.preventDefault();
					gl.slick('slickNext');
				});
				prev.off('click.prev').on('click.prev', function(e){
					e.preventDefault();
					gl.slick('slickPrev');
				});

			} else {
				newNext.off('click.next').on('click.next', function(e){
					e.preventDefault();
					e.stopPropagation();
					gl.slick('slickNext');
				});
				newPrev.off('click.prev').on('click.prev', function(e){
					e.preventDefault();
					e.stopPropagation();
					gl.slick('slickPrev');
				});
			}

			// gl.find('a.btn-1').off('click.add').on('click.add', function(e){
			// 	e.preventDefault();
			// 	if(!bookingSummary.hasClass('active')){
			// 		trigger.trigger('click.openBS');
			// 	}
			// });
			// if(global.vars.detectDevice.isTablet()){
			// 	parentSelt.css({
			// 		'background-image': 'url(' + self.attr('src') + ')'
			// 	});
			// 	self.attr('src', config.imgSrc.transparent);
			// }
			// if(idx === totalImage - 1){
			// 	var next = fl.parent().siblings('.ico-point-r');
			// 	var prev = fl.parent().siblings('.ico-point-l');
			// 	var gl = fl.find('.slides');
			// 	fl.width(wrapperHLS.width());
			// 	fl.css('visibility', 'visible');
			// 	gl.slick({
			// 		dots: false,
			// 		speed: 300,
			// 		draggable: true,
			// 		prevArrow: '',
			// 		nextArrow: '',
			// 		slidesToShow: 2,
			// 		slidesToScroll: 2,
			// 		accessibility: false,
			// 		useCSS: global.vars.isNewIE() ? false : true
			// 	});

			// 	next.off('click.next').on('click.next', function(e){
			// 		e.preventDefault();
			// 		gl.slickNext();
			// 	});
			// 	prev.off('click.prev').on('click.prev', function(e){
			// 		e.preventDefault();
			// 		gl.slickPrev();
			// 	});
			// 	win.off('resize.flexSlider').on('resize.flexSlider',function() {
			// 		flexSlider.width(wrapperHLS.width());
			// 	}).trigger('resize.flexSlider');

			// 	setAccordition = setInterval(function(){
			// 		if(slides.hasClass('slick-initialized')){
			// 			initAccordion();
			// 			$('.slick-dots').css('margin-left', 0);
			// 			clearInterval(setAccordition);
			// 		}
			// 	}, 2000);
			// }
			$(fl).closest('.accordion__content').width($('.content-wrapper').width() - 20 * 2); //30 = padding
		};

		flexSlider.each(function(){
			var fl = $(this);
			// var totalImage = fl.find('img');
			init(fl);
			// totalImage.each(function(idx) {
			// 	var self = $(this);
			// 	var parentSelt = self.parent();
			// 	var nI = new Image();
			// 	nI.onload = function(){
			// 		loadBackgroundHighlight(self, parentSelt, idx, totalImage.length, fl);
			// 	};
			// 	nI.src = self.attr('src');
			// });
		});

		var originalWidth = $(window).width();
		$(window).on('resize.set-slider-width', function() {
			if(originalWidth !== $(this).width()) {
				originalWidth = $(this).width();
				flexSlider.each(function(f, fl) {
					$(fl).closest('.accordion__content').width($('.content-wrapper').width() - 20 * 2); //30 = padding
				});
			}
		});
	};

	initSlickSlide();

	// Toggle value of hidden input.
	var createAddOn = function() {
		$('body')
		.off('click.addOn')
		.on('click.addOn', '[data-add-booking-summary]', function() {
			var name = $(this).data('add-booking-summary');
			var toggleButton = $(this).data('toggle-value');
			var hiddenInput = $('input[name="' + name + '"]');
			if(typeof(toggleButton) !== 'undefined') {
				if(hiddenInput.val() === 'true') {
					hiddenInput.val('false');
				}
				else {
					hiddenInput.val('true');
				}
			}
			else {
				hiddenInput.val('true');
			}
		});
	};

	createAddOn();

	// render hotel addon
	var getHotelData = function() {
		$.ajax({
			url: config.url.addons.hotel.json,
			dataType: 'json',
			type: global.config.ajaxMethod,
			beforeSend: function() {},
			success: function(data) {
				if (data) {
					hotelData = data[0].responseBody.hotels;
					getHotelTemplate(data[0].responseBody);
				}
			},
			error: function(xhr, status) {
				if (status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	};

	var getHotelTemplate = function(data) {
		$.get(config.url.addons.hotel.template, function(tpl) {
			renderAllHotelList(tpl, data.hotels);
			hotelTemplateView(tpl, data);
			formSearchValidation();
		});
	};

	var renderAllHotelList = function(html, hotels) {
		var hotelBlock = $('.hotel-block'),
			listHotel = hotelBlock.find('.hotel-list'),
			seeMoreBtn = hotelBlock.find('.see-more-btn');

		var template = window._.template(html, { data: hotels });

		$(template).insertBefore(seeMoreBtn);

		initSlickSlide(true);
		if(SIA.accordion) {
			SIA.accordion();
		}

		if(SIA.initTabMenu) {
			SIA.initTabMenu();
		}
		if(SIA.multiTabsWithLongText) {
			SIA.multiTabsWithLongText();
		}

		if(SIA.plusOrMinusNumber) {
			SIA.plusOrMinusNumber();
		}
		$('.hotel-list [data-tooltip]').kTooltip();
		if(hotelBlock.is('[data-baidu-map]')) {
			SIA.baiduMap(true);
		} else {
			SIA.googleMap(true);
		}

		$('.hotel-list').each(function(i, e) {
			var _self = $(e),
				hotelControl = _self.find('.head-hotel.accordion__control'),
				hotelId = _self.data('hotel-id'),
				btnPoliClose = _self.find('[data-trigger-poli-close]'),
				btnAmenClose = _self.find('[data-trigger-amen-close]'),
				btnMapClose = _self.find('[data-trigger-map-close]'),
				parentAccordionControll = _self.parent().parent().siblings('[data-accordion-trigger="1"]'),
				triggerArrow = _self.find('[data-trigger-arrow]'),
				roomTable = _self.find('table[data-room-table]'),
				btnAddRoom = _self.find('[data-add-room]'),
				btnRemoveRoom = _self.find('[data-remove-room]'),
				addOnItem = _self.closest('.addon-item'),
				addOnDescription = addOnItem.find('.description'),
				addOnAddedFlag = addOnItem.find('.addon-added');


			function changeAccState(e) {
				e.preventDefault();
				var heightBspBooking = $('[data-booking-summary-panel]').height();
				if($('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
					hotelControl.trigger('click.accordion');
					htmlBody.animate({ scrollTop: hotelControl.offset().top - heightBspBooking }, 'slow');
				}else{
					hotelControl.trigger('click.accordion');
					htmlBody.animate({ scrollTop: _self.offset().top}, 'slow');
				}
			}

			btnPoliClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});
			btnAmenClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});
			btnMapClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});

			_self.off('click.accordion keyup.accordion').on('click.accordion keyup.accordion', function(e){
				e.preventDefault();

				if((e.keyCode !== 13 && e.keyCode !== undefined) || !$(e.target).is('.hotel-list') ) {
					return;
				}

				hotelControl.trigger('click.accordion');

			});

			triggerArrow.off('click.accordion').on('click.accordion', function(e){
				e.preventDefault();
				hotelControl.trigger('click.accordion');
			});

			//add room
			btnAddRoom.off('click.addroom').on('click.addroom', function(e){
				var _self = $(e),
						listLinkEl = $('<ul class="list-link">' +
												'<li><a href="#" class="trigger-accordion-added"><em class="ico-point-r--addon"></em>Edit add-on</a>'+
												'</li><li><a href="#" class="trigger-accordion-remove"><em class="ico-point-r--addon"></em>Cancel</a>' +
												'</li></ul>'),
						roomData,
						selfAddBtn = $(this),
						surcharges,
						totalEl;

				//closeAccordion
				// parentAccordionControll.trigger('click.accordion');
				//show/hide added hotel
				addOnItem.addClass('hotel-added-wrapper');
				if($('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
					var parentButtonClick =  $(this).closest('.addons-landing-content').find('.button-group-3');
					parentButtonClick.find('[data-select-item]').addClass('hidden');
					parentButtonClick.find('[data-select-item]').next().removeClass('hidden');
					addOnDescription.removeClass('hidden');
				}else{
					addOnDescription.addClass('hidden');
				}
				addOnAddedFlag.removeClass('hidden');
				addOnItem.find('[data-trigger-ico]').addClass('hidden');

				if(parentAccordionControll.find('.list-link').length === 0) {
					parentAccordionControll.append(listLinkEl);
				}

				roomData = $.grep(hotelData, function(hotel){
					return parseInt(hotel.hotelID) === selfAddBtn.data('selfhotelid')
				});

				roomData = $.grep(roomData[0].rooms, function(room){
					return parseInt(room.roomID) === selfAddBtn.data('roomid')
				});

				roomData = roomData[0];

				totalEl = '<span class="title-6--grey">Total price</span><span class="include-price">'+ roomData.currency + ' ' + roomData.rate.totalRate +'</span>';

				if(roomData.surcharges && roomData.surcharges.length > 0) {
					totalEl += '<p class="text">'
					$.grep(roomData.surcharges, function(surcharge){
						if(surcharge.surchargeType === 'Mandatory') {
							totalEl += '<span>Included in price: ' + (parseInt(surcharge.surchargeTax) > 0 ? 'Hotel tax ' + surcharge.surchargeTax + '%, ' : '') + surcharge.surchargeName + ' ' + roomData.currency + ' ' + surcharge.surchargeTotalAmount + '</span>';
						} else if(surcharge.surchargeType === 'Excluded') {
							totalEl += '<span>Not included in price (collected by property): ' + surcharge.surchargeName + ' ' + roomData.currency + ' ' + surcharge.surchargeTotalAmount + '</span>';
						}
					});
				}

				totalEl += '</p><p>By adding this room, you agree to the<a href="#">&nbsp;terms and conditions&nbsp;</a>of your hotel booking.</p>'
				if(!$('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
					renderAddOnAddedSales(hotelId, addOnItem, roomData);
				}

				//show/hide other btn
				// addOnItem.find('[data-add-room]').removeClass('hidden');
				// addOnItem.find('[data-remove-room]').addClass('hidden');

				// show/hide btn
				$('[data-add-room]').removeClass('hidden');
				$(this).addClass('hidden');
				$('[data-remove-room]').addClass('hidden');
				$(this).parent().find('[data-remove-room]').removeClass('hidden');

				var roomDetails = $(this).closest('.hotel-room--details');

				$('.hotel-room--details').each(function(){
					$(this).find('.terms-conditions').each(function(){
						if($(this).is('.added-room')) {
							$(this).empty();
							$(this).addClass('hidden');
						} else {
							$(this).removeClass('hidden');
						}
					});
				});

				roomDetails.find('.terms-conditions').each(function(){
					if($(this).is('.added-room')) {
						$(this).empty();
						$(this).append(totalEl);
						$(this).removeClass('hidden');
					} else {
						$(this).addClass('hidden');
					}
				});

				var totalPrice = roomDetails.find('.terms-conditions.added-room');

				setTimeout(function(){
					htmlBody.animate({ scrollTop: totalPrice.offset().top}, 'slow');
				}, 500);

				parentAccordionControll.children('.ico-point-r').addClass('hidden');
				parentAccordionControll.find('.list-link').removeClass('hidden');



				//scroll top

				var editAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-added'),
						cancelAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-remove');

				//edit add-on
				editAddonTriger.off('click.editAddon').on('click.editAddon', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(!parentAccordionControll.is('.active')){
						parentAccordionControll.trigger('click.accordion');
					}
					htmlBody.animate({ scrollTop: hotelControl.offset().top}, 'slow');
					if(!hotelControl.is('.active')){
						hotelControl.trigger('click');
					}
				});

				//cancel add-on
				cancelAddonTriger.off('click.cancelAddon').on('click.cancelAddon', function(e){
					e.preventDefault();
					e.stopPropagation();

					addOnItem.removeClass('hotel-added-wrapper');
					addOnItem.find('.add-ons-item-added').remove();
					addOnDescription.removeClass('hidden');
					addOnAddedFlag.addClass('hidden');
					addOnItem.find('[data-trigger-ico]').removeClass('hidden');
					parentAccordionControll.find('.list-link').addClass('hidden');

					// show/hide add/remove btn
					btnAddRoom.removeClass('hidden');
					btnRemoveRoom.addClass('hidden');
					parentAccordionControll.children('.ico-point-r').removeClass('hidden');
					htmlBody.animate({ scrollTop: addOnItem.offset().top}, 'slow');
					if(parentAccordionControll.is('.active')){
						parentAccordionControll.trigger('click.accordion');
					}

				});
			});

			btnRemoveRoom.off('click.removeroom').on('click.removeroom', function(e){
				var _self =$(e);
				var roomDetails = $(this).closest('.hotel-room--details');

				if($('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
					var parentButtonClick =  $(this).closest('.addons-landing-content').find('.button-group-3');
					parentButtonClick.find('[data-select-item]').removeClass('hidden');
					parentButtonClick.find('[data-select-item]').next().addClass('hidden');
				}

				addOnItem.removeClass('hotel-added-wrapper');
				addOnItem.find('.add-ons-item-added').remove();
				addOnDescription.removeClass('hidden');
				addOnAddedFlag.addClass('hidden');
				addOnItem.find('[data-trigger-ico]').removeClass('hidden');
				parentAccordionControll.find('.list-link').addClass('hidden');

				// show/hide add/remove btn
				btnAddRoom.removeClass('hidden');
				btnRemoveRoom.addClass('hidden');
				parentAccordionControll.children('.ico-point-r').removeClass('hidden');

				roomDetails.find('.terms-conditions').each(function(){
					if(!$(this).is('.added-room')) {
						$(this).removeClass('hidden');
					} else {
						$(this).empty();
						$(this).addClass('hidden');
					}
				});
				// htmlBody.animate({ scrollTop: addOnItem.offset().top}, 'slow');
				// if(parentAccordionControll.is('.active')){
				// 		parentAccordionControll.trigger('click.accordion');
				// 	}


			});

		});
	}


	var hotelTemplateView = function(html, data) {
		var hotelBlock = $('.hotel-block'),
			hotelItems = hotelBlock.find('.hotel-list'),
			slider = $('#slider-range'),
			labelFrom = slider.find('.ui-slider_from'),
			labelTo = slider.find('.ui-slider_to'),
			inputFrom = slider.find('[name="from-price"]'),
			inputTo = slider.find('[name="to-price"]'),
			initValue = [slider.data('current-min'), slider.data('current-max')],
			seeMoreBtn = hotelBlock.find('.see-more-btn'),
			location = $('#location'),
			customLocation = location.closest('[data-customselect]'),
			sort = $('#custom-select--sort-1'),
			customSort = sort.closest('[data-customselect]'),
			firstView = hotelBlock.data('hotel-number-per-page') || 5,
			nextView = seeMoreBtn.data('see-more') || 5,
			filteredData = [],
			newData = [],
			holderUI = [],
			ratingBlock = $('.rating-block'),
			rate = ratingBlock.children(),
			startRating = ratingBlock.data('start-rate'),
			slideFrom = initValue[0],
			slideTo = initValue[1],
			filteredItems,
			sortDropdown = $('#custom-select--sort-1'),
			formSearchWrapper = hotelBlock.siblings('[data-search-hotel]')
			searchBtn = formSearchWrapper.find('#search-hotel-btn'),
			checkinDay = $('[data-search-hotel]').find('[data-start-date]'),
			checkoutDay = $('[data-search-hotel]').find('[data-return-date]');

		//start set default today for check-in and today + 3 for checkout
		var today = new Date(),
			dd = today.getDate(),
			ddCheckout = dd + 3,
			mm = today.getMonth()+1,
			yyyy = today.getFullYear(),
			ckDay = "";

		dd = dd < 10 ? '0' + dd : dd;
		mm = mm < 10 ? '0' + mm : mm;
		ddCheckout = ddCheckout < 10 ? '0' + ddCheckout : ddCheckout;

		today = dd+"/"+mm+"/"+yyyy;
		ckDay = ddCheckout +"/"+mm+"/"+yyyy;

		checkinDay.val(today);
		checkoutDay.val(ckDay);
		//end set default day

		//range slider
		slideFrom = data.minHotelSliderVal ? parseInt(data.minHotelSliderVal) : initValue[0];
		slideTo = data.maxHotelSliderVal ? parseInt(data.maxHotelSliderVal) : initValue[1];

		slider.slider({
			range: true,
			min: slideFrom,
			max: slideTo,
			values: initValue,
			create: function() {
				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
			},
			slide: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
				inputFrom.val(slideFrom);
				inputTo.val(slideTo);
			},
			stop: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
				holderUI = [slideFrom, slideTo];
				filterHotelByPrice();
				// filterHotelByRating();
				// filterHotelByLocation(locationSelect.val());
			}
		});

		var starRating = function() {
			var rate = $('[data-start-rate]').children();
			if (!startRating) {
				startRating = ratingBlock.data('start-rate');
			}
			var chooseRate = function() {
				rate.removeClass('rated');
				rate.each(function(i) {
					var self = $(this);
					if (i < startRating) {
						self.addClass('rated');
					}
				});
			};
			var clearStar = function(){
        rate.removeClass('rated');
      };
			rate.each(function(i) {
				$(this).off('click.rate').on('click.rate', function(e) {
					e.preventDefault();
					startRating = i + 1;
						chooseRate();
					filterHotelByPrice();
				});
			});
			$('[data-clear]').on('click',function(e){
        e.preventDefault();
        clearStar();
        filterHotelByPrice();
      });
      $('[data-clear]').on('keydown', function(e){
        switch(e.keyCode){
          case 13:
            e.preventDefault();
            clearStar();
            filterHotelByPrice();
          break;
        }
      });
			chooseRate();
		};



		var handleSeeMoreBtn = function(){
			if(filteredItems){

				if(filteredItems.length > nextView){
					seeMoreBtn.removeClass('hidden');
					filteredItems.addClass('hidden');
					filteredItems.slice(0, nextView).removeClass('hidden');
				} else {
					seeMoreBtn.addClass('hidden');
				}

				seeMoreBtn.on('click.sshHotel', function(e){
					e.preventDefault();
					var hiddenItems = filteredItems.filter('.hidden');
					var numberOfHiddenItems = hiddenItems.length;
					hiddenItems.slice(0,(numberOfHiddenItems > nextView) ? nextView : numberOfHiddenItems).removeClass('hidden');
					hiddenItems = filteredItems.filter('.hidden');
					if(hiddenItems.length === 0){
						seeMoreBtn.addClass('hidden');
					}
				});
			}
		};

		var filterHotelByPrice = function() {
			hotelItems = hotelBlock.find('.hotel-list');
			var minPrice = parseInt(labelFrom.text().replace(',', ''));
			var maxPrice = parseInt(labelTo.text().replace(',', ''));
			// var formFilterHotel = $('#form-filter-hotel');
			var hotelItemFiltered = hotelItems.filter(function() {
				var val = parseInt($(this).data('price').toString().replace(',', ''));
				return val >= minPrice && val <= maxPrice;
			});
			if(hotelItemFiltered.length > 0){
				$('.hotel-list').removeClass('hidden');
				// update block no hotels available
				hotelBlock.find('.no-cars-or-hotel-available').addClass('hidden');
			}else{
				// update block no hotels available
				// formFilterHotel.addClass('hidden');
				hotelBlock.find('.no-cars-or-hotel-available').removeClass('hidden');
			}
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			filterHotelByRating();
		};

		var filterHotelByRating = function(isSearch) {
			var sliderV = $('#slider-range').slider('option', 'values');
			hotelItems = hotelBlock.find('.hotel-list').not('.hidden');
			// var formFilterHotel = $('#form-filter-hotel');
			var rateFilter = $('[data-start-rate]').children().filter('.rated').length;
			var hotelItemFiltered = hotelItems.filter(function() {
				var rate = $(this).data('rate');
				return rate >= rateFilter;
			});
			if(hotelItemFiltered.length > 0){
				// update block no hotels available
				hotelBlock.find('.no-cars-or-hotel-available').addClass('hidden');
			}else{
				// update block no hotels available
				// formFilterHotel.addClass('hidden');
				hotelBlock.find('.no-cars-or-hotel-available').removeClass('hidden');
			}
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			filteredItems = hotelItemFiltered;
			if(isSearch == undefined) {
				$('#aria-add-on-filter').text('');
				$('#aria-add-on-filter').text(L10n.wcag.sliderLabel.format(sliderV[0], sliderV[1])+ ', '+ L10n.wcag.foundLabel.format(filteredItems.length));
			}
			handleSeeMoreBtn();

			if(sortName === 'price-desc') {
				sortData('price', true);
			} else if(sortName === 'price-asc') {
				sortData('price');
			} else if(sortName === 'rate-desc') {
				sortData('rate', true);
			} else if(sortName === 'price-asc') {
				sortData('rate');
			}

		};

		sortDropdown.off('change.sortData').on('change.sortData', function(e){

			var value = $(this).val(),
					attrName;

			e.preventDefault();
			if(value === 'price-desc') {
				attrName = 'price';
				sortName = 'price-desc';
				sortData(attrName, true);
			} else if(value === 'price-asc') {
				attrName = 'price';
				sortName = 'price-asc';
				sortData(attrName);
			} else if (value === 'star-rating-asc') {
				attrName = 'rate';
				sortName = 'rate-asc';
				sortData(attrName);
			} else if (value === 'star-rating-desc') {
				attrName = 'rate';
				sortName = 'rate-desc';
				sortData(attrName, true);
			}

		});

		function sortData(attrName, isDesc) {
			if(!filteredItems){
				return;
			}

			isDesc = isDesc || false;
			var currentLength = filteredItems.not('.hidden').length;
			filteredItems.removeClass('hidden');
			var elArr = [];
			var dataVals = getValues.call(filteredItems, attrName);
			var dataValsSorted = dataVals.sort(function(a, b) {
				return a - b;
			});
			var len = dataValsSorted.length;
			filteredItems.detach();

			for (var i = 0; i < len; i++) {
				for (var j = 0; j < len; j++) {
					var item = filteredItems.eq(j).not('.sorted');
					if(parseFloat(item.length > 0 && item.attr('data-' + attrName).replace(',', '')) === dataValsSorted[i]){
						item.addClass('sorted')
								.addClass('hidden');
						elArr.push(item);
					}
				}
			}

			var list = isDesc ? elArr.reverse() : elArr;
			for (var i2 = 0; i2 < len; i2 ++) {
				if (i2 < 2) {
					$(list[i2]).removeClass('hidden');
				}
			}

			hotelBlock.prepend(list);
			// filteredItems = hotelBlock.find('.hotel-list');
			$('.sorted').removeClass('sorted');
			// list.addClass('hidden');
			// list.slice(0,currentLength).removeClass('hidden');
			// filteredItems.addClass('hidden');
			// filteredItems.slice(0,currentLength).removeClass('hidden');
			// if(filteredItems.slice(0,currentLength).length === 0){
			// 	$('.hotel-list').addClass('hidden');
			// }else{
			// 	$('.hotel-list').removeClass('hidden');
			// }
			handleSeeMoreBtn();
		}

		var getValues = function(attrName) {
			var len = this.length;
			var arr = [];
			for (var i = 0; i < len; i++) {
				arr.push(parseFloat(this.eq(i).attr('data-' + attrName).replace(',', '')));
			}
			return arr;
		};

		starRating();
		filterHotelByPrice();

		searchBtn.off('click.searchHotel').on('click.searchHotel', function(){
			var checkin = formSearchWrapper.find('[data-checkin-date]').val(),
				checkout = formSearchWrapper.find('[data-checkout-date]').val(),
				rooms = formSearchWrapper.find('[data-room] select').val(),
				adults = formSearchWrapper.find('[data-adult] select').val(),
				children = formSearchWrapper.find('[data-child] select').val(),
				hotelItemFiltered;

			hotelItems = hotelBlock.find('.hotel-list');
			hotelItemFiltered = hotelItems.filter(function() {
				var _self = $(this),
						checkinData = _self.data('hotel-checkinday'),
						checkoutData = _self.data('hotel-checkoutday'),
						allRoomsData = _self.data('hotel-allrooms'),
						roomsData = parseInt(_self.data('hotel-rooms')),
						adultsData = parseInt(_self.data('hotel-adults')),
						childrenData = parseInt(_self.data('hotel-children'));

				return (rooms === 0 ? true : rooms <= roomsData)
							&& (adults === 0 ? true : adults <= adultsData)
							&& (children === 0 ? true : children <= childrenData);
			});

			if(hotelItemFiltered.length > 0){
				$('.hotel-list').removeClass('hidden');
			}
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			starRating();
			filterHotelByPrice();

		});

	};

	if((isNewAddonPage || isBspPanel) && !isAddInsurance) {
		getHotelData();
	}

	var renderAddOnAddedSales = function(hId, addOnItem, roomData){
		var fillContent = function(res) {
			var listAddOnAddeds = addOnItem,
			addOnItemAdded = listAddOnAddeds.find('.add-ons-item-added'),
			checkinDate = $.datepicker.formatDate( "yy/mm/dd", new Date($('#check-in-day').val())),
			checkoutDate = $.datepicker.formatDate( "yy/mm/dd", new Date($('#check-out-day').val())),
			roomType = roomData.roomName,
			adult = parseInt($('#cabin-2').val()),
			child = parseInt($('#cabin-3').val()),
			guest = adult ? (adult > 1 ? adult + 'Adults' : adult + ' Adult') : '' + child ? (child > 1 ? adult + 'Childrens' : adult + ' Children') : '';

			if(listAddOnAddeds.length){
				$.get(global.config.url.addOnAddedTemplateSales, function (data) {
					var template = window._.template(data, {
						agodaBooking: res,
						checkinDate: checkinDate,
						checkoutDate: checkoutDate,
						roomType: roomType,
						guest: guest
					});
					//remove old added item
					if(addOnItemAdded.length) {
						addOnItemAdded.remove();
					}
					//add new added item
					$(template).prependTo(listAddOnAddeds);
				}, 'html');
				//render event
			}
		};
		_.forEach(globalJson.dataAddOn.agodaBookingList, function(value, i){
			if(parseInt(value.hotelID) === hId) {
				fillContent(value);
			}
		});
	};

  var formSearchValidation = function() {
		var formSearchHotel = $('[data-search-hotel]');
		formSearchHotel.each(function(i, e) {
			var _form = $(this);
			_form.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess
			});
		});
	};

	var formSearchCarValidation = function(){
		$('#form-search-car').validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	}

	formSearchCarValidation();

	var getCarData = function(url) {
		$.ajax({
			url: url,
			dataType: 'json',
			type: global.config.ajaxMethod,
			beforeSend: function() {},
			success: function(data) {
				if (data) {
					renderCheckbox(data.response.details.vehicles);
					getCarTemplate(data.response.details);
				}
			},
			error: function(xhr, status) {
				if (status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	}

	var renderListCar = function(filteredData) {
		var templateCarUrl = config.url.addons.car.templateCar,
				listCar = $('.addons-landing-block .accordion__content--car .list-cars-result')[0];

		currentCarData = filteredData;

		$(listCar).empty();

		$.get(templateCarUrl, function (data) {
			if(filteredData.length > 0) {
				var lengthSlideCarItem = $('#car-avaliable-slider').find('.slide-car-item').length;
				if(lengthSlideCarItem > 0){
					$('.accordion__content--car .addon-no-result').addClass('hidden');
				}else{
					$('.accordion__content--car .addon-no-result').removeClass('hidden');
				}
				var template = window._.template(data, {
					data: filteredData
				});
				$(template).appendTo($(listCar));
			} else {
				$('.accordion__content--car .addon-no-result').removeClass('hidden');
				// update block no cars available
				// var blockSearchFilter = $('.block-search-filter');
				// blockSearchFilter.find('#car-avaliable').addClass('hidden');
				// blockSearchFilter.find('.addon-car-editfilter').addClass('hidden');
				// blockSearchFilter.find('.list-cars-result').addClass('hidden');
			}
		$('.cancellation [data-tooltip]').kTooltip();
		}, 'html');
	}

	var renderSliderCar = function(dataCarSize, filterData) {
		var appendElement = $('.addons-landing-block .accordion__content--car #car-avaliable'),
				templateSliderUrl;
				if($('body').hasClass('add-ons-1-landing-slider-page')){
					templateSliderUrl = config.url.addons.car.templateSliderDouble;
				}else{
					templateSliderUrl = config.url.addons.car.templateSlider;
				}
		appendElement.empty();
    $.get(templateSliderUrl, function (data) {
      var template = window._.template(data, {
        data: dataCarSize
      });
      $(template).appendTo(appendElement);
      fitlerByCarSize(filterData);
      var lengthItem = $('.accordion__content--car').find('.slide-car-item').length;
      $('.addon-item').find('.accordion-car').on('click', function() {
      	sliderLandingSales();
      });
      $('#select-car-btn').closest('[data-trigger-mobile-popup]').on('click', function() {
      	sliderLandingSales();
      });
      $('#select-car-btn').closest('[data-trigger-mobile-popup]').on('click', function() {
      	sliderLandingSales();
      });
    }, 'html');

	}

	var sliderLandingSales = function(){
		$('.car-avaliable').find('#car-avaliable-slider').addClass('flexslider flexslider--3');
  	var totalLandscapeSlide = 5,
  			totalTabletSlide = 2;
    $('[data-mobile-slider]').each(function() {
      var slider = $(this);
      slider.find('img').each(function() {
        var self = $(this),
            newImg = new Image();
        newImg.onload = function() {
          slider.css('visibility', 'visible');
        slider.find('.slides').slick({
            siaCustomisations: true,
            dots: true,
            speed: 300,
            draggable: true,
            slidesToShow: totalLandscapeSlide,
            slidesToScroll: totalLandscapeSlide,
            accessibility: false,
            arrows: true,
            responsive: [
							{
								breakpoint: config.tablet,
								settings: {
									slidesToShow: totalTabletSlide,
									slidesToScroll: totalTabletSlide
								}
							},
							{
								breakpoint: config.mobile,
								settings: {
									slidesToShow: totalTabletSlide,
									slidesToScroll: totalTabletSlide
								}
							},
							{
								breakpoint: 480,
								settings: {
									slidesToShow: totalTabletSlide,
									slidesToScroll: totalTabletSlide
								}
							}
						]
          });
        };
        newImg.src = self.attr('src');
      });
    });
    $('.item-inner').css('padding-right', '4px');
    $('.slick-dots').css('margin-top', '10px');
	}

	var fitlerByCarSize = function(data){
		var filterData = [],
				carSizeItem = $('#car-avaliable-slider .slide-car-item'),
				removeEl = carSizeItem.find('[data-remove-carsize]'),
				filterTag = $('.addon-car-editfilter').find('.block-result');

		arrCarSizeChecked = null;

		carSizeItem.each(function(){
			$(this).off('click.filterCarSize').on('click.filterCarSize', function(e){
				e.preventDefault();
				var carSize = $(this).data('carsize'),
						carSizeDesc = $(this).data('carsize-desc'),
						addMoreResult = $('.block-item-choose').find('.add-more-result');
				if(!$(this).is('.active')) {
					carSizeItem.removeClass('active');
					filterTag.find('.result-item').each(function(){
						if($(this).data('carsize-tag') === arrCarSizeChecked) {
							$(this).remove();
						}
					});
					$(this).addClass('active');
					arrCarSizeChecked = carSize;

					if($('.block-result').find('.result-item').length < 4){
						addMoreResult.addClass('hidden');
						filterTag.append('<div class="result-item result-item-show" data-carsize-tag='+carSize+'><span>'+carSizeDesc+'</span><a href="#" class="close-btn" aria-label="'+carSizeDesc+' close"><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
					}else{
						addMoreResult.removeClass('hidden');
						filterTag.append('<div class="result-item hidden" data-carsize-tag='+carSize+'><span>'+carSizeDesc+'</span><a href="#" class="close-btn" aria-label="'+carSizeDesc+' close"><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
						var lengthItemShow = $('.block-result').find('.result-item').length - 4;
						addMoreResult.text('+ ' + lengthItemShow + ' more');
					}

					filterData = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);
					filterData = sortBy === 'Recommended' ? sortDataCar('order', filterData) : sortDataCar('price', filterData);
					if(JSON.stringify(filterData) !== JSON.stringify(currentCarData)) {
						renderListCar(filterData);
					}
				}
			});
		});

		removeEl.each(function(){
			$(this).off('click.removeFilterCarSize').on('click.removeFilterCarSize', function(e){
				e.preventDefault();
				e.stopPropagation();
				var selfItem = $(this).closest('.slide-car-item'),
						carSize = selfItem.data('carsize'),
						addMoreResult = $('.block-item-choose').find('.add-more-result');
				if(selfItem.is('.active')) {
					arrCarSizeChecked = null;
					selfItem.removeClass('active');
					filterTag.find('.result-item').each(function(){
						if($(this).data('carsize-tag') === carSize) {
							$(this).remove();
							if($('.block-result').find('.result-item').length < 5){
								$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
								addMoreResult.text('');
								addMoreResult.addClass('hidden');
							}else{
								if($('.block-result').find('.result-item.result-item-show').length < 4){
									$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
								}
								var lengthItemShow = $('.block-result').find('.result-item').length - 4;
								addMoreResult.text('+ ' + lengthItemShow + ' more');
							}
						}
					});

					filterData = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);
					filterData = sortBy === 'Recommended' ? sortDataCar('order', filterData) : sortDataCar('price', filterData);
					if(JSON.stringify(filterData) !== JSON.stringify(currentCarData)) {
						renderListCar(filterData);
					}
				}
			});
		});

	}

	var getCarSizeData = function(data) {
		var newObj = {},
		 		codes = {},
		 		listCarSize = [];

		$.grep(data,function(vehicle, vehicleIdx){
			var code = vehicle.carSize.code;
			if(!(code in codes)) {
				newObj[code] = [];
				codes[code] = true;
			}
			newObj[code].push(vehicle);
		});

		_.each(newObj, function(carSize, carSizeIdx){
			if(carSize.length === 1) {
				listCarSize.push(carSize[0]);
			} else {
				var minCarSize = carSize.sort(function(a,b){
					return parseInt(a.price.basePrice) - parseInt(b.price.basePrice)
				})[0];

			listCarSize.push(minCarSize);
			}
		});

		return listCarSize;
	}

	var getCarTemplate = function(carData) {
		pickupDateObj = new Date(carData.pickup.dateAndTime);
		dropoffDateObj = new Date(carData.dropoff.dateAndTime);
		var	pickupDateFormat = convertDateObj(pickupDateObj),
				dropoffDateFormat = convertDateObj(dropoffDateObj),
				pickupId = carData.pickup.locationID,
				pickupLocation = carData.pickup.locationName,
				dropoffId = carData.dropoff.locationID,
				dropoffLocation = carData.dropoff.locationName,
				filteredData = [],
				dataCarSize = [],
				vehiclesList = carData.vehicles,
				sortBySelect = $('.addon-car-editfilter .sortby__select').find('select'),
				partySizeSelect = $('[data-select-partysize]').find('select');


		initDataSearchForm(pickupDateFormat, dropoffDateFormat, pickupLocation, dropoffLocation);
		filteredData = filterDataByLocation(vehiclesList,pickupId,dropoffId,filteredData);
		dataCarSize = getCarSizeData(filteredData);

		filteredData = sortDataCar('order', filteredData);

		initCarData = filteredData;

		partySize = $('[data-select-partysize]').find('select').val();

		renderSliderCar(dataCarSize, filteredData);

		editFilters(filteredData);

		renderListCar(filteredData);

		sortBySelect.off('change.handleSort').on('change.handleSort', function(){

			sortBy === $(this).val();

			var data = sortDataCar($(this).val(), currentCarData);

			if(JSON.stringify(data) !== JSON.stringify(currentCarData)) {
				renderListCar(data);
			}

		});

		partySizeSelect.off('change.filterByPartySize').on('change.filterByPartySize', function(){

			partySize = $(this).val();
			var addMoreResult = $('.block-item-choose').find('.add-more-result');

			if(partySize !== '') {
				if($('[data-partysize]').length > 0) {
					$('[data-partysize]').find('span').attr('data-index', partySize).text(partySize);
				} else {
					if($('.block-result').find('.result-item').length < 4){
						addMoreResult.addClass('hidden');
						$('.block-result').last().append('<div class="result-item result-item-show" data-partySize="true"><span data-index="'+partySize+'">'+partySize+'</span><a href="#" class="close-btn" aria-label="'+partySize+' close" ><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
					}else{
						addMoreResult.removeClass('hidden');
						$('.block-result').last().append('<div class="result-item hidden" data-partySize="true"><span data-index="'+partySize+'">'+partySize+'</span><a href="#" class="close-btn" aria-label="'+partySize+' close" ><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
						var lengthItemShow = $('.block-result').find('.result-item').length - 4;
						addMoreResult.text('+ ' + lengthItemShow + ' more');
					}
				}
			}

			var data = filterDataByCheckbox(initCarData, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);

			data = sortBy === 'Recommended' ? sortDataCar('order', data) : sortDataCar('price', data);

			if(JSON.stringify(data) !== JSON.stringify(currentCarData)) {
				renderListCar(data);
			}

		});

	}

	var initDataSearchForm = function(pickupDateObj, dropoffDateObj, pickupLocation, dropoffLocation){
		var pickupEl = $('[data-pickup]'),
				dropoffEl = $('[data-dropoff]'),
				pickupDate = pickupEl.find('[data-time-input]'),
				pickupHour = pickupEl.find('.pickup-time.time-minute'),
				pickupMinute = pickupEl.find('.pickup-time.time-second'),
				pickupLocal = pickupEl.find('[data-cars-json]'),
				dropoffDate = dropoffEl.find('[data-time-input]'),
				dropoffHour = dropoffEl.find('.dropoff-time.time-minute'),
				dropoffMinute = dropoffEl.find('.dropoff-time.time-second'),
				dropoffLocal = dropoffEl.find('[data-cars-json]');


		pickupHour.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(pickupDateObj.timeFormat.split(':')[0])) {
				$(this).attr('selected', 'selected');
			}
		});

		pickupMinute.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(pickupDateObj.timeFormat.split(':')[1])) {
				$(this).attr('selected', 'selected');
			}
		});

		pickupHour.defaultSelect('update');
		pickupMinute.defaultSelect('update');

		pickupHour.find('.select__text').text(pickupDateObj.timeFormat.split(':')[0]);

		pickupMinute.find('.select__text').text(pickupDateObj.timeFormat.split(':')[1]);


		dropoffHour.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(dropoffDateObj.timeFormat.split(':')[0])) {
				$(this).attr('selected', 'selected');
			}
		});

		dropoffMinute.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(dropoffDateObj.timeFormat.split(':')[1])) {
				$(this).attr('selected', 'selected');
			}
		});

		dropoffHour.defaultSelect('update');
		dropoffMinute.defaultSelect('update');

		dropoffHour.find('.select__text').text(dropoffDateObj.timeFormat.split(':')[0]);

		dropoffMinute.find('.select__text').text(dropoffDateObj.timeFormat.split(':')[1]);

		pickupDate.val(pickupDateObj.dateFormat);
		dropoffDate.val(dropoffDateObj.dateFormat);

		if($('#locationForRentalCars') && $('#locationForRentalCars').val() !== '') {
			var term1 = $('#locationPickupForRentalCars').val(),
					term2 = $('#locationDropoffForRentalCars').val(),
					prepareApiSrc1 = 'http://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=1&solrTerm=' + term1,
					prepareApiSrc2 = 'http://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=1&solrTerm=' + term2;
			$.get(prepareApiSrc1, function(data) {
				var data1 = data.results.docs[0];
				pickupLocal.val(data1.name + ', ' + data1.country);
				$('.accordion__content--car input[name="pickup-locationID"]').val(data1.locationId);
				$('.accordion__content--car input[name="pickup-country"]').val(data1.country);
				$('.accordion__content--car input[name="pickup-city"]').val(data1.city);

			});

			$.get(prepareApiSrc2, function(data) {
				var data1 = data.results.docs[0];
				dropoffLocal.val(data1.name + ', ' + data1.country);
				$('.accordion__content--car input[name="dropoff-locationID"]').val(data1.locationId);
				$('.accordion__content--car input[name="dropoff-country"]').val(data1.country);
				$('.accordion__content--car input[name="dropoff-city"]').val(data1.city);

			})
		}

	}

	var filterDataByLocation = function(vehiclesList, pickupId, dropoffId,filteredData) {

		var filteredData = vehiclesList.filter(function(vehicle){
			var selfDropoffId = vehicle.route.dropOff.locationID,
					selfPickupId = vehicle.route.pickUp.locationID;
			return (selfDropoffId === dropoffId) && (selfPickupId === pickupId);
		});
		return filteredData;
	}

	var filterDataByCheckbox = function(data, arrCarsize, arrSupplier, arrLocal, arrFuel, arrAutomatic, aircondition, doors, partySize){
		var filterData = [],
				filterDataCarSize = [],
				filterDataSupplier = [],
				filterDataLocation = [],
				filterDataFuel = [],
				filterDataAutomatic = [],
				selfData = [];

		if(arrCarsize) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.carSize.code === arrCarsize;
			});
			data = selfData;
		}

		if(arrSupplier.length > 0) {
			$.grep(arrSupplier, function(supplier, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.supplier.supplierName === supplier;
				});
				filterDataSupplier = $.unique([].concat.apply([],[filterDataSupplier,selfData]));
			});
			data = filterDataSupplier;
		}

		if(arrLocal.length > 0) {
			$.grep(arrLocal, function(local, idx){
				selfData = [],
				local = true;
				selfData = data.filter(function(vehicle){
					return vehicle.route.pickUp.onAirport === local;
				});
				filterDataLocation = $.unique([].concat.apply([],[filterDataLocation,selfData]));
			});
			data = filterDataLocation;
		}

		if(arrFuel.length > 0) {
			$.grep(arrFuel, function(fuel, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.fuelPolicy.description === fuel;
				});
				filterDataFuel = $.unique([].concat.apply([],[filterDataFuel,selfData]));
			});
			data = filterDataFuel;
		}

		if(aircondition) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.aircon === 'Yes';
			});
			data = selfData;
		}

		if(arrAutomatic.length > 0) {
			$.grep(arrAutomatic, function(other, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.automatic === other;
				});
				filterDataAutomatic = $.unique([].concat.apply([],[filterDataAutomatic,selfData]));
			});
			data = filterDataAutomatic;
		}

		if(doors) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.doors >= 4;
			});
			data = selfData;
		}

		selfData = [];
		selfData = data.filter(function(vehicle){
			if(partySize === "1 to 2") {
				return parseInt(vehicle.seats) <= 5;
			} else if(partySize === "3 to 4") {
				return parseInt(vehicle.seats) === 5;
			} else if(partySize === "5 and more") {
				return parseInt(vehicle.seats) >= 5;
			} else {
				return parseInt(vehicle.seats) >= 1;
			}
		});
		data = selfData;

		return data;

	}

	var sortDataCar = function(name, data, isDesc){
		if(name === 'order') {
			data.sort(function(a, b){
				return parseInt(a.order) - parseInt(b.order);
			});
		} else if(name === 'price') {
			data.sort(function(a, b){
				return (parseInt(a.price.basePrice) + parseInt(a.price.discount)) - (parseInt(b.price.basePrice) + parseInt(b.price.discount));
			});
		}

		return data;
	}

	var getCheckbox = function(item, arrCheckbox, flag){
		var itemName = item,
				flag;

		if(arrCheckbox.length === 0) {
			arrCheckbox.push(itemName);
		} else {
			for(var i = 0; i < arrCheckbox.length; i ++) {
				if(arrCheckbox[i] === itemName) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
			if(flag === false) {
				arrCheckbox.push(itemName);
			}
		}

		return arrCheckbox;
	}

	var checkOther = function(item, checkAir, check) {
		var flag = false;

		for(var i = 0; i < item.length; i++) {
			if(!checkAir) {
				if(parseInt(item[i].doors) > 4) {
					flag = true;
					break;
				}
			}
			else {
				if(item[i].aircon) {
					flag = true;
					break;
				}
			}
		}

		return flag;
	}

	var appendCheckbox = function(el, arrCheckbox, flag, air, door) {
		var listCheckbox = '';

		el.empty();

		flag === true ? listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ air +'" id="'+ air.replace(/\s/g,"-") +'" aria-labelledby="'+ air +'" type="checkbox" data-aircondition value="'+air+'" aria-label="'+ air +'"><label for="'+ air.replace(/\s/g,"-") +'">'+ air +'</label></div></li>' : ''

		$.grep(arrCheckbox, function(item, idx){
			listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ item +'" id="'+ item.replace(/\s/g,"-") +'" type="checkbox" data-automatic value="'+item+'" aria-label="'+ item +'"><label for="'+ item.replace(/\s/g,"-") +'">'+ item +'</label></div></li>'
		});

		flag === true ? listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ door +'" id="'+ door.replace(/\s/g,"-") +'" aria-labelledby="'+ door +'" type="checkbox" data-4door value="'+door+'" aria-label="'+ door +'"><label for="'+ door.replace(/\s/g,"-") +'">'+ door +'</label></div></li>' : ''

		el.append($(listCheckbox));
	}

	var renderCheckbox = function(data) {
		var arrSupplier = [],
				arrFuel = [],
				arrCarAutomatic = [],
				arrSupplierLocal = [],
				flag = false,
				supplierCheckbox = $('[data-check-supplier]'),
				localtionCheckbox = $('[data-check-location]'),
				fuelCheckbox = $('[data-check-fuel]'),
				carspecificCheckbox = $('[data-check-carspecific]'),
				CarAir,
				CarDoor;


		for(var i = 0; i < data.length; i++) {
			if(data[i].supplier.supplierName) {
				flag = true;
				break;
			}
		};

		flag ? arrSupplierLocal.push('In Terminal') : '';


		checkOther(data, true) ? CarAir = 'Air-conditioning' : '';

		$.grep(data, function(vehicle, idx){
			arrSupplier = getCheckbox(vehicle.supplier.supplierName, arrSupplier);
			arrFuel = getCheckbox(vehicle.fuelPolicy.description, arrFuel);
			arrCarAutomatic = getCheckbox(vehicle.automatic, arrCarAutomatic);
		});

		checkOther(data, false) ? CarDoor = '4+ Doors' : '';

		appendCheckbox(supplierCheckbox, arrSupplier, false, null, null);
		appendCheckbox(localtionCheckbox, arrSupplierLocal, false, null, null);
		appendCheckbox(fuelCheckbox, arrFuel, false, null, null);
		appendCheckbox(carspecificCheckbox, arrCarAutomatic, true, CarAir, CarDoor);

	}

	var editFilters = function(data){
		var blockwrapper = $('.addon-car-editfilter'),
				blockFilters = blockwrapper.find('.block-item-choose'),
				showContentChoose = blockwrapper.find('.show-block-content'),
				resulItem = blockwrapper.find('.result-item'),
				closeBtn = blockwrapper.find('.close-btn'),
				itemChoose = blockwrapper.find('.list').find('input'),
				closeBlock = $('.block-content').find('.close-addon'),
				applyClick = $('.block-content').find('.btn-apply'),
				dataAfterCheckbox = [],
				filteredData;
		showContentChoose.on('click',function(e){
			$(this).addClass('hidden');
			blockwrapper.find('.block-content').removeClass('hidden');
			e.preventDefault();
		});
		$(document).on('click', '.close-addon', function(e){
			e.preventDefault();
			$(this).closest('.block-content').addClass('hidden');
		});
		$(document).on('click','.close-btn', function(e){
			e.preventDefault();
			var indexClose = $(this).closest('.result-item');
			var addMoreResult = $('.block-item-choose').find('.add-more-result');
			if(indexClose.data('carsize-tag')) {
				var carSize = indexClose.data('carsize-tag');
				$('[data-carsize="'+carSize+'"] [data-remove-carsize]').trigger('click.removeFilterCarSize');
			} else if(indexClose.data('partysize')) {
				$('[data-select-partysize]').find('.select__text').text('Select');
				$('[data-select-partysize]').find('select').val('');
			} else {
				var dataIndex = indexClose.find('span').data('index');
				itemChoose.each(function(index, obj){
					if(obj.id === dataIndex){
						$(this).attr('checked', false);
					}
				});
			}
			applyClick.trigger('click.startFilter', true);
			indexClose.remove();
			if($('.block-result').find('.result-item').length < 5){
				$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
				addMoreResult.text('');
				addMoreResult.addClass('hidden');
			}else{
				$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
				var lengthItemShow = $('.block-result').find('.result-item').length - 4;
				addMoreResult.text('+ ' + lengthItemShow + ' more');
			}
		});
		itemChoose.on('click', function(){
			var dataId = $(this).attr('id');
			var nameItem = $(this).closest('.custom-checkbox').find('label').text();
			var resultItem = $('.block-result').find('.result-item');
			var addMoreResult = $('.block-item-choose').find('.add-more-result');
			if($(this).is(':checked')){
				if($('.block-result').find('.result-item').length < 4){
					addMoreResult.addClass('hidden');
					blockFilters.find('.block-result').last().append('<div class="result-item result-item-show"><span data-index="'+dataId+'">'+nameItem+'</span><a href="#" class="close-btn" aria-label="'+nameItem+' close" ><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
				}else{
					addMoreResult.removeClass('hidden');
					blockFilters.find('.block-result').last().append('<div class="result-item hidden"><span data-index="'+dataId+'">'+nameItem+'</span><a href="#" class="close-btn" aria-label="'+nameItem+' close" ><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
					var lengthItemShow = $('.block-result').find('.result-item').length - 4;
					addMoreResult.text('+ ' + lengthItemShow + ' more');
				}
			}else{
				resultItem.each(function(index, obj){
					if(obj.children[0].attributes[0] && obj.children[0].attributes[0].value === dataId){
						$(this).remove();
						if($('.block-result').find('.result-item').length < 5){
							$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
							addMoreResult.text('');
							addMoreResult.addClass('hidden');
						}else{
							if($('.block-result').find('.result-item.result-item-show').length < 4){
								$('.block-result').find('.result-item-show').last().next().addClass('result-item-show').removeClass('hidden');
							}
							var lengthItemShow = $('.block-result').find('.result-item').length - 4;
							addMoreResult.text('+ ' + lengthItemShow + ' more');
						}
					}
				});
			}
		});
		closeBlock.on('click',function(){
			blockwrapper.find('.block-content').addClass('hidden');
			showContentChoose.removeClass('hidden');
		});
		applyClick.off('click.startFilter').on('click.startFilter',function(e, flag){

			arrSupplierChecked = [];
			arrLocalChecked = [];
			arrFuelChecked = [];
			arrAutomaticChecked = [];
			AirChecked = null;
			DoorChecked = null;

			 $('[data-check-supplier]').find('input[type="checkbox"]:checked').each(function(){
			 	arrSupplierChecked.push($(this).val());
			 });

			 $('[data-check-location]').find('input[type="checkbox"]:checked').each(function(){
			 	arrLocalChecked.push($(this).val());
			 });

			 $('[data-check-fuel]').find('input[type="checkbox"]:checked').each(function(){
			 	arrFuelChecked.push($(this).val());
			 });

			 $('[data-check-carspecific]').find('[data-automatic]:checked').each(function(){
			 	arrAutomaticChecked.push($(this).val());
			 });

			 $('[data-check-carspecific]').find('[data-aircondition]:checked').each(function(){
			 	AirChecked = $(this).val();
			 });

			 $('[data-check-carspecific]').find('[data-4door]:checked').each(function(){
			 	DoorChecked = $(this).val();
			 });

			 sortBy = $('addon-car-editfilter .sortby__select').find('select').val();

			 partySize = $('[data-select-partysize]').find('select').val();


			dataAfterCheckbox = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);

			if(!flag) {
				blockwrapper.find('.block-content').addClass('hidden');
				showContentChoose.removeClass('hidden');
			}

			filteredData = sortBy === 'Recommended' ? sortDataCar('order', dataAfterCheckbox) : sortDataCar('price', dataAfterCheckbox);

			if(JSON.stringify(filteredData) !== JSON.stringify(currentCarData)) {
				renderListCar(filteredData);
			}

		});

	};


	var convertDateObj = function(obj) {
		var date = $.datepicker.formatDate('dd/mm/yy', obj),
				hour = obj.getHours(),
				minute = (obj.getMinutes() < 10) ? '0' + obj.getMinutes() : obj.getMinutes(),
				time = hour + ' : ' + minute,
				obj = {
					dateFormat : date,
					timeFormat : time
				};
		return obj;
	}

	var url = $('.addons-landing-content .accordion__content--car').data('carjson');

	!isAddInsurance &&  getCarData(url);

	var resetFilter = function(){
		var btnReset = $('.addon-no-result .reset');

		btnReset.off('click.resetFilter').on('click.resetFilter', function(e){
			e.preventDefault();
			handleResetFilter()
		});

	}

	var handleResetFilter = function(){
		arrSupplierChecked = [];
		arrLocalChecked = [];
		arrFuelChecked = [];
		arrAutomaticChecked = [];
		AirChecked = null;
		DoorChecked = null;
		arrCarSizeChecked = null;
		partySize = "";

		$('[data-select-partysize]').customSelect('updateSync');

		$('#car-avaliable-slider .slide-car-item').removeClass('active');
		$('.block-apply input[type="checkbox"]').removeAttr('checked');
		$('.addon-car-editfilter .block-result').empty();
		renderListCar(initCarData);
	}

	resetFilter();

  var selectCars = function(url, dataVehicleid){
    var tplBookingCar;
    var appendAfterDiv = $('.accordion__content--car');
    var tplBlockCar = function(data1){
      $.get(global.config.url.addOnCarSelected, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateBlockCar = $(template);
        appendAfterDiv.find('.block-search-filter').addClass('hidden');
        appendAfterDiv.append(templateBlockCar);
        $('#form-selected-car').find('[data-customselect]').each(function(){
        	$(this).defaultSelect('refresh');
        });
        SIA.forceInput();
        $('#form-selected-car').validate({
					focusInvalid: true,
					errorPlacement: global.vars.validateErrorPlacement,
					success: global.vars.validateSuccess
				});
				enableConfirmBtn();
        win.scrollTop(appendAfterDiv.offset().top);
        addedCar(data1);
        $('.cancellation [data-tooltip]').kTooltip();
        $('.editor [data-tooltip]').kTooltip();
      });
    };
    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data2 = reponse.response.details;
        var vehicle = data2.vehicles;
        for(var i = 0,length = vehicle.length ; i < length; i++) {
        	if(dataVehicleid == parseInt(vehicle[i].vehicleId)){
        		var vehicleItem = vehicle[i];
        	}
        }
        tplBlockCar(vehicleItem);
      }
    });
  };

  var selectTermAndonditions = function(url){
    var TermAndonditions;
    var appendBeforeDiv = $('.popup--add-ons-car-term-condition-1');
    var tplTermAndonditions = function(data1){
      $.get(global.config.url.addOnCarTermAndonditions, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateTermAndonditions = $(template);
        appendBeforeDiv.append(templateTermAndonditions);
      });
    };
    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data2 = reponse.response.details;
        tplTermAndonditions(data2);
      }
    });
  };
  selectTermAndonditions(url);

  var enableConfirmBtn = function(){
  	var validator = $('#form-selected-car').validate();
  	  	firstName = $('#form-selected-car').find('input[name="first-name"]'),
  			surName = $('#form-selected-car').find('input[name="sur-name"]'),
  			confirm = $('#form-selected-car').find('input[name="confirm"]'),
  			checkTerms = $('#form-selected-car').find('input[name="terms"]');

  	$('#form-selected-car').find('input[name="first-name"], input[name="sur-name"]').each(function(){
  		$(this).off('keyup.enableConfirm').on('keyup.enableConfirm', function(){
  			if(validator.checkForm() === true && checkTerms.is(':checked')) {
  				confirm.removeAttr('disabled').removeClass('disabled');
  			} else {
  				confirm.attr('disabled', true);
  				confirm.addClass('disabled');
  			}
  		});
  	});

  	$('#title').off('change.enableConfirm').on('change.enableConfirm', function(){
  		if(validator.checkForm() === true && checkTerms.is(':checked')) {
  			confirm.removeAttr('disabled').removeClass('disabled');
  		} else {
  			confirm.attr('disabled', true);
  			confirm.addClass('disabled');
  		}
  	});

  	checkTerms.off('change.enableConfirm').on('change.enableConfirm', function(){
  		if(validator.checkForm() === true && checkTerms.is(':checked')) {
  			confirm.removeAttr('disabled').removeClass('disabled');
  		} else {
  			confirm.attr('disabled', true);
  			confirm.addClass('disabled');
  		}
  	});
  }

  var addedCar = function(data){
  	$('.block-extras input[name="confirm"]').off('click.addedCar').on('click.addedCar', function(){
  			var addonItem = $(this).closest('.addon-item'),
  		 			parentAccordionControll = addonItem.find('[data-accordion-trigger]'),
  		 			desc = addonItem.find('.description'),
  					listLinkEl = $('<ul class="list-link">' +
												'<li><a href="#" class="trigger-accordion-added"><em class="ico-point-r--addon"></em>Edit add-on</a>'+
												'</li><li><a href="#" class="trigger-accordion-remove"><em class="ico-point-r--addon"></em>Cancel</a>' +
												'</li></ul>');
  		if(!$('body').hasClass('mp-add-ons-page')) {
	  		parentAccordionControll.find('.ico-point-r').addClass('hidden');

	  		if(parentAccordionControll.find('.list-link').length === 0) {
	  			parentAccordionControll.append(listLinkEl);
	  		}

	  		parentAccordionControll.find('.list-link').removeClass('hidden');

	  		desc.addClass('hidden');

	  		if(addonItem.find('.add-ons-item-added').length <= 0) {
	  			renderCarAddonAdded(addonItem, data);
	  		}

	  		addonItem.find('.addon-added').removeClass('hidden');
  		}else{
  			var parentButtonClick =  $(this).closest('.addons-landing-content').find('.button-group-3');
  			var itemClicked = parentButtonClick.find('[data-select-item]');
  			var itemClickedNext = itemClicked.next();
				itemClicked.addClass('hidden');
				itemClickedNext.removeClass('hidden');
				itemClickedNext.next().find('em').removeClass('hidden');
  		}

  		$('.car-extra .btn-back').trigger('click.backResultList');

  		// htmlBody.animate({ scrollTop: addonItem.offset().top}, 'slow');

			var editAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-added'),
					cancelAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-remove');

			cancelAddonTriger.off('click.cancelAddon').on('click.cancelAddon', function(e){
				e.preventDefault();
				e.stopPropagation();
				addonItem.find('.addon-added').addClass('hidden');
				desc.removeClass('hidden')
				addonItem.find('.add-ons-item-added').remove();
				parentAccordionControll.find('.list-link').addClass('hidden');
				parentAccordionControll.children('.ico-point-r').removeClass('hidden');
				htmlBody.animate({ scrollTop: addonItem.offset().top}, 'slow');
				if(parentAccordionControll.is('.active')){
					parentAccordionControll.trigger('click.accordion');
				}

				$('.accordion__content--car').find('.car-extra').remove();
				$('.accordion__content--car .block-search-filter').removeClass('hidden');
				$('.accordion__content--car').find('.list-cars-result input[name="remove-car"]').addClass('hidden');
    		$('.accordion__content--car').find('.list-cars-result input[name="select-car"]').removeClass('hidden');

    		handleResetFilter();

			});
  	});
  }

  var renderCarAddonAdded = function(el, dataCarAdded) {
  	var pickupDate = covertDateToRender(pickupDateObj),
  			dropoffDate = covertDateToRender(dropoffDateObj);

  	$.get(global.config.url.addons.car.templateCarAdded, function (data) {
  		var template = window._.template(data, {
  			data: dataCarAdded,
  			pickupDate: pickupDate,
  			dropoffDate: dropoffDate
  		});
  		$(template).insertBefore(el.find('[data-accordion-trigger]'));
  	}, 'html');

  }

  var covertDateToRender = function(objdate){
  	var hour = objdate.getHours(),
  			minute = (objdate.getMinutes() < 10) ? '0' + objdate.getMinutes() : objdate.getMinutes(),
  			date = $.datepicker.formatDate( "yy MM dd (D)", objdate),
  			dateAfter;

  	dateAfter = hour < 12 ? (hour + ':' + minute + ' AM ' + date) : (hour + ':' + minute + ' PM ' + date);

  	return dateAfter;
  }

  $(document).on('click','input[name="select-car"]', function(){
    var dataVehicleid = $(this).closest('.item-result').data('vehicleid'),
    		selectBtn = $(this);

    setTimeout(function(){
    	removeCarAdded();
    	selectCars(url, dataVehicleid);
    	selectBtn.closest('.list-cars-result').find('input[name="select-car"]').removeClass('hidden');
    	selectBtn.closest('.list-cars-result').find('input[name="remove-car"]').addClass('hidden');
    	selectBtn.addClass('hidden');
    	selectBtn.next().removeClass('hidden');
    	$(document).ready(function(){
        var mySetInterval = setInterval(function(){
        	var dataID = $('.accordion__content--car').find('.car-extra').attr('data-id', dataVehicleid);
	        clearInterval(mySetInterval);
	    	},300);
	    });
    }, 100);

  });

  var removeCarAdded = function() {
  	var addonItem = $('.accordion__content--car').closest('.addon-item'),
  			desc = addonItem.find('.description');

  		addonItem.find('.addon-added').addClass('hidden');
  		addonItem.find('.ico-point-r').removeClass('hidden');
  		addonItem.find('.list-link').addClass('hidden');
  		desc.removeClass('hidden');
			addonItem.find('.add-ons-item-added').remove();

  }

  $(document).on('click.backResultList', '#remove-car, .car-extra > .btn-back', function(e) {
  	e.preventDefault();
    $('.accordion__content--car').find('.car-extra').remove();
    $('.accordion__content--car .block-search-filter').removeClass('hidden');
    setTimeout(function(){
			htmlBody.animate({ scrollTop: $('.list-cars-result').offset().top}, 'slow');
    },300);
  });

  $(document).on('click', 'input[name="remove-car"]', function(e) {
  	e.preventDefault();
    $('.accordion__content--car').find('.list-cars-result input[name="remove-car"]').addClass('hidden');
    $('.accordion__content--car').find('.list-cars-result input[name="select-car"]').removeClass('hidden');
    if($('body').hasClass('mp-add-ons-page')) {
			var parentButtonClick =  $(this).closest('.addons-landing-content').find('.button-group-3');
			var itemClicked = parentButtonClick.find('[data-select-item]');
			var itemClickedNext = itemClicked.next('[data-select-item]');
			itemClicked.removeClass('hidden');
			itemClickedNext.addClass('hidden');
			itemClickedNext.next().find('em').removeClass('hidden');
		}
    removeCarAdded();
  });

  $(document).on('click', '#remove-protection', function() {
 		var groupButton = $('.button-group-2');
  	$(this).addClass('hidden');
  	$('.check-full-protection').find('#full-protection').prop('checked', false);
  	groupButton.find('#add-protection').removeClass('hidden');
  	groupButton.children().eq(0).addClass('hidden');
		groupButton.children().eq(1).removeClass('hidden');
  });

  $(document).on('click', '#add-protection', function() {
  	var groupButton = $('.button-group-2');
  	$(this).addClass('hidden');
  	$('.check-full-protection').find('#full-protection').prop('checked', true);
  	groupButton.find('#remove-protection').removeClass('hidden');
  	groupButton.children().eq(0).removeClass('hidden');
		groupButton.children().eq(1).addClass('hidden');
  });

  $('.accordion__content--car').off('click.handleSeemoreCar').on('click.handleSeemoreCar', '[data-seemore-car]', function(){
  	 var listCar = $(this).closest('.list-cars-result'),
  	 		 listCarItem = listCar.find('.item-result'),
  	 		 count = 2,
  	 		 noHiddenIdx = listCarItem.not('.hidden').length;
  	 for(var i = noHiddenIdx; i < noHiddenIdx + count; i++) {
  	 	listCarItem.eq(i).removeClass('hidden');
  	 }
  	 if(listCarItem.not('.hidden').length === listCarItem.length) {
  	 	$(this).addClass('hidden');
  	 }
  });

  $('.block-search-filter').on('click','.custom-checkbox > input', function() {
  	var agedDriver = $('.driver-field').find('#drive-age-1');
  	var validateAged =  agedDriver.find('.input-5__text');
  	var checkboxNoError = $('.driver-field').find('.custom-checkbox');
  	if($(this).is(':checked')){
  		agedDriver.addClass('hidden');
  		validateAged.find('input').attr('data-rule-required', false);
  		agedDriver.next('.text-error').addClass('hidden');
  		checkboxNoError.find('label').addClass('style-checkbox-error');
  	}else{
  		validateAged.find('input').attr('data-rule-required', true);
  		agedDriver.removeClass('hidden');
  		checkboxNoError.find('label').removeClass('style-checkbox-error');
  	}
  });

	var popup1 = $('.popup--add-ons-car-popup-excess-explained');
  var popup2 = $('.popup--add-ons-car-term-condition-1');
  $(document).on('click', '.trigger-popup-term-condition', function() {
    $(this).trigger('dblclick');
    popup2.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('dblclick', '.trigger-popup-term-condition', function() {
    popup2.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.trigger-popup-excess-explained', function() {
    popup1.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
    $(this).trigger('dblclick');
  });
  $(document).on('dblclick', '.trigger-popup-excess-explained',function(){
      popup1.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
    });
  $(document).on('click', '.popup__close', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    event.preventDefault();
  });

	var formEl = $('.accordion__content').find('[data-form-validate]');
	var inputFormEl = formEl.find('input');
	inputFormEl.on('click', function() {
		/* Act on the event */
		if(formEl.length > 0 && !formEl.valid()) {
				$('.button-group').find('#insurance-1').attr('disabled', 'true');
				$('.button-group').find('#insurance-1').addClass('disabled');
		}else{
			$('.button-group').find('#insurance-1').removeAttr('disabled');
			$('.button-group').find('#insurance-1').removeClass('disabled');
		}
	});
	$(document).on('click','.ui-menu-item', function() {
		/* Act on the event */
		if(inputFormEl.valid() === false) {
				$('.button-group').find('#insurance-1').attr('disabled', 'true');
				$('.button-group').find('#insurance-1').addClass('disabled');
		}else{
			$('.button-group').find('#insurance-1').removeAttr('disabled');
			$('.button-group').find('#insurance-1').removeClass('disabled');
		}
	});
	inputFormEl.on('keyup ', function() {
		/* Act on the event */
		if(inputFormEl.valid() === false) {
				$('.button-group').find('#insurance-1').attr('disabled', 'true');
				$('.button-group').find('#insurance-1').addClass('disabled');
		}else{
			$('.button-group').find('#insurance-1').removeAttr('disabled');
			$('.button-group').find('#insurance-1').removeClass('disabled');
		}
	});

	var renderAddonSegments = function() {
		var templateSegments;
		var appendDiv = $('#addons-segments-block');
		var tplUrl = $('body').is('.mp-1-add-ons-page') ? global.config.url.mp1AddonsSegmentsTemplate : global.config.url.mpAddonsSegmentsTemplate;
		var segmentsDetail = function(data1){
				$.get(tplUrl, function (data) {
					var template = window._.template(data, {
						data: data1
					});
					templateSegments = $(template);
					appendDiv.append(templateSegments);
					SIA.initCustomSelect();
					SIA.initTabMenu();
					$('.review-baggage--item [data-tooltip]').kTooltip();

					templateSegments.find('[data-accordion-wrapper]').each(function(){
						var accWrap = $(this);

						accWrap.find('[data-accordion-trigger="1"]').one({
							'click.acctrigger': function(e){

								accWrap.find('[data-accordion-trigger="2"]').each(function(id){
									var trigger = $(this);
									if(id == 0) {
										if(!trigger.hasClass('active')) trigger.trigger('click');
									}else {
										if(trigger.hasClass('active')) trigger.trigger('click');
									}
								});
							}
						});
					});

					$('.popup--flight-addon-term-condition .popup__close').on({
						'click.focus': function(){
							$('#btn-to-pay').focus();
						}
					});
				});
			};
		var url;
		if($('body').hasClass('mp-addons-piece-page')) {
			url = global.config.url.mpAddonsSegmentsPieceJSON;
		}else {
			url = global.config.url.mpAddonsSegmentsJSON
		}
		$.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
      	var data2 = reponse;
        segmentsDetail(data2);
      }
		});
	};
	if($('body').hasClass('mp-add-ons-page')){
		renderAddonSegments();
		SIA.BspAddons.init();
	}
};

SIA.BspAddons = function() {
	var p = {};

	p.body = SIA.global.body;
	p.bspMain = $('[data-booking-summary-panel]');

	p.bsptpl = '<div data-addons-details class="bsp-flights-cost bsp-<%= type %>-addons">\
	  <span class="bsp-flights-cost-title"><span class="text-left"><%= title %> ADD-ONS</span></span>\
	  <ul class="bsp-flights-cost__details">\
	   	<li class="sub-total"><span>Subtotal</span><span class="price"></span></li>\
	  </ul>\
	</div>';
	p.bspPassNameTpl = '<li data-passenger-name="<%= passengerName %>"><span><%= passengerName %></span><br></li>';
	p.bspFlightsItemTpl = '<span style="float:left;" class="bsp-addons__details" data-addonsType="<%= addonsType %>">\
		<%= addonSelType %> - <%= addonItem %> for <%= originDest %>\
	</span>\
	<span class="price" data-price data-addonsType="<%= addonsType %>"><%= price %></span>';
	p.bspTripItemTpl = '<li data-addonsType="<%= addonsType %>">\
		<span><%= tripType %></span>\
		<span data-price class="price"><%= price %></span>\
	</li>';
	p.bspGrandPrice = '<div class="bsp-flights-cost" data-grandtotal>\
		<ul class="bsp-flights-cost__details bsp-grand-total">\
			<li class="sub-total"><span class="grand-total__title">Grand total</span><span class="price"></span></li>\
		</ul>\
	</div>';

	var init = function() {
		addonSelects();
	};

	var addonSelects = function() {
		var flightSelClss = [
			'.for-your-flight-block .your-flight-item .select-price input, ',
			'.for-your-flight-block .your-flight-item .select-price-button input, ',
			'.for-your-flight-block .your-flight-item [data-customselect] select',
		];
		var tripSelClss = [
			'.your-flight-item.addon-item .travel-location input ,',
			'.your-flight-item.addon-item .hotel-list .btn-group input, ',
			'.your-flight-item.addon-item .list-cars-result .price-car input'
		];

		$(document).on('click', tripSelClss[0] + tripSelClss[1] + tripSelClss[2], function() {
			var self = $(this);

			initBspAddons('trip');

			var inputAttr = self.attr('value');
			var addonsType = getAddonsSelType(self);

			if (inputAttr == 'Add' || inputAttr.toLowerCase().search('select') >= 0) {
				addTripsData(self, addonsType);
			} else if (inputAttr.toLowerCase().search('remove') >= 0 || inputAttr == 'SELECTED') {
				removeTripsData(cleanRefId(addonsType));
				removeBspAddons('trip');
			}

			computeSubTotal('bsp-trip-addons');
			computeGrandTotal();
		});

		$(document).on('click blur', flightSelClss[0] + flightSelClss[1] + flightSelClss[2], function() {
			var self = $(this);

			initBspAddons('flights');

			var inputAttr = self.attr('value');
			var passName = getPassName(self);
			var addonSelType = getAddonsSelType(self);
			var originDestCode = getOriginDestCode(self, addonSelType);

			addPassengerName(passName);

			if (inputAttr == 'Select' || self.is('select')) {
				addFlightsData($(this), passName, originDestCode, addonSelType);
			}

			if(inputAttr == 'Selected' || self.val() == '0') {
				removeFlightsData(passName, originDestCode.addonsNameType);
				removeBspAddons('flights');
				removePassengerName(passName);
			}

			computeSubTotal('bsp-flights-addons');
			computeGrandTotal();
		});
	};

	var initBspAddons = function(bspAddonType) {
		if (p.bspMain.find('.bsp-' + bspAddonType + '-addons').length == 0){
			var addons = [
				{
					'type': 'flights',
					'title': 'FLIGHT'
				},
				{
					'type': 'trip',
					'title': 'TRIP'
				}
			];

			if (p.bspMain.length >= 1) {
				var bspTotalFareEl = p.bspMain.find('.bsp-total-fare .cta-group');
				for(var i = 0; i < 2; i++) {
					var data = addons[i];

					if (data.type == bspAddonType) {
						var bspEl = $(_.template(p.bsptpl, {
							type: data.type,
							title : data.title
						}));

						bspTotalFareEl.before(bspEl);
					}
				}

				p.bspMain.find('.bsp-total-fare [data-grandtotal]').remove();
				bspTotalFareEl.before($(p.bspGrandPrice));
			}
		}
	};

	var addPassengerName = function(passName) {
		var flightAddon = p.bspMain.find('.bsp-flights-addons');
		if (flightAddon.find('[data-passenger-name="' + passName + '"]').length == 0) {
			flightAddon.find('.sub-total').before( $(_.template( p.bspPassNameTpl, {
				passengerName: passName
			})) );
		}
	};

	var removePassengerName = function(passName) {
		var flightAddon = p.bspMain.find('.bsp-flights-addons').find('[data-passenger-name="' + passName + '"]');

		if (flightAddon.find('[data-addonstype]').length == 0) {
			flightAddon.remove();
		}
	};

	var removeBspAddons = function(bspAddonType) {
		var sel = '.bsp-' + bspAddonType + '-addons';
		if (p.bspMain.find(sel + ' [data-addonsType]').length <= 0){
			p.bspMain.find(sel).remove();
		}
	};

	var addFlightsData = function(self, passName, originDestCode, addonSelType) {
		var parentSelf = self.parent();

		var el = p.bspMain.find('.bsp-flights-addons').find('[data-passenger-name="' + passName  + '"]');
		el.find('[data-addonsType="' + originDestCode.addonsNameType + '"]').remove();

		el.append( $(_.template(p.bspFlightsItemTpl, {
			addonsType: originDestCode.addonsNameType,
			addonSelType: addonSelType,
			addonItem: getAddonItem(parentSelf),
			price: getAddonSelPrice(parentSelf),
			originDest: originDestCode.originDestCode
		})) );
	};

	var addTripsData = function(self, addonsType) {
		var refId = cleanRefId(addonsType);
		var el = p.bspMain.find('.bsp-trip-addons');
		el.find('[data-addonsType="' + refId + '"]').remove();

		el.find('.bsp-flights-cost__details .sub-total').before($(_.template(p.bspTripItemTpl, {
			addonsType: refId,
			tripType: addonsType,
			price: getAddonSelPrice(self)
		})));
	};

	var removeFlightsData = function(passName, addonsNameType) {
		p.bspMain.find('.bsp-flights-addons')
			.find('[data-passenger-name="' + passName  + '"]')
			.find('[data-addonsType="' + addonsNameType + '"]').remove();
	};

	var removeTripsData = function(addonsNameType) {
		p.bspMain.find('.bsp-trip-addons').find('[data-addonsType="' + addonsNameType + '"]').remove();
	};

	var getOriginDestCode = function(el, addonSelType) {
		var originCode = el.attr('name').split('-')[2];
		var originDestCode = '';
		var addonsNameType = '';
		var toFrom = p.bspMain.find('.bsp-booking-summary__heading-wrap .bsp-flight.hidden-mb-small').html()
			.split('<em class="ico-return-arrows"></em>');

		if (originCode == toFrom[0].split(' - ')[0]){
			addonsNameType += cleanRefId(addonSelType + '-' + 1 + '-' + getItemType(el));
			originDestCode = toFrom[0];
		} else if (originCode == toFrom[1].split(' - ')[0]) {
			addonsNameType += cleanRefId(addonSelType.trim() + '-' + 2 + '-' + getItemType(el));
			originDestCode = toFrom[1];
		}

		return {
			originDestCode: originDestCode,
			addonsNameType: addonsNameType
		};
	};

	var getItemType = function(el) {
		var inputAttr = el.attr('name');
		if (inputAttr.search('bundle') >= 0) {
			return 'bundle';
		} else if (inputAttr.search('baggage') >= 0) {
			return 'baggage';
		} else if (el.attr('name').search('add-weight') >= 0) {
			return 'add-weight';
		}
	};

	var cleanRefId = function(str) {
		var newRefId = str;
		return newRefId.trim().toLowerCase().replace(' ', '-').replace('\n', '');
	}

	var getAddonsSelType = function(el) {
		var parentBtn = el.parent();

		if (parentBtn.hasClass('select-price')) {
			return parentBtn.prev().find('li').eq(0).text();
		} else if (parentBtn.hasClass('select-price-button')) {
			return parentBtn.closest('.preferred-flight-item').find('.text-item').html();
		} else if (parentBtn.hasClass('custom-select')) {
			return parentBtn.closest('.weight-baggage').find('.text-item strong').html();
		} else if (el.closest('.travel-location').length >= 1) {
			return 'Travel Insurance';
		} else if (el.attr('name').search('room') >= 0) {
			return 'Hotel';
		} else if (el.attr('name').search('car') >= 0) {
			return 'Car rental';
		}
	};

	var getPassName = function(el) {
		var parentBtn = el.parent();
		var passName = '';

		if (parentBtn.hasClass('select-price')) {
			passName = parentBtn.closest('.bundle-flight-block').find('.title-5--blue').html().split(' ');
		} else if (parentBtn.hasClass('select-price-button') || parentBtn.hasClass('custom-select')) {
			passName = parentBtn.closest('.preferred-flight-item').find('.title-5--blue').html().split(' ');
		}

		return passName[1] + ' ' + passName[2];
	};

	var getAddonSelPrice = function(el) {
		if (el.hasClass('select-price')) {
			return el.find('.sgd-price').html().split(' ')[1];
		} else if (el.hasClass('select-price-button')) {
			return el.prev().find('.sgd-price').html().split(' ')[1];
		} else if (el.closest('.weight-baggage').length >= 1) {
			var price = el.find('select').find('[value="' + el.find('select').val() + '"]').html();
			return price.slice(price.indexOf('$') + 1, price.indexOf(')'))
		} else if (el.closest('.travel-location').length >= 1) {
			var price = el.parent().prev().find('.sgd-price').html();
			return price.slice(price.indexOf(' ') + 1);
		} else if (el.attr('name').search('add-room') >= 0) {
			var price = el.closest('td').prev().html();
			return price.slice(price.indexOf(' ') + 1);
		} else if (el.attr('name').search('car') >= 0) {
			var price = el.closest('.price-car').find('.sub-heading-2--blue').html();
			return price.slice(price.indexOf(' ') + 1);
		}
	};

	var getAddonItem = function(el) {
		if (el.hasClass('select-price')) {
			return el.parent().find('.title-5--blue').text();
		} else if (el.hasClass('select-price-button')) {
			return '';
		} else if (el.find('select').attr('name').search('add-weight') >= 0) {
			var addKilo = el.find('select').find('[value="' + el.find('select').val() + '"]').html();
			return addKilo.slice(0, addKilo.indexOf(' ('));
		}
	};

	var computeSubTotal = function(addonType) {
		var parent = p.bspMain.find('.' + addonType);
		var prices = parent.find('[data-price]');
		var subTotal = 0;

		if (prices.length >= 1) {
			for (var i = 0, iLen = prices.length; i < iLen; i++) {
				subTotal += parseFloat($(prices[i]).html());
			}

			parent.find('.sub-total .price').html(subTotal.toFixed(2));
		}
	};

	var computeGrandTotal = function() {
		var flightsPrice = $('[data-booking-summary-panel] .flights__info').next().find('.sub-total .price');
		var subTotalPrices = p.bspMain.find('[data-addons-details] .sub-total .price');
		var grandTotalEl = p.bspMain.find('[data-grandtotal] .price');
		var allSubPrices = $.merge(flightsPrice, subTotalPrices);

		if  (subTotalPrices.length >= 1) {
			var grandTotal = 0;

			for (var i = 0, iLen = allSubPrices.length; i < iLen; i++) {
				var subTotalEl = $(allSubPrices[i]).html();
				if (subTotalEl != ''){
					grandTotal += parseFloat(subTotalEl);
				}
			}

			grandTotalEl.html('SGD ' + grandTotal.toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2}));
		} else {
			grandTotalEl.parent().remove();
		}
	};

	return {
		init: init,
		p: p
	};
}();
