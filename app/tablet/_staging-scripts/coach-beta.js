/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.coachBeta = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var body = $('body');
	var coachs = $('.coach-popup');
	var closeBtn = $('[data-close-coach]');
	var nextBtn = $('[data-proceed]');
	var overlay = $('.overlay-beta');
	var overlayInner = overlay.find('.overlay-beta__inner');
	var freezeFlag = false;

	var isStickyOpen = function() {
		var sticky = $('.sticky');
		if (!sticky.length) {
			return false;
		}
		return !sticky.hasClass('minibar');
	};

	var triggerToggleSticky = function() {
		var sticky = $('.sticky');
		var toggleBtn = sticky.find('.mini-beta-link');
		toggleBtn.trigger('click');
	};

	var showFirstCoach = function() {
		showCoach($('.coach-1'));
	};

	var freezeBody = function() {
		body.addClass('no-flow');
		freezeFlag = true;
	};

	var deFreeze = function() {
		if(freezeFlag){
			body.removeClass('no-flow');
			freezeFlag = false;
		}
	};

	// var handleScroll = function(){
	// 	var sticky = $('.sticky');
	// 	var h = overlayInner.height();
	// 	overlay.css('overflow-y', 'auto');
	// 	overlayInner.height(h + sticky.height());
	// };

	var showOverlay = function() {
		overlay.removeClass('hidden');
		var isFreeze = body.hasClass('no-flow');
		if(!isFreeze){
			freezeBody();
		}
		setTimeout(function() {
			overlay.addClass('active');
			if (!isStickyOpen()) {
				triggerToggleSticky();
			}
		}, 0);
		// handleScroll();
	};

	var hideOverlay = function() {
		var currentCoach = getCurrentCoach();
		currentCoach.removeClass('active');
		deFreeze();
		overlay.removeClass('active');
		if (isStickyOpen()) {
			triggerToggleSticky();
		}
		setTimeout(function() {
			currentCoach.addClass('hidden');
			overlay.addClass('hidden');
		}, 700);
	};

	var getCurrentCoach = function() {
		return coachs.filter('.active');
	};

	var showCoach = function(coach) {
		var currentCoach = getCurrentCoach();
		currentCoach.addClass('hidden').removeClass('active');
		coach.removeClass('hidden');
		if (window.innerWidth < 768 && window.innerWidth >= 480) {
			if (coach.hasClass('coach-3') || coach.hasClass('coach-4')) {
				overlayInner.addClass('search-flight-coach');
			} else {
				if (overlayInner.hasClass('search-flight-coach')) {
					overlayInner.removeClass('search-flight-coach');
				}
			}
		}
		setTimeout(function() {
			coach.addClass('active');
		}, 0);
	};

	win.off('resize.changeBg').on('resize.changeBg', function() {
		// overlayInner.css('height', '');
		// handleScroll();
		var currentCoach = getCurrentCoach();
		if (currentCoach.hasClass('coach-3') || currentCoach.hasClass('coach-4')) {
			if (window.innerWidth < 768 && window.innerWidth >= 480) {
				if (!overlayInner.hasClass('search-flight-coach')) {
					overlayInner.addClass('search-flight-coach');
				}
			}
			else {
				if (overlayInner.hasClass('search-flight-coach')) {
					overlayInner.removeClass('search-flight-coach');
				}
			}
		}
	});

	body.on('click.coachBeta', '.what-new-btn', function(e) {
		e.preventDefault();
		if($(this).hasClass('disabled')){
			return;
		}
		$(this).addClass('disabled');
		showOverlay();
		showFirstCoach();
	});

	closeBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		hideOverlay();
		$('.what-new-btn').removeClass('disabled');
	});

	nextBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		var btn = $(this);
		var nextCoach = $(btn.data('next-btn'));
		showCoach(nextCoach);
	});
};
