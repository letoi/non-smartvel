/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.sticky = function() {
	var global = SIA.global;
	var body = $('body');
	var timer = null;
	var sticky = null;
	var toggleBtn = null;
	var minibarTpl = '<em class="arrow-up"></em>BETA';
	var nonMinibarTpl = '<em class="arrow-right"></em>BETA.Singaporeair.com';

	var appendTemplate = function() {
		$.get(global.config.url.stickyTemplate, function(data) {
			var template = window._.template(data, {
				data: {}
			});
			var templateEl = $(template);
			body.append(templateEl);
			sticky = $('.sticky');
			toggleBtn = sticky.find('.mini-beta-link');
			sticky.addClass('minibar');
			toggleBtn.html(minibarTpl);
			toggleBtn.off('click.additionBeta').on('click.additionBeta', function(e) {
				e.preventDefault();
				toggleSticky();
			});
		});
	};

	var isOpen = function() {
		if (!sticky.length) {
			return false;
		}
		return !sticky.hasClass('minibar');
	};

	var toggleSticky = function() {
		if($('.overlay-beta').hasClass('active')){
			return;
		}
		if (isOpen()) {
			clearTimeout(timer);
			sticky.addClass('minibar');
			toggleBtn.html(minibarTpl);
		} else {
			sticky.removeClass('minibar');
			toggleBtn.html(nonMinibarTpl);
			autoHideSticky();
		}
	};

	var autoHideSticky = function() {
		clearTimeout(timer);
		timer = setTimeout(function() {
			if (!isOpen() || $('.overlay-beta').hasClass('active')) {
				return;
			}
			sticky.addClass('minibar');
			toggleBtn.html(minibarTpl);
		}, 5000);
	};

	var initModule = function() {
		appendTemplate();
	};

	initModule();
};
