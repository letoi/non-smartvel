/**
 * @name SIA
 * @description Define global flight economy functions
 * @version 1.0
 */

SIA.flightEconomy = function(){
  var global = SIA.global,
      config = global.config,
      body =$('body'),
      container = $('#container'),
      slider = $('[data-fs-slider]'),
      wrapperSlides = slider.find('.slides'),
      btnPrev = slider.find('.btn-prev'),
      btnNext = slider.find('.btn-next'),
      isChooseDate = null,
      priceDateOutbound = 0,
      clickedBtn,
      miniFareData;
  var slideshowpremiumeconomy = function(){
    if($('body').hasClass('fs-economy')){
      var totalDesktopSlide = 1,
      totalLandscapeSlide = 1;
      $('[data-slideshow-premium-economy]').each(function() {
        var slider = $(this);
        slider.find('img').each(function() {
          var self = $(this),
              newImg = new Image();
          newImg.onload = function() {
            slider.css('visibility', 'visible');
            slider.find('.slides').slick({
              siaCustomisations: true,
              dots: false,
              speed: 300,
              draggable: true,
              slidesToShow: totalDesktopSlide,
              slidesToScroll: totalLandscapeSlide,
              accessibility: false,
              arrows: true,
            });
          };
          newImg.src = self.attr('src');
        });
      });
    }
  };
  var initSlider = function(sliderEl, btnNext, btnPrev) {
    var wrapperSlides = $(sliderEl).find('.slides');

    if(wrapperSlides.is('.slick-initialized')) {
      wrapperSlides.slick('unslick');
    }

    wrapperSlides.slick({
      dots: false,
      draggable: false,
      infinite: false,
      speed: 300,
      slidesToShow: 5,
      slidesToScroll: 5,
      prevArrow: btnPrev,
      nextArrow: btnNext,
    })

    selectItemSlider(wrapperSlides);

    if(!sliderEl.data('slider-outbound')) {
      sliderEl.find('.slide-item').each(function(){
        var selfPrice = parseFloat($(this).find('.large-price').text()),
            priceAfterSelect = selfPrice - priceDateOutbound;

        $(this).find('.large-price').text('+ ' + (priceAfterSelect > 0 ? priceAfterSelect : 0));
      })
    }
  }

  var selectItemSlider = function(wrapperSlides) {
    var slideItem = wrapperSlides.find('.slick-slide');
    slideItem.each(function(){
      if (isChooseDate) {
        $(this).data('date') === isChooseDate && $(this).addClass('selected');
      }
      $(this).off('click.selectSlideItem').on('click.selectSlideItem', function(e){
        e.preventDefault();
        var isOutBound = $(this).closest('[data-fs-slider]').data('slider-outbound');
        isOutBound && $('[data-fs-slider]').attr('data-selected-date', $(this).data('date'));
        slideItem.removeClass('selected');
        $(this).addClass('selected');
        isChooseDate = $(this).data('date');

        if (isOutBound) {
          priceDateOutbound = parseFloat($(this).find('.large-price').text());
        }
      });
    })
  }

  var renderPopupBenefit = function(popupEl, callback) {
    $.get(global.config.url.fsEconomyBenefitTemplate, function(data) {
      var content = popupEl.find('.popup__content');
      var template = window._.template(data, {
        data: data
      });
      $(template).prependTo(content.empty());
      // reinit js
      SIA.initTabMenu();
      SIA.multiTabsWithLongText()

      if (callback) {
        callback();
      }
    });
  }

  var renderPopupPremiumBenefit = function(popupEl, callback) {
    $.get(global.config.url.fsEconomyPremiumBenefitTemplate, function(data) {
      var content = popupEl.find('.popup__content');
      var template = window._.template(data, {
        data: data
      });
      $(template).prependTo(content.empty());
      // reinit js
      SIA.initTabMenu();
      SIA.multiTabsWithLongText()

      if (callback) {
        callback();
      }
    });
  }

  var initPopup = function(data) {
    var triggerPopup = $('[data-trigger-popup]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',

          afterHide: function(){
            container.css('padding-right', '');
            body.css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
              if (ua.indexOf('chrome') > -1) { }
              else {
                body.attr('style', function(i, s) {
                  if(s !== undefined) {
                    return s.replace('overflow:hidden !important;','');
                  }
                });
              }
            }
          }
        });
      }
      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        var jsonURL = self.data('flight-json-url');
        if (!self.hasClass('disabled')) {
          // render popup benefit detail
          if (self.data('trigger-popup') === '.popup-view-benefit--krisflyer') {
            renderPopupBenefit(popup, function(){
              popup.Popup('show');
              $(window).trigger('resize');
            });
          } else if(self.data('trigger-popup') === '.popup--oal') {
           popup.Popup('show');
           $(window).trigger('resize');
          } else if (self.data('trigger-popup') === '.popup-view-partner-airlines') {
            popup.Popup('show');
          } else if (self.data('trigger-popup') === '.popup-view-premium-benefit--krisflyer') {
            renderPopupPremiumBenefit(popup, function(){
              popup.Popup('show');
              $(window).trigger('resize');
            });
          }
        }
      });

    });
  };

  var selectFlightAnimation = function() {
    var triggerAnimation = $('[data-trigger-animation]');
    triggerAnimation.removeAttr('data-trigger-popup');

    triggerAnimation.each(function(){
      $(this).off('click.toggleAnimation').on('click.toggleAnimation', function(){
        var parentRecommended = $(this).parent(),
            flightItem = parentRecommended.find('[data-flight-item]'),
            wrapFlight = flightItem.find('[data-wrap-flight]'),
            btnMore = flightItem.find('[data-more-details-table]'),
            flightStation = flightItem.find('.flight-station'),
            hiddenRecommended = parentRecommended.siblings('[data-hidden-recommended]'),
            hiddenRecommended1 = parentRecommended.siblings('[data-hidden-recommended-1]'),
            isOpen = $(this).is('.active'),
            hiddenRecommendedHeight = hiddenRecommended.data('fare-block-height');
        // reset animation
        if (wrapFlight) {
          flightItem.removeClass('active');
          wrapFlight.css({
            'height': 0,
            '-webkit-transform' : 'translate3d(0)',
            '-moz-transform'    : 'translate3d(0)',
            '-ms-transform'     : 'translate3d(0)',
            '-o-transform'      : 'translate3d(0)',
            'transform'         : 'translate3d(0)'
          })
        }
        parentRecommended.find('[data-trigger-animation]').removeClass('active')
        hiddenRecommended.removeClass('active economy-flight--green , business-flight--blue');
        hiddenRecommended1.removeClass('active economy-flight--pey , business-flight--red');
        var recommendedFlight = $(this).closest('.flight-list-item').find('.flight-station-item').find('.inner-info');
        recommendedFlight.find('.cabin-color').addClass('hidden');
        // hiddenRecommended.css({
        //   'height' : 0 + 'px'
        // });
        // start animation
        if (!isOpen) {
          if(!$(this).hasClass('not-available')){
            btnMore.trigger('click.showMore');
            $(this).addClass('active');
            // hiddenRecommended.css({
            //   'height' : 729 - 50 + 'px'
            // });
            if($(this).hasClass('column-trigger-animation')){
              var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
              if(!$(body).hasClass('fs-business')){
                hiddenRecommended.addClass('active economy-flight--green');
              } else {
                hiddenRecommended.addClass('active business-flight--blue');
              }
              recommendedFlightItem.find('[data-col-cabinclass="'+$(this).data('target-cabinclass')+'"]').removeClass('hidden');
              recommendedFlightItem.find('[data-col-cabinclass="'+$(this).prev().data('target-cabinclass')+'"]').addClass('hidden');
            }
            if($(this).hasClass('column-trigger-animation-1')){
              var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
              if(!$(body).hasClass('fs-business')){
                hiddenRecommended1.addClass('active economy-flight--pey');
              } else {
                hiddenRecommended1.addClass('active business-flight--red');
              }

              recommendedFlightItem.find('[data-col-cabinclass="'+$(this).data('target-cabinclass')+'"]').removeClass('hidden');
              recommendedFlightItem.find('[data-col-cabinclass="'+$(this).next().data('target-cabinclass')+'"]').addClass('hidden');
            }
          }
        }
      });
    });
  }

  var getTemplateCarousel = function(sliderEl, btnNext, btnPrev, daysData, callback) {
    $.get(global.config.url.fsSevenDayFareTemplate, function(data) {
      var slides = sliderEl.find('.slides');
      var template = window._.template(data, {
        data: daysData
      });
      slides.empty().append($(template));
      initSlider(sliderEl, btnNext, btnPrev);

      if(callback) {
        callback();
      }
    });
  }

  var loadCarouselSevenDay = function(sliderEl, btnNext, btnPrev, isNext) {
    $.get(global.config.url.fsSevenDayFareJson, function(data){
      var initDays = data.response.byDay,
          currDate = sliderEl.data('current-date'),
          currDateIdx = 0,
          slideToIdx = 0,
          currDateIdx1 = 5;

      _.map(initDays, function(day, idx){
        if(day.month === currDate) {
          currDateIdx = idx;
        }
        return false;
      })
      if (typeof isNext !== 'undefined') {
        currDateIdx = isNext ? currDateIdx + 5 : currDateIdx - 5;
      }

      currDateIdx = currDateIdx < 0 ? 0 : currDateIdx;

      currDateIdx1 = currDateIdx;

      if(initDays.length - currDateIdx < 5) {
        currDateIdx = currDateIdx - (5 - (initDays.length - currDateIdx));
      }


      days = initDays.slice(currDateIdx, currDateIdx + 5);
      getTemplateCarousel(sliderEl, btnNext, btnPrev, days, function() {
        if (typeof isNext !== 'undefined') {
          sliderEl.data('current-date', initDays[currDateIdx].month);
        } else {
          $('.slick-slide.slick-active').each(function(){
            if($(this).data('date') === sliderEl.attr('data-selected-date') && !$(this).is('.selected')) {
              $(this).trigger('click.selectSlideItem');
            }
          })
        }
        btnNext && initDays.length - currDateIdx1 < 5 && currDateIdx !== 0 ? btnNext.attr('disabled', true) : btnNext.attr('disabled', false);
        btnPrev && currDateIdx === 0 ? btnPrev.attr('disabled', true) : btnPrev.attr('disabled', false);
      });
    })
  }

  var handleActionSlider = function() {
    var slider = $('[data-fs-slider]');
    // init
    slider.each(function(){
      var self = $(this);
          btnPrev = $(this).find('.btn-prev'),
          btnNext = $(this).find('.btn-next');

      loadCarouselSevenDay($(this), btnNext, btnPrev);

      btnPrev.off('click.slidePrev').on('click.slidePrev', function(){
        loadCarouselSevenDay(self, $(this).siblings('.btn-next'), $(this), false);
      });

      btnNext.off('click.slideNext').on('click.slideNext', function(){
        loadCarouselSevenDay(self, $(this), $(this).siblings('.btn-prev'), true);
      });

    });

  }

  var showMoreLessDetails = function() {
    var btnMore = $('[data-more-details-table]'),
        btnLess = $('[data-less-details-table]'),
        triggerAnimation = $('[data-trigger-animation]'),
        wrapFlights = $('[data-wrap-flight]'),
        fareBlocks = $('[data-hidden-recommended]');

    // set data-height for wrap flight
    wrapFlights.each(function(){
      var flightLegItem = $(this).find('.flight-result-leg'),
          wrapFlightHeight = 0;

      flightLegItem.each(function(){
        wrapFlightHeight += $(this).outerHeight();
      });

      $(this).attr('data-wrap-flight-height', wrapFlightHeight);
    });

    btnMore.each(function(){
      $(this).off('click.showMore').on('click.showMore', function(e){
        e.preventDefault();
        var flightItem = $(this).closest('[data-flight-item]'),
            controlFlight = flightItem.find('.control-flight-station'),
            wrapFlight = flightItem.find('[data-wrap-flight]');
            controlFlightHeight = wrapFlight.siblings('.control-flight-station').outerHeight() || 84;
        wrapFlightHeight = wrapFlight.data('wrap-flight-height');

        if (wrapFlight) {
          flightItem.addClass('active');
          wrapFlight.css({
            'height': wrapFlightHeight - controlFlightHeight + 'px',
            '-webkit-transform' : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-moz-transform'    : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-ms-transform'     : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            '-o-transform'      : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)',
            'transform'         : 'translate3d(0px, -'+ controlFlightHeight +'px, 0)'
          })
        }
      });
    });

    btnLess.each(function(){
      $(this).off('click.showLess').on('click.showLess', function(e){
        e.preventDefault();
        var flightItem = $(this).closest('[data-flight-item]'),
            controlFlight = flightItem.find('.control-flight-station'),
            wrapFlight = flightItem.find('[data-wrap-flight]');

        if (wrapFlight) {
          flightItem.removeClass('active');
          wrapFlight.css({
            'height': 0,
            '-webkit-transform' : 'translate3d(0)',
            '-moz-transform'    : 'translate3d(0)',
            '-ms-transform'     : 'translate3d(0)',
            '-o-transform'      : 'translate3d(0)',
            'transform'         : 'translate3d(0)'
          })
        }
      })
    });
  }

  var showHideFilters = function() {
    var fsFilters = $('.flight-search-filter-economy');

    fsFilters.each(function(){
      var linkShow = $(this).find('.link-show'),
          linkHide = $(this).find('.link-hide'),
          content = $(this).find('.content');

      linkShow.off('click').on('click', function(e) {
        e.preventDefault();
        if(!$(this).parent().is('.active')) {
          $(this).parent().addClass('active')
          linkShow.hide();
          content.fadeIn();
        }
      });
      linkHide.off('click', 'a').on('click', 'a', function(e) {
        e.preventDefault();
        content.hide();
        linkShow.fadeIn();
        $(this).closest('.flight-search-filter-economy').removeClass('active');
      });
    })
  };

  var formatTimeToHour = function(seconds) {
    return parseFloat(seconds / 3600).toFixed(2) + 'hr';
  }

  var formatTimeToDate = function(seconds) {
    var dateObj = new Date(seconds),
        date = dateObj.getDate(),
        month = dateObj.getMonth() + 1,
        hour = dateObj.getHours(),
        minute = dateObj.getMinutes();

    return hour + ':' + minute;
  }

  var sliderRange = function(){
    var rangeSlider = $('[data-range-slider]');

    rangeSlider.each(function(){
      var min = $(this).data('min'),
          max = $(this).data('max'),
          step = $(this).data('step'),
          unit = $(this).data('unit'),
          type = $(this).data('range-slider');
          labelFrom = '<span class="slider-from '+ type +'"></span',
          labelTo = '<span class="slider-to '+ type +'"></span',

      $(this).slider({
        range: true,
        min: min,
        max: max,
        step: step,
        values: [min, max],
        create: function() {
          var slider = $(this),
              leftLabel,
              rightLabel;

          switch( type ) {
            case "tripDuration": case "layover":
              leftLabel = $(labelFrom).text(formatTimeToHour(min));
              rightLabel = $(labelTo).text(formatTimeToHour(max));
              break;
            case "departure": case "arrival":
              leftLabel = $(labelFrom).text(formatTimeToDate(min));
              rightLabel = $(labelTo).text(formatTimeToDate(max));
              break;
            default:
              break;
          }

          $(this).append(leftLabel);
          $(this).append(rightLabel);
        },
        slide: function(event, ui) {
          var Label;

          switch( type ) {
            case "tripDuration": case "layover":
              $(this).find('.slider-from').text(formatTimeToHour(ui.values[0]));
              $(this).find('.slider-to').text(formatTimeToHour(ui.values[1]));
              break;
            case "departure": case "arrival":
              $(this).find('.slider-from').text(formatTimeToDate(ui.values[0]));
              $(this).find('.slider-to').text(formatTimeToDate(ui.values[1]));
              break;
            default:
              break;
          }
        }
      })

      if(type === 'tripDuration') {
       $(this).find('.ui-slider-handle').eq(0).remove();
       $(this).find('.slider-from').remove();
      }

    });
  }

 var countLayover = function(segment) {
   var countLayover = 0;

   _.map(segment.legs, function(leg){
     leg.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;

     leg.stops.length && _.map(leg.stops, function(stop){
       stop.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;
     })
   })

   return countLayover;
 }

 var getFilterData = function(flightsData){
   var arr = [];

   _.map(flightsData, function(flight, flightIdx){
     var obj = {
       "nonStop" : false,
       "oneStop": false,
       "twoStop": false,
       "codeShare": false
     };


     var tripDuration = _.sortBy(flight.segments, function(segment){
         return segment.tripDuration;
     });

     var departure = _.sortBy(flight.segments, function(segment){
         return new Date(segment.departureDateTime.replace(/-/g, '/')).getTime();
     });

     var arrival = _.sortBy(flight.segments, function(segment){
         return new Date(segment.arrivalDateTime.replace(/-/g, '/')).getTime();
     });

     var layover = _.sortBy(flight.segments, function(segment){
         var totalLayover = 0;

         _.map(segment.legs, function(leg, legIdx){
           totalLayover += leg.layoverDuration;
         })
         segment['totalLayover'] = totalLayover
         return totalLayover
     });

     _.map(flight.segments, function(segment){
       var count = countLayover(segment);

       !segment.legs.length && (obj.nonStop = true);
       switch(count) {
         case 0:
           !obj.nonStop && (obj.nonStop = true);
           break;
         case 1:
           !obj.oneStop && (obj.oneStop = true);
           break;
         case 2:
           !obj.twoStop && (obj.twoStop = true);
           break;
         default:
           break;
       }

       segment.legs.length && _.map(segment.legs, function(leg){
         if(typeof leg.codeShareFlight === "boolean") {
           leg.codeShareFlight && (obj.codeShare = true);
         }
       })
     })


     obj['minTripDuration'] = tripDuration[0].tripDuration;
     obj['maxTripDuration'] = tripDuration[tripDuration.length - 1].tripDuration;
     obj['minDeparture'] = new Date(departure[0].departureDateTime.replace(/-/g, '/')).getTime();
     obj['maxDeparture'] = new Date(departure[departure.length - 1].departureDateTime.replace(/-/g, '/')).getTime();
     obj['minArrival'] = new Date(arrival[0].arrivalDateTime.replace(/-/g, '/')).getTime();
     obj['maxArrival'] = new Date(arrival[arrival.length - 1].arrivalDateTime.replace(/-/g, '/')).getTime();
     obj['minLayover'] = layover[0].totalLayover;
     obj['maxLayover'] = layover[layover.length - 1].totalLayover;

     arr.push(obj);
   })

   return arr;
 }

 var filterFlights = function(data){
   var filterBlock = $('[data-flight-filter]'),
       flightsArr = data.flights;

   filterBlock.each(function(){
     var flightsBlock = $(this).siblings('.recommended-flight-block'),
         flightIdx = $(this).data('flightFilter'),
         nonStopCheckbox = $('input[name="non-stop-'+ flightIdx +'"]'),
         oneStopCheckbox = $('input[name="one-stop-'+ flightIdx +'"]'),
         twoStopCheckbox = $('input[name="two-stop-'+ flightIdx +'"]'),
         codeShareCheckbox = $('input[name="codeshare-'+ flightIdx +'"]'),
         saGroupCheckbox = $('input[name="sa-group-'+ flightIdx +'"]'),
         sliderTripDuration = $(this).find('[data-range-slider="tripDuration"]'),
         sliderDeparture = $(this).find('[data-range-slider="departure"]'),
         sliderArrival = $(this).find('[data-range-slider="arrival"]'),
         filterObj = {
           "stopover" : {
             "nonstopVal": true,
             "onestopVal": true,
             "twostopVal": true
           },
           "operating" : {
             "codeshareVal": true
           },
           "tripduration": sliderTripDuration.slider("values"),
           "departure": sliderDeparture.slider("values"),
           "arrival": sliderArrival.slider("values")
         };

     // get value from checkbox

     nonStopCheckbox.off('change.getValue').on('change.getValue', function(){
       filterObj.stopover.nonstopVal = $(this).is(':checked');
       handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
     });

     oneStopCheckbox.off('change.getValue').on('change.getValue', function(){
       filterObj.stopover.onestopVal = $(this).is(':checked')
       handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
     });

     twoStopCheckbox.off('change.getValue').on('change.getValue', function(){
       filterObj.stopover.twostopVal = $(this).is(':checked')
       handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
     });

     codeShareCheckbox.off('change.getValue').on('change.getValue', function(){
       filterObj.operating.codeshareVal = $(this).is(':checked')
       handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
     });

     saGroupCheckbox.off('change.resetFilter').on('change.resetFilter', function(){
       filterObj.stopover.nonstopVal = false;
       filterObj.stopover.onestopVal = false;
       filterObj.stopover.twostopVal = false;
       filterObj.operating.codeshareVal = false;
       filterObj.tripduration = [sliderTripDuration.data('min'), sliderTripDuration.data('max')];
       filterObj.departure = [sliderDeparture.data('min'), sliderDeparture.data('max')];
       filterObj.arrival = [sliderArrival.data('min'), sliderArrival.data('max')];
       handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
     });

     // get value from range slider

     sliderTripDuration.slider({
       stop: function(event, ui) {
         filterObj.tripduration = ui.values;
         handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
       }
     });

     sliderDeparture.slider({
       stop: function(event, ui) {
         filterObj.departure = ui.values;
         handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
       }
     });

     sliderArrival.slider({
       stop: function(event, ui) {
         filterObj.arrival = ui.values;
         handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
       }
     });

     // handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);

   });
 }

 var handleFilterFlights = function(el, data, flightData, filterObj, flightIdx) {
   var selfData = [],
       filterDataStopover = [],
       filterDataOperating = [],
       obj = {};

    _.map(filterObj.stopover, function(value, key){
     selfData = [];

     switch(key) {
       case 'nonstopVal':
         value && (selfData = flightData.filter(function(segment){
           var count = countLayover(segment);

           return segment.legs.length === 0 || count === 0;
         }));
         break;
       case 'onestopVal':
         value && (selfData = flightData.filter(function(segment){
           var count = countLayover(segment);

           return count === 1;
         }));
         break;
       case 'twostopVal':
         value && (selfData = flightData.filter(function(segment){
           var count = countLayover(segment);

           return count === 2;
         }));
         break;
       default:
         break;
     }

     filterDataStopover = $.unique([].concat.apply([],[filterDataStopover, selfData]));
   });

   filterDataStopover.length && (flightData = filterDataStopover);

    _.map(filterObj.operating, function(value, key){
     selfData = [];

     _.map(flightData, function(flight){
       flight.legs.length && _.map(flight.legs, function(leg){
         if(key === 'codeshareVal') {
           if(value) {
             typeof leg.codeShareFlight !== 'undefined' && selfData.push(flight);
           } else {
             typeof leg.codeShareFlight === 'undefined' && selfData.push(flight);
           }
           return false;
         }
       })
       return false;
     })

     filterDataOperating = $.unique([].concat.apply([],[filterDataOperating, selfData]));
   });

   filterDataOperating.length && (flightData = filterDataOperating);

   var tripDuration = filterObj.tripduration;

   selfData = flightData.filter(function(flight){
     return flight.tripDuration >= tripDuration[0] &&  flight.tripDuration <= tripDuration[1];
   });

   flightData = selfData;

   var departureDateTime = filterObj.departure;

   selfData = flightData.filter(function(flight){
     return new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() >= departureDateTime[0] &&  new Date(flight.departureDateTime.replace(/-/g, '/')).getTime()  <= departureDateTime[1];
   });

   flightData = selfData;

   var arrivalDateTime = filterObj.arrival;

   selfData = flightData.filter(function(flight){
     return new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() >= arrivalDateTime[0] &&  new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() <= arrivalDateTime[1];
   });

   flightData = selfData;

   obj['segments'] = flightData;

   getTemplateFlightTable(el, data, obj, flightIdx, true);

 }

  var handleLoadmore = function(flightBlock, loadmoreBlock) {
    var flights = flightBlock.find('.flight-list-item'),
        flightsHidden = flightBlock.find('.flight-list-item.hidden');

    flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
    loadmoreBlock.find('[data-total-flight]').text(flights.length);
    loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
    loadmoreBlock.off('click.loadMore').on('click.loadMore', function(e){
      e.preventDefault();
      flightBlock.find('.flight-list-item.hidden').slice(0,5).removeClass('hidden');
      flightsHidden = flightBlock.find('.flight-list-item.hidden');
      loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
      flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
      selectFlightAnimation();
      showMoreLessDetails();
    });
  }

  var labelStatusCheapest = function(){
    var arrPriceCheapest = [];
    var blockWrapFirst =  $('.wrap-content-fs').first();
    var colInfoLeft = blockWrapFirst.find('.col-info-left');
    colInfoLeft.each(function(idexLabel){
      arrPriceCheapest.push($(this).find('.flight-price').find('.price-cheapest-colum').text());
      arrPriceCheapest.sort(function(a, b) {
       return parseFloat(a) - parseFloat(b);
      });
      if($(this).find('.flight-price').find('.price-cheapest-colum').text() === arrPriceCheapest[0]){
        $(this).find('.head-col').append('<span class="label-status anim-all"></span');
      }
    });
  }

  var tableLoadPage = function(isRenderTemplate){
    !isRenderTemplate && setTimeout(function(){
      if($('.main-inner').find('.wrap-content-fs').length == 2){
        $('.wrap-content-fs').last().addClass('hidden');
      };
    }, 500);
    var flightBlockItem = $('.recommended-flight-block').find('.flight-list-item');
    flightBlockItem.each(function(idx){
      var rowSelect = $(this).find('[data-hidden-recommended]').find('.multy-column').find('.row-head-select');
      var rowSelect1 = $(this).find('[data-hidden-recommended-1]').find('.multy-column').find('.row-head-select');
      var rowSelectOneColumn = $(this).find('[data-hidden-recommended]').find('.one-column').find('.row-head-select');
      var rowSelectOneColumn1 = $(this).find('[data-hidden-recommended-1]').find('.one-column').find('.row-head-select');
      colSelectColor = rowSelect.find('.col-select');
      colSelectColor1 = rowSelect1.find('.col-select');
      colSelectColorOneColumn = rowSelectOneColumn.find('.col-select');
      colSelectColorOneColumn1 = rowSelectOneColumn1.find('.col-select');
      $(this).find('.row-head-select').find('.col-select').removeClass('economy-fs--green-1 , economy-fs--green-2 , economy-fs--green-3 , economy-fs--pey-1');
      if($('body').hasClass('fs-business')){
        flightBlockItem.removeClass('economy-flight-bgd').addClass('business-flight-bgd');
        flightBlockItem.find('.column-trigger-animation').addClass('business-flight--blue');
        var cbType = flightBlockItem.find('.column-trigger-animation-1').find('.text-head').html();
        if (cbType == 'Premium Economy') {
          flightBlockItem.addClass('economy-flight-bgd');
          flightBlockItem.find('.column-trigger-animation-1').addClass('economy-flight--pey');
        } else {
          flightBlockItem.find('.column-trigger-animation-1').addClass('business-flight--red');
        }
        colSelectColor.each(function(idx){
          $(this).addClass('business-fs--blue-'+idx);
        });
        colSelectColor1.each(function(idx){
            if (cbType == 'Premium Economy') {
              $(this).addClass('economy-fs--pey-'+idx);
            } else {
              $(this).addClass('business-fs--red-'+idx);
            }
        });

        colSelectColorOneColumn.each(function(idx){
          $(this).addClass('business-fs--blue-1');
        });
        colSelectColorOneColumn1.each(function(idx){
            if (cbType == 'Premium Economy') {
              $(this).addClass('economy-fs--pey-'+idx);
            } else {
              $(this).addClass('business-fs--red-'+idx);
            }
        });

      } else {
        colSelectColor.each(function(idx1){
          $(this).addClass('economy-fs--green-'+idx1);
        });
        colSelectColor1.each(function(idx1){
          $(this).addClass('economy-fs--pey-'+idx1);
        });

        colSelectColorOneColumn.each(function(idx1){
          $(this).addClass('economy-fs--green-1');
        });
        colSelectColorOneColumn1.each(function(idx1){
          $(this).addClass('economy-fs--pey-1');
        });
      }
    });

    var dataRecommended = $('[data-hidden-recommended]');
    var dataRecommended1 = $('[data-hidden-recommended-1]');
    dataRecommended.each(function(idxData){
      var colSelectItem = $(this).find('.select-fare-table.hidden-mb-small').find('.col-select');
      var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
      var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
      var btnPrice = colSelectItem.find('.btn-price');
      if(btnPrice.length > 1){
        var buttonFirst = btnPrice.first();
        buttonFirst.addClass('btn-price-cheapest-select');
       buttonFirst.closest('.col-select').next().find('.btn-price').addClass('btn-price-cheapest-select');
      }
      if(lengthColSelectItem == 2){
        rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
        rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
      }
    });

    dataRecommended1.each(function(idxData){
      var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
      var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
      if(lengthColSelectItem == 2){
        rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
        rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
      }
    });
    labelStatusCheapest();
  }

  var wrapContent, parentFlightList, el, blockParentWrap,
      blockParentColSelect, wrapContents, wrapContentList,
      nameHead, baggage, seatSelection ,earnKrisFlyer, upgrade,
      cancellation1, cancellation2, bookingChange1, bookingChange2, noShow1, noShow2, priceCurrentSelected, idx1, idx2;

  var hiddenBlockClick = function(){
    var selfHeight = el.closest('.flight-list-item').find('.recommended-table [data-wrap-flight-height]').data('wrap-flight-height');
    if (el.closest('.wrap-content-fs').index() === wrapContents.length-1) {
      el = $('.btn-price.active');
      var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
      var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text();
      var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text();
      var listRow = el.closest('.select-fare-table').find('.row-select, .row-head-select');
      var thisCol = el.closest('.col-select');
      var idx = thisCol.index();
      var listContents, colRight, colLeft;
      var noteFare = $('.has-note-fare');
      listRow.each(function() {
        $(this).find('.col-select').eq(idx).addClass('col-selected');
      });
      var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
      var colSelected = listRow.find('.col-select.col-selected');
      var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
      idx2 = colSelected.find('.index-of').text();
      nameHead = colSelected.find('.name-header-hidden').text();
      baggage = colSelected.find('.baggage').text();
      seatSelection = colSelected.find('.seat-selection').text();
      earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
      upgrade = colSelected.find('.upgrade').text();
      cancellation2 = colSelected.find('.cancellation').text();
      bookingChange2 = colSelected.find('.booking-change').text();
      noShow2 = colSelected.find('.no-show').text();
      listContents = summaryGroup.find('.row-select .column-left span, .row-select .column-right span');
      colLeft = summaryGroup.find('.col-select.column-left');
      colRight = summaryGroup.find('.col-select.column-right');
      summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').attr('class', className );
      summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').addClass('border-fare-family');
      summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
      summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
      summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('name',"ttt-"+ eligibleRecommendationIds);
      summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('value',eligibleRecommendationIds);
      colRight.find('.baggage').text(baggage);
      colRight.find('.seat-selection').text(seatSelection);

      // colRight.find('.seat-selection').siblings('.complimentary-note').remove();
      // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
      //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
      //   if(!colRight.find('.seat-selection').siblings('.complimentary-note').length) {
      //     colRight.find('.seat-selection').parent().append(seatSelectionExtra.clone());
      //   }
      // }

      colRight.find('.earn-krisFlyer').text(earnKrisFlyer);

      var upgradeTextSummary = colRight.find('.upgrade');
      upgradeTextSummary.text(upgrade);
      if(colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
        var upgradeText = colSelected.find('.upgrade');
        var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
        if(!upgradeTextSummary.siblings('[data-tooltip]').length) {
          upgradeTextSummary.parent().append(upgradeTooltip.clone());
          upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
        }
      } else {
        upgradeTextSummary.siblings('em').remove();
      }

      // --- Compare 2 fare conditions
      noteFare.show();
      if(idx1 < idx2) {
        colLeft.find('.cancellation').text(cancellation1);
        colLeft.find('.booking-change').text(bookingChange1);
        colLeft.find('.no-show').text(noShow1);
        colRight.find('.cancellation').text(cancellation1 + ' *');
        colRight.find('.booking-change').text(bookingChange1 + ' *');
        colRight.find('.no-show').text(noShow1 + ' *');
      } else if (idx1 > idx2) {
        colLeft.find('.cancellation').text(cancellation2 + ' *');
        colLeft.find('.booking-change').text(bookingChange2 + ' *');
        colLeft.find('.no-show').text(noShow2 + ' *');
        colRight.find('.cancellation').text(cancellation2);
        colRight.find('.booking-change').text(bookingChange2);
        colRight.find('.no-show').text(noShow2);
      } else {
        noteFare.hide();
        colLeft.find('.cancellation').text(cancellation1);
        colLeft.find('.booking-change').text(bookingChange1);
        colLeft.find('.no-show').text(noShow1);
        colRight.find('.cancellation').text(cancellation2);
        colRight.find('.booking-change').text(bookingChange2);
        colRight.find('.no-show').text(noShow2);
      }
      // --- End

      var eachColSelect = summaryGroup.find('.row-head-select').find('.col-select');
      eachColSelect.each(function() {
        var valuettt = eachColSelect.find('.ttt').val();
        if(valuettt === "0"){
          summaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
          summaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
          summaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
        } else{
          summaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
          summaryGroup.find('.button-group-1').find('.text').addClass('hidden');
          summaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
        }
      });

      // --- Render color accordingly
      listContents.each(function() {
        var _this = $(this),
            thisText = _this.text();
        _this.removeClass('complimentary not-allowed fare-price');
        if(thisText.includes('Complimentary')){
          _this.addClass('complimentary');
        } else if(thisText.includes('Not allowed')){
          _this.addClass('not-allowed');
        } else {
          _this.addClass('fare-price');
        }
      });
      // --- End

      var rcmidCorresponding = el.find('.rcmid-corresponding').text();
      var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
      var flightId = el.closest('.flight-list-item').find('.segment-id').text();
      summaryGroup.find('#recommendation-id').val(rcmidCorresponding);
      summaryGroup.find('#fare-family-inbound').val(fareFamilyId);
      summaryGroup.find('#flight-inbound').val(flightId.trim());

      summaryGroup.removeClass('hidden');
      slideshowpremiumeconomy();
      listRow.each(function() {
        $(this).find('.col-select').eq(idx).removeClass('col-selected');
      });

      var nameTtem2 = summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
      $('.flight-search-summary-conditions').find('.tab-item-2').text(nameTtem2.text());
      $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');

      showCorrelativeTab();

      // scrollTop func
      var wrapContentBsp;

      setTimeout(function(){
        if($('.bsp-booking-summary').data('booking-summary-panel-1') !== undefined) {
          wrapContentBsp = $('.wrap-content-bsp');
        } else {
          wrapContentBsp = $('[data-booking-summary-panel]');
        }
        $('html, body').animate({
          scrollTop: $('.wrap-content-fs:eq(1)').offset().top - wrapContentBsp.innerHeight() - 20
        }, 400);
      }, 750);
    } else {
      el = $('.btn-price.active');
      var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
      var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text();
      var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text();
      var listRow1 = el.closest('.select-fare-table').find('.row-select, .row-head-select');
      var thisCol = el.closest('.col-select');
      var idx = thisCol.index();
      listRow1.each(function() {
        $(this).find('.col-select').eq(idx).addClass('col-selected');
      });
      var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
      var colSelected = listRow1.find('.col-select.col-selected');
      var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
      idx1 = colSelected.find('.index-of').text();
      nameHead = colSelected.find('.name-header-hidden').text();
      baggage = colSelected.find('.baggage').text();
      seatSelection = colSelected.find('.seat-selection').text();
      earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
      upgrade = colSelected.find('.upgrade').text();
      cancellation1 = colSelected.find('.cancellation').text();
      bookingChange1 = colSelected.find('.booking-change').text();
      noShow1 = colSelected.find('.no-show').text();
      colLeft = summaryGroup.find('.col-select.column-left');
      summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').attr('class', className );
      summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').addClass('border-fare-family');
      summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
      summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
      summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('name',"ttt-"+ eligibleRecommendationIds);
      summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('value',eligibleRecommendationIds);
      colLeft.find('.baggage').text(baggage);
      colLeft.find('.seat-selection').text(seatSelection);

      // colLeft.find('.seat-selection').siblings('.complimentary-note').remove();
      // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
      //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
      //   if(!colLeft.find('.seat-selection').siblings('.complimentary-note').length) {
      //     colLeft.find('.seat-selection').parent().append(seatSelectionExtra.clone());
      //   }
      // }

      colLeft.find('.earn-krisFlyer').text(earnKrisFlyer);

      var upgradeTextSummary = colLeft.find('.upgrade');
      upgradeTextSummary.text(upgrade);
      if(colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
        var upgradeText = colSelected.find('.upgrade');
        var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
        if(!upgradeTextSummary.siblings('[data-tooltip]').length) {
          upgradeTextSummary.parent().append(upgradeTooltip.clone());
          upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
        }
      } else {
        upgradeTextSummary.siblings('em').remove();
      }

      var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
      var flightId = el.closest('.flight-list-item').find('.segment-id').text();
      summaryGroup.find('#fare-family-outbound').val(fareFamilyId);
      summaryGroup.find('#flight-outbound').val(flightId.trim());

      summaryGroup.addClass('hidden');
      var nameTtem1 = summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
      $('.flight-search-summary-conditions').find('.tab-item-1').text(nameTtem1.text());
      $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

      listRow1.each(function() {
        $(this).find('.col-select').eq(idx).removeClass('col-selected');
      });

      // scrollTop func
      var wrapContentBsp;

      setTimeout(function(){
        if($('.bsp-booking-summary').data('booking-summary-panel-1') !== undefined) {
          wrapContentBsp = $('.wrap-content-bsp');
        } else {
          wrapContentBsp = $('[data-booking-summary-panel]');
        }
        $('html, body').animate({
          scrollTop: $('.wrap-content-fs:eq(0)').offset().top - wrapContentBsp.innerHeight() - 20
        }, 400);
      }, 750);
    }
    wrapContent = el.closest('.wrap-content-fs');
    el.closest('.upsell').addClass('hidden');
    parentFlightList = el.closest('.flight-list-item');
    parentFlightList.find('.col-info-left').get(0).click();
    parentFlightList.find('.change-flight-item.bgd-white').find('[data-wrap-flight-height]').data('wrap-flight-height', selfHeight);
    parentFlightList.find('.change-flight-item.bgd-white').removeClass('hidden');
    wrapContent.find('.economy-slider , .fs-status-list , .sub-logo , .monthly-view , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').addClass('hidden');
    wrapContent.find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
    wrapContent.find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
    wrapContent.find('.col-info-select').removeClass('active');
    parentFlightList.find('[data-less-details-table]').removeClass('hidden').removeClass('premium-economy');

    if(blockParentColSelect.find('.btn-price').hasClass('active') && $('.main-inner').find('.wrap-content-fs').length == 2 ){
      blockParentWrap.next().removeClass('hidden');

      var dataTriggercolumn = blockParentWrap.next().find('[data-trigger-animation]');
      if(dataTriggercolumn.hasClass('has-disabled')){
        dataTriggercolumn.removeClass('active');
        dataTriggercolumn.closest('.recommended-table').find('.airline-info').find('.less-detail').get(0).click();
        dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
        dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
      }

    }else{
      blockParentWrap.next().addClass('hidden');
    }

    el.closest('.wrap-content-fs').find('.flight-list-item').attr('tabindex', -1);
    handleLoadmore(blockParentWrap.next().find('.recommended-flight-block'), blockParentWrap.next().find('[data-loadmore]'));
    handleActionSlider();
    showMoreLessDetails();
  }

  var upgradeAdditional = function(){
    var nextBtn = el.closest('.col-select').next().find('.btn-price');
    var nameHeaderClick = nextBtn.data('header-class');
    var priceClick = el.find('.btn-price-cheapest-colum').text();
    var priceUpgrade = nextBtn.find('.btn-price-cheapest-colum').text();
    el.closest('[data-hidden-recommended]').find('.upsell').find('.name-family').text( nameHeaderClick );
    el.closest('[data-hidden-recommended]').find('.upsell').find('.price-sgd').text(' ' + parseFloat(priceUpgrade) - parseFloat(priceClick));
  }

  $(document).on('click','[data-trigger-progress]', function(){
    var blockParentWrap = $(this).closest('.wrap-content-fs');
    if(blockParentWrap.index() === 0) {
      if (!$(this).hasClass('btn-price-cheapest-select')) {
        $('[data-booking-summary-panel]').addClass('clicked');
      } else {
        $('[data-booking-summary-panel]').removeClass('clicked');
      }
    } else {
      $('[data-booking-summary-panel]').addClass('clicked');
    }
  });

  $(document).on('click.calculatePriceInbound', '.btn-price', function(e) {
    e.preventDefault();

    if(!$(this).hasClass('btn-price-cheapest-select')){
      var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
      changeFlightItem.each(function() {
        $(this).addClass('has-choose');
      });
    }

    var arrayPrice1, arrayPrice2;
    wrapContentList = $('.wrap-content-list');
    wrapContents = wrapContentList.find('.wrap-content-fs');
    var wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');
    clickedBtn = $(this);

    el = $(this);
    blockParentWrap = el.closest('.wrap-content-fs');

    var dataTriggerAnimation =  $('.wrap-content-fs').find('[data-flight-item]');
    if(!$(this).hasClass('btn-price-cheapest-select')){
      dataTriggerAnimation.each(function() {
        if($(this).hasClass('active')){
          $(this).closest('.flight-list-item').find('.col-info').removeClass('selected-item')
          $(this).find('.less-detail').get(0).click();
        }
      });
    }
    blockParentWrap.removeClass('selected-item');
    if (el.closest('.wrap-content-fs').index() === 0) {
      wrapContentNotFirst.find('.change-flight-item.bgd-white').addClass('hidden');
      wrapContentNotFirst.find('.economy-slider , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
      wrapContentNotFirst.find('.col-select').find('.btn-price').removeClass('active');

    }

    var listItems = wrapContentNotFirst.find('.flight-list-item');
    var listCol = listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
    var listPrice = listItems.find('.btn-price').find('.list-price');
    var oneCol = listItems.find('.one-column');
    var contentListPrice = $(this).find('.list-price').text();

    // el.closest('.select-fare-block').addClass('selected-item');
    if(el.hasClass('column-left')){
      el.closest('.flight-list-item').find('.column-trigger-animation').addClass('selected-item');
    } else if(el.hasClass('column-right')){
      el.closest('.flight-list-item').find('.column-trigger-animation-1').addClass('selected-item');
    }

    upgradeAdditional();
    blockParentWrap = el.closest('.wrap-content-fs');
    blockParentColSelect =  blockParentWrap.find('.col-select');
    blockParentColSelect.find('.btn-price').removeClass('active');
    $('.col-select').find('.btn-price').removeClass('active');
    $('.upsell').addClass('hidden');
    $(this).addClass('active');
    if($(this).hasClass('btn-price-cheapest-select')){
      var upsell = $(this).closest('[data-hidden-recommended]').find('.upsell');
      if(blockParentWrap.index() === 0){
        upsell.removeClass('hidden');
      upsell.removeClass('hidden');
      } else{
        hiddenBlockClick();
      }
      setTimeout(function(){
        upsell.find('input[name="btn-keep-selection"]').focus();
      },100);
    } else{
      hiddenBlockClick();
    }

    if (el.closest('.wrap-content-fs').index() !== 0) {
      return;
    }

    listCol.addClass('has-disabled');
    oneCol.each(function() {
      if(!$(this).hasClass('hidden')) {
        var thisIdx = $(this).closest('.select-fare-block').data('col-index');
        var thisColSelect = $(this).closest('.flight-list-item').find('[data-trigger-animation]');

        thisColSelect.each(function() {
          if($(this).data('trigger-animation') === thisIdx) {
            $(this).addClass('has-disabled');
          }
        });
      }
    });

    var listRcmID = listItems.find('.btn-price').find('.rcmid');
    var rcmIdListPrice = $(this).find('.rcmid-out').text();
    var rcmidListPriceTrim = rcmIdListPrice.trim();
    var arrayRcmId1 = rcmidListPriceTrim.split(" ");
    if(listRcmID.length){
      listRcmID.each(function(){
        var self = $(this);
        var arrayList = self.text().split("-");
        for(var j = 0 ; j < arrayList.length ; j++){ // 21 63
          for(var z = 0 ; z < arrayList[j].length ; z++){ //
              for(var i = 0 ; i < arrayRcmId1.length ; i++){ // 8
                if(arrayList[z] !== null || arrayList[z] !== "undefined" || arrayList[z] !== undefined ){
                  if(arrayList[z]){
                    if(arrayRcmId1[i] === arrayList[z].slice(0 , 4).trim()){

                      var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
                      var thisCol = $(this).closest('.col-select');
                      var idx = thisCol.index();
                      var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
                      listRow.each(function() {
                        $(this).find('.col-select').eq(idx).removeClass('has-disabled');
                      });
                      thisColSelectOneCol.removeClass('has-disabled');

                      $(this).closest('.btn-price').find('.btn-price-cheapest-colum').text(arrayList[z].slice(3).trim());
                      $(this).closest('.btn-price').find('.rcmid-corresponding').text(arrayRcmId1[i]);
                    }
                  }
                }
              }
          }
        }
      });
    }


    // var contentListPriceTrim = contentListPrice.trim();
    // arrayPrice1 = contentListPriceTrim.split(" ");
    // listPrice.each(function(){
    //   var self = $(this);
    //   arrayPrice2 = self.text().split(" ");
    //   for(var i = 0; i < arrayPrice1.length; i++){
    //     for(var j = 0; j < arrayPrice2.length; j++){
    //       if(arrayPrice1[i] === arrayPrice2[j]){
    //         var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
    //         var thisCol = $(this).closest('.col-select');
    //         var idx = thisCol.index();
    //         var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation]');
    //         listRow.each(function() {
    //           $(this).find('.col-select').eq(idx).removeClass('has-disabled');
    //         });
    //         thisColSelectOneCol.removeClass('has-disabled');
    //       }
    //     }
    //   }
    // });
    listItems.each(function() {
      var that = $(this);
      var thisFareBlock = $(this).find('.select-fare-block');
      var listColInfoSel = $(this).find('.col-info-select');
      thisFareBlock.each(function() {
        var listRowHeadSel = $(this).find('.select-fare-table.multy-column.hidden-mb-small .row-head-select');
        var listColSelLength = listRowHeadSel.find('.col-select:not(:eq(0))').length,
            listColSelDisabledLength = listRowHeadSel.find('.col-select.has-disabled').length;
        if (listColSelLength === listColSelDisabledLength) {
          var idx = $(this).data('col-index');
          listColInfoSel.each(function() {
            if($(this).data('trigger-animation') === idx) {
              $(this).addClass('has-disabled');
            }
          });
        }
      });
    });
    var flightColumnTrigger = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation');
    var flightColumnTrigger1 = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation-1');
    if (el.closest('.wrap-content-fs').index() === 0) {
      var arrPriceCheapest = [];
      var blockWrapFirst =  $('.wrap-content-fs').last();
      var colInfoLeft = blockWrapFirst.find('.col-info-left');
      colInfoLeft.find('.head-col').find('.label-status').remove();

      priceCurrentSelected = el.find('.btn-price-cheapest-colum').text();
      flightColumnTrigger.each(function(idx){
        $(this).find('.price-cheapest-outbound').text(priceCurrentSelected);
        $(this).closest('.flight-list-item').find('[data-hidden-recommended]').find('.price-cheapest-colum').text(priceCurrentSelected);
        var priceCheapest = $(this).find('.price-cheapest-colum').text();
        var priceSelectedOutBound = $(this).find('.price-cheapest-outbound').text();
        if(priceCheapest && priceSelectedOutBound) {
          var totalPriceCabin = parseFloat(priceCheapest) - parseFloat(priceSelectedOutBound);
          if(totalPriceCabin < 0 ){
            totalPriceCabin = 0;
          }
          var triggerAnimation = $(this);
          var priceStyleSmall = totalPriceCabin.toFixed(2).indexOf(".");

          triggerAnimation.find('.price').text("+ " + parseFloat(totalPriceCabin.toFixed(2).slice(0, priceStyleSmall)).toLocaleString());
          triggerAnimation.find('.price').append("<small></small>");
          triggerAnimation.find('.price').find('small').text(totalPriceCabin.toFixed(2).slice(priceStyleSmall));

          triggerAnimation.find('.price').append('<span class="compare-number hidden">'+totalPriceCabin+'</span>');
        }
      // }
      });

      colInfoLeft.each(function(idexLabel){
        arrPriceCheapest.push($(this).find('.price').find('.compare-number').eq(0).text());
      });

      arrPriceCheapest.sort(function(a, b) {
       return parseFloat(a) - parseFloat(b);
      });

      colInfoLeft.each(function(){
        if(parseFloat($(this).find('.flight-price').find('.compare-number').eq(0).text()) === parseFloat(arrPriceCheapest[0])){
          $(this).find('.head-col').append('<span class="label-status anim-all"></span');
        }
      })

      flightColumnTrigger1.each(function(idx1){
        $(this).find('.price-cheapest-outbound-1').text(priceCurrentSelected);
        $(this).closest('.flight-list-item').find('[data-hidden-recommended-1]').find('.price-cheapest-colum').text(priceCurrentSelected);
        // if(!$(this).hasClass('has-disabled')){
          var priceCheapest1 = $(this).find('.price-cheapest-colum').text();
          var priceSelectedOutBound1 = $(this).find('.price-cheapest-outbound-1').text();
          if(priceCheapest1 && priceSelectedOutBound1) {
            var totalPriceCabin1 = parseFloat(priceCheapest1) - parseFloat(priceSelectedOutBound1);
            if(totalPriceCabin1 < 0 ){
              totalPriceCabin1 = 0;
            }
            var triggerAnimation1 = $(this);
            var priceStyleSmall1 = totalPriceCabin1.toFixed(2).indexOf(".");
            triggerAnimation1.find('.price').text("+ " + parseFloat(totalPriceCabin1.toFixed(2).slice(0, priceStyleSmall1)).toLocaleString());
            triggerAnimation1.find('.price').append("<small></small>");
            triggerAnimation1.find('.price').find('small').text(totalPriceCabin1.toFixed(2).slice(priceStyleSmall1));
          }
        // }
      });
      var colSelect = $('.wrap-content-fs').last().find('[data-hidden-recommended]').find('.col-select').find('.btn-price');
      var colSelect1 = $('.wrap-content-fs').last().find('[data-hidden-recommended-1]').find('.col-select').find('.btn-price');
        colSelect.each(function(idxColSelect){
          var priceCheapestColumn = $(this).closest('[data-hidden-recommended]').find('.price-cheapest-colum').text();
          var priceCheapestColumnCabin = $(this).find('.btn-price-cheapest-colum').text();

          var totalPriceBtnColumn = parseFloat(priceCheapestColumnCabin) - parseFloat(priceCheapestColumn);
          var toFixedPrice = totalPriceBtnColumn.toFixed(2);
          if(totalPriceBtnColumn < 0 ){
            totalPriceBtnColumn = 0;
          }

          var priceStyleUnitSmall = toFixedPrice.indexOf(".");
          if(priceCheapestColumnCabin !== ''){
            $(this).find('.unit-small').text("");
            $(this).find('.btn-price-cheapest').empty().text("+ " + parseFloat(totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall)).toLocaleString());
            $(this).find('.unit-small').text(totalPriceBtnColumn.toFixed(2).slice(priceStyleUnitSmall));
            $(this).attr('data-price-segment-after', totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall));
          }
        });

        colSelect1.each(function(idxColSelect1){
          var priceCheapestColumn1 = $(this).closest('[data-hidden-recommended-1]').find('.price-cheapest-colum').text();
          var priceCheapestColumnCabin1 = $(this).find('.btn-price-cheapest-colum').text();

          var totalPriceBtnColumn1 = parseFloat(priceCheapestColumnCabin1) - parseFloat(priceCheapestColumn1);
          var toFixedPrice1 = totalPriceBtnColumn1.toFixed(2);

          if(totalPriceBtnColumn1 < 0 ){
            totalPriceBtnColumn1 = 0;
          }

          var priceStyleUnitSmall1 = toFixedPrice1.indexOf(".");
          if(priceCheapestColumnCabin1 !== ''){
            $(this).find('.unit-small').text("");
            $(this).find('.btn-price-cheapest-1').empty().text("+ " + parseFloat(totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1)).toLocaleString());
            $(this).find('.unit-small').text(totalPriceBtnColumn1.toFixed(2).slice(priceStyleUnitSmall1));
            $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
          }
        });
    }
  });

  $(document).on('click', '.group-btn > #btn-keep-selection', function(e) {
    e.preventDefault();
    el = $(this);
    if(!$(this).hasClass('btn-price-cheapest-select')){
      var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
      changeFlightItem.each(function() {
        $(this).addClass('has-choose');
      });
    }
    hiddenBlockClick();
  });

  $(document).on('click', '.group-btn > #btn-upgrade', function(e) {
    e.preventDefault();
    var btnPriceActive =  $(this).closest('.flight-list-item').find('.col-select').find('.btn-price.active');
    var parentBtn = btnPriceActive.closest('.col-select');

    var prevUpsell =  $(this).closest('.upsell').prev().find('.col-select');
    var btnEqActive = prevUpsell.find('.btn-price');
    // btnEqActive.eq(1).addClass('item-next-active');
    // var btnPriceNext = parentBtn.next().find('.btn-price.item-next-active');
    var btnPriceNext = parentBtn.next().find('.btn-price');
    if(btnPriceNext.length){
      btnPriceActive.removeClass('active');
      btnPriceNext.addClass('active');

      var priceActive = $(this).closest('[data-hidden-recommended]').find('.btn-price.active');
      var textPriceActive = priceActive.find('.header-family').text();
      var priceNext = priceActive.find('.btn-price-cheapest-colum').text();
      var priceCurrent = $(this).closest('.upsell').find('.price-selected ').text();
      var minus = parseFloat(priceNext) - parseFloat(priceCurrent);
      $(this).closest('.upsell').find('.name-family').text(textPriceActive);
      if(btnPriceNext.length){
        $(this).closest('.upsell').find('.price-sgd').text(" SGD "+ minus );
      }
      $(this).closest('.upsell').find('.price-selected ').text(priceNext);
      hiddenBlockClick();
    }
  });

  $(document).on('click', '.button-group-1 > .button-change', function(e) {
    var buttonSelected = $(this).closest('.recommended-flight-block').find('[data-trigger-animation].selected-item');
    if(buttonSelected.length){
      buttonSelected.get(0).click();
    }

    var wrapContentBsp;

    setTimeout(function(){
      if($('.bsp-booking-summary').data('booking-summary-panel-1') !== undefined) {
        wrapContentBsp = $('.wrap-content-bsp');
      } else {
        wrapContentBsp = $('[data-booking-summary-panel]');
      }
      $('html, body').animate({
        scrollTop: wrapContentBlock.offset().top - wrapContentBsp.innerHeight() - 20
      }, 400);
    }, 750);

    var wrapContentBlock = $(this).closest('.wrap-content-fs');
    $(this).closest('.main-inner').find('.fare-summary-group').addClass('hidden');
    wrapContentBlock.find('.select-fare-block.selected-item').addClass('active');
    if($('.main-inner').find('.wrap-content-fs').length == 2){
      wrapContentBlock.find('.change-flight-item.bgd-white').addClass('hidden');
      wrapContentBlock.find('.economy-slider , .fs-status-list , .monthly-view , .sub-logo , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
      wrapContentBlock.find('.col-select').find('.btn-price').removeClass('active');

      if(wrapContentBlock.index() === 0){
        wrapContentBlock.next().addClass('hidden');
        wrapContentBlock.next().find('.col-select').find('.btn-price').removeClass('active');
      } else {
        $('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
        $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
      }
      handleLoadmore(wrapContentBlock.find('.recommended-flight-block'), wrapContentBlock.find('[data-loadmore]'));
      showMoreLessDetails();
      handleActionSlider();
    }
    $('form.fare-summary-group').addClass('hidden');

    var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
    changeFlightItem.each(function() {
      if(!changeFlightItem.is(':visible')){
        $(this).removeClass('has-choose');
      } else {
        $(this).addClass('has-choose');
      }
    });

  });

  // show 2nd tab when click link right
  function showCorrelativeTab() {
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-left', function() {
      $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
    });
    $('.table-fare-summary.hidden-mb-small').on('click', '.link-right', function() {
      $('.flight-search-summary-conditions').find('.tab-right').trigger('click');
    });
  }

  var workflowTable = function(isRenderTemplate){
    tableLoadPage(isRenderTemplate);
    var priceTable = $('.recommended-table').find('.price');
    var flightListItem = $('.flight-list-item');
    var flightList = $('.wrap-content-fs').first().find('.flight-list-item');
    priceTable.each(function(idx){
      var contentInner = $(this).text();
      if($.trim(contentInner) === ""){
        var parentPrice = $(this).closest('.col-info-select');
        parentPrice.addClass('not-available');
        parentPrice.find('.flight-price').remove();
        parentPrice.find('span.not-available').empty().text('Not available');
      }
    });
    flightListItem.each(function(idx){
      var flightStationItem = $(this).find('.flight-station-item');
      flightStationItem.each(function(idx){
        if(!$(this).is(':last-child')){
          $(this).find('.less-detail').remove();
        }
      });
    });
  }


  var getTemplateFlightTable = function(el, data1, flightData, index, isFilter) {
    var loadmoreBlock = el.siblings('[data-loadmore]');
    var miniUrl = 'ajax/minifare-conditions.json';
    var upgradeUrl = 'ajax/fare-conditions-upgrade.json';

    if($('body').hasClass('sk-ut-flight-search-a')){
      miniUrl = 'ajax/minifare-conditions-a.json';
      upgradeUrl = 'ajax/fare-conditions-upgrade-a.json';
    } else if($('body').hasClass('sk-ut-flight-search-b')){
      miniUrl = 'ajax/minifare-conditions-b.json';
      upgradeUrl = 'ajax/fare-conditions-upgrade-b.json';
    }

    if($('body').hasClass('fs-sk-mixed-rbd-biz')){
       // miniUrl = 'ajax/sk-minifare-conditions.json';
       miniUrl = 'ajax/sk-minifare-conditions.json';

    }

    if($('body').hasClass('fs-economy-rbd')){
         miniUrl = 'ajax/sk-minifare-conditions-pey-biz-syd-maa-new.json';
    }


    var tplUrl = ''; 
    if ($('body').hasClass('fs-OAL-prompt')) {
      miniUrl = 'ajax/sk-fare-families-264-minifare.json';
      tplUrl = global.config.url.fsEconomyFlightTable2;
    } else {
      tplUrl = global.config.url.fsEconomyFlightTable
    }


    if(miniFareData) {
      // $.get(global.config.url.fsEconomyFlightTable, function(data) {
      $.get(tplUrl, function(data) {
        var template = window._.template(data, {
          data: data1,
          familyData: miniFareData,
          flight: flightData ? flightData : data1.flights[index],
          flightIdx: index
        });

        el.find('.flight-list-item').remove();
        el.append($(template));
        el.find('.flight-list-item:gt(4)').addClass('hidden');

        if(isFilter && index === 1) {
          $(clickedBtn).trigger('click.calculatePriceInbound');
        }

        selectFlightAnimation();
        showMoreLessDetails();
        initPopup();
        // init tooltip
        if($('[data-tooltip]')) {
          $('[data-tooltip]').kTooltip();
        }
        workflowTable(isFilter);
        handleLoadmore(el, loadmoreBlock);

        var segmentsLength = el.find('.flight-list-item').length;

        if(!segmentsLength) {
          el.addClass('hidden');
          el.siblings('.no-result-filter').removeClass('hidden');
        } else {
          el.siblings('.no-result-filter').addClass('hidden');
          el.removeClass('hidden');
        }

      })
    } else {
      $.ajax({
        url: 'ajax/minifare-conditions.json',
        type: SIA.global.config.ajaxMethod,
        dataType: 'json',
        success: function(response) {
          miniFareData = response;
          // $.get(global.config.url.fsEconomyFlightTable, function(data) {
          $.get(tplUrl, function(data) {
            var template = window._.template(data, {
              data: data1,
              familyData: response,
              flight: flightData ? flightData : data1.flights[index],
              flightIdx: index
            });

            el.find('.flight-list-item').remove();
            el.append($(template));
            el.find('.flight-list-item:gt(4)').addClass('hidden');

            if(isFilter && index === 1) {
              $(clickedBtn).trigger('click.calculatePriceInbound');
            }

            selectFlightAnimation();
            showMoreLessDetails();
            initPopup();
            // init tooltip
            if($('[data-tooltip]')) {
              $('[data-tooltip]').kTooltip();
            }
            workflowTable(isFilter);
            handleLoadmore(el, loadmoreBlock);

            var segmentsLength = el.find('.flight-list-item').length;

            if(!segmentsLength) {
              el.addClass('hidden');
              el.siblings('.no-result-filter').removeClass('hidden');
            } else {
              el.siblings('.no-result-filter').addClass('hidden');
              el.removeClass('hidden');
            }

          })
        }
      });
    }
  }

  var renderFlightTable = function(data1) {
    var flightBlock = $('.recommended-flight-block');

    flightBlock.each(function(index){
      var self = $(this);
      if(index === 0) {
        getTemplateFlightTable (self, data1, null, index);
      } else {
        setTimeout(function(){
          getTemplateFlightTable (self, data1, null, index);
        }, 2000);
      }
    })
  }

  var resetFilter = function() {
    var btnReset = $('[data-reset-filter]');

    btnReset.each(function(){
      $(this).off('click.resetFilter').on('click.resetFilter', function(e){
        e.preventDefault();
        var filterBlock = $(this).closest('.no-result-filter').siblings('[data-flight-filter]'),
            listCheckbox = filterBlock.find('input[type="checkbox"]').not("[disabled]"),
            listRangeSlider = filterBlock.find('[data-range-slider]'),
            checkboxReset = filterBlock.find('input[type="checkbox"]:disabled');

        listCheckbox.each(function(){
          $(this).prop('checked', false);
        })

        listRangeSlider.each(function(){
          var min = $(this).data('min'),
              max = $(this).data('max');
          $(this).slider("option", "values", [min, max]);
        })

        checkboxReset.prop('checked', true).trigger('change.resetFilter');

      })
    })
  }

  var renderCombinationsJson = function(){
    var templateBookingPayment;
    var appendDiv;
    if(!$('body').hasClass('fs-economy')){
      appendDiv = $('.combinations-json');
    }else{
      appendDiv = $('.fs-economy').find('.top-main-inner');
    }

    var combinationsJson = function(data1){
      if(!$('body').hasClass('fs-economy')){
        $.get(global.config.url.combinationsJsonTpl, function (data) {
          var template = window._.template(data, {
            data: data1
          });
          templateBookingPayment = $(template);
          appendDiv.append(templateBookingPayment);
        });
      }else{
        $.get(global.config.url.fsEconomy, function (data) {
          var filterArr = getFilterData(data1.flights);
          var template = window._.template(data, {
            data: data1,
            filterArr: filterArr
          });
          templateBookingPayment = $(template);
          appendDiv.append(templateBookingPayment);
          renderFlightTable(data1);
          showHideFilters();
          handleActionSlider();
          sliderRange();
          filterFlights(data1);
          resetFilter(data1.flights);

          var urlPage;
          if($('body').hasClass('sk-ut-flight-search-a')){
            urlPage = 'sk-ut-passenger-details-a.html';
          } else if($('body').hasClass('sk-ut-flight-search-b')){
            urlPage = 'sk-ut-passenger-details-b.html';
          }

          if(urlPage) {
            $('form[name="flight-search-summary"]').attr('action', urlPage);
          }

        });
      }
    };
    var url;
    if($('body').hasClass('fs-economy-response-page')){
      url = "ajax/flightsearch-response.json";
    }
    if($('body').hasClass('fs-economy-response-new-page') || $('body').hasClass('fs-economy')){
      url = "ajax/sin-sfo-30 JUNE-2017-Most-updated.json";
    }
    if($('body').hasClass('fs-business')){
      url = "ajax/sin-sfo-business-first.json";
    }
    if($('body').hasClass('fs-economy-scoot')){
      url = "ajax/sin-sfo-economy-scoot.json";
    }
    if($('body').hasClass('fs-economy-two-column-premium-economy')){
      url = "ajax/sin-sfo-tow-column-premium-economy.json";
    }
    if($('body').hasClass('fs-economy-sin-maa-roundtrip-page')){
      url = "ajax/flight-search-SIN-MAA-Roundtrip-2A1C1I.json";
    }
    if($('body').hasClass('fs-economy-four-column')){
      url = "ajax/sin-sfo-economy-four-column.json";
    }

    if($('body').hasClass('fs-sk-mixed-rbd-biz')){
      url = "ajax/sk-bus-first-syd-maa.json";
    }
    if($('body').hasClass('fs-economy-rbd')){
      url = "ajax/sk-pey-biz-syd-maa-new.json";
    }

    if ($('body').hasClass('fs-OAL-prompt')) {
      url = 'ajax/sk-fare-families-264.json'; 
    }

    
    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(response) {
        var data1 = response.response;
        combinationsJson(data1);
      }
    });
  };

  var popup1 = $('.popup--flights-details-sf');
  var popup2 = $('.flight-search-summary-conditions');
  $(document).on('click', '.flights-details-sf', function(e) {
    e.preventDefault();
    $(this).trigger('dblclick');
    popup1.Popup('show');
     $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('dblclick', '.flights-details-sf',function(){
  popup1.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.trigger-summary-of-fare-condition', function(e) {
    e.preventDefault();
    $(this).trigger('dblclick');
    if($(this).hasClass('link-left')){
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
    } else{
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').removeClass('active');
      $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').addClass('active');
    }
    popup2.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('dblclick', '.trigger-summary-of-fare-condition',function(){
  popup2.Popup('show');
    $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.popup__close', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    event.preventDefault();
  });

  if($('body').hasClass('fs-economy-page')){
    renderCombinationsJson();
  }

};
