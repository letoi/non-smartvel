var SIA = SIA || {};

SIA.PromptPaxPromo = function() {

	var verifiedEmail = { };

    var init = function() {
		selectPassengerListener();
		emailVerifyListener();
		addNextPassengerListener();
	};

	var addNextPassengerListener = function() {
		$("#adult-passenger-input-1").off().on("click", function(evt) {
			var $this = $(this);
			var activeIndex = $(".booking-nav.booking-nav--1").find("a.active").data("idx");
			var validatedEmail = verifiedEmail["passenger" + activeIndex];
			var isNotVerified = $(".form-passenger-detail").valid();

			if (!validatedEmail || !isNotVerified) {
				evt.preventDefault();
				evt.stopImmediatePropagation();
				if (
					isNotVerified &&
					$("#email-address-promo").valid()
				) {
					emailValidate(function() {
						if ($this.val().toLowerCase() == "next") {
							return $this.closest("form")[0].submit();
						}

						$this.trigger("click");
					});
				} else if (isNotVerified) {
					$("#email-address-promo").focus();
				}
				return;
			} else if (!validatedEmail.isValid) {
				evt.preventDefault();
				evt.stopImmediatePropagation();
				// $("html, body").animate({
				// 	scrollTop: $(".promo-alert.error-alert").offset().top - 200
				// }, 500);
				$("#email-address-promo").focus();
				return;
			} else {
				if ($this.val().toLowerCase() == "next") {
					evt.preventDefault();
					evt.stopImmediatePropagation();
					return $this.closest("form")[0].submit();
				}
				$(".success-alert, .error-alert").addClass("hidden");
				if (activeIndex + 1 == 1) {
					setTimeout(function() {
						$this.val("Next");
					}, 500);
				}
			}
		});
	};

	var selectPassengerListener = function() {
		$(".booking-nav.booking-nav--1").find("a").on("click", function() {
			var $this = $(this);
			var validatedEmail = verifiedEmail["passenger" + $this.data("idx")];
			if (validatedEmail) {
				toggleAlertClass(true);
				setTimeout(function() {
					$("#email-address-promo").val(validatedEmail.email);
				}, 500);
			} else {
				$(".success-alert, .error-alert").addClass("hidden");
			}
		});
	};

	var emailVerifyListener = function() {
		$("#email-address-promo").on("focus", function() {
			$.validator.messages.email = "Enter a valid email address.";
		});	

		$("#verify-email").off().on("click.verifyEmail", function() {
			if ($("#email-address-promo").valid()) {
				emailValidate();
			}
			$("#verify-email").blur();
		});
	};
	
	var emailValidate = function(cb) {
		var validQueryParam = window.location.href.match(/valid=(\w+)&*/);
		var queryString = (validQueryParam) ? validQueryParam[1] : null;
		if (queryString.toLowerCase() == "pass") {
			checkValidEmail("pax-promo-success.json", cb);
		} else if (queryString.toLowerCase() == "fail") {
			checkValidEmail("pax-promo-fail.json", cb);
		}
	};

	var checkValidEmail = function(jsonFile, cb) {
		$.get("ajax/" + jsonFile, function(response) {
			var isValid = response.validEmail;
			toggleAlertClass(isValid);
			logVerifiedEmails(isValid);
			if (cb) {
				cb(response.validEmail);
			}
		});
	};

	var logVerifiedEmails = function (isValid) {
		var passengerId = $(".booking-nav.booking-nav--1").find("a.active").data("idx");
		verifiedEmail["passenger"+passengerId] = {
			email: $("#email-address-promo").val(),
			isValid: isValid
		}
	};

	var toggleAlertClass = function(isValid) {
		$(".promo-alert.success-alert")[isValid ? "removeClass":"addClass"]("hidden");
		$(".promo-alert.error-alert")[!isValid ? "removeClass":"addClass"]("hidden");
	};

    var pub = {
		init: init
	};

	return pub;
}();

var waitForUnderscore = function () {
	setTimeout(function () {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.PromptPaxPromo.init();
		}
	}, 100);
};

$(function () {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.PromptPaxPromo.init();
});
