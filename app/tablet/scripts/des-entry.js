/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */

SIA.desEntry = function(){
	var global = SIA.global,
		win = global.vars.win,
		config = global.config,
		btnSeeMore = $('.country-button [data-see-more]'),
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startMbNum = 0,
		defaultMbNum = 4,
		seeMoreMbNum = 2,
		startDeskNum = 0,
		startTabNum = 0,
		defaultDeskNum = 15,
		defaultTabNum = 9,
		seeMoreDeskNum = 5,
		seeMoreTabNum = 9,
		seeMoreCount = 0,
		winW = window.innerWidth,
		isTablet = winW < config.tablet && winW >= config.mobile,
		isMobile = winW < config.mobile,
		isShuffleActive = false;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var initShuffle = function() {
		var countryItem = container.find('.static-item.col-mb-3').eq(0);
		container.shuffle({
			speed: 0,
			itemSelector: '.static-item',
			sizer: countryItem,
			supported: false,
			isLeftSort: false
		});
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region-categories');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var changeRegion = function(regionVal) {
		initCity(res.cities, (regionVal != 1 ? regionVal : null));
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		cities = _.sortBy(cities, function(o) { return o.city; });
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg == region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region-categories') == region || region == "1") &&
					(!city || self.data('city').toLowerCase() == city.toLowerCase());
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);

		var state = (isMobile ? (startMbNum = 0) : (!isTablet ? (startDeskNum = 0) : (startTabNum = 0))) || fillContent(false);
		return state;
	};

	var generateTemplate = function(tpl, resLen) {
		if (!isShuffleActive) {
			initShuffle();
			isShuffleActive = true;
		}
		else {
			container.shuffle('appended', tpl);
			var searchKey = $("#search-city").val();
			if (!searchKey) {
				_.each(tpl, function(el) {
					el = $(el);
					if (!el.hasClass("col-mb-3")) {
						el.addClass("col-mb-3");
					}
				});
				var canLoadMore = container.find(".static-item:not(.shuffle-item)").length;
				if (!canLoadMore) {
					SIA.desEntryFilter.canLoadMoreFn(false);
				}
				SIA.desEntryFilter.arrangeTpl();
			} else {
				SIA.desEntryFilter.resetFilters();
			}
		}
		
		changeSeemoreText(resLen);
	};

	var changeSeemoreText = function(resLen) {
		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.addClass('hidden');
			$('.country-button-border').addClass('hidden');

			return isMobile ?
				(startMbNum = resLen) :
				(!isTablet ? (startDeskNum = resLen) : (startTabNum = resLen));
		}
		else {
			// btnSeeMore.text(seeMoreCount === 2 ?
			// 	L10n.kfSeemore.seeAll : L10n.kfSeemore.loadMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx, resLen) {
		var tpl = $(".static-item");
		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');

				if (!img.data('loaded')) {
					var newImg = new Image();
					newImg.onload = function() {
						self.removeClass('hidden');
						img.data('loaded', true);

						if (!tpl.filter('.hidden').length) {
							generateTemplate(tpl, resLen);
						}
						img.parent().css({
							'background-image': 'url(' + this.src + ')'
						});
					};
					newImg.src = img.data('img-src');
					img.attr('src', config.imgSrc.transparent);
				}
				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(tpl, resLen);
					}
				}
			});
		}
	};

	var fillContent = function(isResize, setNumber) {
		if (staticItems.length) {
			var endIdx = 0;
			var resLen = staticItems.length;

			if (isResize) {
				generateClass(staticItems);
			}

			if (isMobile) {
				endIdx = !startMbNum ?
					(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 2 : startMbNum + defaultMbNum)) :
					(seeMoreCount > 2 ? resLen : startMbNum + seeMoreMbNum);
				startMbNum = endIdx;
			}
			else {
				if (!isTablet) {
					endIdx = !startDeskNum ?
						(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 5 : startDeskNum + defaultDeskNum)) :
						(seeMoreCount > 2 ? resLen : startDeskNum + seeMoreDeskNum);
					startDeskNum = endIdx;
				}
				else {
					endIdx = !startTabNum ?
						(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 3 : startTabNum + defaultTabNum)) :
						(seeMoreCount > 2 ? resLen : startTabNum + seeMoreTabNum);

					startTabNum = endIdx;
				}
			}

			getTemplate(staticItems, endIdx, resLen);
		}
	};

	var generateClass = function(res) {
		resetClass(res.removeAttr('style'), 'static-item--large col-mb-6', 'hidden col-mb-3');
		return (isTablet ?
			(generateClassForTablet(res)) :
			(!isTablet && !isMobile ? (generateClassForDesktop(res)) : 0)) || true;

		// if (!isTablet && !isMobile) {
		// 	generateClassForDesktop(res);
		// }
		// else if (isTablet) {
		// 	generateClassForTablet(res);
		// }
	};

	var resetClass = function(elm, rClass, aClass) {
		elm
			.removeClass(rClass)
			.addClass(aClass);
	};

	var generateClassForDesktop = function(res) {
		var i = -6, temp = 0;
		do {
			if (temp <= 2) {
				i += 6;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 3;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	var generateClassForTablet = function(res) {
		var i = -4, temp = 0;
		do {
			if (temp <= 1) {
				i += 4;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 2;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	var resizeTime;
	win.off('resize.template').on('resize.template', function() {
		clearTimeout(resizeTime);
		resizeTime = setTimeout(function() {
			var winW = window.innerWidth;
			if(winW >= config.tablet){
				if(isTablet || isMobile) {
					container.removeClass('static-item__mb-height');
					var setNumber = isMobile ? Math.ceil(startMbNum / 2) : Math.ceil(startTabNum / 3);
					isTablet = false;
					isMobile = false;
					startDeskNum = 0;
					fillContent(true, setNumber);
				}
			}
			else if (winW < config.tablet && winW >= config.mobile) {
				if (!isTablet || isMobile) {
					container.removeClass('static-item__mb-height');
					var setNumber = isMobile ? Math.ceil(startMbNum / 2) : Math.ceil(startDeskNum / 5);
					isTablet = true;
					isMobile = false;
					startTabNum = 0;
					fillContent(true, setNumber);
				}
			}
			else {
				if (!isMobile) {
					container.addClass('static-item__mb-height');
					var setNumber = isTablet ? Math.ceil(startTabNum / 3) : Math.ceil(startDeskNum / 5);
					isMobile = true;
					isTablet = false;
					startMbNum = 0;
					fillContent(true, setNumber);
				}
			}
		}, 100);
	});

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		SIA.desEntryFilter.resetFilters();
	});

	var canClick = function(status) {
		preventClick = false;
	};

	var initModule = function(){
		var public = {
			renderTemplate: renderTemplate,
			changeRegion: changeRegion,
			canClick: canClick
		};

		container.toggleClass('static-item__mb-height', isMobile);
		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);
		SIA.desEntryFilter.init(public);
	};

	initModule();
};

SIA.desEntryFilter = function() {
	var p, widthEl = 0, heightEl = 0, canLoadMore = true, tabletLimit = 9, tabletSeeMore = 9, step = 1;

	var addInspireMeListeners = function() {
		step = 1;
		addInspireFiltersListeners();
	};

	var addInspireFiltersListeners = function() {
		$(".destination-inspireme-category").find("[type='radio']").off().on("click", function(evt) {
			step = 1;
			filterTriggers();
			regionTriggers();
		});

		step = 1;
		filterTriggers();
		regionTriggers();
	};

	var addRegionListeners = function() {
		$(".destination-regions-categories").find("input").off().on("change", function(evt) {
			step = 1;
			
			filterTriggers();
			regionTriggers();
		});
	};

	var regionTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var regionFilters = getRegionSelected();
		regionFilters = regionFilters.length ? regionFilters : ["all"];
		var filteredElements = getAllFilterBy(regionFilters, "region-categories");
		var showElLimit = showLoadMore(filteredElements);
		if (showElLimit.hidden.length) {
			removeAllHidden($(".country-button"));
		} else {
			$(".country-button").addClass("hidden");
		}
		toggleElements(showElLimit.display);
		var displayLength = showElLimit.display.length;
		var totalLength = showElLimit.display.length + showElLimit.hidden.length;
		$(".no-search-found")[displayLength ? "addClass" : "removeClass"]("hidden");
		$(".search-found").find(".no-search").text(totalLength);
		$(".search-found").find(".no-search").next().text((displayLength < 2) ? "destination found" : "destinations found");
		$(".country-button").find(".display-item").text(displayLength);
		$(".country-button").find(".total-item").text(totalLength);
		removeSearchText();
	};

	var removeSearchText = function() {
		$("#search-city").val("");
	};

	var showLoadMore = function(filteredElements) {
		var tabletLoad = 1, stepValue = 0;
		if (step == 1) {
			tabletLoad = tabletLimit;
			stepValue = tabletLoad * step;
		} else {
			tabletLoad = tabletSeeMore;
			stepValue = tabletLimit + (tabletSeeMore * (step - 1));
		}
		var displayEl = filteredElements.splice(0, stepValue);
		_.each(filteredElements, function(el) {
			el = $(el);
			el.addClass("hidden");
		});

		return {
			display: displayEl,
			hidden: filteredElements
		};
	};

	var removeAllHidden = function(el) {
		el.removeClass("hidden");
		el.find(".hidden").each(function() {
			var $this = $(this);
			$this.removeClass("hidden");
		});
	};

	var filterTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var filteredElements;
		var inspireFilter = getInspiredClick();
		
		filteredElements = getAllFilterBy([inspireFilter], "inspire-categories");
		toggleElements(filteredElements);
		p.canClick(false);
	};

	var toggleElements = function(elementList) {
		removeStatic();
		var step = 0;
		var stepValue = heightEl;
		for (var i = 0 ; i < elementList.length ; i += 3) {
			var firstEl = $(elementList[i]);
			var secondEl = $(elementList[i + 1]);
			var thirdEl = $(elementList[i + 2]);
			var topValue = (stepValue * step);

			if (firstEl) {
				firstEl.css("left", "0px");
				firstEl.css("top", topValue + "px");
				if (i == 0) {
					firstEl.addClass("static-item--large col-mb-6");
				}
				firstEl.removeClass("hidden");
				if (!firstEl.hasClass("col-mb-3")) {
					firstEl.addClass("col-mb-3");
				}
			} 
			if (secondEl) {
				secondEl.css("left", widthEl + "px");
				secondEl.css("top", topValue + "px");
				secondEl.removeClass("hidden");
				if (!secondEl.hasClass("col-mb-3")) {
					secondEl.addClass("col-mb-3");
				}
			}
			if (thirdEl) {
				thirdEl.css("left", (widthEl * 2) + "px");
				thirdEl.css("top", topValue + "px");
				thirdEl.removeClass("hidden");
				if (!thirdEl.hasClass("col-mb-3")) {
					thirdEl.addClass("col-mb-3");
				}
			}
			step ++;
		}
		if (step == 0)   {
			step ++;
		}
		$(".static-block--item").css("height", (stepValue * step) + "px");
	};

	var removeStatic = function() {
		var staticEL = $(".static-block--item").find(".static-item--large");
		staticEL.removeClass("static-item--large col-mb-6");
		staticEL.addClass("hidden");
	}

	var getAllFilterBy = function(filters, dataAttr) {
		var regexString = _.map(filters, function(i) {
			return "(" + i + ")";
		}).join("|");
		var regex = new RegExp(regexString, "i");
		var allVisibleElements = $("[data-" + dataAttr + "]:visible");
		var hiddenElements = $("[data-" + dataAttr + "].hidden");
		if (dataAttr != "region-categories") {
			allVisibleElements = $.merge(allVisibleElements, hiddenElements);
		} 
		var filteredElements = _.filter(allVisibleElements, function(el) {
			el = $(el);
			
			var containsValue = regex.test(el.data(dataAttr));
			if (containsValue || filters[0] == "all") {
				return el;
			} else {
				el.addClass("hidden");
			}
		});
		
		return filteredElements;
	};

	var getRegionSelected = function() {
		var allCustomRegion = $(".custom--region");
		var clickedFilters = [];
		_.each(allCustomRegion, function(el, idx) {
			el = $(el);
			
			if (el.find("input").is(":checked")) {
				clickedFilters.push(el.data("custom-region"));
			}
		});
		
		return clickedFilters;
	};

	var getInspiredClick = function() {
		var checkedElement = $(".inspireme-radio-filter").find("[type='radio']:checked");
		return checkedElement.val();
	};

	var toggleCheckbox = function(status) {
		var allCustomRegion = $(".custom--region");
		_.each(allCustomRegion, function(el, idx) {
			el = $(el);

			if (el.find("input").is(":checked") == status) {
				el.find("label").trigger("click");
				setTimeout(function() {
					if (el.find("input").is(":checked")) {
						el.find("input").prop("checked", false);
					}
				}, 100);
			}
		});
	};

	var arrangeTpl = function() {
		filterTriggers();
		regionTriggers();
	};

	var canLoadMoreFn = function(status) {
		canLoadMore = status;
	};

	var resetFilters = function() {
		var city = $("#search-city").val();
		$("#radio-inspireme-all").prop("checked", true);
		toggleCheckbox(true);
		if (city) {
			$(".block-search").css("opacity", "0");
			setTimeout(function() {
				$(".block-search").css("opacity", "1");
				displaySearchItem(city);
			}, 10);
		} else {
			displaySearchItem(city);
		}
	};

	var displaySearchItem = function(city) {
		var showSearch = [];
		$(".static-item[data-city]").each(function() {
			var $this = $(this);
			if ($this.data("city").toLowerCase() == city.toLowerCase()) {
				showSearch.push($this);
			} else {
				$this.addClass("hidden");
			}
		});
		toggleElements(showSearch);
		$(".no-search-found")[showSearch.length ? "addClass" : "removeClass"]("hidden");
		$(".search-found").find(".no-search").text(showSearch.length);
		$(".search-found").find(".no-search").next().text((showSearch.length < 2) ? "destination found" : "destinations found");
		$(".country-button").addClass("hidden");
	};

	var loadMoreButton = function() {
		$("[data-see-more]").off().on("click", function(evt) {
			evt.preventDefault();
			evt.stopPropagation();
			step ++;
			filterTriggers();
			regionTriggers();
		});
	};

	var resetSearchButtonListener = function() {
		$(".reset-search-button").off().on("click", function(evt) {
			evt.preventDefault();
			resetFilters();
			if($("#radio-inspireme-all").prop("checked")) {
				setTimeout(function(){
					filterTriggers();
					regionTriggers();
				}, 200);
			}else {
				$("#radio-inspireme-all").trigger("click");
			}
		});
	};

	var init = function(public) {
		p = public;
		addInspireMeListeners();
		addRegionListeners();
		loadMoreButton();
		resetSearchButtonListener();
		p.renderTemplate();
	};

	return {
		init: init,
		arrangeTpl: arrangeTpl,
		canLoadMoreFn: canLoadMoreFn,
		resetFilters: resetFilters,
		displaySearchItem: displaySearchItem
	}
}();
