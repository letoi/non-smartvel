var SIA = SIA || {};

SIA.whereWeFly = function () {
    var onHoverElements = [
        ".region-wrapper"
    ];
    var imageDefault = "images/map-land.png";
    var imageBGElement = $(".map-display");

    var init = function() {
        onClickListener();
        backButtonListener();
        customTooltipListener();
    };

    var toggleBack = function(state) {
        var classIds = [
            ".map-back-button"
        ];

        $(classIds.join(","))[state ? "removeClass" : "addClass"]("hidden");
    };

    var customTooltipListener = function() {
        $(".custom-tooltip").find(".tooltip-header").off().on("click", function() {
            toggleTooltip(!$(".custom-tooltip").hasClass("active"));
        });
        toggleTooltip(false);
    };

    var openAccordion = function(code) {
        var accTrigger = $("[data-map-accordion='" + code + "']").find("[data-accordion-trigger]");
        if (!accTrigger.hasClass("active")) {
            accTrigger.trigger("click");
        }
    };

    var openDefaultAccordion = function(id) {
        var accTrigger = $("[data-map-accordion='"+ id +"']").find(".accordion__control");
        if (!id) {
            $("[data-map-accordion]").find(".accordion__control.active").trigger("click");
        } else if (!accTrigger.hasClass("active")) {
            accTrigger.trigger("click");
        }
    };

    var toggleTooltip = function(state) {
        $(".custom-tooltip").find(".content")[state ? "removeClass" : "addClass"]("hidden");
        $(".custom-tooltip")[state ? "addClass" : "removeClass"]("active");
    };
 
    var onHoverListener = function() {
        $(onHoverElements.join(",")).off()
            .on("click", function() {
                var imageSelected = $(this).data("map-image-hover");
                hideAllHoverSpan();
                if (imageSelected == imageBGElement.attr("src")) {
                    return imageBGElement.attr("src", imageDefault);
                }
                imageBGElement.attr("src", imageSelected);
                $(this).find(".hover-span").removeClass("hidden");
            });
        onClickListener();
    };

    var hideAllHoverSpan = function() {
        $(".hover-span:not(.hidden)").each(function() {
            $(this).addClass("hidden");
        });
    };

    var backButtonListener = function() {
        $(".map-back-button").off().on("click", function(evt) {
            evt.preventDefault();
            imageBGElement.attr("src", imageDefault);
            onHoverListener();
            toggleBack(false);
            openDefaultAccordion();
            $(".top-text").removeClass("hidden");
        });
    };

    var onClickListener = function() {
        $("body").on("click.map", "[data-map-image-onclick]", function(evt) {
            evt.preventDefault();
            
            var imageOnClickData = $(this).data("map-image-onclick");
            if (imageOnClickData) {
                imageBGElement.attr("src", imageOnClickData);
                $(".top-text").addClass("hidden");
                $(onHoverElements.join(",")).off("click");
                $("body").off("click.map");
                openAccordion($(this).data("map-accordion-trigger"));
                toggleBack(true);
                hideAllHoverSpan();
            }
        });
    };

    return {
        init: init
    };
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.whereWeFly.init();
        }
    }, 100);
};

(function($){

    $.event.special.doubletap = {
      bindType: 'touchend',
      delegateType: 'touchend',
  
      handle: function(event) {
        var handleObj   = event.handleObj,
            targetData  = jQuery.data(event.target),
            now         = new Date().getTime(),
            delta       = targetData.lastTouch ? now - targetData.lastTouch : 0,
            delay       = delay == null ? 300 : delay;
  
        if (delta < delay && delta > 30) {
          targetData.lastTouch = null;
          event.type = handleObj.origType;
          ['clientX', 'clientY', 'pageX', 'pageY'].forEach(function(property) {
            event[property] = event.originalEvent.changedTouches[0][property];
          })
  
          // let jQuery handle the triggering of "doubletap" event handlers
          handleObj.handler.apply(this, arguments);
        } else {
          targetData.lastTouch = now;
        }
      }
    };
  
  })(jQuery);

$(function() {
    waitForUnderscore();
});
