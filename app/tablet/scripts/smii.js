SIA.FormValidator = function() {
	var p = {};

	p.errElClss = '.gigya-error-msg';

	var init = function() {

	};

	var attachEvnt = function(obj) {
		var evnts = ['keypress', 'blur', 'click', 'focus'];

		if (typeof obj != 'undefined' && typeof obj.el != 'undefined') {
			for (var i = 0, iLen = evnts.length; i < iLen; i++) {
				var evntType = evnts[i];
				var evntFunc = obj[evntType];

				if (typeof evntFunc != 'undefined') {
					obj.el.on(evntType, evntFunc);
				}
			}
		}
	};

	var validateEmail = function(val) {
		var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

		return {
			state: emailReg.test(val),
			msg: 'Please enter a valid email address.'
		};
	};

	var validateEmpty = function(val) {
		return {
			state: (val != '') ? true : false,
			msg: 'This field is required'
		};
	};
	var isAlphaNumeric = function(val) {
		var regex = /^[a-z0-9]+$/i;

		return {
			state: regex.test(val),
			msg: 'Enter a valid promotional code.'
		}
	}

	var isNotNumeric = function(val) {
		var value = val;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			var str = value[i];
			if (isNaN(str)) {
				return true;
			}
		}

		return false;
	};

	var isValRepetitive = function(val) {
		var value = val;
		var repetitiveTotal = parseInt(value[0]) * value.length;
		var valueTotal = 0;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == repetitiveTotal) ? true : false;
	};

	var isValSequential = function(val) {
		var value = val;
		var sequentialTotal = 0;
		var valueTotal = 0;

		for (var j = parseInt(value[0]), jLen = parseInt(value[value.length - 1]); j <= jLen; j++) {
			sequentialTotal += j;
		}

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == sequentialTotal) ? true : false;
	};

	var computeBirthDate = function(val) {
		var date = parseDate(val);

		// first month starts at 0
		var birthDate = new Date(date.year, date.month - 1, date.day);
		var nowDate = new Date();
		var age =  nowDate.getFullYear() - birthDate.getFullYear();
		var month = nowDate.getMonth() - birthDate.getMonth();

		// compute the exact birthdate of the user
		if(month < 0 || (month === 0 && nowDate.getDate() < birthDate.getDate())) { age--; }

		return age;
	};

	var validatePassword = function(val) {
		var passInputReg;
		var errorMsg = '';
		var passState = true;

		if($('body').hasClass("gigya-login")){
			 passInputReg = /(?=^\S{6,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}else{
			 passInputReg = /(?=^\S{8,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}
		if(val.indexOf('.') != -1 || val.indexOf(',') != -1 || val.indexOf('~') != -1 || !passInputReg.test(val)){
			errorMsg = 'Please enter a valid Password';
			passState = false;
		}

		return {
			state: passState,
			msg: errorMsg
		};

		if (val.length < 6) {
			return {
				state: false,
				msg: 'Please enter a 6 digit PIN.'
			};
		} else if (isValRepetitive(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that has different digits.'
			};
		} else if (isValSequential(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that is not formed by sequential digits.'
			};
		} else if (isNotNumeric(val)) {
			return {
				state: false,
				msg: 'Enter numbers only'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validatePassportExpDate = function(val, arrivalDate){
		var dateFormatValid = validateDateFormat(val);

		if (! dateFormatValid.state) {
			return dateFormatValid;
		} else {
			var date = parseDate(val);

			// convert passport exp. date to numerical
			var passportExpMonth = new Date(Date.parse(date.month +" 1, 2012")).getMonth() + 1;
			var nowDate = new Date();

			var month = arrivalDate - passportExpMonth;
			if ( (month > 0 && month < 6) || ( (month < 0 || month >= 6) && date.year < parseInt(nowDate.getFullYear())) ) {
				return {
					state: false,
					msg: 'Your passport must be valid for at least six months from your arrival date.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		}
	};

	var validateGuardianAge = function(val) {
		var guardianMinAge = 2;
		var guardianMaxAge = 12;
		var age = computeBirthDate(val);

		if (age < 2) {
			return {
				state: false,
				msg: 'A KrisFlyer member must be at least 2 years old.',
				needsGuardian: false
			};
		} else if (age >= guardianMinAge && age <= guardianMaxAge) {
			return {
				state: true,
				msg: '',
				needsGuardian: true
			};
		} else {
			return validateLegalAge(val);
		}
	};

	var validateLegalAge = function(val) {
		var dateFormat = validateDateFormat(val);

		if (dateFormat.state) {
			// compute the exact birthdate of the user
			var age = computeBirthDate(val);

			if (age < 16) {
				return {
					state: false,
					msg: 'You must be at least 16 years old to be a basic account holder.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var parseDate = function(val) {
		var value = val;
		var fSeparator = value.indexOf('/');
		var lSeparator = value.lastIndexOf('/');

		return {
			day: value.slice(0, fSeparator).trim(),
			month: value.slice(fSeparator + 1, lSeparator).trim(),
			year: value.slice(lSeparator + 1).trim()
		};
	};

	var validateDateFormat = function(val) {
		var date = parseDate(val);
		var yearNow = (new Date()).getFullYear();

		if (date.day == '' && date.month == '' && date.year == '') {
			return {
				state: false,
				msg: 'This date is required'
			};
		} else if (parseInt(date.day) < 1 || parseInt(date.day) > 31) {
			return {
				state: false,
				msg: 'Please enter a valid day'
			};
		} else if (parseInt(date.month) < 0 || parseInt(date.month) == 0 || parseInt(date.month) > 12) {
			return {
				state: false,
				msg: 'Please enter a valid month'
			};
		} else if (isNaN(date.day) || isNaN(date.month) || isNaN(date.year)) {
			return {
				state: false,
				msg: 'Please enter digits from [0-9]'
			};
		} else if (date.year.length < 4) {
			 return {
				state: false,
				msg: 'Please enter a valid year'
			};
		} else if (date.year > yearNow || date.year < yearNow-100){
			return {
				state: false,
				msg: 'Please enter a valid year'
			}
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validateAlphaSpecialChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaSpecialChars = /^[A-Za-z\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, and if required, any of these symbols /\'@()\”\-'
			};
		}
	};

	var validateAlphaNumSpecChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaNumSpecialChars = /^[A-Za-z0-9\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaNumSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, the numbers 0 - 9, and if required, any of these symbols /\'@()”-'
			};
		}
	}

	var addErrState = function(el, inputclss, msgClss, errMsg) {
		el.addClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.addClass(msgClss);
		msgEl.html(errMsg);
	};

	var addValidState = function(el, inputclss, msgClss) {
		el.addClass(inputclss);
	};

	var removeInputState = function(el, inputclss, msgClss) {
		el.removeClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.removeClass(msgClss);
		msgEl.html('');
	};

	var inputValidator = function(el, result, errClss, validClss) {
		if (!result.state) {
			addErrState(el, errClss.input, errClss.msg, result.msg);
		} else {
			removeInputState(el, errClss.input, errClss.msg);
			addValidState(el, validClss);
		}
	};

	return {
		init: init,
		attachEvnt : attachEvnt,
		validateEmail : validateEmail,
		validateEmpty : validateEmpty,
		validatePassword : validatePassword,
		addErrState : addErrState,
		addValidState : addValidState,
		removeInputState : removeInputState,
		inputValidator : inputValidator,
		validateDateFormat: validateDateFormat,
		validateLegalAge: validateLegalAge,
		computeBirthDate:computeBirthDate,
		validateGuardianAge: validateGuardianAge,
		validateAlphaSpecialChars: validateAlphaSpecialChars,
		validateAlphaNumSpecChars: validateAlphaNumSpecChars,
		validatePassportExpDate: validatePassportExpDate,
		parseDate: parseDate,
		isAlphaNumeric: isAlphaNumeric
	};
}();

SIA.Verification = function(){
	var v = {};

	var init = function(){
			v.verificationContainer = $('div#ruLoginContainer');
			v.verificatiobForm = v.verificationContainer.find('form#gigya-resend-verification-code-form');
			v.resendBtn = v.verificatiobForm.find('label.resend-btn');
			v.verificatiobForm.attr('action',v.verificationContainer.attr('data-url'));
			v.verificatiobForm.attr('method',v.verificationContainer.attr('data-method'));
			v.resendBtn.click(function(){
				document.getElementById("gigya-resend-verification-code-form").submit();
			});
		}

	return {
        init: init
    };
}();

SIA.TogglePassword = function(appendEl, input) {
	var p = {};

	p.appendEl = appendEl;
	p.input = input;
	p.iconShow  = 'images/show-eye.png';
 	p.iconHide = 'images/hidden-eye.png';
 	p.icon = '<img data-togglePass class="pin-eye-icon" src="images/show-eye.png" width="auto" heigh="15">';

	var init = function() {
		p.appendEl.css('position', 'relative');

		var icon = $(p.icon);
		p.appendEl.append(icon);
		icon.attr('src', p.iconShow);

		// add default setting to hide password mask
		p.input.attr('type', 'password');

		evnt(p.appendEl.find('[data-togglePass]'), p.input);
	};

	var evnt = function(el, input) {
		el.each(function(i){
			$(this).on('click', function() {
				var inputType = input.eq(i).attr('type');
				var iconEl = input.eq(i).parent().find('[data-togglePass]');

				if (inputType == 'password') {
					input.eq(i).attr('type', 'text');
					iconEl.addClass('show-pincode');
					iconEl.attr('src', p.iconHide);
				} else if (inputType == 'text') {
					input.eq(i).attr('type', 'password');
					iconEl.removeClass('show-pincode');
					iconEl.attr('src', p.iconShow);
				}
			});
		});
	};

	return { init: init };
};

SIA.RadioTab = function() {
    var p = {};

    p.gigyaEl = $('#ruLoginContainer');
    p.gigyaTabs = 'data-radiotab';
    p.gigyaContents = 'data-radiotab-content';

    p.ruErrorCont = $('#ruLoginError');

    var init = function() {
        // show default checked radio
        showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + p.gigyaEl.find('[checked="checked"]').attr(p.gigyaTabs) + '"]'));

        evnt(p.gigyaEl.find('[' + p.gigyaTabs + ']'));
    };

    var evnt = function(el) {
        el.on('click', function() {
            var t = $(this);

            // hide all contents
            p.gigyaEl.find('[' + p.gigyaContents + ']').addClass('hidden');

            // show current select tab content
            showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + t.attr(p.gigyaTabs) + '"]'));

            t.attr('data-radiotab') === '2' ? p.ruErrorCont.removeClass('hidden') : p.ruErrorCont.addClass('hidden')

        });
    };

    var showContent = function(el) {
        el.removeClass('hidden');
    };

    return {
        init: init,
        p: p
    };
}();

SIA.DatePicker = function(){
	var p = {};

	p.datePlaceholder = '<div data-datepicker style="position:absolute;top: 28px;left: 5px;height: 40px;width: 100%;">\
		<input type="text" placeholder="DD" data-day style="padding-left: 8px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="MM" data-month style="padding-left: 5px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="YYYY *" data-year style="padding-left: 5px;background-color:transparent;width: 52px;border: none;text-align: left;height:100%;color: black;">\
	</div>';

	p.inputDay = '[data-day]';
	p.inputMonth = '[data-month]';
	p.inputYear = '[data-year]';

	var init = function(elParent, el) {
		p.dateParentEl = elParent;
		p.dateEl = el;

		customize(p.dateParentEl, p.dateEl);
		evnt(p.dateParentEl);
	};

	var customize = function(elParent, el) {
		elParent.css('position', 'relative');
		elParent.find(el).after($(p.datePlaceholder));
	};

	var transferVal = function() {
		var day = p.dateParentEl.find(p.inputDay).val();
		var month = p.dateParentEl.find(p.inputMonth).val();
		var year = p.dateParentEl.find(p.inputYear).val();
		var separator = '  /  ';

		p.dateEl.val(day + separator + month + separator + year);
	};

	var isInputEmpty = function(el) {
		return el.val().length == 0 ? true : false;
	};

	var removeErrorState = function(el) {
		var gigyaErrorMsgEl = el.find('.gigya-error-msg');
		gigyaErrorMsgEl.html('');
		gigyaErrorMsgEl.removeClass('gigya-error-msg-active');

		el.find('.gigya-error').removeClass('gigya-error');
	};

	var moveInputFocus = function(el) {
		el.on('keyup.moveInputFocus', p.inputDay + ', ' + p.inputMonth  + ', ' + p.inputYear, function() {
			var t = $(this);
			var nxtFocus = t.next().next();

			if ((t.val().length == 2) &&
				nxtFocus.length) {

				nxtFocus.focus();
				removeErrorState(p.dateParentEl);

				if (! isInputEmpty(nxtFocus)) {
					nxtFocus.select();
				}
			}
		});
	};

	var moveKeysFocus = function(el) {
		el.on('keydown', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function(event) {
			var t = $(this);
			var inputFocus = null;
			var inputVal = null;
			var backspaceKey = 8;
			var tabkey = 9;

			// tab key
			if (event.keyCode == tabkey) {
				inputFocus = t.next().next();

				p.dateParentEl.off('keyup.moveInputFocus');

				if (inputFocus.length) {
					event.preventDefault();

					if (isInputEmpty(inputFocus)) {
						inputFocus.focus();
						removeErrorState(p.dateParentEl);

						setTimeout(function() {
							removeErrorState(p.dateParentEl);
						}, 100);
					} else {
						inputFocus.select();
					}
				}
			} else {
				moveInputFocus(p.dateParentEl);
			}

			// backspace key
			if (event.keyCode == backspaceKey && t.val().length == 0) {
				t.prev().prev().focus()
				removeErrorState(p.dateParentEl);
			}
		});
	};

	var evnt = function(elParent) {
		elParent.on('blur', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			// transfer to actual input field
			transferVal();
		});

		elParent.on('click', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			var t = $(this);

			// check if DD || MM || YYYY field is empty, then focus to DD field.
			var fieldParent = t.parent();
			var dayField = fieldParent.find(p.inputDay);
			if ( isInputEmpty(dayField) &&
				isInputEmpty(fieldParent.find(p.inputMonth)) &&
				isInputEmpty(fieldParent.find(p.inputYear)) ) {
					dayField.focus();

					// remove error msg / styles,
					// because it causes an on blur event when changing the focus to DD field
					removeErrorState(elParent);

					return;
			}
		});

		moveInputFocus(elParent);
		moveKeysFocus(elParent);
	};

	return {
		init: init
	};
};

SIA.RULogin = function() {
    var p = {};

    p.formValidator = SIA.FormValidator;

    p.gigyaErrClss = {
        input: 'gigya-error',
        msg: 'gigya-error-msg-active'
    };
    p.gigyaValidClss = 'gigya-valid';

    p.containerName = 'loginContainer';
    p.containerId = $('#' + p.containerName);

    var init = function() {
        //  find email field attached by gigya, save html and remove
        p.emailInput = p.containerId.find('.gigya-login-form .user-email-address input');

        // remove clear button
        p.emailInput.parent().find('.add-clear-text').remove();

         p.formValidator.attachEvnt({
            el: p.emailInput,
            keypress: function() {
                var t = $(this);

                p.formValidator.inputValidator(
                    t,
                    p.formValidator.validateEmail(t.val()),
                    p.gigyaErrClss,
                    p.gigyaValidClss);
            },
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmail(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
         });

        p.passInput = p.containerId.find('.gigya-login-form .user-password input');

     		// add max characters length
			p.passInput.attr('maxlength', '16');

        p.formValidator.attachEvnt({
            el: p.passInput,
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validatePassword(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
        });

        p.ruLoginContainer = $('#ruLoginContainer');
    };

     var onError = function(e){
     	$('#gigya-login-screen .gigya-error-display').prepend('<em class="ico-close-round-fill"></em>');
    };

    var onFieldChanged = function(e){
    	if(e.field === 'loginID') {
    		p.ruLoginError.html('');
    	}
    };

    return {
        init: init,
        p: p,
        onError: onError,
        onFieldChanged: onFieldChanged
    };
}();

SIA.RURegistration = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function(containerID) {
		p.containerName = containerID;
		p.containerId = $('#' + p.containerName);

		p.registrationForm = p.containerId.find('.gigya-register-form');
		p.submitionCheckbox = p.registrationForm.find('[name="subscriptions.terms.email.isSubscribed"]');
		p.countryInput = p.registrationForm.find('[name="profile.country"]');
		p.signupBtn = p.registrationForm.find('.gigya-input-submit');
		p.checkBoxText = p.registrationForm.find('span.gigya-checkbox-text');
		p.checkBoxSingapore = p.registrationForm.find('[name="subscriptions.SQmailer.email.isSubscribed"]');
		p.checkBoxsilkAir = p.registrationForm.find('[name="subscriptions.MImailer.email.isSubscribed"]');
		p.checkBoxPartners = p.registrationForm.find('[name="data.partnerMailer"]');

		p.signupBtn.attr('disabled',true);
		p.signupBtn.addClass('disabled');
		p.checkBoxPartners.attr('disabled',true);
		p.checkBoxPartners.closest('div.gigya-composite-control-checkbox').removeClass('tc-partners');

		p.checkBoxSingapore.click(function(){
			p.checkBoxSingapore.is(':checked') || p.checkBoxsilkAir.is(':checked') ? enableBox(p.checkBoxPartners) : uncheckAndDisable(p.checkBoxPartners);
		});

		p.checkBoxsilkAir.click(function(){
			p.checkBoxSingapore.is(':checked') || p.checkBoxsilkAir.is(':checked') ? enableBox(p.checkBoxPartners) : uncheckAndDisable(p.checkBoxPartners);
		});

		var enableBox = function(checkbox){
			checkbox.attr('disabled',false);
			checkbox.closest('div.gigya-composite-control-checkbox').addClass('tc-partners');
		}

		var uncheckAndDisable = function(checkbox){
			checkbox.attr('disabled',true);
			checkbox.attr('checked',false);
			checkbox.closest('div.gigya-composite-control-checkbox').removeClass('tc-partners');
		}

		p.submitionCheckbox.click(function(){
			p.checkBoxText.css("font-weight", "normal");
			p.signupBtn.attr('disabled',!p.submitionCheckbox.is(':checked'));
			p.submitionCheckbox.is(':checked') ? p.signupBtn.removeClass('disabled') : p.signupBtn.addClass('disabled');
		});

		// validateCountry();

		p.signupBtn.click(function(){
			validateCountry();
		});

		p.countryInput.focusout(function(){
			validateCountry();
		});

		// attach show/hide mask password
	 	p.passInput = p.registrationForm.find('.gigya-input-password');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '16');

		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		validateEmptyFields();

		// firstname, lastname
		addMaxLen();

		// date
		p.dateBirthInput = p.registrationForm.find('[data-gigya-name="data.birthDate"]');

		setTimeout(function() {
			p.dateBirthInput.css('color', 'transparent');
		}, 1000);

		disableDate();

		// customize placeholder
		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.dateBirthInput.parent(), p.dateBirthInput);

		validateDateBirth();

		initTabs();
		disableFirstName();
	};

	var validateCountry = function(){
		if(p.countryInput.val() == "value"){
			p.formValidator.inputValidator(
				p.countryInput,
				{state:false, msg:"Select country"},
				p.gigyaErrClss,
				p.gigyaValidClss);
			
		}else{
			p.formValidator.inputValidator(
				p.countryInput,
				{state:true, msg:"Select country"},
				p.gigyaErrClss,
				p.gigyaValidClss);
		}
	};

	var addMaxLen = function() {
		p.registrationForm.find('[data-gigya-name="profile.firstName"]').attr('maxlength', '50');
		p.registrationForm.find('[data-gigya-name="profile.lastName"]').attr('maxlength', '30');
	};

	var disableFirstName = function() {
		var firstName = p.registrationForm.find('[data-gigya-name="profile.firstName"]');
		var noFirstName = p.registrationForm.find('[data-gigya-name="data.noFirstName"]');
		var asteriskLabel = firstName.prev().find('label');

		noFirstName.on('click', function() {
			if (noFirstName.prop('checked')) {
				// disable
				firstName.attr('disabled', true);

				firstName.addClass('firstname-disabled');

				// remove evnt
				firstName.off();

				firstName.val('');
				firstName.removeAttr('placeholder');

				// hide asterisk on label
				asteriskLabel.addClass('hidden-asterisk');

				// just in case there is an error msg, remove it
				var errorEl = firstName.parent().find('.gigya-error-msg');
				errorEl.removeClass('gigya-error-msg-active');
				errorEl.html('');
			} else {
				// enable
				firstName.attr('disabled', false);
				firstName.attr('placeholder', 'First Name *');

				firstName.removeClass('firstname-disabled');

				// show asterisk on label
				asteriskLabel.removeClass('hidden-asterisk');

				setTimeout(function() {
					asteriskLabel.removeClass('gigya-hidden');
				},100);

				// attach validator
				firstNameValidator();
			}
		});
	};

	var firstNameValidator = function() {
		p.formValidator.attachEvnt({
			el: p.registrationForm.find('[data-gigya-name="profile.firstName"]'),
			blur: function() {
			var t = $(this);

 			if(t.val() == '') {
 				p.formValidator.inputValidator(
					t,
					p.formValidator.validateEmpty(t.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
 				}
			}
		});
	};

	var initTabs = function(){
		p.tabContainer = $('#ruTabs');
		p.tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					p.tabContainer.find('.tab-item').removeClass('active');
					p.tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					p.tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	};

	// remove placeholder
	var disableDate = function() {
		p.dateBirthInput.attr('placeholder', '');
		p.dateBirthInput.attr('readonly', true);
		p.dateBirthInput.css('color', 'white');
	};

	// firstname, lastname, password, country & email
	var validateEmptyFields = function() {
		var inputs = [
			p.registrationForm.find('[data-gigya-name="profile.firstName"]'),
			p.registrationForm.find('[data-gigya-name="profile.lastName"]'),
			p.registrationForm.find('[data-gigya-name="email"]'),
			p.registrationForm.find('[data-gigya-name="profile.country"]'),
			p.passInput
		];

		for (var i = 0, iLen = inputs.length; i < iLen; i++) {
			p.formValidator.attachEvnt({
				el: inputs[i],
				blur: function() {
				var t = $(this);

	 			if(t.val() == '') {
	 				p.formValidator.inputValidator(
						t,
						p.formValidator.validateEmpty(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	 				}
				}
			});
		}
	};

	// date
	var validateDateBirth = function() {
		var dateBirthFields = [
			p.registrationForm.find('[data-day]'),
			p.registrationForm.find('[data-month]'),
			p.registrationForm.find('[data-year]')
		];

		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					p.formValidator.inputValidator(
						p.dateBirthInput,
						p.formValidator.validateLegalAge(p.dateBirthInput.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});

			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}
		}

		p.formValidator.attachEvnt({
			el: p.dateBirthInput,
			focus: function() {
				p.formValidator.inputValidator(
					p.dateBirthInput,
					p.formValidator.validateLegalAge(p.dateBirthInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});
	};

	return {
		init: init
	};
}();

SIA.RURegIntermediate = function() {
	var p = {};

	p.formValidator = SIA.FormValidator;

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function () {

	};

	var onAfterScreenLoad = function() {
		p.container = $('#gigyaRegistrationIntermediate');

		// move container position to top
		p.container.closest('.ru-reg-intermediate .popup__inner').css('top', '319px');

		// email
		p.formValidator.attachEvnt({
         el: p.container.find('[data-gigya-name="profile.email"]'),
         keypress: function() {
       		var t = $(this);

          	p.formValidator.inputValidator(
           		t,
           		p.formValidator.validateEmail(t.val()),
           		p.gigyaErrClss,
              	p.gigyaValidClss);
         },
         blur: function() {
             var t = $(this);

             if (t.val() == '') {
                 	p.formValidator.inputValidator(
                     t,
                     p.formValidator.validateEmpty(t.val()),
                     p.gigyaErrClss,
                     p.gigyaValidClss);
             } else {
              	p.formValidator.inputValidator(
                  t,
                  p.formValidator.validateEmail(t.val()),
                  p.gigyaErrClss,
                  p.gigyaValidClss);
             }
         }
   	});

		// attach show/hide mask password
	 	p.passInput = p.container.find('[data-gigya-name="password"]');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '6');

		// password
		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		// add max characters length
		p.passInput.attr('maxlength', '6');
	};

	var onError = function() {
		var errorMsgEl = p.container.find('.gigya-error-display');
		// p.container.find('.gigya-error-display').remove();
    //
		// errorMsgEl.prepend('<em class="ico-close-round-fill"></em>');
		// p.container.find('.gigya-profile-form h2').after(errorMsgEl);
    //
		// p.container.find('[type="submit"]').on('click', function() {
		// 	p.container.find('.gigya-error-display .ico-close-round-fill').remove();
    //
		// 	p.container.find('form').trigger('submit');
		// });
		p.container.find('.gigya-error-display .ico-close-round-fill').remove();
		errorMsgEl.prepend('<em class="ico-close-round-fill"></em>');
	};

	var onBeforeSubmit = function () {
		var passInput = $('.gigya-profile-form').find('[data-gigya-name="password"]');
		var passwordValid = p.formValidator.validatePassword($(passInput[2]).val());

		return passwordValid.state;
	};

	return {
		init: init,
		onAfterScreenLoad: onAfterScreenLoad,
		onError: onError,
		onBeforeSubmit: onBeforeSubmit
	};
}();

SIA.ProfileUpdate = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var afterScreenLoad = function() {
		p.container = $('#changePassword');

		p.passInput = p.container.find('[data-screenset-element-id="gigya-password-passwordRetype"]');

		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);

		togglePass.init();

		p.passInput.attr('maxlength', '6');

		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});
	};

	var error = function() {
		var formError = p.container.find('.gigya-composite-control-form-error');
		formError.find('.ico-close-round-fill').remove();
		formError.prepend('<em class="ico-close-round-fill"></em>');
	};

	var beforeSubmit = function() {
		var passwordValid = p.formValidator.validatePassword($(p.passInput).val());

		return passwordValid.state;
	};

	return {
		afterScreenLoad : afterScreenLoad,
		error: error,
		beforeSubmit: beforeSubmit
	};
}();

SIA.JoinKrisFlyer = function() {
	var p = {};

	p.container = $('#joinKrisflyerTab');
	p.formValidator = SIA.FormValidator;
	p.gigyaErrClss = {
     	input: 'gigya-error',
      msg: 'gigya-error-msg-active'
   };
   p.gigyaValidClss = 'gigya-valid';

	var init = function() {
		p.firstName = p.container.find('#personal-infor-first-name');
		p.lastName = p.container.find('#lastName');
		p.preferredName = p.container.find('#preferredName');
		p.noFirstName = p.container.find('#personal-infor-first-name-cb');
		p.birthdate = p.container.find('#dateOfBirth');
		p.postCode = p.container.find('#postCode');
		p.promotionCode = p.container.find('#promotionCode');
		p.address = p.container.find('#addressLine');
		p.guardianFirstName = p.container.find('#guardian-infor-first-name');
		p.guardianLastname = p.container.find('#guardianlastName');
		p.guardianNoFirstName = p.container.find('#guardian-infor-first-name-cb');
		p.answer = p.container.find('#answer');
		p.membership = $('#membership-number');
		p.membership.attr('maxlength',10);
		p.membership.on('input',function(evt){
			evt = (evt) ? evt : window.event;

			var charCode = (evt.which) ? evt.which : evt.keyCode;
			var value = $(this).val();
			var charCodeAt = value.charCodeAt(value.length-1);

			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			} 
			
			if (charCodeAt > 31 && (charCodeAt < 48 || charCodeAt > 57) || isNaN(charCodeAt) || value.length >= 11) {
				$(this).val($(this).val().substring(0,$(this).val().length-1));
				return false;
			}

			return true;
		})

		updateKrisFlyerCard([
				p.firstName,
				p.lastName
			],
			p.preferredName
		);

		noFirstName(p.noFirstName, p.firstName, p.lastName, p.preferredName, '[for="personal-infor-first-name"]');
		noFirstName(p.guardianNoFirstName, p.guardianFirstName, p.guardianLastname, '', '[for="guardian-infor-first-name"]');
		dateOfBirth(p.birthdate);

		alphaNumericInput([
			p.postCode,
			p.promotionCode
		]);

		alphaSpecialCharsInput();
		alphaNumSpecChars(p.answer);

		inputAddressAdder(p.address);

		setTimeout(function() {
			p.birthdate.parent().find('.add-clear-text').remove()
		}, 1000);
	};

	var updateKrisFlyerCard = function(input, preferredName) {
		for (var i = 0, Ilen = input.length; i < Ilen; i++) {
			$(input[i]).on('blur', function() {
				updatePreferredName($(input[0]).val(), $(input[1]).val(), preferredName);
			});
		}
	};

	var updatePreferredName = function(firstName, lastName, preferredName) {
		preferredName.val(lastName + ((firstName.length === 0) ? '' : (' ' + firstName)) );
	};

	var noFirstName = function(input, firstName, lastName, preferredName, firstNameLabelAsterisk) {
		input.on('click', function() {
			// get first name label
			var firstNameLabel = firstName.closest('.grid-inner').find(firstNameLabelAsterisk);
			var val = firstNameLabel.html();
			var len =  val.length;

			if (input.prop('checked')) {
				// remove asterisk(*) as required
				firstNameLabel.html(val.slice(0, len - 1));

				firstName.attr('disabled', true);
				firstName.val('');
				firstName.parent().addClass('firstname-disabled');
				removeErrorInput(firstName);

				if (preferredName != ''){
					updatePreferredName('', lastName.val(), preferredName);
				}
			} else {
				// add asterisk(*) to set required
				firstNameLabel.html(val + '*');

				firstName.attr('disabled', false);
				firstName.parent().removeClass('firstname-disabled');
			}
		});
	};

	var dateOfBirth = function(birthdateInput) {
		var dateParent = birthdateInput.parent();
		var dateBirth = new SIA.DatePicker();
		dateBirth.init(dateParent, birthdateInput);

		var dateBirthFields = [
			dateParent.find('[data-day]'),
			dateParent.find('[data-month]'),
			dateParent.find('[data-year]')
		];

		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					var ageResult = p.formValidator.validateGuardianAge(birthdateInput.val());
					p.formValidator.inputValidator(
						birthdateInput,
						ageResult,
						p.gigyaErrClss,
						p.gigyaValidClss);

					showGuardian(ageResult);
					addErrClss(birthdateInput, ageResult);
				}
			});

			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}
		}

		p.formValidator.attachEvnt({
			el: birthdateInput,
			focus: function() {
				var ageResult = p.formValidator.validateGuardianAge(birthdateInput.val());
				p.formValidator.inputValidator(
					birthdateInput,
					ageResult,
					p.gigyaErrClss,
					p.gigyaValidClss);

				showGuardian(ageResult);
				addErrClss(birthdateInput, ageResult);
			}
		});

		birthdateInput.css('color', 'transparent');

		// adjust position top
		dateParent.find('[data-datepicker]').css('top', '0');
	};

	var addErrClss = function(input, age) {
		var input = input.parent();
		if (!age.state) {
			input.addClass('input-error');
		} else {
			input.removeClass('input-error');
		}
	};

	var showGuardian = function(val) {
		var guardianSection = p.container.find('.guardian-section');
		if (val.needsGuardian) {
			guardianSection.removeClass('hidden');
			$(".spacer-1").addClass('guadian-section-active');
		} else {
			guardianSection.addClass('hidden');
			$(".spacer-1").removeClass('guadian-section-active');
		}
	};

	var generateList = function(from, to) {
		var list = [];
		for (var i = from; i <= to; i++) {
			list.push(i);
		}

		return list;
	};

 	var alphaNumericInput = function(inputs) {
		p.keyCodeList = $.merge($.merge(generateList(48, 57), generateList(65, 90)), generateList(97, 122));

		for (var i = 0, iLen = inputs.length; i < iLen; i++) {
			$(inputs[i]).on('keypress', function(e) {
				// english letters / numbers only
				// http://www.theasciicode.com.ar/ascii-printable-characters/capital-letter-z-uppercase-ascii-code-90.html
				if(p.keyCodeList.indexOf(e.which) <= -1) {
					return false;
				}
			});
		}
	};

	var alphaSpecialCharsInput = function() {
		p.firstName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.lastName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.preferredName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.guardianFirstName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.guardianLastname.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});
	};

	var alphaNumSpecChars = function (el) {
		el.on('blur', function() {
			var val = $(this).val();

			var isEmpty = p.formValidator.validateEmpty(val);
			if (! isEmpty.state) {
				addErrorInput(el, isEmpty.msg);
				return;
			}

			var validChars = p.formValidator.validateAlphaNumSpecChars(el);
			if (! validChars.state){
				addErrorInput(el, validChars.msg);
				return;
			} else {
				removeErrorInput(el);
				return;
			}
		});
	}

	var validateAlphaSpecialChars = function(el) {
		var t = el;
		var val = t.val();

		var isEmpty = p.formValidator.validateEmpty(val);
		var validVal  = p.formValidator.validateAlphaSpecialChars(el);
		if (!isEmpty.state && t.attr('id') == 'preferredName') {
			return;
		}

		if (!isEmpty.state){
			addErrorInput(el, isEmpty.msg);
			return;
		}

		if(!validVal.state) {
			addErrorInput(el, validVal.msg);
			return;
		} else {
			removeErrorInput(el);
			return;
		}
	};

	var addErrorInput = function(el, msg) {
		el.parent().addClass('input-error');
		el.closest('.grid-inner').find('.gigya-error-msg').html(msg);
	};

	var removeErrorInput = function(el) {
		el.parent().removeClass('input-error');
		el.closest('.grid-inner').find('.gigya-error-msg').html('');
	};

	var inputAddressAdder = function(el) {
		var btn = el.find('#addAddressLine');

		btn.on('click', function(e) {
			e.preventDefault();

			var t = $(this);
			var max = parseInt(el.attr('data-addressline-max'));
			var currentAddressLine = el.find('[data-addressline-default]').length;

			if (currentAddressLine <= max) {
				var newIndex = currentAddressLine + 1;
				var addressLine = $(el.find('.additional-address-cont')[0]).clone();
				var addressLineEl = addressLine.find('input');

				var placeholder = addressLineEl.attr('placeholder');
				addressLineEl.attr('placeholder', placeholder.slice(0, -1) + newIndex);

				var idname = addressLineEl.attr('id');
				var newIdName = idname.slice(0, -1) + newIndex;
				addressLineEl.attr('id', newIdName);
				addressLineEl.attr('name', newIdName);

				var ariaLabel = addressLineEl.attr('aria-labelledby');
				var currentIndexPos = ariaLabel.indexOf('2');
				var newAriaLabel = ariaLabel.slice(0, currentIndexPos) + newIndex + ariaLabel.slice(currentIndexPos + 1);
				addressLineEl.attr('aria-labelledby', newAriaLabel);

				addressLineEl.val('');

				el.find('.additonal-address-line').before(addressLine);

				if (newIndex == max) {
					btn.closest('.additonal-address-line').addClass('hidden');
				}
			}
		});
	};

	return {
		init: init
	};
}();

SIA.DataProfileEdit = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var falseCounter = 0;

	var init = function(){
		p.containerId = $('#profileEditContainer');
		p.inputs = []
		p.formContainer = p.containerId.find(".gigya-profile-form");
		p.inputs.push({el: p.formContainer.find('[name="profile.firstName"]'), errMsg:"Enter first/given name"});
		p.inputs.push({el: p.formContainer.find('[name="profile.lastName"]'), errMsg:"Enter last/family name"});
		p.inputs.push({el: p.formContainer.find('[name="data.birthDate"]'), errMsg:"Enter date of birth"});
		p.inputs.push({el: p.formContainer.find('[name="profile.country"]'), errMsg:"Select country"});
		p.inputs.push({el: p.formContainer.find('[name="profile.email"]'), errMsg:"This field is required"});
		p.noFirstName = p.formContainer.find('[name="data.noFirstName"]');
		p.saveBtn = p.formContainer.find("input.gigya-input-submit");
		p.aggreeCheckBox = p.formContainer.find('[name="subscriptions.terms.email.isSubscribed"]');
		p.inputs[3].el.val("value2");
		p.checkBoxText = p.formContainer.find('span.gigya-checkbox-text');

		setTimeout(function() {
			p.inputs[2].el.css('color', 'transparent');
		}, 1000);

		disableDate();

		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.inputs[2].el.parent(), p.inputs[2].el);

		validateDateBirth();
		p.saveBtn.attr('disabled',true);
		p.saveBtn.addClass('disabled');
		
		p.formValidator.attachEvnt({
			el: p.saveBtn,
			click: function(evt){
				validatorFunction(p.inputs[4].el,p.inputs[4]);
			}
		});	

		// p.formValidator.attachEvnt({
		// 	el: p.saveBtn,
		// 	click: function(evt){
		// 		setTimeout(function(){ 
		// 			falseCounter = 0;
		// 			_.each(p.inputs, function(inputData){
		// 				validatorFunction(inputData.el,inputData);
		// 			});
		// 		}, 800);
		// 	}
		// });

		p.inputs[3].el.focusout(function(){
			if(p.inputs[3].el.val() == "value"){
				formValidatorAdder(p.inputs[3].el,{state:false, msg:p.inputs[3].errMsg});
			}else{
				formValidatorAdder(p.inputs[3].el,{state:true, msg:p.inputs[3].errMsg});	
			}
		});

		p.aggreeCheckBox.click(function(){
			falseCounter = 0;
			p.checkBoxText.css("font-weight", "normal");
			p.saveBtn.attr('disabled',!p.aggreeCheckBox.is(":checked"));
			p.aggreeCheckBox.is(":checked") ? p.saveBtn.removeClass('disabled') : p.saveBtn.addClass('disabled');
		});

		p.noFirstName.click(function(){
			p.inputs[0].el.attr('disabled',p.noFirstName.is(":checked"));
			p.inputs[0].el.val("");
			$(this).is(":checked")? $(this).closest('.gigya-composite-control').addClass("first-name-checkbox") : $(this).closest('.gigya-composite-control').removeClass("first-name-checkbox");
			setTimeout(function() {
				p.noFirstName.is(":checked")? p.inputs[0].el.attr('placeholder', '') : p.inputs[0].el.attr('placeholder', 'First name');
				formValidatorAdder(p.inputs[0].el,{state:true, msg:p.inputs[0].errMsg});
			}, 100);
		});
		
		
		_.each(p.inputs,function(inputData){
			p.formValidator.attachEvnt({
				el: inputData.el,
				blur: function(){
					validatorFunction($(this),inputData);
				}
			});
		});
		
	}

	var disableDate = function() {
		p.inputs[2].el.attr('placeholder', '');
		p.inputs[2].el.attr('readonly', true);
		p.inputs[2].el.css('color', 'white');
	};

	var validateDateBirth = function() {
		var dateBirthFields = [
			p.formContainer.find('[data-day]'),
			p.formContainer.find('[data-month]'),
			p.formContainer.find('[data-year]')
		];
		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						p.formValidator.validateLegalAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}

			p.formValidator.attachEvnt({
				el: p.inputs[2].el,
				focus: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						p.formValidator.validateLegalAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
		}
	};

	var beforeSubmit = function(){
		falseCounter = 0;
		var status = false;
		_.each(p.inputs, function(inputData){
			validatorFunction(inputData.el,inputData);
		});
		formValidatorAdder(p.inputs[2].el,p.formValidator.validateLegalAge(p.inputs[2].el.val()));
		
		if(!falseCounter){
			status = true;
		}
		return status;
	}

	var formValidatorAdder = function(element,validatorData){
		p.formValidator.inputValidator(
			element,
			validatorData,
			p.gigyaErrClss,
			p.gigyaValidClss
		);
		if(!validatorData.state){
			falseCounter++;
		}
	}

	var validatorFunction = function(el,inputData){
		var element = el;
		if(element.val() == '' && !element.attr('disabled')) {
			
			formValidatorAdder(element,emptyInputValidator(inputData));

		}else{
			if((element.attr('id') == "gigya-textbox-24751563586290030" || element.attr('id') == "gigya-textbox-123595718365923790") && !element.attr('disabled')){
				
				formValidatorAdder(element,p.formValidator.validateAlphaSpecialChars(el));
			
			}else if(element.attr('id') == "gigya-dropdown-72948804609574500" && element.val() == "value"){

				formValidatorAdder(element,{state:false, msg: "Select country"});

			}
		}
		
	}

	var emptyInputValidator = function(inputData){
		validatorResult = p.formValidator.validateEmpty(inputData.el.val());
		validatorResult.msg = inputData.errMsg;
		return validatorResult;
	}
	
	
	return {
		init: init,
		onBeforeSubmit:beforeSubmit,
		formValidatorAdder:formValidatorAdder
	}
}();

SIA.EditPhonePin = function() {
	var p = {};
	var falseCounter = 0;
	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function(){
		p.container = $('#editPhonePin');
		p.editBtn = $('[data-toggle-screenset="editPhonePin"]');
		p.formContainer =  p.container.find('.gigya-edit-phone-pin-form');
		
		p.oldPin = p.formContainer.find('#oldPin');
		p.newPin = p.formContainer.find('#newPin');
		
		p.oldPinSpan = p.oldPin.closest('span.oldPin');
		p.newPinSpan = p.newPin.closest('span.newPin');
		
		p.oldPinSpan.removeClass('message-error');
		p.newPinSpan.removeClass('message-error');
		p.newErrorSpan = $('span.oldPin');
		p.retryErrorSpan = $('span.newPin');
		p.newErrorSpan.text("");
		p.retryErrorSpan.text("");
p.cancelBtn = p.container.find('.cancel-btn');

		p.cancelBtn.click(function(e){
			e.preventDefault();
			p.container.addClass('hidden');
			p.editBtn.removeClass('hidden');
		});


		p.saveBtn = p.container.find('#submit-1');
		p.checkBox = p.formContainer.find('#edit-phone-policy');
		p.saveBtn.attr('disabled', true);
		p.saveBtn.addClass('disabled');

		p.cancelBtn = p.container.find('.cancel-btn');

		p.cancelBtn.click(function(e){
			e.preventDefault();
			p.container.addClass('hidden');
			p.editBtn.removeClass('hidden');
		});

		p.checkBox.click(function(){			
			p.saveBtn.attr('disabled',!p.checkBox.is(":checked"));
			p.saveBtn.hasClass('disabled') ? p.saveBtn.removeClass('disabled') : p.saveBtn.addClass('disabled');
		});

		
		new SIA.TogglePassword(p.oldPin.parent(),p.oldPin).init();
		new SIA.TogglePassword(p.newPin.parent(),p.newPin).init();
	

		

		p.editBtn.on('click', function(e){
			e.preventDefault();
			$(this).addClass('hidden');
			p.container.removeClass('hidden');
		});

		p.saveBtn.click(function(e){
			e.preventDefault();
			var submit = true;
			if(p.oldPin.val() == ""){
				p.oldPinSpan.addClass('message-error');
				p.newErrorSpan.text("Enter a valid password");					
				p.oldPin.parent('span').addClass('error-message');
				submit = false;
			}
			if(p.newPin.val() == ""){
				p.newPinSpan.addClass('message-error');
				p.retryErrorSpan.text("Enter a valid password");
				p.newPin.parent('span').addClass('error-message');
				submit = false;
			}
			if(submit && !p.oldPinSpan.hasClass('message-error') && !p.newPinSpan.hasClass('message-error') ){
				p.formContainer.submit();
			}
		});

		p.oldPin.blur(function(){
			var validate = validatePassword(p.oldPin);
			if(!validate.state){
				p.oldPinSpan.addClass('message-error');
				p.newErrorSpan.text(validate.msg);

				p.oldPin.parent('span').addClass('error-message');
			}else{
				p.oldPinSpan.removeClass('message-error');
				p.oldPin.parent('span').removeClass('error-message');
				p.newErrorSpan.text("");
			}
		});
		

		p.newPin.blur(function(){
			var validate = validatePassword(p.newPin);
			if(!validate.state){
				p.newPinSpan.addClass('message-error');
				p.retryErrorSpan.text(validate.msg);
				p.newPin.parent('span').addClass('error-message');
			}else{

				p.newPin.parent('span').removeClass('error-message');
				p.newPinSpan.removeClass('message-error');
				p.retryErrorSpan.text("");
				if(p.newPin.val() != p.oldPin.val()){
					p.newPinSpan.addClass('message-error');
					p.retryErrorSpan.text("Password did not match");
				}
			}
		});
	}

	var validatePassword = function(input) {
		var passInputReg;
		var errorMsg = '';
		var passState = true;

		
		passInputReg = /\d{6,6}/;
		if (hasSequential(input.val()) ||  hasSequential(reverseString(input.val()))) {
			errorMsg = 'Please do not use consecutive numbers (eg 123456) or repetitive numbers (eg 999999).';
			passState = false;
		}

		var pinInputRepReg = /^([0-9])\1{5}$/;

		if (pinInputRepReg.test(input.val())) {
			errorMsg = 'Please do not use consecutive numbers (eg 123456) or repetitive numbers (eg 999999).';
			passState = false;	
		}		
		if(input.val().indexOf('.') != -1 || input.val().indexOf(',') != -1 || input.val().indexOf('~') != -1 || !passInputReg.test(input.val())){
			errorMsg = 'Please enter a 6 digit PIN';
			passState = false;
		}

		

		return {
			state: passState,
			msg: errorMsg
		};
	}

	var hasSequential = function(str) {
		var baseString = "012345678901234";
		return baseString.indexOf(str) >= 0 ? true:false;
	}

	var reverseString = function(str) {
		var splitString = str.split(""); 
		var reverseArray = splitString.reverse(); 
		var joinArray = reverseArray.join("");
    	return joinArray;
	}
	
	
	return {
		init: init
	}
}();


SIA.DataPasswordUpdate = function() {	
	var p = {};
	var falseCounter = 0;
	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;
	
		/*$(document).on('click','[data-toggle-screenset="updatePassword"]', function(e){
			e.preventDefault();
			$(this).addClass('hidden');			
			$("div#updatePassword").removeClass('hidden');
		});

		var cancelBtn = $('[data-translation-key="LABEL_33050880146403732_LABEL"]');

		cancelBtn.click(function(e){
			e.preventDefault();
			$("div#updatePassword").addClass('hidden');
			$('[data-toggle-screenset="updatePassword"]').removeClass('hidden');
		});
*/
	var init = function(){		
		p.inputs = [];
		p.container = $('#updatePassword');
		p.formContainer = p.container.find(".gigya-reset-password-form, .gigya-profile-form");
		p.editBtn = $('[data-toggle-screenset="updatePassword"]');		
		p.inputs.push({el: p.formContainer.find('[name="oldPassword"]'), errMsg:"Enter a valid password"});
		p.inputs.push({el: p.formContainer.find('[name="newPassword"]'), errMsg:"Enter a valid password"});
		p.inputs.push({el: p.formContainer.find('[name="passwordRetype"]'), errMsg:"Enter a valid password"});
		p.termCheckbox = p.formContainer.find('.gigya-input-checkbox');
		p.submitBtn = $('.gigya-input-submit');
		p.submitBtn.attr('disabled',true);
		p.submitBtn.addClass('disabled');
		p.checkBoxText = $('span.gigya-checkbox-text');

	
		_.each(p.inputs,function(inputData){
			new SIA.TogglePassword(inputData.el.parent(),inputData.el).init();
		});
		p.cancelBtn = $('[data-translation-key="LABEL_33050880146403732_LABEL"]');

		$(document).on('click','[data-translation-key="LABEL_33050880146403732_LABEL"]',function(e){
			e.preventDefault();
			p.container.addClass('hidden');
			p.editBtn.removeClass('hidden');
		});

		$(document).on('click','[data-toggle-screenset="updatePassword"]', function(e){
			e.preventDefault();
			console.log('edit');
			p.editBtn.addClass('hidden');			
			p.container.removeClass('hidden');
		});

		p.termCheckbox.click(function(){
			p.submitBtn.attr('disabled',!p.termCheckbox.is(":checked"));
			p.submitBtn.hasClass('disabled') ? p.submitBtn.removeClass('disabled') : p.submitBtn.addClass('disabled');
			p.checkBoxText.css("font-weight", "normal");
		});

		p.submitBtn.click(
			function(){
				formValidatorAdder(p.inputs[0].el,errorMsgUpdator(p.inputs[0].errMsg,validatePassword(p.inputs[0].el)));
				formValidatorAdder(p.inputs[2].el,errorMsgUpdator(p.inputs[2].errMsg,validatePassword(p.inputs[2].el)));
			}
		);

		_.each(p.inputs,function(inputData){
			p.formValidator.attachEvnt({
				el: inputData.el,
				blur: function(e){
					formValidatorAdder($(this),errorMsgUpdator(inputData.errMsg,validatePassword($(this))));
				}
			});
		});
	}

	var errorMsgUpdator = function(errMsg,validatorFunction){
		validatorFunction.msg = errMsg;
		return validatorFunction;
	}

	var validatePassword = function(input) {
		var passInputReg;
		var errorMsg = '';
		var passState = true;

		if(input.attr('name') == "oldPassword"){
			 passInputReg = /(?=^\S{6,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}else{
			 passInputReg = /(?=^\S{8,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}
		if(input.val().indexOf('.') != -1 || input.val().indexOf(',') != -1 || input.val().indexOf('~') != -1 || !passInputReg.test(input.val())){
			errorMsg = 'Please enter a valid Password';
			passState = false;
		}

		return {
			state: passState,
			msg: errorMsg
		};
	}
	
	var beforeSubmit = function(){
		falseCounter = 0;
		var status = false;
		_.each(p.inputs, function(inputData){
			formValidatorAdder(inputData.el,errorMsgUpdator(inputData.errMsg,validatePassword(inputData.el)));
		});
		
		if(!falseCounter){
			status = true;
		}
		return status;
	}

	var formValidatorAdder = function(element,validatorData){
		p.formValidator.inputValidator(
			element,
			validatorData,
			p.gigyaErrClss,
			p.gigyaValidClss
		);
		if(!validatorData.state){
			falseCounter++;
		}
	}

	return {
		init: init,
		onBeforeSubmit:beforeSubmit
	}
}();

SIA.AccountCreatePassword = function() {
	var p = {};
	var falseCounter = 0;
	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function(){
		p.container = $('#createPassword');
		p.createBtn = $('[data-toggle-screenset="createPassword"]');
		p.formContainer =  p.container.find('#form--login');
		p.newPass = p.formContainer.find('#newPass');
		p.retypePass = p.formContainer.find('#retypePass');
		p.saveBtn = $('#submit-1');
		p.checkBox = $('#change-payment-currency');
		p.saveBtn.attr('disabled', true);
		p.saveBtn.addClass('disabled');
		p.newPassSpan = p.newPass.closest('span.input-1');
		p.retypePassSpan = p.retypePass.closest('span.input-1');
		p.newPassSpan.removeClass('error-message');
		p.retypePassSpan.removeClass('error-message');
		p.newErrorSpan = $('span.newPass');
		p.retryErrorSpan = $('span.retypePass');
		p.newErrorSpan.text("");
		p.retryErrorSpan.text("");
p.cancelBtn = p.container.find('.cancel-btn');

		p.cancelBtn.click(function(e){
			e.preventDefault();
			p.container.addClass('hidden');
			p.createBtn.removeClass('hidden');
		});
		
		new SIA.TogglePassword(p.newPass.parent(),p.newPass).init();
		new SIA.TogglePassword(p.retypePass.parent(),p.retypePass).init();
	

		p.checkBox.click(function(){
			p.saveBtn.attr('disabled',!p.checkBox.is(":checked"));
			p.saveBtn.hasClass('disabled') ? p.saveBtn.removeClass('disabled') : p.saveBtn.addClass('disabled');
		});

		p.createBtn.on('click', function(e){
			e.preventDefault();
			$(this).addClass('hidden');
			p.container.removeClass('hidden');
		});

		p.saveBtn.click(function(e){
			e.preventDefault();
			var submit = true;
			if(p.newPass.val() == ""){
				p.newPassSpan.addClass('error-message');
				p.newErrorSpan.text("Enter a valid password");
				submit = false;
			}
			if(p.retypePass.val() == ""){
				p.retypePassSpan.addClass('error-message');
				p.retryErrorSpan.text("Enter a valid password");
				submit = false;
			}
			if(submit && !p.newPassSpan.hasClass('error-message') && !p.retypePassSpan.hasClass('error-message') ){
				p.formContainer.submit();
			}
		});

		p.newPass.keyup(function(){
			if(!validatePassword(p.newPass).state){
				p.newPassSpan.addClass('error-message');
				p.newErrorSpan.text("Enter a valid password");
			}else{
				p.newPassSpan.removeClass('error-message');
				p.newErrorSpan.text("");
			}
		});

		p.newPass.blur(function(){
			if(!validatePassword(p.newPass).state){
				p.newPassSpan.addClass('error-message');
				p.newErrorSpan.text("Enter a valid password");
			}else{
				p.newPassSpan.removeClass('error-message');
				p.newErrorSpan.text("");
			}
		});
		
		p.retypePass.keyup(function(){
			if(!validatePassword(p.retypePass).state){
				p.retypePassSpan.addClass('error-message');
				p.retryErrorSpan.text("Enter a valid password");
			}else{
				p.retypePassSpan.removeClass('error-message');
				p.retryErrorSpan.text("");
				if(p.retypePass.val() != p.newPass.val()){
					p.retypePassSpan.addClass('error-message');
					p.retryErrorSpan.text("Password did not match");
				}
			}
		});

		p.retypePass.blur(function(){
			if(!validatePassword(p.retypePass).state){
				p.retypePassSpan.addClass('error-message');
				p.retryErrorSpan.text("Enter a valid password");
			}else{
				p.retypePassSpan.removeClass('error-message');
				p.retryErrorSpan.text("");
				if(p.retypePass.val() != p.newPass.val()){
					p.retypePassSpan.addClass('error-message');
					p.retryErrorSpan.text("Password did not match");
				}
			}
		});
	}

	var validatePassword = function(input) {
		var passInputReg;
		var errorMsg = '';
		var passState = true;

		if(input.attr('name') == "oldPassword"){
			 passInputReg = /(?=^\S{6,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}else{
			 passInputReg = /(?=^\S{8,16}$)(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*?\W)/;
		}
		if(input.val().indexOf('.') != -1 || input.val().indexOf(',') != -1 || input.val().indexOf('~') != -1 || !passInputReg.test(input.val())){
			errorMsg = 'Please enter a valid Password';
			passState = false;
		}

		return {
			state: passState,
			msg: errorMsg
		};
	}
	
	return {
		init: init
	}
}();

SIA.SocialMediaLink = function(){
	var container;
	var context = {
		msg:'This is my params.context.msg'
	}; 
	 
	var params = {
		captionText:'This is my caption text',
		showTermsLink:false,
		context:context,
		containerID: '',
		autoLogin: true,
        enabledProviders: 'facebook,google,linkedin',
		onLoad: function(e){
			var fb = container.find('.gig-edit-connections-social-provider-image img[src*="facebook"]');
			var ggl = container.find('.gig-edit-connections-social-provider-image img[src*="google"]');
			var li = container.find('.gig-edit-connections-social-provider-image img[src*="linkedin"]');
			fb.attr('src', 'images/smii-social-fb.png');
			ggl.attr('src', 'images/smii-social-ggl.png');
			li.attr('src', 'images/smii-social-li.png');
	
			container.removeAttr('style');
		}
	};

	var replaceSocialImages = function(e){
		setTimeout(function(){
			var fb = container.find('.gig-edit-connections-social-provider-image img[src*="facebook"]');
			var ggl = container.find('.gig-edit-connections-social-provider-image img[src*="google"]');
			var gglplus = container.find('.gig-edit-connections-social-provider-image img[src*="googleplus"]');
			var li = container.find('.gig-edit-connections-social-provider-image img[src*="linkedin"]');
			fb.attr('src', 'images/smii-social-fb.png');
			ggl.attr('src', 'images/smii-social-ggl.png');
			li.attr('src', 'images/smii-social-li.png');
		}, 600);
	}
	 
	var init = function(id){
		container = $('#'+id);

		params.containerID = id;
		gigya.socialize.showEditConnectionsUI(params);
		gigya.socialize.addEventHandlers({
			onConnectionRemoved: replaceSocialImages,
			onConnectionAdded: replaceSocialImages
		});
	};

	return {
		init: init
	}
}();
SIA.KFlogin = function(){
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var falseCounter = 0;

	var init = function(containerId){
		p.containerId = $('#'+containerId);
		p.formContainer = p.containerId.find(".gigya-login-form");
		p.labelEmail = p.formContainer.find('label.email-add-prompt');
		p.krisFlyer = p.formContainer.find('[name="data.kfMembershipNum"]');
		// p.krisFlyer.remove();
		p.labelEmail.addClass("hidden");	

		
	}

	return {
		init : init
	}
}();
SIA.KFRegistration = function(){
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var falseCounter = 0;

	var init = function(containerId){
		p.containerId = $('#'+containerId);
		p.formContainer = p.containerId.find(".gigya-register-form");
		p.inputs = [];
		p.selects = [];
		p.password = [];
		p.checkBoxs = [];

		p.gigyaRow = p.formContainer.find('[name="data.kfRefMembershipNum"]').closest('.gigya-layout-row').find('.gigya-layout-row')[0];
		$(p.gigyaRow).before('<div class="kf-referal"></div>');

		p.gigyaBelowRow = p.formContainer.find('[name="data.kfRefMembershipNum"]').closest('.gigya-composite-control-textbox');
		$(p.gigyaBelowRow).after('<div class="kf-promo-code"></div>');

		p.noFirstName1 = p.formContainer.find('[name="data.kfNoFirstName"]');
		p.noFirstName2 = p.formContainer.find('[name="data.kfGuardian.NoFirstName"]');
		p.kfMembershipNum = p.formContainer.find('[name="data.kfRefMembershipNum"]');
		p.kfMembershipNum.attr('maxlength',10);
		p.kfPGMembershipNum = p.formContainer.find('[name="data.kfGuardian.membershipNum"]');
		p.kfPGMembershipNum.attr('maxlength',10);
		
		p.areaCode = p.formContainer.find('[name="data.kfMobileArea"]');
		p.areaCode.attr('maxlength',4);

		p.phoneNumber = p.formContainer.find('[name="data.kfMobileNumber"]');
		p.phoneNumber.attr('maxlength',15);

		$(document).on('input','[name="data.kfRefMembershipNum"],[name="data.kfGuardian.membershipNum"]',function(evt){
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			var value = $(this).val();
			// alert(value);
			var charCodeAt = value.charCodeAt(value.length-1);
			
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			} 
			
			if (charCodeAt > 31 && (charCodeAt < 48 || charCodeAt > 57)) {
				$(this).val($(this).val().substring(0,$(this).val().length-1));
				return false;
			}
			return true;
		});
		
		p.selects.push({el: p.formContainer.find('[name="data.kfSalutation"]'), errMsg:"Select title"});
		p.selects.push({el: p.formContainer.find('[name="data.kfGuardian.salutation"]'), errMsg:"Select title"});
		p.selects.push({el: p.formContainer.find('[name="data.kfGuardian.relationship"]'), errMsg:"Select relationship"});
		p.selects.push({el: p.formContainer.find('[name="data.kfMobileCountrycode"]'), errMsg:"This field is required"});
		p.selects.push({el: p.formContainer.find('[name="profile.country"]'), errMsg:"Select country"});
		p.selects.push({el: p.formContainer.find('[name="profile.city"]'), errMsg:"Select city"});
p.country = p.formContainer.find('[name="profile.country"]');
		p.state = p.formContainer.find('[name="profile.state"]');
		p.country.on('change',function(e){
			// alert($(this).val());
			if ($(this).val() == 'value' || $(this).text() == 'default') {
				p.state.attr('disabled','disabled');
			} else {
				p.state.removeAttr('disabled');
			}
		});

		p.country.trigger('change');
		p.inputs.push({el: p.formContainer.find('[name="profile.firstName"]'), errMsg:"Enter first/given name"});
		p.inputs.push({el: p.formContainer.find('[name="profile.lastName"]'), errMsg:"Enter last/family name"});
		p.inputs.push({el: p.formContainer.find('[name="data.birthDate"]'), errMsg:"Enter date of birth"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.firstName"]'), errMsg:"Enter first/given name"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.lastName"]'), errMsg:"Enter last/family name"});
		// p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.membershipNum"]'), errMsg:"Enter membership number"})
		p.inputs.push({el: p.formContainer.find('[name="email"]'), errMsg:"Enter email"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfMobileArea"]'), errMsg:"This field is required"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfMobileNumber"]'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('#gigya-textbox-74091525809889070'), errMsg:"This field is required"});
		p.inputs.push({el: p.formContainer.find('[name="password"]'), errMsg:"This field is required"});
		p.inputs.push({el: p.formContainer.find('[name="passwordRetype"]'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('[name="data.kfRefMembershipNum"]'), errMsg:"This field is required"});

		p.password.push({el: p.formContainer.find('[name="password"]'), errMsg:"This field is required"});
		p.password.push({el: p.formContainer.find('[name="passwordRetype"]'), errMsg:"This field is required"});

		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfAccountStatements.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfGroupNews.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfGroupPromotions.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfPartnerPromotions.email.isSubscribed"]'));

		p.termCheckbox = p.formContainer.find('[name="subscriptions.kfTnC.email.isSubscribed"]');

		p.submitBtn = p.formContainer.find('.gigya-input-submit');
		p.submitBtn.attr('disabled',true);
		p.submitBtn.addClass('disabled');

		p.checkBoxs[p.checkBoxs.length - 1].attr('disabled',true);
		p.checkBoxs[p.checkBoxs.length - 1].closest('div.gigya-composite-control-checkbox').removeClass('tc-partners');

		_.each(p.formContainer.find('.parent-guardian-group'),function(el){
			$(el).addClass('hidden');
		});	

		p.applyBtn = p.formContainer.find('.apply-promocode-btn');

		p.applyInput = p.formContainer.find('[name="data.kfPromocode"]');
		
		p.applyInput.attr('maxlength',12);
		p.applyBtn.click(function(){
			if(p.applyInput.val().length > 0){
				if (p.formValidator.isAlphaNumeric(p.applyInput.val()).state == false) {
					p.formContainer.find('.valid-promo-code').remove();
					p.formContainer.find('.invalid-promo-code').remove();

					p.applyInput.closest('.gigya-layout-row').after(`<div class="invalid-promo-code">
						<div class="promo-message-container">
						<em class="ico-close-round-fill"></em>
						<p class="error-message">Enter a valid promotional code.</p>
						</div>
						</div>`);
				} else {
					p.formContainer.find('.valid-promo-code').remove();
					p.formContainer.find('.invalid-promo-code').remove();
					p.applyInput.closest('.gigya-layout-row').after(`<div class="valid-promo-code">
						<div class="promo-message-container">
						<em class="ico-success"></em>
						<p class="success-message">Promo code valid</p>
						</div>
						</div>`);
				}
			}else{
				p.formContainer.find('.valid-promo-code').remove();
				p.formContainer.find('.invalid-promo-code').remove();

				p.applyInput.closest('.gigya-layout-row').after(`<div class="invalid-promo-code">
					<div class="promo-message-container">
					<em class="ico-close-round-fill"></em>
					<p class="error-message">Promo code invalid</p>
					</div>
					</div>`);
			}
			
		});

		p.submitBtn.click(function(){
			_.each(p.selects,function(selectData){
				if(!((p.formContainer.find('[name="data.kfGuardian.salutation"]').attr('id') == selectData.el.attr('id') || p.formContainer.find('[name="data.kfGuardian.relationship"]').attr('id') == selectData.el.attr('id'))  && p.formContainer.find('.parent-guardian-group.hidden').length > 0)){
					if($(selectData.el).val() == "value"){
						formValidatorAdder($(selectData.el),{state:false, msg:selectData.errMsg});
					}else{
						formValidatorAdder($(selectData.el),{state:true, msg:selectData.errMsg});	
					}
				}
			});
		});

		setTimeout(function() {
			p.inputs[2].el.css('color', 'transparent');
		}, 1000);

		disableDate();

		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.inputs[2].el.parent(), p.inputs[2].el);

		validateDateBirth();

		_.each(p.checkBoxs,function(checkbox){
			checkbox.click(function(){
				p.checkBoxs[0].is(':checked') || p.checkBoxs[1].is(':checked') || p.checkBoxs[2].is(':checked') ? enableBox(p.checkBoxs[3]) : uncheckAndDisable(p.checkBoxs[3]);
			});
		});

		_.each(p.password,function(passwordData){
			new SIA.TogglePassword(passwordData.el.parent(),passwordData.el).init();
			passwordData.el.attr('maxlength', '16');
		});
		
		_.each(p.selects,function(selectData){
			p.formValidator.attachEvnt({
				el: selectData.el,
				blur: function(){
					if($(this).val() == "value"){
						formValidatorAdder($(this),{state:false, msg:selectData.errMsg});
					}else{
						formValidatorAdder($(this),{state:true, msg:selectData.errMsg});	
					}
				}
			});
		});

		_.each(p.inputs,function(inputData){
			p.formValidator.attachEvnt({
				el: inputData.el,
				blur: function(){
					validatorFunction($(this),inputData);
				}
			});
		});

		p.termCheckbox.click(function(){
			p.submitBtn.attr('disabled',!p.termCheckbox.is(":checked"));
			p.submitBtn.hasClass('disabled') ? p.submitBtn.removeClass('disabled') : p.submitBtn.addClass('disabled');
		});

		p.noFirstName1.click(function(){
			p.inputs[0].el.attr('disabled',$(this).is(":checked"));
			p.inputs[0].el.val("");
			$(this).is(":checked")? $(this).closest('.gigya-composite-control').addClass("first-name-checkbox") : $(this).closest('.gigya-composite-control').removeClass("first-name-checkbox");
			setTimeout(function() {
				$(this).is(":checked")? p.inputs[0].el.attr('placeholder', '') : null;
				formValidatorAdder(p.inputs[0].el,{state:true, msg:p.inputs[0].errMsg});
			}, 100);
		});

		p.noFirstName2.click(function(){
			p.inputs[3].el.attr('disabled',$(this).is(":checked"));
			p.inputs[3].el.val("");
			$(this).is(":checked")? $(this).closest('.gigya-composite-control').addClass("first-name-checkbox") : $(this).closest('.gigya-composite-control').removeClass("first-name-checkbox");
			setTimeout(function() {
				$(this).is(":checked")? p.inputs[3].el.attr('placeholder', '') : null;
				formValidatorAdder(p.inputs[3].el,{state:true, msg:p.inputs[3].errMsg});
			}, 100);
		});
	}

	var enableBox = function(checkbox){
		checkbox.attr('disabled',false);
		checkbox.closest('div.gigya-composite-control-checkbox').removeClass('mute');
	}

	var uncheckAndDisable = function(checkbox){
		checkbox.attr('disabled',true);
		checkbox.attr('checked',false);
		checkbox.closest('div.gigya-composite-control-checkbox').addClass('mute');
	}

	var disableDate = function() {
		p.inputs[2].el.attr('placeholder', '');
		p.inputs[2].el.attr('readonly', true);
		p.inputs[2].el.css('color', 'white');
	};

	var checkAge = function(val) {
		var dateFormat = p.formValidator.validateDateFormat(val);
		if (dateFormat.state) {
			// compute the exact birthdate of the user
			var age = p.formValidator.computeBirthDate(val);

			if (age <=  16 && age >= 2) {
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).removeClass('hidden');
					$('.spacer-1').addClass('guardian-section-active');
				});
				return {
					state: true,
					msg: ''
				};
			}else if(age <= 1){
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).addClass('hidden');
					$('.spacer-1').removeClass('guardian-section-active');
				});
				return {
					state: false,
					msg: 'A KrisFlyer member must be at least 2 years old.'
				};
			}else{
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).addClass('hidden');
					$('.spacer-1').removeClass('guardian-section-active');
				});
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var validateDateBirth = function() {
		var dateBirthFields = [
			p.formContainer.find('[data-day]'),
			p.formContainer.find('[data-month]'),
			p.formContainer.find('[data-year]')
		];
		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						checkAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}

			p.formValidator.attachEvnt({
				el: p.inputs[2].el,
				focus: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						checkAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
		}
	};

	var formValidatorAdder = function(element,validatorData){
		p.formValidator.inputValidator(
			element,
			validatorData,
			p.gigyaErrClss,
			p.gigyaValidClss
		);
		if(!validatorData.state){
			falseCounter++;
		}
	}

	var validatorFunction = function(el,inputData){
		var element = el;
		if(!element.attr('disabled')) {
			if (element.attr('name') != "data.kfMobileArea"){
				formValidatorAdder(element,emptyInputValidator(inputData));
			}
			if(element.val() && element.attr("type") == 'email'){
				formValidatorAdder(element,p.formValidator.validateEmail(element.val()));
			}else if(element.val() && (element.attr("id") == 'gigya-password-50139321792236740' || element.attr("id") == 'gigya-password-88724776704780830')){
				formValidatorAdder(element,p.formValidator.validatePassword(element.val()));
				if($('[name="password"]').val() && $('[name="passwordRetype"]').val()){
					$('[name="password"]').val() == $('[name="passwordRetype"]').val() ? formValidatorAdder(element,{state:true, msg:"password mismatch"}) :
					formValidatorAdder(element,{state:false, msg:"password mismatch"});
				}
			} else if (element.attr('name') == "data.kfMobileArea" || element.attr('name') == "data.kfMobileNumber") {
				if (element.val() != "") {
					formValidatorAdder(element,validateNumber(element.val()));
				}
				
			}
		}
	}

	var validateNumber = function (val) {
		var regex = /^[0-9]*$/;
		return {
			state: regex.test(val),
			msg: 'Enter a phone number'
		};
	}

	var emptyInputValidator = function(inputData){
		validatorResult = p.formValidator.validateEmpty(inputData.el.val());
		// validatorResult.msg = inputData.errMsg;
		return validatorResult;
	}

	var beforeSubmit = function(){
		falseCounter = 0;
		var status = false;
		_.each(p.inputs, function(inputData){
			validatorFunction(inputData.el,inputData);
		});

		_.each(p.selects,function(selectData){
			if(!((p.formContainer.find('[name="data.kfGuardian.salutation"]').attr('id') == selectData.el.attr('id') || p.formContainer.find('[name="data.kfGuardian.relationship"]').attr('id') == selectData.el.attr('id'))  && p.formContainer.find('.parent-guardian-group.hidden').length > 0)){
				if($(selectData.el).val() == "value"){
					formValidatorAdder($(selectData.el),{state:false, msg:selectData.errMsg});
				}else{
					formValidatorAdder($(selectData.el),{state:true, msg:selectData.errMsg});	
				}
			}
		});
		// formValidatorAdder(p.inputs[2].el,p.formValidator.validateLegalAge(p.inputs[2].el.val()));
		
		if(!falseCounter){
			status = true;
		}
		return status;
	}
	

	return {
		init: init,
		onBeforeSubmit:beforeSubmit,
	}
}();

SIA.KFRegistrationCompletion = function(){
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var falseCounter = 0;

	var init = function(containerId){
		p.containerId = $('#'+containerId);
		p.formContainer = p.containerId.find(".gigya-profile-form");
		p.inputs = [];
		p.selects = [];
		p.password = [];
		p.checkBoxs = [];

		p.gigyaRow = p.formContainer.find('[name="data.kfRefMembershipNum"]').closest('.gigya-layout-row').find('.gigya-layout-row')[0];
		$(p.gigyaRow).before('<div class="kf-referal"></div>');

		p.gigyaBelowRow = p.formContainer.find('[name="data.kfRefMembershipNum"]').closest('.gigya-composite-control-textbox');
		$(p.gigyaBelowRow).after('<div class="kf-promo-code"></div>');

		p.noFirstName1 = p.formContainer.find('[name="data.kfNoFirstName"]');
		p.noFirstName2 = p.formContainer.find('[name="data.kfGuardian.NoFirstName"]');
		p.kfMembershipNum = p.formContainer.find('[name="data.kfRefMembershipNum"]');
		p.kfMembershipNum.attr('maxlength',10);
		p.kfPGMembershipNum = p.formContainer.find('[name="data.kfGuardian.membershipNum"]');
		p.kfPGMembershipNum.attr('maxlength',10);
		p.areaCode = p.formContainer.find('[name="data.kfMobileArea"]');
		p.areaCode.attr('maxlength',4);

		p.phoneNumber = p.formContainer.find('[name="data.kfMobileNumber"]');
		p.phoneNumber.attr('maxlength',15);

		$(document).on('input','[name="data.kfRefMembershipNum"],[name="data.kfGuardian.membershipNum"]',function(evt){
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			var value = $(this).val();
			var charCodeAt = value.charCodeAt(value.length-1);
			// alert(value);
			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			} 
			
			if (charCodeAt > 31 && (charCodeAt < 48 || charCodeAt > 57)) {
				$(this).val($(this).val().substring(0,$(this).val().length-1));
				return false;
			}
			return true;
		});
		p.kfMembershipNum.on('blur',function(){
			if ($(this).val().length < 10 && $(this).val().length > 0) {
					$(this).addClass('gigya-error');
					$(this).next('span').css('visibility','visible').text('Enter 10 digit membership number');
			} else {
					$(this).removeClass('gigya-error');
					$(this).next('span').css('visibility','hidden').text();
			}
		});
		p.selects.push({el: p.formContainer.find('[name="data.kfSalutation"]'), errMsg:"Select title"});
		p.selects.push({el: p.formContainer.find('[name="data.kfGuardian.salutation"]'), errMsg:"Select title"});
		p.selects.push({el: p.formContainer.find('[name="data.kfGuardian.relationship"]'), errMsg:"Select relationship"});
		p.selects.push({el: p.formContainer.find('[name="data.kfMobileCountrycode"]'), errMsg:"This field is required"});
		p.selects.push({el: p.formContainer.find('[name="profile.country"]'), errMsg:"Select country"});
		p.selects.push({el: p.formContainer.find('[name="profile.city"]'), errMsg:"Select city"});
p.country = p.formContainer.find('[name="profile.country"]');
		p.state = p.formContainer.find('[name="profile.state"]');
		p.country.on('change',function(e){
			// alert($(this).val());
			if ($(this).val() == 'value' || $(this).text() == 'default') {
				p.state.attr('disabled','disabled');
			} else {
				p.state.removeAttr('disabled');
			}
		});

		p.country.trigger('change');
		p.inputs.push({el: p.formContainer.find('[name="profile.firstName"]'), errMsg:"Enter first/given name"});
		p.inputs.push({el: p.formContainer.find('[name="profile.lastName"]'), errMsg:"Enter last/family name"});
		p.inputs.push({el: p.formContainer.find('[name="data.birthDate"]'), errMsg:"Enter date of birth"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.firstName"]'), errMsg:"Enter first/given name"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.lastName"]'), errMsg:"Enter last/family name"});
		// p.inputs.push({el: p.formContainer.find('[name="data.kfGuardian.membershipNum"]'), errMsg:"Enter membership number"})
		p.inputs.push({el: p.formContainer.find('[name="username"]'), errMsg:"Enter email"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfMobileArea"]'), errMsg:"This field is required"});
		p.inputs.push({el: p.formContainer.find('[name="data.kfMobileNumber"]'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('#gigya-textbox-74091525809889070'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('#gigya-textbox-87283140885140770'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('#gigya-textbox-146936954425140220'), errMsg:"This field is required"});
		// p.inputs.push({el: p.formContainer.find('[name="data.kfRefMembershipNum"]'), errMsg:"This field is required"});

		// p.password.push({el: p.formContainer.find('#gigya-textbox-87283140885140770'), errMsg:"This field is required"});
		// p.password.push({el: p.formContainer.find('#gigya-textbox-146936954425140220'), errMsg:"This field is required"});

		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfAccountStatements.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfGroupNews.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfGroupPromotions.email.isSubscribed"]'));
		p.checkBoxs.push(p.formContainer.find('[name="subscriptions.kfTnC.email.isSubscribed"]'));

		p.termCheckbox = p.formContainer.find('[name="subscriptions.kfTnC.email.isSubscribed"]');

		p.submitBtn = p.formContainer.find('.gigya-input-submit');
		p.submitBtn.attr('disabled',true);
		p.submitBtn.addClass('disabled');

		p.checkBoxs[p.checkBoxs.length - 1].attr('disabled',true);
		p.checkBoxs[p.checkBoxs.length - 1].closest('div.gigya-composite-control-checkbox').removeClass('tc-partners');

		p.applyBtn = p.formContainer.find('.apply-promocode-btn');

		p.applyInput = p.formContainer.find('[name="data.kfPromocode"]');
		p.applyInput.attr('maxlength',12);
		_.each(p.formContainer.find('.parent-guardian-group'),function(el){
			$(el).addClass('hidden');
		});	
		
		// p.applyBtn.click(function(){
		// 	if(p.applyInput.val().length > 0){
		// 		p.formContainer.find('.valid-promo-code').remove();
		// 		p.formContainer.find('.invalid-promo-code').remove();
		// 		p.applyInput.closest('.gigya-layout-row').after(`<div class="valid-promo-code">
		// 															<em class="ico-success"></em>
		// 															<p class="success-message">Promo code valid</p>
		// 														</div>`);
		// 	}else{
		// 		p.formContainer.find('.valid-promo-code').remove();
		// 		p.formContainer.find('.invalid-promo-code').remove();
		// 		p.applyInput.closest('.gigya-layout-row').after(`<div class="invalid-promo-code">
		// 														<em class="ico-close-round-fill"></em>
		// 														<p class="error-message">Promo code invalid</p>
		// 													</div>`);
		// 	}
			
		// });

		p.submitBtn.click(function(){
			_.each(p.selects,function(selectData){
				if(!((p.formContainer.find('[name="data.kfGuardian.salutation"]').attr('id') == selectData.el.attr('id') || p.formContainer.find('[name="data.kfGuardian.relationship"]').attr('id') == selectData.el.attr('id'))  && p.formContainer.find('.parent-guardian-group.hidden').length > 0)){
					if($(selectData.el).val() == "value"){
						formValidatorAdder($(selectData.el),{state:false, msg:selectData.errMsg});
					}else{
						formValidatorAdder($(selectData.el),{state:true, msg:selectData.errMsg});	
					}
				}
			});
		});

		setTimeout(function() {
			p.inputs[2].el.css('color', 'transparent');
		}, 1000);

		disableDate();

		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.inputs[2].el.parent(), p.inputs[2].el);

		validateDateBirth();

		_.each(p.checkBoxs,function(checkbox){
			checkbox.click(function(){
				p.checkBoxs[0].is(':checked') || p.checkBoxs[1].is(':checked') || p.checkBoxs[2].is(':checked') ? enableBox(p.checkBoxs[3]) : uncheckAndDisable(p.checkBoxs[3]);
			});
		});

		_.each(p.password,function(passwordData){
			new SIA.TogglePassword(passwordData.el.parent(),passwordData.el).init();
			passwordData.el.attr('maxlength', '16');
		});
		
		_.each(p.selects,function(selectData){
			p.formValidator.attachEvnt({
				el: selectData.el,
				blur: function(){
					if($(this).val() == "value"){
						formValidatorAdder($(this),{state:false, msg:selectData.errMsg});
					}else{
						formValidatorAdder($(this),{state:true, msg:selectData.errMsg});	
					}
				}
			});
		});

		_.each(p.inputs,function(inputData){
			p.formValidator.attachEvnt({
				el: inputData.el,
				blur: function(){
					validatorFunction($(this),inputData);
				}
			});
		});

		p.termCheckbox.click(function(){
			p.submitBtn.attr('disabled',!p.termCheckbox.is(":checked"));
			p.submitBtn.hasClass('disabled') ? p.submitBtn.removeClass('disabled') : p.submitBtn.addClass('disabled');
		});

		p.noFirstName1.click(function(){
			p.inputs[0].el.attr('disabled',$(this).is(":checked"));
			p.inputs[0].el.val("");
			$(this).is(":checked")? $(this).closest('.gigya-composite-control').addClass("first-name-checkbox") : $(this).closest('.gigya-composite-control').removeClass("first-name-checkbox");
			setTimeout(function() {
				$(this).is(":checked")? p.inputs[0].el.attr('placeholder', '') : null;
				formValidatorAdder(p.inputs[0].el,{state:true, msg:p.inputs[0].errMsg});
			}, 100);
		});

		p.noFirstName2.click(function(){
			p.inputs[3].el.attr('disabled',$(this).is(":checked"));
			p.inputs[3].el.val("");
			$(this).is(":checked")? $(this).closest('.gigya-composite-control').addClass("first-name-checkbox") : $(this).closest('.gigya-composite-control').removeClass("first-name-checkbox");
			setTimeout(function() {
				$(this).is(":checked")? p.inputs[3].el.attr('placeholder', '') : null;
				formValidatorAdder(p.inputs[3].el,{state:true, msg:p.inputs[3].errMsg});
			}, 100);
		});
	}

	var enableBox = function(checkbox){
		checkbox.attr('disabled',false);
		checkbox.closest('div.gigya-composite-control-checkbox').removeClass('mute');
	}

	var uncheckAndDisable = function(checkbox){
		checkbox.attr('disabled',true);
		checkbox.attr('checked',false);
		checkbox.closest('div.gigya-composite-control-checkbox').addClass('mute');
	}

	var disableDate = function() {
		p.inputs[2].el.attr('placeholder', '');
		p.inputs[2].el.attr('readonly', true);
		p.inputs[2].el.css('color', 'white');
	};

	var checkAge = function(val) {
		var dateFormat = p.formValidator.validateDateFormat(val);
		if (dateFormat.state) {
			// compute the exact birthdate of the user
			var age = p.formValidator.computeBirthDate(val);

			if (age <=  16 && age >= 2) {
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).removeClass('hidden');
					$('.spacer-1').addClass('guardian-section-active');
				});				
				return {
					state: true,
					msg: ''
				};
			}else if(age <= 1){
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).addClass('hidden');
					$('.spacer-1').removeClass('guardian-section-active');
				});
				return {
					state: false,
					msg: 'A KrisFlyer member must be at least 2 years old.'
				};
			}else{
				_.each(p.formContainer.find('.parent-guardian-group'),function(el){
					$(el).addClass('hidden');
					$('.spacer-1').removeClass('guardian-section-active');
				});
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var validateDateBirth = function() {
		var dateBirthFields = [
			p.formContainer.find('[data-day]'),
			p.formContainer.find('[data-month]'),
			p.formContainer.find('[data-year]')
		];
		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						checkAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}

			p.formValidator.attachEvnt({
				el: p.inputs[2].el,
				focus: function() {
					p.formValidator.inputValidator(
						p.inputs[2].el,
						checkAge(p.inputs[2].el.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});
		}
	};

	var formValidatorAdder = function(element,validatorData){
		p.formValidator.inputValidator(
			element,
			validatorData,
			p.gigyaErrClss,
			p.gigyaValidClss
		);
		if(!validatorData.state){
			falseCounter++;
		}
	}

	var validatorFunction = function(el,inputData){
		var element = el;
		if(!element.attr('disabled')) {
			if (element.attr('name') != "data.kfMobileArea"){
				formValidatorAdder(element,emptyInputValidator(inputData));
			}
			if(element.val() && element.attr("type") == 'email'){
				formValidatorAdder(element,p.formValidator.validateEmail(element.val()));
			}/*else if(element.val() && (element.attr("id") == 'gigya-password-50139321792236740' || element.attr("id") == 'gigya-password-88724776704780830')){
				formValidatorAdder(element,p.formValidator.validatePassword(element.val()));
				if($('[name="password"]').val() && $('[name="passwordRetype"]').val()){
					$('[name="password"]').val() == $('[name="passwordRetype"]').val() ? formValidatorAdder(element,{state:true, msg:"password mismatch"}) :
					formValidatorAdder(element,{state:false, msg:"password mismatch"});
				}
			} */else if (element.attr('name') == "data.kfMobileArea" || element.attr('name') == "data.kfMobileNumber") {
				if (element.val() != "") {
					formValidatorAdder(element,validateNumber(element.val()));
				}
				
			}
		}
	}
	var validateNumber = function (val) {
		var regex = /^[0-9]*$/;
		return {
			state: regex.test(val),
			msg: 'Enter a phone number'
		};
	}

	var emptyInputValidator = function(inputData){
		validatorResult = p.formValidator.validateEmpty(inputData.el.val());
		// validatorResult.msg = inputData.errMsg;
		return validatorResult;
	}

	var beforeSubmit = function(){
		falseCounter = 0;
		var status = false;
		_.each(p.inputs, function(inputData){
			validatorFunction(inputData.el,inputData);
		});

		_.each(p.selects,function(selectData){
			if(!((p.formContainer.find('[name="data.kfGuardian.salutation"]').attr('id') == selectData.el.attr('id') || p.formContainer.find('[name="data.kfGuardian.relationship"]').attr('id') == selectData.el.attr('id'))  && p.formContainer.find('.parent-guardian-group.hidden').length > 0)){
				if($(selectData.el).val() == "value"){
					formValidatorAdder($(selectData.el),{state:false, msg:selectData.errMsg});
				}else{
					formValidatorAdder($(selectData.el),{state:true, msg:selectData.errMsg});	
				}
			}
		});
		formValidatorAdder(p.inputs[2].el,p.formValidator.validateLegalAge(p.inputs[2].el.val()));
		
		if(!falseCounter){
			status = true;
		}
		return status;
	}
	

	return {
		init: init,
		onBeforeSubmit:beforeSubmit,
	}
}();




$(function() {
	var body = SIA.global.vars.body;

	if (body.hasClass('gigya-login')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'loginContainer',
	        startScreen: "gigya-login-screen",
	        onAfterScreenLoad: SIA.RULogin.init,
	        onError: SIA.RULogin.onError
	    });

	    SIA.RadioTab.init();
	}

	var renderInfoBox = function(){
		var infoBox = $('.info-box');

		infoBox.each(function(){
			var self = $(this);
			var infoBoxButton = $('.info__button', self);

			infoBoxButton.off('click').on('click', function(){
				self.remove();
			});
		});
	};

	renderInfoBox();

	SIA.EditPhonePin.init();

	if (body.hasClass('gigya-registration')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTab',
	  		startScreen: "gigya-register-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTab');

	  			SIA.JoinKrisFlyer.init();
	  		}
	   });
	}

	if (body.hasClass('gigya-reg-intermediate')) {
		// var frmPg = body.hasClass('gigya-reg-intermediate-2') ? 'from-login' : 'from-reg';
		var pgSrc = body.find('aside.ru-reg-intermediate').attr('data-msg');

		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'gigyaRegistrationIntermediate',
	  		startScreen: "gigya-register-intermediate-screen",
	  		width: "100%",
  			onError: SIA.RURegIntermediate.onError,
  			onBeforeSubmit: SIA.RURegIntermediate.onBeforeSubmit,
				context: { 'ruPg': pgSrc },
				onAfterScreenLoad: function(evtObj){
					SIA.RURegIntermediate.onAfterScreenLoad();
					body.find('.sub-heading-1--blue').addClass('hidden');
					body.find('.main-text').addClass('hidden');

					// From login page
					if(typeof evtObj.context === 'undefined' || evtObj.context.ruPg === 'from-login') {
						body.find('.sub-heading-1--blue').each(function(){
							var t = $(this);
							if(!$(this).hasClass('intermediate-2')) {
								t.removeClass('hidden');
							}
						});
						body.find('.main-text').each(function(){
							var t = $(this);
							if(!$(this).hasClass('intermediate-2')) {
								t.removeClass('hidden');
							}
						});
					}

					// From registration page
					if(evtObj.context.ruPg === 'from-reg') {
						body.find('.sub-heading-1--blue').each(function(){
							var t = $(this);
							if($(this).hasClass('intermediate-2')) {
								t.removeClass('hidden');
							}
						});
						body.find('.main-text').each(function(){
							var t = $(this);
							if($(this).hasClass('intermediate-2')) {
								t.removeClass('hidden');
							}
						});
					}

				},
	   });
	}

	if (body.hasClass('gigya-reg-completion')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTabCompletion',
	  		startScreen: "gigya-complete-registration-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTabCompletion');

	  			SIA.JoinKrisFlyer.init();
	  		}
	   });
	}

	if (body.hasClass('js_kf-upgrade')) {
		// [Mon: 24 Jul 2018] Need to call after 'gigya-reg-completion'
		SIA.JoinKrisFlyer.init();

		// make sure to have the checkbox logic
		$('.js_kf-checkbox').change(function() {
			if ( $('.js_kf-checkbox--account').is(':checked') ||
			     $('.js_kf-checkbox--newsletter').is(':checked') ||
			     $('.js_kf-checkbox--promotions').is(':checked') ) {
				$('.js_kf-checkbox--partner').prop('disabled', false); 
			} else {
				$('.js_kf-checkbox--partner').prop('disabled', true); 
				$('.js_kf-checkbox--partner').prop('checked', false);
			}
		});		


		// custom logic for promocode
		function kf_promo_code_input_update( js_css_class ) {
			var promo_code_input = $.trim( $( js_css_class ).val() );
			if( promo_code_input.length > 0 ) {
				$('.js_kf-form__promo-code__span-error').hide(); 
				$('.js_kf-form__promo-code__span').removeClass('input-error')
			} else {
				$('.js_kf-form__promo-code__span-error').show(); 
				$('.js_kf-form__promo-code__span').addClass('input-error')
			}
		}
		$('.js_kf-form__promo-code__btn').click(function(e){
			e.preventDefault();

			var el_kf_promo_code_input = '.js_kf-form__promo-code__input'; 
			kf_promo_code_input_update( el_kf_promo_code_input ); 

			var promo_code_input = $.trim( $( el_kf_promo_code_input ).val() );
			if( promo_code_input.length > 0 ) {

				// hide all errors whenever there is a change
				$('.js_kf-form__alert').hide(); 

				// if error
				if ( promo_code_input == 'x' )
					$('.js_kf-form__alert--error').show(); 

				// if success
				else 
					$('.js_kf-form__alert--success').show(); 
			}

		});
	}

	// for smii-kf-acct-profile-completion.html
	if (body.hasClass('js_kf-acct__body')) {
		SIA.JoinKrisFlyer.init();

		// make the eye icon work for #kf-acct__form__pin-input
		el_create_pin = $('#kf-acct__form__pin-input'); 
		new SIA.TogglePassword( el_create_pin.parent(), el_create_pin ).init();

		// a copy of #addAddressLine func
		el_add = $( '#addressLine' )
		var btn = $( '.js_kf-acct__form__add-address' );
		btn.on('click', function(e) {
			e.preventDefault();

			var t = $(this);
			var max = parseInt(el_add.attr('data-addressline-max'));
			var currentAddressLine = el_add.find('[data-addressline-default]').length;

			if (currentAddressLine <= max) {
				var newIndex = currentAddressLine + 1;
				var addressLine = $(el_add.find('.additional-address-cont')[0]).clone();

				// Remove all label's text in all cloned
				addressLine.find('label').empty();
				addressLine.find('.text-error').remove();
				addressLine.removeClass( 'error' );

				// remove post code on clone
				addressLine.children('div:eq(1)').remove();

				// make sure to only get {type=text] & [type=tel]
				var addressLineEl = addressLine.find('input[type=text]');
				var placeholder = addressLineEl.attr('placeholder');
				addressLineEl.attr('placeholder', placeholder.slice(0, -1) + newIndex);

				// lets remove all required related attributes
				addressLineEl
					.removeAttr( "data-rule-required" )
					.removeAttr( "data-msg-required" )
					.removeAttr( "data-rule-address" )
					.removeAttr( "data-msg-address" )
					.removeAttr( "aria-required" )
					.removeClass( "error" )

				var idname = addressLineEl.attr('id');
				var newIdName = idname.slice(0, -1) + newIndex;
				addressLineEl.attr('id', newIdName);
				addressLineEl.attr('name', newIdName);

				var ariaLabel = addressLineEl.attr('aria-labelledby');
				var currentIndexPos = ariaLabel.indexOf('2');
				var newAriaLabel = ariaLabel.slice(0, currentIndexPos) + newIndex + ariaLabel.slice(currentIndexPos + 1);
				addressLineEl.attr('aria-labelledby', newAriaLabel);

				addressLineEl.val('');

				el_add.find('.js_kf-acct__form__clone-here').before(addressLine);

				if (newIndex == max) {
					btn.closest('.additonal-address-line').addClass('hidden');
				}
			}
		});
	}

	if (body.hasClass('profile-update')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-ProfileUpdate',
	        containerID: 'changePassword',
	        startScreen: "gigya-change-password-screen",
	        onAfterScreenLoad: SIA.ProfileUpdate.afterScreenLoad,
	        onError: SIA.ProfileUpdate.error,
	        onBeforeSubmit: SIA.ProfileUpdate.beforeSubmit
	    });
	}

	if (body.hasClass('password-change-success')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-ProfileUpdate',
	        containerID: 'resetPasswordSuccess',
	        startScreen: "Success"
	    });
	}

	if (body.hasClass('link-accounts')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-LinkAccounts',
	        containerID: 'forgotPasswordContainer',
	        startScreen: "gigya-forgot-password-screen"
	    });
	}

	if (body.hasClass('link-accounts')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-LinkAccounts',
	        containerID: 'forgotPasswordSuccessContainer',
	        startScreen: "gigya-forgot-password-success-screen"
	    });
	}

	if (body.hasClass('login-social-media')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-LinkAccounts',
	        containerID: 'loginSocialMedia',
	        startScreen: "gigya-link-account-screen"
	    });
	}

	if (body.hasClass('gigya-login')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'verificationPending',
			startScreen: "gigya-verification-pending-screen",
			onAfterScreenLoad: function() {
				if ($('#verificationPending').attr('data-modal-onload') === "true") {
					$('.ru-login').Popup('show');
					SIA.Verification.init();
				}
        	}
	    });
	}

	// check for edit profile attribute
	if($('[ data-profile-edit="true"]').length) {
		gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'profileEditContainer',
	        startScreen: "gigya-profile-edit-screen",
			onAfterScreenLoad:SIA.DataProfileEdit.init,
			onBeforeSubmit:SIA.DataProfileEdit.onBeforeSubmit
	    });
	}

	// check for account management social media attribute to initialise social media linking
	if($('[data-acct-mng-social]').length) {
		$('[data-acct-mng-social]').each(function(){
			SIA.SocialMediaLink.init($(this).attr('data-acct-mng-social'));
		});
	}

	// check for edit update password attribute to initialise screenset
	if($('[data-update-password="true"]').length) {
		if ($(body).hasClass('smii-kf-profile-page')) {
			gigya.accounts.showScreenSet({
		        screenSet: 'KF-ProfileUpdate',
		        containerID: 'updatePassword',
		        startScreen: 'gigya-update-pwd-screen',
				onAfterScreenLoad:SIA.DataPasswordUpdate.init,
				onBeforeSubmit:SIA.DataPasswordUpdate.onBeforeSubmit
			});
		} else {
			gigya.accounts.showScreenSet({
		        screenSet: 'RU-RegistrationLogin',
		        containerID: 'updatePassword',
		        startScreen: 'gigya-update-pwd-screen',
				onAfterScreenLoad:SIA.DataPasswordUpdate.init,
				onBeforeSubmit:SIA.DataPasswordUpdate.onBeforeSubmit
			});
		}
	}

	// check for edit create password attribute to initialise screenset
	if($('[data-create-password="true"]').length) {
		SIA.AccountCreatePassword.init()
	}

	// check for edit create password attribute to initialise screenset
	if($('[data-screenset-id]').length) {
		gigya.accounts.showScreenSet({
			screenSet: $('[data-screenset-id]').attr('data-screenset-id'),
			containerID: $('[data-screenset-id]').attr('id'),
			startScreen: $('[data-screenset-id]').attr('data-start-screen'),
			onAfterScreenLoad:function(){
				if ($('body').hasClass('popup-on-load')) {
					$('.ru-login').Popup('show');
					$('.popup-update-password').Popup('show');
				}
				if($('[data-screenset-id]').attr('data-start-screen') === 'gigya-register-screen'){
					SIA.KFRegistration.init($('[data-screenset-id]').attr('id'));
				}else if($('[data-screenset-id]').attr('data-start-screen') === 'gigya-complete-registration-screen'){
					SIA.KFRegistrationCompletion.init($('[data-screenset-id]').attr('id'));
				}else if($('[data-screenset-id]').attr('data-start-screen') == 'gigya-login-screen'){
					SIA.KFlogin.init($('[data-screenset-id]').attr('id'));
				}
				
				modifyGigyaElements();
				$('[data-gigya-name="newPassword"]').each(function(){
					new SIA.TogglePassword($(this).parent(),$(this)).init();
				});

				$('[data-gigya-name="passwordRetype"]').each(function(){
					new SIA.TogglePassword($(this).parent(),$(this)).init();
				});
				

				// $("#kfResetPassword .email-wrapper").next('label').hide();
				/*$("#kfResetPassword .email-wrapper").parent('div').next('div').find('div.krisflyer-number-input-field').hide();
				$("#kfResetPassword .email-wrapper").parent('div').next('div').find('a[data-translation-key="LINK_45775721928500420_LABEL"]').hide();*/
				$(".popup--kf-flows-update-password").removeClass('hidden');


			},
			onBeforeSubmit:function(){
				if($('[data-screenset-id]').attr('data-start-screen') === 'gigya-register-screen'){
					SIA.KFRegistration.onBeforeSubmit();
				}else if($('[data-screenset-id]').attr('data-start-screen') === 'gigya-complete-registration-screen'){

				}
			}
		});
	}

	var modifyGigyaElements = function() {
		var $hideThis = [
			"LABEL_57542060841061710_LABEL",
			"LABEL_78216077377727250_LABEL",
			"LABEL_127120853607225180_LABEL",
			"HEADER_52718613360185736_LABEL",
			"HEADER_119803489452460820_LABEL"		
		];
	
		$.each($hideThis, function( key, value ) {
				$('div#KfLogin [data-translation-key="'+value+'"]').addClass('hidden');
				$('div#KfWrongSocialMedia [data-translation-key="'+value+'"]').addClass('hidden');
		});
		
		$("div#KfLogin .krsflyer-email-bar").show();
		$("div#KfLogin .krisflyer-number-input-bar").show();
		/*KF Login*/
		if ($('body').hasClass('gigya-update-email')) {
			$('div#KfLogin label[data-translation-key="LABEL_142155286706228670_LABEL"]').removeClass('hidden');
			$('div#KfLogin label[data-translation-key="LABEL_57542060841061710_LABEL"]').removeClass('hidden');
			$('div#KfLogin label[data-translation-key="LABEL_127120853607225180_LABEL"]').removeClass('hidden');
			$("div#KfLogin .krsflyer-email-bar").hide();
		} else {
			$("div#KfLogin .krisflyer-number-input-bar").hide();
		}
		
		var $validator = SIA.FormValidator;	
		
		$("div#KfLogin [data-gigya-name='data.kfMembershipNum'").on('blur',function(e){
		
			$validateEmpty = $validator.validateEmpty($(this).val());
			if ($validateEmpty.state) {
				$(this).next('span').text('').removeClass('gigya-error-msg-active');	
				// $(this).removeClass('gigya-empty gigya-error');
			} else {
				$(this).next('span').text($validateEmpty.msg).addClass('gigya-error-msg-active');
				
			}
		});

		$("div#KfLogin [data-gigya-name='password'").on('keypress',function(e){
			$validate = $validator.validateEmpty($(this).val());
			if ($validate.state) {
				$(this).next('span').text('').removeClass('gigya-error-msg-active');	
			} else {
				$(this).next('span').text($validate.msg).addClass('gigya-error-msg-active');
			}
			// console.log('k');
		})
	}
	
	

	var tabContainer = $('#ruTabs');
	if(tabContainer.length) {
		tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					tabContainer.find('.tab-item').removeClass('active');
					tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	}
});
