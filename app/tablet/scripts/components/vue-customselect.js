SIA.VueCustomSelect = function() {

    var templateUI = `<v-select 
        :min-length="minLength" 
        :options="options"
        v-model="value"
        :on-change="onChange" 
        :max-height="maxHeight"
        :readonly="readonly"
        :use-key="useKey"
        :clear-search-on-select="false"
        :autocomplete="autocomplete"
        :placeholder="placeholder"
        :disabled="disabled">
        <template slot="option" slot-scope="option">
            <div>
                {{ useLabel ? option[useLabel] : mergeObject(option) }}
            </div>
        </template>
        <span slot="no-options">No Results Found</span>
    </v-select>`;
    var icoDown = `<span class="ico-dropdown ico-point-d"></span>`;
    var icoClose = `<a @click.prevent="clearText()" href="#" class="ico-cancel-thin add-clear-text hidden">
        <span class="ui-helper-hidden-accessible">clear</span>
    </a>`;

    var customSelectClass = "custom-select custom-select--2";

    var getComponentMethod = function() {
        return {
            onChange: onChange,
            mergeObject: mergeObject
        }
    };

    var init = function() {
        var vSelect = extendVue(VueSelect.VueSelect);
        Vue.component("v-select", vSelect);
        Vue.component("vue-customselect", {
            template: templateUI,
            props: {
                options: Object,
                minLength: {
                    type: Number,
                    default: 1
                },
                readonly: Boolean,
                value: String,
                useKey: String,
                useLabel: String,
                autocomplete: {
                    type: Boolean,
                    default: false
                },
                maxHeight: {
                    type: String,
                    default: "270px"
                },
                disabled: {
                    type: Boolean,
                    default: false
                },
                placeholder: String
            },
            methods: getComponentMethod()
        });
    };
    
    // METHOD
    var onChange = function(option) {
        var $this = this;
        this.$emit("input", option);
        setTimeout(function() {
            var selectTag = $this.$el.querySelectorAll(".selected-tag")[0];
            if (selectTag) {
                selectTag.removeClass("selected-tag");
                selectTag.addClass("select__text");
            }
        }, 10);
    };

    var mergeObject = function(option) {
        delete option.format;
        return Object.values(option).join("");
    };
    // OVERRIDE
    var extendVue = function(VueElement) {
        VueElement.mounted = onMount;
        VueElement.computed.filteredOptions = filteredOptions;
        VueElement.props.pristine = {
            type: Boolean,
            default: true
        };
        VueElement.props.minLength = Number;
        VueElement.props.readonly = Boolean;
        VueElement.props.useKey = String;
        VueElement.props.autocomplete = Boolean;
        VueElement.methods.onSearchFocus = onFocus;
        VueElement.methods.onSearchBlur = onBlur;
        VueElement.methods.isOptionSelected = isOptionSelected;
        VueElement.methods.select = onSelect;
        VueElement.methods.maybeAdjustScroll = function() { return true };
        VueElement.watch.search = onValueChange;
        VueElement.watch.value = onModelChange;
        VueElement.watch.disabled = onDisableChange;
        return VueElement;
    };

    var onMount = function() {
        var $this = this;
        var el = this.$el;
        this.search = (this.mutableValue || "");
        el.querySelectorAll(".dropdown-toggle")[0].addClass(customSelectClass);
        el.querySelectorAll(".dropdown-toggle")[0][this.disabled ? "addClass" : "removeClass"]("disabled");
        el.querySelectorAll(".clear")[0].removeElement();
        el.querySelectorAll(".open-indicator")[0].removeElement();
        el.querySelectorAll("input")[0].style.opacity = (this.mutableValue) ? 0 : 1;
        if (this.disabled) {
            el.querySelectorAll("input")[0].style.opacity = 0;
        }
        if (this.readonly) {
            el.querySelectorAll("input")[0].attr("readonly", "true");
            el.querySelectorAll("input")[0].insertAfter(icoDown);
        } else {
            var newComponent = Vue.extend({
                template: icoClose,
                methods: function() {
                    return {
                        clearText: clearText
                    }
                }()
            });
            var $icoClose = new newComponent().$mount();
            el.querySelectorAll("input")[0].insertAfter($icoClose.$el, true);
            el.querySelectorAll(".ico-cancel-thin")[0].insertAfter(icoDown);
            $icoClose.$on("ico:clear-text", function() {
                $this.search = "";
            });
        }
    };

    var filteredOptions = function () {
        if (!this.filterable && !this.taggable) {
            return this.mutableOptions.slice()
        }
        if (!this.autocomplete) {
            return this.mutableOptions;
        }
        let options = this.mutableOptions.filter((option) => {
            var regExp = new RegExp(escapeRegExp(this.search), "i");
            var optionValue = (typeof option == "object") ? option[this.useKey] : option;
            
            if (this.pristine) {
                return option;
            }

            if (regExp.test(optionValue) && this.search.length >= this.minLength) {
                return option;
            }
        })
        if (this.taggable && this.search.length && !this.optionExists(this.search)) {
            options.unshift(this.search)
        }

        return options;
    };

    var onSelect = function (option) {
        if (!this.isOptionSelected(option)) {
            if (this.taggable && !this.optionExists(option)) {
                option = this.createOption(option);
            }
            if (this.multiple && !this.mutableValue) {
                this.mutableValue = [option];
            } else if (this.multiple) {
                this.mutableValue.push(option);
            } else {
                this.mutableValue = option;
            }
        }
        if (this.useKey) {
            this.search = this.mutableValue[this.useKey] || "";
        } else {
            this.search = this.mutableValue || "";
        }

        this.onAfterSelect(option);
    };

    var onFocus = function() {
        var $this = this;
        var el = this.$el;
        var inputElement = this.$el.querySelectorAll("input")[0];
        this.open = true;
        inputElement.selectionStart = this.search.length;
        inputElement.selectionEnd = this.search.length;
        this.pristine = true;
        this.$emit('search:focus');
        setFocusClass(el, true);
        setTimeout(function() {
            var highlight = el.querySelectorAll(".highlight")[0];
            if (highlight) {
                if (!highlight.hasClass("active")) highlight.removeClass("highlight");
            }
            scrollTo(el, ".active");
            onKeyEvent(true, el);
            if ($this.search) {
                setIceClose(el, true);
            }
        }, 10);
    };

    var onBlur = function() {
        var el = this.$el;
        if (this.clearSearchOnBlur) {
            this.search = '';
        }
        this.mutableValue = (this.search || "");
        this.pristine = true;
        this.open = false;
        this.$emit('search:blur');
        setFocusClass(el, false);
        setTimeout(function() {
            onKeyEvent(false);
            setIceClose(el, false);
        }, 10);
    };

    var onKeyEvent = function(state, $this) {
        document.onkeydown = (state) ? function(e) {
            if (e.which == 38 || 
                e.keyCode == 38 ||
                e.which == 40 || 
                e.keyCode == 40
            ) {
                scrollTo($this, ".highlight");
            }
        } : null;
    };

    var isOptionSelected = function(option) {
        if (!this.mutableValue) {
            return false;
        }

        if (typeof this.mutableValue == "object") {
            return this.mutableValue[this.useKey] == option[this.useKey];
        } else if (typeof option == "object") {
            return this.mutableValue == option[this.useKey];
        }

        return this.mutableValue == option;
    };

    var onModelChange = function(val) {
        this.search = this.mutableValue = (val || "");
        if (this.$el.hasClass("open")) {
            setIceClose(this.$el, (this.search) ? true : false);
        }
    };

    var onValueChange = function() {
        var el = this.$el;
        el.querySelectorAll("input")[0].style.opacity = (this.search) ? 0 : 1;
        if (this.disabled) {
            el.querySelectorAll("input")[0].style.opacity = 0;
        }
        this.pristine = false;
        if (!this.search) {
            this.search = this.mutableValue = "";
        }
    };

    var onDisableChange = function() {
        var el = this.$el;
        el.querySelectorAll(".dropdown-toggle")[0][this.disabled ? "addClass" : "removeClass"]("disabled");
        el.querySelectorAll("input")[0].style.opacity = 1;
        if (this.disabled) {
            var errorParent = el.closest(".error");
            el.querySelectorAll("input")[0].style.opacity = 0;
            if (errorParent) {
                errorParent.removeClass("error");
                _.each(errorParent.querySelectorAll(".text-error"), function(i) {
                    i.addClass("hidden");
                });
            }
        }
    };

    var clearText = function() {
        this.$emit("ico:clear-text");
    };
    // UTILITY
    var scrollTo = function($this, elementClass) {
        var parent = $this.querySelectorAll("ul")[0];
        var el = $this.querySelectorAll(elementClass)[0];
        if (el) {
            parent.scrollTop = el.offsetTop - el.offsetHeight;
        }
    };
    var escapeRegExp = function (str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    };
    var setIceClose = function($el, state) {
        var ele = $el.querySelectorAll(".ico-cancel-thin")[0];
        if (ele) {
            ele[state ? "removeClass" : "addClass"]("hidden");
        }
    };
    var setFocusClass = function($el, state) {
        var ele = $el.querySelectorAll(".dropdown-toggle")[0];
        ele[state ? "addClass" : "removeClass"]("focus");
    };

    return {
        init: init
    }
}();

Element.prototype.hasClass = function (cls) {
    return !!this.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
};

Element.prototype.addClass = function (cls) {
    if (!this.hasClass(cls)) this.className += " " + cls;
};

Element.prototype.removeClass = function (cls) {
    if (this.hasClass(cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        this.className = this.className.replace(reg, ' ');
    }
};

Element.prototype.removeElement = function () {
    this.parentNode.removeChild(this);
};

Element.prototype.attr = function (attr, val) {
    if (val) {
        this.setAttribute(attr, val);
    } else {
        return this.getAttribute(attr);
    }
};

Element.prototype.insertAfter = function (ele, isVueElement) {
    var t = document.createElement('template');
    t.innerHTML = ele;
    var content = (isVueElement) ? ele : t.content.cloneNode(true);
    this.parentNode.insertBefore(content, this.nextSibling);
};

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof Vue == "undefined") {
            return waitForLibraries(fn);
        }

        fn.init();
    }, 100);
};

$(function() {
    waitForLibraries(SIA.VueCustomSelect);
});