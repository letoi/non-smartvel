SIA.languageSelector = function() {
    var popUpClassName = ".country-selection";

    var init = function() {
        chooseLanguagePopup();
    };

    var chooseLanguagePopup = function() {
        $(".popup--terms-and-conditions:not(" + popUpClassName + ")").each(function() {
            $(this).remove();
        });
    };

    return {
        init: init
    }

}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.languageSelector.init();
        }
    }, 100);
};

$(function() {
    waitForUnderscore();
});