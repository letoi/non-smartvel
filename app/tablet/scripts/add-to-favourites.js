var SIA = SIA || {};

SIA.AddToFavouritesModule = function () {
	var p = {};

	var init = function () {

		if (
			$("body").hasClass("city-guide-page") ||
			$("body").hasClass("fares-details-page")
		) {
			if (hasFavourites) {
				$("[data-addto-favourites]").each(function() {
					var self = $(this);
					toggleFavouritesStar(self);
				});
			}

			clickFavouritesListener();
		}
	};

	var initPopupTriggerModal = function (triggerModal) {
		triggerModal.Popup({
			overlayBGTemplate: SIA.global.config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			afterHide: function (popup) {

			}
		});
	};

	var clickFavouritesListener = function() {
		$(".add-tofavourites").on("click", function(evt) {
			var self = $(this);
			saveFavourites(self, self.data("addto-favourites"));

			evt.preventDefault();
			evt.stopImmediatePropagation();
		});

		$("body").delegate(".popup__close", "click", function (evt) {
			evt.preventDefault();
			$(".popup").Popup("hide");
		});
	};

	var openPopUp = function() {
		var popUpModal = (loggedIn) ? ".popup--favourite" : ".popup--login-favourites";
		$(popUpModal).Popup("show");
		$(popUpModal).Popup("show");
		$('body').find('.bg-modal').addClass('overlay');
	}

	var saveFavourites = function(el, favouritesId) {
		$.get("ajax/add-favourites-response.json",
			function(response) {
				if (response.status) {
					if (loggedIn && !hasFavourites) {
						hasFavourites = true;
						openPopUp();
					} else if (!loggedIn) {
						openPopUp();
					}

					if (loggedIn) {
						toggleFavouritesStar(el);
					}
				}
			});
	};

	var toggleFavouritesStar = function(el) {
		var isActive = el.hasClass('active');
		el[(!isActive) ? "addClass" : "removeClass"]("active");
	};

	var pub = {
		init: init,
		p: p,
		clickFavouritesListener: clickFavouritesListener
	};

	return pub;
}();

// check if underscore is loaded
var waitForUnderscore = function () {
	setTimeout(function () {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.AddToFavouritesModule.init();
		}
	}, 100);
};

$(function () {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.AddToFavouritesModule.init();
});
