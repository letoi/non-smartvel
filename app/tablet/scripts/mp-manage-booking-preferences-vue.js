SIA.MPManageBookingPreferences = function() {

    var templateDate = {
        bookingOverviewData : {
            flights:[],
            destinationImage:""
        },
        passengerAddOnsData : {},
        tripAddon : {},
        openAccordion: "",
        activeTab: 0,
        passengerData:[]
    }

    var init = function() {
        Vue.use(VTooltip);
        Vue.component('vue-slide-up-down', VueSlideUpDown);
        Vue.component("manage-booking-preferences", {
            template: "#mp-manage-booking-template-preferences",
            data(){
                return templateDate
            },
            methods:{
                changeTab: changeTab,
                addAccordion: addAccordion,
                accordionIsActive: accordionIsActive,
                isActive:isActive
            },
            computed:{
            },
            mounted: onMount
        });

        new Vue({
            el: "#app-container"
        });
    };

    var isActive = function(index){
        if(index == this.activeTab){
            return true;
        }else{
            return false;
        }
    }
    
    var deepClone = function(list) {
        return JSON.parse(JSON.stringify(list));
    };

    var changeTab = function(index){
        var self = this;
        self.activeTab = index;
        self.openAccordion = [];
    }

    var arrangeJson = function(self){
        paxData = self.passengerAddOnsData;
        _.each(paxData.passengers, function(passenger){
            passenger.flightLevel = "undefined";
            passenger.changeSeat = "undefined";
            passenger.changeMeal = "undefined";
            passenger.firstName = passenger.firstName.charAt(0).toUpperCase() + passenger.firstName.substr(1).toLowerCase();
            passenger.lastName = passenger.lastName.charAt(0).toUpperCase() + passenger.lastName.substr(1).toLowerCase();
            passenger.flights = getFlights(passenger.passengerID,paxData);
            getPassengerPackDetails(passenger,paxData);
            _.each(paxData.flightLevelEligibiltyDetails, function(flightLevel){
                if(passenger.passengerID == flightLevel.paxId && flightLevel.eligibleToAddBaggage == "true"){
                    passenger.flightLevel = flightLevel;
                }
            });
            _.each(paxData.segmentLevelEligibiltyDetails, function(segmentLevel){
                if(passenger.passengerID == segmentLevel.paxId && segmentLevel.eligibleToChangeSeat == "true"){
                    passenger.changeSeat = segmentLevel;
                }
                if(passenger.passengerID == segmentLevel.paxId && segmentLevel.eligibleToChangeMeal == "true"){
                    passenger.changeMeal = segmentLevel;
                }
            });
        });
    }

    var getPassengerPackDetails = function(passenger,paxData){
        passenger.packDetails = [];
        _.each(paxData.packDetails,function(pack){
            if(pack.paxId == passenger.passengerID){
                passenger.packDetails.push(pack);
            }
        });
    }

    var getFlights = function(passengerId,paxData){
        flights = deepClone(paxData.flights);
        _.each(flights,function(flight){
            flight.seatDetails = "";
            _.each(paxData.seatDetails,  function(seat){
                if(seat.flightID == flight.flightID && seat.paxId == passengerId && flight.seatDetails == ""){
                    flight.seatDetails = seat;
                    return;
                }
            });

            _.each(flight.flightSegments, function(flightSegment){
                _.each(flightSegment.legs, function(leg){
                    _.each(paxData.seatDetails,  function(seat){
                        if(flightSegment.segmentID == seat.segmentID && seat.paxId == passengerId && flight.flightID == seat.flightID && seat.legID == leg.legID){
                            leg.seatDetails = seat;
                        }
                    });

                    _.each(paxData.mealDetails,  function(meal){
                        if(flightSegment.segmentID == meal.segmentID && meal.paxId == passengerId && flight.flightID == meal.flightID && meal.legID == leg.legID){
                            leg.mealDetails = meal;
                        }
                    });
                });
            });

            _.each(paxData.baggageDetails,  function(baggage){
                if(baggage.flightID == flight.flightID && baggage.paxId == passengerId){
                    flight.baggageDetails = baggage;
                }
            });
        });
        return flights;
    }


    // var getBackGround = function(){
    //     var self = this;
    //     return { 'background-image': 'url(' + self.bookingOverviewData.destinationImage + ')' }
    // }

    // var timeParser = function(time){
    //     var hours  = parseInt(time / (60*60));
    //     var mins = (time % (60*60)) / 60 ;
    //     return `${hours}hrs ${mins}mins`;
    // }

    var accordionIsActive = function(accordionName){
        var self = this;
        return self.openAccordion == accordionName;
    }

    var addAccordion = function(accordionName){
        var self = this;
        if(self.openAccordion == accordionName){
            self.openAccordion = "";
        }else if(self.openAccordion != ""){
            self.openAccordion = accordionName;
        }else{
            self.openAccordion = accordionName;
        }
    }
    
    // var isNotLast = function(key1,key2){
    //     var self = this;
    //     return key2 != self.bookingOverviewData.flights[key1].flightSegments.length - 1;
    // }

    var onMount = function() {
        var self = this;
        runRequest(self);
    };

    var runRequest = function(self){
        var apiRequest = [
            axios.get('ajax/JSONS/MP/ManageBookingOverView.json'),
            axios.get('ajax/JSONS/MP/PassengerAddons.json'),
            axios.get('ajax/JSONS/MP/TripAddon.json')
        ];
        axios.all(apiRequest)
            .then(axios.spread(function(bookingOverview, passengerAddOns, tripAddon) {
                self.bookingOverviewData = bookingOverview.data;
                self.passengerAddOnsData = passengerAddOns.data;
                self.tripAddon = tripAddon.data;
                arrangeJson(self);
            }));
    }

    return {
        init: init
    };
}();

var waitForLibraries = function(fn) {
    setTimeout(function() {
        if (typeof _ == "undefined" ||
            typeof Vue == "undefined" ||
            typeof VTooltip == "undefined" ||
            typeof axios == "undefined"
        ) {
            waitForLibraries(fn);
        } else {
            fn.init();
        }
    }, 100);
};

$(function() {
    waitForLibraries(SIA.MPManageBookingPreferences);
});