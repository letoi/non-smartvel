SIA.costBreakDown = function () {
    var global = SIA.global;
    var config = global.config;
    var costBreakDownTpl = "";
    var costBreakDownUrl = "";
    var appendBreakdownDetailTpl = $('.popup--flights-details').find('.popup__content');
    
    var checkBodyClass = function (classList) {
        var classStatus = false;
        _.each(classList, function (className) {
            if ($('body').hasClass(className)) {
                classStatus = true;
            }
        });
        return classStatus;
    };

    var mpCostBreakdownDetail = function (costBreakDownData) {
        $.get(costBreakDownTpl, function (data) {
            var template = window._.template(data, {
                data: costBreakDownData
            });
            var templateEl = $(template);
            appendBreakdownDetailTpl.append(templateEl);
        });
    };

    var renderCostBreakDown = function () {
        $.ajax({
            url: costBreakDownUrl,
            type: config.ajaxMethod,
            dataType: 'json',
            async: true,
            cache: false,
            success: function (response) {
                mpCostBreakdownDetail(response);
            }
        });
    };
    
    if (checkBodyClass(["mp-1-confirmation"])) {
        costBreakDownTpl = config.url.mpCostBreakdownTpl;
        costBreakDownUrl = config.url.bookingSummaryPanelJson;
        renderCostBreakDown();
    }
};