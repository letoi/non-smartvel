SIA.customeFormValidator = function(){
    var init = function() {
        addCustomValidate();
    };

    var addCustomValidate = function() {
        var optionText = [];
        $.validator.addMethod("exclude", function(value, element) {
            var optionEl = $(element).closest("[data-autocomplete]").find("select").find('option');
            optionText =  optionTextExtractor(optionEl);
            return optionText.indexOf(value.toLowerCase()) != -1;
        });
    };

    return {
        init: init
    }
}();

var optionTextExtractor = function(options){
    
    return _.map(options,function(el){
        el = $(el);
        return el.data('text').toLowerCase();
    })
};

var waitForJquery = function () {
	setTimeout(function () {
		if (typeof $.validator == 'undefined') {
			waitForJquery();
		} else {
			SIA.customeFormValidator.init();
		}
	}, 100);
};

$(function () {
	typeof $.validator == 'undefined' ? waitForJquery() : SIA.customeFormValidator.init();
});