var SIA = SIA || {};

SIA.StickyGroup = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;

	var p = {};
 // btn-grp-sticky
	var checkPosition = function(){
		var elTop = p.stickyGrp.position().top + p.header.height()+200;
		var elBtm = elTop + p.stickyGrp.height();
		var winTop = $(window).scrollTop()+50;
		var winBtm = winTop + $(window).height();

		if(elBtm > winTop && p.stickytarget.hasClass('sticky-component--show')){
			p.stickytarget.removeClass('sticky-component--show');
		}
		if(winBtm > elTop && p.stickytarget.hasClass('sticky-component--show')){
			p.stickytarget.removeClass('sticky-component--show');
		}

		// Check if element is off screen
		if( winTop > elBtm && !p.stickytarget.hasClass('sticky-component--show')) {
			p.stickytarget.addClass('sticky-component--show');
		}
		if( elTop > winBtm && !p.stickytarget.hasClass('sticky-component--show')) {
			p.stickytarget.addClass('sticky-component--show');
		}

		p.animKeyFrame = requestAnimationFrame(checkPosition);
	};

	var init = function(){
		p.stickyGrp = $('[data-sticky-group]');
		if(p.stickyGrp.attr('data-sticky-pos') === 'bottom') {
			p.stickytarget = $('[data-sticky-footer="'+p.stickyGrp.attr('data-sticky-group')+'"]');
		}

		p.header = $('.header');

		if (typeof p.stickyGrp.position() !== 'undefined') p.animKeyFrame = requestAnimationFrame(checkPosition);
	}

	var oPublic = {
		init: init
	};

	return oPublic;
}();

SIA.StickyGroup.init();
