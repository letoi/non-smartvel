var SIA = SIA || {};

SIA.FavouritesPage = function () {
	var p = {};

	p.global = SIA.global;
	p.body = p.global.vars.body;
	p.win = p.global.vars.win;

	p.promoData = globalJson.promoData;
	p.faveData = globalJson.favouritesData;
	
	p.data = [];

	p.favBlocks = [];
	p.expPavBlocks = [];
	p.accordionArr = [];
	p.hasExpired = false;
	p.hasFavourite = false;

	var geoCity, regCity;

	var init = function () {
		// Combine the data provided
		processData();

		// Add the destination blocks
		addBlocks();

		initAddDestFields();

		geoCity = p.body.data("geo-city");
		regCity = p.body.data("reg-city");

		if(typeof geoCity !== 'undefined' && typeof regCity !== 'undefined') checkIfDifferentCity();

		SIA.FavouritesFilter();
		useNativeSelect();

		setTimeout(function(){
			$("[data-faredealcity]").find("#fare-deal-city-1").trigger('blur');
		}, 100);

		window.addEventListener("orientationchange", function() {
			setTimeout(function(){
				repaint();
			}, 300);
		}, false);
	};

	var useNativeSelect = function() {
		$("[data-filters]").each(function() {
			var self = $(this);
			var nativeSelect = self.find("select");
			self.removeAttr("data-customselect");
			nativeSelect.removeClass("hidden");
			self.find(".input-overlay").remove();

			nativeSelect.off("change.nativeSelect").on("change.nativeSelect", function() {
				var textValue = $(this).find("option:selected").text();
				self.find(".select__text").text(textValue);
			});
		});
	};

	var checkIfDifferentCity = function() {
		var promptEl = $(".different-city-prompt");

		promptEl.find(".geo-city").text(geoCity);
		promptEl.find(".reg-city").text(regCity);

		if (geoCity.toLowerCase() !== regCity.toLowerCase()) {
			promptEl.removeClass("hidden");
		}
	};

	var processData = function () {
		for (var i = 0; i < p.faveData.favouriteDetail.favouriteCities.length; i++) {
			var curFav = p.faveData.favouriteDetail.favouriteCities[i];
			var promos = [];
			var exppromos = [];
			
			if(curFav.isFavourite) p.hasFavourite = true;

			for (var j = 0; j < p.promoData.promoVO[0].cityVO.length; j++) {
				var curPromo = p.promoData.promoVO[0].cityVO[j];
				if (curPromo.destCountry === curFav.destCountry && curPromo.destCityCode === curFav.destCityCode && curPromo.originCityName === curFav.originCityName) {
					curPromo.price = Number(curPromo.price);

					if(curPromo.isFavouriteCityExpired){
						exppromos.push(curPromo);
						p.hasExpired = true;
					}else {
						promos.push(curPromo);
					}
				}
			}
			
			curFav.promos = _.sortBy(promos, 'price');
			curFav.exppromos = _.sortBy(exppromos, 'price');

			p.data.push(curFav);
		}
	};

	var updateData = function(val, operator){
		if(operator === 'remove') {
			p.data.splice(val, 1);
			p.hasExpired = false;
			p.hasFavourite = false;

			for (var i = 0; i < p.data.length; i++) {
				var curFav = p.data[i];
				if(curFav.isFavourite) p.hasFavourite = true;

				if(curFav.exppromos.length > 0) p.hasExpired = true;
			}
		}
	};

	var initAddDestFields = function(){
		p.favouritesDisabledAddDesBlk = $('.favourites-disabled').find('.add-destination-block');
		p.formWrap = $('[data-add-dest-tofave]');
		p.firstDestSet = p.formWrap.find('[data-validate-by-group]').clone();
		p.firstDestSet.find('.destination-city.dropdown-text-align').attr('placeholder', 'Add another city');
		
		p.addDestToggle = $('[data-toggle-view]');
		p.toggleTarget = $('[data-toggle-view-target="'+p.addDestToggle.attr('data-toggle-view')+'"]');

		p.addDestToggle.on({
			'click': function(){
				p.toggleTarget.removeClass('hidden');
				p.addDestToggle.addClass('hidden');
			}
		});

		p.addDestCancel = p.toggleTarget.find('[data-add-dest-cancel]');
		p.addDestCancel.on({
			'click': function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				p.addDestToggle.addClass('hidden');
				$(this).closest('[data-toggle-view-target]').addClass('hidden');
				p.addDestToggle.removeClass('hidden');
			}
		});

		$("#addDestinationToFavourites").delegate("input[type='text']", "blur", function() {
			if (!$(this).val()) {
				removeExtraFields();
			}
		});
		if((p.faveData.favouriteDetail.favouriteCities.length) >= 20){
			$('.add-destination-favourites-form').addClass("disabled-button");
			p.favouritesDisabledAddDesBlk.find('.fav-disabled-info-max').removeClass('hidden');
			p.favouritesDisabledAddDesBlk.find('.button-group-1').addClass('add-bottom-px');
			p.favouritesDisabledAddDesBlk.find('.add-dest-to-favourites').addClass('add-city-placeholder-disable');
			p.favouritesDisabledAddDesBlk.find('[data-dest-add-favourites]').addClass('add-btn-disable');
		}
		if((p.faveData.favouriteDetail.favouriteCities.length) <= 19) {
			p.favouritesDisabledAddDesBlk.find('.fav-disabled-info-max').addClass('hidden');
			p.favouritesDisabledAddDesBlk.find('[data-dest-add-favourites]').removeClass('add-btn-disable');
		}
	};

	var addCityList = function(field){
		// Remove city from list
		p.firstDestSet.find('[data-text="'+field.val()+'"]').remove();

		// Add a new city set
		if(p.formWrap.find('[data-validate-by-group]').length < 10) {
			var haveEmptyFied = checkEmptyFields();
			if (!haveEmptyFied) {
				p.formWrap.append(p.firstDestSet.clone());
				SIA.initAutocompleteCity();
			}
			trimCityCode();
		}
	};

	var removeExtraFields = function() {
		var willRemoveExtraField = false;
		var inputElements = $("#addDestinationToFavourites").find("input[type='text']");
		if (inputElements.length == 1) {
			return;
		}

		var sortEl = $("#addDestinationToFavourites").find("[data-validate-by-group]");
		sortEl = sortEl.sort(function(a, b) {
			var aVal = $(a).find("input[type='text']").val().length ? 1 : 0;
			var bVal = $(b).find("input[type='text']").val().length ? 1 : 0;

			return aVal < bVal;
		});

		$(".add-destination-wrap").html("").append(sortEl);
		SIA.initAutocompleteCity();
		clearBtnReInitialize(sortEl);
		
		sortEl
			.each(function() {
				var self = $(this);
				var inputVal = self.find("input[type='text']").val();

				if (!inputVal) {
					if (!willRemoveExtraField) {
						willRemoveExtraField = true;
					} else {
						self.closest("[data-validate-by-group]").remove();
					}
				}
			});
	};

	var clearBtnReInitialize = function(inputElements) {
		inputElements.each(function() {
			var inputEl = $(this).find("input[type='text']")
			var el = $(this).find(".ico-cancel-thin");
			el.off().on("click", function(e) {
				e.preventDefault();
			
				inputEl.val('');
				inputEl.trigger('blur');
	
				$(this).addClass('hidden');
			});
		});
	};

	var checkEmptyFields = function () {
		var haveEmptyField = false;
		$("#addDestinationToFavourites").find("input[type='text']")
			.each(function() {
				var self = $(this);
				if (!self.val()) {
					haveEmptyField = true;
				}
			});
		
		return haveEmptyField;
	};

	var trimCityCode = function() {
		$("#addDestinationToFavourites").find("input[type='text']")
			.each(function() {
				var self = $(this);
				var text = self.val().match(/^(.+)-\s\w+$/i);
				
				if (text) {
					self.val(text[1]);
				}
			});
	};

	var addBlocks = function(){
		p.blckWrapper = $('#blkWrap');
		
		if(!p.hasExpired && !p.hasFavourite) addDefaultBlocks();

		if(p.hasExpired) addExpiredBlocks();
		if(p.hasFavourite) addFaveBlocks();

		var expiredBlk = p.blckWrapper.find('.expiring-deals-content');
		var normalBlk = p.blckWrapper.find('.fave-list--normal');

		expiredBlk.find('.dest-block').each(function(i){
			var t = $(this);
			t.removeClass('reposition');
			if(i % 2 == 0) t.addClass('reposition');

			var accordion = new SIA.CSSAccordion();
			p.accordionArr.push(accordion);

			accordion.init(t.find('[data-accordion-trigger]'), t.find('[data-accordion-content]'), p.accordionArr);
		});
		normalBlk.find('.dest-block').each(function(i){
			var t = $(this);
			t.removeClass('reposition');
			if(i % 2 == 0) t.addClass('reposition');

			if(!t.hasClass('dest-block-nofares')) {
				var accordion = new SIA.CSSAccordion();
				p.accordionArr.push(accordion);
				accordion.init(t.find('[data-accordion-trigger]'), t.find('[data-accordion-content]'), p.accordionArr);
			}
		});
	};

	var repaint = function(){
		var expiredBlk = p.blckWrapper.find('.expiring-deals-content');
		var normalBlk = p.blckWrapper.find('.fave-list--normal');
		expiredBlk.parent().addClass('hidden');
		expiredBlk.html('');
		normalBlk.html('');

		addBlocks();
	};

	var addExpiredBlocks = function(){
		var wrap = p.blckWrapper.find('.expiring-deals-content');
		wrap.parent().removeClass('hidden');

		var blkClassArr = ['two-third','one-third'];
		var clId = 0;

		for (var i = 0; i < p.data.length; i++) {
			var cData = p.data[i];
			
			if(!cData.isFavourite) continue;
			if(cData.exppromos.length === 0) continue;
			
			var dBlk = new SIA.DestinationBlock();
			dBlk.init(i, cData, blkClassArr[clId], true);

			if(clId < blkClassArr.length-1) {
				clId++;
			}else {
				clId = 0;
				blkClassArr.reverse();
			}
			
			wrap.append(dBlk.blk);

			p.expPavBlocks.push(dBlk);

		}
	};

	var addFaveBlocks = function(){
		var wrap = p.blckWrapper.find('.fave-list--normal');

		var blkClassArr = ['two-third','one-third'];
		var clId = 0;

		for (var i = 0; i < p.data.length; i++) {
			var cData = p.data[i];

			if(!cData.isFavourite) continue;
			
			var dBlk = new SIA.DestinationBlock();
			dBlk.init(i, cData, blkClassArr[clId], false);

			if(clId < blkClassArr.length-1) {
				clId++;
			}else {
				clId = 0;
				blkClassArr.reverse();
			}
			
			wrap.append(dBlk.blk);

			p.favBlocks.push(dBlk);
		}
	};

	// Add initial state(no added favourites)
	var addDefaultBlocks = function(){
		var wrap = p.blckWrapper.find('.fave-list--normal');
		var blkClassArr = ['two-third','one-third'];
		var clId = 0;
		for (var i = 0; i < p.data.length; i++) {
			var cData = p.data[i];

			if(!cData.isFavourite && cData.promos.length === 0) continue;
			
			var dBlk = new SIA.DestinationBlock();
			dBlk.init(i, cData, blkClassArr[clId], false);

			if(clId < blkClassArr.length-1) {
				clId++;
			}else {
				clId = 0;
				blkClassArr.reverse();
			}
			
			wrap.append(dBlk.blk);

			p.favBlocks.push(dBlk);
		}
	};

	var pub = {
		init: init,
		p: p,
		addCityList: addCityList,
		repaint: repaint,
		updateData: updateData
	};

	return pub;
}();

SIA.DestinationBlock = function () {
	var p = {};

	var tpls = {
		blockWrap: '<article class="promotion-item dest-block promotion-item--2 fadeIn animated <%= initClass %>" data-fave-id="<%= blkId %>">\
			<button class="promotion-item__inner css-accordion-trigger" data-accordion-trigger="<%= blkId %>" aria-expanded="false">\
				<div class="dest-blk-content-wrap">\
					<div class="flight-item"><img src="<%= imgPath %>" alt="photo-1" longdesc="img-desc.html">\
						<div class="country-name"><h3><%= cityName %></h3></div>\
						<a href="#" class="ico-container add-tofavourites<%= isFavourite %>" data-addto-favourites="<%= favouriteCityId %>">&nbsp;</a>\
					</div>\
					<div class="promotion-item__content">\
						<div class="promotion-item__desc">\
							<span class="expiring-deals<%= isExpiringDeal %>"><%= noExpiring %> expiring deals</span>\
							<span class="avail-fare-deals<%= isNewFareDeal %>">new fare deals available</span>\
							<p><%= cabin %> return from</p>\
							<h3 class="price"><%= currency %><%= cheapestPrice %><sup>*</sup></h3>\
							<em class="ico-point-r css-accordion-pointer">&nbsp;</em>\
						</div>\
					</div>\
					<div class="overlay-delete add-to-fave-controls">\
						<div class="promotion-item__content">\
							<p class="title-5--blue">Are you sure you want to remove <%= cityName %> from Favourites?</p>\
							<div class="button-group-1">\
								<a href="#" tab-index="-1" class="btn-1 right" data-favourite-id="<%= favouriteCityId %>" data-remove-favourites="<%= removeId %>">Confirm</a>\
								<a href="#" tab-index="-1" class="btn-2 right btn-cancel">Cancel</a>\
							</div>\
						</div>\
					</div>\
				</div>\
			</button>\
		</article>',
		accordionContentWrap: '<div data-accordion-content="<%= blkId %>" data-destination-id="<%= blkId %>" class="promotion-item__detail css-accordion-content dest-block--content">\
			<h3 class="title"><%= originCityName %> to <%= destinationCityName %></h3>\
			<div class="editor">\
			<table data-sort-content="true" class="fare-basic-table" data-room-table="true">\
				<caption class="hidden">Promotion item descriptions</caption>\
				<thead class="hidden-mb">\
					<tr>\
						<th class="th-5" scope="col">Prices from</th>\
						<th class="th-5 advance-purchase" scope="col">Advanced purchase</th>\
						<th class="th-5" scope="col">Book by</th>\
						<th class="th-5" scope="col"><%= travelPeriodLabel %></th>\
						<th class="th-5" scope="col">Min. pax to go</th>\
						<th class="th-5" scope="col"><span class="ui-helper-hidden-accessible">Table Heading</span></th>\
					</tr>\
				</thead>\
				<tbody data-promo-wrap="<%= blkId %>"></tbody>\
			</table>\
			</div>\
			<a href="#" class="close-btn">Close<em class="ico-close"></em></a>\
		</div>',
	  	promoRow: '<tr>\
			<td data-th="Prices from" aria-describedby="wcag-currency-<%= blkId %>">\
				<%= currency %> <%- price.toLocaleString() %><sup>*</sup>\
				<span class="ui-helper-hidden-accessible" id="wcag-currency-<%= blkId %>"><%= currency %> <%- price.toLocaleString() %>*</span></td>\
			<td data-th="Advanced purchase" aria-describedby="wcag-advance-<%= blkId %>">\
				<%= advancePurchase %> days<span class="ui-helper-hidden-accessible" id="wcag-advance-<%= blkId %>"><%= advancePurchase %> days</span>\
			</td>\
			<td data-th="Book by" class="book-by" aria-describedby="wcag-bookby-<%= blkId %>">\
			<%= bookBy %><span class="ui-helper-hidden-accessible" id="wcag-bookby-<%= blkId %>"><%= bookBy %></span>\
			</td>\
			<td data-th="Travel period" aria-describedby="wcag-duration<%= blkId %>">\
			<%= durationStartEn %> - <%= durationEndEn %>\
			<span class="ui-helper-hidden-accessible" id="wcag-duration-<%= blkId %>"><%= durationStartEn %> - <%= durationEndEn %></span>\
			</td>\
			<td data-th="Min pax to go" class="min-pax" aria-describedby="wcag-minpaxtogo-<%= blkId %>">\
				<span class="min-pax-hide"><%= minPaxToGoCount %></span>\
				<ul class="min-to-go" data-min-togo="<%= blkId %>">\
				<%= minPaxToGo %>\
				</ul>\
				<span class="ui-helper-hidden-accessible" id="wcag-minpaxtogo-<%= blkId %>"><%= minPaxToGoCount %></span>\
			</td>\
			<td>\
				<a href="<%= shareUrl %>" target="_blank" class="btn-1 dest-booknow">Book Now</a>\
			</td>\
		</tr>',
		blockNoFares: '<article class="promotion-item dest-block dest-block-nofares promotion-item--2 fadeIn animated <%= initClass %>" data-fave-id="<%= blkId %>">\
			<div class="promotion-item__inner css-accordion-trigger" data-accordion-trigger="<%= blkId %>" aria-expanded="false">\
				<div class="dest-blk-content-wrap">\
					<div class="flight-item"><img src="<%= imgPath %>" alt="photo-1" longdesc="img-desc.html">\
						<div class="country-name"><h3><%= cityName %></h3></div>\
						<a href="#" class="ico-container add-tofavourites<%= isFavourite %>" data-addto-favourites="<%= favouriteCityId %>">&nbsp;</a>\
					</div>\
					<div class="promotion-item__content" data-booking-widget="true">\
						<div class="search-flight-container">\
							<span class="no-fare">no fare deals available</span>\
							<p class="flight-search">\
								<em class="ico-point-r"></em>\
								<a href="#" class="link-8" data-bookingwidget-trigger="true">Search flights</a>\
							</p>\
						</div>\
					</div>\
					<div class="overlay-delete add-to-fave-controls">\
						<div class="promotion-item__content">\
							<p class="title-5--blue">Are you sure you want to remove <%= cityName %> from Favourites?</p>\
							<div class="button-group-1">\
								<a href="#" tab-index="-1" class="btn-1 right" data-favourite-id="<%= favouriteCityId %>" data-remove-favourites="<%= removeId %>">Confirm</a>\
								<a href="#" tab-index="-1" class="btn-2 right btn-cancel">Cancel</a>\
							</div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</article>'
	};

	p.data = [];
	p.id = 0;

	var init = function (id, data, clName, expired) {
		p.data = data;
		p.id = id;
		p.expired = expired;

		p.bookingWidget = $('.booking-widget.sticky');
		p.bookingWidgetClose = p.bookingWidget.find('.sticky__close');

		

		paintBlock(clName);
		initAddFaveBtn();
		if(p.data.promos.length === 0) {
			pub.blk.find('[data-bookingwidget-trigger]').on({
				'click': function(e){
					e.preventDefault();
					e.stopImmediatePropagation();

					p.bookingWidget.addClass('active');
				}
			});

			p.bookingWidgetClose.on({
				'click': function(e){
					e.preventDefault();
					e.stopImmediatePropagation();

					p.bookingWidget.removeClass('active');
				}
			});
			// console.log("triggered!!!!");
			checkRadio();
		}
	};

	var paintBlock = function(clName){
		var imgPath = p.data.imgUrl;
		if(clName === 'two-third') {
			imgPath = p.data.imgUrlWide;
		}
		
		if(p.id % 2 === 0) clName = clName + ' reposition';

		var isNewFareDeal = p.data.isNewFaredeal ? '' : ' hidden';
		var isFavourite = p.data.isFavourite ? ' active' : '';
		var isExpiringDeal = p.expired ? '' : ' hidden';
		var curPromoObj = p.data.promos;
		var noExpiring = p.data.exppromos.length ? p.data.exppromos.length : '';

		if(p.expired) curPromoObj = p.data.exppromos;

		// Add block wrapper
		if(curPromoObj.length > 0) {
			pub.blk = $(_.template(tpls.blockWrap, {
				'blkId': p.data.favouriteCityId,
				'initClass': clName,
				'imgPath': imgPath,
				'cityName': p.data.destinationCityName,
				'cheapestPrice': curPromoObj[0].price,
				'currency': curPromoObj[0].currency,
				'cabin': curPromoObj[0].cabin,
				'isNewFareDeal': isNewFareDeal,
				'favouriteCityId': p.data.favouriteCityId,
				'isFavourite': isFavourite,
				'isExpiringDeal': isExpiringDeal,
				'noExpiring': noExpiring,
				'removeId': p.id
			}));

			// Add promos in the wrapper
			var promoAccordionWrapper = $(_.template(tpls.accordionContentWrap, {
				'blkId': p.id,
				'originCityName': p.data.originCityName,
				'destinationCityName': p.data.destinationCityName,
				'travelPeriodLabel': curPromoObj[0].travelPeriodLabel
			}));

			var accordionWrapper = promoAccordionWrapper.find('[data-promo-wrap="'+p.id+'"]');

			for(var i = 0; i < curPromoObj.length; i++) {
				var promo = curPromoObj[i];

				var minPaxToGoHtml = '';
				for (var j = 0; j < promo.minPaxToGo; j++) {
					minPaxToGoHtml = minPaxToGoHtml + '<li><em class="ico-user"></em></li>';
				}

				var promoRow = $(_.template(tpls.promoRow, {
					'blkId': p.id,
					'originCityName': promo.originCityName,
					'destinationCityName': promo.destinationCityName,
					'price': promo.price,
					'currency': promo.currency,
					'advancePurchase': promo.advancePurchase,
					'bookBy': promo.bookByEn,
					'travelPeriodLabel': promo.travelPeriodLabel,
					'durationStartEn': promo.durationStartEn,
					'durationEndEn': promo.durationEndEn,
					'minPaxToGo': minPaxToGoHtml,
					'minPaxToGoCount': promo.minPaxToGo,
					'shareUrl': promo.shareurl
				}));

				accordionWrapper.append(promoRow);
			}

			pub.blk.append(promoAccordionWrapper);
		}else {
			pub.blk = $(_.template(tpls.blockNoFares, {
				'blkId': p.data.favouriteCityId,
				'initClass': clName,
				'imgPath': imgPath,
				'cityName': p.data.destinationCityName,
				'isNewFareDeal': isNewFareDeal,
				'favouriteCityId': p.data.favouriteCityId,
				'isFavourite': isFavourite,
				'isExpiringDeal': isExpiringDeal,
				'noExpiring': noExpiring,
				'removeId': p.id
			}));
		}

	};

	var updateFavouritesByIdx = function(currIdx) {
		_.each(SIA.FavouritesPage.p.data, function(faveData, idx) {
			if (faveData.favouriteCityId == currIdx) {
				SIA.FavouritesPage.p.data[idx].isFavourite = true;
			}
		});
	};

	var initAddFaveBtn = function(){
		p.btn = pub.blk.find('[data-addto-favourites]');
		p.overlay = pub.blk.find('.add-to-fave-controls');
		p.loader = $('.overlay-loading');

		p.btn.on({
			'click': function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				var self = $(this);
				var accordionTrigger = self.closest(".promotion-item").find("[data-accordion-trigger]");
				
				if (accordionTrigger.hasClass("active")) {
					accordionTrigger.trigger("click");
				}
				
				if (!self.hasClass("active")) {
					self.addClass("active");
					var idx = self.data("addto-favourites");
					// updateFavouritesByIdx(idx);
					// SIA.FavouritesPage.repaint();

					$.ajax({
						type: "GET",
						url: "ajax/add-favourites-response.json",
						dataType: "json",
						data: idx,
						success: function (msg) {
							window.location.href = 'favourites-complete.html';
						}
					});
				} else {
					p.overlay.addClass('active');
					p.overlay.find('.btn-cancel').attr('tabindex', '0');
					p.overlay.find('[data-remove-favourites]').attr('tabindex', '0');
				}
			}
		});

		p.overlay.on({
			"click": function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				p.overlay.removeClass('active');
			}
		});

		p.overlay.find('.btn-cancel').on({
			'click': function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				p.overlay.removeClass('active');

				p.overlay.find('.btn-cancel').attr('tabindex', '-1');
				p.overlay.find('[data-remove-favourites]').attr('tabindex', '-1');
			}
		});

		p.overlay.find('[data-remove-favourites]').on({
			'click': function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				var t = $(this);
				p.loader.removeClass('hidden');
				
				$.ajax({
					type: "GET",
					url: "ajax/add-favourites-response.json",
					dataType: "json",
					data: t.attr('data-favourite-id'),
					success: function (msg) {
						if(msg.status === true){
							SIA.FavouritesPage.updateData(t.attr('data-remove-favourites'), 'remove');
							if(SIA.FavouritesPage.p.hasExpired || SIA.FavouritesPage.p.hasFavourite) {
								SIA.FavouritesPage.repaint();
							}else {
								window.location.href = 'favourites-initial-login.html';
							}

							p.loader.addClass('hidden');
						}else {
							p.loader.addClass('hidden');
						}

						p.overlay.removeClass('active');
						p.overlay.find('.btn-cancel').attr('tabindex', '-1');
						p.overlay.find('[data-remove-favourites]').attr('tabindex', '-1');
					}
				});
			}
		});
	};

	var pub = {
		init: init,
		p: p
	};

	return pub;
};

// Simple CSS driven accordion
SIA.CSSAccordion = function () {
	var p = {};

	var init = function (trigger, content, accordionArr) {
		p.content = content;
		p.trigger = trigger;
		p.accordionArr = accordionArr;

		content.attr('data-content-height', content.height());
		content.css({
			'height': 0
		});

		// If destination block
		if(content.hasClass('dest-block--content')) {
			var leftOffset = trigger.offset().left - trigger.parent().parent().offset().left - 23;
			content.css({
				'left': '-' + leftOffset + 'px'
			})
		}

		trigger.on({
			'click': function(e){
				e.stopImmediatePropagation();

				// Close other accordions
				if(!$(this).hasClass('active')) {
					for(var i = 0; i < p.accordionArr.length; i++) {
						p.accordionArr[i].toggleContent(false);
					}
				}

				$(this).toggleClass('active');
				toggleContent($(this).hasClass('active'));

				setTimeout(function(){
					$('html,body').animate({ scrollTop: trigger.offset().top }, 'slow');
				}, 400);

			}
		});

		content.find('.close-btn').on({
			'click': function(e){
				e.preventDefault();
				toggleContent(false);
				trigger.removeClass('active');
			}
		});
	};

	var toggleContent = function(toggle){
		if(toggle) {
			p.content.addClass('active');
			p.content.css({
				'height': p.content.attr('data-content-height') + 'px'
			});
		}else {
			p.content.removeClass('active');
			p.content.css({
				'height': 0
			});

			p.trigger.removeClass('active');
		}
	};

	var pub = {
		init: init,
		p: p,
		toggleContent: toggleContent
	};

	return pub;
};

SIA.FavouritesFilter = function() {
	var pCopy = JSON.parse(JSON.stringify(SIA.FavouritesPage.p.data));
	var filterCategory = {};
	
	var initSearchFrom = function(){
		var destCountries = globalJson.favouritesData.favouriteDetail.destCountry;
		$("#country-list").append($('<option value="All" data-text="All" selected=selected>All</option>'));
		for(var i = 0; i < destCountries.length; i++) {
			var country = destCountries[i];
			$("#country-list").append($('<option value="'+country.code+'" data-text="'+country.description+'">'+country.locDescription+'</option>'));
		}

		var tripType = globalJson.favouritesData.favouriteDetail.tripType.reverse();
		for(var i = 0; i < tripType.length; i++) {
			var type = tripType[i];
			$("#trip-type").append($('<option value="'+type+'" data-text="'+type+'">'+type+'</option>'));
		}

		var tripClass = globalJson.favouritesData.favouriteDetail.availableClass;
		for(var i = 0; i < tripClass.length; i++) {
			var cl = tripClass[i];
			$("#trip-class").append($('<option value="'+cl+'" data-text="'+cl+'">'+cl+'</option>'));
		}

		var favoriteCities = SIA.FavouritesPage.p.data;
		var originCities = [];
		for(var i = 0; i < favoriteCities.length; i++) {
			var newOriginCity = favoriteCities[i].originCityName;
			var hasDupe = false;
			// check if has duplicate
			for(var j = 0; j < originCities.length; j++) {
				var originCity = originCities[j];
				if(originCity === newOriginCity) {
					hasDupe = true;
				}
			}
			
			if(!hasDupe) {
				originCities.push(newOriginCity);
			}
		}

		for(var k = 0; k < originCities.length; k++) {
			var city = originCities[k];
			$('[data-filter-favourites]').append($('<option value="'+city+'" data-text="'+city+'"'+(i === 0 ? 'selected=selected' : '')+'>'+city+'</option>'));
		}

		$('[data-filter-favourites-input]').val(originCities[0]);
	};

	var getValues = function() {
		filterCategory.city = $("#fare-deal-city-1").val();
		filterCategory.country = $("#country-list").val();
		filterCategory.tripType = $("#trip-type").val();
		filterCategory.cabin = $("#trip-class").val();
	};

	var filterData = function(id) {
		var filtered = [];
		filtered = _.filter(pCopy, function(item) {
			var conditions = [
				item.cabin.toLowerCase() == filterCategory.cabin.toLowerCase()
			];

			if (filterCategory.country.toLowerCase() != "all") {
				conditions.push(item.destCountry.toLowerCase() == filterCategory.country.toLowerCase());
			}

			if (filterCategory.tripType.toLowerCase() != "all") {
				conditions.push(item.tripType.toLowerCase() == filterCategory.tripType.toLowerCase());
			}

			if (filterCategory.city.toLowerCase() != '') {
				conditions.push(item.originCityName.toLowerCase() == filterCategory.city.toLowerCase());
			}

			if (conditions.indexOf(false) == -1) {
				return item;
			}
		});

		return filtered;
	};

	initSearchFrom();

	$("[data-customselect]").find("select").on("change", function() {
		var self = $(this);
		getValues();
		var filtered = filterData(self.attr("id"));
		SIA.FavouritesPage.p.data = filtered;
		if (!self.is("#sticky-cabin-2") && !self.is("#sticky-cabin-3") && !self.is("#sticky-cabin-4")) {
			SIA.FavouritesPage.repaint();
		}
	});
	$("[data-faredealcity]").find("#fare-deal-city-1").on("blur", function() {
		var self = $(this);
		getValues();
		var filtered = filterData(self.attr("id"));
		SIA.FavouritesPage.p.data = filtered;
		SIA.FavouritesPage.repaint();
	});
};
var checkRadio = function(){
    var radioElement = $('.favourites-page').find('.form-booking-widget').find('input:radio');
	var targetElement = $('.favourites-page').find('[data-target]');
	radioElement.each(function(idx){
		var self = $(this);
		if(self.is(':checked')){
			if(idx){
			  targetElement.eq(1).removeClass('hidden');
			  targetElement.eq(0).addClass('hidden');
			}
			else{
			  targetElement.eq(0).removeClass('hidden');
			  targetElement.eq(1).addClass('hidden');
			}
		  }
		  self.off('change.stickyWidget').on('change.stickyWidget', function () {
			if (self.is(':checked')) {
				if (idx) {
					targetElement.eq(1).removeClass('hidden');
					targetElement.eq(0).addClass('hidden');
				} else {
					targetElement.eq(0).removeClass('hidden');
					targetElement.eq(1).addClass('hidden');
				}
			}
		});
	});
};

// check if underscore is loaded
var waitForUnderscore = function () {
	setTimeout(function () {
		if (typeof _ == 'undefined') {
			waitForUnderscore();
		} else {
			SIA.FavouritesPage.init();
		}
	}, 100);
};

$(function () {
	typeof _ == 'undefined' ? waitForUnderscore() : SIA.FavouritesPage.init();
});