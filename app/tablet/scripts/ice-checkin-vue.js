SIA.MpIceCheckIn = function () {
    var templateInstance;

    var templateData = {
        bookReference: "",
        departureCity: "",
        arrivalCity: "",
        flightSegments: [],
        flightsInOut: [],
        flightCabinClass: "",
        delayedFlightNotice: "",
        isAutoCheck: false
    };
    var getTemplateMethods = function () {
        return {
            toggleAccordion: toggleAccordion,
            checkAll: checkAll,
            checkSelectAll: checkSelectAll,
            canSelectAll: canSelectAll,
            canSelectAllState: canSelectAllState,
            isAdultCheckedInLegs: isAdultCheckedInLegs,
            checkboxIsSelected: checkboxIsSelected
        }
    };

    var getTemplateFilters = function () {
        return {
            dateParser: dateParser,
            timeParser: timeParser
        }
    };

    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var init = function () {
        Vue.component('vue-slide-up-down', VueSlideUpDown);
        Vue.component('ice-checkin', {
            template: "#ice-checkin-template",
            data: function () {
                return templateData
            },
            props: {
                platform: String
            },
            mounted: onMount,
            methods: getTemplateMethods(),
            filters: getTemplateFilters()
        });

        templateInstance = new Vue({
            el: "#app-container",
            data: {
                platform: "desktop"
            }
        });
    };

    var onMount = function () {
        var $this = this;
        var url = SIA.URLParamParser.getURLParams('json') || "ICE_PaxDetailsJson_Online.json";
        $.get(`ajax/JSONS/ICE/${url}`, function (response) {
            var data = response.paxRecordVO;
            var flights = data.flights;
            var prevOrigin = "";
            var prevDestination = "";

            var outbound = [flights[0]];
            var inbound = [];

            if (flights[0].origin.airportCode == flights[flights.length - 1].destination.airportCode) {
                //TO-DO: DETERMINE THE OUTBOUND AND INBOUND FLIGHTS AND PUSH THEM INTO an array
                _.each(flights, function (flight, index) {
                    if (index > 0) {
                        if ((prevDestination == flight.origin.airportCode) && (prevOrigin != flight.destination.airportCode) && (flights[0].origin.airportCode != flight.destination.airportCode)) {
                            outbound.push(flight);
                        } else {
                            inbound.push(flight);
                        }
                    }
                    prevOrigin = flight.origin.airportCode;
                    prevDestination = flight.destination.airportCode
                });

                outbound = addPassengerInFlights($this.flightSegments, outbound, data.passengers);
                inbound = addPassengerInFlights($this.flightSegments, inbound, data.passengers);

                $this.flightsInOut.push(outbound);
                $this.flightsInOut.push(inbound);
            } else {
                //TO-DO: NO RETURN FLIGHT PAINT DIRECTLY
                $this.flightSegments = _.map(flights, function (segment, index) {
                    segment.active = (index == 0) ? true : false;
                    segment.isSelectedAll = false;
                    segment.passengers = deepClone(_.map(data.passengers, function (passenger) {
                        passenger.isInfant = (passenger.passengerType == "IN") ? true : false;
                        passenger.canBeSelected = (index == 0) ? true : false;
                        passenger.isDisabled = (index == 0 && !(passenger.passengerType == "IN")) ? false : true;
                        passenger.isAutoCheck = false;
                        passenger.isSelected = false;
                        return passenger;
                    }));
                    return segment;
                });
                $this.flightsInOut.push($this.flightSegments);
            }

            $this.bookReference = data.recordLocator;
            $this.departureCity = flights[0].origin.cityName;
            $this.arrivalCity = flights[flights.length - 1].destination.cityName;
            $this.flightCabinClass = (flights[0] || {}).cabinClass;
            $this.delayedFlightNotice = (flights[0] || {}).delayedFlightNotice;
        })
    };

    var addPassengerInFlights = function (flightSegments, flights, passengers) {
        return _.map(flights, function (segment, index) {
            segment.active = (index == 0) ? true : false;
            segment.isSelectedAll = false;
            segment.passengers = deepClone(_.map(passengers, function (passenger) {
                passenger.isInfant = (passenger.passengerType == "IN") ? true : false;
                passenger.canBeSelected = (index == 0) ? true : false;
                passenger.isDisabled = (index == 0 && !(passenger.passengerType == "IN")) ? false : true;
                passenger.isAutoCheck = false;
                passenger.isSelected = false;
                return passenger;
            }));
            flightSegments.push(segment);
            return segment;
        });
    }

    var dateParser = function (date) {
        var date = new Date(date);
        var dayLabel = days[date.getDay()];
        var dateLabel = (date.getDate().toString().length == 1) ? "0" + date.getDate() : date.getDate();
        var monthLabel = months[date.getMonth()];

        return `${dayLabel} ${dateLabel} ${monthLabel}`;
    };

    var timeParser = function (date) {
        var date = new Date(date);
        var hourLabel = (date.getHours().toString().length == 1) ? "0" + date.getHours() : date.getHours();
        var minuteLabel = (date.getMinutes().toString().length == 1) ? "0" + date.getMinutes() : date.getMinutes();

        return `${hourLabel}:${minuteLabel}`;
    };

    var toggleAccordion = function (flights) {
        flights.active = !flights.active;
    };

    var checkAll = _.debounce(function (flightInOut, flights, segmentIndex) {
        checkboxState(flights, flights.isSelectedAll);
        for (var passengerIndex = 0; passengerIndex < flights.passengers.length; passengerIndex++) {
            canSelectPassenger(flightInOut, flights, segmentIndex, passengerIndex);
        }
    }, 50);

    var checkSelectAll = _.debounce(function (flightInOut, flights, segmentIndex, passengerIndex) {
        flights.isSelectedAll = getCheckBoxAllStatus(flights, "&&");
        canSelectPassenger(flightInOut, flights, segmentIndex, passengerIndex);
    }, 50);

    //FOR CHECKING IF THERE'S ANY OF THE CHECKBOX SELECTED
    var checkboxIsSelected = function () {
        var result = _.reduce(this.flightSegments, function (segmentMem, segment) {
            var isSelected = _.reduce(segment.passengers, function (passengerMem, i) {
                return passengerMem || i.isSelected;
            }, false);

            return segmentMem || isSelected;
        }, false);

        return result;
    };

    var canSelectPassenger = function (flightSegments, flights, segmentIndex, passengerIndex) {
        var currentLegflights = flightSegments[segmentIndex];

        if (isAdultCheckedInLegs(flights)) {
            updateInfantState(currentLegflights.passengers, false);
        } else {
            updateInfantState(currentLegflights.passengers, true);
        }

        var rci = !flightSegments[segmentIndex + 1] ? null : currentLegflights.rci;
        var tci = !flightSegments[segmentIndex + 1] ? null : currentLegflights.tci;
        var stopOver = flightSegments[segmentIndex + 1] ? flightSegments[segmentIndex + 1].stopOver : currentLegflights.stopOver;

        if (rci && (!tci || tci == null) && (!stopOver || stopOver == null)) {
            isAutoCheck = false;
        } else if (!rci && tci && !stopOver) {
            isAutoCheck = true;
        } else if (!rci && tci && stopOver) {
            isAutoCheck = false;
        } else if (rci && tci && !stopOver) {
            isAutoCheck = false;
        } else if (rci && tci && stopOver) {
            isAutoCheck = false;
        } else {
            isAutoCheck = false;
        }

        if (flightSegments[segmentIndex + 1]) {
            var nextLegflights = flightSegments[segmentIndex + 1] ? flightSegments[segmentIndex + 1] : null;
            var passenger = nextLegflights.passengers[passengerIndex];

            var haveSelectedPassenger = currentLegflights.passengers[passengerIndex].isSelected;

            passenger.canBeSelected = haveSelectedPassenger;
            flightSegments[segmentIndex + 1].active = true;
            if (!haveSelectedPassenger) {
                passenger.isSelected = false;
            } else if (isAutoCheck) {
                passenger.isSelected = true;
                passenger.isAutoCheck = true;
            }
            nextLegflights.isSelectedAll = getCheckBoxAllStatus(nextLegflights, "&&");
            passenger.isDisabled = !haveSelectedPassenger;

            if (isAdultCheckedInLegs(flights)) {
                updateInfantState(nextLegflights.passengers, false);
            } else {
                updateInfantState(nextLegflights.passengers, true);
            }
        }
    };

    var updateInfantState = function (passengers, state) {
        _.each(passengers, function (passenger) {
            if (passenger.passengerType == "IN") {
                passenger.canBeSelected = !state;
                passenger.isDisabled = state;
                if (state && passenger.isSelected) {
                    passenger.isSelected = !state;
                }
            }
        });
    };

    var isAdultCheckedInLegs = function (flightLeg) {
        return _.reduce(flightLeg.passengers, function (passengerMem, passenger) {
            return passengerMem || (passenger.passengerType == "A" && passenger.isSelected) ? true : false;
        }, false);

    };

    var canSelectAll = function (flights) {
        return !_.chain(flights.passengers)
            .map(function (passenger) {
                return passenger.isSelected;
            })
            .reduce(function (memory, current) {
                return memory || current;
            }, false)
            .value();
    };

    var canSelectAllState = function (flights) {
        return _.chain(flights.passengers)
            .map(function (passenger) {
                return passenger.isAutoCheck;
            })
            .reduce(function (memory, current) {
                return memory || current;
            }, false)
            .value();
    };

    // UTILITIES
    var deepClone = function (list) {
        return JSON.parse(JSON.stringify(list));
    };

    var checkboxState = function (flights, state) {
        _.each(flights.passengers, function (passenger) {
            if (passenger.canBeSelected || (passenger.passengerType == "IN" && !passenger.canBeSelected)) {
                passenger.isSelected = state;
            }
        });

        flights.isSelectedAll = state;
    };

    var getCheckBoxAllStatus = function (flights, operator) {
        if (!flights.passengers.length) {
            return false;
        }

        return _.chain(flights.passengers)
            .map(function (passenger) {
                return passenger.isSelected;
            })
            .reduce(function (memory, bool) {
                switch (operator) {
                    case "&&":
                        return memory && bool;
                    case "||":
                        return memory || bool;
                }

            }, (operator == "&&") ? true : false)
            .value();
    };

    return {
        init: init
    };
}();

var waitForLibraries = function () {
    setTimeout(function () {
        if (typeof _ == "undefined" || typeof Vue == "undefined") {
            waitForLibraries();
        } else {
            SIA.MpIceCheckIn.init();
        }
    }, 100);
};

$(function () {
    waitForLibraries();
});