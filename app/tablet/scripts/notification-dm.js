/**
 * @name SIA
 * @description Define global landingSearchFlight functions
 * @version 1.0
 */

SIA.landingSearchFlight = function() {
	var global = SIA.global;
	var win = global.vars.win;

	var init = function() {
		$("#travel-radio-1").click(function() {
			changeImgPlaceholder("LINE");
		});
		$("#travel-radio-2").click(function() {
			changeImgPlaceholder("WECHAT");
		});
		$("#travel-radio-3").click(function() {
			changeImgPlaceholder("FB");
		});
	};

	var changeImgPlaceholder = function(imgSource) {

		$(".qr-placeholder").attr("src", "images/QR_BAR_" + imgSource + ".png");
	};

	init();
}();
