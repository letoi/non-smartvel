var SIA = SIA || {};

SIA.AjaxCaller = function () {
  var call = function (url, method, data) {
    var defer = $.Deferred();

    $.ajax({
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      url: url,
      data: JSON.stringify(data),
      type: method || 'GET',
      async: true,
      success: function(response) {
        defer.resolve(response);
      },
      error: function(error) {
        // console.error(error);
        defer.reject(error);
      },
    });

    return defer.promise();
  };

  var getTemplate = function(tplFile) {
    var templatePath = 'template/' + tplFile;

    var defer = $.Deferred();

    $.ajax({
      url: templatePath,
      type: 'GET',
      async: true,
      success: function(response) {
        defer.resolve(response);
      },
      error: function(error) {
        console.error(error);
        defer.reject(error);
      },
    });

    return defer.promise();
  };

  var getJsonContent = function(jsonFile) {

    var jsonPath = 'ajax/' + jsonFile;

    var defer = $.Deferred();

    $.ajax({
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      url: jsonPath,
      type: 'GET',
      async: true,
      success: function(response) {
        defer.resolve(response);
      },
      error: function(error) {
        console.error(error);
        defer.reject(error);
      },
    });

    return defer.promise();
  };

  return {
    api: call,
    template: getTemplate,
    json: getJsonContent
  };
}();
