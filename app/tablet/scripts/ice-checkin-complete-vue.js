SIA.MpIceCheckInComplete = function () {
    var templateInstance;

    var templateData = {
        bookReference: "",
        departureCity: "",
        arrivalCity: "",
        flightSegments: [],
        flightCabinClass: "",
        delayedFlightNotice: "",
        essentialInformation: {
            counterOperatingTime: {}
        }
    };
    var getTemplateMethods = function () {
        return {
            getFlightSegment: getFlightSegment,
            getIndexOfValue: getIndexOfValue
        }
    };
    var getMainMethods = function () {
        return {
            onAddEmail: onAddEmail,
            removeEmailFields: removeEmailFields,
            validateEmail: validateEmail,
            onSubmitValidation: onSubmitValidation,
            focusInput: focusInput,
            getIndexOfValue: getIndexOfValue,
            onAddEmailBoardingPasses: onAddEmailBoardingPasses
        }
    };

    var getTemplateFilters = function () {
        return {
            dateParser: dateParser,
            timeParser: timeParser
        }
    };

    var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var init = function () {
        Vue.use(VTooltip);
        Vue.component('ice-checkin-complete', {
            template: "#ice-checkin-complete-template",
            data: function () {
                return templateData
            },
            props: {
                platform: String
            },
            mounted: onMount,
            methods: getTemplateMethods(),
            filters: getTemplateFilters()
        });

        templateInstance = new Vue({
            el: "#app-container",
            data: {
                platform: "desktop",
                emailSendCheckIn: [{isError: false}],
                emailGetBoarding: [{}],
                flightSegments: [{}]
            },
            methods: getMainMethods()
        });
    };

    var onMount = function () {
        var $this = this;
        $.get("ajax/JSONS/ICE/ICE_PaxDetailsJson.json", function (response) {
            var data = response.paxRecordVO;
            var flights = data.flights;
            $this.bookReference = data.recordLocator;
            $this.departureCity = flights[0].origin.cityName;
            $this.arrivalCity = flights[flights.length - 1].destination.cityName;
            $this.flightSegments = _.map(flights, function (segment, index) {
                segment.passengers = deepClone(_.map(data.passengers, function (passenger, index) {
                    return passenger;
                }));

                segment.checkedInPassengersData = data.services.filter(function (checkedInPassengersData) {
                    return flights[index].flightIDs.indexOf(checkedInPassengersData.flightID) > -1 && checkedInPassengersData.dcsStatus.checkedIn == true;
                });
                return segment;
            });
            $this.flightCabinClass = (flights[0] || {}).cabinClass;
            $this.delayedFlightNotice = (flights[0] || {}).delayedFlightNotice;
            $this.essentialInformation = data.essentialInformation;
        })
    };

    var dateParser = function (date) {
        var date = new Date(date);
        var dayLabel = days[date.getDay()];
        var dateLabel = (date.getDate().toString().length == 1) ? "0" + date.getDate() : date.getDate();
        var monthLabel = months[date.getMonth()];

        return `${dayLabel} ${dateLabel} ${monthLabel}`;
    };

    var timeParser = function (date) {
        var date = new Date(date);
        var hourLabel = (date.getHours().toString().length == 1) ? "0" + date.getHours() : date.getHours();
        var minuteLabel = (date.getMinutes().toString().length == 1) ? "0" + date.getMinutes() : date.getMinutes();

        return `${hourLabel}:${minuteLabel}`;
    };

    var validateEmail = function (event, emails) {
        $(event.target).parent('span.input-1').removeClass("focus");
        if (isEmailValid(event.target.value) || !event.target.value) {
            emails.isError = false;
        } else {
            emails.isError = true;
        }
    };

    var isEmailValid = function (email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    };

    var onSubmitValidation = function (emails) {
        _.each(emails, function(email) {
            email.isError = !isEmailValid(email.address) && !!email.address;
        });
    }

    var onAddEmail = function () {
        this.emailSendCheckIn.push({isError: false});
        recenterLightBox();
    };

    var onAddEmailBoardingPasses = function () {
        this.emailGetBoarding.push({});
    };

    var removeEmailFields = function (index) {
        this.emailSendCheckIn.splice(index, 1);
        recenterLightBox();
    };

    var focusInput = function (event) {
        $(event.target).parent('span.input-1').addClass("focus");
    }

    var recenterLightBox = function () {
        var emailSendCheckInElement = $("aside.popup--check-in-email > div.popup__inner > div");
        var emailSendCheckInElementTop = ($(window).height()) / 2 - ((emailSendCheckInElement.height() + 65) / 2);
        emailSendCheckInElement.css({
            top: emailSendCheckInElementTop
        });
    };

    var getFlightSegment = function (passengers, checkedInPassengers) {
        var checkedInData = [];
        _.each(checkedInPassengers, function(checkedInPassenger) {
            _.each(passengers, function(passenger) {
                if (passenger.passengerID == checkedInPassenger.passengerID) {
                    checkedInData.push({firstName: passenger.firstName, lastName: passenger.lastName, isError: false})
                }
            });
        });
        this.$emit("load-flight", checkedInData);
    };

    // UTILITIES
    var deepClone = function (list) {
        return JSON.parse(JSON.stringify(list));
    };

    var getIndexOfValue = function (object, key, value) {
        var indexFound = -1;
        object.some(function (currentValue, index) {
            if (currentValue[key] == value) {
                indexFound = index;
                return true;
            }
        });
        return indexFound;
    }

    return {
        init: init
    };
}();

var waitForLibraries = function () {
    setTimeout(function () {
        if (typeof _ == "undefined" || typeof Vue == "undefined") {
            waitForLibraries();
        } else {
            SIA.MpIceCheckInComplete.init();
        }
    }, 100);
};

$(function () {
    waitForLibraries();
});