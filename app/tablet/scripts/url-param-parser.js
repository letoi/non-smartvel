var SIA = SIA || {};

SIA.URLParamParser = function() {
    var html = document.getElementsByTagName('html');
    var className = 'ie8';
    var urlParams = new URLSearchParams(window.location.search);
    
    function getURLParams(name) {
        return urlParams.get(name);
    };
    
    var pub = {
        getURLParams: getURLParams
	};
    
	return pub;
}();