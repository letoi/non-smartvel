$(document).ready(function() {
	var hiddenClassName = 'hidden';
	var showAllClass = '.js-show-all';

	var $vouchers = $('.js-mb-voucher-active');

	function getVoucherActive(data) {
		var vouchers = [];
		for (var i = 0; i < data.length; i++) {
			var keys = Object.keys(data[i]);

			for (var j = 0; j < keys.length; j++) {
				var voucherWithKey = data[i][keys[j]];

				for (var k = 0; k < voucherWithKey.length; k++) {
					voucherWithKey[k].type = keys[j];
				}

				vouchers = vouchers.concat(voucherWithKey);
			}
		}

		var voucherActive = vouchers.filter(function(obj) {
			return obj.rewardStatus === 'Available';
		});

		return voucherActive;
	}

	function addVoucherToDOM(tpl, data) {
		var template = window._.template(tpl, {
			data: data,
		});

		$vouchers.html(template);
	}

	function initialize() {
		return SIA.AjaxCaller.template('vouchers-active.tpl').then(function(tplContent) {
			return 	SIA.AjaxCaller.json('voucher-active.json').then(function(data) {
				var vouchersActive = getVoucherActive(data.SAARewardInfo || []);
				addVoucherToDOM(tplContent, vouchersActive);
			});
		}).catch(function(err) {
			console.log(err);
		});
	}

	$(document).on('click', showAllClass, function() {
		$('.js-voucher-item').removeClass(hiddenClassName);
		$(this).addClass(hiddenClassName);
	});

	initialize();
});
