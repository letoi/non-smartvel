/**
 * @name SIA
 * @description Define global passengerDetail functions
 * @version 1.0
 */
SIA.passengerDetail = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var wrapPassenger = $('.wrap-passenger');
	var formPassenger = $('.form-passenger-detail');
	var contentPassengerDetails = $('.block-content-passenger-details');
	var initData = {};
	var prCard;
	var prCardInfant;
	var isIcePage = false;

	if (formPassenger.length || contentPassengerDetails.length) {
	} else {
		return;
	}

	if (!formPassenger.length) {
		return;
	}

	var convertMonth = function(n, isGetIndex) {
		var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return isGetIndex ? m.indexOf(n ? n.slice(0, 3) : 'Jan') : m[n];
	};

	$.validator.addMethod('required_issue_place', function(value, el, param) {
		if (!!$(param[0]).val()) {
			return !!this.elementValue(el);
		}
		return false;
	}, L10n.validator.requiredIssuePlace);

	// $.validator.addMethod('checkofadult', function(value, el, param) {
	// 	var day = $(param[1]).val();
	// 	var month = convertMonth($(param[2]).val(), true);
	// 	var year = $(param[3]).val();
	// 	var age = param[0];
	// 	var mydate = new Date();
	// 	mydate.setFullYear(year, month, day);

	// 	var setDate = new Date();
	// 	setDate.setFullYear(mydate.getFullYear() + age, month, day);
	// 	var currdate = new Date();

	// 	if ((currdate - setDate) >= 0) {
	// 		return true;
	// 	} else {
	// 		return false;
	// 	}
	// }, L10n.validator.checkofadult);

	// $.validator.addMethod('checkofchild', function(value, el, param) {
	// 	var day = $(param[1]).val();
	// 	var month = convertMonth($(param[2]).val(), true);
	// 	var year = $(param[3]).val();
	// 	var age = param[0];
	// 	var mydate = new Date();
	// 	mydate.setFullYear(year, month, day);

	// 	var currdate = new Date();
	// 	var setDate = new Date();
	// 	setDate.setFullYear(mydate.getFullYear() + age, month, day);

	// 	// if (currdate - mydate < (2 * 365 * 24 * 60 * 60 * 1000) || currdate - setDate > 0 || mydate > currdate) {
	// 	if (currdate > setDate || mydate > currdate) {
	// 		return false;
	// 	} else {
	// 		return true;
	// 	}
	// }, L10n.validator.checkofchild);

	// $.validator.addMethod('checkofinfant', function(value, el, param) {
	// 	var arr = param[2].split(',');
	// 	var day = $(arr[0]).val();
	// 	var month = convertMonth($(arr[1]).val(), true);
	// 	var year = $(arr[2]).val();
	// 	var age = param[0];
	// 	var date = param[1];
	// 	var mydate = new Date();
	// 	mydate.setFullYear(year, month, day);

	// 	var currdate = new Date();
	// 	var setDate = new Date();
	// 	setDate.setFullYear(mydate.getFullYear() + age, month, day);

	// 	if (currdate > setDate || mydate > currdate) {
	// 		return false;
	// 	} else {
	// 		if (currdate - mydate > setDate || ((currdate - mydate)/1000/60/60/24) < date) {
	// 			return false;
	// 		}
	// 		return true;
	// 	}
	// }, L10n.validator.checkofinfant);

	$.validator.addMethod('checkCurrentDate', function(value, el, param) {
		if ($(el).is('.hidden')) {
			return true;
		}
		var arr = param[0].split(',');
		var cd = new Date();
		var optDate = new Date($(arr[2]).val(), convertMonth($(arr[1]).val(), true), $(arr[0]).val());
		// ignore validate
		if ($(param[1]).is(':checked')) {
			return true;
		}
		// detect validate with condition
		if (optDate < cd) {
			return false;
		} else {
			return true;
		}
		return true;
	}, L10n.validator.checkCurrentDate);

	// $.validator.addMethod('checkpassport', function(value, el, param) {
	// 	if ($(el).is('.hidden')) {
	// 		return true;
	// 	}
	// 	var arr = param[1].split(',');
	// 	var cd = new Date();
	// 	var td = new Date(cd.getFullYear(), cd.getMonth() + param[0], cd.getDate());
	// 	var optDate = new Date($(arr[2]).val(), convertMonth($(arr[1]).val(), true), $(arr[0]).val());
	// 	// ignore validate
	// 	if ($(param[3]).is(':checked')) {
	// 		return true;
	// 	}
	// 	// detect validate with condition
	// 	if (param[2]) {
	// 		if ((optDate < td) && $(param[2]).is(':checked')) {
	// 			return false;
	// 		} else {
	// 			return true;
	// 		}
	// 	} else {
	// 		if (optDate < td) {
	// 			return false;
	// 		}
	// 	}
	// 	return true;
	// }, L10n.validator.checkpassport);

	$.validator.addMethod('check_alpha_numberic', function(value) {
		if (!value.length) {
			return true;
		}
		return /^[a-zA-Z0-9]+$/i.test(value);
	}, L10n.validator.checkAlphaNumberic);

	$.validator.addMethod('check_addr_usa', function(value) {
		if (!value.length) {
			return true;
		}
		return /^[a-zA-Z0-9\s\-\#\/\'\,\[\]]+$/i.test(value);
	}, L10n.validator.checkAddrUsa);

	$.validator.addMethod('check_kf_membership_number', function(value) {
		if (value.length === 10 && /^81/i.test(value)) {
			return true;
		}
		return false;
	}, L10n.validator.checkKfMembershipNumber);

	// var passengerCbk1 = $('#passerger-checkbox-1');
	// var passengerCbk2 = $('#passerger-checkbox-2');
	var passsengerName = $('.sidebar .booking-nav a.booking-nav__item');
	var selectTab = passsengerName.siblings('select');
	var popupShow = $();
	var timerTabAfterChange = null;
	var flyingFocus = $();

	var detectGender = function(resourceElement, referenceElement) {
		if (!resourceElement.length || !referenceElement.length) {
			return;
		}
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if ($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function() {
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	detectGender($('[data-gender-resource]'), $('[data-gender-reference]'));

	var detectSucess = function() {
		var dt = {
			index: 0,
			success: true
		};
		formPassenger.each(function(i) {
			if (!$(this).data('success')) {
				dt = {
					index: i,
					success: false
				};
				return false;
			}
		});
		return dt;
	};
	var afertUpdate = function(currentTab) {
		var checkBoxConfirm = currentTab.find('[class|="button-group"] input');
		var wpCf = currentTab.find('[data-confirm-tc]');
		if (formPassenger.filter(function() {
				return $(this).data('success');
			}).length === formPassenger.length - 1) {
			if (!currentTab.data('success')) {
				if (currentTab.closest('[data-lastpax]').length > 0 && true === currentTab.closest('[data-lastpax]').data('lastpax') && wpCf.length) {
					// checkBoxConfirm.val(L10n.passengerDetail.proceed);
					if (wpCf.find(':checkbox').is(':checked')) {
						checkBoxConfirm.removeClass('disabled').prop('disabled', false);
					} else {
						checkBoxConfirm.addClass('disabled').prop('disabled', true);
					}
					wpCf.show();
				}
			}
		} else {
			// checkBoxConfirm.removeClass('disabled').prop('disabled', false).val(L10n.passengerDetail.next);
			checkBoxConfirm.removeClass('disabled').prop('disabled', false);
			wpCf.hide();

		}
	};

	// var correctDate = function(){
	// 	var target = $('[data-rule-validatedate]');
	// 	var detectDate = function(d, m, y, el){
	// 		var nd = new Date();
	// 		var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
	// 		if(d > getLastDate.getDate()){
	// 			el.val(getLastDate.getDate());
	// 			el.defaultSelect('refresh');
	// 			setTimeout(function(){
	// 				el.children().eq(getLastDate.getDate()).prop('selected', true);
	// 				el.blur();
	// 			}, 400);
	// 		}
	// 	};
	// 	target.each(function(){
	// 		var self = $(this);
	// 		var data = self.data('rule-validatedate');
	// 		var date = $(data[0]);
	// 		var month = $(data[1]);
	// 		var year = $(data[2]);
	// 		// console.log(date, month, year);
	// 		/*date.off('change.correctDate').on('change.correctDate', function(){
	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 		});
	// 		month.off('change.correctDate').on('change.correctDate', function(){
	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 		});
	// 		year.off('change.correctDate').on('change.correctDate', function(){
	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
	// 		});*/

	// 		date.off('blur.correctDate').on('blur.correctDate', function() {
	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
	// 		});
	// 		month.off('blur.correctDate').on('blur.correctDate', function() {
	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
	// 		});
	// 		year.off('blur.correctDate').on('blur.correctDate', function() {
	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
	// 		});
	// 	});

	// };

	// correctDate();

	// var correctDMY = function(){
	// 	$('[data-month],[data-date],[data-year]').each(function(){
	// 		var self  = $(this);
	// 		var input = self.find('input');
	// 		var select = self.find('select');
	// 		var hasValue = function(val, el){
	// 			var valid = false;
	// 			el.each(function(){
	// 				if($(this).data('value') === val){
	// 					valid = true;
	// 				}
	// 			});
	// 			return valid;
	// 		};
	// 		input.off('blur.correctMonth').on('blur.correctMonth', function(){
	// 			var li = input.autocomplete( 'widget' ).find('li');
	// 			if(li.length === 1 && li.data('value') === L10n.globalSearch.noMatches){
	// 				input.val(select.find('option:first').text());
	// 				self.removeClass('default');
	// 			}
	// 			else{
	// 				if(li.length !== select.children().length){
	// 					if(!hasValue(input.val(), li)){
	// 						input.val(li.first().data('value'));
	// 						self.removeClass('default');
	// 					}
	// 				}
	// 			}
	// 		});
	// 	});
	// };

	// correctDMY();

	// Init Confirm pax pop up && Mismatch pop up
	var popupConfirmPassenger = $('.popout--confirm-passenger');
	var popupMismatch = $('.popout--mismatch');
	var popupAlertConfirm = $('.popup--passenger-condition');

	if (!popupConfirmPassenger.data('Popup')) {
		popupConfirmPassenger.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}
	if (!popupMismatch.data('Popup')) {
		popupMismatch.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}
	if (!popupAlertConfirm.data('Popup')) {
		popupAlertConfirm.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}

	if (formPassenger.length > 1) {
		$('[data-tab]').tabMenu({
			tab: 'aside.sidebar a.booking-nav__item',
			tabContent: 'form.form-passenger-detail',
			selectOnMobile: 'aside.sidebar .booking-nav > select',
			activeClass: 'active',
			afterChange: function(tabContent) {
				var currentTab = tabContent.filter('.active');
				if (!currentTab.data('initValidation')) {
					initValidation(currentTab, currentTab.index());
					/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
						// on Desktop
						$('[data-customselect]').not(':hidden').customSelect({
							itemsShow: 5,
							heightItem: 43,
							scrollWith: 2
						});
					}
					else{
						// on Tablet and Mobile
						$('select').not(':hidden').each(function(){
							if(!$(this).data('defaultSelect')){
								$(this).defaultSelect({
									wrapper: '.custom-select',
									textCustom: '.select__text',
									isInput: false
								});
							}
						});
					}*/

					// on Tablet and Mobile
					$('select').not(':hidden').each(function() {
						if (!$(this).closest('[data-autocomplete]').length) {
							if (!$(this).data('defaultSelect')) {
								$(this).defaultSelect({
									wrapper: '.custom-select',
									textCustom: '.select__text',
									isInput: false
								});
							}
						}
					});
				}
				clearTimeout(timerTabAfterChange);
				timerTabAfterChange = setTimeout(function() {
					$('html,body').stop().animate({
						'scrollTop': 0
					}, 1000, function() {
						flyingFocus = $('#flying-focus');
						if (flyingFocus.length) {
							flyingFocus.remove();
						}
					});
				}, 200);
				afertUpdate(currentTab);
			}
		});
	} else {
		// passsengerName.on('click.switch-passenger', function(e) {
		// 	e.preventDefault();
		// 	if(formPassenger.data('validator').checkForm()) {
		// 		// formPassenger.submit();
		// 		var confirmCheckbox = formPassenger.find('[data-confirm-tc] :checkbox');
		// 		if(confirmCheckbox.length) {
		// 			if(!confirmCheckbox.is(':checked')) {
		// 				popupAlertConfirm.Popup('show');
		// 				formPassenger.data('checkBoxConfirm', true);
		// 			} else {
		// 				formPassenger.data('checkBoxConfirm', false);
		// 			}
		// 		}
		// 		if(!formPassenger.data('checkBoxConfirm')) {
		// 			formPassenger.submit();
		// 		}
		// 	}
		// 	else {
		// 		window.setTimeout(function() {
		// 			if(!formPassenger.data('validatedOnce')) {
		// 				formPassenger.data('validatedOnce', true);
		// 			}
		// 			formPassenger.valid();
		// 		}, 1);
		// 		var firstInvalidInput = formPassenger.find('.error:input').eq(0);
		// 		// var formGroupInvalid = firstInvalidInput.closest('.form-group');
		// 		// firstInvalidInput.focus();
		// 		if(firstInvalidInput.is('select') && firstInvalidInput.parent().is('[data-customselect]')) {
		// 			firstInvalidInput.parent().addClass('focus');
		// 		}
		// 		// if(formGroupInvalid) {
		// 		// 	$('html, body').animate({
		// 		// 		scrollTop: formGroupInvalid.offset().top
		// 		// 	}, 400);
		// 		// }
		// 	}
		// });
		// var selectedIndex = selectTab.prop('selectedIndex');
		// selectTab.on('change.switch-passenger', function(e) {
		// 	e.preventDefault();
		// 	if(formPassenger.data('validator').checkForm()) {
		// 		// formPassenger.submit();
		// 		var confirmCheckbox = formPassenger.find('[data-confirm-tc] :checkbox');
		// 		if(confirmCheckbox.length) {
		// 			if(!confirmCheckbox.is(':checked')) {
		// 				popupAlertConfirm.Popup('show');
		// 				formPassenger.data('checkBoxConfirm', true);
		// 			} else {
		// 				formPassenger.data('checkBoxConfirm', false);
		// 			}
		// 		}
		// 		if(!formPassenger.data('checkBoxConfirm')) {
		// 			formPassenger.submit();
		// 		}
		// 	}
		// 	else {
		// 		window.setTimeout(function() {
		// 			if(!formPassenger.data('validatedOnce')) {
		// 				formPassenger.data('validatedOnce', true);
		// 			}
		// 			formPassenger.valid();
		// 		}, 1);
		// 		// $(this).prop('selectedIndex', selectedIndex);
		// 		// var firstInvalidInput = formPassenger.find('.error').eq(0);
		// 		// firstInvalidInput.focus();
		// 		// $('html, body').animate({
		// 		// 	scrollTop: firstInvalidInput.closest('.form-group').offset().top
		// 		// }, 400);
		// 	}
		// }).off('blur.resetValue').on('blur.resetValue', function(){
		// 	$(this).children().eq(selectedIndex).prop('selected', true);
		// });
	}
	// passengerCbk1.on('change.resetError', function(){
	// 	if($(this).is(':checked')){
	// 		var parent = $('#lim-2').closest('.form-group');
	// 		parent.find('.text-error').remove();
	// 		// parent.parent().find('.ico-error').remove();
	// 		parent.removeClass('error');
	// 	}
	// });
	// passengerCbk2.on('change.resetError', function(){
	// 	if($(this).is(':checked')){
	// 		var parent = $('#lim-19').closest('.form-group');
	// 		parent.find('.text-error').remove();
	// 		// parent.parent().find('.ico-error').remove();
	// 		parent.removeClass('error');
	// 	}
	// });

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		var doValidate = function(els){
	// 			var pass = true;
	// 			els.each(function(){
	// 				if(!pass){
	// 					return;
	// 				}
	// 				pass = $(this).valid();
	// 			});
	// 		};
	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 			formGroup.each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		});

	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 			self.data('change', true);
	// 		});
	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
	// 			self.data('change', true);
	// 			$(this).valid();
	// 		}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
	// 			if($(this).val()){
	// 				self.data('change', true);
	// 			}
	// 		});
	// 		if(global.vars.isSafari){
	// 			self.find(':radio').off('afterTicked.passengerDetail').on('afterTicked.passengerDetail', function(){
	// 				self.data('change', true);
	// 			});
	// 		}
	// 	});
	// };

	var initValidation = function(formPassengerDetail) {
		// var formGroup = formPassengerDetail.find('.form-group');
		var popupAccountUpdate = $(formPassengerDetail.data('popup'));
		var wrapCheckInput = $('[data-disable-value]', formPassengerDetail);

		wrapCheckInput.each(function() {
			var wrap = $(this);
			var checkBox = $('input:checkbox', wrap);
			var inputSibling = $('input:text', wrap);
			if (inputSibling.data('ruleRequired')) {
				checkBox.each(function(i) {
					var self = $(this);
					var sibl = inputSibling.eq(i);
					self.off('change.disabledInputSibling').on('change.disabledInputSibling', function() {
						if (self.is(':checked')) {
							sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
							wrap.find('.text-error').remove();
							wrap.find('.error').removeClass('error');
						} else {
							sibl.prop('disabled', false).closest('span').removeClass('disabled');
						}
					});
				});
			}
		});

		if (popupAccountUpdate.length) {
			popupShow = popupAccountUpdate;
			// update target field
			var passport = formPassengerDetail.find('[data-passport] input');
			var country = formPassengerDetail.find('[data-country] input');
			var email = formPassengerDetail.find('[data-email] input');
			var expireDay = formPassengerDetail.find('[data-expire] [data-day] select');
			var expireMonth = formPassengerDetail.find('[data-expire] [data-month] select');
			var expireYear = formPassengerDetail.find('[data-expire] [data-year] select');
			var mobileCode = formPassengerDetail.find('[data-mobile] [data-country-code] input');
			var mobileArea = formPassengerDetail.find('[data-mobile] [data-area] input');
			var mobilePhone = formPassengerDetail.find('[data-mobile] [data-phone] input');

			// compare field
			// var cpPassport = popupAccountUpdate.find('[data-compare-passport] input');
			// var cpCountry = popupAccountUpdate.find('[data-compare-country] input');
			// var cpExpire = popupAccountUpdate.find('[data.compare-expire] input');
			// var cpMobile = popupAccountUpdate.find('[data-compare-mobile] input');

			// update field
			var cpPassport = popupAccountUpdate.find('[data-update-passport] input');
			var cpCountry = popupAccountUpdate.find('[data-update-country] input');
			var cpEmail = popupAccountUpdate.find('[data-update-email] input');
			var cpExpire = popupAccountUpdate.find('[data-update-expire] input');
			var cpMobile = popupAccountUpdate.find('[data-update-mobile] input');

			// update value
			passport.off('change.passport').on('change.passport', function() {
				cpPassport.val($(this).val());
			});
			cpPassport.val(passport.val());
			email.off('change.passport').on('change.passport', function() {
				cpEmail.val($(this).val());
			});
			cpEmail.val(email.val());
			// country.closest('.custom-select').off('afterSelect.country').on('afterSelect.country', function(){
			// 	cpCountry.val(country.find('option:selected').text());
			// });
			country.off('blur.country').on('blur.country', function() {
				cpCountry.val($(this).val());
			});
			cpCountry.val(country.val());

			// expireDay.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			// });
			expireDay.off('change.expireDay').on('change.expireDay', function() {
				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			});
			// expireMonth.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			// });
			expireMonth.off('change.expireDay').on('change.expireDay', function() {
				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			});
			// expireYear.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			// });
			expireYear.off('change.expireDay').on('change.expireDay', function() {
				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
			});
			cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());

			// mobileCode.closest('.custom-select').off('afterSelect.mobileCode').on('afterSelect.mobileCode', function(){
			// 	cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			// });
			mobileCode.off('blur.mobileCode').on('blur.mobileCode', function() {
				if (mobileCode.val()) {
					cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length - 1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
				}
			});
			/*mobileArea.closest('.custom-select').off('afterSelect.mobileArea').on('afterSelect.mobileArea', function(){
				cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			});*/
			mobileArea.off('change.mobileArea').on('change.mobileArea', function() {
				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length - 1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			});
			/*mobilePhone.closest('.custom-select').off('afterSelect.mobilePhone').on('afterSelect.mobilePhone', function(){
				cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			});*/
			mobilePhone.off('change.mobilePhone').on('change.mobilePhone', function() {
				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length - 1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			});
			if (mobileCode.val()) {
				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length - 1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
			}

			// init popup
			var flyingFocus = $('#flying-focus');
			popupAccountUpdate.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function() {
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
					// if($(window).width() < 768){
					// 	container.css('height', $('.popup--login').find('.popup__content').outerHeight(true));
					// 	container.css('overflow', 'hidden');
					// }
				},
				afterHide: function() {
					// if($(window).width() < 768){
					// 	container.css('height', '');
					// 	container.css('overflow', '');
					// }
					// setTimeout(function(){
					// 	$('html,body').animate({
					// 		'scrollTop': 0
					// 	});
					// }, 200);
				}
			}).find('[class|="button-group"] .btn-1').off('click.checksubmit').on('click.checksubmit', function() {
				// update btn
				// if(!detectSucess().success){
				// 	e.preventDefault();
				// 	passsengerName.eq(detectSucess().index).trigger('click.show');
				// 	if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
				// 		selectTab.prop('selectedIndex', detectSucess().index);
				// 	}
				// 	popupShow.Popup('hide');
				// }
			}).end().find('[class|="button-group"] .btn-2').off('click.checksubmit').on('click.checksubmit', function() {
				// skip btn
				// if(!detectSucess().success){
				// 	e.preventDefault();
				// 	passsengerName.eq(detectSucess().index).trigger('click.show');
				// 	if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
				// 		selectTab.prop('selectedIndex', detectSucess().index);
				// 	}
				// 	popupShow.Popup('hide');
				// }
				// e.preventDefault();
				// popupShow.Popup('hide');
			});
			global.vars.checkAllList(popupAccountUpdate.find('input[data-checkbox-master]'), popupAccountUpdate.find('.table > div').not(':eq(0)'));

			var renderLoggedInUser = function() {
				popupAccountUpdate.find('input:text').each(function() {
					var that = this;
					var el = $(that);
					var parentElement = that.parentNode.parentNode;
					var dataParent = $(parentElement).data();
					if (!$.isEmptyObject(dataParent)) {
						for (var objData in dataParent) {
							if (globalJson.loggedInUser[objData]) {
								el.val(globalJson.loggedInUser[objData]);
							}
						}
					}
				});
			};
			if (globalJson.loggedInUser) {
				renderLoggedInUser();
			}
		}

		var checkUpdate = function(els, els1) {
			var change = false;
			els.each(function(i) {
				if ($(this).val() !== els1.eq(i).val()) {
					change = true;
					return;
				}
			});
			return change;
		};

		formPassengerDetail.data('initValidation', true);
		/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
					// on Desktop
			if(!$('[data-customselect]').data('customSelect')){
				$('[data-customselect]').not(':hidden').customSelect({
					itemsShow: 5,
					heightItem: 43,
					scrollWith: 2
				});
			}
		}
		else{
			// on Tablet and Mobile
			$('select').not(':hidden').each(function(){
				if(!$(this).data('defaultSelect')){
					$(this).defaultSelect({
						wrapper: '.custom-select',
						textCustom: '.select__text',
						isInput: false
					});
				}
			});
		}*/

		// on Tablet and Mobile
		$('select').not(':hidden').each(function() {
			if (!$(this).closest('[data-autocomplete]').length) {
				if (!$(this).data('defaultSelect')) {
					$(this).defaultSelect({
						wrapper: '.custom-select',
						textCustom: '.select__text',
						isInput: false
					});
				}
			}
		});

		var multipleSubmit = function() {
			var nextPassengerBtn = $('#adult-passenger-input-1');
			var passengerItems = $('.booking-nav__item');
			var passengerSelect = $('.tab-select');
			var mainForm = $('#form-passenger-1');
			var formHeading = mainForm.find('.sub-heading-1--blue');
			var multiValueField = $('#multiValue');
			var numberOfPassengers = passengerItems.length;
			var currentSelectedIdx = 0;
			if (!localStorage.groupData) {
				localStorage.setItem('groupData', '[]');
			}
			var groupData = JSON.parse(localStorage.groupData);

			var handleData = function(){
				var paxInfoJson = globalJson.paxInfoJson;
				if(paxInfoJson){
					// paxInfoJson = JSON.parse(paxInfoJson);
					for(var i = 0; i < paxInfoJson.length; i++){
						var idx = paxInfoJson[i].idx;
						if(groupData[idx]){
							continue;
						}
						groupData[idx] = paxInfoJson[i].paxInfo;
					}
				}
			};
			handleData();

			var addDisabled = function() {
				passengerItems.addClass('disabled');
				passengerSelect.find('option').attr('disabled', 'disabled');
				passengerItems.eq(0).removeClass('disabled');
				passengerSelect.find('option').eq(0).removeAttr('disabled');
			};
			addDisabled();

			var removeDisabled = function(item) {
				var idx = item.data('idx');
				if (item.is('option')) {
					item.removeAttr('disabled');
					passengerItems.eq(idx).removeClass('disabled');
				} else {
					item.removeClass('disabled');
					passengerSelect.find('option').eq(idx).removeAttr('disabled');
				}
			};

			var submitable = function() {
				var data = $.parseJSON(localStorage.groupData);

				if (!data.length) {
					return false;
				}

				for (var i = 0; i < numberOfPassengers; i++) {
					if (!data[i]) {
						return false;
					}
				}

				return !data.filter(function(dataItem) {
					return dataItem.isValid === false;
				}).length;
			};

			var triggerSubmit = function(form) {
				form.trigger('submit');
			};

			var isFormValid = function(form) {
				return form.valid();
			};

			var resetForm = function(form, isCbName) {
				// $('input[type="text"]').trigger('blur');
				var fisrtNameEl = $('#first-name-last');
				if(!isCbName){
					fisrtNameEl.removeAttr('disabled');
					fisrtNameEl.parent().removeClass('disabled');
				}
				form[0].reset();
				validator.resetForm();
				$('[data-citizenship-visa-status-relation]').css('display', 'none');

				var customSelects = mainForm.find('[data-customselect]').not(':hidden');
				$.each(customSelects, function() {
					var selectEl = $(this);
					var text = selectEl.find('option').filter(':selected').text();
					selectEl.find('.select__text').text(text);
				});
			};

			var renderForm = function(blocks, paxType) {
				if( typeof blocks !== 'undefined' ){
					var len = blocks.length;
					if (!len || !(blocks instanceof Array)) {
						return;
					}

					mainForm.find('[data-passenger-block]').addClass('hidden');
					for (var i = 0; i < len; i++) {
						if ($(blocks[i]).length) {
							$(blocks[i]).removeClass('hidden');
						}
					}
				}

				// $('[data-birth-info]').addClass('hidden');
				// $('[data-birth-'+paxType+']').removeClass('hidden');
				if(paxType !== 'adult'){
					setTimeout(function() {
						if( typeof($('#under-18-years-old')[0]) !== 'undefined' ){
							$('#under-18-years-old').prop('checked','true');
							$('#under-18-years-old')[0].checked = true;
						}
					}, 10);
				}
			};

			$('.overlay-loading').on('afterHide.ready', function(){
				renderForm(passengerItems.eq(0).data('blocks'), passengerItems.eq(0).data('pax-title'));
			});

			var scrollToElement = function(element) {
				$('html, body').animate({
					scrollTop: element.offset().top
				}, 400);
			};

			var showPassengerItems = function(currentPassenger, isFormValid) {
				// isFormValid : prev form was valid or not
				isFormValid = true;
				var formBlocks = currentPassenger.data('blocks');
				var paxType = currentPassenger.data('pax-title');
				renderForm(formBlocks, paxType);
				passengerItems.removeClass('active');
				if (passengerItems.is(':hidden')) {
					passengerItems.filter(function() {
						return $(this).data('passengerid') === currentPassenger.data('passengerid');
					}).addClass('active');
				} else {
					currentPassenger.addClass('active');
				}
				passengerSelect.find('option').eq(currentSelectedIdx).prop('selected', true);
				formHeading.text(currentPassenger.text());
			};

			var saveFormData = function(form, idx, isValid) {
				var data = {};
				var formData = form.serialize().split('&');
				var len = formData.length;

				data.isValid = isValid;

				for (var i = 0; i < len; i++) {
					data[formData[i].split('=')[0]] = formData[i].split('=')[1];
				}

				groupData[idx] = data;
				localStorage.groupData = JSON.stringify(groupData);
			};

			var loadDataBack = function(form, idx) {
				var data = groupData[idx];
				var value = '';
				if (data === void 0) {
					return;
				}

				var getData = function() {
					return $(this).attr('value') === value;
				};

				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						if (!data[key]) {
							continue;
						}
						var field = $('#' + key);
						data[key] = data[key].toString().replace(/\+/g, ' ').replace(/%2F/g, '/').replace(/%26/g, '&').replace(/%40/g, '@');
						value = data[key];

						if (key === 'traveller' || key === 'traveller-infant' || key === 'contact-rd') {
							var radioEl = $('[name="' + key + '"]').filter(getData);
							if(radioEl.length) {
								radioEl[0].checked = 'true';
								radioEl.trigger('change');

								if (!data['checbox-expiry-last']) {
									$('#checbox-expiry-last')[0].checked = false;
									$('#checbox-expiry-last').trigger('change');
								}
							}
						}

						if (!field.length) {
							continue;
						}
						if(field.is($('#checkbox-name-last'))){
							field[0].checked = true;
							field.trigger('change.disabledInputSibling');
							continue;
						}
						if (field.attr('type') === 'text' || field.attr('type') === 'tel' || field.is('textarea') || field.is('input[type="email"]')) {
							field.val(value);
						}
						if (field.parents('[data-customselect]').length) {
							var text = '';
							var selectedField = null;
							if (isNaN(value * 1)) {
								field.val(value);
								selectedField = field.find('option').filter(getData);
							} else {
								selectedField = field.find('option').eq(value * 1);
								selectedField[0].selected = 'true';
							}
							text = selectedField ? selectedField.text() : '';
							field.parents('[data-customselect]').find('.select__text').text(text);
						}
						if(field.parents('[data-autocomplete]').length){
							field.parents('[data-autocomplete]').removeClass('default');
						}
					}
				}
			};

			//load data back after force refresh page
			if (groupData.length) {
				loadDataBack(mainForm, 0);
				for (var i = 0; i < groupData.length; i++) {
					if(!groupData[i]){
						continue;
					}
					if (groupData[i].isValid) {
						passengerItems.eq(i).addClass('marked').removeClass('disabled');
						passengerSelect.find('option').eq(i).removeAttr('disabled');
					}
				}
			}

			/** Change button text based on the current active navigation. **/
			var changeTextOnLastPax = function() {
				/** Get the current active navigation **/
				var activeNavigation = passengerItems.is(':hidden')
					? passengerSelect.find('option').filter(':selected')
					: passengerItems.filter('.active');
				(activeNavigation.index() === 1)
					? nextPassengerBtn.val(L10n.passengerDetail.next)
					: nextPassengerBtn.val(L10n.passengerDetail.nextPassenger);
			};

			var finalSubmit = function(url) {
				$.ajax({
					url: url,
					// data: multiValueField,
					success: function(serverData){
						if(!serverData.errors || !serverData.errors.length){
							window.localStorage.removeItem('groupData');
							window.location = serverData.url;
						}

						passengerItems.addClass('disabled');
						passengerSelect.find('option').attr('disabled', 'disabled');
						passengerItems.removeClass('active');
						serverData.errors
							.sort(function(a,b){
								return a.idx - b.idx;
							})
							.forEach(function(item, index) {
								var dataIdx = item.idx;
								if (index === 0) {
									passengerItems.eq(dataIdx).addClass('active');
									currentSelectedIdx = dataIdx;
									passengerSelect.trigger('click.groupValidate');
									var formBlocks = passengerItems.eq(dataIdx).data('blocks');
									var paxType = passengerItems.eq(dataIdx).data('pax-title');
									renderForm(formBlocks, paxType);
									formHeading.text(passengerItems.eq(dataIdx).text());
									resetForm(mainForm);
									loadDataBack(mainForm, dataIdx);
								}
								passengerSelect.find('option').eq(dataIdx).removeAttr('disabled');
								passengerItems.eq(dataIdx)
									.removeClass('disabled')
									.removeClass('marked')
									.addClass('unmarked');
							});
					}
				});
			};

			var selectNextPassenger = function() {
				// check if there is an error
				var inputErrors = mainForm.find('.has-error');
				if (inputErrors.length){
					$(inputErrors[0]).focus();
					return;
				}

				triggerSubmit(mainForm);
				if (!isFormValid(mainForm)) {
					return;
				}

				$('[data-infant-block]').addClass('hidden');

				/* Get the current active passenger navigation <a> element */
				var currentPassenger = passengerItems.is(':hidden') ?
					passengerSelect.find('option').filter(':selected') :
					passengerItems.filter('.active');

				if (currentPassenger.data('idx') === 1) {
					$('#btn-add-on').removeClass('hidden');
					$('#btn-next').addClass('hidden');
				} else {
					$('#btn-next').removeClass('hidden');
					$('#btn-add-on').addClass('hidden');
				}

				/* Get the next passenger navigation */
				var nextPassenger = currentPassenger.next();
				/* Get both current and next idx */
				var idx = currentPassenger.data('idx');
				var nextIdx = nextPassenger.data('idx');
				currentSelectedIdx = nextIdx ? nextIdx : currentSelectedIdx;
				removeDisabled(nextPassenger);
				passengerItems.filter('.active').removeClass('unmarked').addClass('marked');
				saveFormData(mainForm, idx, true);
				if (nextIdx !== void 0) {
					showPassengerItems(nextPassenger);
					var fn = (groupData[idx]['first-name-last'] === null || typeof groupData[idx]['first-name-last'] === 'undefined') ? '' : groupData[idx]['first-name-last'];
					$('#passenger-'+idx+'-pop-up').text(fn + ' ' + groupData[idx]['last-name-second']);
					// $('#passenger-'+idx+'-pop-up').text(groupData[idx]['first-name-last']+' '+groupData[idx]['last-name-second']);
					if(groupData[nextIdx]){
						resetForm(mainForm, groupData[nextIdx]['checkbox-name-last']);
					}else{
						resetForm(mainForm);
					}
					prCard = null;
					prCardInfant = null;
				}
				if (groupData[nextIdx]) {
					loadDataBack(mainForm, nextIdx);
				}

				if (currentPassenger.data('idx') === numberOfPassengers - 1) {
					if(submitable()){
						finalSubmit(config.url.paxFakeData);
					}else{
						currentPassenger.removeClass('active');
						passengerItems.filter('.active').removeClass('active');
						var currentPassenger = passengerItems.filter('.disabled').eq(0).length ? passengerItems.filter('.disabled').eq(0) : passengerItems.filter('.unmarked').eq(0);
						currentPassenger.removeClass('disabled').addClass('active');
						var notCompleteItem = passengerSelect.find('option').eq(currentPassenger.data('idx'));
						notCompleteItem[0].selected = 'true';
						notCompleteItem.prop('disabled',false);
						resetForm(mainForm);
						var formBlocks = currentPassenger.data('blocks');
						renderForm(formBlocks);
						formHeading.text(currentPassenger.text());
						currentSelectedIdx = currentPassenger.data('idx');
						loadDataBack(mainForm, currentPassenger.data('idx'));
					}
				}
				changeTextOnLastPax();
				scrollToElement(formHeading);

				if(isIcePage) {
					var passengerSelected = {},
						infantData = {},
						passengerID = nextPassenger.data('passengerid');

					passengerSelected = initData.passengers.filter(function(passenger){
			      return passenger.passengerID === passengerID;
			    })[0];

	        infantData = initData.passengers.filter(function(passenger){
	        	return (passenger.passengerID === passengerSelected.passengerID) && (passenger.passengerType === 'I');
	        })[0];

	        changeFormData(passengerSelected);

	        !_.isEmpty(infantData) && activeInfantBlock(infantData);
					showGroupConfirm();
				}
			};

			var updatePassengerSelected = function() {
				passengerSelect.find('option')[currentSelectedIdx].selected = 'true';
			};

			var selectPassenger = function(e) {
				e.preventDefault();
				var isTablet = passengerItems.is(':hidden');
				var passengerItem = isTablet ? passengerSelect.find('option').filter(':selected') : $(this);

				if (passengerItem.hasClass('disabled') || passengerItem.attr('disabled') === 'disabled') {
					return;
				}

				$('[data-infant-block]').addClass('hidden');

				// var currentPassenger = isTablet ? passengerSelect.find('option').filter(':selected') : passengerItems.filter('.active');
				// var currentIdx = currentPassenger.data('idx');
				var idx = passengerItem.data('idx');

				/* Add remove button btn-add-on and btn-next
				 based on the current idx of passengerItem */
				if (idx === 2) {
					$('#btn-add-on').removeClass('hidden');
					$('#btn-next').addClass('hidden');
				} else {
					$('#btn-next').removeClass('hidden');
					$('#btn-add-on').addClass('hidden');
				}

				if (isFormValid(mainForm)) {
					saveFormData(mainForm, currentSelectedIdx, true);
					passengerItems.filter('.active').removeClass('unmarked').addClass('marked');
				} else {
					saveFormData(mainForm, currentSelectedIdx, false);
					passengerItems.filter('.active').removeClass('marked').addClass('unmarked');
				}

				showPassengerItems(passengerItem, mainForm.valid());
				if(groupData[idx]){
					resetForm(mainForm, groupData[idx]['checkbox-name-last']);
				}else{
					resetForm(mainForm);
				}

				prCard = null;
				prCardInfant = null;

				loadDataBack(mainForm, idx);
				// passengerSelect.find('option').eq(currentSelectedIdx).prop('selected', false);
				currentSelectedIdx = idx;
				changeTextOnLastPax();
				passengerSelect.find('option')[currentSelectedIdx].selected = true;
				scrollToElement(formHeading);

				$('#form-passenger-1').focus();

				if(isIcePage) {
					var passengerSelected = {},
						infantData = {},
						passengerID = passengerItem.data('passengerid');

					passengerSelected = initData.passengers.filter(function(passenger){
			      return passenger.passengerID === passengerID;
			    })[0];

	        infantData = initData.passengers.filter(function(passenger){
	        	return (passenger.passengerID === passengerSelected.passengerID) && (passenger.passengerType === 'I');
	        })[0];

	        changeFormData(passengerSelected);

	        !_.isEmpty(infantData) && activeInfantBlock(infantData);

					showGroupConfirm();
				}
			};

			nextPassengerBtn.off('click.groupValidate').on('click.groupValidate', selectNextPassenger);
			passengerItems.off('click.groupValidate').on('click.groupValidate', selectPassenger);
			passengerSelect.off('change.groupValidate').on('change.groupValidate', selectPassenger);
			passengerSelect.off('click.groupValidate').on('click.groupValidate', updatePassengerSelected);
			$('#btn-add-on').off('click.validateForm').on('click.validateForm', function () {
				triggerSubmit(mainForm);
				if (!isFormValid(mainForm)) {
					return;
				}
				$('#passenger-email-pop-up').text($('#email-address').val());
				$( '#btn-add-on-pop-up' ).trigger( 'click' );
			});

			$('#first-name-last').off('blur.updateNavFirstname').on('blur.updateNavFirstname', function() {
				var currentPassenger = passengerItems.is(':hidden') ?
					passengerSelect.find('option').filter(':selected') :
					passengerItems.filter('.active');
				currentPassenger.find('span').filter('.passenger-firstname').text($(this).val());
			});

			$('#last-name-second').off('blur.updateNavLastname').on('blur.updateNavLastname', function() {
				var currentPassenger = passengerItems.is(':hidden') ?
					passengerSelect.find('option').filter(':selected') :
					passengerItems.filter('.active');
				currentPassenger.find('span').filter('.passenger-lastname').text($(this).val());
			});

			var validator = formPassengerDetail.validate({
				ignore: ':hidden',
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess,
				onfocusout: global.vars.validateOnfocusout,
				onkeyup: function(el) {
					validator.element(el);
				},
				submitHandler: function() {
					if (!submitable()) {
						return false;
					}
					// localStorage.groupData = JSON.stringify(groupData);
					// console.log(groupData);
					multiValueField.val(JSON.stringify(groupData));
					multiValueField.val(localStorage.groupData);
					if(true){
						return false;
					}

					passsengerName.closest('.active').addClass('passed').find('.ico-check-thick').show();
					formPassengerDetail.data('success', true);
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
					if (detectSucess().success) {
						if (wrapPassenger.data('li')) {
							if (checkUpdate(popupShow.find('[data-compare-passport] input,[data-compare-country] input,[data-compare-expire] input,[data-compare-mobile] input, [data-compare-email] input'), popupShow.find('[data-update-passport] input,[data-update-country] input,[data-update-expire] input,[data-update-mobile] input, [data-update-email] input'))) {
								popupShow.Popup('show');
							}
						}

						// process for Confirm pax pop up && Mismatch pop up
						/*if((passsengerName.length - 1) === selectTab.prop('selectedIndex')) {*/
						if (wrapPassenger.data('lastStep')) {
							if (globalJson.cibPassengerInfoCompare) {
								var isCompare = true;
								var infoCompares = globalJson.cibPassengerInfoCompare;

								for (var nameInfo in infoCompares) {
									var elCompare = formPassengerDetail.find('[name="' + nameInfo + '"]').val().toString();
									if (elCompare) {
										if (elCompare !== infoCompares[nameInfo]) {
											isCompare = false;
											break;
										}
									}
								}

								if (!isCompare) {
									popupMismatch.Popup('show');
									popupMismatch.find('.btn-2').off('click.closeMismatch').on('click.closeMismatch',
										function(e) {
											e.preventDefault();
											popupMismatch.Popup('hide');
										});
									return false;
								}
							}

							popupConfirmPassenger.Popup('show');
							popupConfirmPassenger.find('.btn-2').off('click.closeConfirm').on('click.closeConfirm',
								function(e) {
									e.preventDefault();
									popupConfirmPassenger.Popup('hide');
								});

							return false;
						}
					} else {
						if (!detectSucess().success) {
							passsengerName.eq(detectSucess().index).trigger('click.show');
							if (win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)) { // mobile
								selectTab.prop('selectedIndex', detectSucess().index);
							}
						}
					}
					if (!wrapPassenger.data('li') && detectSucess().success) {
						return true;
					} else if (wrapPassenger.data('li') && detectSucess().success) {
						return false;
					} else if (!detectSucess().success) {
						return false;
					}
				},
				invalidHandler: function(form, validator) {
					var errors = validator.numberOfInvalids();
					if (errors) {
						var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
						if (errorColEl.length) {
							win.scrollTop(errorColEl.offset().top - 40);
						}
					}
				}
			});
		};
		multipleSubmit();


		// validateFormGroup(formGroup);
		afertUpdate(formPassengerDetail);

		// confirm checkbox
		// var wrapperFieldConfirm = formPassengerDetail.find('[data-confirm-tc]');
		// var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

		// if(confirmCheckbox.length){
		// 	confirmCheckbox.off('change.confirm').on('change.confirm', function(e){
		// 		e.preventDefault();
		// 		var btn = confirmCheckbox.closest('.form-group-full').next().find('input');
		// 		if(confirmCheckbox.is(':checked')){
		// 			btn.removeClass('disabled').prop('disabled', false);
		// 		}
		// 		else{
		// 			btn.addClass('disabled').prop('disabled', true);
		// 		}
		// 	}).trigger('change.confirm');
		// 	if(global.vars.isSafari){
		// 		confirmCheckbox.off('afterTicked.confirm').on('afterTicked.confirm', function(e){
		// 			e.preventDefault();
		// 			confirmCheckbox.trigger('change.confirm');
		// 		});
		// 	}
		// }

		var formFields = formPassengerDetail.find('input, textarea, select');
		var displayCheckTab = function(element) {
			if (formPassengerDetail.validate('validator').checkForm()) {
				element.addClass('passed');
				// element.find('.ico-check-thick').show();
			} else {
				element.removeClass('passed');
				// element.find('.ico-check-thick').hide();
			}
		};

		// passsengerName.removeClass('passed').find('.ico-check-thick').hide();

		// Change label button
		if (!wrapPassenger.data('lastStep')) {
			var btnSubmits = formPassengerDetail.find('[type="submit"]');
			// var btnSubmit = btnSubmits.not('[data-url]');
			var btnUrl = btnSubmits.filter('[data-url]');
			// formFields.each(function(){
			// 	// $(this).change(function(){
			// 	$(this).off('change.txtNextButton').on('change.txtNextButton', function() {
			// 		if(formPassengerDetail.validate('validator').checkForm()) {
			// 			if(btnSubmit.val().toLowerCase() === L10n.passengerDetail.next.toLowerCase()) {
			// 				btnSubmit.val(L10n.passengerDetail.nextPassenger);
			// 			}
			// 		} else {
			// 			if(btnSubmit.val().toLowerCase() === L10n.passengerDetail.nextPassenger.toLowerCase()) {
			// 				btnSubmit.val(L10n.passengerDetail.next);
			// 			}
			// 		}
			// 	});
			// });
			btnUrl.off('click.changeURLSubmit').on('click.changeURLSubmit', function(e) {
				e.preventDefault();
				var self = $(this);
				var form = self.closest('form');
				form.attr('action', self.data('url'));
				form[0].submit();
			});
		} else {
			passsengerName.closest('.active').addClass('passed') /*.prevAll().find('.ico-check-thick').show()*/ ;
			var compareDateWithNow = function(year, month, day, islessthan) {
				var now = new Date();
				var birthday = new Date(year + ' ' + month + ' ' + day);
				return islessthan ? (now > birthday) : (now < birthday);
			};
			var birthInfo = $('[data-birth-info]');
			birthInfo.off('change.birthSelects', 'select').on('change.birthSelects', 'select', function() {
				var dayOfBirth = birthInfo.find('[data-day] option:selected').val();
				var monthOfBirth = birthInfo.find('[data-month] option:selected').val();
				var yearOfBirth = birthInfo.find('[data-year] option:selected').val();
				if (dayOfBirth !== '' && monthOfBirth !== '' && yearOfBirth !== '') {
					var under12ConfirmMess = $('[data-under-12]');
					var over12ConfirmMess = $('[data-over-12]');
					if (compareDateWithNow(parseInt(yearOfBirth) + 5, monthOfBirth, dayOfBirth, true)) {
						if (compareDateWithNow(parseInt(yearOfBirth) + 12, monthOfBirth, dayOfBirth, false)) {
							under12ConfirmMess.removeClass('hidden');
							over12ConfirmMess.addClass('hidden');
						} else if (compareDateWithNow(parseInt(yearOfBirth) + 18, monthOfBirth, dayOfBirth, false)) {
							over12ConfirmMess.removeClass('hidden');
							under12ConfirmMess.addClass('hidden');
						} else {
							over12ConfirmMess.addClass('hidden');
							under12ConfirmMess.addClass('hidden');
						}
					} else {
						over12ConfirmMess.addClass('hidden');
						under12ConfirmMess.addClass('hidden');
					}
					showPopupConfirm();
				}
			});
		}

		displayCheckTab(passsengerName.closest('.active'));

		// Control hide show for icon the check
		formFields.each(function() {
			$(this).off('change.formField').on('change.formField', function() {
				// displayCheckTab(passsengerName.closest('.active'));
				var iconCheckForm = passsengerName.closest('.active').find('.ico-check-thick');
				if (!iconCheckForm.is(':hidden')) {
					iconCheckForm.hide();
				}
			});
		});

	};

	// initValidation(formPassenger.filter('.active'), formPassenger.filter('.active').index());
	!isIcePage && initValidation(formPassenger);

	/* Select Citizenship/Visa Status */
	formPassenger.each(function() {
		var citizenshipStatus = $(this).find('[data-citizenship-visa-status]');

		citizenshipStatus.off('change.change-citizenship').on('change.change-citizenship', function() {
			// var citizenshipFields = $(this).closest('form').find('[data-citizenship-visa-status-relation]');
			var citizenshipFields = $(this).closest('[data-passenger-block]').find('[data-citizenship-visa-status-relation]');
			var status = $(this).data('citizenship-visa-status');
			var shownFields = citizenshipFields.hide().filter('[data-citizenship-visa-status-relation="' + status + '"]').show();
			shownFields.find('select').each(function() {
				if (!$(this).closest('[data-autocomplete]').length) {
					if (!$(this).data('defaultSelect')) {
						$(this).defaultSelect({
							wrapper: '.custom-select',
							textCustom: '.select__text',
							isInput: false
						});
					}
				}
			});

			// if($(this).closest('form').data('validatedOnce')) {
			// 	citizenshipFields.find('input, select, textarea').not(':hidden').each(function() {
			// 		$(this).closest('form').validate().element($(this));
			// 	});
			// }
			$(this).trigger('change.txtNextButton');
			citizenshipFields.find('[data-use-passport-expiry-date]').trigger('change.use-expiry-date');
		}).filter(':checked').trigger('change.change-citizenship');

		var chbUsePassportExpiryDate = $(this).find('[data-use-passport-expiry-date]');

		chbUsePassportExpiryDate
			.off('change.use-expiry-date')
			.on('change.use-expiry-date', function() {
				var passportExpiryDate = $($(this).data('use-passport-expiry-date')).closest('[data-autocomplete]');
				var passengerForm = $(this).closest('[data-passenger-block]');
				var checked = $(this).is(':checked');

				if (passportExpiryDate.length) {
					passportExpiryDate.find('input').prop('disabled', checked);
					passportExpiryDate.toggleClass('disabled', checked);
				} else {
					passportExpiryDate.find('select').prop('selectedIndex', 0).prop('disabled', checked).toggleClass('hidden', checked);
				}

				var validator = $(this).closest('form').data('validator');
				var validatedOnce = $(this).closest('form').data('validatedOnce');
				if ((validator && validatedOnce) || checked) {
					passportExpiryDate.find('select').each(function() {
						validator.element(this);
					});
				}
				$(this).trigger('change.txtNextButton');

				setTimeout(function(){
					passengerForm.closest('[data-infant-block]').length ? updateCardExpiry(passengerForm, prCardInfant) : updateCardExpiry(passengerForm, prCard) ;
				}, 0);

				/*passportExpiryDate
					.toggleClass('disabled', checked)
					.find('select')
					.prop('selectedIndex', 0)
					.prop('disabled', checked)
					.toggleClass('hidden', checked);
				var validator = $(this).closest('form').data('validator');
				var validatedOnce = $(this).closest('form').data('validatedOnce');
				if((validator && validatedOnce) || checked) {
					passportExpiryDate.find('select').each(function(){validator.element(this);});
				}
				$(this).trigger('change.txtNextButton');*/
			}).trigger('change.use-expiry-date');
	});

	$('.passenger-logged .btn-1').off('click.loginPassengerDetail').on('click.loginPassengerDetail', function() {
		var triggerLoginPoup = $('.menu-bar a.login');
		triggerLoginPoup.trigger('click.showLoginPopup');
	});

	var showPopupConfirm = function() {
		var regionPopupConfirm = $('[data-region-popup-confirm]'),
			popupConfirm = regionPopupConfirm.children();

		if (popupConfirm.filter('.hidden').length === popupConfirm.length) {
			regionPopupConfirm.addClass('hidden');
		} else {
			regionPopupConfirm.removeClass('hidden');
		}
	};

	showPopupConfirm();

	var updateCardExpiry = function(passengerForm, prCard){
		if(prCard) {
			var cardType = passengerForm.find('[data-cardtype] select'),
					cardNum = passengerForm.find('[data-cardnumber] input'),
					cardCountry = passengerForm.find('[data-cardcountry] input'),
					cardExpiryDay = passengerForm.find('[data-day] input'),
					cardExpiryMonth = passengerForm.find('[data-month] input'),
					cardExpiryYear = passengerForm.find('[data-year] input'),
					cardObj = new Date(prCard.expiryDate);

			cardType.find('option').removeAttr('selected');
			cardType.find('option').not(':first').each(function(){
				$(this).text() === prCard.type && $(this).prop('selected', true);
			})
			cardType.closest('[data-customselect]').defaultSelect('refresh');

			cardNum.val(prCard.number);

			countryName = globalJson.countryCodes.find(function(country){
				return country.key === prCard.issuingCountryCode;
			}).value;

			cardCountry.val(countryName);

			cardExpiryDay.val(cardObj.getDate());
			cardExpiryMonth.val($.datepicker.formatDate('MM', cardObj));
			cardExpiryYear.val(cardObj.getFullYear());
		}
	};

	var populatePassportExpiry = function() {
		var cbxPassportExpiryDate = $('[data-use-passport-expiry-date]'),
			wrapPassportReference = $('[data-passport-reference]'),
			selectsPassportReference = wrapPassportReference.find('input[type="text"]');

		selectsPassportReference.off('blur.passportExpiry').on('blur.passportExpiry', function() {
			if (cbxPassportExpiryDate.is(':checked')) {
				var el = $(this),
					datePassportExpiry = cbxPassportExpiryDate.data('usePassportExpiryDate'),
					arrDateId;

				if (datePassportExpiry) {
					var dayEl, monthEl, yearEl;
					arrDateId = datePassportExpiry.split(',');

					if (el.closest('[data-day]').length) {
						dayEl = $(arrDateId[0]);
						dayEl.val(el.val());
						/*if(dayEl.data('defaultSelect')) {
							dayEl.data('defaultSelect').refresh();
						}*/
					} else if (el.closest('[data-month]').length) {
						monthEl = $(arrDateId[1]);
						monthEl.val(el.val());
						/*if(monthEl.data('defaultSelect')) {
							monthEl.data('defaultSelect').refresh();
						}*/
					} else if (el.closest('[data-year]').length) {
						yearEl = $(arrDateId[2]);
						yearEl.val(el.val());
						/*if(yearEl.data('defaultSelect')) {
							yearEl.data('defaultSelect').refresh();
						}*/
					}
				}
			}
		});

		cbxPassportExpiryDate.on('change.use-expiry-date', function() {
			selectsPassportReference.trigger('blur.passportExpiry');
		});
	};

	populatePassportExpiry();

	var frequentFlyerProgrammeEl = $('[data-check-kf-value]'),
		membershipNumberEl = $('[data-check-kf-field="false"]'),
		isKf = 'false',
		assignAccess = $('#assign-access');

	//code by squall start
	if (frequentFlyerProgrammeEl.length && membershipNumberEl.length) {

		membershipNumberEl.data('backupMsgRequired', membershipNumberEl.data('msgRequired'));

		frequentFlyerProgrammeEl.each(function(){
			var ffpSelf = $(this);
			var memNum = ffpSelf.parents('.form-group').next().find('[data-check-kf-field="false"]');

			ffpSelf.off('blur.checkKrisFlyer').on('blur.checkKrisFlyer', function() {
				var self = $(this);
				var memNum = self.parents('.form-group').next().find('[data-check-kf-field="false"]');

				if (this.value && this.value === $(this).data('checkKfValue')) {
					isKf = 'true';

					//membershipNumberEl.data('ruleCheck_kf_membership_number', true);
					memNum.data('msgRequired', L10n.validator.checkKfMembershipNumber);
					memNum.attr('data-rule-rangelength', '[10,10]');
					memNum.attr('maxlength', 10);
					memNum.data('rule-alphanumeric', false);
					memNum.data('rule-digits', true);
					memNum.trigger('blur');

					frequentFlyerDetails(memNum);

				}
				else{
					isKf = 'false';

					//membershipNumberEl.data('ruleCheck_kf_membership_number', false);
					memNum.removeAttr('data-ruleCheck_kf_membership_number');
					memNum.data('msgRequired', membershipNumberEl.data('backupMsgRequired'));
					memNum.removeAttr('rule-rangelength');
					memNum.removeAttr('maxlength');
					memNum.data('rule-alphanumeric', true);
					memNum.data('rule-digits', false);
					memNum.trigger('blur');

					memNum.removeClass('valid');
					assignAccess.attr('disabled', 'disabled');
					assignAccess.removeAttr('checked');
					assignAccess.next().removeClass('assign-access-checked');
					assignAccess.next().off('click');
				}

				if( this.value !== ''){
					membershipNumberEl.data('rule-required', true);
				}else{
					membershipNumberEl.data('rule-required', false);
				}

			});

			memNum.off('keyup').on('keyup', function(){
				if( this.value.length < 1){
					ffpSelf.data('rule-required', false);
				}else{
					ffpSelf.data('rule-required', true);
				}
			});

		});


	}

	var frequentFlyerDetails = function(member){
		member.each(function(){
			var self = $(this);
			console.log(self.parents('fieldset').data('new-passenger'));

			if( self.parents('fieldset').data('new-passenger') === false){
				console.log(self.parents('fieldset').data('new-passenger'));
				return;
			}

			self.off('keyup').on('keyup', function(){
				function checkKeyEvent(){
					if(self.hasClass('valid') && isKf === 'true' && self.val() !== '' && self.val().length === 10){
						assignAccess.removeAttr('disabled');
						assignAccess.next().addClass('assign-access-checked');
					}else{
						assignAccess.attr('disabled', 'disabled');
						assignAccess.next().removeClass('assign-access-checked');
					}
				}
				setTimeout(checkKeyEvent, 600);
			});
			assignAccess.next().off('click').on('click', function(){
				if(membershipNumberEl.hasClass('valid') && isKf === 'true'){
					$(this).toggleClass('assign-access-checked');
				}
			});
		});
	};

	var newTravellingPassenger = function(){
		var travellingPassenger = $('#travelling-passenger');
		var dataNewPassenger = $('[data-new-passenger="true"]');
		var birthDate = $('[data-verify-age]');
		var verifyAge = $('[name^="under-18-years-old"]');
		dataNewPassenger.hide();
		birthDate.hide();
		if($('.custom-scroll').length){
			$('.custom-scroll li').off('click').on('click', function(){
				var self = $(this);
				if(self.data('value') === 3){
					$('[data-new-passenger="true"]').show();
					$('[data-new-passenger="false"]').hide();
				}else{
					$('[data-new-passenger="true"]').hide();
					$('[data-new-passenger="false"]').show();
				}
			});
		}else{

			travellingPassenger.off('change').on('change', function(){
				var self = $(this);

				if( this.value === '' ){
					self.parent().find('.select__text').text('Select');
				}else{
					self.parent().find('.select__text').text($('option[value='+this.value+']', self).text());
				}

				if(this.value === 'anct'){
					$('[data-new-passenger="true"]').show();
					$('[data-new-passenger="false"]').hide();
				}else{
					$('[data-new-passenger="true"]').hide();
					$('[data-new-passenger="false"]').show();
				}
			});
		}
		verifyAge.off('change').on('change', function(){
			var self = $(this);
			if(self.is(':checked')){
				$('[data-verify-age="'+self.attr('name')+'"]').show();
			}else{
				$('[data-verify-age="'+self.attr('name')+'"]').hide();
			}
		});
	};

	newTravellingPassenger();

  var changeFormData = function(data) {
  	var passengerForm = $('#form-passenger-1'),
		dropdownGender = passengerForm.find('select[name="title-name-last"]'),
		lastName = passengerForm.find('input[name="last-name-second"]'),
		firstName = passengerForm.find('input[name="first-name-last"]'),
		genderCheckbox = passengerForm.find('input[name="gender-sex-last"]'),
		passportNum = passengerForm.find('input[name="passport-number-last"]'),
		passportCountry = passengerForm.find('input[name="passport-issue-last"]'),
		national = passengerForm.find('input[name="nationality-last"]'),
		mobileInput = passengerForm.find('input[name="pre-country-code"]'),
		expiryDay = passengerForm.find('input[name="passport-day-last"]'),
		expiryMonth = passengerForm.find('input[name="passport-month-last"]'),
		expiryYear = passengerForm.find('input[name="passport-year-last"]'),
		birthDay = passengerForm.find('input[name="birth-day-last"]'),
		birthMonth = passengerForm.find('input[name="birth-month-last"]'),
		birthYear = passengerForm.find('input[name="birth-year-last"]'),
		title = passengerForm.find('.sub-heading-1--blue'),
		countryResidenceInput = passengerForm.find('input[name="country-residence"]'),
		address = passengerForm.find('textarea[name="address-usa-last"]'),
		city = passengerForm.find('input[name="city-town-last"]'),
		state = passengerForm.find('select[name="state-province-last"]'),
		zip = passengerForm.find('input[name="26-z"]'),
		countryName,
		mobileName,
		fullName;

  	lastName.val(data.lastName);
  	firstName.val(data.firstName);

  	fullName = data.firstName + ' ' + data.lastName;

  	fullName += data.passengerType === 'A' ? ' - adult' : ' - child';

  	title.text(fullName);
  	// init dropdown gender
  	dropdownGender.find('option').removeAttr('selected');
  	dropdownGender.find('option').not(':first').each(function(){
  		$(this).val().toLowerCase() === data.title.toLowerCase() && $(this).prop('selected', true);
  	})
  	dropdownGender.closest('[data-customselect]').defaultSelect('refresh');

  	// init DOB
  	var dob = new Date(data.passportExpDate);

  	birthDay.val(dob.getDate());
  	birthMonth.val($.datepicker.formatDate('MM', dob));
  	birthYear.val(dob.getFullYear());

  	// init passport value
  	if(data.passport !== null) {
  		var passportObj = data.passport,
  				passportNumber = passportObj.passportNumber,
  				passportExpiry = new Date(passportObj.expiryDate),
  				mobileDropdown = $('#pre-country-code-label');

  		countryName = globalJson.countryCodes.find(function(country){
  			return country.key === passportObj.nationalityCode;
  		}).value;

  		mobileDropdown.find('option').each(function(){
  			var str = $(this).text();

  			if(str.indexOf(countryName) !== -1) {
  				mobileName = str;
  			}
  		})

  		passportNum.val(passportObj.passportNumber);
  		passportCountry.val(countryName);
  		mobileInput.val(mobileName);
  		national.val(countryName);
  		expiryDay.val(passportExpiry.getDate());
  		expiryMonth.val($.datepicker.formatDate('MM', passportExpiry));
  		expiryYear.val(passportExpiry.getFullYear());
  	}

  	// init gender checkbox

  	genderCheckbox.each(function(){
  		if(data.gender === 'M' && $(this).val() === 'male') {
  			$(this).prop('checked', true)
  		}
  		if(data.gender === 'F' && $(this).val() === 'female') {
  			$(this).prop('checked', true)
  		}
  	})

  	if(data.countryOfResidence) {
  		var countryResidence = data.countryOfResidence;

  		state.find('option').removeAttr('selected');
  		state.find('option').not(':first').each(function(){
  			$(this).val().toLowerCase() === countryResidence.stateName.toLowerCase() && $(this).prop('selected', true);
  		})
  		state.closest('[data-customselect]').defaultSelect('refresh');

  		countryResidenceInput.val(countryResidence.countryName);
  		address.val(countryResidence.address);
  		city.val(countryResidence.cityName);
  		zip.val(countryResidence.zipCode)
  	}

  	if(data.prCard) {
  		prCard = data.prCard;
  	}
  }

  var activeInfantBlock = function(data) {
  	var infantBlock = $('[data-infant-block]'),
	  	passengerForm = $('#form-passenger-1'),
	  	lastName = passengerForm.find('input[name="last-name-second-infant"]'),
	  	firstName = passengerForm.find('input[name="first-name-last-infant"]'),
	  	genderCheckbox = passengerForm.find('input[name="gender-sex-last-infant"]'),
	  	passportNum = passengerForm.find('input[name="passport-number-last-infant"]'),
			passportCountry = passengerForm.find('input[name="passport-issue-last-infant"]'),
			national = passengerForm.find('input[name="nationality-last-infant"]'),
			mobileInput = passengerForm.find('input[name="pre-country-code-infant"]'),
	  	expiryDay = passengerForm.find('input[name="passport-day-last-infant"]'),
	  	expiryMonth = passengerForm.find('input[name="passport-month-last-infant"]'),
	  	expiryYear = passengerForm.find('input[name="passport-year-last-infant"]'),
	  	birthDay = passengerForm.find('input[name="birth-day-last-infant"]'),
	  	birthMonth = passengerForm.find('input[name="birth-month-last-infant"]'),
	  	birthYear = passengerForm.find('input[name="birth-year-last-infant"]'),
			title = passengerForm.find('.sub-heading-1--dark'),
			countryResidenceInput = passengerForm.find('input[name="country-residence-infant"]'),
			address = passengerForm.find('textarea[name="address-usa-last-infant"]'),
			city = passengerForm.find('input[name="city-town-last-infant"]'),
			state = passengerForm.find('select[name="state-province-last-infant"]'),
			zip = passengerForm.find('input[name="zip-code"]'),
	  	countryName,
	  	mobileName,
			fullName;

  	infantBlock.removeClass('hidden');

  	lastName.val(data.lastName);
  	firstName.val(data.firstName);

  	fullName = data.firstName + ' ' + data.lastName;

  	title.text('INFANT: ' + fullName);

  	// init DOB
  	var dob = new Date(data.passportExpDate);

  	birthDay.val(dob.getDate());
  	birthMonth.val($.datepicker.formatDate('MM', dob));
  	birthYear.val(dob.getFullYear());

  	// init passport value
  	if(data.passport !== null) {
  		var passportObj = data.passport,
  				passportNumber = passportObj.passportNumber,
  				passportExpiry = new Date(passportObj.expiryDate),
  				mobileDropdown = $('#pre-country-code-label');

  		countryName = globalJson.countryCodes.find(function(country){
  			return country.key === passportObj.nationalityCode;
  		}).value;

  		mobileDropdown.find('option').each(function(){
  			var str = $(this).text();

  			if(str.indexOf(countryName) !== -1) {
  				mobileName = str;
  			}
  		})

  		passportNum.val(passportObj.passportNumber);
  		passportCountry.val(countryName);
  		mobileInput.val(mobileName);
  		national.val(countryName);
  		expiryDay.val(passportExpiry.getDate());
  		expiryMonth.val($.datepicker.formatDate('MM', passportExpiry));
  		expiryYear.val(passportExpiry.getFullYear());
  	}

  	// init gender checkbox

  	genderCheckbox.each(function(){
  		if(data.gender === 'M' && $(this).val() === 'male') {
  			$(this).prop('checked', true)
  		}
  		if(data.gender === 'F' && $(this).val() === 'female') {
  			$(this).prop('checked', true)
  		}
  	})

  	if(data.countryOfResidence) {
  		var countryResidence = data.countryOfResidence;

  		state.find('option').removeAttr('selected');
  		state.find('option').not(':first').each(function(){
  			$(this).val().toLowerCase() === countryResidence.stateName.toLowerCase() && $(this).prop('selected', true);
  		})
  		state.closest('[data-customselect]').defaultSelect('refresh');

  		countryResidenceInput.val(countryResidence.countryName);
  		address.val(countryResidence.address);
  		city.val(countryResidence.cityName);
  		zip.val(countryResidence.zipCode)
  	}

  	if(data.prCard) {
  		prCardInfant = data.prCard;
  	}
  }

  var showGroupConfirm = function() {
  	var navItem = wrapPassenger.find('.booking-nav__item');
  	var navItemLast = navItem.last();
  	var formGroupFull = $('.form-passenger-detail-simple').find('.form-group-full');
  	var buttonNextPassenger = $('.form-passenger-detail-simple').find('.button-next-passenger');
  	var buttonSubmitPassenger = $('.form-passenger-detail-simple').find('.button-submit-passenger');
  	if(formGroupFull !== null){
  		if(navItemLast.hasClass('active')){
  			formGroupFull.removeClass('hidden');
  			buttonNextPassenger.addClass('hidden');
  			buttonSubmitPassenger.removeClass('hidden');
  		} else {
  			formGroupFull.addClass('hidden');
  			buttonNextPassenger.removeClass('hidden');
  			buttonSubmitPassenger.addClass('hidden');
  		}
  	}
  };

  var activeBlockUsbound = function(flightData){
  	var usboundBlock = $('[data-passenger-block=".block-passenger-apis"]'),
  			isActive = false;
  	if(flightData.origin && flightData.origin.countryCode && flightData.origin.countryCode === 'USA') {
  			isActive = true;
  	}
  	if(flightData.destination && flightData.destination.countryCode && flightData.destination.countryCode === 'USA') {
  			isActive = true;
  	}

  	isActive && usboundBlock.removeClass('hidden');
  }

	var urlPaxDetails;
	var renderBlockPassengerDetails = function(){
    var templatePaxDetails;
    var appendDiv = $('[data-fixed]').find('.booking-nav');
    appendDiv.empty();
    var paxDetailsTpl = function(data1){
      $.get(global.config.url.paxDetailsTpl, function (data) {
	      var passengerSelected = {},
	      	infantData = {};
        var template = window._.template(data, {
          data: data1
        });
        templatePaxDetails = $(template);
        appendDiv.html(templatePaxDetails);
        var formPassenger = $('.form-passenger-detail');
        var wrapPassenger = $('.wrap-passenger');
        var bookingNavItem = wrapPassenger.find('.booking-nav__item');
        var tabSelectOption = wrapPassenger.find('.tab-select').find('option');
        var dataPassengers = data1.passengers;
        var dataPassengerID , dataFirstName, dataLastName, passengerTypeInfant;
       	for (var i = 0; i < dataPassengers.length; i++) {
					if(dataPassengers[i].passengerType == 'I'){
						dataPassengerID = dataPassengers[i].passengerID;
						dataFirstName = dataPassengers[i].firstName;
						dataLastName = dataPassengers[i].lastName;
						passengerTypeInfant = "- INFANT";
					}
				}
				bookingNavItem.each(function(idex) {
					$(this).find('.passenger-info__number').text(idex + 1 + '.');
					if ($(this).data('passengerid') == dataPassengerID) {
						$(this).find('.passenger-type-infant').text(dataFirstName + ' ' + dataLastName + ' ' + passengerTypeInfant);
					}
				});
				tabSelectOption.each(function(idex) {
					$(this).val(idex + 1);
					$(this).attr('data-idx', idex);
				});
				initValidation(formPassenger);
    		passengerSelected = data1.passengers.filter(function(passenger){
        	return passenger.activePassenger;
        })[0];

        infantData = data1.passengers.filter(function(passenger){
        	return (passenger.passengerID === passengerSelected.passengerID) && (passenger.passengerType === 'I');
        })[0];

        changeFormData(passengerSelected);

        !_.isEmpty(infantData) && activeInfantBlock(infantData);

        initData = data1;

        showGroupConfirm();
        activeBlockUsbound(data1.flights[0]);
      });
    };
    $.ajax({
      url: urlPaxDetails,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data1 = reponse.paxRecordVO;
        paxDetailsTpl(data1);
      }
    });
  };
  if($('body').hasClass('passenger-details-simple-page')){
  	isIcePage = true;
  	urlPaxDetails = $('[data-tab]').data('json-url');
  	renderBlockPassengerDetails();
  }
};

SIA.FormValidator = function() {
	var p = {};

	p.errElClss = '.gigya-error-msg';

	var init = function() {

	};

	var attachEvnt = function(obj) {
		var evnts = ['keypress', 'blur', 'click', 'focus'];

		if (typeof obj != 'undefined' && typeof obj.el != 'undefined') {
			for (var i = 0, iLen = evnts.length; i < iLen; i++) {
				var evntType = evnts[i];
				var evntFunc = obj[evntType];

				if (typeof evntFunc != 'undefined') {
					obj.el.on(evntType, evntFunc);
				}
			}
		}
	};

	var validateEmail = function(val) {
		var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

		return {
			state: emailReg.test(val),
			msg: 'Please enter a valid email address.'
		};
	};

	var validateEmpty = function(val) {
		return {
			state: (val != '') ? true : false,
			msg: 'This field is required'
		};
	};

	var isNotNumeric = function(val) {
		var value = val;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			var str = value[i];
			if (isNaN(str)) {
				return true;
			}
		}

		return false;
	};

	var isValRepetitive = function(val) {
		var value = val;
		var repetitiveTotal = parseInt(value[0]) * value.length;
		var valueTotal = 0;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == repetitiveTotal) ? true : false;
	};

	var isValSequential = function(val) {
		var value = val;
		var sequentialTotal = 0;
		var valueTotal = 0;

		for (var j = parseInt(value[0]), jLen = parseInt(value[value.length - 1]); j <= jLen; j++) {
			sequentialTotal += j;
		}

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == sequentialTotal) ? true : false;
	};

	var computeBirthDate = function(val) {
		var date = parseDate(val);

		// first month starts at 0
		var birthDate = new Date(date.year, date.month - 1, date.day);
		var nowDate = new Date();
		var age =  nowDate.getFullYear() - birthDate.getFullYear();
		var month = nowDate.getMonth() - birthDate.getMonth();

		// compute the exact birthdate of the user
		if(month < 0 || (month === 0 && nowDate.getDate() < birthDate.getDate())) { age--; }

		return age;
	};

	var validatePassword = function(val) {
		if (val.length < 6) {
			return {
				state: false,
				msg: 'Please enter a 6 digit PIN.'
			};
		} else if (isValRepetitive(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that has different digits.'
			};
		} else if (isValSequential(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that is not formed by sequential digits.'
			};
		} else if (isNotNumeric(val)) {
			return {
				state: false,
				msg: 'Enter numbers only'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validatePassportExpDate = function(val, arrivalDate){
		var dateFormatValid = validateDateFormat(val);

		if (! dateFormatValid.state) {
			return dateFormatValid;
		} else {
			var date = parseDate(val);

			// convert passport exp. date to numerical
			var passportExpMonth = new Date(Date.parse(date.month +" 1, 2012")).getMonth() + 1;
			var nowDate = new Date();

			var month = arrivalDate - passportExpMonth;
			if ( (month > 0 && month < 6) || ( (month < 0 || month >= 6) && date.year < parseInt(nowDate.getFullYear())) ) {
				return {
					state: false,
					msg: 'Your passport must be valid for at least six months from your arrival date.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		}
	};

	var validateGuardianAge = function(val) {
		var guardianMinAge = 2;
		var guardianMaxAge = 12;
		var age = computeBirthDate(val);

		if (age < 2) {
			return {
				state: false,
				msg: 'A KrisFlyer member must be at least 2 years old.',
				needsGuardian: false
			};
		} else if (age >= guardianMinAge && age <= guardianMaxAge) {
			return {
				state: true,
				msg: '',
				needsGuardian: true
			};
		} else {
			return validateLegalAge(val);
		}
	};

	var validateLegalAge = function(val) {
		var dateFormat = validateDateFormat(val);

		if (dateFormat.state) {
			// compute the exact birthdate of the user
			var age = computeBirthDate(val);

			if (age < 16) {
				return {
					state: false,
					msg: 'You must be at least 16 years old to be a basic account holder.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var parseDate = function(val) {
		var value = val;
		var fSeparator = value.indexOf('/');
		var lSeparator = value.lastIndexOf('/');

		return {
			day: value.slice(0, fSeparator).trim(),
			month: value.slice(fSeparator + 1, lSeparator).trim(),
			year: value.slice(lSeparator + 1).trim()
		};
	};

	var validateDateFormat = function(val) {
		var date = parseDate(val);

		if (date.day == '' && date.month == '' && date.year == '') {
			return {
				state: false,
				msg: 'This date is required'
			};
		} else if (parseInt(date.day) < 1 || parseInt(date.day) > 31) {
			return {
				state: false,
				msg: 'Please enter a valid day'
			};
		} else if (parseInt(date.month) < 0 || parseInt(date.month) == 0 || parseInt(date.month) > 12) {
			return {
				state: false,
				msg: 'Please enter a valid month'
			};
		} else if (isNaN(date.day) || isNaN(date.month) || isNaN(date.year)) {
			return {
				state: false,
				msg: 'Please enter digits from [0-9]'
			};
		} else if (date.year.length < 4) {
			 return {
				state: false,
				msg: 'Please enter a valid year'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validateAlphaSpecialChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaSpecialChars = /^[A-Za-z\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, and if required, any of these symbols /\'@()\”\-'
			};
		}
	};

	var validateAlphaNumSpecChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaNumSpecialChars = /^[A-Za-z0-9\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaNumSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, the numbers 0 - 9, and if required, any of these symbols /\'@()”-'
			};
		}
	}

	var addErrState = function(el, inputclss, msgClss, errMsg) {
		el.addClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.addClass(msgClss);
		msgEl.html(errMsg);
	};

	var addValidState = function(el, inputclss, msgClss) {
		el.addClass(inputclss);
	};

	var removeInputState = function(el, inputclss, msgClss) {
		el.removeClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.removeClass(msgClss);
		msgEl.html('');
	};

	var inputValidator = function(el, result, errClss, validClss) {
		if (!result.state) {
			addErrState(el, errClss.input, errClss.msg, result.msg);
		} else {
			removeInputState(el, errClss.input, errClss.msg);
			addValidState(el, validClss);
		}
	};

	return {
		init: init,
		attachEvnt : attachEvnt,
		validateEmail : validateEmail,
		validateEmpty : validateEmpty,
		validatePassword : validatePassword,
		addErrState : addErrState,
		addValidState : addValidState,
		removeInputState : removeInputState,
		inputValidator : inputValidator,
		validateDateFormat: validateDateFormat,
		validateLegalAge: validateLegalAge,
		validateGuardianAge: validateGuardianAge,
		validateAlphaSpecialChars: validateAlphaSpecialChars,
		validateAlphaNumSpecChars: validateAlphaNumSpecChars,
		validatePassportExpDate: validatePassportExpDate,
		parseDate: parseDate
	};
}();

SIA.DatePicker = function(){
	var p = {};

	p.datePlaceholder = '<div data-datepicker style="position:absolute;top: 28px;left: 5px;height: 40px;width: 100%;">\
		<input type="text" placeholder="DD" data-day style="padding-left: 8px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="MM" data-month style="padding-left: 5px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="YYYY *" data-year style="padding-left: 5px;background-color:transparent;width: 52px;border: none;text-align: left;height:100%;color: black;">\
	</div>';

	p.inputDay = '[data-day]';
	p.inputMonth = '[data-month]';
	p.inputYear = '[data-year]';

	var init = function(elParent, el) {
		p.dateParentEl = elParent;
		p.dateEl = el;

		customize(p.dateParentEl, p.dateEl);
		evnt(p.dateParentEl);
	};

	var customize = function(elParent, el) {
		elParent.css('position', 'relative');
		elParent.find(el).after($(p.datePlaceholder));
	};

	var transferVal = function() {
		var day = p.dateParentEl.find(p.inputDay).val();
		var month = p.dateParentEl.find(p.inputMonth).val();
		var year = p.dateParentEl.find(p.inputYear).val();
		var separator = '  /  ';

		p.dateEl.val(day + separator + month + separator + year);
	};

	var isInputEmpty = function(el) {
		return el.val().length == 0 ? true : false;
	};

	var removeErrorState = function(el) {
		var gigyaErrorMsgEl = el.find('.gigya-error-msg');
		gigyaErrorMsgEl.html('');
		gigyaErrorMsgEl.removeClass('gigya-error-msg-active');

		el.find('.gigya-error').removeClass('gigya-error');
	};

	var moveInputFocus = function(el) {
		el.on('keyup.moveInputFocus', p.inputDay + ', ' + p.inputMonth  + ', ' + p.inputYear, function() {
			var t = $(this);
			var nxtFocus = t.next().next();

			if ((t.val().length == 2) &&
				nxtFocus.length) {

				nxtFocus.focus();
				removeErrorState(p.dateParentEl);

				if (! isInputEmpty(nxtFocus)) {
					nxtFocus.select();
				}
			}
		});
	};

	var moveKeysFocus = function(el) {
		el.on('keydown', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function(event) {
			var t = $(this);
			var inputFocus = null;
			var inputVal = null;
			var backspaceKey = 8;
			var tabkey = 9;

			// tab key
			if (event.keyCode == tabkey) {
				inputFocus = t.next().next();

				p.dateParentEl.off('keyup.moveInputFocus');

				if (inputFocus.length) {
					event.preventDefault();

					if (isInputEmpty(inputFocus)) {
						inputFocus.focus();
						removeErrorState(p.dateParentEl);

						setTimeout(function() {
							removeErrorState(p.dateParentEl);
						}, 100);
					} else {
						inputFocus.select();
					}
				}
			} else {
				moveInputFocus(p.dateParentEl);
			}

			// backspace key
			if (event.keyCode == backspaceKey && t.val().length == 0) {
				t.prev().prev().focus()
				removeErrorState(p.dateParentEl);
			}
		});
	};

	var evnt = function(elParent) {
		elParent.on('blur', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			// transfer to actual input field
			transferVal();
		});

		elParent.on('click', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			var t = $(this);

			// check if DD || MM || YYYY field is empty, then focus to DD field.
			var fieldParent = t.parent();
			var dayField = fieldParent.find(p.inputDay);
			if ( isInputEmpty(dayField) &&
				isInputEmpty(fieldParent.find(p.inputMonth)) &&
				isInputEmpty(fieldParent.find(p.inputYear)) ) {
					dayField.focus();

					// remove error msg / styles,
					// because it causes an on blur event when changing the focus to DD field
					removeErrorState(elParent);

					return;
			}
		});

		moveInputFocus(elParent);
		moveKeysFocus(elParent);
	};

	return {
		init: init
	};
};

SIA.PassengerDetails = function() {
	var p = {};

	p.formValidator = SIA.FormValidator;
	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';
	p.mainForm = $('#form-passenger-1');

	var init = function() {
		p.lastName = $('#last-name-second');

		p.passportExpDate = $('#passportExpDate');

		if  (p.passportExpDate.length) {
			var passportExpDatePrnt = p.passportExpDate.parent();

			var passengerExpDateEl = new SIA.DatePicker();
			passengerExpDateEl.init(passportExpDatePrnt, p.passportExpDate);

			// customize style
			passportExpDatePrnt.find('[data-datepicker]').css('top', '0');
			p.passportExpDate.css('color', 'transparent');

			// remove asterisk in yyyy field
			var yearField = passportExpDatePrnt.find('[data-year]');
			var yearPlaceholder = yearField.attr('placeholder');
			yearField.attr('placeholder', yearPlaceholder.slice(0, yearPlaceholder.length -1));

			validateDate(passportExpDatePrnt, p.passportExpDate);
		}


		removeClearBtn();

		p.noFirstNameCheckBox = $('#checkbox-name-last');
		noFirstName(p.noFirstNameCheckBox);

		nextPassenger();
		sideNav();
		lastname();
	};

	var removeClearBtn = function (){
		var removeClearBtn = setInterval(function() {
			p.passportExpDate.parent().find('.add-clear-text').remove();
		}, 500);

		setInterval(function() {
			var clearBtnExist = p.passportExpDate.parent().find('.add-clear-text').length;

			if  (clearBtnExist) {
				clearInterval(removeClearBtn);
			}
		}, 500);
	};

	var validateDate = function(elParentInput, elInput) {
		var dateFields = [
			elParentInput.find('[data-day]'),
			elParentInput.find('[data-month]'),
			elParentInput.find('[data-year]')
		];

		for (var j = 0, jLen = dateFields.length; j < jLen; j++) {
			var el = dateFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					var validateRes = p.formValidator.validatePassportExpDate(elInput.val(), getArrivalDate());
					p.formValidator.inputValidator(
						elInput,
						validateRes,
						p.gigyaErrClss,
						p.gigyaValidClss);

					addErrClss(elInput, validateRes);

					if ($(this).val() == '') {
						elInput.val('');
					}

					if (! validateRes.state) {
						p.passportExpDate.addClass('error');
						elInput.val('');
					} else {
						p.passportExpDate.removeClass('error');
					}
				},
				focus: function() {
					$(this).closest('.input-1').removeClass('input-error');
				}
			});

			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}
		}

		p.formValidator.attachEvnt({
			el: elInput,
			focus: function() {
				var result = p.formValidator.validatePassportExpDate(elInput.val(), getArrivalDate());
				p.formValidator.inputValidator(
					elInput,
					result,
					p.gigyaErrClss,
					p.gigyaValidClss);

				if (! result.state) {
					p.passportExpDate.addClass('error');
					elInput.val('');
				} else {
					p.passportExpDate.removeClass('error');
				}
			}
		});
	};

	var addErrClss = function(input, result) {
		var input = input.parent();
		if (!result.state) {
			input.addClass('input-error');
		} else {
			input.removeClass('input-error');
		}
	};

	var noFirstName = function(el) {
		el.on('click', function() {
			var t = $(this);
			var sideNav = $('.booking-nav.booking-nav--1');

			if (t.prop('checked')) {
				// look for active then remove the firstname
				var sideNavActive = sideNav.find('.booking-nav__item.active');

				sideNavActive.find('.passenger-firstname').html('');
			}
		});
	};

	var lastname = function () {
		p.formValidator.attachEvnt({
			el: p.lastName,
			blur: function () {
				var t = $(this);
				var value = t.val();

				var result = p.formValidator.validateAlphaSpecialChars(p.lastName);
				p.formValidator.inputValidator(
					p.lastName,
					result,
					p.gigyaErrClss,
					p.gigyaValidClss);

				var parent = p.lastName.parent();
				var errorEl = p.lastName.closest('.grid-inner').find('.gigya-error-msg');
				var nextPassengerBtn = p.mainForm.parent().find('#btn-next input');
				if (!result.state && !p.lastName.hasClass('error')) {
					parent.addClass('input-error');
					p.lastName.addClass('error');
					errorEl.html(result.msg);
					errorEl.addClass('error-last-name');

					p.lastName.addClass('has-error');
				} else {
					parent.removeClass('input-error');
					p.lastName.removeClass('error');
					errorEl.html('');
					errorEl.removeClass('error-last-name');

					p.lastName.removeClass('has-error');
				}
			}
		});
	};

	var changeContactTitle = function() {
		var navItems = $('.booking-nav.booking-nav--1');

		// change contact details heading to lowercase
		// assumes that contact details is always on the last nav item
		var contactDetails = navItems.find('a[data-idx]');
		var contactDetailTitle = p.mainForm.find('h3.sub-heading-1--blue');

		var activeNav = navItems.find('.active');

		if (parseInt(activeNav.attr('data-idx')) == (contactDetails.length - 1)) {
			contactDetailTitle.addClass('sub-heading-2');
		} else {
			contactDetailTitle.removeClass('sub-heading-2');
		}
	};

	var nextPassenger = function() {
		var nextPassengerBtn = p.mainForm.parent().find('#btn-next input');

		nextPassengerBtn.on('click', function() {
			// call it again to ensure that validation is completed
			// before manipulating the passport exp. date field.
			p.mainForm.valid();

			var inputErrors = p.mainForm.find('input.error');

			// if the passport exp. date is the only field that has error
			if (inputErrors.length === 1 && inputErrors.attr('id') == 'passportExpDate'){
				p.passportExpDate.focus();

				addErrClss(
					p.passportExpDate,
					p.formValidator.validatePassportExpDate(p.passportExpDate.val(), getArrivalDate()));
			}

			if (inputErrors.length > 1 && p.passportExpDate.length){
				var result = p.formValidator.validatePassportExpDate(p.passportExpDate.val(), getArrivalDate());
				addErrClss(p.passportExpDate, result);
				p.passportExpDate.parent().find('.gigya-error-msg-active').html(result.msg);
			}


			setTimeout(function (){
				changeContactTitle();
			}, 1000);
		});
	};

	var getArrivalDate = function() {
		var arrivalDateEl = $('[data-booking-summary-panel]')
			.find('.bsp-flights__info--group.position-even')
			.find('.bsp-flights__info--detail').eq(1)
			.find('.time').html();

		// return the month only
		// assumes format is DD/MM/(DAY)
		var date = arrivalDateEl.split(' ')[1];

		// convert month to numerical
		return new Date(Date.parse(date +" 1, 2012")).getMonth() + 1;
	};

	var sideNav = function() {
		p.sideNav = $('.booking-nav.booking-nav--1');

		p.sideNav.find('.booking-nav__item').on('click', function (){
			var navItems = $(this).parent();

			setTimeout(function() {
				// restore passport exp. date
				var passportExpDate = $('#passportExpDate');
				if (passportExpDate.length){
					var prevDate = p.formValidator.parseDate(passportExpDate.val());

					passportExpDate.parent().find('[data-day]').val(prevDate.day);
					passportExpDate.parent().find('[data-month]').val(prevDate.month);
					passportExpDate.parent().find('[data-year]').val(prevDate.year);
				}
			}, 100);

			setTimeout(function (){
				changeContactTitle();
			}, 1000);
		});
	};

	return {
		init: init
	};
}();

SIA.PassengerDetails.init();
