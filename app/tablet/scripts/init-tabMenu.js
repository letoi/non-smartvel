/**
 * @name SIA
 * @description Define global initTabMenu functions
 * @version 1.0
 */
SIA.initTabMenu = function(){
	var wrapperTab = $('[data-wrapper-tab]');

	if(wrapperTab.length) {
		var global = SIA.global,
				win = global.vars.win,
				timerResize;

		var setSelectWidth = function() {
			if(wrapperTab.hasClass('multi-tabs') || wrapperTab.hasClass('multi-tabs-extra')) {
				if(win.width() < global.config.tablet) {
					wrapperTab.children('select').width(wrapperTab.find('li.active').width());
				} else {
					wrapperTab.children('select').width('');
				}
			}
		};

		win.off('resize.tabMenu').on('resize.tabMenu', function() {
			clearTimeout(timerResize);
			timerResize = setTimeout(function() {
				setSelectWidth();
			}, 400);
		}).trigger('resize.tabMenu');
	}

	// init tab plugin
	$('.tabs--1').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active'
	});

	$('.tabs--2').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active'
	});

	$('.tabs--4').tabMenu({
		tab: '> ul.main--tabs .tab-item',
		tabContent: '> div.tab-wrapper-1 > div.tab-content-1',
		activeClass: 'active'
	});
};
