SIA.mpAddOnsChanges = function () {

    var addWeightSelect = function () {
        var addWeight = $(".add-weight-select").get();
        var segment = {};

        var applyBaggages = function (segmentIdx) {
            var count = 0;
            var segmentObj = segment["seg-"+segmentIdx];
            for(var key in segmentObj) {
                if (segmentObj[key]) count++;
            }

            if (count) {
                $(".segment-" + segmentIdx + ">span").html(count);
                return true;
            } else {
                return false;
            }
        };

        var hideOrShowAdditionalBaggages = function (el, state) {
            if (state) {
                $(el).removeClass("hidden");
            } else {
                $(el).addClass("hidden");
            }
        };
        
        _.each(addWeight, function(select) {
            var initialValue = $(select).find("option:selected").text();
            changeWeightText(select, initialValue);
            
            $(select).on("change", function() {
                var baggages = $(this).attr("id").split("-");
                var value = parseInt(this.value);
                var selectTextValue = $(this).find("option:selected").text();
                changeWeightText(this, selectTextValue);

                segment["seg-"+baggages[0]] = segment["seg-"+baggages[0]] ? segment["seg-"+baggages[0]] : {};
                segment["seg-"+baggages[0]]["bag"+baggages[1]] = value;
            });
        });

        $(".addons-landing-content").delegate(".accordion__control", "click", function () {
            var xbagAccordion = $(".xbag-accordion");
            var bundleAccordion = $(".bundle-accordion");
            _.each(xbagAccordion, function(singleAccordion) {
                var accordionControl = $(singleAccordion).find(".accordion__control");
                var expanded = accordionControl.hasClass("active");
                var el = accordionControl.children(".bundle-selected").get(0);
                
                if ($("body").hasClass("mp-1-addons-piece-page")) {
                    var parentAccordion = $(singleAccordion);
                    var totalBaggage = 0;
                    var baggageSelected = parentAccordion.find(".btn-8.selected");
                    var isActive = accordionControl.hasClass("active");
                    var bundleSelected = accordionControl.find(".bundle-selected");

                    baggageSelected.each(function() {
                        var self = $(this);
                        var parent = self.closest("[data-parent-accordion]");
                        parent.find("input[type='tel']").each(function() {
                            totalBaggage += parseInt($(this).val());
                        });
                    });

                    if (totalBaggage > 0) {
                        bundleSelected.find("span").text(totalBaggage.toString());
                        bundleSelected[(!isActive ? "removeClass" : "addClass")]("hidden");
                    } else {
                        bundleSelected.addClass("hidden");
                    }

                    return;
                }

                if (!expanded) { 
                    if ($(el).hasClass("segment-0")) {
                        var status = applyBaggages(0);
                        hideOrShowAdditionalBaggages(el, status);
                    }
                    if ($(el).hasClass("segment-1")) {
                        var status = applyBaggages(1);
                        hideOrShowAdditionalBaggages(el, status);                    
                    }
                } else {
                    $(el).addClass("hidden");
                }
            });
            _.each(bundleAccordion, function(singleAccordion) {
                var accordionControl = $(singleAccordion).find(".accordion__control");                
                var expanded = accordionControl.hasClass("active");
                
                if (!expanded) {
                    accordionControl.find(".bundle-selected").removeClass("hidden");
                } else {
                    accordionControl.find(".bundle-selected").addClass("hidden");                    
                }
            });
        });
    };

    var changeWeightText = function (el, textValue) {
        $(el).siblings(".select__text").text(textValue);
    }

    var accordionButtonControl = function(el) {
        var buttonControls = [];
        var self = el;

        setTimeout(function() {
            var accordionStatus = $(self.closest("[data-accordion-trigger]").get(0)).hasClass("active");
            var parentDataAccordion = $(self.closest("[data-accordion='1']").get(0));
            buttonControls = parentDataAccordion.find(".accordion_button_control");

            self.addClass("hidden");
            if (accordionStatus) {
                resetButtonState(buttonControls, self);
                self.next().removeClass("hidden");
            } else {
                self.prev().removeClass("hidden");
            }
        }, 100);
    };

    var resetButtonState = function(buttons, self) {
        _.each(buttons, function(button) {
            if (!$(button).is(self) || !self) {
                $(button).removeClass("hidden");
                $(button).next().addClass("hidden");
                setTimeout(function(){ 
                    $(button).siblings(".chevron-ico").find("em").addClass("hidden");
                }, 300);
            }
        });
    };

    var triggerClickListener = function() {
        $(".accordion_button_control, .selected-button-1").click(function() {
            accordionButtonControl($(this));
        });
    };

    var broadcastListener = function() {
        $(".accordion_button_control").on("iconCLicked", function (evt, button) {
            resetButtonState([button]);
        });
    };

    var init = function () {
        addWeightSelect();
        triggerClickListener();
        broadcastListener();
    };

    if ($("body").hasClass("mp-1-add-insurance")) {
		SIA.accordionCustom();
	}

    init();
};

SIA.mpAddOnsPieceChanges = function () {
    var calculateBaggagePrice = function(self) {
        var parent = self.closest("[data-plus-or-minus-number]");
        var priceValue = parent.siblings(".sgd-price").find("span");
        var unitValue = parseFloat(parent.data("unit"));
        var unitMultiplier = parseInt(parent.find("input").val());
        var calculatePrice = (unitValue * unitMultiplier);
        if (calculatePrice > 0) {
            parent.siblings(".sgd-price").removeClass("hidden");
            priceValue.text(calculatePrice.toFixed(2));
        } else {
            parent.siblings(".sgd-price").addClass("hidden");
        }
    };

    var plusMinusButtonListener = function() {
        $("[data-plus-or-minus-number]").find("button").on("click", function() {
            var self = $(this);
            var parent = self.closest("[data-parent-accordion]");
            var trigger = parent.find("[data-child-accordion-click]").find("input");
            var haveBaggageSelected = false;

            setTimeout(function() {
                calculateBaggagePrice(self);
                parent.find("input[type='tel']").each(function() {
                    if (parseInt($(this).val()) > 0) {
                        haveBaggageSelected = true;
                    }
                });
                trigger[(haveBaggageSelected ? "addClass" : "removeClass")]("selected");
                trigger.val((!haveBaggageSelected ? "Select" : "Selected"));
            }, 100);
        });
    };

    var addBagPiece = function() {
        $("[data-child-accordion-click]").on("click", function() {
            var self = $(this);
            var parent = self.closest("[data-parent-accordion]");
            var inputButton = self.find("input[type='button']");
            var isActive = parent.find("[data-child-accordion-content]").hasClass("active");
            
            if (isActive) {
                $("[data-child-accordion-click]").trigger("childAccordionTrigger", {
                    up: true,
                    self: self
                });
            } else if (!isActive) {
                $("[data-child-accordion-click]").trigger("childAccordionTrigger", {
                    down: true,
                    self: self
                });
            }
        });
    };

    var init = function () {
        plusMinusButtonListener();
        addBagPiece();
    };

    init();
};