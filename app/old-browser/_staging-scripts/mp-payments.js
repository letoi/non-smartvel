/**
 * @name SIA
 * @description Define global mpPayments functions
 * @version 1.0
 */

SIA.mpPayments = function(){
  // Promo code
  var promoBlock = $('.mp-payments .promo-code'),
      inputPromo = promoBlock.find('.input-1 input'),
      inputWrapper = inputPromo.closest('.form-group'),
      btnPromo = promoBlock.find('.btn-1'),
      alertBlock = promoBlock.find('.alert-block'),
      removeLink = promoBlock.find('.link-4'),
      errorContent = '<p class="text-error" id="input-promo-multi-city-error"><span>' + inputPromo.data('msg-required') + '</span></p>'
  btnPromo.on('click', function(e) {
    e.preventDefault();
    if (inputPromo.val()) {
      alertBlock.fadeIn();
    } else if (!inputWrapper.hasClass('error')) {
      inputWrapper.addClass('error').append(errorContent);
    }
  });
  removeLink.on('click', function(e) {
    e.preventDefault();
    alertBlock.fadeOut();
  });

  // Sidebar
  $(".mp-payments .sidebar-1 .method-nav__item").each(function(){
    $(this).off('click.handleTabPayment').on('click.handleTabPayment', function(e){
      e.preventDefault();
      $(this).siblings('a.active').removeClass("active");
      $(this).addClass("active");
      var index = $(this).index();
      $(".mp-payments .payment-method-content .tab-content").removeClass("active");
      $(".mp-payments .payment-method-content .tab-content").eq(index).addClass("active");
    })
  })

  // Sub-total price
  var accordionItem = $('.accordion-item');
  // Calculate total of each accordion
  var calculateTotal = function() {
    accordionItem.each(function(index) {
      var flightCostPanel = $(this).find('.flights-cost__details'),
          subTotalText = $(this).find('.mp-payments-total .sub-total .price'),
          accordionSubTotal = $(this).find('.group-title .sub-total span'),
          grandTotalText = 0,
          totalPricePaid = $('.total-price-paid .unit');

      flightCostPanel.each(function() {
        var itemPrice = $(this).find('li').not('.sub-total').find('.price'),
            newSubTotalText = 0;
        itemPrice.each(function(index) {
          var itemPriceText = $(this).text();
          if (itemPriceText) {
            newSubTotalText = newSubTotalText + Number(itemPriceText.replace(',', ''));
            // itemPriceText = Number($(this).text()).toLocaleString(undefined, {
            //   minimumFractionDigits: 2,
            //   maximumFractionDigits: 2
            // });
            // $(this).text(itemPriceText);
          }
        });
        newSubTotalText = newSubTotalText.toLocaleString(undefined, {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        $(this).find('.sub-total .price').html(newSubTotalText);
      });

      subTotalText.each(function() {
        grandTotalText = grandTotalText + Number($(this).text().replace(',', ''));
      });
      grandTotalText = grandTotalText.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
      accordionSubTotal.html(grandTotalText);
      if (index === accordionItem.length-1) {
        $(this).find('.mp-payments-total .grand-total .text-total span').html(grandTotalText);
      }
      totalPricePaid.html(totalPricePaid.text().substring(0,4) + grandTotalText);
    });
  };
  calculateTotal();

  var removePriceLink = accordionItem.find('.link-remove');
  removePriceLink.on('click', function(e) {
    e.preventDefault();
    var storeItemText = $(this).siblings('span').text();
    $(this).parent('li').remove();
    accordionItem.last().find('li:contains("' + storeItemText + '")').remove();
    calculateTotal();
  });

  var blockAccordionItem = $('[data-block-accordion-item]')
  blockAccordionItem.each(function(index) {
    $(this).find('[data-number]').text(index + 1);
  });

  var addAddress = function() {
    var btnAdd = $('[data-add-address]'),
        index = 0,
        isFirst = true;

    btnAdd.off('click.addAddress').on('click.addAddress', function(e){
      e.preventDefault()
      var addressInputLength = $(this).closest('.address-name').find('.grid-col').length;

      addressInputLength === 3 && $(this).addClass('hidden');

      if(addressInputLength < 4) {
        index = isFirst ? parseInt($(this).data('index')) + 1 : index + 1;
        var str = '<div class="grid-col full"><div class="grid-inner"><span data-address-line'+index+' class="input-1"><input type="text" name="address-line'+index+'"="" id="address-line'+index+'" placeholder="Address line '+ index +'" value="" aria-labelledby="address-line'+index+'-error" data-rule-address="true" data-msg-address="Enter a valid address line." class="default valid" aria-invalid="false"><a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a></span></div></div>';
        $(str).insertBefore($(this));
        isFirst = false;
        SIA.fixPlaceholder();
      }
    })
  }
  $('input#credit-debit-number').payment('formatCardNumber');

  addAddress();
};
