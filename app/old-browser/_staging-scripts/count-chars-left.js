/**
 * @name SIA
 * @description Define CountCharLeft functions
 * @version 1.0
 */
SIA.countCharsLeft = function() {
	var countCharsLeftEls = $('[data-count-chars-left]');

	var countCharsLeft = function(countEl) {
		var limitEl = countEl.parent().siblings('.limit-character');
		var maxLength = parseInt(countEl.attr('maxlength'));
		var vlue = countEl.val();
		var length = vlue.length;
		var charLeft = maxLength - length;
		var charText = '';

		charText = charLeft <= 0 ?
			'0 character left' : charLeft === 1 ?
			'1 character left' : window.accounting.formatNumber(charLeft, false) + ' characters left.';

		if(length > maxLength) {
			countEl.val(vlue.slice(0, maxLength));
			limitEl.text('0 character left.');
		}
		else {
			limitEl.text(charText);
		}
	};

	countCharsLeftEls
		.off('keyup.feedBack change.feedBack blur.feedBack focus.feedBack')
		.on('keyup.feedBack change.feedBack blur.feedBack focus.feedBack', function() {
			countCharsLeft($(this));
		})
		.off('paste.feedBack')
		.on('paste.feedBack', function() {
			setTimeout(function() {
				countCharsLeft($(this));
			}, 200);
		})
		.trigger('keyup.feedBack');
};
