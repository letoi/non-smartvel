/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.coachBeta = function() {
	var body = $('body');
	var coachs = $('.coach-popup');
	var closeBtn = $('[data-close-coach]');
	var nextBtn = $('[data-proceed]');
	var overlay = $('.overlay-beta');
	var overlayInner = overlay.find('.overlay-beta__inner');
	var freezeFlag = false;
	var sticky = $();
	var toggleBtn = $();
	var minibarTpl = '<em class="arrow-up"></em>BETA';
	var nonMinibarTpl = '<em class="arrow-right"></em>BETA.Singaporeair.com';

	var showFirstCoach = function() {
		showCoach($('.coach-1'));
	};

	var freezeBody = function(){
		body.attr('scroll', 'no');
		freezeFlag = true;
	};

	var deFreeze = function(){
		if(freezeFlag){
			body.removeAttr('scroll');
			freezeFlag = false;
		}
	};

	var deFreeze = function(){
		body.removeAttr('style');
	};

	var showOverlay = function() {
		if (!overlayInner.find('div.bgd-mask').length) {
			// var urls = overlayInner.css('backgroundImage').replace(/^url\(["']?/, '').replace(/["']?\)$/, '').split('images');
			// var url = 'images' + urls[urls.length - 1];
			overlayInner.prepend('<div class="bgd-mask"><img alt="" src="images/bg-coach-desktop.jpg" /></div>');
		}

		var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

		if (winW < 1280 && winW >= 988) {
			overlayInner.addClass('coach-988');
		}
		else {
			overlayInner.removeClass('coach-988');
		}

		overlay.removeClass('hidden');
		if(body.attr('scroll') !== 'no'){
			freezeBody();
		}

		$(window).off('resize.coachbeta').on('resize.coachbeta', function() {
			winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (winW >= 1280) {
				overlayInner.removeClass('coach-988');
			}
		});

		setTimeout(function() {
			overlay.addClass('active');
			toggleBtn.html(nonMinibarTpl);
			sticky.trigger('mouseenter.additionBeta').data('prevent-mouseevent', true);
		}, 0);
	};

	var hideOverlay = function() {
		$(window).off('resize.coachbeta');
		var currentCoach = getCurrentCoach();
		currentCoach.removeClass('active');
		deFreeze();
		toggleBtn.html(minibarTpl);
		sticky.removeData('prevent-mouseevent').trigger('mouseleave.additionBeta');
		overlay.removeClass('active');
		setTimeout(function() {
			currentCoach.addClass('hidden');
			overlay.addClass('hidden');
		}, 700);
	};

	var getCurrentCoach = function() {
		return coachs.filter('.active');
	};

	var showCoach = function(coach) {
		var currentCoach = getCurrentCoach();
		currentCoach.addClass('hidden').removeClass('active');
		coach.removeClass('hidden');
		setTimeout(function(){
			coach.addClass('active');
		}, 0);
	};

	body.on('click.coachBeta', '.what-new-btn', function(e) {
		e.preventDefault();
		if (!(sticky.length && toggleBtn.length)) {
			sticky = $('.sticky');
			toggleBtn = sticky.find('.mini-beta-link');
		}
		if($(this).hasClass('disabled')){
			return;
		}
		$(this).addClass('disabled');
		showOverlay();
		showFirstCoach();
	});

	closeBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		hideOverlay();
		$('.what-new-btn').removeClass('disabled');
	});

	nextBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		var btn = $(this);
		var nextCoach = $(btn.data('next-btn'));
		showCoach(nextCoach);
	});
};
