/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteHome = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var popupPromo = global.vars.popupPromo;
	var travelWidget = $('#travel-widget');
	var formManageBooking = travelWidget.find('#form-manage-booking');
	var formCheckIn = travelWidget.find('#form-check-in');
	var formPackage = travelWidget.find('#form-packages');
	var formFlightStatus = travelWidget.find('#form-flight-status');
	var formFlightStatus1 = travelWidget.find('#form-flight-status-1');
	var loginBtn = $('[data-trigger-popup="true"]');
	var travelWidgetVisibleInput = 'input[type="text"]';
	var popupPromoMember = $('.popup--promo-code-kf-member');
	var popupPromoKF = $('[data-popup-promokf]');

	$.validator.addMethod('bookingEticket', function(value) {
		if (value.length === 6 || value.length === 13) {
			if (value.length === 6) {
				return /^[a-zA-Z0-9]+$/.test(value) && !/(0|1)/.test(value) && !/[-_\s]/g.test(value);
			}
			if (value.length === 13) {
				return /[^-_\s]+$/.test(value) && /^\d+$/.test(value);
			}
		} else {
			return false;
		}
		return true;
	}, L10n.validator.bookingEticket);


	if (travelWidget.length) {
		var formTravel = travelWidget.find('#form-book-travel');
		var radioFilter = formTravel.find('.form-group--tooltips input[type="radio"]');
		var radioTooltips = formTravel.find('.radio-tooltips');

		radioFilter.each(function(index, el) {
			var self = $(el);
			self.off('change.showTooltip').on('change.showTooltip', function() {
				radioTooltips.removeClass('active');
				radioTooltips.eq(index).addClass('active');
			});
		});

		travelWidget.tabMenu({
			tab: '.tab .tab-item',
			tabContent: 'div.tab-content',
			activeClass: 'active',
			templateOverlay: config.template.overlay,
			zIndex: config.zIndex.tabContentOverlay,
			isPopup: false,
			afterChange: function(tabs) {
				var tab = tabs.filter('.active');
				var isFirstFocus = tab.data('focus');
				if (isFirstFocus) {
					tab.find('form input[type=text]:first').focus();
				}
			}
		});
	}

	var highlightSlider = $('#highlight-slider');
	if (highlightSlider.length) {
		var wrapperHLS = highlightSlider.parent();
		var imgsHl = highlightSlider.find('img');
		var loadBackgroundHighlight = function(self, parentSelt, idx) {
			if (idx === imgsHl.length - 1) {
				highlightSlider.width(wrapperHLS.width() + 22);
				highlightSlider.css('visibility', 'visible');
				highlightSlider.find('.slides')
					.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: 4,
						slidesToScroll: 4,
						accessibility: false,
						autoplay: false,
						pauseOnHover: false
					});
				win.off('resize.highlightSlider').on('resize.highlightSlider', function() {
					highlightSlider.width(wrapperHLS.width() + 22);
				}).trigger('resize.highlightSlider');

				$('.popup--dark-site-lightbox').on('afterHide', function() {
					win.trigger('resize.highlightSlider');
				});
			}
		};

		imgsHl.each(function(idx) {
			var self = $(this);
			var parentSelt = self.parent();
			var nI = new Image();
			nI.onload = function() {
				loadBackgroundHighlight(self, parentSelt, idx);
			};
			nI.src = self.attr('src');
		});
	}

	var triggerProCode = $('[data-promo-code-popup]');
	if (triggerProCode.length) {
		var flyingFocus = $('#flying-focus');

		if (globalJson.loggedUser) {
			popupPromo = popupPromoMember;
			global.vars.popupPromo = popupPromoMember;
		}

		popupPromo.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			afterShow: function() {
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
			},
			triggerCloseModal: '.popup__close'
		});

		triggerProCode.off('click.showPromo').on('click.showPromo', function(e) {
			e.preventDefault();
			popupPromo.Popup('show');
		});
	}

	popupPromo.find('.form--promo').validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});

	var validateFormGroup = function(formGroup) {
		formGroup.each(function() {
			var self = $(this);
			self.off('click.triggerValidate').on('click.triggerValidate', function() {
				formGroup.not(self).each(function() {
					if ($(this).data('change')) {
						$(this).find('select, input').valid();
					}
				});
			});

			self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function() {
				formGroup.not(self).each(function() {
					if ($(this).data('change')) {
						$(this).find('select, input').valid();
					}
				});
			}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function() {
				self.data('change', true);
			});
			self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function() {
				self.data('change', true);
			});
		});
	};


	var _manageBookingValidation = function() {
		var formGroup = formManageBooking.find('.form-group');
		validateFormGroup(formGroup);
		formManageBooking.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _checkInValidation = function() {
		var formGroup = formCheckIn.find('.form-group');
		validateFormGroup(formGroup);
		formCheckIn.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatusValidation = function() {
		var formGroup = formFlightStatus.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatus1Validation = function() {
		var formGroup = formFlightStatus1.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus1.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _formPackageValidation = function() {
		var ppSearchLeaving = $('.popup--search-leaving');
		var btnContinue = ppSearchLeaving.find('[data-continue]');

		ppSearchLeaving.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			afterShow: function() {
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
			}
		});

		btnContinue.off('click.searchPackage').on('click.searchPackage', function() {
			ppSearchLeaving.Popup('hide');
			formPackage[0].submit();
		});

		var formGroup = formPackage.find('.form-group');
		validateFormGroup(formGroup);

		formPackage.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				ppSearchLeaving.Popup('show');
				return false;
			}
		});
	};

	var formPromoKFValidation = function() {
		popupPromoKF.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	formPromoKFValidation();

	var _fareDeals = function() {
		var fareDeals = $('.fare-deals .content');
		var fareDealsAutocomplete = $('.fare-deals #fare-deal-city');
		var customSelectFareDeal = fareDealsAutocomplete.closest('[data-customSelect]');

		var renderTemplate = function(json) {
			var allData = json;
			var dataJSON = allData ? allData.promoVO[0] : null;
			var options = [];


			var createSelect = function() {
				options = [];
				for (var i = 0; i < allData.promos.city.length; i++) {
					var option = '<option data-code="' + allData.promos.city[i].code + '-' + allData.promos.city[i].description + '" value="' + allData.promos.city[i].code + '" >' + allData.promos.city[i].description + '</option>';
					options.push(option);
				}
				fareDealsAutocomplete.html(options.join(''));
				fareDealsAutocomplete.closest('[data-customSelect]').customSelect('_createTemplate');
				fareDealsAutocomplete.closest('[data-customSelect]').off('afterSelect.faredeals').on('afterSelect.faredeals', function() {
					var index = getIndex(fareDealsAutocomplete.find(':selected').data('code'));
					if (index !== -1) {
						dataJSON = allData.promoVO[index];
						generateTemplateFromJSON(dataJSON);
					} else {
						generateTemplateFromJSON(null);
					}
				});
			};

			var createDataFaredeals = function(json, numberOfItem, limit) {
				var obj = {
					'listFares': []
				};
				for (var i = 0; i < json.length; i++) {
					if (i < limit) {
						if (i % 2 === 0) {
							if (typeof obj.listFares[0] === 'undefined') {
								obj.listFares[0] = {
									'listDeals': []
								};
							}
							obj.listFares[0].listDeals.push(json[i]);
						} else if (i % 2 === 1) {
							if (typeof obj.listFares[1] === 'undefined') {
								obj.listFares[1] = {
									'listDeals': []
								};
							}
							obj.listFares[1].listDeals.push(json[i]);
						}
					}
				}
				return obj;
			};

			var getIndex = function(value) {
				var idx = -1;
				for (var i = 0; i < allData.promoVO.length; i++) {
					if (allData.promoVO[i].city === value) {
						idx = i;
					}
				}
				return idx;
			};

			var generateTemplateFromJSON = function(json) {
				if (json) {
					for (var k in json.cityVO) {
						json.cityVO[k].price = window.accounting.formatMoney(json.cityVO[k].price, ' ', 0, ',', '.');
					}
					$.get(config.url.templateFareDeal, function(data) {
						data = data.replace(/td>\s+<td/g, 'td><td');
						var template = window._.template(data, {
							data: createDataFaredeals(json.cityVO, 5, 10)
						});
						fareDeals.html(template);
					}, 'html');
				} else {
					fareDeals.html('<p class="fare-deals-note">' + L10n.fareDeal.nodata + '</p>');
				}
			};

			if (fareDeals.length) {
				if (dataJSON) {
					createSelect();
					generateTemplateFromJSON(dataJSON);
				} else {
					var mainHeading = customSelectFareDeal.prev();
					var mainHeadingText = mainHeading.text().split(' ');
					customSelectFareDeal.hide();
					mainHeading.text(mainHeadingText.slice(0, mainHeadingText.length - 1).join(' '));
					fareDeals.html('<p class="fare-deals-note">' + L10n.fareDeal.nofare + '</p>');
				}
			}
		};
		renderTemplate(globalJson.promotionFareDeals);

	};

	_manageBookingValidation();
	_checkInValidation();
	_flightStatusValidation();
	_flightStatus1Validation();
	_formPackageValidation();
	_fareDeals();

	loginBtn.off('click.triggerLoginPopup').on('click.triggerLoginPopup', function(e) {
		e.preventDefault();
		jQuery(loginBtn.data('popup')).Popup('show');
	});

	var checkEmptyInput = function(input) {
		var isEmpty = false;
		input.each(function() {
			if (!$(this).val()) {
				isEmpty = true;
			}
		});
		return isEmpty;
	};

	var changeText = function(form, input, btn) {
		var inputs = form.find(input);
		var b = form.find(btn);
		inputs.each(function() {
			var self = $(this);
			self.off('change.checkEmptyInput').on('change.checkEmptyInput', function() {
				if (!checkEmptyInput(inputs)) {
					b.val(L10n.home.proceed);
				} else {
					b.val(L10n.home.retrive);
				}
			});
		});
	};

	changeText(formManageBooking, travelWidgetVisibleInput, '#retrieve-1');
	changeText(formCheckIn, travelWidgetVisibleInput, '#retrieve-2');

	var bookingWidgetSwitch = function() {
		var manageBookingTabs = $('[data-manage-booking]');
		var manageBookingForms = $('[data-manage-booking-form]');

		var checkinTabs = $('[data-checkin]');
		var checkinForms = $('[data-checkin-form]');

		var flightStatusTabs = $('[data-flight-status]');
		var flightStatusForms = $('[data-flight-status-form]');

		var apply = function(tabs, form, dataTab, dataForm) {
			tabs
				.off('change.switch-tab')
				.on('change.switch-tab', function() {
					var data = $(this).data(dataTab);
					if (!dataForm) {
						dataForm = dataTab + '-form';
					}
					form.removeClass('active').filter('[data-' + dataForm + '="' + data + '"]').addClass('active');
				});
		};

		apply(manageBookingTabs, manageBookingForms, 'manage-booking');
		apply(checkinTabs, checkinForms, 'checkin');
		apply(flightStatusTabs, flightStatusForms, 'flight-status');

		var flightStatusFormSecond = flightStatusForms.filter('[data-flight-status-form="by-number"]');
		var optionDepartingArriving = flightStatusFormSecond.find('[data-option] input');
		var departingArriving = flightStatusFormSecond.find('[data-target]');

		optionDepartingArriving
			.off('change.changeDepartingArriving')
			.on('change.changeDepartingArriving', function() {
				var index = optionDepartingArriving.index($(this));
				departingArriving.removeClass('hidden').eq(index === 0 ? 1 : 0).addClass('hidden');
			});
	};

	var hideElement = function() {
		var bannerSlider = $('#banner-slider');
		bannerSlider.removeClass('flexslider');
		bannerSlider.find('img').remove();
		$('.wrapper.first').remove();
		$('.beta-footer').addClass('hidden');
		$('.footer-top').addClass('hidden');
		$('.footer .social').addClass('hidden');
	};

	var initPackagesSlider = function(){
		var packages = $('.packages');
		var packagesSlider = packages.find('[data-slideshow]');

		var startSlider = function(self, idx){
			var imgBannerLength = packagesSlider.find('.slide-item img').length - 1;
			var option = packagesSlider.data('option') ? $.parseJSON(packagesSlider.data('option').replace(/\'/gi, '"')) : {};
			option.pauseOnHover = false;
			option.siaCustomisations = true;

			if(idx === imgBannerLength){
				packagesSlider.css('visibility', 'visible');
				packagesSlider.find('.slides').slick(option);
			}
		};

		packagesSlider.find('.slide-item img').each(function(idx) {
			var self = $(this);
			var nI = new Image();
			nI.onload = function() {
				startSlider(self, idx);
			};

			nI.src = self.attr('src');
		});
	};

	hideElement();
	bookingWidgetSwitch();
	initPackagesSlider();
};
