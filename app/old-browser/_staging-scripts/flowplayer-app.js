/**
 * @name SIA
 * @description Define global flowPlayerApp functions
 * @version 1.0
 */
SIA.flowPlayerApp = function(){
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var flyingFocus = $();
	var dataVideo = globalJson.dataVideo;

	// if(dataVideo){
	// 	dataVideo = {
	// 		url: {
	// 			webm: '//stream.flowplayer.org/night7.webm',
	// 			mp4: '//stream.flowplayer.org/night7.mp4',
	// 			m3u8: '//stream.flowplayer.org/night7.m3u8'
	// 		},
	// 		image: [{
	// 			url: 'images/img-1.jpg',
	// 			description: 'Lorem ipsum dolor sit dipiscing elit.'
	// 		},{
	// 			url:'images/img-2.jpg',
	// 			description: 'Curabitur congue tortor vitae era.'
	// 		}],
	// 		playlist: [
	// 			{
	// 				sources: [
	// 					{
	// 						type: 'video/webm',
	// 						src: '//stream.flowplayer.org/night7.webm'
	// 					},
	// 					{
	// 						type: 'video/mp4',
	// 						src: '//stream.flowplayer.org/night7.mp4'
	// 					},
	// 					{
	// 						type: 'video/flash',
	// 						src: 'mp4:night7'
	// 					}
	// 				]
	// 			},
	// 			{
	// 				sources: [
	// 					{
	// 						type: 'video/webm',
	// 						src:'//stream.flowplayer.org/night5.webm'
	// 					},
	// 					{
	// 						type: 'video/mp4',
	// 						src: '//stream.flowplayer.org/night5.mp4'
	// 					},
	// 					{
	// 						type: 'video/flash',
	// 						src: 'mp4:night5'
	// 					}
	// 				]
	// 			}
	// 		]
	// 	};
	// }

	// var allVideos = [
	// 	{
	// 		sources: [
	// 			{
	// 				type: 'video/webm',
	// 				src: '//stream.flowplayer.org/night7.webm'
	// 			},
	// 			{
	// 				type: 'video/mp4',
	// 				src: '//stream.flowplayer.org/night7.mp4'
	// 			},
	// 			{
	// 				type: 'video/flash',
	// 				src: 'mp4:night7'
	// 			}
	// 		]
	// 	},
	// 	{
	// 		sources: [
	// 			{
	// 				type: 'video/webm',
	// 				src:'//stream.flowplayer.org/night5.webm'
	// 			},
	// 			{
	// 				type: 'video/mp4',
	// 				src: '//stream.flowplayer.org/night5.mp4'
	// 			},
	// 			{
	// 				type: 'video/flash',
	// 				src: 'mp4:night5'
	// 			}
	// 		]
	// 	},
	// ];

	var initFlowPlayer = function(){
		var player;
		var url;

		var applyFlowPlayer = function(temp){
			var videoCollect = $('[data-flow-url]');
			var videoLightbox = $(temp).appendTo(body);
			var thumbnail = videoLightbox.find('.info-watch');

			videoLightbox.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',
				afterShow: function(){
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
					initFlowPlayer();
				},
				afterHide: function(popup){
					player.close();
					player = null;
					closeYoutubeVideo($(popup).find('.wrap-video'));
				}
			});

			var closeYoutubeVideo = function(videoBlock, callback){
				videoBlock.html('<div id="flowplayer"></div>');
				if(callback){
					callback();
				}
			};

			var initFlowPlayer = function(){
				player = window.$f('flowplayer', {
						src: 'http://releases.flowplayer.org/swf/flowplayer-3.2.18.swf',
						// version: [10, 1],
						width: 640,
						height: 390
					},
					{
						clip: {
							url: dataVideo.old.playlist[url].url,
							// use baseUrl so we can play with shorter file names
							// baseUrl: 'http://stream.flowplayer.org',
							// use first frame of the clip as a splash screen
							// autoPlay: false,
							autoBuffering: true
						}
					}
				);
			};

			thumbnail.each(function(idx){
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e){
					e.preventDefault();
					if(player){
						window.$f().play(dataVideo.old.playlist[idx]);
					}
				});
			});

			videoCollect.each(function(){
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e){
					e.preventDefault();
					url = self.data('flow-url');
					if(player){
						window.$f().resume().play(dataVideo.old.playlist[url]);
					}
					else{
						initFlowPlayer();
					}
					videoLightbox.Popup('show');
				});
			});
		};

		$.get(config.url.videoLightbox.flowplayer, function (temp) {
			var template = window._.template(temp, {
				'data': dataVideo
			});
			applyFlowPlayer(template);
		}, 'html');
	};

	var initModule = function(){
		initFlowPlayer();
	};
	initModule();
};
