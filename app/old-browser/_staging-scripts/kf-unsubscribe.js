/**
 * @name SIA
 * @description Define global kfUnsubscribe functions
 * @version 1.0
 */
SIA.kfUnsubscribe = function() {
	var controlOtherField = function() {
		var wrapRadioEl = $('[data-textarea-field]');
		wrapRadioEl.each(function() {
			var self = $(this),
					textareaOtherEl = $(self.data('textareaField')),
					wrapTextarea = textareaOtherEl.parent();

			self.find(':radio').off('change.showField').on('change.showField', function() {
				if($(this).data('showTextarea')) {
					wrapTextarea.removeClass('hidden');
				} else {
					var colEl = wrapTextarea.parents('.error');
					colEl.find('.text-error').remove();
					colEl.removeClass('error');
					wrapTextarea.addClass('hidden');
				}
			});
		});
	};

	var initModule = function() {
		controlOtherField();
	};

	initModule();
};
