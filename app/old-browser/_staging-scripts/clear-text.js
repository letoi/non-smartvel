// // Author: Stephen Korecky
// // Website: http://stephenkorecky.com
// // Plugin Website: http://github.com/skorecky/Add-Clear
// // Modify by K

(function($){
	$.fn.extend({
		addClear: function(options) {
			var selector = this.selector,
					htmlEl = $('html');

			if(options === 'hide') {
				$(this).siblings('.ico-cancel-thin').hide();
				return this;
			}

			var options =  $.extend({
				closeSymbol: "&#10006;",
				color: "#CCC",
				top: 1,
				right: 4,
				returnFocus: true,
				showOnLoad: false,
				onClear: null,
				hideOnBlur: true
			}, options);
			// the time is 400 that is used to fix issue not erase text when focusing on lumia 1020
			var nav = window.navigator.userAgent;
			var delayTime = ((nav.indexOf('Touch; NOKIA; 909') !== - 1) && (nav.indexOf('MSIE 10.0') !== - 1)) ? 400 : 200;

			var clearBlur = null,
				clearSelectText = null;

			$(this).each(function(){
				if($(this).data('no-clear-text')){
					return;
				}
				// var clearBlur = null,
				// 		clearSelectText = null;

				$(this).after('<a href="#clear" class="ico-cancel-thin add-clear-text" tabindex="-1" style="display: none;"><span class="ui-helper-hidden-accessible">clear</span></a>');

				if($(this).val().length >= ($(this).data('minLength') || 1) && options.showOnLoad === true) {
					$(this).siblings("a.add-clear-text").css('display', 'block');
				}

				$(this).off('focus.addClear').on('focus.addClear', function() {
					var self = $(this),
							that = this;

					clearTimeout(clearBlur);

					if(self.val().length >= ($(this).data('minLength') || 1) && (that.value !== self.attr('placeholder') || !htmlEl.hasClass('ie'))) {
						self.siblings("a.add-clear-text").css('display', 'block');
						if(htmlEl.is('.ie7')) {
							self.parent().children('.add-clear-text').css('display', 'block');
						}

						clearTimeout(clearSelectText);
						clearSelectText = setTimeout(function() {
							self.select();
							if(!htmlEl.hasClass('ie7') && !htmlEl.hasClass('ie8')) {
								that.setSelectionRange(0, that.value.length);
							}
						}, delayTime);

						self.off('mouseup.addClear').on('mouseup.addClear',function (e) {
							e.preventDefault();
							self.off('mouseup.addClear');
						});
					}
				});

				$(this).off('blur.addClear').on('blur.addClear', function() {
						var self = this,
								el = $(self);

						if (options.hideOnBlur) {
							clearBlur =	setTimeout(function () {
								el.siblings("a.add-clear-text").hide();
								el.off('mouseup.addClear');
								if(el.data('rule-required')){
									// if(self.defaultValue && (!self.value || self.value === el.attr('placeholder'))) {
									// 	el.val(self.defaultValue).closest('[data-autocomplete]').removeClass('default');
									// }

									if(typeof el.data('rule-required') !== 'boolean' && typeof el.data('rule-required') === 'string'){
										var rule = el.data('rule-required').split(':filled');
										if(rule.length > 1){
											$(rule[0]).closest('.grid-col').removeClass('error').find('p.text-error').remove();
											$(rule[0]).valid();
										}
									}
								}
							}, 200);
						}
				});

				$(this).off('keyup.addClear').on('keyup.addClear',function() {
					if($(this).val().length >= ($(this).data('minLength') || 1)) {
						$(this).siblings("a.add-clear-text").css('display', 'block');
					} else {
						$(this).siblings("a.add-clear-text").hide();
					}
				});

				$(this).siblings(".add-clear-text").off('click.addClear').on('click.addClear', function(){
					clearTimeout(clearBlur);
					$(this).siblings(selector).val("").trigger('change').trigger('blur');
					$(this).siblings(selector).off('mouseup.addClear');
					$(this).hide();
					if(options.returnFocus === true){
						$(this).siblings(selector).focus();
					}
					if (options.onClear){
						options.onClear($(this).siblings("input"));
					}
					return false;
				});
			});
			return this;
		}
	});
})(jQuery);
