/**
 * @name SIA
 * @description
 * @version 1.0
 */
SIA.DESCityGuide = function() {
	var global = SIA.global,
			win = global.vars.win,
			blockSlider = $('#where-to-stay-slider'),
			wrapperSlider = blockSlider.parent(),
			imgSliderLength = blockSlider.find('img').length - 1,
			totalDesktopSlide = 2,
			slideMarginRight = 20;

	var loadBackgroundSlider = function(self, parentSelf, idx) {
		if(idx === imgSliderLength) {
			blockSlider.width(wrapperSlider.width() + slideMarginRight);
			blockSlider.css('visibility', 'visible');
			blockSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: totalDesktopSlide,
					slidesToScroll: totalDesktopSlide,
					accessibility: false
				});

			win.off('resize.blockSlider').on('resize.blockSlider',function() {
				blockSlider.width(wrapperSlider.width() + slideMarginRight);
			}).trigger('resize.blockSlider');
		}
	};

	blockSlider.find('img').each(function(idx) {
		var self = $(this),
				parentSelf = self.parent(),
				newImg = new Image();

		newImg.onload = function() {
			loadBackgroundSlider(self, parentSelf, idx);
		};
		newImg.src = self.attr('src');
	});

	var focusElementForm = function() {
		var classCustomEl = $('[data-class]'),
			adultCustomEl = $('[data-adult]'),
			childCustomEl = $('[data-child]');

			$(classCustomEl).off('change.saveClassData').on('change.saveClassData',function(){
				var AdultInputEl = $(this).closest('form').find('[data-adult]'),
					timeOut = null;
				timeOut && clearTimeout(timeOut);
				if(AdultInputEl.length) {
				  timeOut = setTimeout(function(){
				    AdultInputEl.trigger('click');
				  }, 100);
				}
			});
			$(adultCustomEl).off('change.saveAdultData').on('change.saveAdultData',function(event, flag){
				var ChildInputEl = $(this).closest('form').find('[data-child]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(ChildInputEl.length) {
					  timeOut = setTimeout(function(){
					    ChildInputEl.trigger('click');
					  }, 100);
					}
			});
			$(childCustomEl).off('change.saveChildData').on('change.saveChildData',function(event, flag){
				var InfantInputEl = $(this).closest('form').find('[data-infant]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(InfantInputEl.length) {
					  timeOut = setTimeout(function(){
					    InfantInputEl.trigger('click');
					  }, 100);
					}
			});			
		};
	focusElementForm();
};
