/**
 * @name SIA
 * @description
 * @version 1.0
 */
SIA.countDownTimer = function() {
  (function(factory) {
    "use strict";
    if (typeof define === "function" && define.amd) {
      define(["jquery"], factory);
    } else {
      factory(jQuery);
    }
  })(function($) {
    "use strict";

    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if (!Array.prototype.forEach) {
      Array.prototype.forEach = function(callback/*, thisArg*/) {
        var T, k;
        if (this == null) {
          throw new TypeError('this is null or not defined');
        }
        // 1. Let O be the result of calling toObject() passing the
        // |this| value as the argument.
        var O = Object(this);
        // 2. Let lenValue be the result of calling the Get() internal
        // method of O with the argument "length".
        // 3. Let len be toUint32(lenValue).
        var len = O.length >>> 0;
        // 4. If isCallable(callback) is false, throw a TypeError exception.
        // See: http://es5.github.com/#x9.11
        if (typeof callback !== 'function') {
          throw new TypeError(callback + ' is not a function');
        }
        // 5. If thisArg was supplied, let T be thisArg; else let
        // T be undefined.
        if (arguments.length > 1) {
          T = arguments[1];
        }
        // 6. Let k be 0
        k = 0;
        // 7. Repeat, while k < len
        while (k < len) {
          var kValue;
          // a. Let Pk be ToString(k).
          //    This is implicit for LHS operands of the in operator
          // b. Let kPresent be the result of calling the HasProperty
          //    internal method of O with argument Pk.
          //    This step can be combined with c
          // c. If kPresent is true, then
          if (k in O) {
            // i. Let kValue be the result of calling the Get internal
            // method of O with argument Pk.
            kValue = O[k];
            // ii. Call the Call internal method of callback with T as
            // the this value and argument list containing kValue, k, and O.
            callback.call(T, kValue, k, O);
          }
          // d. Increase k by 1.
          k++;
        }
        // 8. return undefined
      };
    };

    
    var globalSale = $('.global-sale'),
        countdownWrapper = globalSale.find('.countdown'),
        countdownContent = globalSale.find('.countdown-content'),
        urlGetDataTime = countdownWrapper.data('current-datetime'),
        dataCurrentDate, currentDateTime,
        instances = [], matchers = [], defaultOptions = {
        precision: 1000,
        elapse: false,
        defer: false
    };

    var callAjax = function(){
      $.ajax({
        url: urlGetDataTime,
        type: SIA.global.config.ajaxMethod,
        dataType: 'json',
        async:false,
        success: function (reponse) {
          var serverTime = reponse.servertime;
          countdownWrapper.data('current-datetime', serverTime);
        },
        error: function(){
          // input error function
        },
      });
    };

    callAjax();
    dataCurrentDate = countdownWrapper.data('current-datetime');
    currentDateTime = new Date(dataCurrentDate).getTime();

    matchers.push(/^[0-9]*$/.source);
    matchers.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers = new RegExp(matchers.join("|"));
    function parseDateString(dateString) {
      if (dateString instanceof Date) {
        return dateString;
      }
      if (String(dateString).match(matchers)) {
        if (String(dateString).match(/^[0-9]*$/)) {
          dateString = Number(dateString);
        }
        if (String(dateString).match(/\-/)) {
          dateString = String(dateString).replace(/\-/g, "/");
        }
        return new Date(dateString);
      } else {
        throw new Error("Couldn't cast `" + dateString + "` to a date object.");
      }
    }
    var DIRECTIVE_KEY_MAP = {
      Y: "years",
      m: "months",
      n: "daysToMonth",
      d: "daysToWeek",
      w: "weeks",
      W: "weeksToMonth",
      H: "hours",
      M: "minutes",
      S: "seconds",
      D: "totalDays",
      I: "totalHours",
      N: "totalMinutes",
      T: "totalSeconds"
    };
    function escapedRegExp(str) {
      var sanitize = str.toString().replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
      return new RegExp(sanitize);
    }
    function strftime(offsetObject) {
      return function(format) {
        var directives = format.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
        if (directives) {
          for (var i = 0, len = directives.length; i < len; ++i) {
            var directive = directives[i].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/), regexp = escapedRegExp(directive[0]), modifier = directive[1] || "", plural = directive[3] || "", value = null;
            directive = directive[2];
            if (DIRECTIVE_KEY_MAP.hasOwnProperty(directive)) {
              value = DIRECTIVE_KEY_MAP[directive];
              value = Number(offsetObject[value]);
            }
            if (value !== null) {
              if (modifier === "!") {
                value = pluralize(plural, value);
              }
              if (modifier === "") {
                if (value < 10) {
                  value = "0" + value.toString();
                }
              }
              format = format.replace(regexp, value.toString());
            }
          }
        }
        format = format.replace(/%%/, "%");
        return format;
      };
    }
    function pluralize(format, count) {
      var plural = "s", singular = "";
      if (format) {
        format = format.replace(/(:|;|\s)/gi, "").split(/\,/);
        if (format.length === 1) {
          plural = format[0];
        } else {
          singular = format[0];
          plural = format[1];
        }
      }
      if (Math.abs(count) > 1) {
        return plural;
      } else {
        return singular;
      }
    }
    var Countdown = function(el, finalDate, options) {
      this.el = el;
      this.$el = $(el);
      this.interval = null;
      this.offset = {};
      this.options = $.extend({}, defaultOptions);
      this.instanceNumber = instances.length;
      instances.push(this);
      this.$el.data("countdown-instance", this.instanceNumber);
      if (options) {
        if (typeof options === "function") {
          this.$el.on("update.countdown", options);
          this.$el.on("stoped.countdown", options);
          this.$el.on("finish.countdown", options);
        } else {
          this.options = $.extend({}, defaultOptions, options);
        }
      }
      this.setFinalDate(finalDate);
      if (this.options.defer === false) {
        this.start();
      }
    };
    $.extend(Countdown.prototype, {
      start: function() {
        if (this.interval !== null) {
          clearInterval(this.interval);
        }
        var self = this;
        this.update(currentDateTime);
        this.interval = setInterval(function() {
          currentDateTime += 1000;
          self.update.call(self, currentDateTime);
        }, this.options.precision);
      },
      stop: function() {
        clearInterval(this.interval);
        this.interval = null;
        this.dispatchEvent("stoped");
      },
      toggle: function() {
        if (this.interval) {
          this.stop();
        } else {
          this.start();
        }
      },
      pause: function() {
        this.stop();
      },
      resume: function() {
        this.start();
      },
      remove: function() {
        this.stop.call(this);
        instances[this.instanceNumber] = null;
        delete this.$el.data().countdownInstance;
      },
      setFinalDate: function(value) {
        this.finalDate = parseDateString(value);
      },
      update: function(currentDateTime) {
        if (this.$el.closest("html").length === 0) {
          this.remove();
          return;
        }
        var hasEventsAttached = $._data(this.el, "events") !== undefined, now = new Date(currentDateTime), newTotalSecsLeft;

        newTotalSecsLeft = this.finalDate.getTime() - now.getTime();
        newTotalSecsLeft = Math.ceil(newTotalSecsLeft / 1e3);
        newTotalSecsLeft = !this.options.elapse && newTotalSecsLeft < 0 ? 0 : Math.abs(newTotalSecsLeft);
        if (this.totalSecsLeft === newTotalSecsLeft || !hasEventsAttached) {
          return;
        } else {
          this.totalSecsLeft = newTotalSecsLeft;
        }
        this.elapsed = now >= this.finalDate;
        this.offset = {
          seconds: this.totalSecsLeft % 60,
          minutes: Math.floor(this.totalSecsLeft / 60) % 60,
          hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
          days: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
          daysToWeek: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
          daysToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 % 30.4368),
          weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
          weeksToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7) % 4,
          months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30.4368),
          years: Math.abs(this.finalDate.getFullYear() - now.getFullYear()),
          totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
          totalHours: Math.floor(this.totalSecsLeft / 60 / 60),
          totalMinutes: Math.floor(this.totalSecsLeft / 60),
          totalSeconds: this.totalSecsLeft
        };

        if (!this.options.elapse && this.totalSecsLeft === 0) {
          this.stop();
          this.dispatchEvent("finish");
        } else {
          this.dispatchEvent("update");
        }
      },
      dispatchEvent: function(eventName) {
        var event = $.Event(eventName + ".countdown");
        event.finalDate = this.finalDate;
        event.elapsed = this.elapsed;
        event.offset = $.extend({}, this.offset);
        event.strftime = strftime(this.offset);
        this.$el.trigger(event);
      }
    });
    $.fn.countdown = function() {
      var argumentsArray = Array.prototype.slice.call(arguments, 0);
      return this.each(function() {
        var instanceNumber = $(this).data("countdown-instance");
        if (instanceNumber !== undefined) {
          var instance = instances[instanceNumber],
            method = argumentsArray[0];
          if (Countdown.prototype.hasOwnProperty(method)) {
            instance[method].apply(instance, argumentsArray.slice(1));
          } else if (String(method).match(/^[$A-Z_][0-9A-Z_$]*$/i) === null) {
            instance.setFinalDate.call(instance, method);
            instance.start();
          } else {
            $.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, method));
          }
        } else {
          new Countdown(this, argumentsArray[0], argumentsArray[1]);
        }
      });
    };


    $(function() {
      var labels = ['days', 'hours', 'minutes'],
          stringGreater = 's',
          template = _.template($('#countdown-template').html()),
          currDate = '00:00:00:00',
          nextDate = '00:00:00:00',
          parser = /([0-9]{2})/gi,
          dataPromoStart = countdownWrapper.data('promo-start'),
          dataPromoEnd = countdownWrapper.data('promo-end'),
          startDate = new Date(dataPromoStart),
          endDate = new Date(dataPromoEnd),
          isReadMessage = false,
          banner = $('.full-banner__text', globalSale),
          textComing = banner.data('text-coming'),
          textEnd = banner.data('text-end');

      // Parse countdown string to an object
      function strfobj(str) {
        var parsed = str.match(parser),
            obj = {};
        labels.forEach(function(label, i) {
          obj[label] = parsed[i]
        });
        return obj;
      }
      // Return the time components that diffs
      function diff(obj1, obj2) {
        var diff = [];
        labels.forEach(function(key) {
          if (obj1[key] !== obj2[key]) {
            diff.push(key);
          }
        });
        return diff;
      }
      // Build the layout
      var initData = strfobj(currDate);
      labels.forEach(function(label, i) {
        countdownContent.append(template({
          curr: initData[label],
          next: initData[label],
          label: label
        }));
      });
      /* Act on the event */
      function initEndData() {
        banner.html(textEnd);
        isReadMessage = false;
        if (!isReadMessage) {
          $('#countdown-timer-copy').text(textEnd);
          isReadMessage = true;
        }
        return false;
      }

      globalSale.addClass('global-sale-end');

      if (currentDateTime < endDate.getTime()) {
        countdownWrapper.countdown(endDate, function(event) {
          var data,
              newDate = event.strftime('%d:%H:%M:%S'),
              dayTimes = newDate.slice(0,2),
              hourTimes = newDate.slice(3,5),
              minTimes = newDate.slice(6,8),
              secondTimes = newDate.slice(9,11),
              dayTimesBlockFirst = dayTimes.slice(0,1),
              dayTimesBlockSecond = dayTimes.slice(1,2),
              hourTimesBlockFirst = hourTimes.slice(0,1),
              hourTimesBlockSecond = hourTimes.slice(1,2),
              minTimesBlockFirst = minTimes.slice(0,1),
              minTimesBlockSecond = minTimes.slice(1,2),
              timeDaysBlockFirst = $('.days').find('.dozens').find('.count'),
              timeDaysBlockSecond = $('.days').find('.unit').find('.count'),
              timeHourBlockFirst = $('.hours').find('.dozens').find('.count'),
              timeHourBlockSecond = $('.hours').find('.unit').find('.count'),
              timeMinBlockFirst = $('.minutes').find('.dozens').find('.count'),
              timeMinBlockSecond = $('.minutes').find('.unit').find('.count'),
              timeSecondblock = $('.seconds'),
              labelOneDay, labelOneHour, labelOneMin,
              labelDay = $('.days').find('.time-label'),
              labelHour = $('.hours').find('.time-label'),
              labelMin = $('.minutes').find('.time-label');

          if (newDate !== nextDate) {
            if (secondTimes !== '00') {
              if (minTimesBlockSecond < 9) {
                minTimesBlockSecond++;
              } else {
                minTimesBlockSecond = 0;
                if (minTimesBlockFirst < 5) {
                  minTimesBlockFirst++;
                } else {
                  minTimesBlockFirst = 0;
                  if (hourTimesBlockSecond < 3) {
                    hourTimesBlockSecond++;
                  } else {
                    hourTimesBlockSecond = 0;
                    if (hourTimesBlockFirst < 2) {
                      hourTimesBlockFirst++;
                    } else {
                      hourTimesBlockFirst = 0;
                      if (dayTimesBlockSecond < 9) {
                        dayTimesBlockSecond++;
                      } else {
                        dayTimesBlockSecond = 0;
                        dayTimesBlockFirst++;
                      }
                    }
                  }
                }
              }
            }
            newDate = dayTimesBlockFirst + dayTimesBlockSecond + ':' + hourTimesBlockFirst + hourTimesBlockSecond + ':'  + minTimesBlockFirst + minTimesBlockSecond + ':'+ secondTimes;
            currDate = nextDate;
            nextDate = newDate;
            // Setup the data
            data = {
              'curr': strfobj(currDate),
              'next': strfobj(nextDate)
            };
            // Apply the new values to each node that changed
            if (currDate !== '00:00:00:00') {
              diff(data.curr, data.next).forEach(function(label) {
                var selector = '.%s'.replace(/%s/, label),
                    $node = countdownWrapper.find(selector);
                // Update the node
                $node.removeClass('flip');
                $node.find('.current').text(data.curr[label]);
                $node.find('.next').text(data.next[label]);
                // Wait for a repaint to then flip
                _.delay(function($node) {
                  $node.addClass('flip');
                }, 50, $node);
              });
            }
          }

          timeDaysBlockFirst.text(dayTimesBlockFirst);
          timeDaysBlockSecond.text(dayTimesBlockSecond);
          timeHourBlockFirst.text(hourTimesBlockFirst);
          timeHourBlockSecond.text(hourTimesBlockSecond);
          timeMinBlockFirst.text(minTimesBlockFirst);
          timeMinBlockSecond.text(minTimesBlockSecond);

          if (currentDateTime < startDate.getTime()) {
            banner.html(textComing);
            if (!isReadMessage) {
              $('#countdown-timer-copy').text(textComing);
              isReadMessage = true;
            }
          }

          if (currentDateTime >= startDate.getTime()) {
            if (globalSale.hasClass('global-sale-end')) {
              var wcagTime = timeDaysBlockFirst.eq(0).text() + timeDaysBlockSecond.eq(0).text() + " days " + timeHourBlockFirst.eq(0).text() + timeHourBlockSecond.eq(0).text() + " Hours " + timeMinBlockFirst.eq(0).text() + timeMinBlockSecond.eq(0).text() + " minutes ";

              countdownContent.fadeIn();
              isReadMessage = false;
              globalSale.removeClass('global-sale-end');
              $(window).resize();

              if (!isReadMessage) {
                var regionSelector = $('#region-selector-copy'),
                    regionSelectorText = regionSelector.length ? wcagTime + ' ' + regionSelector.text() : wcagTime,
                    message = 'Promo will end in ' + regionSelectorText;
                $('#countdown-timer-copy').text(message);
                isReadMessage = true;
              }
            }
          }

          if(timeDaysBlockSecond.eq(0).text() <= '1' && timeDaysBlockFirst.eq(1).text() < "1"){
            labelOneDay = labelDay.text().slice(0, 3);
            labelDay.text(labelOneDay);
          }else{
            labelOneDay = labelDay.text().slice(0, 3);
            labelDay.text(labelOneDay + stringGreater);
          }
          if(timeHourBlockSecond.eq(0).text() <= '1' && timeHourBlockFirst.eq(1).text() < "1"){
            labelOneHour = labelHour.text().slice(0, 4);
            labelHour.text(labelOneHour);
          }else{
            labelOneHour = labelHour.text().slice(0, 4);
            labelHour.text(labelOneHour + stringGreater);
          }
          if(timeMinBlockSecond.eq(0).text() <= '1' && timeMinBlockFirst.eq(1).text() < "1"){
            labelOneMin = labelMin.text().slice(0, 6);
            labelMin.text(labelOneMin);
          }else{
            labelOneMin = labelMin.text().slice(0, 6);
            labelMin.text(labelOneMin + stringGreater);
          }
        })
        .on('finish.countdown', function(event) {
          globalSale.addClass('global-sale-end');
          countdownContent.fadeOut();
          initEndData();
        });
      } else {
        initEndData();
      }
    });
  });
};
