/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.flightSelect = function(){
    if(!$('.flight-select-page').length){
    	return;
    }
    var global = SIA.global;
    var config = global.config;
    var flightsSearch = $('div.flights__searchs');
    var titleColumn = 0;
    var posLeftNextBtn = 0,
    	posRightNextBtn = 0,
    	widthPackage3,
    	heightPackage,
    	win = $(window),
    	resizeTimeout = -1;
    var checked2Flights = false;

    function parseDate(date) {
      // const parsed = Date.parse(date);
      var parsed = Date.parse(date);
      if (!isNaN(parsed)) {
           return parsed;
      }
      return Date.parse(date.replace(/-/g, '/').replace(/[a-z]+/gi, ' '));
    }

    var popupActions = function(){
        // init after render html template
        var popupEditSearch = $('.popup--edit-search');
        var popupCompare = $('.popup--search-compare');
        var triggerEditSearch = $('.flights__target a.search-link');
        var triggerCompare = $('a.btn-compare');
        var flyingFocus = $('#flying-focus');

        var radioTab = function(wp, r, t){
            wp.each(function(){
                var radios = wp.find(r);
                var tabs = wp.find(t);
                radios.each(function(i){
                    var self = $(this);
                    self.off('change.selectTabs').on('change.selectTabs', function(){
                        tabs.removeClass('active').eq(i).addClass('active');
                    });
                });
            });
        };
        radioTab(popupEditSearch, '[data-search-flights]', '.widget-edit-search');

        if(!popupEditSearch.data('Popup')){
            popupEditSearch.Popup({
                overlayBGTemplate: config.template.overlay,
                modalShowClass: '',
                triggerCloseModal: '.popup__close, [data-close]',
                afterShow: function(){
                    flyingFocus = $('#flying-focus');
                    if(flyingFocus.length){
                        flyingFocus.remove();
                    }
                },
                closeViaOverlay: false,
                beforeHide: function() {
                    popupEditSearch.find('[data-customselect]').customSelect('hide');
                }
            });
        }

        triggerEditSearch.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
            e.preventDefault();
            popupEditSearch.Popup('show');
        });

        if(!popupCompare.data('Popup')){
            popupCompare.Popup({
                overlayBGTemplate: config.template.overlay,
                modalShowClass: '',
                triggerCloseModal: '.popup__close, [data-close]',
                afterShow: function(){
                    flyingFocus = $('#flying-focus');
                    if(flyingFocus.length){
                        flyingFocus.remove();
                    }
                }
            });
        }

        triggerCompare.off('click.triggerCompare').on('click.triggerCompare', function(e){
            e.preventDefault();
            popupCompare.Popup('show');
        });
    };

    var groupTooltip = function(){
        var groupTooltips = $('.form-group--tooltips');
        groupTooltips.each(function(){
            var self = $(this);
            var radioFilter = self.find('.custom-radio input[type="radio"]');
            var radioTooltips = self.find('.radio-tooltips');
            //var cityFrom = formTravel.find('#city-1');
            //var cityTo = formTravel.find('#city-2');

            radioFilter.each(function(index, el){
                $(el).off('change.showTooltip').on('change.showTooltip', function(){
                    radioTooltips.removeClass('active');
                    radioTooltips.eq(index).addClass('active');
                });
            });
        });
    };

    var run = function(){
        var previousBtn = $('a.wi-icon.wi-icon-previous');
        var nextBtn = $('a.wi-icon.wi-icon-next');
        var flightsTable = flightsSearch.find('table.flights__table');
        var flightsTableCib = flightsSearch.find('table.flights__table--cib');
        var titleHead = flightsTable.find('thead .title-head');
        var marginLeftPreBtn = Math.round(previousBtn.width()/2);
        var marginTopPreBtn = Math.round(previousBtn.height()/2);
        var marginTopNexBtn = Math.round(nextBtn.height()/2);
        var btnNext = $('#btn-next');
        var flyingFocus = $('#flying-focus');
        var buttonShowMore = $('[data-see-more]');
        var customSelectSort = $('.custom-select--sort');

        /** Package Carousel STARTS **/
        var packageToggle = function(){

        	var tableInner = $('.flights__table--1__inner');

        	flightsTableCib.each(function(){
        		var self = $(this);
        		var visible = self.data('column');
        		var packageTh = self.find('th[class*="package-"]');
        		// var packageTd = self.find('td[class*="package-"]');

        		titleColumn = self.data('column');

        		packageTh.slice(0, visible).show();
        		self.find('th[class*="package-"]:visible:last').addClass('border-image');


        	});

        	tableInner.each(function(){
        		var self = $(this);
        		var packageTds = self.find('td[class*="package-"]');
        		var visible = self.parents('table').data('column');

        		//Show only some packages starts from 0, to visible
        		packageTds.slice(0, visible).show();
        		self.find('td[class*="package-"]:visible:last').addClass('border-image');

        	});

        	nextBtn.each(function(){
        		$(this).off('click.showHidenFare').on('click.showHidenFare',function(e){
        			var btnSelf = $(this);
        			var targetTable = btnSelf.parents('.control').next();
        			var total = targetTable.find('th[class*="package-"]').length;
        			var packageTh = targetTable.find('th[class*="package-"]');
        			var packageTd = targetTable.find('td[class*="package-"]');
        			var visible = targetTable.data('column');
        			var groupBy = targetTable.data('column');
        			var temp = total;

        			targetTable.find('td[class*="package-"]').removeClass('border-image');
        			targetTable.find('th[class*="package-"]').removeClass('border-image');

        			e.preventDefault();

        			packageTh.hide();
        			packageTd.hide();

        			if( (temp-groupBy) <= visible ){

        				targetTable.removeClass('next-package');
        				targetTable.addClass('previous-package');

        				packageTh.slice((temp-visible), temp).show();
        				targetTable.find('.flights__table--1__inner').each(function(){
        					$(this).find('td[class*="package-"]').slice((temp-visible), temp).show();
        					groupBy += groupBy;

        					$(this).find('td[class*="package-"]:visible:first').addClass('border-image');
        					targetTable.find('th[class*="package-"]:visible:first').addClass('border-image');

        				});

        				btnSelf.next().show();
        				btnSelf.hide();
        				groupBy = visible;


        			}else{

        				packageTh.slice(groupBy, groupBy+visible).show();
        				targetTable.find('.flights__table--1__inner').each(function(){
        					$(this).find('td[class*="package-"]').slice(groupBy, groupBy+visible).show();
        					groupBy += groupBy;

        					$(this).find('td[class*="package-"]:visible:last').addClass('border-image');
        					targetTable.find('th[class*="package-"]:visible:last').addClass('border-image');

        				});



        			}


        			if( temp-groupBy === 0 ){
        				btnSelf.next().show();
        				btnSelf.hide();
        				// packageTd.removeClass('border-image');
        				// packageTh.removeClass('border-image');
        				// btnSelf.parent().next().find('td[class*="package-"]:visible:last').addClass('border-image');
        				// btnSelf.parent().next().find('th[class*="package-"]:visible:last').addClass('border-image');
        			}

        			btnSelf.next().css({'top':$('thead',flightsTable).outerHeight()/2});
        		});
        	});

        	previousBtn.each(function(){
        		$(this).off('click.showHidenFare').on('click.showHidenFare',function(e){
        			var btnSelf = $(this);
        			var targetTable = btnSelf.parents('.control').next();
        			var total = targetTable.find('th[class*="package-"]').length;
        			var packageTh = targetTable.find('th[class*="package-"]');
        			var packageTd = targetTable.find('td[class*="package-"]');
        			var visible = targetTable.data('column');
        			var temp = total;

        			targetTable.find('td[class*="package-"]').removeClass('border-image');
        			targetTable.find('th[class*="package-"]').removeClass('border-image');

        			e.preventDefault();

        			packageTh.hide();
        			packageTd.hide();
        			var firstSlice = temp-visible;
        			var secondSlice = firstSlice-visible;

        			if( firstSlice <= visible ){

        				targetTable.removeClass('previous-package');
        				targetTable.addClass('next-package');

        				secondSlice = 0;
        				firstSlice = visible;
        				// packageTh.slice(secondSlice, firstSlice).show();
        				// packageTd.slice(secondSlice, firstSlice).show();

        				packageTh.slice(secondSlice, firstSlice).show();
        				targetTable.find('.flights__table--1__inner').each(function(){
        					$(this).find('td[class*="package-"]').slice(secondSlice, firstSlice).show();
        					temp = total;

        					$(this).find('td[class*="package-"]:visible:last').addClass('border-image');
        					targetTable.find('th[class*="package-"]:visible:last').addClass('border-image');

        				});



        				btnSelf.prev().show();
        				btnSelf.hide();



        			}else{

        				// packageTh.slice(secondSlice, firstSlice).show();
        				// packageTd.slice(secondSlice, firstSlice).show();

        				packageTh.slice(secondSlice, firstSlice).show();
        				targetTable.find('.flights__table--1__inner').each(function(){
        					$(this).find('td[class*="package-"]').slice(secondSlice, firstSlice).show();
        					temp = firstSlice;
        				});

        			}

        			if( firstSlice === visible ){
        				btnSelf.prev().show();
        				btnSelf.hide();

        				targetTable.find('.flights__table--1__inner').each(function(){
        					$(this).find('td[class*="package-"]:visible:last').addClass('border-image');
        					targetTable.find('th[class*="package-"]:visible:last').addClass('border-image');
        				});

        			}

        			btnSelf.prev().css({'top':$('thead',flightsTable).outerHeight()/2});
        		});
        	});

        };

        /** Package Carousel ENDS **/

        var loopTableTitleHead = function(){
        	posLeftNextBtn = 0;
        	posRightNextBtn = 0;
        	widthPackage3 = 0;
        	heightPackage = 0;
        	titleHead.each(function( index ) {
        		if(index >= (titleColumn+1)) {
        			return;
        		}

        		if(flightsTable.data('dynamic')){
        			if(index<1){
        				posLeftNextBtn += $(this).outerWidth();
        				posRightNextBtn += $(this).outerWidth();
        			}else if(index<(titleColumn+1)){
        				widthPackage3 = $(this).outerWidth()/2;

        				if(titleColumn > 3){
        					posRightNextBtn += $(this).is(':hidden') ? 0 : ( $(this).outerWidth() - 3 );
        				}else{
        					posRightNextBtn += $(this).is(':hidden') ? 0 : $(this).outerWidth();
        				}

        			}
        		}else{
        			if(index<1){
        				posLeftNextBtn += $(this).outerWidth();
        				posRightNextBtn += $(this).outerWidth();
        			}else if(index<5){
        				widthPackage3 = $(this).outerWidth()/2;
        				posRightNextBtn += $(this).outerWidth();
        			}
        		}


        		if($(this).hasClass('package-3')){
        			heightPackage = $(this).outerHeight()/2;
        			posLeftNextBtn += 2;
        			// posRightNextBtn -= marginLeftPreBtn + 4;
        			// return false;
        		}
        	});
        	posRightNextBtn -= marginLeftPreBtn + 4;
        };

        /*  Switch active state between rows */
        flightsTable.on('change.select-flight', 'input[type="radio"]', function() {
        	var tableRow = $(this).parents('tr').last();

        	var waitlistedBlock = $(this).closest('.flights__table').find('.waitlisted');
        	var waitlistedBlockMessage = waitlistedBlock.find('.message-waitlisted');
        	if($(this).data('waitlisted')) {
        		waitlistedBlock.removeClass('hidden');
        		if(globalJson.bookingSummary && globalJson.bookingSummary.pwmMessage) {
        			var pwmMessage = '<em class="ico-checkbox"></em>';
        			pwmMessage += globalJson.bookingSummary.pwmMessage[0][$(this).val()];
        			waitlistedBlockMessage.html(pwmMessage);
        		}
        	}
        	else {
        		waitlistedBlock.addClass('hidden');
        	}

        	var classFlight = tableRow.find('.flights__info--group .class-flight');
        	if(!tableRow.is('.active')) {
        		tableRow.addClass('active').siblings('.active').removeClass('active');
        	}
        	var related = $(this).data('related');
        	tableRow.find('[data-related="' + related + '"]').not($(this)).prop('checked', true);
        	checked2Flights = flightsTable.filter(function() {
        		return $(this).find('input[type="radio"]:checked').length > 0;
        	}).length === flightsTable.length;
        	btnNext.prop('disabled', !checked2Flights).toggleClass('disabled', !checked2Flights);
            $(this).closest('.flights__table--select').find('.class-flight').text('');
        	classFlight.text($(this).data('flight-class'));
        });

        var initTableTooltip = function(table) {
        	table.find('[data-tooltip]').each(function(){
        		if(!$(this).data('tooltip-initialized')) {
        			if(!$(this).data('kTooltip')){
        				$(this).kTooltip({
        					afterShow: function(tt){
        						var tp = $('[data-trigger-popup]', tt);
        						tp.each(function(){
        							var self = $(this);
        							var popup = $(self.data('trigger-popup'));
        							if(!popup.data('Popup')){
        								popup.Popup({
        									overlayBGTemplate: config.template.overlay,
        									modalShowClass: '',
        									triggerCloseModal: '.popup__close, [data-close]',
        									afterShow: function(){
        										flyingFocus = $('#flying-focus');
        										if(flyingFocus.length){
        											flyingFocus.remove();
        										}
        									}
        								});
        							}
        							self.off('click.showPopup').on('click.showPopup', function(e){
        								e.preventDefault();
        								tt.hide();
        								popup.Popup('show');
        							});
        						});
        					}
        				});
        			}
        			$(this).data('tooltip-initialized', true);
        		}
        	});
        };

        initTableTooltip(flightsTable);

        buttonShowMore
        .off('click.show-more')
        .on('click.show-more', function() {
        	var idx = $(this).data('see-more');

        	var table = flightsSearch.filter('[data-flight="' + idx + '"]');
        	table.find('table.flights__table > tbody').children('.hidden:lt(6)').removeClass('hidden');
        	if(!table.children('.hidden').length) {
        		$(this).addClass('hidden');
        	}
        }).each(function() {
        	var idx = $(this).data('see-more');

        	var table = flightsSearch.filter('[data-flight="' + idx + '"]');
        	if(table.find('table.flights__table > tbody').children('.hidden').length === 0) {
        		$(this).addClass('hidden');
        	}
        });


        packageToggle();

        loopTableTitleHead();

        previousBtn.css({'left':posLeftNextBtn,'top':heightPackage,'margin-left':-marginLeftPreBtn+3,'margin-top':-marginTopPreBtn}).hide();

        nextBtn.css({'left':posRightNextBtn,'top':heightPackage,/*'margin-left':-marginLeftPreBtn-4,*/'margin-top':-marginTopNexBtn});

        previousBtn.css('visibility','visible');
        nextBtn.css('visibility','visible');

        function resizeHandler() {
        	clearTimeout(resizeTimeout);
        	resizeTimeout = setTimeout(resizeHandlerAction, 150);
        }

        function resizeHandlerAction() {
        	resizeSetBtnPrevNext();
        }

        function resizeSetBtnPrevNext() {
        	loopTableTitleHead();
        	previousBtn.css({'left':posLeftNextBtn,'top':heightPackage});
        	nextBtn.css({'left':posRightNextBtn,'top':heightPackage});
        }

        win.resize(resizeHandler);

        flightsTable.off('click.showInformation').on('click.showInformation', '.flights--detail span', function(){
        	var infFlightDetail = $(this).next();
        	var self = $(this);
        	if(infFlightDetail.is('.hidden')) {
        		infFlightDetail.removeClass('hidden').hide();
        	}

        	// self.children('em').toggleClass('ico-point-d ico-point-u');
        	self.toggleClass('active');

        	if(!infFlightDetail.is(':visible')) {
        		self.children('em').addClass('hidden');
        		self.children('.loading').removeClass('hidden');
        		$.ajax({
        			url: global.config.url.flightSearchFareFlightInfoJSON,
        			dataType: 'json',
        			type: global.config.ajaxMethod,
        			data: {
        				flightNumber: self.parent().data('flight-number'),
        				carrierCode: self.parent().data('carrier-code'),
        				date: self.parent().data('date'),
        				origin: self.parent().data('origin')
        			},
        			success: function(data) {
        				var flyingTime = '';
        				for(var ft in data.flyingTimes){
        					flyingTime = data.flyingTimes[ft];
        				}
        				var textAircraftType = L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType;
        				var textFlyingTime = L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime;
        				var rowAircraftType = $('<p>' + textAircraftType + '</p>');
        				var rowFlyingTime = $('<p>' + textFlyingTime + '</p>');
        				infFlightDetail.empty();
        				infFlightDetail.append(rowAircraftType, rowFlyingTime);
        				infFlightDetail.stop().slideToggle(400);
        			},
        			error: function(xhr, status) {
        				if(status !== 'abort'){
        					window.alert(L10n.flightSelect.errorGettingData);
        				}
        			},
        			complete: function() {
        				self.children('em').removeClass('hidden');
        				self.children('.loading').addClass('hidden');
        			}
        		});
        	}
        	else {
        		infFlightDetail.stop().slideToggle(400);
        	}
        });

        popupActions();
        groupTooltip();

        var sortData = function() {
        	var filter = $('[data-filter]');
        	filter.off('afterSelect.sort').on('afterSelect.sort', function() {
        		var self = $(this);
        		flightsSearch.each(function() {
        			var table = $(this).find('.flights__table');
        			var rows = table.find('> tbody > tr');
        			var visibleRowsNumber = rows.filter(':visible').length;
        			var sortBy = self.find('select').val();
        			switch(sortBy) {
        				case 'recommended':
        					rows.sort(function(a, b) {
        						if($(a).data('recommended') > $(b).data('recommended')) {
        							return 1;
        						}
        						if($(a).data('recommended') < $(b).data('recommended')) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'price-asc':
        					rows.sort(function(a, b) {
        						var priceA = window.accounting.unformat($(a).data('price'));
        						var priceB = window.accounting.unformat($(b).data('price'));
        						if(priceA > priceB) {
        							return 1;
        						}
        						else if (priceA < priceB) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'travel-asc':
        					rows.sort(function(a, b) {
        						if($(a).data('travel-time') > $(b).data('travel-time')) {
        							return 1;
        						}
        						else if($(a).data('travel-time') < $(b).data('travel-time')) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'departure-asc':
        					rows.sort(function(a, b) {
        						var departA = parseDate($(a).data('departure-time'));
        						var departB = parseDate($(b).data('departure-time'));
        						if(departA > departB) {
        							return 1;
        						}
        						else if(departA < departB) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'departure-desc':
        					rows.sort(function(a, b) {
        						var departA = parseDate($(a).data('departure-time'));
        						var departB = parseDate($(b).data('departure-time'));
        						if(departA < departB) {
        							return 1;
        						}
        						else if(departA > departB) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'arrival-asc':
        					rows.sort(function(a, b) {
        						var arrivalA = parseDate($(a).data('arrival-time'));
        						var arrivalB = parseDate($(b).data('arrival-time'));
        						if(arrivalA > arrivalB) {
        							return 1;
        						}
        						else if(arrivalA < arrivalB) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				case 'arrival-desc':
        					rows.sort(function(a, b) {
        						var arrivalA = parseDate($(a).data('arrival-time'));
        						var arrivalB = parseDate($(b).data('arrival-time'));
        						if(arrivalA < arrivalB) {
        							return 1;
        						}
        						else if(arrivalA > arrivalB) {
        							return -1;
        						}
        						else {
        							return 0;
        						}
        					});
        					break;
        				default:
        					break;
        			}
        			rows.removeClass('hidden even-item').each(function(rowIdx, row) {
        				if(rowIdx % 2 !== 0) {
        					$(row).addClass('even-item');
        				}
        				if(rowIdx > visibleRowsNumber - 1) {
        					$(row).addClass('hidden');
        				}
        			});
        			rows.detach().appendTo(table.children('tbody'));
        		});
        	});
        };
        sortData();

        var radioNavigation = function() {
        	if(/Apple/g.test(window.navigator.vendor)) {
        		return;
        	}

        	var keys = {
        		up: 38,
        		down: 40,
        		left: 37,
        		right: 39
        	};
        	$(document).off('keydown.radio-navigation').on('keydown.radio-navigation', function(e) {
        		var key = e.which || e.keyCode;
        		var focusRadio = $('input[type="radio"]:focus');
        		if(focusRadio.length) {
        			var name = focusRadio.attr('name');
        			var visibleRadio = focusRadio.closest('form').find('input[type="radio"][name="' + name + '"]:visible');
        			var currentIndexRadio = visibleRadio.index(focusRadio);
        			if(key === keys.up || key === keys.left) {
        				e.preventDefault();
        				if(currentIndexRadio > 0) {
        					visibleRadio.eq(currentIndexRadio - 1).prop('checked', true).focus().trigger('click.customRadio').trigger('click.toggleTooltip');
        				}
        			}
        			if(key === keys.right || key === keys.down) {
        				e.preventDefault();
        				if(currentIndexRadio < visibleRadio.length - 1) {
        					visibleRadio.eq(currentIndexRadio + 1).prop('checked', true).focus().trigger('click.customRadio').trigger('click.toggleTooltip');
        				}
        			}
        		}
        	});
        };

        radioNavigation();

        var hoverSelectSort = function() {
        	var arrow = customSelectSort.find('[class^="ico-point"]');
        	if($('html').is('.ie7')) {
        		if(arrow && arrow.length){
        			var selectText = $(this).prev('.select_text');
        			arrow
        				.off('mouseover.sort').on('mouseover.sort', function(e) {
        					e.stopPropagation();
        					selectText.addClass('hover');
        				})
        				.off('mouseout.sort').on('mouseout.sort', function(e) {
        					e.stopPropagation();
        					selectText.removeClass('hover');
        				});
        		}
        	}
        };

        hoverSelectSort();

    };

    var onFilterDataHandler = function (tableEle) {
      var filter = tableEle;
      var flightsTableSearch = tableEle.closest('.head-flight-info').find('.bgd-active-green-1');
      flightsTableSearch.each(function() {
        var table = $(this).find('.flights__table');
        var rows = table.find('> tbody > tr');
        var visibleRowsNumber = rows.filter(':visible').length;
        var sortBy = tableEle.find('select').val();
        switch(sortBy) {
          case 'recommended':
            rows.sort(function(a, b) {
              if($(a).data('recommended') > $(b).data('recommended')) {
                return 1;
              }
              if($(a).data('recommended') < $(b).data('recommended')) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'price-asc':
            rows.sort(function(a, b) {
              var priceA = window.accounting.unformat($(a).data('price'));
              var priceB = window.accounting.unformat($(b).data('price'));
              if(priceA > priceB) {
                return 1;
              }
              else if (priceA < priceB) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'travel-asc':
            rows.sort(function(a, b) {
              if($(a).data('travel-time') > $(b).data('travel-time')) {
                return 1;
              }
              else if($(a).data('travel-time') < $(b).data('travel-time')) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'departure-asc':
            rows.sort(function(a, b) {
              var departA = parseDate($(a).data('departure-time'));
              var departB = parseDate($(b).data('departure-time'));
              if(departA > departB) {
                return 1;
              }
              else if(departA < departB) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'departure-desc':
            rows.sort(function(a, b) {
              var departA = parseDate($(a).data('departure-time'));
              var departB = parseDate($(b).data('departure-time'));
              if(departA < departB) {
                return 1;
              }
              else if(departA > departB) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'arrival-asc':
            rows.sort(function(a, b) {
              var arrivalA = parseDate($(a).data('arrival-time'));
              var arrivalB = parseDate($(b).data('arrival-time'));
              if(arrivalA > arrivalB) {
                return 1;
              }
              else if(arrivalA < arrivalB) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          case 'arrival-desc':
            rows.sort(function(a, b) {
              var arrivalA = parseDate($(a).data('arrival-time'));
              var arrivalB = parseDate($(b).data('arrival-time'));
              if(arrivalA < arrivalB) {
                return 1;
              }
              else if(arrivalA > arrivalB) {
                return -1;
              }
              else {
                return 0;
              }
            });
            break;
          default:
            break;
        }
        rows.removeClass('hidden even-item').each(function(rowIdx, row) {
          if(rowIdx % 2 !== 0) {
            $(row).addClass('even-item');
          }
          if(rowIdx > visibleRowsNumber - 1) {
            $(row).addClass('hidden');
          }
        });
        rows.detach().appendTo(table.children('tbody'));
      });
    };

    var tableFlightSearch = function(){
        $.propHooks.disabled = {
          set: function (el, value) {
            if (el.disabled !== value) {
              el.disabled = value;
              value && $(el).trigger('disabledSet');
              !value && $(el).trigger('enabledSet');
            }
          }
        };
        var url = $('.form-flight-search').find('fieldset').data('table-flight');
        var templateTableFlight;
        var appendAfterDiv = $('.form-flight-search').find('.flights__searchs');
        appendAfterDiv.empty();

        var triggerRadio = function(radioEl) {
          radioEl.off('enabledSet').one('enabledSet', function() {
            radioEl.off('enabledSet').trigger('click');
          })
        };

        var CarDetailAddOnSales = function(data1){
          $.get(global.config.url.flightSearchInterim, function (data) {
            var template = window._.template(data, {
              data: data1
            });
            $('#booking-summary').attr('data-currency', data1.currency.code);
            templateTableFlight = $(template);
            appendAfterDiv.append(templateTableFlight);
            SIA.initCustomSelect();
            SIA.flightTableBorder();


            $('[data-price]').each(function(){
              var column = $(this).find('[data-radiogroup]');
              var _selfTr = $(this);
              var newArr = [];
              var min = 0;
              column.each(function(){
                newArr.push(parseInt($(this).find('.package--price-number').data('price-number')));
              });
              min  = Math.min.apply(Math,newArr);
              _selfTr.attr('data-price', min);
            });

            run();

            // default checked radio
            var defaultReId = data1.defaultRecommendationID;

            var defaultReArr = $.grep(data1.recommendations, function(recommendation){
              return recommendation.recommendationID === defaultReId;
            })[0];

            var arrSegmentCheck = [],
                fareCheck = {},
                fareIdx = 0;

            for(var i = 0; i < defaultReArr.segmentBounds.length; i++) {
              arrSegmentCheck.push(defaultReArr.segmentBounds[i].segments[0].segmentID);
            }

            fareCheck = defaultReArr.fareFamily;

            $.grep(data1.fareFamilies, function(fareFamily, idx){
              if(fareFamily.fareFamily === fareCheck) {
                fareIdx = idx;
              }
            });


            for(var j = 0; j < arrSegmentCheck.length; j++) {
              var radioEl = $('#flight-select-'+ j + '-' + arrSegmentCheck[j] + '-' + fareIdx);
              if(radioEl) {
                if (j === 0) {
                  radioEl.trigger('click');
                } else {
                  triggerRadio(radioEl);
                }
              }
            }

            $(document).on('change', '[data-table-filter]', function() {
                onFilterDataHandler($(this));
            });

          });
        };
        $.ajax({
          url: url,
          type: SIA.global.config.ajaxMethod,
          dataType: 'json',
          success: function(reponse) {
            var data2 = reponse.response;
            CarDetailAddOnSales(data2);
          }
        });
    };

    if($('body').is('.flight-select-interim-page')) {
        tableFlightSearch();
    } else {
        run();
    }

    $(document).on('click', '.flight-info', function() {
      var hiddenflightsInfo = $(this).next();
      if(!hiddenflightsInfo.hasClass('triggerClick')){
        hiddenflightsInfo.addClass('triggerClick');
        hiddenflightsInfo.slideDown(500);
        $(this).find('.ico-point-d').addClass('rotate-icon');
      }else{
        hiddenflightsInfo.removeClass('triggerClick');
        hiddenflightsInfo.slideUp(500);
        $(this).find('.ico-point-d').removeClass('rotate-icon');
      }
    });
    $(document).on("keydown",' .flights--detail-1', function(e){
    switch(e.keyCode) {
      case 13:
          $(this).find('.flight-info').trigger('click');
      break;
    }
});
};
