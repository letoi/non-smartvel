/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */

SIA.CheckinOverview = function() {
    var p = {};

    p.global = SIA.global;
    p.body = p.global.vars.body;

    p.summary = p.body.hasClass('checkin-summary');

    var init = function() {
        p.container = $("#container .main-inner");

        paintBody();
    };

    var paintBody = function() {
       // Prepare data for the modal windows
        formatData();

        if (globalJson.checkinData) {
            var checkinReviewTPL = p.global.config.url.checkedInOverviewTPL;
            $.ajax({
                url: checkinReviewTPL,
                context: document.body,

                success: function(data) {
                    addMainBody(data);
                }
            });
        }
    };

    var paintCancelCheckinModals = function() {
        // check if there is someone eligible for cancel checkin
        if (p.hasCheckin) {
            
            // TO DO //
            // this will control the cancel all checkin if we need to implement a logic to just show cancel all checkin button if you have more than 1 leg with checkedin pax
            p.checkedLeg = 0;

            // add the cancel all checkin modal
            addCancelCheckin(p.formattedData[0], true);

            // assign curLeg on the main json object
            p.formattedData[0].curLeg = 0;

            // loop through each leg to get if there it has checkedin pax
            _.each(p.formattedData, function(obj) {
              p.modalLeg = 0;

              _.each(obj.flights, function(leg){

                if(leg.hasCheckin) {
                  addCancelCheckin(obj, false);
                  p.checkedLeg++;
                }

              });

            });
        } else {
            // init booking summary module
            p.bookingSummary = new SIA.bookingSummary();
        }
    };

    var addCancelCheckin = function(data, cancelAll) {
        // adding cancel all checkin
        var tplUrl = cancelAll ? p.global.config.url.cancelAllCheckinTPL : p.global.config.url.cancelCheckinTPL;

        p.loadedCancelModal = 0;

        $.ajax({
          url: tplUrl,
          context: document.body,
          success: function(tplObj) {
            var tpl = _.template(tplObj);

            data.curLeg = p.modalLeg;

            var el = $(tpl(data));

            p.body.append(el);

            if(!cancelAll) {
                p.loadedCancelModal++;
                if(p.totalFlightWCheckin == p.loadedCancelModal) {
                    // init booking summary module
                    p.bookingSummary = new SIA.bookingSummary();
                }
                
            }

            if(!cancelAll) p.modalLeg++;

          }
        });
    };

    var addErrorModal = function(){
       // Load error message lightbox
        var errTpl = p.global.config.url.checkinError;
        $.ajax({
          url: errTpl,
          context: document.body,
          success: function(tplObj) {
            p.errorTpl = _.template(tplObj);

            var el = $(p.errorTpl());

            p.body.append(el);

            // start painting the boardingPass modal window
            paintBoardingPassModal();

          }
        });
    };

    var paintBoardingPassModal = function() {
        // loop through each leg to get if there it has checkedin pax
        var curLeg = 0;
        p.loadedBoardingPass = 0;

        _.each(p.formattedData, function(obj) {

          _.each(obj.flights, function(leg){

            if(leg.isEligibleForBP) {
                obj.curLeg = curLeg;
                // Load error message lightbox
                var ebpTpl = p.global.config.url.getBoardingPassTPL;
                $.ajax({
                  url: ebpTpl,
                  context: document.body,
                  success: function(tplObj) {
                    p.loadedBoardingPass++;

                    p.ebpTpl = _.template(tplObj);

                    var el = $(p.ebpTpl(obj));

                    p.body.append(el);

                    if(p.boardPassEligibleCount == p.loadedBoardingPass){
                        // start painting the modal windows for cancel checkin
                        paintCancelCheckinModals();
                    }
                  }
                });

            }

            curLeg++;

          });

        });
    };

    var addMealModal = function(){
       // Load error message lightbox
        var mealTpl = p.global.config.url.viewMealModal;
        $.ajax({
          url: mealTpl,
          context: document.body,
          success: function(tplObj) {
            p.mealTpl = _.template(tplObj);

            var el = $(p.mealTpl());

            p.body.append(el);

            // start painting the modal windows for cancel checkin and get boarding pass
            addErrorModal();

          }
        });
    };

    var addMainBody = function(data) {
        var tpl = _.template(data);
        _.each(p.formattedData, function(obj) {
            // add summary attribute for the checkin summary template
            var el = $(tpl(obj));
            p.container.append(el);
        });

        // Add the error modal for multiple leg with checkedin passengers
        addMealModal();
    };

    var formatData = function() {
        // check if data exist
        if (globalJson.checkinData) {
            // assign an array to hold the formatted data
            p.formattedData = [];
            p.totalFlightWCheckin = 0;
            p.boardPassEligibleCount = 0;

            // iterate on checkinData values
            // set a counter
            var i = 1;
            _.each(globalJson.checkinData, function(obj) {
                // assign a holder var
                var data = {};
                // push data to the formattedData rray
                p.formattedData.push(data);
                data.recordLocator = obj.paxRecordVO.recordLocator;

                // assign the counter i so that it counts through all paxRecordVO's inside checkinData array
                data.flying_info = i;

                // assign summary variable for the check in checkin complete
                data.checkinSummary = p.summary;

                // cache variables
                data.allPax = [];
                data.hasBoardingPassIssued = false;

                data.showCancelAllCheckin = false;
                data.showBoardingPassError = false;


                // add a global variable for an indication if atleast one person is checkedin
                p.hasCheckin = false;

                // get all flight IDs
                data.allFlightIDs = [];
                data.flights = obj.paxRecordVO.flights;

                var hastciArr = [];
                var hastci = false;
                _.each(data.flights, function(fl) {
                    // assign hasCheckin variable as false by default
                    fl.hasCheckin = false;
                    // assign boardingpass issued 
                    fl.hasBoardingPassIssued = false;
                    // assign eligible for boarding pass 
                    fl.isEligibleForBP = true;

                    if(typeof fl.tci != 'undefined') {
                        if(fl.rci && !hastci) {
                            hastci = true;
                        }
                    }

                    if(typeof fl.rci != 'undefined') {
                        if(fl.rci && !hastci) {
                            hastci = true;
                        }
                    }

                    hastciArr.push(hastci);

                    // get all flight id's per leg
                    _.each(fl.flightIDs, function(id) {
                        data.allFlightIDs.push(id);
                    });
                });

                if(data.flights.length > 1) {
                    var ntci = true;
                    for (var i = hastciArr.length - 1; i >= 0; i--) {

                        if(!hastciArr[i] && ntci) {
                            ntci = false;
                        }
                    }
                    
                    data.hastci = ntci;
                }else {
                    data.hastci = hastciArr[0];
                }

                // assign all pax
                _.each(obj.paxRecordVO.passengers, function(pax) {
                    var paxObj = {};
                    paxObj.paxDetails = pax;
                    paxObj.services = [];

                    data.allPax.push(paxObj);
                });

                // assign services to each pax
                _.each(obj.paxRecordVO.services, function(service) {
                    // after getting the current service loop through the flight ID's
                    // each flight id has a matching unique pax service

                    if (service.dcsStatus.checkedIn && !data.showCancelAllCheckin) {
                        data.showCancelAllCheckin = true;
                    }
                    if (service.dcsStatus.checkedIn && !p.hasCheckin) {
                        p.hasCheckin = true;
                    }
                    if (!service.dcsStatus.boardingPassIssued && service.dcsStatus.checkedIn && !data.showBoardingPassError) {
                        data.showBoardingPassError = true;
                    }

                    // match pax with the service and flight id
                    _.each(data.allFlightIDs, function(flID) {
                        if (service.flightID == flID) {
                            // loop through each pax obj in allPax array
                            _.each(data.allPax, function(pax) {
                                if (pax.paxDetails.passengerID == service.passengerID) {
                                    pax.services.push(service);
                                }
                            });
                        }
                    });

                    // find which leg has the current service with checkin
                    if (service.dcsStatus.checkedIn) {
                        _.each(data.flights, function(leg) {
                            _.each(leg.flightIDs, function(id) {
                                // check if current leg has boarding pass issued
                                if(service.flightID == id && !leg.hasBoardingPassIssued && service.dcsStatus.boardingPassIssued) {
                                  leg.hasBoardingPassIssued = true;
                                }
                                // check if current leg isEligibleForBP
                                if(service.flightID == id && leg.isEligibleForBP && !service.dcsStatus.eligibleForBP) {

                                  leg.isEligibleForBP = false;
                                }
                                // check if current leg has checkedin pax
                                if(service.flightID == id && !leg.hasCheckin) {
                                  leg.hasCheckin = true;

                                  // count total flight legs with checked in pax
                                  p.totalFlightWCheckin++;
                                }

                            });
                        });
                    }
                });

                // save the eligible for BP count for loading the boarding pass modal
                _.each(data.flights, function(flight) {
                    if(flight.isEligibleForBP) {
                        p.boardPassEligibleCount++;
                    }
                });

            });
        }
    };

    var toDate = function(epoch, format, locale) {
        var date = new Date(epoch),
        format = format || 'dd/mmm/YY',
        locale = locale || 'en',
        dow = {};

        dow.en = ['Sun','Mon','Tue','Wed','Thurs','Fri','Sat'];

        var monthNames = ["Jan","Feb", "Mar", "Apr", "May", "June ","July", "Aug", "Sept", "Oct", "Nov", "Dec"];

        var formatted = format
        .replace('D', dow[locale][date.getDay()])
        .replace('d', ("0" + date.getDate()).slice(-2))
        .replace('mmm', ((monthNames[date.getMonth()])))
        .replace('yyyy', date.getFullYear())
        .replace('yy', (''+date.getFullYear()).slice(-2))
        .replace('hh',  ("0" + date.getHours()).slice(-2))
        .replace('mn', date.getMinutes())

        return formatted
    };

    var oPublic = {
        init: init,
        p: p
    };
    return oPublic;
}();

// check if underscore has been loaded
var waitForUnderscore = function() {
    setTimeout(function() {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.CheckinOverview.init();
        }
    }, 100);
};

if (typeof _ == 'undefined') {
    waitForUnderscore();
}

/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
SIA.bookingSummary = function() {
    var global = SIA.global;
    var config = global.config;
    var dataDepart = {},
        dataReturn = {};
    // var isReSMS = false;
    // var win = global.vars.win;

    $.validator.addMethod('validateEmail', function(value) {
        if (!value.length) {
            return true;
        }
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return emailReg.test(value);
    }, L10n.validator.validateEmail);

    $.validator.addMethod('validateAtLeastOne', function(value, el, param) {
        var form = $(el).closest('form');
        var allField = form.find(param);
        var valid = false;
        allField.find('input:text, input[type="tel"]').each(function() {
            if ($.trim($(this).val()) !== '') {
                valid = true;
                return false;
            }
        });
        return valid;
    }, L10n.validator.validateAtLeastOne);


    if (!$('.sia-breadcrumb').length || $('.passenger-details-page').length) {
        return;
    }

    // init checkAllList function
    var booking = $('.booking');
    booking.each(function() {
        var bookingPassengerHeading = $('.booking-passenger-heading', this);
        var btn = $('.btn-1', this);
        bookingPassengerHeading.each(function(i) {
            var self = $(this);
            global.vars.checkAllList(self.find(':checkbox'), self.siblings('.booking-passenger-list'), function(con) {
                if (!i) {
                    if (con) {
                        btn.removeClass('disabled').prop('disabled', false);
                    } else {
                        btn.addClass('disabled').prop('disabled', true);
                    }
                }
            });
        });
    });

    // $('.booking-control a.btn-1').off('click.disabled').on('click.disabled', function(e){
    //  if($(this).hasClass('disabled')){
    //    e.preventDefault();
    //  }
    // });

    // Init accordion
    var initAccordion = function() {
        if ($('.main-inner .booking').length) {
            if (!$('.main-inner').data('accordion')) {
                $('.main-inner').accordion({
                    wrapper: '.booking .booking-item',
                    triggerAccordion: 'a.booking-passenger__control',
                    contentAccordion: 'div.booking-passenger-content',
                    activeClass: 'active',
                    duration: 600
                });
            }
        }
    };

    // Enable checkbox
    var enableCheckboxes = function(trigger, trigger2, nextFlight, passenger1, passenger2, openNextFlight) {
        var trigger2Length = trigger2.length,
            passenger2Length = passenger2.length,
            loopCheckbox = passenger2Length / trigger2Length;

        trigger.off('change.enableCheckboxes').on('change.enableCheckboxes', function() {
            if (trigger.is(':checked')) {
                nextFlight.removeClass('disabled');
                passenger2.filter(function() {
                    return !$(this).parent('.custom-checkbox--checked-in').length;
                }).prop({
                    'disabled': false
                }).parent().removeClass('disabled');
                passenger2.filter(function() {
                    return !$(this).parent('.custom-checkbox--checked-in').length;
                }).trigger('change.checkAllList');
                trigger2.prop('disabled', false).parent().removeClass('disabled');
                if (!openNextFlight.hasClass('active')) {
                    openNextFlight.trigger('click.accordion');
                }
            } else {
                nextFlight.addClass('disabled');
                passenger2.filter(function() {
                    return !$(this).parent('.custom-checkbox--checked-in').length;
                }).prop({
                    'disabled': true,
                    'checked': false
                }).parent().addClass('disabled').eq(0).closest('ul').prev().find(':checkbox').prop({
                    'checked': false
                });
                passenger2.filter(function() {
                    return !$(this).parent('.custom-checkbox--checked-in').length;
                }).trigger('change.checkAllList');
                trigger2.prop('disabled', true).parent().addClass('disabled');
            }
        });

        if (trigger.is(':checked')) {
            trigger.trigger('change.checkAllList').trigger('change.enableCheckboxes');
        }

        passenger1.each(function(idx) {
            var self = $(this);
            self.off('change.selectPassenger').on('change.selectPassenger', function() {
                if (self.is(':checked')) {
                    trigger2.prop('disabled', false).parent().removeClass('disabled');

                    passenger2.filter(function(index) {
                        return index % loopCheckbox === idx;
                    }).prop({
                        'disabled': false
                    }).parent().removeClass('disabled');

                    passenger2.filter(function(index) {
                        return index % loopCheckbox === idx;
                    }).trigger('change.checkAllList');


                    // if(!openNextFlight.hasClass('active')){
                    // openNextFlight.trigger('click.accordion');
                    // }
                    openNextFlight.each(function() {
                        var self = $(this);
                        if (!self.hasClass('active')) {
                            self.addClass('active').siblings('.booking-passenger-content').slideDown(400);
                        }
                    });


                    if (nextFlight.hasClass('disabled')) {
                        nextFlight.removeClass('disabled');
                    }
                } else {
                    passenger2.filter(function(index) {
                        return index % loopCheckbox === idx;
                    }).prop({
                        'disabled': true,
                        'checked': false
                    }).parent().addClass('disabled');

                    if (!passenger1.filter(function() {
                            return !$(this).parent('.custom-checkbox--checked-in').length;
                        }).is(':checked')) {
                        nextFlight.addClass('disabled');
                        trigger2.prop('disabled', true).parent().addClass('disabled');
                    }

                    passenger2.filter(function() {
                        return !$(this).parent('.custom-checkbox--checked-in').length;
                    }).trigger('change.checkAllList');
                }
            });
        });
    };

    var bookingCheckboxAllFirstFlight = $('#booking-checkbox-all-1');
    var bookingCheckboxAllOtherFlight = $('[data-checkbox-all]').not(bookingCheckboxAllFirstFlight);
    var wrapperFirstFlight = bookingCheckboxAllFirstFlight.closest('.booking-item');
    var nextFlight = wrapperFirstFlight.siblings('.booking-item');
    initAccordion();

    enableCheckboxes(
        bookingCheckboxAllFirstFlight,
        bookingCheckboxAllOtherFlight,
        nextFlight.children('.sub-heading-3--dark'),
        bookingCheckboxAllFirstFlight.closest('.booking-passenger-heading').next().find(':checkbox'),
        nextFlight.find(':checkbox').not(bookingCheckboxAllOtherFlight),
        nextFlight.find('a.booking-passenger__control')
    );

    var formTwo = $('#form-booking-2');
    formTwo.find(':checkbox').each(function() {
        if ($(this).is(':checked')) {
            formTwo.find('input[type="submit"]').removeClass('disabled').prop('disabled', false);
            formTwo.find('.booking-passenger__control').trigger('click.accordion');
            return;
        }
    });

    var flyingFocus = $('#flying-focus');

    // cancel popup
    var cancelCheckIn = $('.cancel-flight');
    var cancelAllFlight = $('.cancel-all-flight');
    var popupCancelFlight = $('.popup--checkin-cancel');
    var popupCancelFlightConfirm = $('.popup--checkin-cancel-confirm');
    var popupCancelAllFlight = $('.popup--checkin-cancel-all');
    var popupCancelAllFlightConfirm = $('.popup--checkin-cancel-all-confirm');
    var popupCheckinErrorMessage = $('.popup--checkin-error-message');

    // CuriousLab Start
    var cancelPopups = [];
    popupCancelFlight.each(function(i){
      var popup = $(this);
      popup.Popup({
          overlayBGTemplate: config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, .btn-back-booking',
          afterShow: function() {
              flyingFocus = $('#flying-focus');
              if (flyingFocus.length) {
                  flyingFocus.remove();
              }
          },
          closeViaOverlay: false
      }).find('#success-flight-submit').off('click.confirm').on('click.confirm', function(e) {
          e.preventDefault();
          popup.Popup('hide');
          // fix for Safari
          setTimeout(function() {
              popupCancelFlightConfirm.Popup('show');
          }, 200);
      });

      cancelPopups.push(popup);
    });

    cancelCheckIn.each(function(i) {
        var self = $(this);
        self.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
            e.preventDefault();
            if (!$(this).hasClass('disabled')) {
                if (self.hasClass('first-flight') && cancelPopups.length > 1) {
                    popupCheckinErrorMessage.Popup('show');
                } else {
                    cancelPopups[i].Popup('show');
                }
            }
        });
        self.closest('li').prev().find('a').off('click.changeSeat').on('click.changeSeat', function(e) {
            if ($(this).hasClass('disable')) {
                e.preventDefault();
            }
        });
    });

    var tciCheckBox = $('.has-tci');
    tciCheckBox.each(function(){
        var t = $(this);
        t.on({
            'change.tci': function(e){
                var id = $(this).attr('data-id');
                var checked = $(this).is(':checked');

                $('[data-id="'+id+'"]').prop('checked', checked);
            }
        })
    });

    var changeSeatLink = $('.change-seat');
    changeSeatLink.on({
        'click': function(e){
            if($(this).hasClass('disabled')) e.preventDefault();
        }
    })

    // CuriousLab End

    popupCheckinErrorMessage.Popup({
        overlayBGTemplate: config.template.overlay,
        modalShowClass: '',
        triggerCloseModal: '.popup__close, .btn-back-booking',
        afterShow: function() {
            flyingFocus = $('#flying-focus');
            if (flyingFocus.length) {
                flyingFocus.remove();
            }
        },
        closeViaOverlay: true
    });

    popupCancelAllFlight.Popup({
        overlayBGTemplate: config.template.overlay,
        modalShowClass: '',
        triggerCloseModal: '.popup__close, .btn-back-booking',
        afterShow: function() {
            flyingFocus = $('#flying-focus');
            if (flyingFocus.length) {
                flyingFocus.remove();
            }
        },
        closeViaOverlay: false
    }).find('#cancel-checkbox-cancel-submit').off('click.confirm').on('click.confirm', function(e) {
        e.preventDefault();
        popupCancelAllFlight.Popup('hide');
        // fix for Safari
        setTimeout(function() {
            popupCancelAllFlightConfirm.Popup('show');
        }, 200);
    });

    popupCancelFlightConfirm.Popup({
        overlayBGTemplate: config.template.overlay,
        modalShowClass: '',
        triggerCloseModal: '.popup__close, .btn-back-booking',
        afterShow: function() {
            flyingFocus = $('#flying-focus');
            if (flyingFocus.length) {
                flyingFocus.remove();
            }
        },
        closeViaOverlay: false
    });

    popupCancelAllFlightConfirm.Popup({
        overlayBGTemplate: config.template.overlay,
        modalShowClass: '',
        triggerCloseModal: '.popup__close, .btn-back-booking',
        afterShow: function() {
            flyingFocus = $('#flying-focus');
            if (flyingFocus.length) {
                flyingFocus.remove();
            }
        },
        closeViaOverlay: false
    });

    // cancelCheckIn.each(function() {
    //     var self = $(this);
    //     self.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
    //         e.preventDefault();
    //         if (!$(this).hasClass('disabled')) {
    //             if (self.hasClass('first-flight')) {
    //                 popupCheckinErrorMessage.Popup('show');
    //             } else {
    //                 popupCancelFlight.Popup('show');
    //             }
    //         }
    //     });
    //     self.closest('li').prev().find('a').off('click.changeSeat').on('click.changeSeat', function(e) {
    //         if ($(this).hasClass('disable')) {
    //             e.preventDefault();
    //         }
    //     });
    // });

    cancelAllFlight.each(function() {
        var self = $(this);
        self.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
            e.preventDefault();
            if (self.hasClass('disabled')) {
                return;
            }
            if (self.hasClass('first-flight')) {
                popupCheckinErrorMessage.Popup('show');
            } else {
                popupCancelAllFlight.Popup('show');
            }
        });
    });

    popupCancelFlight.find('.table-default > .table-row--heading').each(function() {
        global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
    });

    popupCancelAllFlight.find('.table-default > .table-row--heading').each(function() {
        global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
    });

    // boarding-pass
    var boardingPassTrigger = $('.boarding-pass');
    var popupBoarding = $('.popup--boarding-3');
    var popupBoardingContent = popupBoarding.find('.popup__content > div:first');
    var popupBoardingEmail = popupBoarding.find('.boarding-1');
    var popupBoardingSMS = popupBoarding.find('.boarding-2');
    var boardingForm = popupBoardingContent.find('form');

    var show = function(popup) {
        popupBoarding.css({
            'overflow': 'hidden'
        });
        popup.css({
            'display': 'block'
        });
        popup.css({
            'position': 'absolute',
            'top': 0,
            'left': 0,
            'z-index': 1,
            'opacity': 0
        }).animate({
            'opacity': 1
        }, 300);
        popupBoardingContent.css('visibility', 'hidden').animate({
            'opacity': 0
        }, 200, function() {
            // popupBoarding.css({
            //  // 'height': height
            // });
        });
    };

    // set value for Boarding SMS
    var setValueForBoardingSMS = function() {
        var boardingSMS = popupBoarding.find('.tab-content.sms'),
            smsRow = boardingSMS.find('.table-row');
        var countryVal, areaVal, phoneVal;
        smsRow.each(function(idx, ele) {
            countryVal = $(ele).find('[data-country]').val();
            areaVal = $(ele).find('[data-area]').val();
            phoneVal = $(ele).find('[data-phone]').val();

            popupBoardingSMS.find('.table-row').eq(idx).find('[data-country] .select__text').text(countryVal);
            popupBoardingSMS.find('.table-row').eq(idx).find('[data-area] input').val(areaVal);
            popupBoardingSMS.find('.table-row').eq(idx).find('[data-phone] input').val(phoneVal);
        });
    };

    // set value for Boarding Email
    var setValueForBoardingEmail = function() {
        var boardingEmail = popupBoarding.find('.tab-content.email'),
            emailRow = boardingEmail.find('.table-row');
        var emailVal;
        emailRow.each(function(idx, ele) {
            emailVal = $(ele).find('[data-email]').val();
            popupBoardingEmail.find('.table-row').eq(idx).find('[data-email] input').val(emailVal);
        });
    };

    popupBoarding.Popup({
        overlayBGTemplate: config.template.overlay,
        modalShowClass: '',
        triggerCloseModal: '.popup__close, .btn-back-booking',
        afterShow: function() {
            flyingFocus = $('#flying-focus');
            if (flyingFocus.length) {
                flyingFocus.remove();
            }
        },
        closeViaOverlay: false
    }).find('#boarding-10-submit').off('click.confirm').on('click.confirm', function() {
        if (boardingForm.valid()) {
            var tab = $(this).closest('form').find('.tabs--1').find('.tab-item.active');
            if (tab.index()) {
                setValueForBoardingSMS();
                show(popupBoardingSMS);
            } else {
                setValueForBoardingEmail();
                show(popupBoardingEmail);
            }
            // e.preventDefault();
        }
    });

    boardingPassTrigger.each(function() {
        var self = $(this);
        self.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
            e.preventDefault();
            if (!self.hasClass('disable')) {
                popupBoardingContent.show();
                popupBoardingContent.css({
                    'opacity': 1,
                    'visibility': 'visible'
                });
                popupBoardingSMS.hide();
                popupBoardingEmail.hide();
                popupBoarding.Popup('show');
            } else {
                // popupCheckinErrorMessage.Popup('show');
            }
        });
    });

    var tableDefault = $('.popup--checkin-cancel-all .table-default');
    tableDefault.each(function() {
        $(this).find(':checkbox').each(function(colIndex) {
            $(this).off('change.checkProcedure').on('change.checkProcedure', function() {
                var checkState = $(this).prop('checked');
                checkboxBackwardEffect(tableDefault, colIndex, checkState);
            });
        });
    });

    var checkboxBackwardEffect = function(currentArea, currentCol, currentState) {
        currentArea.nextAll('div.table-default').each(function() {
            $(this).find(':checkbox').not('[disabled]').each(function(index) {
                if (currentCol === index) {
                    if (currentState) {
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                    $(this).trigger('change.checkAllList');
                }
            });
        });
    };

    // Validate
    var initValidateBoarding = function() {
        boardingForm.validate({
            focusInvalid: true,
            errorPlacement: global.vars.validateErrorPlacement,
            success: global.vars.validateSuccess,
            submitHandler: function() {
                return false;
            }
        });
    };

    initValidateBoarding();

    // Get data from JSON append to Flight info.
    var getFlightInfo = function() {
        var bookingInfoGroup = $('.booking-info-group');
        bookingInfoGroup.find('.flights--detail span').off('click.showInfo').on('click.showInfo', function() {
            var self = $(this);
            if (self.next('.details').is(':not(:visible)')) {
                self.children('em').addClass('hidden');
                self.children('.loading').removeClass('hidden');

                self.next('.details').hide().removeClass('hidden').stop().slideToggle(400);
                self.children('em').removeClass('hidden');
                self.children('.loading').addClass('hidden');

                self.toggleClass('active');

                // $.ajax({
                //     url: config.url.flightSearchFareFlightInfoJSON,
                //     type: config.ajaxMethod,
                //     dataType: 'json',
                //     data: {
                //         flightNumber: self.parent().data('flight-number'),
                //         carrierCode: self.parent().data('carrier-code'),
                //         date: self.parent().data('date'),
                //         origin: self.parent().data('origin')
                //     },
                //     success: function(res) {
                //         self.toggleClass('active');
                //         var html = '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
                //         for (var ft in res.flyingTimes) {
                //             html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
                //         }
                //         self.next('.details').html(html).hide().removeClass('hidden').stop().slideToggle(400);
                //     },
                //     error: function(jqXHR, textStatus) {
                //         // console.log(jqXHR);
                //         if (textStatus === 'timeout') {
                //             window.alert(L10n.flightSelect.timeoutGettingData);
                //         } else {
                //             window.alert(L10n.flightSelect.errorGettingData);
                //         }
                //     },
                //     complete: function() {
                //         self.children('em').removeClass('hidden');
                //         self.children('.loading').addClass('hidden');
                //     }
                // });
            } else {
                self.toggleClass('active');
                self.next('.details').stop().slideToggle(400);
            }
        });
    };

    getFlightInfo();

    var emailAndSMS = function() {
        var triggerEmailSMS = $('[data-trigger-email-sms]');
        var sendCheckinConfirmationPopup = $('.popup--send-checkin-confirmation');
        var sendCheckinForm = $('#send-checkin-confirmation');
        var popupEmailConfirm = sendCheckinConfirmationPopup.find('.popup--email-confirm');
        var PopupSuccessfullySent = sendCheckinConfirmationPopup.find('.popup--successfully-sent');
        var emailSMS = sendCheckinConfirmationPopup.find('.popup--email-sms');
        var tabEmailSMS = sendCheckinConfirmationPopup.find('.tabs--1');
        var addEmailAddress = sendCheckinConfirmationPopup.find('#email-address-submit-3');
        var formEmailConfirm = sendCheckinConfirmationPopup.find('.form--email-confirm');
        var formPhoneNumber = sendCheckinConfirmationPopup.find('.form--phone-number');

        // set value for email confirm Popup
        var setValueEmailConfirmPopup = function() {
            var arr = [];
            var renderData = function() {
                sendCheckinForm.find('.form--email-address input').each(function() {
                    var self = $(this);
                    if (self.val()) {
                        arr.push(self.val());
                    }
                });
            };

            renderData();

            formEmailConfirm.find('.table-default').empty();

            for (var i = 0; i < arr.length; i++) {
                $(config.template.addEmailConfirm.format(i + 1, arr[i])).appendTo(formEmailConfirm.find('.table-default'));
            }

        };

        // set value SMS success sent Popup
        var setValueSMSSuccessfullySentPopup = function() {
            var arr = [];
            var countryVal, areaVal, phoneVal;
            var renderData = function() {
                formPhoneNumber.find('.table-row').each(function(idx, el) {
                    countryVal = $(el).find('[data-country]').val();
                    areaVal = $(el).find('[data-area]').val();
                    phoneVal = $(el).find('[data-phone]').val();

                    if (countryVal) {
                        arr.push({
                            country: countryVal,
                            area: areaVal,
                            phone: phoneVal
                        });
                    }

                });
            };

            renderData();

            PopupSuccessfullySent.find('.table-default').empty();

            for (var i = 0; i < arr.length; i++) {
                $(config.template.AddSMSSuccessfullySent.format(i + 1, arr[i].country, arr[i].area, arr[i].phone)).appendTo(PopupSuccessfullySent.find('.table-default'));

            }
        };

        sendCheckinConfirmationPopup.Popup({
            overlayBGTemplate: config.template.overlay,
            modalShowClass: '',
            triggerCloseModal: '.popup__close, .btn-back-booking',
            afterShow: function() {
                flyingFocus = $('#flying-focus');
                if (flyingFocus.length) {
                    flyingFocus.remove();
                }
            },
            afterHide: function() {
                PopupSuccessfullySent.addClass('hidden');
                popupEmailConfirm.addClass('hidden');
                emailSMS.removeClass('hidden');
            },
            closeViaOverlay: false
        });

        // Validate form group
        var validateFormGroup = function(formGroup) {
            formGroup.each(function() {
                var self = $(this);
                var doValidate = function(els) {
                    var pass = true;
                    els.each(function() {
                        if (!pass) {
                            return;
                        }
                        pass = $(this).valid();
                        // fix for checkin- sms
                        if (els.closest('[data-validate-col]').length && pass) {
                            els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
                        }
                    });
                };
                self.off('click.triggerValidate').on('click.triggerValidate', function() {
                    formGroup.each(function() {
                        if ($(this).data('change')) {
                            doValidate($(this).find('input'));
                        }
                    });
                });

                self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function() {
                    formGroup.not(self).each(function() {
                        if ($(this).data('change')) {
                            doValidate($(this).find('input'));
                        }
                    });
                }).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function() {
                    self.data('change', true);
                });

                self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function() {
                    self.data('change', true);
                    $(this).valid();
                }).off('blur.passengerDetail').on('blur.passengerDetail', function() {
                    if ($(this).val()) {
                        self.data('change', true);
                    } else {
                        // fix for checkin- sms
                        if ($(this).closest('[data-validate-col]').length) {
                            doValidate($(this).closest('[data-validate-row]').find('input'));
                        }
                    }
                });
            });
        };

        // Validate email and SMS
        var initValidateEmailAndSMS = function() {
            sendCheckinForm.validate({
                focusInvalid: true,
                errorPlacement: global.vars.validateErrorPlacement,
                success: global.vars.validateSuccess,
                submitHandler: function() {
                    var currentTab = sendCheckinForm.find('.tabs--1').find('.tab-item.active');

                    SIA.preloader.show();
                    emailSMS.addClass('hidden');
                    if (currentTab.index()) {
                        setValueSMSSuccessfullySentPopup();

                        PopupSuccessfullySent.removeClass('hidden');
                    } else {
                        setValueEmailConfirmPopup();
                        popupEmailConfirm.removeClass('hidden');
                    }
                    setTimeout(function() {
                        sendCheckinConfirmationPopup.Popup('reset');
                        sendCheckinConfirmationPopup.Popup('reposition');
                        SIA.preloader.hide();
                    }, 100);

                    return false;
                },
                invalidHandler: function(form, validator) {
                        if (validator.numberOfInvalids()) {
                            var divScroll = sendCheckinForm.find('[data-scroll-content]'),
                                scrollTo = 0;
                            divScroll.scrollTop(scrollTo);
                            scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top;
                            divScroll.scrollTop(scrollTo);
                        }
                    }
                    // onfocusout: global.vars.validateOnfocusout
            });
            validateFormGroup(sendCheckinForm.find('[data-validate-row]'));
        };

        // start validate after blur for validateAtLeastOne
        var setRuleForValidate = function() {
            if (formPhoneNumber.find('[data-rule-validateatleastone]').length) {
                formPhoneNumber.find('[data-validate-row]').find('input').on('blur.blurValidate', function() {
                    var that = $(this);
                    var inputAtLeastOne = that.closest('[data-validate-row]').find('input');
                    inputAtLeastOne.not(that).valid();
                });
            }
        };

        setRuleForValidate();
        // end validate after blur for validateAtLeastOne


        initValidateEmailAndSMS();

        triggerEmailSMS.off('click.showEmailSMSPopup').on('click.showEmailSMSPopup', function(e) {
            e.preventDefault();
            sendCheckinConfirmationPopup.Popup('show');
        });

        // tab afterChange event
        tabEmailSMS.off('afterChange.handleBtnES').on('afterChange.handleBtnES', function() {
            var self = $(this);
            var form = self.closest('form');
            var currentTab = form.find('.tabs--1').find('.tab-item.active');

            if (currentTab.index()) {
                form.find('#email-address-submit-3').addClass('hidden');
            } else {
                form.find('#email-address-submit-3').removeClass('hidden');
            }
        });

        addEmailAddress.off('click.addEmailAddress').on('click.addEmailAddress', function(e) {
            e.preventDefault();
            var newRow = $(config.template.addEmail.format(($(this).closest('form').find('.form--email-address .table-row').length + 1), L10n.validation.email)).appendTo($(this).closest('form').find('.form--email-address .table-row').parent());
            if (!window.Modernizr.input.placeholder) {
                newRow.find('input').placeholder();
                newRow.find('input').addClear();
            }
        });
    };

    emailAndSMS();
    var parentDataMore, parentDataLess, serachLink,
        win = $(window),
        valueTopBlockHeading,
        appendBlockTpl = $('[data-booking-summary-panel]');

    var mpAddOnsDetail = function(data1, dataDepart, dataReturn) {
        if (!$('body').hasClass('fs-economy-page')) {
            $.get(global.config.url.bookingSummaryPanelTpl, function(data) {
                var template = window._.template(data, {
                    data: data1
                });
                var templateEl = $(template);
                appendBlockTpl.html(templateEl);
                if ($('.bsp-booking-summary').length) {
                    valueTopBlockHeading = $('.bsp-booking-summary').offset().top;
                }
                var heightBspBooking = $('.bsp-booking-summary').find('.bsp-booking-summary__content');
                heightBspBooking.css({
                    'max-height': heightBspBooking.outerHeight()
                });
                var selfBookingSummaryInfo = $('.total-fare--inner').find('.flights__info');
                selfBookingSummaryInfo
                    .after('<div id="booking-summary__info-copy" />')
                    .next().attr({
                        'aria-live': 'polite',
                        'role': 'status',
                        'class': 'says'
                    });
            });
        } else {
            $.get(global.config.url.fsBookingSummaryPanelTpl, function(data) {
                var template = window._.template(data, {
                    data: data1,
                    dataDepart: dataDepart,
                    dataReturn: dataReturn
                });
                var templateEl = $(template);
                appendBlockTpl.children().not('.bsp-animate').remove();
                appendBlockTpl.prepend(templateEl)
                if ($('.bsp-booking-summary').length) {
                    valueTopBlockHeading = $('.bsp-booking-summary').offset().top;
                }
                var heightBspBooking = $('.bsp-booking-summary').find('.bsp-booking-summary__content');
                heightBspBooking.css({
                    'max-height': heightBspBooking.outerHeight()
                });
                var selfBookingSummaryInfo = $('.total-fare--inner').find('.flights__info');
                selfBookingSummaryInfo
                    .after('<div id="booking-summary__info-copy" />')
                    .next().attr({
                        'aria-live': 'polite',
                        'role': 'status',
                        'class': 'says'
                    });

                var checkTravelParty = $('.number-passengers').data('travel-party');

                if (checkTravelParty) {
                    var str = '<div class="travel-party"><div class="travel-thumb"><span class="ico ico-preferred-group"></span></div><div class="travel-content"><div class="title">Your travel party enjoy complimentary advance seat selection, as you\'re travelling with a child</div></div></div>'

                    $('.travel-party').remove();
                    $('.main-inner').prepend($(str));
                }
            });




            $(document).off('click.scrollToInbound').on('click.scrollToInbound', 'input[name="select-inbound"]', function() {
                setTimeout(function() {
                    var inboundTableTop = $('.recommended-flight-block:eq(1)').offset().top;
                    $(window).scrollTop(inboundTableTop - 400);
                }, 500);
            })

            $(document).off('click.scrollToOutbound').on('click.scrollToOutbound', 'input[name="select-outbound"]', function() {
                setTimeout(function() {
                    var outboundTableTop = $('.recommended-flight-block:eq(0)').offset().top;
                    $(window).scrollTop(outboundTableTop - 300);
                }, 500);
            })

        }
    };

    var renderBSP = function(dataDepart, dataReturn) {
        var urlBspPanel;
        if ($('body').hasClass('mp-add-ons-six-city-page')) {
            urlBspPanel = 'ajax/BSP-Flow-With-Pack-Insurance-Hotel-Car-Baggage-PSS-six-city.json';
        } else {
            urlBspPanel = config.url.bookingSummaryPanelJson;
        }
        $.ajax({
            beforeSend: function() {
                $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
            },
            url: urlBspPanel,
            type: SIA.global.config.ajaxMethod,
            dataType: 'json',
            async: true,
            cache: false,
            xhr: function() {
                var xhr = $.ajaxSettings.xhr();
                xhr.onprogress = function(e) {
                    if (e.lengthComputable) {
                        $('[data-booking-summary-panel]').find('.bsp-animate').css({
                            width: (e.loaded / e.total) * 100 + '%'
                        });
                    }
                };
                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        $('[data-booking-summary-panel]').find('.bsp-animate').css({
                            width: (e.loaded / e.total) * 100 + '%'
                        });
                    }
                };
                return xhr;
            },
            success: function(reponse) {
                var data1 = reponse;
                var data2 = reponse.bookingSummary;
                (dataDepart || dataReturn) ? mpAddOnsDetail(data2, dataDepart, dataReturn): mpAddOnsDetail(data2);
                mpCostBreakdownDetail(data1);
                getContentFlight();
            },
            complete: function() {
                $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
                setTimeout(function() {
                    $('[data-booking-summary-panel]').find('.bsp-animate').css({
                        width: 0
                    });
                }, 100)
            }
        });
    }

    renderBSP();

    var mpCostBreakdownDetail = function(data1) {
        var appendBreakdownDetailTpl = $('.popup--flights-details').find('.popup__content');

        $.get(global.config.url.mpCostBreakdownTpl, function(data) {
            var template = window._.template(data, {
                data: data1
            });
            var templateEl = $(template);
            appendBreakdownDetailTpl.append(templateEl);
        });
    };

    var getContentFlight = function() {

        $(document).off('click.getFlightContent').on('click.getFlightContent', '.btn-price', function(e, isUpgrade) {
            var rowFlight = $(this).closest('.flight-list-item'),
                flightIdx = $(this).closest('.recommended-flight-block').data('returntrip'),
                listLeg = rowFlight.find('.recommended-table [data-wrap-flight] .flight-result-leg'),
                price = $(this).is('[data-price-segment-after]') ? parseFloat($(this).data('price-segment-after')) : parseFloat($(this).data('price-segment')),
                arr = [],
                headerClass = $(this).data('header-class'),
                totalTime,
                flightData = {};

            totalTime = rowFlight.data('timetotal');
            listLeg.each(function() {
                var self = $(this),
                    flightStation = self.find('.flight-station--inner');
                flightStation.each(function() {
                    var obj = {};

                    obj['originHour'] = $(this).find('[data-origin-hour]').data('origin-hour');
                    obj['originCountryname'] = $(this).find('[data-origin-countryname]').data('origin-countryname');
                    obj['originDate'] = $(this).find('[data-origin-date]').data('origin-date');
                    obj['originAirportname'] = $(this).find('[data-origin-airportname]').data('origin-airportname');
                    obj['originterminal'] = $(this).find('[data-origin-terminal]').data('origin-terminal');
                    obj['destinationHour'] = $(this).find('[data-destination-hour]').data('destination-hour');
                    obj['destinationCountryname'] = $(this).find('[data-destination-countryname]').data('destination-countryname');
                    obj['destinationDate'] = $(this).find('[data-destination-date]').data('destination-date');
                    obj['destinationAirportname'] = $(this).find('[data-destination-airportname]').data('destination-airportname');
                    obj['destinationTerminal'] = $(this).find('[data-destination-terminal]').data('destination-terminal');
                    obj['layover'] = $(this).next().data('layovertime');
                    obj['timeFlight'] = $(this).find('[data-timeflight]').data('timeflight');
                    obj['operationName'] = $(this).find('[data-operationname]').data('operationname');
                    obj['flightNumber'] = $(this).find('[data-flightnumber]').data('flightnumber');
                    obj['planeName'] = $(this).find('[data-planename]').data('planename');
                    obj['headerClass'] = headerClass;

                    arr.push(obj);
                })
            })

            flightData['totalTime'] = totalTime;
            flightData['data'] = arr;
            flightData['flightidx'] = flightIdx;
            flightData['price'] = price;

            flightIdx === 0 ? dataDepart = flightData : dataReturn = flightData;

            if ($(this).closest('.block-content-flight').siblings('.upsell:not(".hidden")').length) {
                var self = $(this);

                $(this).closest('[data-hidden-recommended]').find('input[name="btn-keep-selection"]')
                    .off('click.renderBSP')
                    .on('click.renderBSP', function() {
                        renderBSP(dataDepart, dataReturn);
                    })

                $(this).closest('[data-hidden-recommended]').find('input[name="btn-upgrade"]')
                    .off('click.renderBSP')
                    .on('click.renderBSP', function() {
                        self.closest('.col-select').next().find('.btn-price').trigger('click.getFlightContent', true);
                    })
            } else {
                renderBSP(dataDepart, dataReturn);
            }

            isUpgrade && renderBSP(dataDepart, dataReturn);
        })

        $(document).off('click.resetBSP').on('click.resetBSP', 'input[name="change-button"]', function() {
            var flightIdx = $(this).closest('.recommended-flight-block').data('returntrip');

            if (flightIdx === 0) {
                dataDepart = {};
                dataReturn = {};
            } else {
                dataReturn = {};
            }

            renderBSP(dataDepart, dataReturn);
        })
    }

    var clickMoreDetails = function() {
        parentDataMore.find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap').addClass('flight-result-leg-wrap-1');
        parentDataMore.find('.bsp-total-fare').next().addClass('hidden');
        parentDataMore.closest('.bsp-booking-summary__content').css('max-height', '1000px');
        parentDataMore.find('.bsp-booking-summary__content-detail').removeClass('hidden');
        parentDataMore.find('.bsp-total-fare').addClass('expand-bsp');
        parentDataMore.find('.bsp-booking-summary__content-detail').find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap-1').addClass('flight-result-leg-wrap');
    };
    var clickLessDetails = function() {
        parentDataLess.find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap').addClass('flight-result-leg-wrap-1');
        parentDataMore.find('.bsp-total-fare').next().next().addClass('hidden');
        parentDataMore.closest('.bsp-booking-summary__content').css('max-height', '200px');
        parentDataMore.find('.bsp-total-fare').next().removeClass('hidden');
        parentDataMore.find('.bsp-total-fare').removeClass('expand-bsp');
        parentDataMore.find('.bsp-total-fare').next().find('.bsp-flights__info--group').removeClass('flight-result-leg-wrap-1').addClass('flight-result-leg-wrap');
    };
    var setOverflow = function() {
        var infoBspFlights = $('.bsp-booking-summary__content-detail .bsp-flights__info--inner'),
            heightBspBooking = $('.bsp-booking-summary').find('.bsp-booking-summary__content'),
            needOverflowScroll = false;
        infoBspFlights.each(function() {
            if ($(this).height() > 387) {
                needOverflowScroll = true;
            }
        });
        if (needOverflowScroll) {
            heightBspBooking.css('overflow-y', 'scroll');
        }
    };
    $(document).on('click', '.more-detail[data-more-details]', function(e) {
        e.preventDefault();
        parentDataMore = $(this).closest('.bsp-booking-summary__content-control');
        clickMoreDetails();
        setOverflow();
    });
    $(document).on('click', '.less-detail[data-less-details]', function(e) {
        e.preventDefault();
        parentDataLess = $(this).closest('.bsp-booking-summary__content-control');
        clickLessDetails();
    });
    $(document).on('click', '.plus-more-detail > .link-4', function(e) {
        e.preventDefault();
        parentDataMore = $(this).closest('.bsp-booking-summary__content-control');
        clickMoreDetails();
        setOverflow();
    });
    var popup1 = $('.popup--flights-details'),
        popup2 = $('.popup--add-ons-baggage'),
        popup3 = $('.popup--add-ons-summary');
    popup4 = $('.popup--flight-addon-term-condition');
    popup5 = $('.flight-search-summary-conditions');
    $(document).on('click', '.trigger-popup-flights-details', function(e) {
        e.preventDefault();
        $(this).trigger('dblclick');
        popup1.Popup('show');
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('dblclick', '.trigger-popup-flights-details', function() {
        popup1.Popup('show');
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.trigger-popup-add-ons-baggage', function(e) {
        e.preventDefault();
        $(this).trigger('dblclick');
        if (!$('body').hasClass('fs-economy')) {
            popup3.Popup('show');
        } else {
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
            popup5.Popup('show');
        }
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('dblclick', '.trigger-popup-add-ons-baggage', function() {
        if (!$('body').hasClass('fs-economy')) {
            popup3.Popup('show');
        } else {
            popup5.Popup('show');
        }
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.trigger-popup-add-ons-summary', function(e) {
        e.preventDefault();
        $(this).trigger('dblclick');
        popup2.Popup('show');
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('dblclick', '.trigger-popup-add-ons-summary', function() {
        popup2.Popup('show');
        $('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.popup__close, .popup__close_2', function(event) {
        popup1.Popup('hide');
        popup2.Popup('hide');
        popup3.Popup('hide');
        popup4.Popup('hide');
        popup5.Popup('hide');
        event.preventDefault();
    });
    win.off('scroll').on('scroll', function() {
        var blockHeading = $('.bsp-booking-summary');
        if ($(this).scrollTop() >= valueTopBlockHeading) {
            blockHeading.css({
                'top': '0',
                'position': 'fixed',
                'right': '0',
                'left': '0',
                'z-index': '10'
            });
        } else {
            blockHeading.css('position', 'static');
        }
        if ($('.bsp-booking-summary__heading').length) {
            var elPos = $('.bsp-booking-summary__heading').offset().top;
            $('[data-stickywidget]').css('top', elPos - 4);
        }
    });

    var triggerThis, timer,
        nextThis, prevThis,
        blockContentFlight, parentThisTrigger,
        blockCheckSeleted, blockWaperContent,
        blockCheckSeletedItemSq,
        lengthSeletedItemSq, termsConditionsSelf,
        buttonSelect = $('[data-selected-button]'),
        triggerlinkAccordion = $('.addons-landing-content').find('a[data-accordion-trigger]');

    var triggerButtonSelect = function() {
        blockContentFlight = triggerThis.closest('.your-flight-item');
        blockCheckSeleted = blockContentFlight.find('[data-select-item]');
        triggerThis.addClass('hidden');
        triggerThis.attr('data-selected-button', false).removeClass('selected-button');
        nextThis.attr('data-selected-button', true).addClass('selected-button');
        triggerThis.addClass('hidden');
        nextThis.removeClass('hidden');
        prevThis.removeClass('hidden');
        if (blockContentFlight.find('[data-selected-button]').hasClass('selected-button')) {
            blockCheckSeleted.eq(0).attr('data-select-item', false).addClass('hidden');
            blockCheckSeleted.eq(0).next().attr('data-select-item', true).removeClass('hidden');
        } else {
            blockCheckSeleted.eq(0).attr('data-select-item', false).removeClass('hidden');
            blockCheckSeleted.eq(0).next().attr('data-select-item', true).addClass('hidden');
        }
        blockCheckSeletedItemSq = triggerThis.closest('.addons-landing-content');
        lengthSeletedItemSq = blockCheckSeletedItemSq.find('.selected-button');
        if (lengthSeletedItemSq.length > 0) {
            blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(lengthSeletedItemSq.length);
        } else if (lengthSeletedItemSq.length < 1) {
            blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(lengthSeletedItemSq.length);
        }
        if (nextThis.hasClass('selected-button')) {
            triggerThis.closest('.weight-flight-item').next('.preferred-flight--info').removeClass('hidden');
        } else {
            triggerThis.closest('.weight-flight-item').next('.preferred-flight--info').addClass('hidden');
        }
        var blockDataPss = triggerThis.closest('.data-pss-xbag');
        if (blockDataPss) {
            var blockDataPssItem = blockDataPss.find('[data-selected-button]');
            blockDataPssItem.removeClass('hidden').removeClass('selected-button-1');
            blockDataPssItem.next().addClass('hidden').removeClass('selected-button-1');
            triggerThis.addClass('hidden').removeClass('selected-button');
            triggerThis.next().removeClass('hidden').addClass('selected-button-1');
            var lengthSeletedItemSqPss = blockCheckSeletedItemSq.find('.selected-button-1');
            blockCheckSeletedItemSq.find('a > .bundle-selected > span').text('');
            blockCheckSeletedItemSq.find('a > .bundle-selected > span').text(lengthSeletedItemSqPss.length);
            if (lengthSeletedItemSqPss.length > 0) {
                blockCheckSeletedItemSq.find('a > .bundle-selected').addClass('hidden');
            }
        }
    };

    buttonSelect.off('click').on('click', function() {
        triggerThis = $(this);
        nextThis = triggerThis.next('[data-selected-button]');
        prevThis = triggerThis.prev('[data-selected-button]');
        if (nextThis.length) {
            triggerButtonSelect();
        } else if (prevThis.length) {
            triggerButtonSelect();
        }
    });

    var linkExpanded = $('[data-select-item]');
    var triggerExpanded = function() {
        if (parentThisTrigger.hasClass('active')) {
            parentThisTrigger.find('em').removeClass('hidden');
        } else {
            setTimeout(function() {
                parentThisTrigger.find('em').addClass('hidden');
            }, 300);
        }
        if (blockWaperContent.find('select').attr('data-dropdpown-selected') === 'true') {
            blockWaperContent.find('[data-select-item]').eq(0).addClass('hidden');
            blockWaperContent.find('[data-select-item]').eq(0).next().removeClass('hidden');
            blockWaperContent.find('select').attr('data-dropdpown-selected', false);
        } else if (blockWaperContent.find('select').attr('data-dropdpown-selected') === 'false') {
            blockWaperContent.find('[data-select-item]').eq(0).removeClass('hidden');
            blockWaperContent.find('[data-select-item]').eq(0).next().addClass('hidden');
            blockWaperContent.find('select').attr('data-dropdpown-selected', true);
        }
    };
    linkExpanded.off('click').on('click', function() {
        clearTimeout(timer);
        blockWaperContent = $(this).closest('.your-flight-item');
        parentThisTrigger = $(this).parent();
        $('.hotel-infor').find('.slider-hotel').addClass('slider-hotel-selected');
        timer = setTimeout(function() {
            triggerExpanded();
        }, 300);
    });
    triggerlinkAccordion.off('click').on('click', function() {
        var findItemSelected = $(this).closest('.addons-landing-content').find('.selected-button');
        if ($(this).hasClass('active')) {
            if (findItemSelected.length > 0) {
                $(this).closest('.addons-landing-content').find('a > .bundle-selected').removeClass('hidden');
            } else if (findItemSelected.length < 1) {
                $(this).closest('.addons-landing-content').find('a > .bundle-selected').addClass('hidden');
            }
        } else {
            $(this).closest('.addons-landing-content').find('a > .bundle-selected').addClass('hidden');
        }
    });

    var changeTotalfare = $('[data-total-fare]');
    changeTotalfare.off('click').on('click', function() {
        var valuetotalFare = $(this).closest('.select-price').find('.sgd-price').text();
        $('[data-booking-summary-panel]').addClass('bsp-loading');
        $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '.3');
        setTimeout(function() {
            $('[data-booking-summary-panel]').addClass('bsp-loaded');
        }, 100);
        $('.total-fare--inner').find('[data-more-details]').css('opacity', '0');
        triggerThis = $(this);
        nextThis = triggerThis.next('[data-selected-button]');
        prevThis = triggerThis.prev('[data-selected-button]');
        if (nextThis.length) {
            triggerButtonSelect();
        } else if (prevThis.length) {
            triggerButtonSelect();
        }
        setTimeout(function() {
            $('.bsp-booking-summary__content-control').find('.total-cost').find('.unit').text(valuetotalFare);
            $('[data-booking-summary-panel]').removeClass('bsp-loaded bsp-loading');
            $('[data-booking-summary-panel]').find('.bsp-booking-summary__content').css('opacity', '1');
            $('.total-fare--inner').find('[data-more-details]').css('opacity', '1');
            var selfBookingSummaryInfo = $('.total-fare--inner').find('.flights__info');
            selfBookingSummaryInfo.find('.says').remove();
            selfBookingSummaryInfo.prepend('<span class="says">Booking summary, Selected Fare:</span>');
            $('#booking-summary__info-copy').html('');
            $('#booking-summary__info-copy').attr('tabindex', '0');
            $('#booking-summary__info-copy').delay(300).html(selfBookingSummaryInfo.text().replace('Booking summary, ', ''));
        }, 2100);
    });

    var clickedPreferred = function() {
        var checkClickedPreferred = $('.check-preferred').find('[data-selected-button]');
        checkClickedPreferred.each(function() {
            if ($(this).hasClass('selected-button')) {
                $(this).trigger('dblclick');
                popup4.Popup('show');
                $('body').find('.fadeInOverlay').addClass('overlay');
                var termsConditionsFlight = $('.terms-conditions-flight').find('#accept-condition');
                termsConditionsFlight.removeAttr('checked');
                termsConditionsSelf = termsConditionsFlight;
                checkInputChecked();
            }
        });
    };

    $('.trigger-popup-flights-addon-term-condition').on('click', function(e) {
        e.preventDefault();
        clickedPreferred();
    });

    var checkInputChecked = function() {
        var buttonGroup = $('.popup--flight-addon-term-condition').find('.button-group-1');
        if (termsConditionsSelf.is(':checked')) {
            buttonGroup.find('#btn-proceed').attr('disabled', false);
            buttonGroup.find('#btn-proceed').removeClass('disabled');
        } else {
            buttonGroup.find('#btn-proceed').addClass('disabled');
            buttonGroup.find('#btn-proceed').attr('disabled', true);
        }
    }

    $('.trigger-popup-flights-addon-term-condition').on('click', function(e) {
        e.preventDefault();
        clickedPreferred();
    });

    var termsConditionsFlight = $('.terms-conditions-flight').find('#accept-condition');
    termsConditionsFlight.off('click').on('click', function() {
        termsConditionsSelf = $(this);
        checkInputChecked();
    });

    var renderBookingPayment = function() {
        var templateBookingPayment;
        var blockPaymentsSection = $('#main-inner').find('.mp-payments-append');
        var appendDiv = blockPaymentsSection.find('[data-accordion-append]');

        var mpBookingPayment = function(data1) {
            $.get(global.config.url.mpBookinngPaymentTpl, function(data) {
                var template = window._.template(data, {
                    data: data1
                });
                templateBookingPayment = $(template);
                appendDiv.append(templateBookingPayment);

                var dataRemoveLastAdd = $('[data-remove-last-add]');
                dataRemoveLastAdd.each(function() {
                    var thisText = $(this).text();
                    thisText = thisText.slice(0, thisText.lastIndexOf('+'));
                    $(this).text(thisText);
                });

                var dataRemoveLastComma = $('[data-remove-last-comma]');
                dataRemoveLastComma.each(function() {
                    var thisText = $(this).text();
                    thisText = thisText.slice(0, thisText.lastIndexOf(','));
                    $(this).text(thisText);
                });
            });
        };
        $.ajax({
            url: global.config.url.bookingSummaryPanelJson,
            type: SIA.global.config.ajaxMethod,
            dataType: 'json',
            success: function(reponse) {
                var data1 = reponse.bookingSummary;
                mpBookingPayment(data1);
            }
        });
    };
    if ($('body').hasClass('mp-payments')) {
        renderBookingPayment();
    }

    // Expand accordions for mp-payments
    $('.for-your-flight-block .your-flight-item').each(function() {
        var flightDetailsInner = $(this).find('.block-flight-details--inner');
        $(flightDetailsInner).find('[data-accordion-trigger]').addClass('active');
    });
    htmlAmenitiesAppend = '<ul class="list-amenities"><li><em class="ico-swim-36"></em><span class="text-icon">Swimming pool</span></li><li><em class="ico-2-lounge"></em><span class="text-icon">Lounge</span></li><li><em class="ico-cup"></em><span class="text-icon">Breakfast</span></li><li><em class="ico-2-hotel"></em><span class="text-icon">24 hour services</span></li><li><em class="ico-info-3"></em><span class="text-icon">Concierge</span></li><li><em class="ico-2-fitness"></em><span class="text-icon">Fitness centre</span></li><li class="spa"><em class="ico-2-spa"></em><span class="text-icon">Spa and wellness</span></li><li><em class="ico-wifi"></em><span class="text-icon">Wifi in public area</span></li><li><em class="ico-assistance"></em><span class="text-icon">Disability support</span></li></ul>';
    var hotelAmenities = $(document).find('.hotel-amenities');
    var listAmenities = hotelAmenities.find('.amenities-content');
    var blockHotelRoomDetails = $(document).find('.hotel-room--details');
    var dataRemoveRoom = blockHotelRoomDetails.find('[data-remove-room]');
    if ($('body').hasClass('mp-add-ons-page')) {
        listAmenities.empty();
        listAmenities.append(htmlAmenitiesAppend);
        dataRemoveRoom.attr('value', 'SELECTED').css('background-color', '#00266b');
    }
};