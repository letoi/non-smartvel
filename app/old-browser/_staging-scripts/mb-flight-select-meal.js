/**
 * @name SIA
 * @description Define global selectMeal functions
 * @version 1.0
 */
SIA.selectMeals = function(){
  var global = SIA.global,
      vars = global.vars,
      win = vars.win,
      config = global.config,
      body =$('body'),
      container = $('#container'),
      jsonPage = $('body').is('.select-meals-land-page-json'),
      currTarget = null,
      myarray = [];

  var isValidated = false;
  var navPaxs = $();
  var selectedPaxIdx = -1;

  var populateData = function() {
    // Render Meals template
    var renderMealTemplate = function(el, json, code, selectedMealVal, listAmount){
      var options = [];
      var category = el.closest('[data-meal-portion]').find('[data-select-category]').val();
      var isSelected = false;

      el.empty();

      if (category === L10n.selectMeal.ifMealOption) {
        options.push('<option selected value="' + L10n.selectMeal.ifMealOption +
        '">' + L10n.selectMeal.ifMealOption + '</option>');
        isSelected = true;
        // el.data('option-meal', L10n.selectMeal.ifMealOption);
      }

      if (el.closest('[data-meal-portion]').hasClass('operated-scoot')) {
        options.push('<option selected value="' + 'Select' + '"' + ' data-code="' + 'Select' + '">' + 'Select' + '</option>');
      }

      if (json && json.length && code && code.length) {
        for(var i = 0; i < json.length; i++){
          if (!isSelected && selectedMealVal && json[i] === selectedMealVal) {
            isSelected = true;
            options.push('<option selected value="' + json[i] + '"' +
              ' data-code="' + code[i] + '">' + json[i] + '</option>');
          }
          else {
            options.push('<option data-attr="'+ listAmount[i] +'" value="' + json[i] +'"' + ' data-code="' +
              code[i] + '">' + json[i] + '</option>');
          }
        }
      }

      if (!el.closest('[data-meal-portion]').hasClass('operated-scoot')) {
        if(options.length > 1) {
          options.push('<option class="select__footer" value="' + L10n.selectMeal.ifMealOption +
          '">' + L10n.selectMeal.ifMealOptionLabel + '</option>');
        }
        if(options.length === 1  && $(options[0]).val() !== L10n.selectMeal.ifMealOption ) {
          options.push('<option class="select__footer" value="' + L10n.selectMeal.ifMealOption +
          '">' + L10n.selectMeal.ifMealOptionLabel + '</option>');
        }
      }

      el.html(options.join(''));
      el.data('option-meal', el.val());

      if(el.closest('[data-customSelect]').data('customSelect')){
        el.closest('[data-customSelect]').customSelect('_createTemplate');
        el.closest('[data-customSelect]').customSelect('refresh');
      }
    };

    // Update value for selectbox Meals.
    var populateMealBlock = function(el, value, cValue , elScoot){
      var blocks,
      getDataId = el.attr('data-id'),
      itemIdPremium = $('.data-id-hidden-premium').find('li'),
      flightsCostPremium = $('.flights-cost__premium').find('.price'),
      subTotalPremium = $('.sub-total-premium').find('.price'),
      unitPriceInfo = $('.unit-price-info'),
      bookingNavItem = $('.booking-nav').find('.booking-nav__item'),
      passengerName = bookingNavItem.find('.passenger-info-hidden').text(),
      datainfoFlight = el.attr('data-info-flight'),
      getDataIdDetails = $('.breakdown-content-passenger').find('tbody').find('tr'),
      select__text = el.find('[data-option-meal]').closest('[data-customselect]').find('.select__text');
      if (bookingNavItem.hasClass('active')) {
        $('.breakdown-content-passenger').find('.passenger-name').text(passengerName);
      }

      if (elScoot == true) {
        blocks = el;
        if(cValue =="Premium Meal" && value !== 'Select') {
          blocks.addClass('has-premium');
          itemIdPremium.each(function(){
            myarray.push($(this).data('id'));
          });
          if(jQuery.inArray(getDataId, myarray) !== -1) {
            itemIdPremium.each(function(){
              if($(this).data('id') == getDataId) {
                $(this).data('price', parseFloat(select__text.attr('data-get-amount')));
              }
            });
            getDataIdDetails.each(function(){
              if($(this).data('id') == getDataId) {
                $(this).remove();
              }
            });
            $('.breakdown-content-passenger').find('tbody').append("<tr class='row-title' data-id='"+ getDataId +"'><td colspan='2'><span class='text-bold'>Premium selection</span></td></tr><tr class='row-content' data-id='"+ getDataId +"'><td>"+ datainfoFlight +"</td><td class='number-price'>"+ select__text.attr('data-get-amount') +"</td></tr>");
              var number = 0;
              var PriceRowContent = $('.row-content').find('.number-price');
              PriceRowContent.each(function() {
                number += parseInt($(this).html());
              });
              flightsCostPremium.text(number.toFixed(2));
              subTotalPremium.text(number.toFixed(2));
              unitPriceInfo.text(number.toFixed(2));
          } else {
            myarray.push(getDataId);
            $('.data-id-hidden-premium').append('<li data-price = "'+ parseFloat(select__text.data('get-amount')) + '" data-id="'+ getDataId +'"></li>');
            $('.breakdown-content-passenger').find('tbody').append("<tr class='row-title' data-id='"+ getDataId +"'><td colspan='2'><span class='text-bold'>Premium selection</span></td></tr><tr class='row-content' data-id='"+ getDataId +"'><td>"+ datainfoFlight +"</td><td class='number-price'>"+ select__text.attr('data-get-amount') +"</td></tr>");
              var number = 0;
              var PriceRowContent = $('.row-content').find('.number-price');
              PriceRowContent.each(function() {
                number += parseInt($(this).html());
              });
              flightsCostPremium.text(number.toFixed(2));
              subTotalPremium.text(number.toFixed(2));
              unitPriceInfo.text(number.toFixed(2));
          }
        } else {
          if (blocks.hasClass('has-premium')) {
            if(jQuery.inArray(getDataId, myarray) !== -1) {
              itemIdPremium.each(function(){
                if($(this).data('id') == getDataId) {
                  var itemtoRemove = getDataId;
                  myarray.splice($.inArray(itemtoRemove, myarray),1);
                  $(this).remove();
                }
              });
              getDataIdDetails.each(function(){
                if($(this).data('id') == getDataId) {
                  $(this).remove();
                }
              });
              var number = 0;
              var PriceRowContent = $('.row-content').find('.number-price');
              PriceRowContent.each(function() {
                number += parseInt($(this).html());
              });
              flightsCostPremium.text(number.toFixed(2));
              subTotalPremium.text(number.toFixed(2));
              unitPriceInfo.text(number.toFixed(2));
            }
            blocks.removeClass('has-premium');
          }
        }
        var operatedScoot = $('.accordion__wrapper--content').find('.operated-scoot');
        if ($(operatedScoot).hasClass('has-premium')) {
            $('.main-inner').find('.booking-summary').css({
              'opacity' : '1',
              'pointer-events': 'inherit'
            });
        } else {
            $('.main-inner').find('.booking-summary').css({
              'opacity' : '0',
              'pointer-events': 'none'
            });
        }
      } else {
        blocks = el.find('[data-meal-portion]').not('.operated-scoot');
      }
      blocks.each(function(){
        var category = $('[data-select-category]', this);
        var meal = $('[data-option-meal]', this);
        if(category.children(':selected').data('populate')) {
          category
            .val(cValue)
            .closest('[data-customSelect]')
            .customSelect('refresh');
          category.data('old-select-category', category.data('select-category'));
          category.data('select-category', category.val());
          meal
            .val(value)
            .data('option-meal', value)
            .closest('[data-customSelect]')
            .addClass('alert-selectbox')
            .customSelect('refresh');
        }
      });
    };

    // Update value for selectbox Category.
    var populateCategoryBlock = function(obj){
      var wrapper = obj.ml.closest('[data-meal-portion]');

      if (wrapper.hasClass('operated-scoot')) {
        var mealList = wrapper.closest('.accordion__content-info').find('.meal-list');
        mealList.find('li > strong').text(obj.cVal);
      }

      var oldMeal = obj.ml.data('option-meal');

      renderMealTemplate(obj.ml, obj.param[0], obj.param[1], obj.param[2], obj.param[3]);
      obj.chks.hide();
      if(obj.ctg.children(':selected').data('populate') && obj.chks.length > 0){
        if ($(obj.ctgs).find('option:selected[data-code="BC"]').length >= 2) {
          return;
        }
        // if (obj.ctgs.length && $(obj.ctgs[0]).find('option:selected').data('code') === 'BC') {
        //   return;
        // }
        if(!wrapper.hasClass('operated-scoot')){
          wrapper
            .next()
            .show()
            .find('span').text(obj.wp.data('alertTemplate').format(obj.ml.val(), oldMeal));
        } else {
          // if(obj.cVal !== "Premium Meal") {
            var getDataId = wrapper.attr('data-id'),
            itemIdPremium = $('.data-id-hidden-premium').find('li'),
            flightsCostPremium = $('.flights-cost__premium').find('.price'),
            subTotalPremium = $('.sub-total-premium').find('.price'),
            unitPriceInfo = $('.unit-price-info'),
            getDataIdDetails = $('.breakdown-content-passenger').find('tbody').find('tr'),
            select__text = wrapper.find('[data-option-meal]').closest('[data-customselect]').find('.select__text');
            if(jQuery.inArray(getDataId, myarray) !== -1) {
              itemIdPremium.each(function(){
                if($(this).data('id') == getDataId) {
                  var itemtoRemove = getDataId;
                  myarray.splice($.inArray(itemtoRemove, myarray),1);
                  $(this).remove();
                }
              });
              getDataIdDetails.each(function(){
                if($(this).data('id') == getDataId) {
                  $(this).remove();
                }
              });
              var number = 0;
              var PriceRowContent = $('.row-content').find('.number-price');
              PriceRowContent.each(function() {
                number += parseInt($(this).html());
              });
              flightsCostPremium.text(number.toFixed(2));
              subTotalPremium.text(number.toFixed(2));
              unitPriceInfo.text(number.toFixed(2));
            }
            wrapper.removeClass('has-premium');
          // }
          var operatedScoot = $('.accordion__wrapper--content').find('.operated-scoot');
          if ($(operatedScoot).hasClass('has-premium')) {
              $('.main-inner').find('.booking-summary').css({
                'opacity' : '1',
                'pointer-events': 'inherit'
              });
          } else {
              $('.main-inner').find('.booking-summary').css({
                'opacity' : '0',
                'pointer-events': 'none'
              });
          }
        }
      }
      obj.ctg.data('old-select-category', obj.ctg.data('select-category'));
      obj.ctg.data('select-category', obj.ctg.val());
      wrapper.next().data('option-meal', oldMeal);

      obj.ctgs.each(function(){
        var that = $(this);
        var category = $('[data-select-category]', that);
        var meal = $('[data-option-meal]', that);
        if(category.children(':selected').data('populate') && obj.ctg.children(':selected').data('populate')) {
          category
            .val(obj.cVal)
            .closest('[data-customSelect]')
            .customSelect('refresh');

          category.data('old-select-category', category.data('select-category'));
          category.data('select-category', category.val());
          that.next().data('option-meal', meal.data('option-meal'));
          renderMealTemplate(meal, obj.param[0], obj.param[1], obj.param[2], obj.param[3]);

          meal
            .val(obj.ml.val())
            .data('option-meal', obj.ml.val())
            .closest('[data-customSelect]')
            .addClass('alert-selectbox')
            .customSelect('refresh');
        }
      });
    };

    var intergrateData = function(el, alert, wp){
      var category = $('[data-select-category]', el);
      var meal = $('[data-option-meal]', el);
      var customSelectMeal = meal.closest('[data-customselect]');
      var checkinAlert = el.closest('.select-meal-item').find('.checkin-alert');

      var undoAction = function(wp){
        wp.each(function(){
          var self = $(this).parents();
          var selfOrigin = $(this);
          var wpCategory = selfOrigin.find('[data-select-category]');
          var wpMeal = selfOrigin.find('[data-option-meal]');

          if(wpCategory.length && wpCategory.children(':selected').data('undo')) {
            wpCategory
              .val(wpCategory.data('old-select-category'))
              .data('select-category', wpCategory.data('old-select-category'))
              .closest('[data-customSelect]').customSelect('refresh');

            var optSelected = wpCategory.children('option:selected');
            var serviceMeal = optSelected.data('service-list-meal');
            var listMeal = serviceMeal ? serviceMeal.split(',') : [];
            var serviceCode = optSelected.data('service-list-code');
            var listCode = serviceCode ? serviceCode.split(',') : [];
            var serviceAmount = optSelected.data('service-list-amount');
            var listAmount = serviceAmount ? serviceAmount.split(',') : [];
            var cValue = wpCategory.find('option:selected').data('code');
            var selectedMealVal = (cValue === 'RL' || cValue === 'BZ' || cValue === 'BC') ? getSelectedMealValue(category) : '';

            renderMealTemplate(wpMeal, listMeal, listCode, selectedMealVal, listAmount);

            wpMeal
              .val(selfOrigin.next().data('option-meal'))
              .data('option-meal', selfOrigin.next().data('option-meal'))
              .closest('[data-customSelect]').customSelect('refresh');
          }
        });
      };

      // Get value from selectbox Meals
      var getSelectedMealValue = function(cEl) {
        var selectedMealVal = '';
        var categoryEls = cEl.closest('.accordion__content-info').find('[data-select-category]').not(cEl);
        categoryEls.each(function() {
          var self = $(this);
          if (self.val() === cEl.val()) {
            selectedMealVal = self.closest('[data-meal-portion]').find('[data-option-meal]').val();
            return false;
          }
        });
        return selectedMealVal;
      };

      meal.data('option-meal', meal.val());
      category.data('select-category', category.val());
      category.data('old-select-category', category.val());
      el.next().data('option-meal', meal.data('option-meal'));

      customSelectMeal.removeClass('alert-selectbox');

      category.closest('[data-customselect]')
        .off('afterSelect.updateMeal')
        .on('afterSelect.updateMeal', function(){
          var optSelected = category.children('option:selected');
          var serviceMeal = optSelected.data('service-list-meal');
          var listMeal = serviceMeal ? serviceMeal.split(',') : [];
          var serviceCode = optSelected.data('service-list-code');
          var listCode = serviceCode ? serviceCode.split(',') : [];
           var serviceAmount = optSelected.data('service-list-amount');
            var listAmount = serviceAmount ? serviceAmount.split(',') : [];
          var cValue = category.find('option:selected').data('code');
          var selfPortion = $(this).closest('[data-meal-portion]');
          var selectedMealVal = (cValue === 'RL' || cValue === 'BZ') ? getSelectedMealValue(category) : '';
          var checkinAlert = $('.select-meal-item').find('.checkin-alert');
          checkinAlert.hide();

          var elmSelfPortion;
          if (selfPortion.hasClass('operated-scoot')) {
            elmSelfPortion = selfPortion;
          } else {
            elmSelfPortion = category.closest('.accordion__wrapper--content').find('[data-meal-portion]').not(selfPortion).not('.operated-scoot');
          }

          var obj = {
            ctgs: elmSelfPortion,
            cVal: category.val(),
            // mVal: meal.val(),
            ctgCode: cValue,
            ctg: category,
            ml: meal,
            wp: wp,
            param: [listMeal, listCode, selectedMealVal, listAmount],
            chks: checkinAlert
          };

          populateCategoryBlock(obj);
          // checkinAlert.hide();

          if (isValidated) {
            var accor = $(this).closest('.accordion__content-info');
            validateAccordion(accor);
          }
        });

      checkinAlert.hide();
      customSelectMeal.off('animationend.category webkitAnimationEnd.category MSAnimationEnd.category oanimationend.category').on('animationend.category webkitAnimationEnd.category MSAnimationEnd.category oanimationend.category' , function(){
        customSelectMeal.removeClass('alert-selectbox');
      });

      customSelectMeal
        .off('afterSelect.updateMeal')
        .on('afterSelect.updateMeal', function(){
          var wrapper = $(this).closest('[data-meal-portion]');
          if(wrapper.hasClass('operated-scoot')) {
            var accorWrapper = wrapper;
          } else {
            var accorWrapper = wrapper.closest('.accordion__wrapper--content');
          }
          var selectedCEl = category.children(':selected');

          var elScoot;
          if(wrapper.hasClass('operated-scoot')) {
            elScoot = true;
            var mealList = wrapper.closest('.accordion__content-info').find('.meal-list');
            if (meal.val() !== 'Select') {
              mealList.find('li > strong').empty();
              mealList.find('li > strong').text(category.val() + ' - ' + meal.val());
            } else {
              mealList.find('li > strong').empty();
              mealList.find('li > strong').text(category.val());
            }
          } else {
            elScoot = false;
          }


          if(selectedCEl.data('populate')){
            populateMealBlock(accorWrapper, meal.val(), category.val(), elScoot);
            customSelectMeal.addClass('alert-selectbox');
          }

          if (selectedCEl.data('undo')) {
            checkinAlert.hide();
            checkinAlert.data('option-meal', meal.data('option-meal'));
            category.data('old-select-category', category.data('select-category'));
            category.data('select-category', category.val());
            if(checkinAlert.length > 1){
              wrapper
                .next().show()
                .find('span').text(wp.data('alertTemplate').format(meal.val(), meal.data('option-meal')));
            }

            meal.data('option-meal', meal.val());

          }

          if (isValidated) {
            var accor = $(this).closest('.accordion__content-info');
            validateAccordion(accor);
          }

          if(meal.val() === L10n.selectMeal.ifMealOption) {
            selectedCEl.parent().find('option:eq(0)').prop('selected', true);
            selectedCEl.parent().closest('[data-customselect]').find('.select__text').html(L10n.selectMeal.ifMealOption);
            selectedCEl.parent().closest('[data-customselect]').trigger('afterSelect.updateMeal');
            checkinAlert.hide();
          }
        });

        var checkinAlertUndo = $('.checkin-alert');
        checkinAlertUndo.find('[data-undo]')
        .off('click.undo')
        .on('click.undo', function(e){
          e.preventDefault();
          undoAction(checkinAlertUndo.prev());
          checkinAlertUndo.hide();
        });
      el.closest('.select-meal-item').find('[data-cancel-meals]')
        .off('click.calcel')
        .on('click.calcel', function(e){
          e.preventDefault();
          el.find('[data-select-category]').closest('[data-customSelect]').customSelect('refresh');
          el.find('[data-option-meal]').closest('[data-customSelect]').customSelect('refresh');
        });
    };

    // Append data from JSON to template
    var renderTemplateFromJSON = function(mealData){
      $.get(config.url.mb.selectFlightMeal, function (tpl) {
        var mealWrapper = $('[data-accordion-wrapper-content]');
        mealWrapper.find('[data-accordion]').remove();
        var template = window._.template(tpl, {
          data: mealData
        });
        $(template).prependTo(mealWrapper);
        SIA.accordion.initAccordion();

        mealWrapper.find('[data-customselect]').each(function(){
          var self = $(this);
          if(!self.data('customSelect')){
            self.customSelect({
              itemsShow: 5,
              heightItem: 43,
              scrollWith: 2
            })
            .customSelect('_createTemplate')
            .customSelect('refresh');
          }
        });

        var blockMeal = $('[data-select-meal]', blockMeal);
        var applyMeal = function(mealPortion, checkinAlert, self){
          mealPortion.each(function(){
            intergrateData($(this), checkinAlert, self);
          });

        };

        blockMeal.each(function(){
          var self = $(this);
          var mealPortion = $('[data-meal-portion]', self);
          var checkinAlert = $('.checkin-alert', self);
          applyMeal(mealPortion, checkinAlert, self);

          if(!self.data('alertTemplate')){
            self.data('alertTemplate', checkinAlert.eq(0).find('span').text());
            window.alertTemplate = self.data('alertTemplate');
          }
        });

        // if (SIA.accordion.initAccordion) {
        //   SIA.accordion.initAccordion();
        // }

        if(SIA.stickySidebar) {
          SIA.stickySidebar()
        }

        reInitEvent();
        initPopup();

      });
    };

    // Render sidebar template
    var renderSidebarTemplate = function(mealData) {
      $.get(config.url.mb.selectFlightMealSidebar, function (tpl) {
        var bookingNav = $('.sidebar .booking-nav');
        var template = window._.template(tpl, {
          data: mealData
        });
        if (bookingNav.length) {
          $(template).prependTo(bookingNav.empty());
          navPaxs = $('.sidebar .booking-nav a.booking-nav__item');
          selectedPaxIdx = navPaxs.siblings('select').find('option:selected').index();
        }
      });
    };

    // Call Ajax
    var getAjaxMeal = function(){
      var url;

      if(!body.is('.select-meals-bah')) {
        url = body.is('.select-meals-new') ? global.config.url.mealFlightJSON1 : global.config.url.mealFlightJSON;
      } else {
        url = global.config.url.mealFlightScootJSON;
      }
      $.ajax({
        url: url,
        dataType: 'json',
        success: function(mealJSON){
          if (mealJSON && mealJSON.MealVO && mealJSON.segmentMealVO) {
            globalJson.selectMeals = mealJSON;

            setHiddenValue(mealJSON);

            renderSidebarTemplate(mealJSON.MealVO);
            renderTemplateFromJSON(mealJSON);
          }
          else {
            renderSidebarTemplate(globalJson.selectMeals.MealVO);
            renderTemplateFromJSON(globalJson.selectMeals);
          }
        },
        error: function() {
          window.alert(L10n.selectMeal.msgError);
        }
      });
    };

    // Set value for input hidden
    var setHiddenValue = function(json) {
      var hiddenPaxId = $('#passenger-id');
      var hiddenSegmentEl = $();
      var hiddenMealCategoryEl = $('#meal-category');
      var selectedIdx = json.MealVO.passengerStartingPoint - 1;
      var mealCategory = '';
      var paxAndMealVo = json.MealVO.passengerAndMealAssociationVO[selectedIdx];

      if (paxAndMealVo) {
        var paxId = paxAndMealVo.passengerId;
        if (paxId) {
          hiddenPaxId.val(paxId);
        }

        var mealSelectedInfo = paxAndMealVo.flightDateInformationVO[0]
          .SectorMealSelectedInfo;
        if (mealSelectedInfo && mealSelectedInfo.length) {
          $.each(mealSelectedInfo, function(idx, mealInfo) {
            hiddenSegmentEl = $('#segment-' + (idx + 1));
            if (hiddenSegmentEl.length) {
              hiddenSegmentEl.val((idx + 1) + ':' + mealInfo.Name);
            }
            if (mealInfo.mealCategoryCode) {
              mealCategory += mealInfo.mealCategoryCode + ',';
            }
          });
          if (mealCategory.lastIndexOf(',') === mealCategory.length - 1) {
            mealCategory = mealCategory.substr(0, mealCategory.length - 1);
          }
          hiddenMealCategoryEl.val(mealCategory);
        }
      }
    };

    // Set min-height
    var setMinHeight = function(){
      var sidebar = $('[data-fixed]');
      var siblings = sidebar.next();
      var mainInnerHeight = $('.main-inner').outerHeight();

      var setHeight = function(){
        var sidebarHeight = sidebar.children('div').outerHeight();
        var accorWrapperHeight = siblings.children('[data-accordion-wrapper]').outerHeight();
        var minHeight = Math.max(mainInnerHeight, sidebarHeight, accorWrapperHeight);
        siblings.css({
          'min-height': minHeight
        });
      };

      win.off('resize.setMinHeightMBSM').on('resize.setMinHeightMBSM', function(){
        setTimeout(function(){
          if(window.innerWidth <= global.config.mobile){
            siblings.css('min-height', '');
          }
          else{
            setHeight();
          }
        }, 10);
      });

      setHeight();
    };

    getAjaxMeal();
    setMinHeight();
  };

  $(document).on('click', '.trigger-popup-details-bah', function(e) {
    var popupBspPremiumBah = $('.details-bsp-premium-bah');
    e.preventDefault();
    popupBspPremiumBah.Popup('show');
    popupBspPremiumBah.Popup('show');
    $('body').find('.bg-modal').addClass('overlay');
  });

  $(document).on('click', '.popup__close, .popup__close_2', function(event) {
    var popupBspPremiumBah = $('.details-bsp-premium-bah');
    popupBspPremiumBah.Popup('hide');
    event.preventDefault();
  });

  // Validate accordion
  var validateAccordion = function(accor) {
    var isValid = true;
    var mealItem = accor.find('[data-meal-portion]');
    var cEls = mealItem.length ?
      mealItem.find('[data-select-category]') : $();

    if (!validateCategory(cEls)) {
      isValid =false;

      if (!accor.find('p.text-error span').length) {
        accor.append('<p class="text-error"><span>' + L10n.selectMeal.msgCategory +
          '</span></p>');
        cEls.closest('.form-group').addClass('error');
      }
    }
    else {
      if (accor.find('p.text-error span').length) {
        accor.find('p.text-error span').remove();
        cEls.closest('.form-group').removeClass('error');
      }
    }

    return isValid;
  };

  // Validate category
  var validateCategory = function(cEls) {
    if (cEls.length > 1) {
      var isValid = true;
      var cVlue = '';
      var mVlue = '';

      cEls.each(function() {
        var cEl = $(this);
        var mEl = cEl.closest('[data-meal-portion]').find('[data-option-meal]');
        var cElVal = cEl.val();
        var cElDataOp = cEl.find('option:selected').data('code');
        if(!cVlue && cElDataOp !== 'JP' && cElDataOp !== 'BC' && cElDataOp !== 'BZ'){
          cVlue = cElVal;
        }
        // if(!cElDataOp){
        //  isValid = false;
        // }
        // if (cElVal !== cVlue && cVlue) {
        //   if(cElDataOp !== 'JP' && cElDataOp !== 'BC'){
        //     cVlue = cElVal;
        //     mVlue = mEl.val();
        //     isValid = false;
        //   }
        // }
      });
      return isValid;
    }
    return true;
  };

  // Handle validation
  var handleValidation = function(){
    var bookingNav = $('.sidebar .booking-nav');
    var form = $('.form--select-meals');
    var directPage = $('[data-url]', form);

    var validateForm = function() {
      var isValid = true;
      isValidated = true;
      form.find('.accordion__content-info').each(function() {
        var self = $(this);
        if(!validateAccordion(self)) {
          isValid = false;
        }
      });
      // if (!isValid) {
      //   var accorTrigger = $('p.text-error span').eq(0)
      //     .closest('[data-accordion]').find('[data-accordion-trigger]');
      //   if (accorTrigger.length && !accorTrigger.hasClass('active')) {
      //     accorTrigger.trigger('click.accordion');
      //   }
      // }
      return isValid;
    };

    var submitHandler = function(e) {
      if(!validateForm()) {
        e.preventDefault();
      }
    };

    bookingNav
      .off('change.switch-passenger')
      .on('change.switch-passenger', 'select', function() {
        var selectTab = $(this);
        var index = selectTab.find('option:selected').index();
        if (validateForm() && navPaxs.length) {
          window.location.href = navPaxs.eq(index).attr('href');
        }
        else {
          selectTab.prop({
            selectedIndex: selectedPaxIdx
          });
        }
      });

    bookingNav
      .off('click.validateMeal')
      .on('click.validateMeal', 'a.booking-nav__item', function(e) {
        submitHandler(e);
      });

    form.off('submit.slCategory').on('submit.slCategory', function(e) {
      submitHandler(e);
    });

    directPage.off('click.changeurlForm').on('click.changeurlForm', function(){
      form.attr('action', directPage.data('url'));
      isValidated = true;
      if (validateForm()) {
        form[0].submit();
      }
    });
  };
  var reInitEvent = function() {
    // Fix Height Sidebar
    var mHeight = $('aside.sidebar').outerHeight() + parseInt($('aside.sidebar').css('bottom'), 10);

    $('.wrap-select-meals').css('min-height', mHeight + 'px');

    $('aside.sidebar inner').css('min-height', $('aside.sidebar inner').outerHeight());

    // Handle Click Select Meal Button

    $('[data-select-meal] .accordion__content-info').each(function(){
      $(this).css('min-height', $(this).outerHeight() - 40);
    });

    $('.select-meal-item').each(function(){
      $(this).find('.text-dark').first().attr('aria-hidden', true);
    });

    $('[data-select-meal-trigger]').on('click.showItemMeal', '.select-meals', function(e){
      var _self = $(this),
          content = _self.parent().find('.select-meal-item');
      _self.attr('aria-hidden', true).hide();
      content.attr('aria-hidden', false).slideDown('slow');
      setTimeout(function(){
        content.find('.text-dark').first().attr({
        'aria-hidden': false
      }).focus();
      },10);

    });
    $('[data-select-meal-trigger]').on('click.hideItemMeal', '.select-meal-item .cancel-select', function(){
      var _self = $(this);
      $.when(_self.parent().attr('aria-hidden', true).slideUp('slow')).done(function(){
        _self.parent().find('.text-dark').first().attr('aria-hidden', true);
        _self.parent().prev().attr('aria-hidden', false).show().focus();
      });
    });
  };

  var initPopup = function(data) {
    var triggerPopup = $('[data-trigger-popup]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));
      var measureScrollbar = (function(){
        var a = document.createElement('div');
        a.className = 'modal-scrollbar-measure';
        body.append(a);
        var b = a.offsetWidth - a.clientWidth;
        return $(a).remove(), b;
      })();

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',
          afterShow: function(that) {
            that.modal.attr({
              'open' : 'true',
              'role' : 'dialog'
            });

            var help = 'Beginning of dialog window. Press escape to cancel and close this window.'; // TODO l10n.prop
            that.modal.find('#popup--help').remove();
            that.modal.prepend('<p id="popup--help" class="says">' + help + '</p>');
            that.modal.find('.popup__content').removeAttr('role');
            that.modal.find('.popup__content').attr({
              'aria-describedby' : 'popup--help',
              'aria-hidden': false
            });
            that.modal.find('.popup__content').find('[tabindex]').first().attr('aria-hidden', false).focus();
            var pEl =that.modal.find('.popup__content').find('p[tabindex],span[tabindex]');
            pEl.each(function(i, v){
              $(v).attr({'aria-label': $(v).text() });
            });
          },
          afterHide: function(){
            container.css('padding-right', '');
            body.css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
              if (ua.indexOf('chrome') > -1) { }
              else {
                body.attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
              }
            }
          }
        });
      }
      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        var jsonURL = self.data('flight-json-url');
        self.attr('data-clicked', true);
        if (!self.hasClass('disabled')) {
          container.css('padding-right', measureScrollbar);
          body.attr('style', function(i, s) { return s ? s + 'overflow:hidden !important;' : 'overflow:hidden !important;'; });
          renderPopUpDetail(popup, jsonURL, function() {
            popup.Popup('show');
            $(window).trigger('resize');
          });
        }
      });
    });
  };

  var renderPopUpDetail = function(popupEl, jsonURL, callback) {
    var flightURL = '';
    if(jsonURL) {
      flightURL = jsonURL;
    } else {
      flightURL = global.config.url.mealFlightPopupJSON;
    }
    $.ajax({
        url: flightURL,
        dataType: 'json',
        success: function(flightMeal){
          $.get(config.url.mb.selectFlightMealPopupMenu, function (tpl) {
            var popupContent = popupEl.find('.popup__content');
            var template = window._.template(tpl, {
              data: flightMeal
            });


            if (popupContent.length) {
              $(template).prependTo(popupContent.empty());

              if(SIA.initTabMenu) {
                SIA.initTabMenu();
              }

              if(SIA.multiTabsWithLongText) {
                SIA.multiTabsWithLongText()
              }
              // hideLoading
              if (callback) {
                callback();
              }
            };
            $('.popup--meal-maincourse').find('.popup__close').on('click.hidePopup', function(e){
              e.preventDefault();
              var clicked =  $('[data-clicked]');
              $(this).closest('.popup--meal-maincourse').Popup('hide');
              $(this).closest('.popup__content').attr('aria-hidden', true);
              clicked.removeAttr('data-clicked');
              setTimeout(function(){
                clicked.focus();
              }, 100);
            });

            var totalTabWidth = 0,
              tabLevel1El = $('.tab-level-1'),
              OpEl = $('<option></option>'),
              listTabLevel1 = $('.tab-level-1').find('.tab-item'),
              selectForCustomSelector = $('#multi-select-limit-1'),
              customSelectSelector = tabLevel1El.find('[data-customselect]');
            for(var i = 0; i < listTabLevel1.length; i ++) {
              var tabItemElement = $(listTabLevel1[i]);
              totalTabWidth += tabItemElement.outerWidth();
              if(totalTabWidth >= tabLevel1El.outerWidth() - 150) {
                if(!tabItemElement.is('.limit-item')) {
                  selectForCustomSelector.append('<option value="' + tabItemElement.text() + '" data-indexTab="' + i + '">' + tabItemElement.text() + '</option>');
                  tabItemElement.detach();
                }
              }
            }

            selectForCustomSelector.find('option').length > 1 ? customSelectSelector.css({'display': 'block'}) : null;
            // desktroy old customSelect
            customSelectSelector['customSelect']('destroy');
            // init new customSelect
            SIA.initCustomSelect();

            customSelectSelector.find('.select__text').text('More');

            var listItemMoreId = $('[data-keep-limit]').find('input').attr('aria-owns');

            $('#'+ listItemMoreId).find('li').removeClass('active');

            var dropdownTab = $('[data-dropdown-tab="true"]'),
                idx = 0;
            dropdownTab.find('select').each(function(){
              var selectEl = $(this);
              selectEl.off('change.textTabs').on('change.textTabs', function(){
                  var _self = $(this),
                      _tabWrap1 = _self.closest('[data-multi-tab]')
                      _listcontent1 = _tabWrap1.find('.tab-wrapper-1 .tab-content-1');

                  idx = selectEl.prop('selectedIndex');
                  _listcontent1.removeClass('active');
                  _listcontent1.eq(idx).addClass('active');
                });
            });
            var idxTab = 0;
            selectForCustomSelector.off('change.tabLevel1').on('change.tabLevel1', function(){
              var _tabWrap = listTabLevel1.closest('[data-multi-tab]')
                  _listcontent = _tabWrap.find('.tab-wrapper .tab-content');

              idxTab = $(this).children('option:selected').attr('data-indexTab');
              if(idxTab > -1) {
                // listTabLevel1.removeClass('active');
                _listcontent.removeClass('active');
                _listcontent.eq(idxTab).addClass('active');
              }
            });


          });
        },
        error: function() {
          window.alert(L10n.selectMeal.msgError);
        }
      });
  };


  var init = function(){
    populateData();
    handleValidation();
  };

  if(jsonPage) {
    init();
  } else {
    reInitEvent();
  }

};
