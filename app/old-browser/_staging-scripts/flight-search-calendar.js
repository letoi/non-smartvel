/**
 * @name SIA
 * @description Define global flightSearchCalendar functions
 * @version 1.0
 */
SIA.flightSearchCalendar = function(){

	if(!$('.flight-search-calendar-page, .orb-flight-schedule').length){
		return;
	}

	var bindActions = function() {
		var flightSearchCalendar = $('.flight-search-calendar');
		var next = $('.flight-search-calendar__control .slick-next', flightSearchCalendar);
		var prev = $('.flight-search-calendar__control .slick-prev', flightSearchCalendar);
		next.attr('href', next.data('url-old'));
		prev.attr('href', prev.data('url-old'));


		var checkedInput = $('.flight-search-calendar__item',flightSearchCalendar).find('input[checked="checked"]');
		checkedInput.parent().addClass('checked');

		flightSearchCalendar
		.off('change.addClassChecked')
		.on('change.addClassChecked', '.flight-search-calendar__item input', function() {
			$(this).closest('.flight-search-calendar').find('.search-calendar.checked').removeClass('checked');
			$(this).parent().addClass('checked');
		});
	};

	if($('.flight-search-calendar-page').length) {
		//CIB page
		bindActions();
	}
	else {
		//ORB Flight Schedule page
		$.get(SIA.global.config.url.orbFlightSchedule, function(html) {
			var template = window._.template(html, {
				data: globalJson.flightSchedule
			});

			$('#form-flight-schedule').html(template);

			bindActions();
		});
	}
};
