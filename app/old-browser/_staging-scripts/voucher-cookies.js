/**
 * @name SIA
 * @description Define cookies used for voucher
 * @version 1.0
 */
SIA.voucherStored = function() {
	var setCookie = function(cname, cvalue, exdays) {
    if (!exdays) {
      exdays = 1;
    }
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+d.toGMTString();
		document.cookie = cname + '=' + JSON.stringify(cvalue) + ';' + expires;
	};

	var getCookie = function(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i].trim();
			if (c.indexOf(name) === 0){
				return $.parseJSON(c.substring(name.length, c.length));
			}
		}
		return '';
	};

	var checkCookie = function (name) {
		var user = getCookie(name);
		return user.length || 0;
	};

  // Return public function
	return {
		setCookie: setCookie,
		getCookie: getCookie,
		checkCookie: checkCookie
	}
};
