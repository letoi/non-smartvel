/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteStatements = function() {
	var global = SIA.global;
	var player;
	var playerEl = $('#player');
	var w = playerEl.width();
	var h = playerEl.height();
	var templateIE7 = '<iframe width="{2}" height="{1}" src="https://www.youtube.com/embed/{0}?autoplay={3}" frameborder="0" allowfullscreen></iframe>';

	var initYoutubePlayer = function() {
		var thumbnails = $('[data-statement-url]');
		var url = thumbnails.eq(0).data('statement-url');

		thumbnails.each(function() {
			var thumbnail = $(this);
			thumbnail.off('click.playYoutube').on('click.playYoutube', function(e) {
				e.preventDefault();
				url = thumbnail.data('statement-url');
				if (!player) {
					window.onYouTubeIframeAPIReady();
				} else {
					if (global.vars.isIE() && global.vars.isIE() > 7) {
						player.loadVideoById(url);
					} else {
						player = $('#player').html(templateIE7.format(url, h, w, 1));
					}
				}
			});
		});

		window.onYouTubeIframeAPIReady = function() {
			if (global.vars.isIE() && global.vars.isIE() > 7) {
				player = new window.YT.Player('player', {
					height: h,
					width: w,
					videoId: url
				});
			} else {
				player = playerEl.html(templateIE7.format(url, h, w, 0));
			}
		};
	};

	var handleArrows = function() {
		var slides = $('.slick-slide');
		var len = slides.length;
		var prevBtn = $('.flexslider-prev');
		var nextBtn = $('.flexslider-next');
		nextBtn.css({
			'display': slides.eq(len - 1).hasClass('slick-active') ? 'none' : 'block'
		});
		prevBtn.css({
			'display': slides.eq(0).hasClass('slick-active') ? 'none' : 'block'
		});
	};

	var initSlider = function() {
		var youtubeList = $('.watch-list-1');
		youtubeList.on('init', function() {
			handleArrows();
		}).on('afterChange', function() {
			handleArrows();
		}).slick({
			siaCustomisations: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			draggable: false,
			slide: 'li',
			infinite: false,
			nextArrow: '<a href="#" class="slick-next flexslider-next">Next</a>',
			prevArrow: '<a href="#" class="slick-prev flexslider-prev">Prev</a>'
		});
	};

	var initModule = function() {
		initYoutubePlayer();
		initSlider();
	};

	initModule();
	window.onYouTubeIframeAPIReady();
};
