/**
 * @name SIA
 * @description Define global chatWidget functions
 * @version 1.0
 */

SIA.chatWidget = function() {
	var needHelp = $('.popup--need-help'),
		closeHelp = needHelp.find('.popup__close');

	window.newChat = function(url) {
		window.open(
			url, 'popUpWindow', 'height=775,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes'
		);
	};

	if (closeHelp.length) {
		closeHelp.on('click.closeNeed', function(e) {
			e.preventDefault();
			needHelp.addClass('hidden');
		});
	}
};