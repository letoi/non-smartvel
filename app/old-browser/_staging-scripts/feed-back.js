/**
 * @name SIA
 * @description Define feed back global feedBack and functions
 * @version 1.0
 */
SIA.feedBack = function() {
	// var global = SIA.global;
	var form = $('#form-feedback');

	var feedBackFieldGrps = form.find('.feedback, .button-group-1, .terms-conditions');
	var feedBackAttachment = feedBackFieldGrps.not('.feedback--details, .feedback--contact');
	var buttonGroup = form.find('.button-group-1');
	var termsConditionsEl = form.find('.terms-conditions');

	var feedBackType = $('[data-feedback-type]');
	var feedBackCategory = $('[data-feedback-category]');
	var feedBackTopic = $('[data-feedback-topic]');
	var feedBackSubTopic = $('[data-feedback-sub-topic]');
	var feedBackQuestion = $('[data-feedback-question]');

	//captcha
	var captchaEl = form.find('.captcha-wrap');

	var type = feedBackType.find('input:radio');
	var category = feedBackCategory.find('[data-customselect]');
	var topic = feedBackTopic.find('[data-customselect]');
	var subTopic = feedBackSubTopic.find('[data-customselect]');

	var feedBackquestion1 = $('[data-feedback-question="1"]');
	var feedBackQuestion2 = $('[data-feedback-question="2"]');

	var typeIndex = -1;
	var scIdx = -1;
	var stIdx = -1;
	var sstIdx = -1;
	var scVlue = -1;
	var stVlue = -1;
	var sstVlue = -1;
	var validateIndex = -1;

	var dashboard = {
		results: {
			result: {
				'1': ['1', '2', '3', '10*', '11*', '12*', '13', '14*'],
				'2': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*'],
				'3': ['1*', '2', '3', '10*', '11*', '12*', '13', '14*'],
				'4': ['1*', '2', '10*', '11*', '12*', '13', '14*'],
				'5': ['17*', '18', '3', '10*', '11*', '12*', '13', '14*'],
				'6': ['1*', '2', '3', '21*', '10*', '11*', '12*', '13', '14*'],
				'7': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '14*'],
				'8': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*'],
				'9': ['1*', '2', '3', '21', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '14*'],
				'11': ['1*', '2', '3', '21*', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*'],
				'10': ['1*', '2', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'12': ['17*', '18', '3', '5*', '6*', '7*', '8*', '10*', '15', '11*', '12*', '13*', '14*'],
				'13': ['17*', '18', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'14': ['1*', '2', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'15': ['17*', '18', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'16': ['17*', '18', '3', '5*', '6*', '7*', '8*', '9', '20', '10*', '15', '11*', '12*', '13*', '14*'],
				'17': ['17*', '18', '3', '19', '22', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'18': ['17*', '18', '3', '19*', '22', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'19': ['1*', '18', '3', '4', '5*', '6*', '7*', '8*', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'20': ['1*', '18', '3', '4', '5', '6', '7', '8', '9', '10*', '15', '11*', '12*', '13', '14*'],
				'21': ['1*', '2', '3', '21', '4', '5', '6', '7', '8', '9', '10*', '11*', '12*', '13', '14*']
			},
			getResult: function(r){
				return this.result[r];
			}
		},
		category: [0, 1, 0, 2],
		subTopics: {
			subTopic: {
				'0': [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
				'1': [-1, 0, 1],
				'2': [-1, 2, 3],
				'3': [-1, 4, -1, 5, -1],
				'4': [-1, -1, -1, -1, -1],
				'5': [-1, -1, -1, -1, 4, -1, 6, -1, 5, -1],
				'6': [-1, -1, -1, -1, -1]
			},
			getSubTopic: function(st) {
				return this.subTopic[st];
			}
		},
		topics: {
			'0': [-1, -1, -1, -1, -1, -1],
			'1': [-1, 0, 1, 2, 3, -1, -1],
			'2': [-1, -1, -1, -1, -1, -1],
			'3': [-1, 4, 2, 5, 6, -1, -1]
		},
		questions: {
			question: [
				// begin compliment category

				{
					type: 0,
					category: 1,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 2,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 3,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},
				{
					type: 0,
					category: 4,
					topic: -1,
					subTopic: -1,
					validateIndex: 9
				},
				{
					type: 0,
					category: 5,
					topic: -1,
					subTopic: -1,
					validateIndex: 7
				},

				// end compliment category

				// -------------------------------------------------------------

				// begin concern category

					// begin category 1

				{
					type: 1,
					category: 1,
					topic: 1,
					subTopic: -1,
					// quesIdx: 0
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 2,
					subTopic: -1,
					// quesIdx: 1
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 3,
					subTopic: -1,
					// quesIdx: 2
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 4,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 5,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 6,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 7,
					subTopic: -1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 8,
					subTopic: -1,
					// quesIdx: 3
					validateIndex: 15
				},
				{
					type: 1,
					category: 1,
					topic: 9,
					subTopic: -1,
					validateIndex: 13
				},
				{
					type: 1,
					category: 1,
					topic: 10,
					subTopic: -1,
					validateIndex: 10
				},

					// end category 1

				// -------------------------------------------------------------

					// begin category 2

				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 1,
					// quesIdx: 4
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 2,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 3,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 4,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 5,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 1,
					subTopic: 6,
					validateIndex: 13
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 1,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 2,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 3,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 4,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 5,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 6,
					validateIndex: 15
				},
				{
					type: 1,
					category: 2,
					topic: 2,
					subTopic: 7,
					validateIndex: 15
				},

					// end category 2

				// ------------------------------------------------------------

					// begin category 3

				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 1,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 2,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 1,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 1,
					quesIdx: 5
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 2,
					quesIdx: 5
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 1,
					category: 3,
					topic: 2,
					subTopic: 4,
					validateIndex: 10
				},

					// end category 3

				// --------------------------------------------------------------

					// begin category 4

				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 2,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 3,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 4,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 5,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 6,
					validateIndex: 11
				},
				{
					type: 1,
					category: 4,
					topic: 1,
					subTopic: 7,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 2,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 3,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 3,
					subTopic: 2,
					validateIndex: 6
				},
				{
					type: 1,
					category: 4,
					topic: 4,
					subTopic: -1,
					validateIndex: 6
				},

					// end category 4

				// ----------------------------------------------------------

					// begin category 5

				{
					type: 1,
					category: 5,
					topic: -1,
					subTopic: -1,
					quesIdx: 6
				},


					// end category 5

				// -----------------------------------------------------------

					// begin category 6

				{
					type: 1,
					category: 6,
					topic: -1,
					subTopic: -1,
					validateIndex: 3
				},

					// end category 6

				// end concern category

				// ----------------------------------------------------------

				// begin suggestion category

				{
					type: 2,
					category: 1,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 2,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 3,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 2,
					category: 4,
					topic: -1,
					subTopic: -1,
					validateIndex: 11
				},
				{
					type: 2,
					category: 5,
					topic: -1,
					subTopic: -1,
					validateIndex: 8
				},

				// end suggestion category

				// --------------------------------------------------------

				// begin question category

					// begin category 1

				{
					type: 3,
					category: 1,
					topic: 1,
					subTopic: -1,
					// quesIdx: 0
					validateIndex: 15
				},
				{
					type: 3,
					category: 1,
					topic: 2,
					subTopic: -1,
					validateIndex: 14
				},
				{
					type: 3,
					category: 1,
					topic: 3,
					subTopic: -1,
					validateIndex: 14
				},
				{
					type: 3,
					category: 1,
					topic: 4,
					subTopic: -1,
					validateIndex: 19
				},

					// end category 1

				// ---------------------------------------------------------

					// begin category 2

				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 1,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 2,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 1,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 1,
					quesIdx: 5
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 2,
					quesIdx: 5
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 3,
					validateIndex: 10
				},
				{
					type: 3,
					category: 2,
					topic: 2,
					subTopic: 4,
					validateIndex: 10
				},

					// end category 2

				// ---------------------------------------------------------

					// begin category 3

				{
					type: 3,
					category: 3,
					topic: 1,
					subTopic: -1,
					validateIndex: 4
				},
				{
					type: 3,
					category: 3,
					topic: 2,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 3,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 1,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 2,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 3,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 4,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 5,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 6,
					validateIndex: 21
				},
				{
					type: 3,
					category: 3,
					topic: 4,
					subTopic: 7,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 5,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 6,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 6,
					subTopic: 2,
					validateIndex: 6
				},
				// {
				// 	type: 3,
				// 	category: 3,
				// 	topic: 6,
				// 	subTopic: 3,
				// 	validateIndex: 6
				// },
				{
					type: 3,
					category: 3,
					topic: 7,
					subTopic: -1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 8,
					subTopic: 1,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 8,
					subTopic: 2,
					validateIndex: 6
				},
				{
					type: 3,
					category: 3,
					topic: 9,
					subTopic: -1,
					validateIndex: 6
				},

					// end category 3

				// -----------------------------------------------------------

					// begin category 4

				{
					type: 3,
					category: 4,
					topic: 1,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 2,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 3,
					subTopic: -1,
					validateIndex: 8
				},
				{
					type: 3,
					category: 4,
					topic: 4,
					subTopic: -1,
					validateIndex: 3
				},

					// end category 4

				// ---------------------------------------------------------

					// begin category 5

				{
					type: 3,
					category: 5,
					topic: -1,
					subTopic: -1,
					quesIdx: 6
				},

					// end category 5

				// -----------------------------------------------------------

					// begin category 6

				{
					type: 3,
					category: 6,
					topic: -1,
					subTopic: -1,
					validateIndex: 3
				}

					// end category 6

				// end question category
			],
			answer: {
				'0': [
					{
						typeIdx: 1,
						results: [15, 1]
					},
					{
						typeIdx: 3,
						results: [15, 3]
					}
				],
				'1': [{	results: [15, 10]	} ],
				'2': [{ results: [15, 13] }],
				'3': [{ results: [15, 20] }],
				'4': [{ results: [5, 13] }],
				'5': [{ results: [18, 17] }],
				'6': [{ results: [-1, 16, 12] }]
			},
			getQuestion: function(type, c, t, s) {
				var idx = 0;
				var ques = this.question;
				c = parseInt(c);
				t = parseInt(t);
				s = parseInt(s);
				for (idx in ques) {
					if (ques[idx].type === type && ques[idx].category === c && ques[idx].topic === t && ques[idx].subTopic === s) {
						return ques[idx];
					}
				}
				return null;
			},
			getAnswer: function(quesIdx, quesAns, typeIdx) {
				var ans = this.answer[quesIdx];
				if (ans && ans.length > 1) {
					for (var i = ans.length - 1; i >= 0; i--) {
						if (ans[i].typeIdx === typeIdx) {
							return ans[i].results[quesAns];
						}
					}
				}
				else {
					return ans[0].results[quesAns];
				}
			}
		}
	};

	var render = function(orders, fields){
		orders.addClass('hidden');
		orders.find('select, input').addClass('ignore-validate');

		for(var i = 0; i < fields.length; i ++){
			// var f = orders.eq(parseInt(fields[i]) - 1);
			var fieldOrder = fields[i];
			var f = $();

			if (fieldOrder.indexOf('c') !== -1) {
				f = orders
					.filter('[data-order="' + parseInt(fieldOrder) + '"]')
					.filter('[data-order-contact]');
			}
			else {
				f = orders
					.filter('[data-order="' + parseInt(fieldOrder) + '"]')
					.not('[data-order-contact]');
			}

			if (f.length) {
				f.removeClass('hidden');
				f.closest('.feedback').removeClass('hidden');
				f.find('label').eq(0).text(f.find('label').eq(0).text().replace('*', ''));

				if(fields[i].indexOf('*') !== -1){
					f.find('select, input').removeClass('ignore-validate');
					f.find('label').eq(0).text(f.find('label').eq(0).text() + '*');
				}
			}
		}

		feedBackAttachment.removeClass('hidden');
		termsConditionsEl.removeClass('hidden');
		captchaEl.removeClass('hidden');
		buttonGroup.removeClass('hidden');

		if (form.resetForm) {
			form.resetForm();
		}
	};

	var renderFeedbackForm = function(fields){
		var orders = $($('.feedback [data-order]').toArray().sort(function(a, b){
			var aVal = $(a).data('order'),
			bVal = $(b).data('order');
			return aVal - bVal;
		}));

		render(orders, fields);
	};

	var setValidate = function(typeIndex, category, topic, subTopic) {
		var ques = dashboard.questions.getQuestion(typeIndex, category, topic, subTopic);
		if (ques) {
			var qIdx = ques.quesIdx;
			if (typeof(qIdx) !== 'undefined' && feedBackquestion1.eq(qIdx).length) {
				qIdx = parseInt(qIdx);
				var question = feedBackquestion1.eq(qIdx);
				feedBackquestion1.addClass('hidden');
				feedBackQuestion2.addClass('hidden');
				question.removeClass('hidden');

				var rdos = question.find('input:radio');
				if (rdos.length) {
					rdos.prop('checked', false);
				}

				var selectEl = question.find('select');
				if (selectEl.length) {
					selectEl.prop('selectedIndex', 0).closest('[data-customselect]').customSelect('refresh');
				}
			}
			else {
				feedBackquestion1.addClass('hidden');
				feedBackQuestion2.addClass('hidden');
				validateIndex = ques.validateIndex;
				if (validateIndex) {
					renderFeedbackForm(dashboard.results.getResult(validateIndex));
				}
			}
		}
		else {
			feedBackquestion1.addClass('hidden');
			feedBackQuestion2.addClass('hidden');
		}
	};

	var setEventHandler = function() {
		type.off('change.chooseType').on('change.chooseType', function(){
			if ($(this).prop('checked')) {
				feedBackTopic.addClass('hidden');
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');
				feedBackCategory.addClass('hidden');
				category.addClass('hidden');

				typeIndex = type.index(this);
				scIdx = dashboard.category[typeIndex];

				var curCategory = category.eq(scIdx);
				curCategory.find('select').prop('selectedIndex', 0);
				curCategory.customSelect('refresh');
				curCategory.removeClass('hidden');
				feedBackCategory.removeClass('hidden');
			}
		});

		category
			.off('afterSelect.chooseCategory')
			.on('afterSelect.chooseCategory', function() {
				feedBackTopic.addClass('hidden');
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var catVlue = self.find('select').val();

				if (catVlue) {
					scVlue = parseInt(catVlue);
					stIdx = dashboard.topics[typeIndex][catVlue];

					if (stIdx !== -1) {
						var curTopic = topic.eq(stIdx);
						topic.addClass('hidden');
						curTopic.removeClass('hidden');
						curTopic.find('select').prop('selectedIndex', 0);
						curTopic.customSelect('refresh');
						feedBackTopic.removeClass('hidden');
					}
					else {
						stVlue = stIdx;
						sstVlue = stIdx;
						setValidate(typeIndex, scVlue, stVlue, sstVlue);
					}
				}
			});

		topic
			.off('afterSelect.chooseTopic')
			.on('afterSelect.chooseTopic', function() {
				feedBackSubTopic.addClass('hidden');
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var topicVlue = self.find('select').val();

				if (topicVlue) {
					stVlue = parseInt(topicVlue);
					sstIdx = dashboard.subTopics.getSubTopic(stIdx)[stVlue];

					if (sstIdx !== -1) {
						var curSubTopic = subTopic.eq(sstIdx);
						subTopic.addClass('hidden');
						curSubTopic.removeClass('hidden');
						curSubTopic.find('select').prop('selectedIndex', 0);
						curSubTopic.customSelect('refresh');
						feedBackSubTopic.removeClass('hidden');
					}
					else {
						sstVlue = sstIdx;
						setValidate(typeIndex, scVlue, stVlue, sstVlue);
					}
				}
			});

		subTopic
			.off('afterSelect.chooseSubTopic')
			.on('afterSelect.chooseSubTopic', function() {
				feedBackQuestion.addClass('hidden');
				feedBackFieldGrps.addClass('hidden');

				var self = $(this);
				var subTopicVlue = self.find('select').val();

				if (subTopicVlue) {
					sstVlue = parseInt(subTopicVlue);
					setValidate(typeIndex, scVlue, stVlue, sstVlue);
				}
			});

		feedBackquestion1
			.off('change.answer')
			.on('change.answer', 'input:radio, select', function() {
				feedBackFieldGrps.addClass('hidden');
				var self = $(this);
				var quesIdx = feedBackquestion1.index(self.closest('[data-feedback-question="1"]'));
				var answerVlue = '-1';
				if (self.is('select')) {
					answerVlue = self.val() || '-1';
				}
				else {
					if (self.prop('checked')) {
						answerVlue = self.closest('[data-feedback-question="1"]').find('input:radio').index(self);
					}
				}
				validateIndex = dashboard.questions.getAnswer(quesIdx, answerVlue, typeIndex);
				if (validateIndex) {
					renderFeedbackForm(dashboard.results.getResult(validateIndex));
				}
			});

		// form.validate({
		// 	focusInvalid: true,
		// 	errorPlacement: global.vars.validateErrorPlacement,
		// 	success: global.vars.validateSuccess
		// });

		// form.off('submit.feedBack').on('submit.feedBack', function() {
		// 	if (!form.valid()) {
		// 		var orderEl = $('input.error').closest('[data-order]').eq(0);
		// 		if (orderEl.length) {
		// 			$('html,body').animate({
		// 				scrollTop: orderEl.offset().top
		// 			}, 400);
		// 		}
		// 	}
		// });
	};

	setEventHandler();

	/* jshint ignore:start */

	var vars = SIA.global.vars;
	var tellUsMore = $('[data-order="10"]').find('textarea');
	var disabledChb = $('[data-order]').find('input:checkbox');
	var maxLength = parseInt(tellUsMore.attr('maxlength'));

	var feedbackTypes = $("input[name='feedbackType']");
	var rCompliment = $("#compliment");
	var rConcern = $("#concern");
	var rSuggestion = $("#suggestion");
	var rQuestion = $("#question");
	var dCategory = $('#category');
	var dCategoryCencern = $('#category-concern');
	var dCategoryQuestion = $('#category-question');
	var categoryOfFeedback = $('#feedbackCategory');
	var dTopicBooking = $('#topic-booking');
	var dTopicTravel = $('#topic-travel');
	var dTopicBaggage = $("#topic-baggage");
	var dTopicMembership = $('#topic-membership');
	var dTopicPackage = $('#topic-packages');
	var dTopicQbooking = $("#topic-qbooking");
	var dTopicQmembership = $("#topic-qmembership");
	var topicOfFeedback = $('#feedbackTopic');
	var dSubtopicBooking = $('#sub-topic-booking');
	var dSubtopicDuring = $('#sub-topic-during');
	var dSubtopicBaggageBefore = $("#sub-topic-baggage-before");
	var dSubtopicBaggageAfter = $("#sub-topic-baggage-after");
	var dSubtopicKrisflyer = $('#sub-topic-krisflyer');
	var dSubtopicServices = $('#sub-topic-services');
	var dSubtopicOther = $('#sub-topic-other');
	var dSubtopicQualification = $("#sub-topic-qualification");
	var sTopicOfFeedback = $('#feedbackSubTopic');

	var lnSblock = $('[data-order="17"]');
	var lnFblock = $('[data-order="1"]');
	var fnSblock = $('[data-order="18"]');
	var fnFblock = $('[data-order="2"]');
	var userTitle = $("#title");
	var userSbLastName = $("#last-name-2");
	var userFbLastNAme = $("#last-name-1");
	var userFamilyName = $("#familyName");
	var userSbFirstName = $("#first-name-2");
	var userFbFirstName = $("#first-name-1");
	var userGivenName = $("#givenName");
	var fBgender = $("#gender-1");
	var sBgender = $("#gender-2");

	$(document).ready(function(){
		// var ele = document.querySelectorAll('.number-only')[0];
		// var preventpaste = document.querySelectorAll('.preventpaste')[0];

		var ele = $('.number-only')[0];
		var preventpaste = $('.preventpaste')[0];

		if (!ele || !preventpaste) {
			return;
		}

		ele.onkeypress = function(e) {
			if(isNaN(this.value+""+String.fromCharCode(e.charCode)))
			return false;
		}
		ele.onpaste = function(e){
			e.preventDefault();
		}
		preventpaste.onpaste = function(e){
			e.preventDefault();
		}
	});

	$(".feedbackLoginButton").click(function(){
		$('#corporateSelect').addClass('hidden');
	});

	$(".login").click(function(){
		$('#corporateSelect').removeClass('hidden');
	});

	$("#checkbox-passenger").click(function(){

		if($(this).is(':checked')){
			$(this).prop('checked', true);
			$("#thirdPartyFlag").val("true");
		}else{
			$(this).prop('checked', false);
			$("#thirdPartyFlag").val("false");
		}
	});

	$("#feedbackAgreement").click(function(){

		if($(this).is(':checked')){
			$("#feedback-submit").removeClass("disabled");
			$("#feedback-submit").prop("disabled",false);
		}else{
			$("#feedback-submit").addClass("disabled");
			$("#feedback-submit").prop("disabled",true);
		}
	});

	$("#file-reference").off('blur.validatefileReference').on('change.validatefileReference',function(){
		var fileref = $.trim($("#file-reference").val());
		var self=$(this);
		var wrap = self.closest('[data-order]');
		if( fileref.length > 0 ){
			var letters = /^[A-Za-z]+$/;
			var freffirst = fileref.substring(0, 5);
			var freflast = fileref.substring(5);
			if(!freffirst.match(letters) || isNaN(freflast)){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validfileReferencenumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validfileReferencenumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			if(wrap.find('.grid-col').hasClass('error')){
				var errorText = self.attr('data-msg-required');
				wrap.addClass("error");
				wrap.find('.grid-col').find('.text-error').text(errorText);
			}else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
	});

	$("#flight-number").off('blur.validateflightnumber').on('change.validateflightnumber',function(){
		var flightNo = $.trim($("#flight-number").val());
		var self=$(this);
		var wrap = self.closest('[data-order]');
		if( flightNo.length > 0 ){
			var letters = /^[A-Za-z]+$/;
			var fNofirst = flightNo.substring(0, 2);
			var fNolast = flightNo.substring(2);
			if(!fNofirst.match(letters) || isNaN(fNolast)){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validflightnumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validflightnumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			if(wrap.find('.grid-col').hasClass('error')){
				var errorText = self.attr('data-msg-required');
				wrap.addClass("error");
				wrap.find('.grid-col').find('.text-error').text(errorText);
			}else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				wrap.find('.grid-col').removeClass('error');
			}
		}
	});

	var mandatoryErrors = function(){

		var flightNumberfield = $('[data-order="5"]');
		if(flightNumberfield.find('.grid-col').hasClass('error')){
			var errorText = $("#flight-number").attr('data-msg-required');
			flightNumberfield.find('.grid-col').find('.text-error').text(errorText);
		}else {
			$("input[name='flightNo']").trigger('change.validateflightnumber');
		}

		var fileReffield = $('[data-order="19"]');
		if(fileReffield.find('.grid-col').hasClass('error')){
			var errorText = $("#file-reference").attr('data-msg-required');
			fileReffield.find('.grid-col').find('.text-error').text(errorText);
		}else {
			$("input[name='fileReference']").trigger('change.validatefileReference');
		}

	}

	$("#membership-number").off('blur.validatekfnumber').on('change.validatekfnumber',function(){
		var value = $.trim($("#membership-number").val());
		var self = $(this);
		var wrap = self.closest('[data-order]');
		if( value.length > 0 ){
			if( value.charAt(0)!= "8" || value.length != 10){
				if(wrap.find(".text-error").length == 0) {
					wrap.find('.grid-col').append('<p class="text-error">' + L10n.feedback.validkfnumber + '</p>');
				} else {
					wrap.find(".text-error").text(L10n.feedback.validkfnumber);
				}
				wrap.addClass("error");
			}
			else{
				$(this).parents('.grid-col').find(".text-error").empty();
				wrap.removeClass("error");
				//wrap.find('.grid-col').removeClass('error');
			}
		}
		else{
			$(this).parents('.grid-col').find(".text-error").empty();
			wrap.removeClass("error");
			//wrap.find('.grid-col').removeClass('error');
		}
	});

	$('#destinationCity').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].getAttribute('data-text')){
				$(parentDiv).find("#destinationCityFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

	$('#departureCity').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].getAttribute('data-text')){
				$(parentDiv).find("#departureCityFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

	$('#countryCode').on('blur',function() {
		var parentDiv = $(this).closest('div');
		var countryValue=$(this).val();
		var select = $(parentDiv).find(".selectValue");
		var couOptions = $(select).find('option');
		for(var i=0;i<couOptions.length;i++){
			if(countryValue == couOptions[i].text){
				$(parentDiv).find("#countryCodeFeedback").val(couOptions[i].value);
				break;
			}
		}
	});

// -------------------- BEGIN UPLOAD FILE MODULE -------------------------------

	var attachList = $('.list-attachment');
	var attachEl = $('.attachment');
	var attachError = attachEl.find('.text-error');
	var chooseFileEl = $('.choose-file');
	var unsupportHtml5Tpl = '<li>{0}<a href="#"> delete</a></li>';
	var errorTpl = '<p class="text-error">{0}</p>';
	var pattern = /(jpg|jpeg|png|gif|tif|tiff|doc|docx|pdf|txt|zip)$/i;

	// $('[data-support-html5="false"]').removeClass('hidden');
	chooseFileEl.find('.chose-img-name').text('');

	var attachFunction = function(){
		attachEl
			.off('change.chooseFile')
			.on('change.chooseFile', 'input:file', function() {
				attachError.remove();

				if (this.value && this.value.match(/[^\/\\]+$/).length) {
					var fileName = this.value.match(/[^\/\\]+$/)[0];
					var fileExtension = fileName.split('.').pop().toLowerCase();

					if (pattern.test(fileExtension)) {
						attachList.prepend(unsupportHtml5Tpl.format(fileName));
						chooseFileEl.addClass('hidden');
						attachEl.off('change.chooseFile');
					}
					else {
						attachError = $(errorTpl.format('Not supported file format')).insertAfter(attachList);
						this.value = '';
					}
				}
			});
	};

	attachEl
		.off('click.removeFile')
		.on('click.removeFile', 'a, .attachment-close', function(e) {
			e.preventDefault();
			$(this).parent().remove();
			attachError.remove();

			chooseFileEl.find('input:file').val('');
			chooseFileEl.removeClass('hidden');
			attachFunction();
		});
		// .off('change.chooseFile')
		// .on('change.chooseFile', 'input:file', function() {
		// 	attachError.remove();

		// 	if (this.value && this.value.match(/[^\/\\]+$/).length) {
		// 		var fileName = this.value.match(/[^\/\\]+$/)[0];
		// 		var fileExtension = fileName.split('.').pop().toLowerCase();

		// 		if (pattern.test(fileExtension)) {
		// 			attachList.prepend(unsupportHtml5Tpl.format(fileName));
		// 			chooseFileEl.addClass('hidden');
		// 		}
		// 		else {
		// 			attachError = $(errorTpl.format('Not supported file format')).insertAfter(attachList);
		// 			this.value = '';
		// 		}
		// 	}
		// });
		attachFunction();

// ---------------------- END UPLOAD FILE MODULE -------------------------------

	feedbackTypes.click(function(){
		switch($(this).val())
		{
			case "compliment" :
				rCompliment.prop('checked', true);
				break;
			case "concern" :
				rConcern.prop('checked', true);
				break;
			case "suggestion" :
				rSuggestion.prop('checked', true);
				break;
			case "question" :
				rQuestion.prop('checked', true);
				break;
		}
		$("input[name='fileReference']").trigger('change.validatefileReference');
		$("input[name='flightNo']").trigger('change.validateflightnumber');
		$("input[name='krisFlyerMembershipNo']").trigger('change.validatekfnumber');
	});

	var typeOfFeedBack = function() {

		if(rCompliment.is(':checked')){

			var fcategory = dCategory .find("option:selected").text();
			dCategory.off('change').on('change',function(){
				fcategory = dCategory .find("option:selected").text();
			});
			var fCategoryValue = $("#category option:contains('"+fcategory+"')").attr('value');
			//alert(fCategoryValue);
			categoryOfFeedback.val(fcategory);

		}else if(rConcern.is(':checked')){

			var fcategory = dCategoryCencern .find("option:selected").text();
			dCategoryCencern.off('change').on('change',function(){
				fcategory = dCategoryCencern .find("option:selected").text();
			});
			var fCategoryValue = $("#category-concern option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

			switch(fCategoryValue)
			{
				case "1" :

					var ftopic = dTopicBooking .find("option:selected").text();
					dTopicBooking.off('change').on('change',function(){
						ftopic = dTopicBooking .find("option:selected").text();
					});
					var ftopicValue = $("#topic-booking option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;

				case "2" :

					var ftopic = dTopicTravel .find("option:selected").text();
					dTopicTravel.off('change').on('change',function(){
						ftopic = dTopicTravel .find("option:selected").text();
					});
					var ftopicValue = $("#topic-travel option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBooking .find("option:selected").text();
							dSubtopicBooking.off('change').on('change',function(){
								 fsubtopic = dSubtopicBooking .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-booking option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicDuring .find("option:selected").text();
							dSubtopicDuring.off('change').on('change',function(){
								fsubtopic = dSubtopicDuring .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-during option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

							}
					break;

				case "3" :

					var ftopic = dTopicBaggage.find("option:selected").text();
					dTopicBaggage.off('change').on('change',function(){
						ftopic = dTopicBaggage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-baggage option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							dSubtopicBaggageBefore.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-before option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							dSubtopicBaggageAfter.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-after option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;

				case "4" :

					var ftopic = dTopicMembership .find("option:selected").text();
					dTopicMembership.off('change').on('change',function(){
						ftopic = dTopicMembership .find("option:selected").text();
					});
					var ftopicValue = $("#topic-membership option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							dSubtopicKrisflyer.off('change').on('change',function(){
								fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-krisflyer option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "4" :

							var fsubtopic = dSubtopicServices .find("option:selected").text();
							dSubtopicServices.off('change').on('change',function(){
								fsubtopic = dSubtopicServices .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-services option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;
			}

		}else if(rSuggestion.is(':checked')){

			var fcategory = dCategory .find("option:selected").text();
			dCategory.off('change').on('change',function(){
				fcategory = dCategory .find("option:selected").text();
			});
			var fCategoryValue = $("#category option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

		}else if(rQuestion.is(':checked')){

			var fcategory = dCategoryQuestion .find("option:selected").text();
			dCategoryQuestion.off('change').on('change',function(){
				fcategory = dCategoryQuestion .find("option:selected").text();
			});
			var fCategoryValue = $("#category-question option:contains('"+fcategory+"')").attr('value');
			categoryOfFeedback.val(fcategory);

			switch(fCategoryValue)
			{
				case "1" :

					var ftopic = dTopicQbooking .find("option:selected").text();
					dTopicQbooking.off('change').on('change',function(){
						 ftopic = dTopicQbooking .find("option:selected").text();
					});
					var ftopicValue = $("#topic-qbooking option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;

				case "2" :

					var ftopic = dTopicBaggage.find("option:selected").text();
					dTopicBaggage.off('change').on('change',function(){
						ftopic = dTopicBaggage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-baggage option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "1" :

							var fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							dSubtopicBaggageBefore.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageBefore .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-before option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "2" :

							var fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							dSubtopicBaggageAfter.off('change').on('change',function(){
								fsubtopic = dSubtopicBaggageAfter .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-baggage-after option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
							}
					break;

				case "3" :

					var ftopic = dTopicQmembership .find("option:selected").text();
					dTopicQmembership.off('change').on('change',function(){
						 ftopic = dTopicQmembership .find("option:selected").text();
					});
					var ftopicValue = $("#topic-qmembership option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);

					switch(ftopicValue){

						case "4" :

							var fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							dSubtopicKrisflyer.off('change').on('change',function(){
								fsubtopic = dSubtopicKrisflyer .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-krisflyer option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "6" :

							var fsubtopic = dSubtopicQualification .find("option:selected").text();
							dSubtopicQualification.off('change').on('change',function(){
								fsubtopic = dSubtopicQualification .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-qualification option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;

						case "8" :

							var fsubtopic = dSubtopicServices .find("option:selected").text();
							dSubtopicServices.off('change').on('change',function(){
								fsubtopic = dSubtopicServices .find("option:selected").text();
							});
							var fsubtopicValue = $("#sub-topic-services option:contains('"+fsubtopic+"')").attr('value');
							sTopicOfFeedback.val(fsubtopic);
							break;
						}
					break;

				case "4" :

					var ftopic = dTopicPackage .find("option:selected").text();
					dTopicPackage.off('change').on('change',function(){
						 ftopic = dTopicPackage .find("option:selected").text();
					});
					var ftopicValue = $("#topic-packages option:contains('"+ftopic+"')").attr('value');
					topicOfFeedback.val(ftopic);
					break;
				}
		}
		var question1 = $('[data-feedback-question="1"]').find(":visible").closest('label').text();
		var sixthques = feedBackquestion1.eq(6).find('label').text();
		var i = question1.indexOf("?");
		if(i > 0){
			var feedbackQuestion1 = question1.slice(0, i+1);
		 }
		if(question1 == sixthques){
			var feedbackans1 = $("#question-lose-item").find("option:selected").text();
		}else{
			var feedbackans1 = $('[data-feedback-question="1"]').find(":visible").find('input:radio:checked').attr('value');
		}
		$("#feedbackQuestion1").val(feedbackQuestion1);
		$("#ans1").val(feedbackans1);


		var question2 = $('[data-feedback-question="2"]').find(":visible").closest('label').text();
		var thirdques = feedBackQuestion2 .eq(3).find('label').text();
		var fifthques = feedBackQuestion2 .eq(5).find('label').text();
		var j = question2.indexOf("?");
		var k = question2.indexOf(".");
		if(j > 0){
			var feedbackQuestion2 = question2.slice(0, j+1);
		 }
		if(k > 0){
			var feedbackQuestion2 = question2.slice(0, k+1);
		 }
		if(question2 == thirdques){
			var feedbackans2 = $("#request").find("option:selected").text();
		}else if(question2 == fifthques){
			var feedbackans2 = $("#airports").find(':selected').val();
		}else{
			var feedbackans2 = $('[data-feedback-question="2"]').find(":visible").find('input:text').val();
		}
		$("#feedbackQuestion2").val(feedbackQuestion2);
		$("#ans2").val(feedbackans2);
	}

	var basicDetails = function() {

		if(lnSblock.hasClass("hidden")){

			var title = fBgender .find("option:selected").text();
			var lastName = userFbLastNAme.val();
			userTitle.val(title);
			userFamilyName.val(lastName);

		}else if(lnFblock.hasClass("hidden")){

			var title = sBgender .find("option:selected").text();
			var lastName = userSbLastName.val();
			userTitle.val(title);
			userFamilyName.val(lastName);
		}

		if(fnSblock.hasClass("hidden")){

			var firstName = userFbFirstName.val();
			userGivenName.val(firstName);

		}else if(fnFblock.hasClass("hidden")){

			var firstName = userSbFirstName.val();
			userGivenName.val(firstName);
		}
	}

	var initForm = function() {
		var limitCharEl = tellUsMore.parent('.textarea-2').siblings('.limit-character');
		var limitText = limitCharEl.text();
		var llimitText = $("#cleft").val();
		var controlKeys = {
			keys: [
				8, 9, 16, 18, 19, 20, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45,	46, 91, 92,
				93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123,	144, 145
			],
			getKey: function(key) {
				return this.keys.indexOf(key);
			}
		};

		var loggedUser = $("#loggedUser").val();
		if(loggedUser == "true")
		{
			var fnAvailable = $("#fnAvailable").val();
			if(fnAvailable == '')
			{
				disabledChb.prop('checked', true);
				disabledChb.prop('disabled', true);
			}
			else
			{
				disabledChb.prop('checked', false);
				disabledChb.prop('disabled', true);
			}
		var phNoCountryCode = $("#phNoCountry").val();
		var phNoCountry=$('#pre-country-code-label').val(phNoCountryCode).find(':selected').data('text');
		$("#pre-country-code").val(phNoCountry);

		var countryOfResidenceCode = $("#countryOfRes").val();
		var countryOfResidence=$('#country-label').val(countryOfResidenceCode).find(':selected').data('text');
		$("#country").val(countryOfResidence);

		}
		else
		{
			disabledChb
			.off('change.disableName')
			.on('change.disableName', function() {
				var self = $(this);
				var wrap = self.closest('[data-order]');
				var sibl = wrap.find('input:text');

				if(self.is(':checked')){
					sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
					wrap.find('.text-error').remove();
					wrap.find('.error').removeClass('error');
				}
				else{
					sibl.prop('disabled', false).closest('span').removeClass('disabled');
				}
			});
		}

		tellUsMore
			.off('keyup.feedBack')
			.on('keyup.feedBack', function(e) {
				var key = e.which || e.keyCode;
				var vlue = tellUsMore.val();
				var breakLine = (vlue.match(/\n/g)||[]).length;

				if (vars.isIE() && vars.isIE() <= 8) {
					breakLine = 0;
				}

				var length = vlue.length;
				var charLeft = maxLength - (length + breakLine);
				var charText = '';
				charText = charLeft <= 0 ?
				'0' : charLeft === 1 ?
				'1' : window.accounting.formatNumber(charLeft, false);

				if(length + breakLine >= maxLength) {
					if (controlKeys.getKey(key) < 0) {
						if(key === 13 && charLeft === -1) {
							if (vars.isIE() && vars.isIE() <= 8) {
								breakLine = 1;
							}
							tellUsMore.val(vlue.slice(0, maxLength - breakLine));
							limitCharEl.empty();
							limitCharEl.text('1'+' '+llimitText);
						}
						else {
							if (!e.ctrlKey) {
								tellUsMore.val(vlue.slice(0, maxLength - breakLine));
							}
							else {
								tellUsMore.val(vlue.slice(0, maxLength));
							}
							limitCharEl.empty();
							limitCharEl.text('0'+' '+llimitText);
						}
					}
				}
				else {
					if(charLeft <=1){
						limitCharEl.empty();
						limitCharEl.text(charText+' '+llimitText);
					}else{
						limitCharEl.empty();
						limitCharEl.text(charText+' '+limitText);
				}
				}
			})
			.off('keydown.feedBack')
			.on('keydown.feedBack', function(e) {
				var key = e.keyCode || e.which;
				if (controlKeys.getKey(key) < 0) {
					var vlue = tellUsMore.val();
					var breakLine = (vlue.match(/\n/g)||[]).length;
					if (vars.isIE() && vars.isIE() <= 8) {
						breakLine = 0;
					}
					var length = vlue.length;

					if(length + breakLine >= maxLength) {
						return false;
					}
				}
			})
			.trigger('keyup.feedBack');

		disabledChb
			.off('change.disableName')
			.on('change.disableName', function() {
				var self = $(this);
				var wrap = self.closest('[data-order]');
				var sibl = wrap.find('input:text');

				if(self.is(':checked')){
					sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
					wrap.find('.text-error').remove();
					wrap.find('.error').removeClass('error');
				}
				else{
					sibl.prop('disabled', false).closest('span').removeClass('disabled');
				}
			});

		/*form.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});*/
		form.off('submit.feedBack').on('submit.feedBack', function() {
			if (!form.valid()) {
				var orderEl = $('input.error').closest('[data-order]').eq(0);
				if (orderEl.length) {
					$('html,body').animate({
						scrollTop: orderEl.offset().top
					}, 400);
				}
			}
			var countryText = $("#country").val();
			var countryResCode = $("#country-label option:contains('"+countryText+"')").attr('value');
			$("#countryOfResCode").val(countryResCode);
			var language = $('input[name=locale]:checked').val();
			$("#language").val(language);
			basicDetails();
			typeOfFeedBack();
			mandatoryErrors();

		});
	};

	var initModule = function() {
		initForm();
		// SIA.initAutocompleteCity();
	};

	initModule();

	/* jshint ignore:end */
};
