<% if(data) {%>
  <% if(data.commonAddons) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
          <h3 class="title-4--blue main-title">Trip-related add-ons</h3>
          <h3 class="title-5--dark sub-total">Sub-total <%- data.currency %> <span>893.80</span></h3>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
          <% if(flightCommonAddons.type == "Travel Insurance") {%>
            <div class="trip-related">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-aig-logo.png" alt="Travel insurance" longdesc="img-desc.html"/>
              </div>
              <ul class="trip-related-details">
                <li><span class="name">Person covered</span><span class="content" data-remove-last-comma><% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %><%- flightPaxDetails.paxName %>, <% }); %></span></li>
                <li><span class="name">Travel location</span><span class="content"><%- flightCommonAddons.travelLocation %></span></li>
                <li><span class="name">Date range</span><span class="content"><%- flightCommonAddons.description %></span></li>
              </ul>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Hotel") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-agoda-logo.png" alt="Hotel" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title">
                  <h4 class="title-4--blue"><%- flightCommonAddons.name %></h4>
                  <p class="des"><%- flightCommonAddons.address %></p>
                </div>
                <div class="trip-thumb"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details">
                  <li><span class="name">Check-in date:</span><span class="content"><%- flightCommonAddons.checkInDate %></span></li>
                  <li><span class="name">Check-out date:</span><span class="content"><%- flightCommonAddons.checkOutDate %></span></li>
                  <li><span class="name">Guests:</span><span class="content"><%- flightCommonAddons.guest %></span></li>
                  <% _.each(flightCommonAddons.rooms, function(flightCommonAddonsRooms, idxRoom){ %>
                    <li><span class="name">Room types:</span><span class="content"><%- flightCommonAddonsRooms.roomType %></span></li>
                    <li><span class="name">No. of nights:</span><span class="content"><%- flightCommonAddonsRooms.numberOfNights %></span></li>
                    <% if(flightCommonAddonsRooms.breakfast == true) {%>
                      <li><span class="name">Breakfast:</span><span class="content">Included for all rooms</span></li>
                    <% } %>
                  <% }); %>
                </ul>
              </div>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Car Rental") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-rentalcars-logo.png" alt="Car rental" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title sub-title-inline">
                  <h4 class="title-4--blue"><%- flightCommonAddons.carType %></h4>
                  <p class="des">or similar</p>
                </div>
                <div class="trip-thumb trip-thumb-inline"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details shorter-width">
                  <li><span class="name">Pick up:</span><span class="content"><%- flightCommonAddons.pickupDate %></span></li>
                  <li><span class="name">Drop off:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.pickupLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                </ul>
              </div>
            </div>
          <% } %>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total has-link shorter-width hidden">
          <div data-tabindex="true" class="flights-cost flights-fax">
            <h4 class="flights-cost-title"><span class="text-left">ADD-ONS</span><span class="text-right">SGD</span></h4>
            <ul class="flights-cost__details">
              <% var subTotalCommon,
                  amountHotel, amountTravelInsurance,
                  amountCarRental;
              %>
              <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
                <% if(flightCommonAddons.type == "Travel Insurance") {%>
                  <li><span><%- flightCommonAddons.type %></span><a href="#" class="link-4 link-remove"><em class="ico-point-r"></em>Remove</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountTravelInsurance = flightCommonAddons.amount %>
                  </li>
                <% } %>
              <% }); %>
              <%
                subTotalCommon = amountHotel + amountTravelInsurance + amountCarRental;
              %>
              <li class="sub-total"><span>Sub-total</span><span class="price"><%- subTotalCommon.toFixed(2) %></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <% } %>
<% } %>
