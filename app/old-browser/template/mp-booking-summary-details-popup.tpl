<% if(data) { %>
  <%
    var checkBodyClass = function (classList) {
      var classStatus = false;
      _.each(classList, function (className) {
        if ($('body').hasClass(className)) {
          classStatus = true;
        }
      });
      return classStatus;
    };
  %>
  <h2 data-anchor="cost" class="popup__heading">Cost breakdown by passenger</h2>

  <h3 class="sub-heading-3--dark">Flight</h3>
  <div class="flights-target">
    <% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
      <table class="table-cost table-addons-mp">
        <thead>
          <tr class="name-passenger">
            <th>
              <span class="text-bold-1">Passenger <%= (paxIdx + 1) %> - <%= pax.paxName %></span>
            </th>
            <th class="hidden-mb">
              <span class="detail">SGD</span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr class="first">
            <td>
              <span class="text-bold indent">Fare (<%= pax.paxType %>)</span>
              <span class="detail detail-1 hidden-mb">SGD</span>
            </td>
            <td data-need-format="2"><%= pax.fareDetails.fareAmount.toFixed(2) %></td>
          </tr>
          <tr class="row-title">
            <td colspan="2">
              <span class="text-bold indent">Airport / Government taxes:</span>
            </td>
          </tr>
          <% _.each(pax.fareDetails.taxes, function(tax, taxIdx) { %>
            <tr>
              <td class="indent"><%= tax.description %> (<%= tax.code %>)</td>
              <td data-need-format="2"><%= tax.amount.toFixed(2) %></td>
            </tr>
          <% }); %>
          <% if(pax.fareDetails.surcharge) { %>
            <tr class="row-title">
              <td colspan="2">
                <span class="text-bold indent">Carrier Surcharges:</span>
              </td>
            </tr>
          <% } %>
          <% _.each(pax.fareDetails.surcharge, function(charge, chargeIdx) { %>
              <tr>
                <% if(typeof(charge.code) == 'number') { %>
                  <td class="indent"><%= charge.description %> (<%= charge.code.toFixed(2) %>)</td>
                <% } else { %>
                  <td class="indent"><%= charge.description %> (<%= charge.code %>)</td>
                <% } %>
                <td data-need-format="2"><%= charge.amount.toFixed(2) %></td>
              </tr>
          <% }); %>
          <% if(pax.detailsPerFlight) { %>
            <%
              var seat = 0, excessBaggage = 0, meal = 0;
              _.each(pax.detailsPerFlight, function(detail, detailIndex) {
                _.each(detail.addonPerPax, function(paxAdd, paxAddIdx) {
                  if(paxAdd.amount) {
                    if(paxAdd.type == 'Seat') {
                      seat += paxAdd.amount.toFixed(2);
                    }
                    else if (paxAdd.type == 'Excess baggage') {
                      excessBaggage += paxAdd.amount.toFixed(2);
                    }
                    else if (paxAdd.type == 'Meal') {
                      meal += paxAdd.meal.toFixed(2);
                    }
                  }
                });
              });
            %>
            <% if (seat > 0) { %>
              <tr class="row-title">
                <td>
                  <span class="text-bold">Preferred Seat</span>
                  <span class="detail">
                    <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                  </span>
                  <span class="detail detail-1">SGD</span>
                </td>
                <td data-need-format="2"><%= seat %></td>
              </tr>
            <% } %>
            <% if (excessBaggage > 0) { %>
              <tr class="row-title">
                <td>
                  <span class="text-bold">Excess baggage</span>
                  <span class="detail">
                    <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                  </span>
                  <span class="detail detail-1">SGD</span>
                </td>
                <td data-need-format="2"><%= excessBaggage %></td>
              </tr>
            <% } %>
            <% if (meal > 0) { %>
              <tr class="row-title">
                <td>
                  <span class="text-bold">Meal</span>
                  <span class="detail">
                    <!-- San Francisco to Singapore – 21 Feb 2015 (Sat) -->
                  </span>
                  <span class="detail detail-1">SGD</span>
                </td>
                <td data-need-format="2"><%= meal %></td>
              </tr>
            <% } %>
          <% } %>
          <tr class="first"></tr>
        </tbody>
        <tfoot>
          <tr>
            <td>
              <span class="text-bold">Sub-total</span>
              <span class="detail detail-1 hidden-mb">SGD</span>
            </td>
            <td class="total-price" data-need-format="2">
              <%= (pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount).toFixed(2) %>
            </td>
          </tr>
        </tfoot>
      </table>
    <% }); %>
    <% if (checkBodyClass(['review-refund-page', 'review-atc-pay', 'atc-payment-page'])) { %>
      <table class="table-cost">
        <thead>
            <tr class="name-passenger">
              <th>
                <span class="text-name-passenger">All passengers</span>
              </th>
              <th class="hidden-mb">
                <span class="detail">SGD</span>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="first">
              <td>
                <span class="text-bold">Rebooking fee</span>
                <span class="detail detail-1 hidden-mb">SGD</span>
              </td>
              <td ><%= data.bookingSummary.rebookingFee.toFixed(2) %></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td>
                <span class="text-bold">Sub-total</span>
                <span class="detail detail-1 hidden-mb">SGD</span>
              </td>
              <td class="total-price" data-need-format="2">
                <%= data.bookingSummary.rebookingFee.toFixed(2) %>
              </td>
            </tr>
          </tfoot>
      </table>
      <table class="table-cost has-2-cols">
        <thead>
            <tr class="name-passenger">
              <th>
                <span class="text-name-passenger">Previously paid</span>
              </th>
              <th>
                <span class="detail">KrisFlyer miles</span>
              </th>
              <th class="hidden-mb">
                <span class="detail">SGD</span>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="first">
              <td>
                <span class="text-bold">Fares</span>
              </td>
              <td ><%= data.bookingSummary.oldItinerary.costPaidByMiles.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",") %></td>
              <td ><%= data.bookingSummary.oldItinerary.costPaidByCash.toFixed(2) %></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td>
                <span class="text-bold">Sub-total</span>
              </td>
              <td class="total-price" data-need-format="2">
                <%= data.bookingSummary.oldItinerary.costPaidByMiles.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",") %>
              </td>
              <td class="total-price" data-need-format="2">
                <%= data.bookingSummary.oldItinerary.costPaidByCash.toFixed(2) %>
              </td>
            </tr>
          </tfoot>
      </table>
    <% } %>
  </div>

  <h3 class="sub-heading-3--dark">Flight add-ons</h3>
  <div class="flights-target">
    <% _.each(data.bookingSummary.paxDetails, function (pax, paxIdx) {%>
      <table class="table-cost table-addons-mp">
        <thead>
          <tr>
            <th>
              <span class="text-bold-1">PASSENGER <%- paxIdx + 1%> - <%- pax.paxName%></span>
            </th>
            <th class="hidden-mb">
              <span class="detail">SGD</span>
            </th>
          </tr>
        </thead>
        <tbody>
          <% var paxAddOnsTotal = 0 %>
          <% _.each(pax.detailsPerFlight, function (flightDetails) { %>
            <% _.each(flightDetails.addonPerPax, function (paxAddOn, addOnIdx) { %>
            <% paxAddOnsTotal += paxAddOn.amount %>
            <tr class="first">
              <td>
                <span class="text-bold indent">
                <% if (paxAddOn.isPackService) { %>
                  <%- paxAddOn.packDescription %>
                <% } else { %>
                  <%- paxAddOn.seatType %>
                <% } %>
                </span>
              </td>
            </tr>
            <tr class="first">
              <td>
                <span class="indent"><%- flightDetails.originCityName%> to <%- flightDetails.destinationCityName%> - <%- flightDetails.formattedDepDate%></span>
              </td>
              <td data-need-format="2"><%- paxAddOn.amount.toFixed(2) %></td>
            </tr>
            <tr class="first"></tr>
            <% }); %>
          <% }); %>
        </tbody>
        <tfoot>
          <tr>
            <td>
              <span class="text-bold">Sub-total</span>
              <span class="detail detail-1">SGD</span>
            </td>
            <td class="total-price" data-need-format="2"><%- paxAddOnsTotal.toFixed(2) %></td>
          </tr>
        </tfoot>
      </table>
    <% }); %>
  </div>

  <h3 class="sub-heading-3--dark">Trip add-ons</h3>
  <div class="flights-target">
      <table class="table-cost table-addons-mp trip-addons">
        <thead>
          <tr>
            <th>
              <span class="text-bold-1"></span>
            </th>
            <th class="hidden-mb">
              <span class="detail">SGD</span>
            </th>
          </tr>
        </thead>
        <tbody>
        <% var commonAddOnsTotal = 0; %>
        <% _.each(data.bookingSummary.commonAddons, function (commonAddOn) { %>
          <% commonAddOnsTotal += commonAddOn.amount %>
          <% if (commonAddOn.type.toLowerCase() == "travel insurance") { %>
          <tr class="first">
            <td>
              <span class="text-bold">Travel insurance</span>
            </td>
          </tr>
          <tr class="first">
            <td>
              <span class=""><%- commonAddOn.description %></span>
            </td>
            <td data-need-format="2"><%- commonAddOn.amount.toFixed(2) %></td>
          </tr>
          <% } else if (commonAddOn.type.toLowerCase() == "hotel") { %>
          <tr class="row-title">
            <td colspan="2">
              <span class="text-bold ">Hotel</span>
            </td>
          </tr>
          <tr>
            <td class=""><%- commonAddOn.name %></td>
          </tr>
          <tr>
            <td class=""><%- commonAddOn.description %></td>
            <td data-need-format="2"><%- commonAddOn.amount.toFixed(2) %></td>
          </tr>
          <% } else if (commonAddOn.type.toLowerCase() == "car rental") { %>
          <tr class="row-title">
            <td colspan="2">
              <span class="text-bold ">Car</span>
            </td>
          </tr>
          <tr>
            <td class=""><%- commonAddOn.description %></td>
            <td data-need-format="2"><%- commonAddOn.amount.toFixed(2) %></td>
          </tr>
          <% } %>
        <% }); %>
          <tr class="first"></tr>
        </tbody>
        <tfoot>
          <tr>
            <td>
              <span class="text-bold">Sub-total</span>
            </td>
            <td class="total-price" data-need-format="2"><%- commonAddOnsTotal.toFixed(2) %></td>
          </tr>
        </tfoot>
      </table>
  </div>

  <div class="grand-total">
    <% if(checkBodyClass(['review-refund-page'])) { %>
      <span class="total-title">Total to be refunded</span>
    <% } else if(checkBodyClass(['review-atc-pay', 'atc-payment-page'])) { %>
      <span class="total-title">Total to be paid</span>
    <% } else { %>
      <span class="total-title">Grand total payable</span>
    <% } %>
    <p class="total-info">
      <span>
        <% if(data.bookingSummary.costPayableByMiles) { %>
          <span data-need-format="0"><%= data.bookingSummary.costPayableByMiles %></span> miles +&nbsp;
        <% } %>
        SGD
          <% if(checkBodyClass(['review-refund-page'])) { %>
            <span data-need-format="2">
              <%= data.bookingSummary.cashToBeRefunded.toFixed(2) %>
              <% if(data.bookingSummary.milesToBeRefunded > 0) { %>
                + <t data-replace-miles>
                  <% if(data.bookingSummary.milesToBeRefunded < 1000) { %>
                  <%- data.bookingSummary.milesToBeRefunded %>
                  <% } else { %>
                  <%- data.bookingSummary.milesToBeRefunded.toLocaleString() %>
                  <% } %>
                  </t> KrisFlyer miles
              <% } %>
            </span>
          <% } else if(checkBodyClass(['review-atc-pay', 'atc-payment-page'])) { %>
            <span data-need-format="2">
              <%= data.bookingSummary.totalToBePaidCash.toFixed(2) %>
              <% if(data.bookingSummary.totalToBePaidMiles > 0) { %>
                +
                <% if(data.bookingSummary.totalToBePaidMiles <1000) { %>
                <%- data.bookingSummary.totalToBePaidMiles %>
                <% } else {%>
                <%- data.bookingSummary.totalToBePaidMiles.toLocaleString() %>
                <% } %>
                  KrisFlyer miles
              <% } %>
            </span>
          <% } else { %>
            <span data-need-format="2">
              <%= data.bookingSummary.costPayableByCash.toFixed(2) %></span>
          <% } %>
      </span>
    </p>
  </div>
<% } %>
