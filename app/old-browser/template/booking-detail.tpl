<% if (data && data.meals && data.meals.sectorMealSelectedInfo && data.meals.sectorMealSelectedInfo.length) { %>
  <div class="booking-details booking-details--2">
    <div class="booking-col col-1">
      <em class="ico-food"></em>
    </div>
    <div class="booking-col col-2">
      Meals
    </div>
    <div class="booking-col col-3">
      <div class="align-wrapper">
        <div class="align-inner">
          <% _.each(data.meals.sectorMealSelectedInfo, function(meal, mealIdx) { %>
            <div class="has-cols">
              <p class="target-info"><%- meal.origin %> TO <%- meal.destination %></p>
              <p class="seat-info">
                <strong><%- meal.type %>: </strong>
                <%- meal.mealName %>
              </p>
            </div>
          <% }); %>
      </div>
      </div>
    </div>
    <div class="booking-col col-4">
      <% if(data.paxId && data.meals && data.meals.departureCityCode && data.meals.departureTime && data.meals.flightNumber) { %>
        <% if (data.meals.eligible && data.meals.eligible === 'true') { %>
          <a href="/meals/?paxId=<%- data.paxId %>&departureCityCode=<%- data.meals.departureCityCode %>&departureDate=<%- data.meals.departureDate %>&flightNumber=<%- data.meals.flightNumber %>">
            <em class="ico-point-r"></em>Select / Change
          </a>
        <% } else { %>
          <a class="disabled" href="javascript:void(0);">
            <em class="ico-point-r"></em>Select / Change
          </a>
        <% } %>
      <% } else { %>
        <a href="#">
          <em class="ico-point-r"></em>Select / Change
        </a>
      <% } %>
    </div>
  </div>
<% } %>
<% if (data && data.baggages && data.baggages.baggageDetails && data.baggages.baggageDetails.length ) { %>
    <% if (data.baggages.baggageDetails.length === 1) { %>
      <div class="single-line booking-details booking-details--2">
    <% } else { %>
      <div class="booking-details booking-details--2">
    <% } %>
    <div class="booking-col col-1">
      <em class="ico-business-1"></em>
    </div>
    <div class="booking-col col-2">
      Baggage
    </div>
    <div class="booking-col col-3">
      <div class="align-wrapper">
        <div class="align-inner">
          <% _.each(data.baggages.baggageDetails, function(baggage, bagIdx) { %>
            <p>
              <strong><%- baggage.baggageType %>: </strong>
              <%- baggage.name %>
            </p>
          <% }); %>
        </div>
      </div>
    </div>
    <div class="booking-col col-4">
      <% if (data.paxId && data.baggages && data.baggages.segmentID) { %>
        <% if (data.baggages.eligible && data.baggages.eligible === 'true') { %>
          <p>
            <a href="/add-ons/?paxId=<%- data.paxId %>&segmentId=<%- data.baggages.segmentID %>"><em class="ico-point-r"></em>Add / Change</a>
          </p>
          <p>
            <a href="/add-ons/?paxId=<%- data.paxId %>&segmentId=<%- data.baggages.segmentID %>&cancel=1" data-trigger-popup=".popup--cancel-additional-baggage">
              <em class="ico-point-r"></em>Cancel
            </a>
          </p>
        <% } else { %>
          <p>
            <a class="disabled" href="javascript:void(0);"><em class="ico-point-r"></em>Add / Change</a>
          </p>
          <p>
            <a class="disabled" href="javascript:void(0);" data-trigger-popup=".popup--cancel-additional-baggage">
              <em class="ico-point-r"></em>Cancel
            </a>
          </p>
        <% } %>
      <% } else { %>
        <p>
          <a href="#"><em class="ico-point-r"></em>Add / Change</a>
        </p>
        <p>
          <a href="#" data-trigger-popup=".popup--cancel-additional-baggage">
            <em class="ico-point-r"></em>Cancel
          </a>
        </p>
      <% } %>
    </div>
  </div>
<% } %>
