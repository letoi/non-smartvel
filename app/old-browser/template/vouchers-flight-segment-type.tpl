<div class="block-2 block-2__main block__flight--segment" id="<%-id %>">
  <h3 class="title-5--blue">Select flight segment</h3>
  <div class="form-group grid-row">
    <label for="select-flight-segment" class="hidden">select-flight-segment</label>
    <div class="grid-col full">
      <div class="grid-inner">
        <div data-customselect="true" class="custom-select custom-select--2 one-half left" data-url="<%- checkAvailUrl %>">
          <label for="select-flight-segment" class="select__label"><span class="ui-helper-hidden-accessible">Label</span>
          </label><span class="select__text">1. Singapore to San Francisco</span><span class="ico-dropdown">1. Singapore to San Francisco</span>
          <select id="select-flight-segment" name="select-flight-segment">
            <% if(data && data.length) {%>
              <% _.each(data, function(segment, idx) { %>
                <% if (segment.segmentId) {%>
                  <% if (segment.itinerary.listSegment.length) {%>
                    <option value="<%- segment.segmentId %>"><%- (segment.index || idx) + '. ' +segment.segmentDropDownValue + ' - ' + segment.itinerary.listSegment[0].fromCity.departuteDate%></option>
                  <% }%>
                <% } else { %>
                  <option value="" disabled selected><%- segment.segmentDropDownValue %></option>
                <% }%>
              <% }); %>
            <% } else {%>
              <option value="" disabled selected>Select</option>
            <% }%>
          </select>
        </div>
        <input type="button" name="remove-flight" id="remove-flight" value="remove voucher" class="btn-7 left custom-button hidden"/>
      </div>
    </div>
  </div>
  <div data-flight-details="true" class="flight-segment-detail"></div>
  <div class="add-more-cta hidden">
    <p>Would you like to apply more vouchers on other flights segment?</p>
    <input type="button" name="more-flight" id="more-flight" value="Add more flight segment" class="btn-1" tabindex="0"/>
  </div>
</div>
