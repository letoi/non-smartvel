<% if(data !== void 0 && data.length > 0){ %>
  <% _.each(data, function(fh, idx) { %>
      <tr class="<%- idx > 9 ? 'hidden' : '' %> <%- idx%2 === 0 ? 'odd' : 'even' %>">
        <td>
          <div class="data-title">Departure date</div><span><%- fh.departureDate %></span>
        </td>
        <td>
          <div class="data-title">Details</div><span><%- fh.from %> to <%- fh.to %><br><%- fh.flightNo %></span>
        </td>
        <td>
          <div class="data-title">Booking Reference</div><span><%- fh.referenceNumber %></span>
        </td>
        <td>
          <div class="data-title">Class</div><span><%- fh.className %></span>
        </td>
        <td>
          <div class="data-title">Booked by</div><span><%- fh.bookedBy %></span>
        </td>
      </tr>
  <% }) %>
<% } %>
