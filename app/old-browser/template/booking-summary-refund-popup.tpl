<%
  var subTotal = 0;

  subTotal = _.reduce(data.sshPackageDetails, function(sum, packageObj){
    return sum + packageObj.sshPrice
  }, 0);
%>
<h2 data-anchor="cost" class="popup__heading">Cost breakdown</h2>
<div class="flights-target flights-target--refund">
  <table class="table-cost ">
  <% _.each(data.sshPackageDetails, function(packageDetails){ %>
      <tbody>
        <tr class="row-title">
          <td>
            <span class="text-bold"><%= packageDetails.sshPkgName %></span>
          </td>
          <td>
            <span><%= data.currency %></span>
          </td>
        </tr>
        <tr>
          <td><%= packageDetails.mainDescription %><br /><%= packageDetails.period %></td>
          <td><%= packageDetails.sshPrice.toLocaleString('en', { minimumFractionDigits: 2 }) %></td>
        </tr>
      </tbody>
  <% }) %>
    <tr class="row-title">
      <td></td>
    </tr>
    <tfoot>
      <tr>
        <td>
          <span class="text-bold">Sub-total</span>
        </td>
        <td class="total-price"><%= subTotal.toLocaleString('en', { minimumFractionDigits: 2 }) %></td>
      </tr>
    </tfoot>
  </table>
  <div class="grand-total">
    <span class="total-title">Total to be refunded</span>
    <p class="total-info">
      <span>
        <%= data.currency %> <span><%= data.totalToBeRefunded.toLocaleString('en', { minimumFractionDigits: 2 }) %></span>
      </span>
    </p>
  </div>
</div>
