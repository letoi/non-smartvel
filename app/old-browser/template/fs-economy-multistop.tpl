<%var curCode=currencyCode;%>
<% if(data) { %>
  <%
    fareFamiliesGroup = [],  familiesCabinGroup = {}, arrWrap = [];
    _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) {
      familiesCabinGroup[fareFamilies.cabinClassName] = fareFamilies.cabinClassName;
    });
  %>

  <%
    function groupBy(arr, key) {
      var newArr = [],
          family = {},
          newItem, i, j, cur;
      for (i = 0, j = arr.length; i < j; i++) {
          cur = arr[i];
           if (!(cur[key] in family)) {
              family[cur[key]] = { type: cur[key], data: [] };
              newArr.push(family[cur[key]]);
          }
          family[cur[key]].data.push(cur);
      }
      return newArr;
    }
  %>
  <% var arr = [] %>
  <% _.each(data.flights, function(flights, flightIdx){ %>
    <% var newArray = [] %>
        <% _.each(flights.segments, function(segments, flightSegmentIdx){ %>
          <% var newArr1 = [] %>
              <% _.each(data.recommendations, function(recommendation, reIdx){ %>
                <% _.each(data.fareFamilies, function(fareFamiliesGroup, fareFamiliesGroupIdx){ %>
                  <% _.each(recommendation.segmentBounds[flightIdx].segments, function(segmentItem, segmentItemIdx){ %>
                    <% if(segmentItem.segmentID === segments.segmentID && fareFamiliesGroup.fareFamily == recommendation.segmentBounds[flightIdx].fareFamily ) { %>
                      <% newArr1.push({"family" : recommendation.segmentBounds[flightIdx].fareFamily,"familyGroup" : fareFamiliesGroup.cabinClassName, "price" : recommendation.fareSummary.fareDetailsPerAdult.totalAmount, "recommendationId" : recommendation.recommendationID, "displayLastSeat" : segmentItem.displayLastSeat, "numOfLastSeats" : segmentItem.numOfLastSeats }) %>
                      <% return false %>
                    <% } %>
                  <% }) %>
                <% }) %>
              <% }); %>
              <% newArray[segments.segmentID] =  newArr1 %>
        <% }); %>
        <% arr.push(newArray) %>
  <% }); %>
  <%
    _.each(arr, function(arr1, idx1){
      _.each(arr1, function(arr2, idx2){
        arr[idx1][idx2] = groupBy(arr2, 'family');
      })
    });
  %>

  <p class="says"><%=labels.tabKey%></p>
  <div class="wrap-content-list">
    <% _.each(data.flights, function(flights, flightIdx) { %>
      <div class="wrap-content-fs">
      <input type="hidden" name="selectedFlightId<%- flightIdx %>" id="selectedFlightId<%- flightIdx %>" class="selectedFlightsIds" value=""/>
        <ul class="status-list fs-status-list">
          <li><span class="status status-lowest"><span class="label-status"></span></span><%=labels.lowestFare%></li>
        </ul>
        <% if( flightIdx === 0 ) { %>
          <p id="table-caption__1" class="says"><%=labels.outbound%></p>
        <% } else if ( flightIdx === 1 ) { %>
          <p id="table-caption__1" class="says"><%=labels.inbound%></p>
        <% } %>
        <div class="slider-group">
            <div class="title-slider">
              <h3 class="sub-heading-3--dark">
              <% _.each(data.airports, function(airports, airportsIdx) { %>
                <% if( airports.airportCode === flights.originAirportCode ) { %>
                  <%- flightIdx + 1 %>. <%- airports.cityName %>
		  <input type="hidden" name="origin-city-code" id="origin-city-code" class="origin-city-code" value="<%-airports.cityName%>-<%-airports.airportCode%>"/> 
			<input type="hidden" name="boardPointOrigin" id="boardPointOrigin" class="boardPointOrigin" value="<%-airports.airportCode%>"/>		
		 <input type="hidden" name="countryOrigin" id="countryOrigin" class="countryOrigin" value="<%-airports.countryCode%>"/>		
		  <% if( flightIdx === 0 ) { %>
		   <input type="hidden" name="first-origin-city-code" id="first-origin-city-code" class="origin-city-code" value="<%-airports.cityName%>-<%-airports.airportCode%>"/> 
			<input type="hidden" name="firstBoardPointOrigin" id="firstBoardPointOrigin" class="boardPointOrigin" value="<%-airports.airportCode%>"/>		
		 <input type="hidden" name="firstCountryOrigin" id="firstCountryOrigin" class="countryOrigin" value="<%-airports.countryCode%>"/>		
		 <% } %>
		  
                <% } %>
              <% }); %> to
              <% _.each(data.airports, function(airports, airportsIdx) { %>
                <% if( airports.airportCode === flights.destinationAirportCode ) { %>
                  <%- airports.cityName %>
		  <input type="hidden" name="destination-city-code" id="destination-city-code" class="destination-city-code" value="<%-airports.cityName%>-<%-airports.airportCode%>"/>
			<input type="hidden" name="boardPointDestination" id="boardPointDestination" class="boardPointDestination" value="<%-airports.airportCode%>"/>
		  <input type="hidden" name="countryDestination" id="countryDestination" class="countryDestination" value="<%-airports.countryCode%>"/>	
		    <% if( flightIdx === 0 ) { %>
		     <input type="hidden" name="first-destination-city-code" id="first-destination-city-code" class="destination-city-code" value="<%-airports.cityName%>-<%-airports.airportCode%>"/>
			<input type="hidden" name="firstBoardPointDestination" id="firstBoardPointDestination" class="boardPointDestination" value="<%-airports.airportCode%>"/>
		  <input type="hidden" name="firstCountryDestination" id="firstCountryDestination" class="countryDestination" value="<%-airports.countryCode%>"/>	
		   <% } %>
		  
                <% } %>
              <% }); %>              
              </h3>            
            </div>       
            <div class="sub-logo">
				<a href="<%=actionUrl%>&amp;_eventId=calendarSearchEvent" class="calendar-link mb-calendar-fare launchLightBoxIgnore" title="See 7 day fare" tabindex="0" aria-label="calendar			
				See 7-day fares
				"><em class="ico-date"><span class="ui-helper-hidden-accessible"><%=labels.calendar%></span></em>
				<%=labels.see%> <span class="seeDates">				
				 <% if(seeDaysFare !== 'undefined' && seeDaysFare === 'mobile' ) { %>
				  <%=labels.three%>
				<% } else if (seeDaysFare !== 'undefined' && seeDaysFare === 'tablet' ) { %>
				  <%=labels.five%>				
				<% } else { %>
				<%=labels.seven%>
				<% } %>
				</span><%=labels.dayFares%>
				</a>
			  <img src="saar5/images/svg/tr.svg" alt="SC Logo" longdesc="img-desc.html">
			  <img src="saar5/images/svg/si.svg" alt="SI Logo" longdesc="img-desc.html">
              <img src="saar5/images/svg/sq.svg" alt="SQ Logo" longdesc="img-desc.html">    
              <span class="text"><%=labels.logo%></span>
              <span class="ico-right">
                <em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2'&gt;<%=labels.toolTipMsg%>&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em>
              </span>
            </div>
        </div>
        <% if(flights.segments.length > 1 && filterArr && filterArr.length) { %>
          <% var filter = filterArr[flightIdx] %>
          <div class="flight-search-filter-economy" data-flight-filter="<%- flightIdx %>">
              <% if (data.isMobile) { %>
                <div class="sort-dropdown-container">
                  <span class="sub-menu-icon rotate-down">
                      <em class="ico-point-r"></em>
                    </span>
                  <div class="custom-select custom-select--2 sort-by">
                    <span class="select__text">
                      <div class="select__title">
                        SORT BY:
                      </div>
                      <div class="select__body">
                        TRAVEL DURATION
                      </div>
                    </span>
                    <label for="sort-select-mobile">&nbsp;
                      <select id="sort-select-mobile">
                        <option value="travel duration" selected>TRAVEL DURATION</option>
                        <option value="price">PRICE</option>
                        <option value="arrival time">ARRIVAL TIME</option>
                        <option value="departure time">DEPARTURE TIME</option>
                      </select>
                    </label>
                  </div>
                </div>
              <% } else { %>
              <div class="sort-filter">
                  <span class="sort-by">SORT BY:</span>
                  <a href="#" class="travel-duration sort-active">TRAVEL DURATION</a>
                  <a href="#" class="sort-price">PRICE</a>
                  <a href="#" class="arrival-time">ARRIVAL TIME</a>
                  <a href="#" class="departure-time">DEPARTURE TIME</a>
              </div>
              <% } %>
            <div class="content">
              <div class="heading-filter">
                <div class="title"><%=labels.filters%></div>
                <div class="link-hide"><a href="#" class="link-4"><em class="ico-point-r">&nbsp;</em><span class="text"><%=labels.hideFilter%></span></a>
                </div>
              </div>
              <div class="block-1">
                <div class="content-filter-search">
                  <div class="content-inner">
                    <div class="left-content">
                      <p class="title-6--dark title"><%=labels.stopOver%></p>
                      <ul class="list">
                        <% if(filter.nonStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="non-stop-<%= flightIdx %>" id="non-stop-<%= flightIdx %>" type="checkbox" checked="true" type="checkbox" aria-label="non-stop">
                              <label for="non-stop-<%= flightIdx %>"><%=labels.non_Stop%></label>
                            </div>
                          </li>
                        <% } %>
                        <% if(filter.oneStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="one-stop-<%= flightIdx %>" id="one-stop-<%= flightIdx %>" type="checkbox" checked="true" aria-label="1-stop">
                              <label for="one-stop-<%= flightIdx %>"><%=labels.one_Stop%></label>
                            </div>
                          </li>
                        <% } %>
                        <% if(filter.twoStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="two-stop-<%= flightIdx %>" id="two-stop-<%= flightIdx %>" type="checkbox" checked="true" aria-label="2-stop">
                              <label for="two-stop-<%= flightIdx %>"><%=labels.two_Stop%></label>
                            </div>
                          </li>
                        <% } %>
                      </ul>
                    </div>
                    <div class="right-content">
                      <p class="title-6--dark title"><%=labels.carrier%></p>
                      <ul class="list">
                        <li>
                          <div class="custom-checkbox custom-checkbox--1">
                            <input name="sa-group-<%= flightIdx %>" id="sa-group-<%= flightIdx %>" checked="true"  type="checkbox" aria-label="Singapore Airlines Group" disabled="true">
                            <label for="sa-group-<%= flightIdx %>"><%=labels.logo%><em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2' data-tooltip-sagroup='<%- flightIdx %>'&gt;<%=labels.toolTipMsg%>&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em></label>
                          </div>
                        </li>
                        <% if(filter.codeShare) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="codeshare-<%= flightIdx %>" id="codeshare-<%= flightIdx %>" type="checkbox" checked="true" aria-label="Partner / codeshare airlines">
                              <label for="codeshare-<%= flightIdx %>"><%=labels.partner%></label>
                            </div>
                          </li>
                        <% } %>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="content-filter-search filter-time">
                  <div class="content-inner">
                    <div class="slider-inner departure">
                      <p class="title-6--dark title"><%=labels.depTime%></p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="departure" data-min="<%- filter.minDeparture %>" data-max="<%- filter.maxDeparture %>" data-step="1800000"></div>
                      </div>
                    </div>
                    <div class="slider-inner arrival">
                      <p class="title-6--dark title"><%=labels.arrTime%></p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="arrival" data-min="<%- filter.minArrival %>" data-max="<%- filter.maxArrival %>" data-step="1800000"></div>
                      </div>
                    </div>
                    <div class="slider-inner travel">
                      <p class="title-6--dark title"><%=labels.duration%></p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="tripDuration" data-min="<%- filter.minTripDuration %>" data-max="<%- filter.maxTripDuration %>" data-step="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="button" name="btn-filter" value="apply filter" class="btn-1 btn-filter hidden-tb-dt">
              </div>
            </div>
          </div>
        <% } %>
        <div class="no-result-filter">
          <%=labels.noResult%>
          <a href="#" title="reset filter" data-reset-filter><%=labels.reset%></a>
        </div>
        <div class="recommended-flight-block recommended-flight-block-<%-flightIdx %>" aria-describedby="table-summary__<%- flightIdx %>" data-returntrip="<%- flightIdx %>">
          <input type="hidden" name="flightIdx" id="flightIdx" class="flightIdx" value="<%-flightIdx%>"/>
	  <span id="table-summary__<%- flightIdx %>" class="says" tabindex="0"><%=labels.fareMsg1%></span>          
          <div class="head-recommended-selected hidden"><span><%=labels.depFlight%></span></div>
          <div class="render-after"></div>
        </div>
        <div class="loadmore-block hidden" data-loadmore>
          <span class="show-result"><%=labels.showing%>&nbsp<span data-loaded-flight><%=labels.five%></span> out of <span data-total-flight><%=labels.ten%></span>&nbsp<%=labels.flights%></span>
          <a href="#" title="btn-loadmore" class="btn-loadmore hidden-mb-small"><%=labels.show%></a>
          <a href="#" title="btn-loadmore" class="btn-loadmore-mb hidden-tb-dt"><%=labels.load%></a>
        </div>
      </div>
    <% }); %>
  </div>

    <form action="#" method="post" class="fare-summary-group hidden" id="chooseFlightForm" commandname="chooseFlightForm" 
    modelattribute="chooseFlightForm" data-check-change="true"> 
   
      <input type="hidden" value="" name="fare-family-outbound-id" id="fare-family-outbound">
      <input type="hidden" value="" name="flight-outbound-id" id="flight-outbound">
      <input type="hidden" value="" name="fare-family-inbound-id" id="fare-family-inbound">
      <input type="hidden" value="" name="flight-inbound-id" id="flight-inbound">
      <input type="hidden" value="" name="recommendation-id" id="recommendation-id">
      <input type="hidden" value="" name="selectedFlightIdDetails" class="selectedFlightIdDetails" id="selectedFlightIdDetails">
	<input type="hidden" name="tripType" id="tripType" class="tripType" value="">
	<input type="hidden" name="SecureFareFlow" id="SecureFareFlow" value="">
	<input type="hidden" name="SecureButtonClick" id="SecureButtonClick" value="">
	<div class="your-flight-item block-2 upgrade-economy-item <%- $('body').hasClass('sk-ut-workflow') ? 'hidden' : '' %> hidden-mi hidden">
        <div class="description">
          <div class="img-slider">
            <div class="img-slider-inner">
              <div class="flexslider flexslider--2" data-slideshow-premium-economy>
                <div class="slides">
                  <div class="slide-item">
                    <div>
                        <img src="saar5/images/sk-upgrade-economy.jpg" alt="Upgrade to Premium Economy and enjoy" longdesc="img-desc.html"/>
                    </div>
                  </div>
                  <div class="slide-item">
                    <div>
                        <img src="saar5/images/sk-upgrade-economy.jpg" alt="Upgrade to Premium Economy and enjoy" longdesc="img-desc.html"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="content">
            <div class="detail-content">
              <h4 class="title-4--blue"><%=labels.upgradePremium%></h4>
              <ul>
                <li><%=labels.moreComfort%></li>
                <li><%=labels.moreChoice%><i>&nbsp;<%=labels.book%>&nbsp;</i>the<i>&nbsp;<%=labels.cookOne%>&nbsp;</i><%=labels.menu%></li>
                <li><%=labels.handling%></li>
              </ul>
            </div>
            <div class="from-price-flight"><span class="note"><%=labels.adtnl%></span><span class="sgd-price"><%-curCode%></span><span class="miles"><%=labels.perPassenger%></span>
              <div class="button-group-3">
                <a href="#" class="btn-8"><%=labels.upgrde%></a>
              </div>
            </div>
          </div>
        </div>
      </div>

	<div class="fare-summary">
        <h3 class="title"><%=labels.summary%></h4>
        <% if(!$('body').hasClass('sk-ut-workflow')) { %>
          <p class="des"><%=labels.fareMsg2%></p>
        <% } %>
      

        <% if(data.tripType==='O'){%>
	
		<div class="table-fare-summary hidden-mb-small">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper fare-condition"><span><%=labels.conditions%></span></div>
            </div>            
            <div class="col-select">
              <div class="head-wrapper column-right ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" value="" class="ttt">
              </div>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right"><span class="baggage"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
              <span><%=labels.seat%></span>
            <% } %>
            </span></div>          
            <div class="col-select column-right"><span class="seat-selection"></span></div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right"><span class="earn-krisFlyer"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
              <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right">
            <span class="upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select column-right"><span class="cancellation"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select column-right"><span class="booking-change"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select column-right"><span class="no-show"></span></div>
          </div>
        </div>
	 
       
  <% }else if(data.tripType==='M'){%>
 <% if(data.flights.length > 2) { %>
<div class="table-fare-summary hidden-mb-small">
          <div class="row-head-select">
            <div class="col-select sk-mtc-left">
              <div class="head-wrapper fare-condition"><span><%=labels.conditions%></span></div>
            </div>            
            <div class="col-select">
               <% if(data.flights.length > 2) { %>
				<div class="head-wrapper column-right sk-mtc-right">
				<ul class="sk-multicity">	     
				<% _.each(data.flights, function(itemFly, idx){ %>				
					<li>
					<span class="code-flight"></span>
					<%- itemFly.originAirportCode %> - <%- itemFly.destinationAirportCode %></span>
					<input type="hidden" value="" class="ttt">
					</li>						
				<% }) %>				
				 </ul>
				 <span class="name-header"></span>
				 </div> 
				  <% } %>	
				
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right sk-mtc-right"><span class="baggage"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right sk-mtc-right"><span class="seat-selection"></span></div>
          </div>
          <div class="row-select first-row">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-icons-42"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right sk-mtc-right"><span class="earn-krisFlyer"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>            
            <div class="col-select column-right sk-mtc-right">
            <span class="upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select column-right sk-mtc-right"><span class="cancellation"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select column-right sk-mtc-right"><span class="booking-change"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select sk-mtc-left"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select column-right sk-mtc-right"><span class="no-show"></span></div>
          </div>
        </div>

 <%} else if(data.flights.length == 2) { %>

		<div class="table-fare-summary hidden-mb-small">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper fare-condition"><span><%=labels.conditions%></span></div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-left ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" value="" class="ttt">
              </div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-right ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" value="" class="ttt">
              </div>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="baggage"></span></div>
            <div class="col-select column-right"><span class="baggage"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="seat-selection"></span></div>
            <div class="col-select column-right"><span class="seat-selection"></span></div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="earn-krisFlyer"></span></div>
            <div class="col-select column-right"><span class="earn-krisFlyer"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left">
            <span class=" upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
            <div class="col-select column-right">
            <span class="upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select column-left"><span class="cancellation"></span></div>
            <div class="col-select column-right"><span class="cancellation"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select column-left"><span class="booking-change"></span></div>
            <div class="col-select column-right"><span class="booking-change"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select column-left"><span class="no-show"></span></div>
            <div class="col-select column-right"><span class="no-show"></span></div>
          </div>
        </div>
 
 
 <%}%>


	 <% } else {%>

		<div class="table-fare-summary hidden-mb-small">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper fare-condition"><span><%=labels.conditions%></span></div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-left ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" value="" class="ttt">
              </div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-right ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" value="" class="ttt">
              </div>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="baggage"></span></div>
            <div class="col-select column-right"><span class="baggage"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="seat-selection"></span></div>
            <div class="col-select column-right"><span class="seat-selection"></span></div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="earn-krisFlyer"></span></div>
            <div class="col-select column-right"><span class="earn-krisFlyer"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select column-left">
            <span class=" upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
            <div class="col-select column-right">
            <span class="upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select column-left"><span class="cancellation"></span></div>
            <div class="col-select column-right"><span class="cancellation"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select column-left"><span class="booking-change"></span></div>
            <div class="col-select column-right"><span class="booking-change"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select column-left"><span class="no-show"></span></div>
            <div class="col-select column-right"><span class="no-show"></span></div>
          </div>
        </div>
	<% } %>
			<%if(data.tripType=='O'){%>
			<div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header"><%=labels.promo%></span><input type="hidden" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div> 
            <div class="col-select"><span class="baggage-price baggage"><%=labels.kilos%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
             <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
           <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade "><%=labels.notAllowed%></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select"><span class="not-allowed cancellation"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select"><span class="fare-price booking-change"><%=labels.sgd%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select"><span class="not-allowed no-show"><%=labels.notAllowed%></span></div>
          </div>
        </div>
			<%}else if(data.tripType=='M'){%>
				<%if(data.flights.length == 2) { %>
				<div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header"><%=labels.promo%></span><input type="hidden" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div> 
            <div class="col-select"><span class="baggage-price baggage"><%=labels.kilos%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
             <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
           <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade "><%=labels.notAllowed%></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select"><span class="not-allowed cancellation"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select"><span class="fare-price booking-change"><%=labels.sgd%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select"><span class="not-allowed no-show"><%=labels.notAllowed%></span></div>
          </div>
        </div>
				<%}else{%>
				<div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header"><%=labels.promo%></span><input type="hidden" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div> 
            <div class="col-select"><span class="baggage-price baggage"><%=labels.kilos%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
             <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
           <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade "><%=labels.notAllowed%></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select"><span class="not-allowed cancellation"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select"><span class="fare-price booking-change"><%=labels.sgd%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select"><span class="not-allowed no-show"><%=labels.notAllowed%></span></div>
          </div>
        </div>
				<%}%>
			
		<%}else if(data.tripType=='R'){%>
			<div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header"><%=labels.promo%></span><input type="hidden" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="baggage-price baggage "><%=labels.kilos%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
           <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade "><%=labels.notAllowed%></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select"><span class="not-allowed cancellation"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select"><span class="fare-price booking-change"><%=labels.sgd%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select"><span class="not-allowed no-show"><%=labels.notAllowed%></span></div>
          </div>
        </div>
		<div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header"><%=labels.promo%></span><input type="hidden" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.baggageOne%></span>
            <% } else { %>
              <span><%=labels.baggage%></span>
            <% } %>
            </span></div> 
            <div class="col-select"><span class="baggage-price baggage"><%=labels.kilos%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
             <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.seatOne%></span>
            <% } else { %>
             <span><%=labels.seat%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
          <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.earnMilesOne%></span>
            <% } else { %>
              <span><%=labels.earnMiles%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
           <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span><%=labels.upgradeOne%></span>
            <% } else { %>
             <span><%=labels.upgrade%></span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade "><%=labels.notAllowed%></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong><%=labels.upgradeAward%></strong><p class='tooltip__text-2'><%=labels.milesRequired%></p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span><%=labels.cancellation%></span></span></div>
            <div class="col-select"><span class="not-allowed cancellation"><%=labels.notAllowed%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span><%=labels.booking%></span></span></div>
            <div class="col-select"><span class="fare-price booking-change"><%=labels.sgd%></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span><%=labels.noShow%></span></span></div>
            <div class="col-select"><span class="not-allowed no-show"><%=labels.notAllowed%></span></div>
          </div>
        </div>
		<%}%>       		
       
        <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
          <div class="has-note-fare"><%=labels.fareMsg3%></div>
        <% } %>
        <div class="has-note-fare">* <%=labels.fareMsg2%></div>
        <div class="full-fare-link">
	<% if(rebookVal == 'false') { %>
          <a href="#" class="link-4 trigger-popup-add-ons-baggage" data-trigger-popup=".popup--add-ons-summary"><em class="ico-point-r"></em><%=labels.fullFareRules%></a>
         <%}%>
	  <span class="fare-complete"><%=labels.fareMsg4%></span>
        </div>
      </div>
      <br>
      <div class="button-group-1">
        <div class="after-summary-fare "></div>
	<input type="hidden" id="_eventId" name="_eventId" value="reviewItineraryEvent"/>
	<input type="submit" name="btn-next" id="btn-next" value="proceed" class="btn-1">	
	<% if(rebookVal == 'false') { %>
		
	 <button id="btn-secure-Disabled" data-tooltip="true" data-content="<p class='tooltip__text-2 '><%=labels.secureOne%><br><%=labels.secureTwo%></p>" data-type="2" data-open-tooltip="data-open-tooltip" class="btn-1 disabled secureFareButton" aria-label="View more information"><%=labels.secureFare%></button>	
	 
	 
	 
	<button id="btn-secure" class="btn-2 not-tootip flights-details-sf" ><%=labels.secureFare%></button><span class="text hidden secureFareText"><%=labels.hold%></span>
	<% } %>
	
      </div>
    </form>
<% } %>
