<% if(data) {%>
  <%
    var countStops = function (flightSegment) {
      var stops = flightSegment.length - 1;
      var stopsLabel = [ "Non-stop", "One-stop", "Two-stop" ];

      return stopsLabel[stops];
    };

    var concatOriginDestinationCity = function (flightDetails) {
      return flightDetails.originCityName + " to " + flightDetails.destinationCityName;
    };

    var concatOriginDestination = function (flightDetails, concatBy) {
      return flightDetails.from + " " + concatBy + " " + flightDetails.to;
    };

    var addOnsDeals = function (flightAddonPerPax) {
      return flightAddonPerPax.packDescription + " + " + flightAddonPerPax.additionalBaggageWeight + "kg";
    };

    var haveTravelDeals = function (addPerPax) {
      var haveDeal = false;
      _.each(addPerPax, function (pax) {
        if (pax.isPackService) {
          haveDeal = true;
        }
      });

      return haveDeal;
    };

    var showLinkOnTier = function (tierCode) {
      var allowedTierCode = ['T', 'T', 'Q', 'G'];
      
      return (allowedTierCode.indexOf(tierCode.toUpperCase()) != -1);
    };
  %>
  <% if(data.flight.length > 0) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="true" class="group-title active">
          <h3 class="title-4--blue main-title"><span data-number> </span>. Itinerary</h3>
          <h3 class="title-5--dark sub-total" >Sub-total <%- data.currency %> <span> <%- data.fareSubTotal %></span></h3>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.flight, function(dataFlight, idx){ %>
          <div class="booking-info-group booking-flight-item">
            <h4 class="title-5--blue"><%- dataFlight.flightSegments[0].carrierCode %><%- dataFlight.flightSegments[0].flightNumber %> - <%- dataFlight.origin %> to <%- dataFlight.destination %></h4>
            <span class="flights-type"><%- dataFlight.flightSegments[0].cabinClassDesc %>
              <em data-tooltip="true" data-max-width="340" tabindex="0" data-content="<div class=&quot;summary-fare-fs&quot;><span class=&quot;title&quot;>Fare conditions</span><ul><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-business-1&quot;></em><span>Baggage</span></span><span class=&quot;fare-right&quot;><span>20kg</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-1-preferred&quot;></em><span>Seat selection at booking</span></span><span class=&quot;fare-right&quot;><span class=&quot;not-allowed&quot;>Only available during online check-in</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-icons-42&quot;></em><span>Earn KrisFlyer miles</span></span><span class=&quot;fare-right&quot;><span>8,000 miles</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-7-upgrade-circle&quot;></em><span>Upgrade with miles</span></span><span class=&quot;fare-right&quot;><span class=&quot;not-allowed&quot;>Not allowed</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-cancel-all&quot;></em><span>Cancellation</span></span><span class=&quot;fare-right&quot;><span class=&quot;not-allowed&quot;>Not allowed</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-refresh&quot;></em><span>Booking change</span></span><span class=&quot;fare-right&quot;><span>SGD 300</span></span></li><li><span class=&quot;fare-icon&quot;><em class=&quot;ico-close-round-fill&quot;></em><span>No show</span></span><span class=&quot;fare-right&quot;><span class=&quot;not-allowed&quot;>Not allowed</span></span></li></ul><a href=&quot;#&quot; data-trigger-popup=&quot;.popup--add-ons-summary&quot; class=&quot;link-4&quot;><em class=&quot;ico-point-r&quot;><span class=&quot;ui-helper-hidden-accessible&quot;>More info</span></em>Full fare conditions</a></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill current tooltip-flight" aria-label="View more information"></em>
            </span>

            <div class="booking-info-content">
              <% _.each(dataFlight.flightSegments, function(dataFlightSegments, idx){ %>
                <div class="flight-station">
                  <% if(idx === 0) { %>
                    <span class="stop-time"><%= countStops(dataFlight.flightSegments)%> • <%- dataFlight.totalTravelTime %></span>
                  <% } %>
                  <div class="flight-station-item">
                    <div class="flight-station--inner">
                      <div class="flight-station-info">
                        <div class="station-stop">
                          <span class="station-stop-detail">
                            <em class="ico-airplane-2"></em>
                            <span class="time"><%- dataFlight.totalTravelTime.split('ins')[0] %></span>
                          </span>
                        </div>
                        <div class="flights-station__info--detail">
                          <span class="hour"><%- dataFlightSegments.deparure.airportCode %> <%- dataFlightSegments.deparure.time %></span>
                          <span class="country-name"><%- dataFlightSegments.deparure.cityName %></span>
                          <span class="date"><%- dataFlightSegments.deparure.date %><br>
                          <%= dataFlightSegments.deparure.airportName %>
                          <% if(dataFlightSegments.deparure.terminal !== "" && dataFlightSegments.deparure.terminal) { %> Terminal <% } %> <%- dataFlightSegments.deparure.terminal %></span>
                        </div>
                        <div class="flights-station__info--detail return-flight">
                          <span class="hour"><%- dataFlightSegments.arrival.airportCode %> <%- dataFlightSegments.arrival.time %></span>
                          <span class="country-name"><%- dataFlightSegments.arrival.cityName %></span>
                          <span class="date"><%- dataFlightSegments.arrival.date %><br>
                          <%= dataFlightSegments.arrival.airportName %>
                          <% if(dataFlightSegments.arrival.terminal !== "" && dataFlightSegments.arrival.terminal) { %> Terminal <% } %> <%- dataFlightSegments.arrival.terminal %></span>
                        </div>
                      </div>
                      <div class="airline-info">
                        <div class="inner-info">
                          <span class="airline-detail<% if(dataFlightSegments.carrierCode === "SQ") { %> singapore-logo<% } else if(dataFlightSegments.carrierCode === "TR") { %> scoot-logo<% } %> ">
                          <% if(dataFlightSegments.carrierCode === "SQ" || dataFlightSegments.carrierCode === "SI" || dataFlightSegments.carrierCode === "TR" ) { %>
                            <img src="images/svg/<%- dataFlightSegments.carrierCode.toLowerCase() %>.svg" alt="<%- dataFlightSegments.carrierCode.toLowerCase() %> Logo" longdesc="img-desc.html">
                          <% } %>
                            <strong><%- dataFlightSegments.carrierName %></strong><span>• </span><%= dataFlightSegments.carrierCode %> <%= dataFlightSegments.flightNumber %>
                          </span>
                          <span class="name-plane"><%- dataFlightSegments.airCraftType %></span>
                          <span class="economy"><%= dataFlightSegments.cabinClassDesc.split(' ')[0] %></span>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              <% }); %>
            </div>
          </div>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total">
          <div data-tabindex="true" class="flights-cost">
            <h4 class="flights-cost-title"><span class="text-left">Flights</span><span class="text-right"><%- data.currency %></span></h4>
            <ul class="flights-cost__details">
            <% if(data.paxDetails) {%>
              <li><span>Fare</span><span class="price"></span></li>
              <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
                <li data-fare="true"><span> &#8228; <%- flightPaxDetails.paxName %> </span><span class="price"><%- flightPaxDetails.fareDetails.fareAmount.toFixed(2) %></span></li>
              <% }); %>
            <% } %>
              <li data-taxes="true"><span>Airport/ Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
              <li data-carrier="true"><span>Carrier surcharges</span><span class="price"><%- data.surchargeTotal.toFixed(2) %></span></li>
              <li data-subtotal="true" class="sub-total"><span>Sub-total</span><span class="price"><%- data.fareSubTotal %></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <% } %>
  <% if(data.paxDetails.length > 0) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
          <h4 class="title-4--blue main-title"><span data-number> </span>. Passengers and fight-related add-ons</h4>
          <h5 class="title-5--dark sub-total">Sub-total <%- data.currency %> <span>300.00</span></h5>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <div data-accordion-wrapper="2">
          <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
            <div data-accordion-wrapper-content="2">
              <div class="booking-info-group" data-accordion="2">
                <div data-accordion-trigger="2" aria-expanded="true" class="group-title disable-click <%- (!idxPaxDetails) ? 'active' : '' %>">
                  <h5 class="title-5--blue"><%- flightPaxDetails.paxName %></h5><em class="ico-point-d hidden-tb-dt"></em>
                </div>
                <div data-accordion-content="2">
                    <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                      <div class="booking-info-content">
                        <h5 class="title-5--blue"><%- flightDetailsPerFlight.flightNo %> - <%- concatOriginDestinationCity(flightDetailsPerFlight)%></h5>
                        <% if (haveTravelDeals(flightDetailsPerFlight.addonPerPax)) { %>
                          <div class="bundle-info">
                            <span>
                              <strong> Travel in Comfort + Deals added - </strong>
                            </span>
                            <span>
                              <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                                <% if (flightAddonPerPax.isPackService) { %>
                                  <%- flightAddonPerPax.packDescription %>
                                <% } %>
                              <% }); %>
                            </span>
                          </div>
                        <% } %>
                        <div class="booking-info--2">
                          <div class="booking-info--1__item">
                              <div class="booking-details__group">
                                  <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                                        <div class="booking-details booking-detail--1">
                                          <div class="booking-col col-1"><em class="ico-change-seat"></em>
                                          </div>
                                          <div class="booking-col col-2">
                                              <div aria-label="Seats" class="Seats">Seats</div>
                                          </div>
                                          <div class="booking-col col-3">
                                              <div class="align-wrapper">
                                              <div class="align-inner">
                                                  <div class="has-cols">
                                                    <p class="target-info"><%- concatOriginDestination(flightDetailsPerFlight, "TO") %></p>
                                                    <p class="seat-info">
                                                      <%- flightAddonPerPax.seatNumber %>
                                                      (<%- flightAddonPerPax.seatType %>)
                                                    </p>
                                                  </div>
                                              </div>
                                              </div>
                                          </div>
                                        </div>
                                      
                                        <div class="booking-details booking-detail--1">
                                          <div class="booking-col col-1"><em class="ico-business-1"></em>
                                          </div>
                                          <div class="booking-col col-2">
                                              <div aria-label="Baggage" class="Baggage">Baggage</div>
                                          </div>
                                          <div class="booking-col col-3">
                                              <div class="align-wrapper">
                                              <div class="align-inner">
                                                  <div class="has-cols">
                                                  <p class="target-info">Included</p>
                                                  <p class="seat-info">
                                                    <%var hasAdditionalBaggage = false; %>
                                                    <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                                                      <% if (flightAddonPerPax.additionalBaggageWeight) hasAdditionalBaggage = true;%>
                                                      <%- (flightAddonPerPax.ffpFreeBaggageWeight) ? flightAddonPerPax.ffpFreeBaggageWeight : 0 %>kg <br>
                                                    <% }); %>
                                                  </p>
                                                  <% if (showLinkOnTier(flightPaxDetails.kfTierCode)) { %>
                                                  <div class="tc-prompt">
                                                      <p><span><a href="#" data-trigger-popup=".popup--terms-and-conditions"> <em class="ico-point-r"></em> View PPS Club / KrisFlyer Elite Gold additional baggage privileges</a></span> </p>
                                                  </div>
                                                  <% } else { %>
                                                    <br>
                                                  <% } %>
                                                  </div>
                                                  <% if (hasAdditionalBaggage) { %>
                                                  <div class="has-cols">
                                                  <p class="target-info">Additional</p>
                                                  <p class="seat-info">
                                                    <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %> 
                                                      <%- flightAddonPerPax.additionalBaggageWeight %>kg <br>
                                                    <% }); %>
                                                  </p>
                                                  </div>
                                                  <% } %>
                                              </div>
                                              </div>
                                          </div>
                                        </div>
                                  <% }); %>
                      
                                  <!--New tpl for WiFi -->
                                  <div class="booking-details hidden booking-detail--1">
                                      <div class="booking-col col-1"><em class="ico-wifi"></em>
                                      </div>
                                      <div class="booking-col col-2">
                                      <div aria-label="WiFi" class="WiFi">WiFi</div>
                                      </div>
                                      <div class="booking-col col-3">
                                      <div class="align-wrapper">
                                          <div class="align-inner">
                                          <div class="has-cols">
                                              <p class="target-info black-txt">Full flight</p>
                                              <p class="seat-info"></p>
                                          </div>
                                          </div>
                                      </div>
                                      </div>
                                  </div>
                                  <!--END -->
                              </div>
                          </div>
                        </div>
                      </div>
                  <% }); %>
                </div>
              </div>
            </div>
          <% }); %>
        </div>
        <div data-flight-cost="true" class="mp-payments-total has-link">
          <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
            <div data-tabindex="true" class="flights-cost">
              <h4 class="flights-cost-title"><span class="text-left"><%- flightPaxDetails.paxName %></span><span class="text-right"><%- data.currency %></span></h4>
              <ul class="flights-cost__details">
                <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                  <% if(flightDetailsPerFlight.addonPerPax) {%>
                    <li><%- concatOriginDestination(flightDetailsPerFlight, "-")%></li>
                    <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                      <li>
                        <span> •
                          <% if (flightAddonPerPax.packDescription) { %>
                            Travel in Comfort + Deal
                          <% } else { %>
                            <%- flightAddonPerPax.type %>
                          <% } %>
                        </span>
                        <a href="#" class="link-4 link-remove" aria-label="Remove Bundle for <%- flightDetailsPerFlight.from %> - <%- flightDetailsPerFlight.to %>"><em class="ico-point-r"></em>Remove</a>
                        <span class="price"><%- flightAddonPerPax.amount.toFixed(2) %></span>
                      </li>
                    <% }); %>
                  <% } %>
                <% }); %>
                <li class="sub-total"><span>Sub-total</span><span class="price">160.00</span>
                </li>
              </ul>
            </div>
          <% }); %>
        </div>
      </div>
    </div>
  <% } %>
  <% if(data.commonAddons.length > 0) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
          <h4 class="title-4--blue main-title"><span data-number> </span>. Trip-related add-ons</h4>
          <h5 class="title-5--dark sub-total">Sub-total <%- data.currency %> <span>893.80</span></h5>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
          <% if(flightCommonAddons.type == "Travel Insurance") {%>
            <div class="trip-related">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-aig-logo.png" alt="Travel insurance" longdesc="img-desc.html"/>
              </div>
              <ul class="trip-related-details">
                <li><span class="name">Person covered</span><span class="content" data-remove-last-comma><% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %><%- flightPaxDetails.paxName %>, <% }); %></span></li>
                <li><span class="name">Travel location</span><span class="content"><%- flightCommonAddons.travelLocation %></span></li>
                <li><span class="name">Date range</span><span class="content"><%- flightCommonAddons.description %></span></li>
              </ul>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Hotel") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-agoda-logo.png" alt="Hotel" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title">
                  <h4 class="title-4--blue"><%- flightCommonAddons.name %></h4>
                  <p class="des"><%- flightCommonAddons.address %></p>
                </div>
                <div class="trip-thumb"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details">
                  <li><span class="name">Check-in date:</span><span class="content"><%- flightCommonAddons.checkInDate %></span></li>
                  <li><span class="name">Check-out date:</span><span class="content"><%- flightCommonAddons.checkOutDate %></span></li>
                  <li><span class="name">Guests:</span><span class="content"><%- flightCommonAddons.guest %></span></li>
                  <% _.each(flightCommonAddons.rooms, function(flightCommonAddonsRooms, idxRoom){ %>
                    <li><span class="name">Room types:</span><span class="content"><%- flightCommonAddonsRooms.roomType %></span></li>
                    <li><span class="name">No. of nights:</span><span class="content"><%- flightCommonAddonsRooms.numberOfNights %></span></li>
                    <% if(flightCommonAddonsRooms.breakfast == true) {%>
                      <li><span class="name">Breakfast:</span><span class="content">Included for all rooms</span></li>
                    <% } %>
                  <% }); %>
                </ul>
              </div>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Car Rental") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-rentalcars-logo.png" alt="Car rental" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title sub-title-inline">
                  <h4 class="title-4--blue"><%- flightCommonAddons.carType %></h4>
                  <p class="des">or similar</p>
                </div>
                <div class="trip-thumb trip-thumb-inline"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details shorter-width">
                  <li><span class="name">Pick up:</span><span class="content"><%- flightCommonAddons.pickupDate %></span></li>
                  <li><span class="name">Drop off:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.pickupLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                </ul>
              </div>
            </div>
          <% } %>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total has-link shorter-width">
          <div data-tabindex="true" class="flights-cost flights-fax">
            <h4 class="flights-cost-title"><span class="text-left">ADD-ONS</span><span class="text-right">SGD</span></h4>
            <ul class="flights-cost__details">
              <% var subTotalCommon,
                  amountHotel, amountTravelInsurance,
                  amountCarRental;
              %>
              <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
                <% if(flightCommonAddons.type == "Travel Insurance") {%>
                  <li><span><%- flightCommonAddons.type %></span>
                  <a href="#" class="link-4 link-remove" aria-label="Remove Travel Insurance"><em class="ico-point-r"></em>Remove</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountTravelInsurance = flightCommonAddons.amount %>
                  </li>
                <% } %>
                <% if(flightCommonAddons.type == "Hotel") {%>
                  <li><span><%- flightCommonAddons.type %></span>
                  <a href="#" class="link-4 link-remove" aria-label="Remove Hotel"><em class="ico-point-r"></em>Remove</a>
                  <a href="#" class="link-4" aria-label="Change Hotel"><em class="ico-point-r"></em>Change</a>
                  <span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountHotel = flightCommonAddons.amount %>
                  </li>
                <% } %>
                <% if(flightCommonAddons.type == "Car Rental") {%>
                  <li><span><%- flightCommonAddons.type %></span>
                  <a href="#" class="link-4 link-remove" aria-label="Remove Car Rental"><em class="ico-point-r"></em>Remove</a>
                  <a href="#" class="link-4" aria-label="Change Car Rental"><em class="ico-point-r"></em>Change</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountCarRental = flightCommonAddons.amount %>
                  </li>
                <% } %>
              <% }); %>
              <%
                subTotalCommon = amountHotel + amountTravelInsurance + amountCarRental;
              %>
              <li class="sub-total"><span>Sub-total</span><span class="price"><%- subTotalCommon.toFixed(2) %></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <% } %>
  
  <div class="accordion-item" data-block-accordion-item>
    <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
      <h4 class="title-4--blue main-title"><span data-number> </span>. Cost breakdown</h4>
      <h5 class="sub-heading-3--blue sub-total grand-total">Grand total <%- data.currency %> <span>3,971.80</span></h5>
      <em class="ico-point-d"></em>
    </a>
    <div data-accordion-content="1" class="group-content">
      <div data-flight-cost="true" class="mp-payments-total">
        <div data-tabindex="true" class="flights-cost">
          <h4 class="flights-cost-title"><span class="text-left">Flights</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
            <% if(data.paxDetails) {%>
              <li><span>Fare</span><span class="price"></span></li>
              <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
                <li data-fare="true">
                  <span> • <%- flightPaxDetails.paxName %> </span>
                  <span class="price"><%- flightPaxDetails.fareDetails.fareAmount.toFixed(2) %></span>
                </li>
              <% }); %>
            <% } %>
            <li data-taxes="true"><span>Airport/ Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
            <li data-carrier="true"><span>Carrier surcharges</span><span class="price"><%- data.surchargeTotal.toFixed(2) %></span></li>
            <li data-subtotal="true" class="sub-total"><span>Sub-total</span><span class="price"><%- data.fareSubTotal %></span></li>
          </ul>
          <h4 class="flights-cost-title"><span class="text-left">Flights Add-ons</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
              <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
                  <li><span><%- flightPaxDetails.paxName.toUpperCase() %></span></li>
                  <ul class="flights-cost__details">
                    <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                      <% if(flightDetailsPerFlight.addonPerPax) {%>
                        <li><b><%- concatOriginDestination(flightDetailsPerFlight, "-")%></b></li>
                        <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                          <li>
                            <span> •
                              <% if (flightAddonPerPax.isPackService) { %>
                                Travel in Comfort + Deal
                              <% } else { %>
                                <%- flightAddonPerPax.type %>
                              <% } %>
                            </span>
                            <a href="#" class="link-4 link-remove" aria-label="Remove Bundle for <%- flightDetailsPerFlight.from %> - <%- flightDetailsPerFlight.to %>"><em class="ico-point-r"></em>Remove</a>
                            <span class="price"><%- flightAddonPerPax.amount.toFixed(2) %></span>
                          </li>
                        <% }); %>
                      <% } %>
                    <% }); %>
                  </ul>
              <% }); %>
            <li class="sub-total"><span>Sub-total</span><span class="price">300.00</span></li>
          </ul>
          <h4 class="flights-cost-title"><span class="text-left">Trip Add-ons</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
            <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
              <% if(flightCommonAddons.type == "Travel Insurance") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountTravelInsurance.toFixed(2)  %></span>
                </li>
              <% } %>
              <% if(flightCommonAddons.type == "Hotel") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountHotel.toFixed(2) %></span>
                </li>
              <% } %>
              <% if(flightCommonAddons.type == "Car Rental") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountCarRental.toFixed(2) %></span>
                </li>
              <% } %>
            <% }); %>
            <li class="sub-total"><span>Sub-total</span><span class="price"><%- subTotalCommon.toFixed(2) %></span>
            </li>
          </ul>
          <div class="grand-total"><span>Grand total</span>
            <div class="grand-total-content">
              <div class="text-total">SGD <span>3,971.80</span></div>
              <div class="des">Includes discounts, tax and surcharges</div>
            </div>
          </div>
          <div class="extra-info">
            <li><span>Payable with KrisFlyer miles<em data-tooltip="true" data-type="2" data-max-width="255" data-content="<p class=&quot;tooltip__text-2&quot;>Use your KrisFlyer miles to pay for all or a part of your airfare (including taxes and surcharges), additional baggage and seats. KrisFlyer miles, Elite miles and PPS Value will be earned in the proportion to the amount (excluding taxes) that you have paid with your credit/debit card.</p>" class="ico-tooltips tooltip-krisflyer"></em></span><span data-krisflyer-miles ><%- data.currency %> <%- data.costPayableByMiles.toFixed(2) %></span></li>
            <li><span data-krisflyer-miles-rest >Not payable with KrisFlyer miles</span><span><%- data.currency %> <%- data.costPayableByCash.toFixed(2) %></span></li>
          </div>
        </div>
      </div><a href="#" class="link-4 link-cost-breakdown trigger-popup-flights-details" data-trigger-popup=".popup--flights-details"><em class="ico-point-r"></em>Cost breakdown by passengers</a>
    </div>
  </div>
  
<% } %>
