<% if (data && data.SAARewardInfo) { %>
  <span class="title-6--dark">About this voucher</span>
  <p>
    <%- data.SAARewardInfo.aboutVoucher %>
  </p>
  <% if (data.SAARewardInfo.voucherList && data.SAARewardInfo.voucherList.length) { %>
    <div class="voucher-list">
      <% _.each(data.SAARewardInfo.voucherList, function(voucher, idx) { %>
        <div class="item-row">
          <div class="item-cell"><%- data.displayName %></div>
          <div class="item-cell"><%- voucher.promoCode + ' ' + voucher.voucherNumber %></div>
          <div class="item-cell"><%- voucher.rewardExpiryDate %></div>
          <div class="item-cell hidden"><span class="status"><%- voucher.voucherStatus %></span></div>
        </div>
      <% }); %>
    <% } %>
  </div><span class="title-6--dark">Voucher usage</span>
    <p class="usage-airport-upgrade hidden">Airport upgrade vouchers can be used by both the principal PPS Club member and redemption nominees.</p>
    <p class="usage-bookable-upgrade hidden">Bookable upgrade vouchers can be used by both the principal PPS Club member and redemption nominees. Bookable upgrade vouchers will be automatically applied to all passengers in the flight segment. If you do not have sufficient Bookable upgrade vouchers for all passengers in the flight segment, your voucher cannot be used.</p>
    <p class="usage-double-miles-accrual hidden">Double miles accrual vouchers can only be used by the principal PPS Club member.</p>
<% } %>
