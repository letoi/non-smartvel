<% if (data && data.segmentDetail && data.segmentDetail.baggageDetails && data.segmentDetail.baggageDetails.length) { %>
    <% if (data.segmentDetail.baggageDetails.length === 1) { %>
      <div class="single-line booking-details booking-details--2">
    <% } else { %>
      <div class="booking-details booking-details--2">
    <% } %>
    <div class="booking-col col-1">
      <em class="ico-business-1"></em>
    </div>
    <div class="booking-col col-2">
      Baggage
    </div>
    <div class="booking-col col-3">
      <div class="align-wrapper">
        <div class="align-inner">
          <% _.each(data.segmentDetail.baggageDetails, function(baggage, bagIdx) { %>
            <p>
              <strong><%- baggage.baggageType %>: </strong>
              <%- baggage.name %>
            </p>
          <% }); %>
        </div>
      </div>
    </div>
    <div class="booking-col col-4">
      <% if (data.paxId && data.segmentDetail.segmentID) { %>
        <% if (data.segmentDetail.eligible && data.segmentDetail.eligible === 'true') { %>
          <p>
            <a href="/add-ons/?paxId=<%- data.paxId %>&segmentId=<%- data.segmentDetail.segmentID %>"><em class="ico-point-r"></em>Add / Change</a>
          </p>
          <p>
            <a href="/add-ons/?paxId=<%- data.paxId %>&segmentId=<%- data.segmentDetail.segmentID %>&cancel=1" data-trigger-popup=".popup--cancel-additional-baggage">
              <em class="ico-point-r"></em>Cancel
            </a>
          </p>
        <% } else { %>
          <p>
            <a class="disabled" href="javascript:void(0);"><em class="ico-point-r"></em>Add / Change</a>
          </p>
          <p>
            <a class="disabled" href="javascript:void(0);" data-trigger-popup=".popup--cancel-additional-baggage">
              <em class="ico-point-r"></em>Cancel
            </a>
          </p>
        <% } %>
      <% } else { %>
        <p>
          <a href="#"><em class="ico-point-r"></em>Add / Change</a>
        </p>
        <p>
          <a href="#" data-trigger-popup=".popup--cancel-additional-baggage">
            <em class="ico-point-r"></em>Cancel
          </a>
        </p>
      <% } %>
    </div>
  </div>
<% } %>
