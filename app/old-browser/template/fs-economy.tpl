<% if(data) { %>
  <%
    fareFamiliesGroup = [],  familiesCabinGroup = {}, arrWrap = [];
    _.each(data.fareFamilies, function(fareFamilies, fareFamiliesIdx) {
      familiesCabinGroup[fareFamilies.cabinClassName] = fareFamilies.cabinClassName;
    });
  %>

  <%
    function groupBy(arr, key) {
      var newArr = [],
          family = {},
          newItem, i, j, cur;
      for (i = 0, j = arr.length; i < j; i++) {
          cur = arr[i];
           if (!(cur[key] in family)) {
              family[cur[key]] = { type: cur[key], data: [] };
              newArr.push(family[cur[key]]);
          }
          family[cur[key]].data.push(cur);
      }
      return newArr;
    }
  %>
  <% var arr = [] %>
  <% _.each(data.flights, function(flights, flightIdx){ %>
    <% var newArray = [] %>
        <% _.each(flights.segments, function(segments, flightSegmentIdx){ %>
          <% var newArr1 = [] %>
              <% _.each(data.recommendations, function(recommendation, reIdx){ %>
                <% _.each(data.fareFamilies, function(fareFamiliesGroup, fareFamiliesGroupIdx){ %>
                  <% _.each(recommendation.segmentBounds[flightIdx].segments, function(segmentItem, segmentItemIdx){ %>
                    <% if(segmentItem.segmentID === segments.segmentID && fareFamiliesGroup.fareFamily == recommendation.segmentBounds[flightIdx].fareFamily ) { %>
                      <% newArr1.push({"family" : recommendation.segmentBounds[flightIdx].fareFamily,"familyGroup" : fareFamiliesGroup.cabinClassName, "price" : recommendation.fareSummary.fareTotal.totalAmount, "recommendationId" : recommendation.recommendationID, "displayLastSeat" : segmentItem.displayLastSeat, "numOfLastSeats" : segmentItem.numOfLastSeats }) %>
                      <% return false %>
                    <% } %>
                  <% }) %>
                <% }) %>
              <% }); %>
              <% newArray[segments.segmentID] =  newArr1 %>
        <% }); %>
        <% arr.push(newArray) %>
  <% }); %>
  <%
    _.each(arr, function(arr1, idx1){
      _.each(arr1, function(arr2, idx2){
        arr[idx1][idx2] = groupBy(arr2, 'family');
      })
    });
  %>

  <p class="says">Please use the tab key to move through this application</p>
  <div class="wrap-content-list">
    <% _.each(data.flights, function(flights, flightIdx) { %>
      <div class="wrap-content-fs">
        <ul class="status-list fs-status-list">
          <li><span class="status status-lowest"><span class="label-status"></span></span>Lowest available fare</li>
        </ul>
        <% if( flightIdx === 0 ) { %>
          <p id="table-caption__1" class="says">Outbound fares</p>
        <% } else if ( flightIdx === 1 ) { %>
          <p id="table-caption__1" class="says">Inbound fares</p>
        <% } %>
        <div class="slider-group">
            <div class="title-slider">
              <h3 class="sub-heading-3--dark">
              <% _.each(data.airports, function(airports, airportsIdx) { %>
                <% if( airports.airportCode === flights.originAirportCode ) { %>
                  <%- flightIdx + 1 %>. <%- airports.cityName %>
                <% } %>
              <% }); %> to
              <% _.each(data.airports, function(airports, airportsIdx) { %>
                <% if( airports.airportCode === flights.destinationAirportCode ) { %>
                  <%- airports.cityName %>
                <% } %>
              <% }); %>
              </h3>
              <a href="cib-fare-calendar.html" class="monthly-view"><em class="ico-chart"><span class="ui-helper-hidden-accessible"></span></em>My dates are flexible</a>
            </div>
            <div class="economy-slider flexslider" data-fs-slider="true" data-current-date="<%- flightIdx === 0 ? '2018-03-03' : '2018-03-17' %>" data-selected-date="<%- flightIdx === 0 ? '2018-03-03' : '2018-03-17' %>" data-slider-outbound="<%- flightIdx === 0 ? true : false %>">
              <button type="button" class="btn-3 btn-slider btn-prev slick-prev"></button>
              <div class="slides"></div>
              <button type="button" class="btn-3 btn-slider btn-next slick-next"></button>
            </div>
            <div class="sub-logo">
              <% if(!$('html').hasClass('ie8')){%>
                <img src="images/svg/sq.svg" alt="SQ Logo" longdesc="img-desc.html">
                <img src="images/svg/si.svg" alt="SI Logo" longdesc="img-desc.html">
                <img src="images/svg/tr.svg" alt="SC Logo" longdesc="img-desc.html">
              <% }else{ %>
                <img src="images/svg/sq.png" alt="SQ Logo" longdesc="img-desc.html">
                <img src="images/svg/si.png" alt="SI Logo" longdesc="img-desc.html">
                <img src="images/svg/tr.png" alt="SC Logo" longdesc="img-desc.html">
              <% } %>
              
              <span class="text">Singapore Airlines Group</span>
              <span class="ico-right">
                <em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2'&gt;Fly to more places around the world with the Singapore Airlines Group, which includes Singapore Airlines, SilkAir and Scoot&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em>
              </span>
            </div>
        </div>
        <% if(flights.segments.length > 1 && filterArr && filterArr.length) { %>
          <% var filter = filterArr[flightIdx] %>
          <div class="flight-search-filter-economy" data-flight-filter="<%- flightIdx %>">
            <% if (data.isMobile) { %>
              <div class="sort-dropdown-container">
                <span class="sub-menu-icon rotate-down">
                    <em class="ico-point-r"></em>
                  </span>
                <div class="custom-select custom-select--2 sort-by">
                  <span class="select__text">
                    <div class="select__title">
                      SORT BY:
                    </div>
                    <div class="select__body">
                      TRAVEL DURATION
                    </div>
                  </span>
                  <label for="sort-select-mobile">&nbsp;
                    <select id="sort-select-mobile">
                      <option value="travel duration" selected>TRAVEL DURATION</option>
                      <option value="price">PRICE</option>
                      <option value="arrival time">ARRIVAL TIME</option>
                      <option value="departure time">DEPARTURE TIME</option>
                    </select>
                  </label>
                </div>
              </div>
            <% } else { %>
            <div class="sort-filter">
                <span class="sort-by">SORT BY:</span>
                <a href="#" class="travel-duration sort-active">TRAVEL DURATION</a>
                <a href="#" class="sort-price">PRICE</a>
                <a href="#" class="arrival-time">ARRIVAL TIME</a>
                <a href="#" class="departure-time">DEPARTURE TIME</a>
            </div>
            <% } %>
            <div class="content">
              <div class="heading-filter">
                <div class="title">Filters</div>
                <div class="link-hide"><a href="#" class="link-4"><em class="ico-point-r">&nbsp;</em><span class="text">Hide filters</span></a>
                </div>
              </div>
              <div class="block-1">
                <div class="content-filter-search">
                  <div class="content-inner">
                    <div class="left-content">
                      <p class="title-6--dark title">Stopover</p>
                      <ul class="list">
                        <% if(filter.nonStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="non-stop-<%= flightIdx %>" id="non-stop-<%= flightIdx %>" type="checkbox" checked="true" type="checkbox" aria-label="non-stop">
                              <label for="non-stop-<%= flightIdx %>">Non-stop</label>
                            </div>
                          </li>
                        <% } %>
                        <% if(filter.oneStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="one-stop-<%= flightIdx %>" id="one-stop-<%= flightIdx %>" type="checkbox" checked="true" aria-label="1-stop">
                              <label for="one-stop-<%= flightIdx %>">1-stop</label>
                            </div>
                          </li>
                        <% } %>
                        <% if(filter.twoStop) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="two-stop-<%= flightIdx %>" id="two-stop-<%= flightIdx %>" type="checkbox" checked="true" aria-label="2-stop">
                              <label for="two-stop-<%= flightIdx %>">2-stop</label>
                            </div>
                          </li>
                        <% } %>
                      </ul>
                    </div>
                    <div class="right-content">
                      <p class="title-6--dark title">Operating carrier</p>
                      <ul class="list">
                        <li>
                          <div class="custom-checkbox custom-checkbox--1 disbled">
                            <input name="sa-group-<%= flightIdx %>" id="sa-group-<%= flightIdx %>" checked="true" disabled="true" type="checkbox" aria-label="Singapore Airlines Group">
                            <label for="sa-group-<%= flightIdx %>">Singapore Airlines Group <em data-tooltip="true" tabindex="0" data-content="&lt;p class='tooltip__text-2' data-tooltip-sagroup='<%- flightIdx %>'&gt;Fly to more places around the world with the Singapore Airlines Group, which includes Singapore Airlines, SilkAir and Scoot.&lt;/p&gt;" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em></label>
                          </div>
                        </li>
                        <% if(filter.codeShare) { %>
                          <li>
                            <div class="custom-checkbox custom-checkbox--1">
                              <input name="codeshare-<%= flightIdx %>" id="codeshare-<%= flightIdx %>" type="checkbox" checked="true" aria-label="Partner / codeshare airlines">
                              <label for="codeshare-<%= flightIdx %>">Partner / codeshare airlines</label>
                            </div>
                          </li>
                        <% } %>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="content-filter-search filter-time">
                  <div class="content-inner">
                    <div class="slider-inner departure">
                      <p class="title-6--dark title">Departure time</p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="departure" data-min="<%- filter.minDeparture %>" data-max="<%- filter.maxDeparture %>" data-step="1800000"></div>
                      </div>
                    </div>
                    <div class="slider-inner arrival">
                      <p class="title-6--dark title">Arrival time</p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="arrival" data-min="<%- filter.minArrival %>" data-max="<%- filter.maxArrival %>" data-step="1800000"></div>
                      </div>
                    </div>
                    <div class="slider-inner travel">
                      <p class="title-6--dark title">Travel duration</p>
                      <div class="slider slider--1">
                        <div class="slider-range" data-range-slider="tripDuration" data-min="<%- filter.minTripDuration %>" data-max="<%- filter.maxTripDuration %>" data-step="3600"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="button" name="btn-filter" value="apply filter" class="btn-1 btn-filter hidden-tb-dt">
              </div>
            </div>
          </div>
        <% } %>
        <div class="no-result-filter hidden">
          Lorem ipsum dolor sit amet sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          <a href="#" title="reset filter" data-reset-filter>Reset filter</a>
        </div>
        <div class="recommended-flight-block" aria-describedby="table-summary__<%- flightIdx %>" data-returntrip="<%- flightIdx %>">
          <span id="table-summary__<%- flightIdx %>" class="says" tabindex="0">You are now on a fare table. To navigate through the fare selection, press tab. To quickly move to the other available flights, use the up or down arrow keys. To select a fare, press enter. To go directly to the Booking Summary Panel, press control Y. </span>
          <div class="head-recommended"><span>Recommended flight for you</span></div>
          <div class="head-recommended-selected hidden"><span>Selected departure flight</span></div>
          <div class="render-after"></div>
        </div>
        <div class="loadmore-block hidden" data-loadmore>
          <span class="show-result">Showing <span data-loaded-flight>5</span> out of <span data-total-flight>10</span> flights</span>
          <a href="#" title="btn-loadmore" class="btn-loadmore hidden-mb-small">Load more flights</a>
          <a href="#" title="btn-loadmore" class="btn-loadmore-mb hidden-tb-dt">Show all flights</a>
        </div>
      </div>
    <% }); %>
  </div>

  <% _.each(data.ocRecommendations, function(ocRecommendations, ocRecommendationsIdx){ %>
    <form action="#" method="get" name="flight-search-summary" class="fare-summary-group hidden">
      <input type="hidden" value="" name="fare-family-outbound-id" id="fare-family-outbound">
      <input type="hidden" value="" name="flight-outbound-id" id="flight-outbound">
      <input type="hidden" value="" name="fare-family-inbound-id" id="fare-family-inbound">
      <input type="hidden" value="" name="flight-inbound-id" id="flight-inbound">
      <input type="hidden" value="" name=" recommendation-id" id="recommendation-id">
      <div class="your-flight-item block-2 upgrade-economy-item <%- $('body').hasClass('sk-ut-workflow') ? 'hidden' : '' %>">
        <div class="description">
          <div class="img-slider">
            <div class="img-slider-inner">
              <div class="flexslider flexslider--2" data-slideshow-premium-economy>
                <div class="slides">
                  <div class="slide-item">
                    <div>
                        <img src="images/sk-upgrade-economy.jpg" alt="Upgrade to Premium Economy and enjoy" longdesc="img-desc.html"/>
                    </div>
                  </div>
                  <div class="slide-item">
                    <div>
                        <img src="images/sk-upgrade-economy.jpg" alt="Upgrade to Premium Economy and enjoy" longdesc="img-desc.html"/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="content">
            <div class="detail-content">
              <h4 class="title-4--blue">Upgrade to Premium Economy and enjoy:</h4>
              <ul>
                <li>More comfort, with a greater seat width and more legroom</li>
                <li>More choices, as you can pre-order from our<i>&nbsp;Book&nbsp;</i>the<i>&nbsp;Cook&nbsp;</i>menu</li>
                <li>More privileges, with priority check-in, boarding and baggage handling</li>
              </ul>
            </div>
            <div class="from-price-flight"><span class="note">Additional</span><span class="sgd-price">SGD 388.00</span><span class="miles">Per passenger</span>
              <div class="button-group-3">
                <a href="#" class="btn-8">Upgrade</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="fare-summary">
        <h3 class="title">Summary of fare conditions</h4>
        <div class="table-fare-summary hidden-mb-small">
          <div class="row-head-select">
            <div class="col-select fare-condition-th">
              <div class="head-wrapper fare-condition"><span>Fare conditions</span></div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-left ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" name="" value="" class="ttt">
              </div>
            </div>
            <div class="col-select">
              <div class="head-wrapper column-right ">
                <span class="code-flight"></span><span class="name-header"></span>
                <input type="hidden" name="" value="" class="ttt">
              </div>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Baggage^</span>
            <% } else { %>
              <span class="summary-label">Baggage</span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="baggage"></span></div>
            <div class="col-select column-right"><span class="baggage"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Seat selection at booking^</span>
            <% } else { %>
              <span class="summary-label">Seat selection at booking</span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="seat-selection"></span></div>
            <div class="col-select column-right"><span class="seat-selection"></span></div>
          </div>
          <div class="row-select first-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Earn KrisFlyer miles^</span>
            <% } else { %>
              <span class="summary-label">Earn KrisFlyer miles</span>
            <% } %>
            </span></div>
            <div class="col-select column-left"><span class="earn-krisFlyer"></span></div>
            <div class="col-select column-right"><span class="earn-krisFlyer"></span></div>
          </div>
          <div class="row-select last-row">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Upgrade with miles^</span>
            <% } else { %>
              <span class="summary-label">Upgrade with miles</span>
            <% } %>
            </span></div>
            <div class="col-select column-left">
            <span class=" upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong>From Economy Standard (Saver upgrade award) to Premium Economy</strong><p class='tooltip__text-2'>Number of miles required: 5,000 miles</p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
            <div class="col-select column-right">
            <span class="upgrade"></span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong>From Economy Standard (Saver upgrade award) to Premium Economy</strong><p class='tooltip__text-2'>Number of miles required: 5,000 miles</p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %>
            </div>
          </div>
          <div class="row-select first-row">
            <div class="col-select cancel-sgd150-sgdastirisk"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
            <div class="col-select column-left cancel-sgd150-sgdastirisk"><span class="cancellation"></span></div>
            <div class="col-select column-right cancel-sgd150-sgdastirisk"><span class="cancellation"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
            <div class="col-select column-left"><span class="booking-change"></span></div>
            <div class="col-select column-right"><span class="booking-change"></span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
            <div class="col-select column-left"><span class="no-show"></span></div>
            <div class="col-select column-right"><span class="no-show"></span></div>
          </div>
        </div>
        <div class="table-fare-summary column-left hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-1"><span class="code-flight"></span><span class="name-header">Promo</span><input type="hidden" name="" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Baggage^</span>
            <% } else { %>
              <span class="summary-label">Baggage</span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="baggage-price baggage">20kg</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Seat selection at booking^</span>
            <% } else { %>
              <span class="summary-label">Seat selection at booking</span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed seat-selection">Not Allowed</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Earn KrisFlyer miles^</span>
            <% } else { %>
              <span class="summary-label">Earn KrisFlyer miles</span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed earn-krisFlyer">Not Allowed</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <span>Upgrade with miles^</span>
            <% } else { %>
              <span class="summary-label">Upgrade with miles</span>
            <% } %>
            </span></div>
            <div class="col-select"><span class="not-allowed upgrade ">Not Allowed</span>
            <% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong>From Economy Standard (Saver upgrade award) to Premium Economy</strong><p class='tooltip__text-2'>Number of miles required: 5,000 miles</p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
            <div class="col-select"><span class="not-allowed cancellation">Not Allowed</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
            <div class="col-select"><span class="fare-price booking-change">SGD 300</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
            <div class="col-select"><span class="not-allowed no-show">Not Allowed</span></div>
          </div>
        </div>
        <div class="table-fare-summary column-right hidden-tb-dt">
          <div class="row-head-select">
            <div class="col-select">
              <div class="head-wrapper economy-fs--green-2"><span class="code-flight"></span><span class="name-header">Good</span><input type="hidden" name="" value="" class="ttt"></div>
            </div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-business-1"></em>
           <span class="summary-baggage summary-label">Baggage</span>
            </span></div>
            <div class="col-select"><span class="baggage-price baggage ">20kg</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-preferred"></em>
           <span class="summary-seat summary-label">Seat selection at booking</span>
            </span></div>
            <div class="col-select"><span class="fare-price seat-selection">From USD 18</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-icons-42"></em>
            <span class="summary-krisflyer summary-label">Earn KrisFlyer miles</span>
            </span></div>
            <div class="col-select"><span class="fare-price earn-krisFlyer">800 miles</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-7-upgrade-circle"></em>

              <span class="summary-upgrade summary-label">Upgrade with miles</span>
            </span></div>
            <div class="col-select"><span class="fare-price upgrade">1600 miles</span><% if($('body').hasClass('sk-ut-flight-search-b')) { %>
              <em data-tooltip="true" data-max-width="280" tabindex="0" data-content="<div class='tooltip-fare-allowed'><strong>From Economy Standard (Saver upgrade award) to Premium Economy</strong><p class='tooltip__text-2'>Number of miles required: 5,000 miles</p></div>" data-type="3" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill tooltip-upgrade current" aria-label="View more information"></em>
            <% } %></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-cancel-all"></em><span>Cancellation</span></span></div>
            <div class="col-select"><span class="not-allowed cancellation">Not Allowed</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-refresh"></em><span>Booking change</span></span></div>
            <div class="col-select"><span class="not-allowed booking-change">Not Allowed</span></div>
          </div>
          <div class="row-select">
            <div class="col-select"><span class="fare-icon"><em class="ico-close-round-fill"></em><span>No show</span></span></div>
            <div class="col-select"><span class="not-allowed no-show">Not Allowed</span></div>
          </div>
        </div>
        <div class="note-fare">
          <div class="has-note-fare-oal hidden">
            <p class="carret-symbol">^</p>
            <p class="note-fare-desc">These fare conditions are only applicable to Singapore Airlines and SilkAir flights. Refer to the full fare rules and conditions for more information when flying with partner airlines.</p>
          </div>
          <!-- <div class="has-note-fare">
            <p class="carret-symbol">*</p>
            <p class="note-fare-desc">Each of the flight segments you've selected comes with its own fare conditions. When you mix fare types, whether within the
            same cabin class or across cabin classes, fare conditions for cancellation, booking change and no show will follow the more
            restrictive fare type.</p>
          </div> -->
        </div>
        <div class="full-fare-link">
          <a href="#" class="link-4 trigger-popup-add-ons-baggage" data-trigger-popup=".popup--add-ons-summary"><em class="ico-point-r"></em>Full fare rules and conditions</a>
          <% if(!$('body').hasClass('sk-ut-workflow')) { %>
            <p class="des">Each of the flight segments you’ve selected comes with its own fare conditions. However when you mix fare type, fare conditions
            for cancellation, booking change and no show will follow the lower fare type.</p>
          <% } %>
          <span class="fare-complete">This booking is only guranteed when you recieve a confirmation with a booking reference number.</span>
        </div>
      </div>
      <br>
      <div class="button-group-1">
        <div class="after-summary-fare"></div><input type="submit" name="btn-next" id="btn-next" value="proceed" class="btn-1">
        <button data-tooltip="true" data-content="<p class='tooltip__text-2'>Fare secure is not applicable to one <br>or more of your selected flights</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="btn-2 cta-wcag-btn disabled has-tootip  hidden" aria-label="View more information">Secure Fare</button>
        <button class="btn-7 not-tootip flights-details-sf hidden">Secure Fare</button><span class="text hidden">Hold this fare for SGD <%- ocRecommendations.totalPricingAmout.toFixed(2) %></span>
      </div>
    </form>
  <% }); %>
<% } %>
