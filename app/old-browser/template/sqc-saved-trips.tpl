<% _.each(data, function(flight, flightIdx) { %>
  <div data-accordion="1" class="block-2 accordion accordion--1 booking--style-2 <%= flightIdx >= 3 ? 'hidden' : '' %>" data-saved-trip="<%= flightIdx %>" data-url="<%= flight.url %>">
    <div class="accordion__control-inner">
      <div data-accordion-trigger="1" class="accordion__control accordion__control--1" tabindex="0">
        <h3 class="custom-checkbox custom-checkbox--1">
          <input name="flight-<%= flightIdx %>" type="checkbox" id="flight-<%= flightIdx %>" data-checkbox-item="true">
          <label for="flight-<%= flightIdx %>">
            <%
              _.each(flight.trips, function(trip, tripIdx) {
                if(tripIdx > 0) {
                  print(' to ');
                }
                print(trip);
              });
            %> - <%= flight.type %>
          </label>
        </h3>
        <div class="sub-text-1"><%= flight.dateFrom %> - <%= flight.dateTo %></div>
        <em class="ico-point-d hidden"></em>
      </div>
      <span class="loading loading--small">Loading...</span>
      <div class="booking-form hidden">
        <a href="#" class="btn-1"><!-- Manage check-in --></a>
      </div>
    </div>
    <div data-accordion-content="1" class="accordion__content">
      <div class="booking-info-group">
      </div>
    </div>
    <div class="accordion__content-bottom">
      <strong class="sub-title">Passengers:</strong>
      <ul class="inline-list-style" data-passengers="<%= flight.passengers %>"><% _.each(flight.passengers, function(pass, passIdx) { %><% if(passIdx < 3) { %><li class="<% passIdx === 2 ? 'last' : '' %>"><%= pass %><% if(passIdx < 3 - 1) { print(','); } %></li><% } %><% }); %>
      </ul>
      <% if(flight.passengers && flight.passengers.length > 3) { %>
        <a href="#" class="link-4" data-toggle-passengers-list="true">
          <em class="ico-point-r"></em>
          <span class="more">See More</span>
          <span class="less">See Less</span>
        </a>
      <% } %>
    </div>
  </div>
<% }); %>
