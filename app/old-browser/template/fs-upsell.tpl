<a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close</span><span class="text"></span></a>
<div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">
  <div class="upsell">
    <div class="content">Upgrade to<strong> Economy Lite</strong> for an additional<strong> SGD 305.30,</strong> and you'll enjoy:</div>
    <div class="list-items">
      <% if(!$('body').hasClass('sk-ut-workflow')) { %>
      <div class="item"><span class="thumb"><em class="ico ico-business-1"></em><strong class="item-info">+ 5kg</strong></span><span class="des">Additional baggage per passenger</span></div>
      <div class="item"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info">50%</strong></span><span class="des">Earn more KrisFlyer miles with this flight</span></div>
      <div class="item"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info">Flexibility</strong></span><span class="des">Save SGD 100 on booking changes</span></div>
      <div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info">Seats</strong></span><span class="des">Pay for seat selection at booking</span></div>
      <% } else { %>
      <div class="item"><span class="thumb"><em class="ico ico-icons-42"></em><strong class="item-info">50%</strong></span><span class="des">Earn 50% KrisFlyer miles with this flight</span></div>
      <div class="item"><span class="thumb"><em class="ico ico-refresh"></em><strong class="item-info">Flexibility</strong></span><span class="des">Save USD 100 on booking changes</span></div>
      <div class="item"><span class="thumb"><em class="ico ico-change-seat"></em><strong class="item-info">Seats</strong></span><span class="des">Pay for seat selection at booking</span></div>
      <% } %>
    </div>
    <div class="form-group group-btn">
      <% if($('body').hasClass('sk-ut-workflow')) { %>
        <input type="submit" name="btn-upgrade" id="btn-upgrade" value="Upgrade" class="btn-1">
      <% } else { %>
        <input type="submit" name="btn-upgrade" id="btn-upgrade" value="Upgrade" class="btn-1" data-close>
      <% } %>
      <input type="submit" name="btn-keep-selection" id="btn-keep-selection" value="Keep my selection" class="btn-4" data-close>
    </div>
  </div>
</div>
