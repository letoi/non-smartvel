<% if(data) {%>
  <% if(data.flight) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="true" class="group-title active">
          <h4 class="title-4--blue main-title"><span data-number> </span>. Itinerary</h4>
          <h5 class="title-5--dark sub-total" >Sub-total <%- data.currency %> <span> <%- data.fareSubTotal %></span></h5>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.flight, function(dataFlight, idx){ %>
          <div class="booking-info-group bsp-flights__info--group booking-info-group--full">
            <h5 class="title-5--blue"><%- dataFlight.flightSegments[0].carrierCode %><%- dataFlight.flightSegments[0].flightNumber %> - <%- dataFlight.origin %> to <%- dataFlight.destination %></h5>
            <div class="booking-info-content">
              <% _.each(dataFlight.flightSegments, function(dataFlightSegments, idx){ %>
                <div class="flights__info--group">
                  <div class="flights--detail" data-flight-number="O6600"><span>Flight <%- dataFlightSegments.carrierCode %>  <%- dataFlightSegments.flightNumber %><em class="ico-point-d"></em><span class="loading loading--small hidden">Loading...</span></span>
                    <div class="details hidden">
                      <% if(dataFlightSegments.airCraftType) {%>
                        <p>Aircraft type: <%- dataFlightSegments.airCraftType %></p>
                      <% } %>
                      <% if(dataFlightSegments.flyingTimes) {%>
                        <p>Flying time: <%- dataFlightSegments.flyingTimes %> </p>
                      <% } %>
                    </div>
                  </div><span aria-label="Economy" class="flights-type"><%- dataFlightSegments.cabinClassDesc %></span>
                </div>
                <div class="booking-info no-border">
                  <div class="booking-info-item one-half">
                    <div class="booking-desc"><span class="hour"><%- dataFlightSegments.deparure.airportCode %> <%- dataFlightSegments.deparure.time %></span><span class="country-name"><%- dataFlightSegments.deparure.cityName %></span>
                      <div class="booking-content"><span><%- dataFlightSegments.deparure.date %>, <br><%- dataFlightSegments.deparure.airportName %><br>
                      <% if(dataFlightSegments.deparure.terminal !== "" && dataFlightSegments.deparure.terminal) { %> Intl Terminal <% } %> <%- dataFlightSegments.deparure.terminal %></span>
                      </div><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                    </div>
                  </div>
                  <div class="booking-info-item one-half">
                    <div class="booking-desc"><span class="hour"><%- dataFlightSegments.arrival.airportCode %> <%- dataFlightSegments.arrival.time %></span><span class="country-name"><%- dataFlightSegments.arrival.cityName %></span>
                      <div class="booking-content"><span><%- dataFlightSegments.arrival.date %>, <br><%- dataFlightSegments.arrival.airportName %><br> <% if(dataFlightSegments.arrival.terminal !== "" && dataFlightSegments.arrival.terminal) { %> Intl Terminal <% } %> <%- dataFlightSegments.arrival.terminal %></span>
                      </div>
                    </div>
                  </div>
                </div>
                <% if(dataFlightSegments.layoverTime) { %>
                  <div class="flights__info border booking-content"><span>Layover time <%- dataFlightSegments.layoverTime %></span>
                  </div>
                <% } %>
              <% }); %>
              <div class="booking-info booking-info-row">
                <div class="booking-content"><span>Total travel time <%- dataFlight.totalTravelTime %> </span></div>
              </div>
            </div>
          </div>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total">
          <div data-tabindex="true" class="flights-cost">
            <h4 class="flights-cost-title"><span class="text-left">Flights</span><span class="text-right"><%- data.currency %></span></h4>
            <ul class="flights-cost__details">
            <% if(data.paxDetails) {%>
              <li><span>Fare</span><span class="price"></span></li>
              <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
                <li data-fare="true"><span> &#8228; <%- flightPaxDetails.paxName %> </span><span class="price"><%- flightPaxDetails.fareDetails.fareAmount.toFixed(2) %></span></li>
              <% }); %>
            <% } %>
              <li data-taxes="true"><span>Airport/ Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
              <li data-carrier="true"><span>Carrier surcharges</span><span class="price"><%- data.surchargeTotal.toFixed(2) %></span></li>
              <li data-subtotal="true" class="sub-total"><span>Sub-total</span><span class="price"><%- data.fareSubTotal %></span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <% } %>
  <% if(data.paxDetails) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
          <h4 class="title-4--blue main-title"><span data-number> </span>. Passengers and fight-related add-ons</h4>
          <h5 class="title-5--dark sub-total">Sub-total <%- data.currency %> <span>300.00</span></h5>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
          <div data-accordion-wrapper="2">
            <div data-accordion-wrapper-content="2">
              <div class="booking-info-group" data-accordion="2">
                <% if(idxPaxDetails == 0) {%>
                  <div data-accordion-trigger="2" aria-expanded="true" class="group-title disable-click active">
                <% } else { %>
                   <div data-accordion-trigger="2" aria-expanded="true" class="group-title disable-click">
                <% } %>
                  <h5 class="title-5--blue"><%- flightPaxDetails.paxName %></h5><em class="ico-point-d hidden-tb-dt"></em>
                </div>
                <div data-accordion-content="2">
                  <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                    <div class="booking-info-content">
                      <h5 class="title-5--blue"><%- flightDetailsPerFlight.flightNo %> - Singapore to San Francisco</h5>
                      <% if(flightDetailsPerFlight.addonPerPax) {%>
                        <div class="bundle-info"><span><strong>Bundle added - </strong></span><span data-remove-last-add><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %><%- flightAddonPerPax.description.concat(' + ') %><% }); %></span></div>
                      <% } %>
                      <div class="booking-info--2">
                        <div class="booking-info--1__item">
                          <div class="booking-details__group">
                            <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                              <% if(flightAddonPerPax.type == "Seat") {%>
                                <div class="booking-details booking-detail--1">
                                  <div class="booking-col col-1"><em class="ico-change-seat"></em>
                                  </div>
                                  <div class="booking-col col-2">
                                    <div aria-label="Seats" class="Seats">Seats</div>
                                  </div>
                                  <div class="booking-col col-3">
                                    <div class="align-wrapper">
                                      <div class="align-inner">
                                        <div class="has-cols">
                                          <p class="target-info"><%- flightDetailsPerFlight.from %> TO <%- flightDetailsPerFlight.to %></p>
                                          <p class="seat-info"><%- flightAddonPerPax.seatNumber %> <% if(flightAddonPerPax.isPreferredSeat == true) {%>(Preferred Seat)<% } %></p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <% } %>
                              <% if(flightAddonPerPax.type == "Additional Baggage") {%>
                                <div class="booking-details booking-detail--1">
                                  <div class="booking-col col-1"><em class="ico-business-1"></em>
                                  </div>
                                  <div class="booking-col col-2">
                                    <div aria-label="Baggage" class="Baggage">Baggage</div>
                                  </div>
                                  <div class="booking-col col-3">
                                    <div class="align-wrapper">
                                      <div class="align-inner">
                                        <div class="has-cols">
                                          <p class="target-info">Included</p>
                                          <p class="seat-info"><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                                            <% if(flightAddonPerPax.freeBaggagePiece != null) {%>
                                             <%- flightAddonPerPax.freeBaggagePiece %>kg
                                            <% } %>
                                            <% if(flightAddonPerPax.ffpAllowanceDesc != null) {%><br>
                                              <%- flightAddonPerPax.freeBaggageWeight %>kg
                                              as <%- flightAddonPerPax.ffpAllowanceDesc %> member
                                            <% } %>
                                          <% }); %>
                                        </div>
                                        <div class="has-cols">
                                          <p class="target-info">Additional</p>
                                          <p class="seat-info"><% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %> <%- flightAddonPerPax.additionalBaggageWeight %><% }); %>kg</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              <% } %>
                            <% }); %>
                          </div>
                        </div>
                      </div>
                    </div>
                  <% }); %>
                </div>
              </div>
            </div>
          </div>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total has-link">
          <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
            <div data-tabindex="true" class="flights-cost">
              <h4 class="flights-cost-title"><span class="text-left"><%- flightPaxDetails.paxName %></span><span class="text-right"><%- data.currency %></span></h4>
              <ul class="flights-cost__details">
                <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                  <% if(flightDetailsPerFlight.addonPerPax) {%>
                    <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                      <li><span>Bundle for <%- flightDetailsPerFlight.from %> - <%- flightDetailsPerFlight.to %></span><a href="#" class="link-4 link-remove"><em class="ico-point-r"></em>Remove</a><a href="#" class="link-4"><em class="ico-point-r"></em>Change</a>
                      <span class="price"><%- flightAddonPerPax.amount.toFixed(2) %></span>
                      </li>
                    <% }); %>
                  <% } %>
                <% }); %>
                <li class="sub-total"><span>Sub-total</span><span class="price">160.00</span>
                </li>
              </ul>
            </div>
          <% }); %>
        </div>
      </div>
    </div>
  <% } %>
  <% if(data.commonAddons) {%>
    <div class="accordion-item" data-block-accordion-item >
        <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
          <h4 class="title-4--blue main-title"><span data-number> </span>. Trip-related add-ons</h4>
          <h5 class="title-5--dark sub-total">Sub-total <%- data.currency %> <span>893.80</span></h5>
          <em class="ico-point-d"></em>
        </a>
      <div data-accordion-content="1" class="group-content">
        <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
          <% if(flightCommonAddons.type == "Travel Insurance") {%>
            <div class="trip-related">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-aig-logo.png" alt="Travel insurance" longdesc="img-desc.html"/>
              </div>
              <ul class="trip-related-details">
                <li><span class="name">Person covered</span><span class="content" data-remove-last-comma><% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %><%- flightPaxDetails.paxName %>, <% }); %></span></li>
                <li><span class="name">Travel location</span><span class="content"><%- flightCommonAddons.travelLocation %></span></li>
                <li><span class="name">Date range</span><span class="content"><%- flightCommonAddons.description %></span></li>
              </ul>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Hotel") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-agoda-logo.png" alt="Hotel" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title">
                  <h4 class="title-4--blue"><%- flightCommonAddons.name %></h4>
                  <p class="des"><%- flightCommonAddons.address %></p>
                </div>
                <div class="trip-thumb"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details">
                  <li><span class="name">Check-in date:</span><span class="content"><%- flightCommonAddons.checkInDate %></span></li>
                  <li><span class="name">Check-out date:</span><span class="content"><%- flightCommonAddons.checkOutDate %></span></li>
                  <li><span class="name">Guests:</span><span class="content"><%- flightCommonAddons.guest %></span></li>
                  <% _.each(flightCommonAddons.rooms, function(flightCommonAddonsRooms, idxRoom){ %>
                    <li><span class="name">Room types:</span><span class="content"><%- flightCommonAddonsRooms.roomType %></span></li>
                    <li><span class="name">No. of nights:</span><span class="content"><%- flightCommonAddonsRooms.numberOfNights %></span></li>
                    <% if(flightCommonAddonsRooms.breakfast == true) {%>
                      <li><span class="name">Breakfast:</span><span class="content">Included for all rooms</span></li>
                    <% } %>
                  <% }); %>
                </ul>
              </div>
            </div>
          <% } %>
          <% if(flightCommonAddons.type == "Car Rental") {%>
            <div class="trip-related trip-related-1">
              <h4 class="title-4--blue main-title"><%- flightCommonAddons.type %></h4>
              <div class="trip-related-icon"><img src="images/addon-rentalcars-logo.png" alt="Car rental" longdesc="img-desc.html"/>
              </div>
              <div class="trip-related-content">
                <div class="sub-title sub-title-inline">
                  <h4 class="title-4--blue"><%- flightCommonAddons.carType %></h4>
                  <p class="des">or similar</p>
                </div>
                <div class="trip-thumb trip-thumb-inline"><img src="<%- flightCommonAddons.image %>" alt="Trip-related thumbnail" longdesc="img-desc.html"/>
                </div>
                <ul class="trip-related-details shorter-width">
                  <li><span class="name">Pick up:</span><span class="content"><%- flightCommonAddons.pickupDate %></span></li>
                  <li><span class="name">Drop off:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.pickupLocation %></span></li>
                  <li><span class="name">Location:</span><span class="content"><%- flightCommonAddons.dropoffLocation %></span></li>
                </ul>
              </div>
            </div>
          <% } %>
        <% }); %>
        <div data-flight-cost="true" class="mp-payments-total has-link shorter-width">
          <div data-tabindex="true" class="flights-cost flights-fax">
            <h4 class="flights-cost-title"><span class="text-left">ADD-ONS</span><span class="text-right">SGD</span></h4>
            <ul class="flights-cost__details">
              <% var subTotalCommon,
                  amountHotel, amountTravelInsurance,
                  amountCarRental;
              %>
              <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
                <% if(flightCommonAddons.type == "Travel Insurance") {%>
                  <li><span><%- flightCommonAddons.type %></span><a href="#" class="link-4 link-remove"><em class="ico-point-r"></em>Remove</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountTravelInsurance = flightCommonAddons.amount %>
                  </li>
                <% } %>
                <% if(flightCommonAddons.type == "Hotel") {%>
                  <li><span><%- flightCommonAddons.type %></span><a href="#" class="link-4 link-remove"><em class="ico-point-r"></em>Remove</a><a href="#" class="link-4"><em class="ico-point-r"></em>Change</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountHotel = flightCommonAddons.amount %>
                  </li>
                <% } %>
                <% if(flightCommonAddons.type == "Car Rental") {%>
                  <li><span><%- flightCommonAddons.type %></span><a href="#" class="link-4 link-remove"><em class="ico-point-r"></em>Remove</a><a href="#" class="link-4"><em class="ico-point-r"></em>Change</a><span class="price"><%- flightCommonAddons.amount.toFixed(2) %></span>
                  <% amountCarRental = flightCommonAddons.amount %>
                  </li>
                <% } %>
              <% }); %>
              <%
                subTotalCommon = amountHotel + amountTravelInsurance + amountCarRental;
              %>
              <li class="sub-total"><span>Sub-total</span><span class="price"><%- subTotalCommon.toFixed(2) %></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <% } %>
  <div class="accordion-item" data-block-accordion-item>
    <a href="#" data-accordion-trigger="1" aria-expanded="false" class="group-title">
      <h4 class="title-4--blue main-title"><span data-number> </span>. Cost breakdown</h4>
      <h5 class="sub-heading-3--blue sub-total grand-total">Grand total <%- data.currency %> <span>3,971.80</span></h5>
      <em class="ico-point-d"></em>
    </a>
    <div data-accordion-content="1" class="group-content">
      <div data-flight-cost="true" class="mp-payments-total">
        <div data-tabindex="true" class="flights-cost">
          <h4 class="flights-cost-title"><span class="text-left">Flights</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
            <% if(data.paxDetails) {%>
              <li><span>Fare</span><span class="price"></span></li>
              <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
                <li data-fare="true"><span> &#8228; <%- flightPaxDetails.paxName %> </span><span class="price"><%- flightPaxDetails.fareDetails.fareAmount.toFixed(2) %></span></li>
              <% }); %>
            <% } %>
            <li data-taxes="true"><span>Airport/ Government taxes</span><span class="price"><%- data.taxTotal.toFixed(2) %></span></li>
            <li data-carrier="true"><span>Carrier surcharges</span><span class="price"><%- data.surchargeTotal.toFixed(2) %></span></li>
            <li data-subtotal="true" class="sub-total"><span>Sub-total</span><span class="price"><%- data.fareSubTotal %></span></li>
          </ul>
          <h4 class="flights-cost-title"><span class="text-left">Flights Add-ons</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
            <% _.each(data.paxDetails, function(flightPaxDetails, idxPaxDetails){ %>
              <li><span><%- flightPaxDetails.paxName %></span><span class="price"></span></li>
              <% _.each(flightPaxDetails.detailsPerFlight, function(flightDetailsPerFlight, idxPaxDetails){ %>
                <% if(flightDetailsPerFlight.addonPerPax) {%>
                  <% _.each(flightDetailsPerFlight.addonPerPax, function(flightAddonPerPax, idxPaxDetails){ %>
                    <li><span> &#8228; Bundle for <%- flightDetailsPerFlight.from %> - <%- flightDetailsPerFlight.to %></span><span class="price"><%- flightAddonPerPax.amount.toFixed(2) %></span></li>
                  <% }); %>
                <% } %>
              <% }); %>
            <% }); %>
            <li class="sub-total"><span>Sub-total</span><span class="price">300.00</span></li>
          </ul>
          <h4 class="flights-cost-title"><span class="text-left">Trip Add-ons</span><span class="text-right">SGD</span></h4>
          <ul class="flights-cost__details">
            <% _.each(data.commonAddons, function(flightCommonAddons, idxCommonAddons){ %>
              <% if(flightCommonAddons.type == "Travel Insurance") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountTravelInsurance.toFixed(2)  %></span>
                </li>
              <% } %>
              <% if(flightCommonAddons.type == "Hotel") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountHotel.toFixed(2) %></span>
                </li>
              <% } %>
              <% if(flightCommonAddons.type == "Car Rental") {%>
                <li><span><%- flightCommonAddons.type %></span><span class="price"><%- amountCarRental.toFixed(2) %></span>
                </li>
              <% } %>
            <% }); %>
            <li class="sub-total"><span>Sub-total</span><span class="price"><%- subTotalCommon.toFixed(2) %></span>
            </li>
          </ul>
          <div class="grand-total"><span>Grand total</span>
            <div class="grand-total-content">
              <div class="text-total">SGD <span>3,971.80</span></div>
              <div class="des">Includes discounts, tax and surcharges</div>
            </div>
          </div>
          <div class="extra-info">
            <li><span>Payable with KrisFlyer miles -<span class="des"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span></span><span data-krisflyer-miles ><%- data.currency %> <%- data.costPayableByMiles.toFixed(2) %></span></li>
            <li><span data-krisflyer-miles-rest >Not payable with KrisFlyer miles</span><span><%- data.currency %> <%- data.costPayableByCash.toFixed(2) %></span></li>
          </div>
        </div>
      </div><a href="#" class="link-4 link-cost-breakdown trigger-popup-flights-details" data-trigger-popup=".popup--flights-details"><em class="ico-point-r"></em>Cost breakdown by passengers</a>
    </div>
  </div>
<% } %>
