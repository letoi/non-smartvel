<div class="accrual-calculator-result">
  <h3 class="sub-heading-3--dark"><%- data.origin%> to <%- data.destination%> <span><%- data.detail%></span></h3>
  <table class="table-1">
    <thead>
      <tr>
        <% _.each( data.header, function( header, idx ){ %>
          <th class='<%= (idx === 0) ? "" : "align-right" %>'><%- header%></th>
        <%})%>
      </tr>
    </thead>
    <tbody>
      <% _.each( data.classMiles, function( classMile, idx ){ %>
        <tr class='<%= (idx%2 ===0) ? "odd" : "even" %>'>
          <td><%- classMile.category%></td>
          <td class="align-right"><%- classMile.miles%></td>
        </tr>
      <%})%>
    </tbody>
  </table>
  <p><%- data.note%></p>
</div>
