<% if(data) { %>
  <% if(data.beforeFlyItemDetails) { %>
    <div class="heading-wrap">
      <h4 class="sub-heading-2--dark">Before you fly</h4>
    </div>
    <div class="booking-reference__item__content">
      <div class="reference-fly__group">
        <div class="reference-fly__row">
          <% _.each(data.beforeFlyItemDetails, function(itemFly, idx){ %>
            <% if(idx % 2 == 0 && idx != 0) {%>
                </div>
                <div class="reference-fly__row">
              <% } %>
                <div class="reference-fly__items"><a href="<%- itemFly.beforeFlyItem.itemLink %>" class="text-head text-head-dt"><em class="type-ico <%- itemFly.beforeFlyItem.itemIconId %>"></em><%- itemFly.beforeFlyItem.itemTitle %><em class="ico-point-r"></em></a>
                  <a href="<%- itemFly.beforeFlyItem.itemLink %>" class="text-head hidden-tb-dt"><em class="type-ico <%- itemFly.beforeFlyItem.itemIconId %>"></em><%- itemFly.beforeFlyItem.itemTitleMobile %><em class="ico-point-r"></em></a>
                  <p class="details"><%- itemFly.beforeFlyItem.itemDescription %></p>
                </div>
          <% }) %>
        </div>
      </div>
    </div>
  <% } %>
  <% if(data.completeTripItemDetails) { %>
    <div class="heading-wrap">
      <h4 class="sub-heading-2--dark">Complete your trip</h4>
    </div>
    <div class="booking-reference__item__content">
      <div class="reference-fly__group">
        <div class="reference-fly__row">
          <% _.each(data.completeTripItemDetails, function(itemTrip, idx){ %>
             <% if(idx % 2 == 0 && idx != 0) {%>
                </div>
                <div class="reference-fly__row">
              <% } %>
                <div class="reference-fly__items">
                  <a href="<%- itemTrip.completeTripItem.itemLink %>" class="text-head text-head-dt"><em class="type-ico <%- itemTrip.completeTripItem.itemIconId %>"></em><%- itemTrip.completeTripItem.itemTitle %><em class="ico-point-r"></em></a>
                  <a href="<%- itemTrip.completeTripItem.itemLink %>" class="text-head hidden-tb-dt"><em class="type-ico <%- itemTrip.completeTripItem.itemIconId %>"></em><%- itemTrip.completeTripItem.itemTitleMobile %><em class="ico-point-r"></em></a>
                  <p class="details"><%- itemTrip.completeTripItem.itemDescription %></p>
                </div>
          <% }) %>
        </div>
      </div>
    </div>
  <% } %>
<% } %>
