<% if (data && data.length) { %>
  <% _.each(data, function(album, idx) { %>
    <article class="ent-item ent-item--1">
      <div class="ent-item__inner">
        <figure class="ent-item__thumb"><img src="<%- album.src %>" alt="<%- album.name %>" longdesc="<%- album.longdesc %>">
        </figure>
        <section class="ent-item__content">
          <header class="ent-item__title"><strong><%- album.name %></strong></header>
          <section class="ent-item__text">
            <p><%- album.singer %></p>
          </section>
          <footer class="ent-item__footer"><a href="#" class="info-link-1"><em class="ico-music"></em><%- album.category %></a>
          </footer>
        </section>
      </div>
    </article>
  <% }); %>
<% } %>
