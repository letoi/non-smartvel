L10n = {
	home: {
		retrive: 'Retrieve Booking',
		proceed: 'Proceed'
	},
	passengerDetail: {
		proceed: 'Check in',
		next: 'Next',
		nextPassenger: 'Next Passenger'
	},
	fareDeal: {
		nodata: 'There are no fare deals available. Change your selections and try again.',
		nofare: 'There are no fare deals available. Change your selections and try again.'
	},
	flightSelect: {
		aircraftTypeLabel: 'Aircraft type',
		flyingTimeLabel: 'Flying time',
		errorGettingData: 'Not available',
		timeoutGettingData: 'Not available'
	},
	globalSearch: {
		noMatches: 'No results found.'
	},
	payment: {
		min: 'Minimum payable with miles',
		defaultString: 'Maximum payable with miles',
		max: 'Maximum payable with miles',
		convertText: '(Converted from SGD {0})'
	},
	label: {
		preferred: 'Preferred Seat',
		add: 'Add',
		remove: 'Remove'
	},
	accordion: {
		open: jQuery('body').hasClass('faqs-pages') ? '<em class="ico-point-d"></em>Open all answers' : 'See All',
		collapse: jQuery('body').hasClass('faqs-pages') ? '<em class="ico-point-d"></em>Close all answers' : 'Close All'
	},
	emptyData: '',
	bookingSummary: {
		fare: 'Fare',
		texts: {
			titleBS: 'Booking summary',
			paid: 'TOTAL TO BE PAID',
			refund: 'TOTAL TO BE REFUNDED',
			cancellation: 'Cancellation Fee',
			krisFlyerMiles : ' KrisFlyer miles',
			oldItinerary: 'Old itinerary',
			newItinerary: 'New itinerary',
			itinerary: 'Itinerary',
			fareOld: 'Fares <br>(with taxes and surcharges)',
			fareOldWithout: 'Fares <br>(without taxes and surcharges)',
			fareOldPaid: 'Fares Paid <br>(without taxes and surcharges)',
			fareRefundable: 'Fare refundable',
			taxesAndSurcharges: 'Taxes and surcharges',
			taxSurchargeRefundable: 'Taxes and surchages refundable',
			fareNew: 'Fares',
			taxes: 'Airport/Government taxes',
			taxPaid: 'Taxes Paid',
			surcharges: 'Carrier surcharges',
			qSurchargeTotal: 'Qsurchargetotal',
			rebooking: 'Re-booking fee',
			miles: ' miles',
			total: 'Total',
			previouslyPaid: 'Previously paid',
			previouslyNewPaid: 'Offset by previously paid amount',
			addon: 'Add-ons',
			subTotal: 'Sub-total',
			grandTotal: 'Grand total',
			cost: 'COST',
			seat: 'Seats',
			baggage: 'Baggage',
			passengers: 'Passengers',
			creditCard: 'Credit card ending in {0}',
			usedToKrisFlyer: 'KrisFlyer miles used to pay for your booking have expired.',
			remainExpiredMile: 'Remaining expired miles (non-refundable)',
			cancelItinerary: 'Itineray to be cancelled',
			refundBreakdown: 'Refund breakdown',
			secureFareAt: 'SECURE THIS FARE AT',
			secureFare: 'SECURE FARE',
			holdFare: 'Hold this fare',
			includeTax: 'Includes taxes and surcharges'
		},
		paxInfor: {
			adult: ' Adult',
			adults: ' Adults',
			infant: ' Infant',
			infants: ' Infants',
			child: ' Child',
			children: ' Children'
		}
	},
	promotion: {
		outPromotionDay: 'There are no fare deals available. Change your selections or search for regular fares.',
		priceRange : 'Price range ({0})',
		msgInvalidPromo : 'Promo period', //'Valid travel dates for promotional fares',
		msgValidPromo : 'Promo period'
	},
	seatEconomy: {
		notSelected: '<p class="text-info">Seats are not selected for all the passengers – To continue with your current modifications click on save. If you want to choose seats for remaining passengers click on cancel.</p>',
		noinfant: '<h2 class="popup__heading">Information on bassinet seats</h2><p class="text-info">Bassinet seats are only available to passengers with infants.</p>',
		selectedChild: '<p class="text-info">Seat should be selected for a child to continue.</p>',
		withInfantAndChild: '<h2 class="popup__heading">Information on emergency exit seats</h2><p class="text-info">Seats in the emergency exit row are not available to passengers who are pregnant, are under 15 years old, travelling with an infant, and/or require special assistance.</p>',
		preAssignedBassinet: '<p class="text-info">By changing your seat, the bassinet seat will no longer be selected for you and your infant.</p>',
		preAssignedPreferred: '<p class="text-info">With the change of your preferred seat, Singapore Airline will not refund the amount paid for previously selected seat. Please note that new preferred seat is chargeable.</p>'
	},
	preferModal: {
		passenger: 'Passenger(s)',
		seat: 'Selected Seat(s)',
		price: '{0}',
		total: 'Total',
		currency: 'SGD ',
		alert: ''
	},
	kfMiles: {
		nodata: 'You don’t have enough KrisFlyer miles yet. It’s easy to quickly accumulate KrisFlyer miles – just fly with us or our airline partners, or use the services of our partners on the ground and online. <a href="#">Learn more</a>',
		nodataEarn: ''
	},
	kfProfiles: {
		edit: 'Edit',
		save: 'Save',
		showPin: 'Show PIN',
		hidePin: 'Hide PIN'
	},
	unit: {
		miles: 'miles'
	},
	upcomingFlights: {
		errorGettingData: ''
	},
	convert: {
		template :'{0} KrisFlyer miles = {1} Velocity point',
		templatePoints :'{0} KrisFlyer miles = {1} Velocity points'
	},
	orbWaitlistedFlight: {
		failMessage: ''
	},
	validation: {
		email: 'Enter a valid email address.',
		captcha: 'Verify that you are not a robot.'
	},
	travellingPassenger: {
		addRedemtionNominee: 'Add redemption nominee',
		child: 'Child',
		other: 'Others',
		choose: 'Select a travelling passenger'
	},
	multiCity: {
		errorTemplate: ''
	},
	selectMeal: {
		defaultOption: 'Standard Meal',/*'Select meal'*/
		ifMealOption: 'Inflight Meal',
		ifMealOptionLabel: 'Change back to Inflight Meal',
		msgError: 'Not found.',
		msgCategory: 'Flight segment can have only one type of IATA Meal.'
	},
	manageBooking: {
		mealAjaxErr: 'Meal json not found!',
		bagAjaxErr: 'Baggage json not found!',
		addLink: 'Add'
	},
	sqcUser: {
		msgSuccess: 'User(s) successfully deleted.',
		msgError: 'User(s) unsuccessfully deleted.',
		salePointError: 'Point(s) of sale unsuccessfully deleted.',
		addTpl: '<span data-point-id="{0}" class="point-item">{1}<a href="#" class="remove-point"><em class="ico-close"></em></a><input value="{0}" type="hidden" name="point[]"></span>'
	},
	mb: {
		manageBookingMeal: 'No meal selected'
	},
	validator: {
		validateEmail: 'Enter a valid email address',
		validateAtLeastOne: '',
		bookingEticket: '',
		eTicketNumber: '',
		checkofadult: '',
		checkofchild: '',
		checkofinfant: '',
		checkofdatetime: 'Drop off time must be 1 hour later than pick up time.',
		checkofdatetimebefore: 'Pick up time must be before Drop off time.',
		checkofpickuptime: 'Pick up time must be at least 1 hour in the future.',
		notSpecialCharacters: 'Enter letters of the English alphabet only.',
		validdateGreaterDate: '',
		checkdaterange: '',
		requiredIssuePlace: 'Select the passport’s country/region of issue. This may be different from the country/region where the passport was collected.',
		checkpassport: 'Select a valid passport expiry date.',
		checkAlphaNumberic: 'Enter numbers, and letters of the English alphabet only.',
		checkAddrUsa: 'Enter letters of the English alphabet, and if required, any of these symbols /’@()”-',
		checkCurrentDate: 'Your passport must be valid for at least six months from your arrival date.',
		checkKfMembershipNumber: 'Enter a valid 10-digit KrisFlyer membership number.',
		checkBookingDate: 'Check-in date must be greater than the booking date 3 days.',
		checkCheckoutDate: 'The numbers of nights should be greater than 0 and less than 10.',
		digitsWithPrefix: 'This field must contain the + prefix and digits.',
		requireAtleastCheckbox: 'Atleast 1 checkbox must be checked.',
		selectOneOpt: 'Select at least one option.',
		webUrl: 'Enter a valid company website address (URL).',
		validateDropDown: 'Select a valid country!'
	},
	truncateBlock: {
		readMore: 'Read more'
	},
	ssh: {
		room: 'Room',
		title: 'Singapore Stopover Holiday',
		insuranceTitle: 'Travel Insurance'
	},
	promotionList: {
		to: {
			defaultOption: 'All'
		}
	},
	option: {
		all: 'all'
	},
	sshSelection: {
		error: 'The selected adults and childs must be same as the booking.'
	},
	kfVoucher: {
		selectLabel: 'Select'
	},
	kfSeemore: {
		seeMore: 'See more',
		seeAll: 'See all',
		loadMore: 'Load more'
	},
	flightStatus: {
		nodata: 'There are no seats available for the journey you\'ve selected. Change your selections or get in touch with your local Singapore Airlines office.'
	},
	oparated: 'Operated by SilkAir',
	seatMap: {
		pax: 'You are now on the seat map of your flight. Press tab to select the passenger. Then press enter to start selecting your seats.',
		deck: {
			main: 'You are now on the main deck of your flight. To navigate through the available seats, press the left or right arrow keys. Then press enter to select your preferred seat.',
			upper: 'You are now on the upper deck of your flight. To navigate through the available seats, press the left or right arrow keys. Then press enter to select your preferred seat.'
		},
		seat: {
			normal: 'Seat {seatlabel} is available. Press enter to select this seat.',
			bassinet: 'Seat {seatlabel} is a bassinet seat and is available. Press enter to select this seat.',
			preferred: 'Seat {seatlabel} is a preferred seat and is available for {seatprice}. Press enter to select this seat.'
		}
	},
	fareCalendar:{
		firstSeat: 'You are now on the flight calendar table. To navigate, press the left or right arrow keys.',
		seat: '{seatDepart}. {seatReturn}. Total: {seatAmount}. This fare is selected. Press enter to continue booking.'
	},
	sliders: {
		carousel: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of items'
		},
		carouselFade: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of items'
		},
		flex: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of items'
		},
		highlights: {
			prevArrowText: 'Previous',
			prevArrowLabel: 'View previous set of highlights',
			nextArrowText: 'Next',
			nextArrowLabel: 'View next set of highlights',
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of highlights'
		},
		homebanner: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of messages'
		},
		krisFlyer: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of Kris deals'
		},
		moreInTheSection: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of teasers'
		},
		packages: {
			prevArrowText: 'Previous',
			prevArrowLabel: 'View previous set of packages',
			nextArrowText: 'Next',
			nextArrowLabel: 'View next set of packages',
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of packages'
		},
		promotions: {
			prevArrowText: 'Previous',
			prevArrowLabel: 'View previous set of promotions',
			nextArrowText: 'Next',
			nextArrowLabel: 'View next set of promotions',
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of promotions'
		},
		whereToStay: {
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of places to stay'
		},
		youtubeList: {
			prevArrowText: 'Previous',
			prevArrowLabel: 'View previous set of videos',
			nextArrowText: 'Next',
			nextArrowLabel: 'View next set of videos',
			customPagingTextPre: 'Set ',
			customPagingTextPost: ' of videos'
		}
	},
	desCityGuide: {
		rating: {
			starSingle: ' star',
			starPlural: ' stars',
			starAndAHalf: ' and a half'
		}
	},
	landmarks: {
		languageToolbar: 'Language',
		languageToolbarClose: 'Close language toolbar',
		travelWidget: 'Booking form'
	},
	labels: {
		displayInEnglishTrue: 'Display this page in English',
		displayInEnglishFalse: 'Display this page in Simplified Chinese',
		menuBarLanguageButton: 'Select location and language',
		menuBarSearchButton: 'Search this site',
		newsTickerViewAll: 'View all news bulletins',
		packagesViewAll: 'View all packages'
	},
	tips: {
		customSelectUsage: {
			windows: 'Press space to open combobox, and up, down, and enter keys, to select an option',
			osx: 'Press space to open combobox, and VoiceOver keys with up, down, and space, to select an option'
		},
		datePickerUsage: {
			windows: 'Select the date by keying in two-digit day, two-digit month, four-digit year. Or press control + left or right arrow keys to navigate through the date and press Enter to select the preferred date.',
			osx: 'Select the date by keying in two-digit day, two-digit month, four-digit year. Or press command + left or right arrow keys to navigate through the date and press Enter to select the preferred date.'
		}
	},
	wcag: {
		favouriteLable: 'favourite',
		unfavouriteLable: 'unfavourite',
		seemoreLabel: 'Load more {0} item(s)',
		foundLabel: 'Found {0} item(s)',
		updateSuccess: 'Update success',
		sliderLabel: 'Slider value min: {0}, max: {1}'
	}
};
