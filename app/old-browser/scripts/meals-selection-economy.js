if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

SIA.MealsSelection = function() {
    var undoMeal;
    var p = {};
    var mealCode = {
      SPML: "Special Meal",
      BTC: "Book the Cook"
    };

    p.global = SIA.global;
    p.body = p.global.vars.body;
    p.win = p.global.vars.win;
    p.htmlBody = $('html, body');
    p.stickySidebar = SIA.stickySidebar;

    // Declare templates
    p.removeButton = '<a class="meal-menu-remove-button" data-remove-trigger="1" href="#">Remove</a>';
    p.accordionWrapperTpl = '<div id="accordionWrapper" class="meals-accordion-wrapper <%= initClass %>" />';
    p.fltBlkTpl = '<div class="flightmeal-control" />';
    p.fltTitleTpl = '<h3 class="title-4--blue" />';
    p.accordionWrapTpl = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper"><div data-accordion-wrapper-content="1" class="accordion-wrapper-content"></div></div>';
    p.accordionItemWrapTpl = '<div data-accordion="1" data-accordion-index="<%= id %>" class="block-2 accordion accordion-box" />';
    p.accordionTriggerTpl = '<a href="#" data-accordion-trigger="1" class="accordion__control">\
          <div class="fm-accordion-label">\
            <ul class="meal-menu">\
              <li class="flight-ind"><%= origin %> - <%= destination %></li>\
              <li class="meal-service-name"><%= mealType %></li>\
              <li class="meal-sub-title-black" data-label="<%= menuSelectionLbl %>"><%= menuSelection %></li>\
            </ul>\
            <span class="fm-ico-text select-change-container"><%= label %></span>\
            <em class="ico-point-d"></em>\
          </div>\
        </a>';

    p.accordionContentTpl = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
            <div class="meals-main-menu-wrap all-meals-menu">\
                <hr class="hr-line"/>\
                <h3 class="fm-title-4--blue">Select a meal</h3>\
                <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                    <article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %>">\
                        <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available">\
                                        </div>\
                                    </div>\
                                    <img src="<%= inflImg %>" alt="<%= inflDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span class="view-only-btn"> VIEW ONLY </span>\
                                        <span><%= inflLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                    <article class="promotion-item promotion-item--1">\
                        <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available">\
                                        </div>\
                                    </div>\
                                    <img src="<%= btcImg %>" alt="<%= btcDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span><%= btcLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                    <article class="promotion-item promotion-item--1">\
                        <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                            <figure>\
                                <div class="flight-item">\
                                    <div class="overlay-meal">\
                                        <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                        <div class="overlay-not-available"></div>\
                                    </div>\
                                    <img src="<%= spmlImg %>" alt="<%= spmlDesc %>" longdesc="img-desc.html">\
                                    <div class="meal-label">\
                                        <span><%= spmlLabel %></span>\
                                        <em class="ico-point-r"></em>\
                                    </div>\
                                </div>\
                            </figure>\
                        </a>\
                    </article>\
                </div>\
                <hr>\
                <div class="not-eating-supper">\
                    <div class="custom-checkbox custom-checkbox--1">\
                        <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                        <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&#8217d prefer not to have <%= mealType %></label>\
                        <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&#8217t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                        Or simply approach our flight attendants on board, who will be happy to serve you any meal that&#8217s available from our Inflight Menu.</p>\
                    </div>\
                </div>\
            </div>\
            <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-in-left hidden">\
                <hr>\
                <div class="fm-backtomeals-btn"></div>\
                <div class="menu-container"></div>\
                <div class="fm-backtomeals-btn floater">\
                    <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
                </div>\
            </div>\
            <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-in-left hidden">\
                <hr>\
                <div class="fm-backtomeals-btn"></div>\
                <div class="fm-backtomeals-btn floater">\
                    <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
                </div>\
                <h2 class="fm-title-blue">Special Meals</h2>\
                <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
                before departure, or 48 hours for nut-free and Kosher meals.</p>\
                <div class="tab-wrapper">\
                    <div class="tab-content active">\
                        <div class="form-seatmap responsive special-meals"></div>\
                    </div>\
                </div>\
            </div>\
        </div>';

    p.accordionContentTplNoImage = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
          <div class="meals-main-menu-wrap all-meals-menu">\
              <hr/>\
              <h3 class="fm-title-4--blue">Select a meal</h3>\
              <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                  <article class="promotion-item promotion-item--1 <%= mealNotReady %> <%= viewOnly %> no-image">\
                      <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                          <figure>\
                              <div class="flight-item">\
                                  <div class="overlay-meal">\
                                      <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                      <div class="overlay-not-available">\
                                      </div>\
                                  </div>\
                                  <div class="meal-label">\
                                    <span class="view-only-btn"> VIEW ONLY </span>\
                                    <span><%= inflLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= inflDesc %></p>\
                                    <p class="not-available-text">The menu will be available for selection from 7 September onwards</p>\
                                  </div>\
                              </div>\
                          </figure>\
                      </a>\
                  </article>\
                  <article class="promotion-item promotion-item--1 no-image">\
                      <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item ">\
                          <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label">\
                                    <span><%= btcLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= btcDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                      </a>\
                  </article>\
                  <article class="promotion-item promotion-item--1 no-image">\
                      <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                          <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label">\
                                    <span><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= spmlDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                      </a>\
                  </article>\
              </div>\
              <hr>\
              <div class="not-eating-supper">\
                  <div class="custom-checkbox custom-checkbox--1">\
                      <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                      <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&#8217d prefer not to have <%= mealType %></label>\
                      <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&#8217t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                      Or simply approach our flight attendants on board, who will be happy to serve you any meal that&#8217s available from our Inflight Menu.</p>\
                  </div>\
              </div>\
          </div>\
          <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-in-left hidden">\
              <hr>\
              <div class="fm-backtomeals-btn"></div>\
              <div class="menu-container"></div>\
              <div class="fm-backtomeals-btn floater">\
                  <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
              </div>\
          </div>\
          <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-in-left hidden">\
              <hr>\
              <div class="fm-backtomeals-btn"></div>\
              <div class="fm-backtomeals-btn floater">\
                  <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
              </div>\
              <h2 class="fm-title-blue">Special Meals</h2>\
              <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
              before departure, or 48 hours for nut-free and Kosher meals.</p>\
              <div class="tab-wrapper">\
                  <div class="tab-content active">\
                      <div class="form-seatmap responsive special-meals"></div>\
                  </div>\
              </div>\
          </div>\
      </div>';

    p.accordionContentTplEco = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr class="hr-line"/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
            <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                <article class="promotion-item promotion-item--1">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item inflight-economy-hr">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label ml-inflight-container">\
                                    <span class="inflight-inner-container">\
                                        <%= inflLabel %> - Select your meal on board on board your flight\
                                        <span class="pull-right select-label">Select</span>\
                                        <span class="inflight-badge hidden">Selected</span>\
                                    </span>\
                                </div>\
                                <div>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <hr class="horizontal-line hr-line">\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label epe-meal-container">\
                                    <span class="special-meal-inner-container"><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= spmlDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
            <hr class="hr-line">\
            <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&#8217;d prefer not to have <%= mealType %></label>\
                    <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&#8217;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                    Or simply approach our flight attendants on board, who will be happy to serve you any meal that&#8217;s available from our Inflight Menu.</p>\
                </div>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <h2 class="fm-title-blue">Special Meals</h2>\
            <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
            before departure, or 48 hours for nut-free and Kosher meals.</p>\
            <div class="tab-wrapper">\
                <div class="tab-content active">\
                    <div class="form-seatmap responsive special-meals"></div>\
                </div>\
            </div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
    </div>';
    
    p.accordionContentTplPey = '<div data-accordion-content="1" class="accordion__content all-meals-list" tabindex="-1">\
        <div class="meals-main-menu-wrap all-meals-menu">\
            <hr class="hr-line"/>\
            <h3 class="fm-title-4--blue">Select a meal</h3>\
            <div class="item-container" data-segment="<%= flightSegment %>" data-sector="<%= flightSector %>" data-curpax="<%= curPax %>" data-service="<%= service %>" data-meal-service-code="<%= mealServiceCode %>">\
                <article class="promotion-item promotion-item--1">\
                    <a href="#" class="promotion-item__inner inflight-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item inflight-economy-hr">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label ml-inflight-container">\
                                    <span class="inflight-inner-container">\
                                        <%= inflLabel %> - Select your meal on board on board your flight\
                                        <span class="pull-right select-label">Select</span>\
                                        <span class="inflight-badge hidden">Selected</span>\
                                    </span>\
                                </div>\
                                <div>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <hr class="horizontal-line hr-line">\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner book-the-cook-menu meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available">\
                                    </div>\
                                </div>\
                                <div class="meal-label epe-meal-container">\
                                    <span><%= btcLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= btcDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
                <article class="promotion-item promotion-item--1 no-image">\
                    <a href="#" class="promotion-item__inner special-meals meals-menu-item">\
                        <figure>\
                            <div class="flight-item">\
                                <div class="overlay-meal">\
                                    <p class="not-available-text"><%= mealNotReadyMsg %></p>\
                                    <div class="overlay-not-available"></div>\
                                </div>\
                                <div class="meal-label epe-meal-container">\
                                    <span class="special-meal-inner-container"><%= spmlLabel %></span>\
                                    <em class="ico-point-r"></em>\
                                    <p class="meal-category-desc"><%= spmlDesc %></p>\
                                </div>\
                            </div>\
                        </figure>\
                    </a>\
                </article>\
            </div>\
            <hr class="hr-line">\
            <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                    <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-type="<%= mealType %>" class="not-eating" <%= checked %>>\
                    <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">I&#8217;d prefer not to have <%= mealType %></label>\
                    <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you won&#8217;t be having <%= mealType %>. If you change your mind, you can always select a meal by returning to this page at least 24 hours before departure.\
                    Or simply approach our flight attendants on board, who will be happy to serve you any meal that&#8217;s available from our Inflight Menu.</p>\
                </div>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap inflight-menu-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <div class="menu-container"></div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
        <div class="meal-sub-menu-wrap special-meal special-meal-wrap pre-slide-right hidden">\
            <hr>\
            <div class="fm-backtomeals-btn"></div>\
            <h2 class="fm-title-blue">Special Meals</h2>\
            <p class="fm-inf-menu-desc">Our Special Meals are specially tailored to meet specific dietary requirements you might have. Just make your selection at least 24 hours\
            before departure, or 48 hours for nut-free and Kosher meals.</p>\
            <div class="tab-wrapper">\
                <div class="tab-content active">\
                    <div class="form-seatmap responsive special-meals"></div>\
                </div>\
            </div>\
            <div class="fm-backtomeals-btn floater">\
                <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back </a>\
            </div>\
        </div>\
    </div>';

    p.accordionInfantContentTpl = '<div data-accordion-content="1" class="accordion__content" tabindex="-1">\
          <div class="meal-sub-menu-wrap inflight-menu-wrap infant-meal-wrap">\
              <hr>\
              <div class="fm-backtomeals-btn"></div>\
              <div class="infant-meals all-meals-list">\
                  <h2 class="fm-title-blue">Infant Meals</h2>\
              </div>\
              <hr>\
              <div class="not-eating-supper">\
                <div class="custom-checkbox custom-checkbox--1">\
                  <input name="not-eating-<%= mealType %>-<%= id %>" id="not-eating-<%= mealType %>-<%= id %>" type="checkbox" data-checktype="not-eating" data-target-trigger="<%= targetTrigger %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" class="not-eating" <%= checked %>>\
                  <label for="not-eating-<%= mealType %>-<%= id %>" class="fm-cbox">Bring my own meal</label>\
                  <p class="not-eating-msg <%= notEatingMsg %>">Thank you for indicating that you will be bringing your own meal. Our flight attendants will not approach you during mealtime. If you change your mind, however, you still select your meal by returning to this page at least 24 hours before your departure.</p>\
                </div>\
              </div>\
          </div>\
      </div';
    p.checkinAlert = '<div class="alert-block checkin-alert">\
      <div class="inner">\
        <div class="alert__icon"><em class="ico-alert"> </em></div>\
        <div class="alert__message"><%= checknMsg %></div>\
      </div>\
    </div>';
    p.mealsLabelTpl = '<span class="meals-label btn-small btn-grey">VIEW ONLY</span>';
    p.inflightAlertPrompt = $('<div class="inflight-confirmation-prompt confirmation-prompt">\
      <em class="ico-alert inflight-ico-alert"></em>\
      <p class="meal-sub-title-black">You&#8217;ve changed your meal choice to <span>Inflight Menu</span>. <a href="#" class="inflight-link-undo"> <em class="ico-point-r"></em> Undo</a></p>\
    </div>');

    // To be used to index each vertical accordion tab to reference when changing arrow text
    p.accordTabArr = [];
    p.accordionTabIndex = 0;

    p.accordionTabOpenTxt = 'Select / Change';
    p.accordionTabCloseTxt = 'Close';

    p.curPaxIndex = 0;
    p.paxNavCount = 0;
    p.paxArr = [];
    p.inflightMenuExist = [];

    p.ifm = [];
    p.spm = [];
    p.btc = [];
    p.infm = [];

    p.cabinClassCodes = {
      pey: ['P', 'T', 'S'],
      eco: ['N', 'Q', 'W', 'H', 'M', 'E', 'B', 'Y']
    };

    p.isPey = false;
    p.isEco = false;

    // declare dish icon for inflight menu scenario
    p.inflightDishIcons = {
        'ICP': 'ico-4-salmon',
        'DWH': 'ico-4-fork-1',
        'WHS': 'ico-4-fork',
        'EPG': 'ico-4-amet',
        'PLF': 'ico-4-cook',
        'MTL': 'ico-4-fork',
        'VGT': 'ico-4-leaf',
        'LCL': 'ico-4-heart',
        'LCA': 'ico-4-sandwich',
        'LCH': 'ico-4-fork',
        'CNY': 'ico-4-info',
        'XMAS': 'ico-4-pine',
        'DEP': 'ico-4-coffee',
        'TWG': 'ico-4-fork',
        'LFD': 'ico-4-cook'
    };

    // init pax data
    p.data = {};

    var init = function(){
        p.json = globalJson.mealsInfo;

        // Assign the initial states
        getInitialStates();

        p.mealSelectionForm = $('#mealSelection');
        p.paxListWrapper = $('#paxListWrapper');
        p.curWinWidth = p.win.width();

        // Add hidden input field in form
        p.mealSelectionForm.prepend(p.input);

        // Update the initial data for the pax
        setupData();

        // Paint meals menu
        paintMealsMenu();

        // add event to checkbox
        addCheckboxEvents();

        // Paint pax navigation
        paintPaxNav();
    };

    var removeButtonListener = function() {
      setTimeout(function() {
          $("[data-accordion-trigger]").find("[data-remove-trigger]").off()
              .on("click" ,function(evt) {
                  var self = $(this);
                  var wrapper = self.closest(".accordion-box");
                  var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
                  var inflightBadge = wrapper.find(".inflight-badge");
                  
                  inflightBadge.removeClass("hidden");
                  inflightBadge.addClass("badge-active");
                  wrapperInflightMenu.addClass("selected-border");
                  wrapperInflightMenu.find(".select-label").addClass("hidden");

                  wrapper.find(".fm-inf-menu.selected").each(function() {
                      var self = $(this);
                      var mealButton = self.find(".fm-select-btn");
                      unSelectMeal(mealButton);
                      undoMeal = mealButton;
                  });

                  wrapper.find(".meal-sub-title-black").text("Inflight Menu");
                  removeInputData(wrapper);
                  evt.preventDefault();
                  evt.stopPropagation();
              });
      }, 500);
    };

    var setupData = function(){
        // Add hidden input field in form
        p.input = $('<input type="hidden" value="" />');
        p.mealSelectionForm.prepend(p.input);

        p.data = {};
        p.data.paxList = [];

        // Loop through the passenger list
        for (var i = 0; i < p.json.paxList.length; i++) {
            var d = p.json.paxList[i];
            var f = p.json.flightSegment;

            var pax = {};
            // Add base pax info
            pax.paxId = d.paxId;
            pax.paxType = d.paxType;
            pax.paxName = d.paxName;

            // Add selected meals array
            // to hold each flightSector's mealServiceCode
            pax.selectedMeals = [];

            for (var j = 0; j < f.length; j++) {
                var segment = {
                    'segmentId': f[j].segmentId,
                    'selectedMeal':{}
                }

                // loop through the sectors
                for (var k = 0; k < f[j].flightSector.length; k++) {
                    var paxServiceType = getServiceType(i);
                    var serviceTypes = f[j].flightSector[k][paxServiceType];

                    for (var l = 0; l < serviceTypes.length; l++) {
                        var mealServiceCode = serviceTypes[l].mealServiceCode;
                        // setup initial values for each service code
                        var mealSelection = {
                            'isEating': true,
                            'isALaCarte': false,
                            'mealCategoryCode':null,
                            'mealCode': null,
                            'mealMenuName': null,
                            'aLaCarteData': null
                        }
                        segment.selectedMeal[mealServiceCode] = mealSelection;
                    }
                }

                pax.selectedMeals.push(segment);
            }

            p.data.paxList.push(pax);

        }
    };

    var updateData = function(menu){
        // Update data
        var pid = parseInt(menu.attr('data-paxid'));
        var sid = parseInt(menu.attr('data-segment'));
        var service = menu.attr('data-meal-servicecode');
        var categoryCode = menu.attr('data-meal-category');
        var mealCode = menu.attr('data-meal-mealcode');
        var mealMenuName = menu.attr('data-meal-menuname');
        var isALC = menu.attr('data-isalc');
        var aLaCarteData = null;

        if(typeof menu.attr('data-alc-json') != 'undefined') aLaCarteData = JSON.parse(menu.attr('data-alc-json'));

        var meal = p.data.paxList[pid].selectedMeals[sid].selectedMeal[service];

        // Check if element clicked is a checkbox
        if(menu.hasClass('not-eating')) {
            meal.isEating = !menu.prop('checked');
            meal.mealCode = null;
            meal.mealMenuName = null;
        } else if(isALC && typeof aLaCarteData != 'undefined') {
            meal.isEating = true;
            meal.mealCode = mealCode;
            meal.mealCategoryCode = categoryCode;
            meal.mealMenuName = mealMenuName;
            meal.isALaCarte = isALC;
            meal.aLaCarteData = aLaCarteData;
        } else {
            if(menu.hasClass('selected')) {
                meal.isEating = true;
                meal.mealCode = mealCode;
                meal.mealCategoryCode = categoryCode;
                meal.mealMenuName = mealMenuName;
                meal.isALaCarte = isALC;
            }else {
                meal.mealCode = null;
                meal.mealCategoryCode = null;
                meal.mealMenuName = null;
            }
        }

        p.input.val(JSON.stringify(p.data));
    };

    var getInitialStates = function(){
        for (var i = 0; i < p.json.paxList.length; i++) {
            if(p.json.paxList[i].paxId === p.json.passengerStartingPoint) {
                p.curPaxIndex = i;
                p.passengerStartingPoint = i;
            }
        }

        for (var i = 0; i < p.json.flightSegment.length; i++) {
            if(p.json.flightSegment[i].segmentId == p.json.startSegment) p.startSegment = i;
        }

        for (var i = 0; i < p.json.flightSegment[p.startSegment].flightSector.length; i++) {
            if(p.json.flightSegment[p.startSegment].flightSector[i].sectorId == p.json.startSector) p.startSector = i;
        }

        p.startMealService = p.json.startMealService;
    };

    var paintMealsMenu = function(initClass) {
        // Reset initial values
        p.accordionTabIndex = 0;
        var segmentId, sectorId, paxId, mealCategoryCode, mealServiceCode = null;

        // Create new accordion wrapper
        p.accordionWrapper = $(_.template(p.accordionWrapperTpl, { 'initClass': initClass }));
        
        for (var i = 0; i < p.json.flightSegment.length; i++) {
            // create new flight block
            var flSegment = $(p.fltBlkTpl);

            // create flight segment title
            var segmentTitle = $(p.fltTitleTpl);
            segmentTitle.html((i + 1) + '. ' + p.json.flightSegment[i].departureAirportName + ' to ' + p.json.flightSegment[i].arrivalAirportName);

            // Add title to segment block
            flSegment.append(segmentTitle);

            // check if second segment available for meal selection
            secSegmentAvailable(flSegment, i);

            // create accordion-wrapper block
            var accordionWrap = $(p.accordionWrapTpl);

            var accordionContentWrap = accordionWrap.find('.accordion-wrapper-content');

            // Check cabin class
            p.isPey = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.pey); // Check if PEY
            p.isEco = checkCabinClass(p.json.flightSegment[i].cabinClassCode, p.cabinClassCodes.eco); // Check if Economy


            for (var j = 0; j < p.json.flightSegment[i].flightSector.length; j++) {
                var data = p.json.flightSegment[i].flightSector[j];
                // get current pax type if adult, child or infant
                p.serviceType = getServiceType(p.curPaxIndex);
                var mealService = data[p.serviceType];

                for (var k = 0; k < mealService.length; k++) {
                    // Activate the accordion based on start states
                    var accLabel = p.accordionTabOpenTxt;

                    // Check if infant
                    var mealCategoryCode, mealServiceCode;
                    if (p.serviceType == 'mealServicesInfant') {
                        mealCategoryCode = mealService[k].allMeals[0].mealCategoryInfant[0].mealCategoryCode;
                        mealServiceCode = mealService[k].mealServiceCode;
                    } else {
                        mealCategoryCode = mealService[k].allMeals[1].mealCategoryInflight[0].mealCategoryCode;
                        mealServiceCode = mealService[k].mealServiceCode;
                    }

                    // Check if current pax selected is the starting passenger
                    // If its not, open the first sector & segment instead
                    if (p.curPaxIndex == p.passengerStartingPoint) {
                        if (i == p.startSegment && j == p.startSector && mealService[k].mealServiceCode == p.startMealService) {
                            accLabel = p.accordionTabCloseTxt;
                        }
                    } else {
                        if (p.accordionTabIndex < 1) {
                            accLabel = p.accordionTabCloseTxt;
                        }
                    }

                    // Each meal service will need an accordian item wrapper
                    var accItemWrap = $(_.template(p.accordionItemWrapTpl, { 'id': (p.accordionTabIndex + 1) }));

                    // add flag for inflight menu
                    p.inflightMenuExist[(p.accordionTabIndex + 1)] = false;

                    // check if is eating
                    var curPaxData = p.data.paxList[p.curPaxIndex].selectedMeals[i].selectedMeal[mealService[k].mealServiceCode];

                    var inflLbl = 'Inflight Menu';
                    if(p.isPey || p.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                    
                    var menuLabel = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : inflLbl;
                    var menuSelectionLbl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? 'Infant Meal' : 'Inflight Menu';

                    if (curPaxData.mealMenuName && curPaxData.isEating) {
                        // Add meal category indicator
                        var mealTypeIndicator;
                        if (curPaxData.mealCategoryCode === 'SPML') mealTypeIndicator = ' (Special Meal)';
                        if (curPaxData.mealCategoryCode === 'INFM') mealTypeIndicator = ' (Inflight Meal)';
                        if (curPaxData.mealCategoryCode === 'BTC') mealTypeIndicator = ' (Book the Cook)';
                        if (curPaxData.mealCategoryCode === 'INF') mealTypeIndicator = ' (Infant Meal)';

                        menuLabel = curPaxData.mealMenuName + mealTypeIndicator;
                    }

                    // Check if theres existing A la carte data
                    if (curPaxData.isALaCarte === 'true') {
                        var parentAccMealSummary = '';
                        _.each(curPaxData.aLaCarteData, function (v, k, l) {
                            if (v.mealMenuName != null) {
                                // set the title info
                                parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';
                            }

                        });
                        menuLabel = parentAccMealSummary.substring(4);
                    }

                    if (!curPaxData.isEating) {
                        menuLabel = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + mealService[k].mealServiceName;
                    }

                    // Create accordion trigger
                    var accTrigger = $(_.template(p.accordionTriggerTpl, {
                        'origin': data.departureAirportCode,
                        'destination': data.arrivalAirportCode,
                        'mealType': mealService[k].mealServiceName,
                        'menuSelection': menuLabel,
                        'menuSelectionLbl': menuSelectionLbl,
                        'label': accLabel
                    }));

                    if (curPaxData.mealCategoryCode == "SPML" || curPaxData.mealCategoryCode == "BTC") {
                        accTrigger.find("li:nth-child(3)").append(p.removeButton);
                        removeButtonListener();
                    }

                    accTrigger.on({
                        'click': function () {
                            resetAccordionLabels($(this));
                        }
                    });

                    p.accordTabArr.push(accTrigger);

                    if (accLabel == 'Close') {
                        accTrigger.addClass('active').attr('aria-expanded', 'true');
                    }

                    // Check all meals availability attribute only shows up in inflight meal
                    var mealAvailData = mealsAvailable(mealService, k);

                    // get contentTpl option
                    var allMeals = mealService[k].allMeals;
                    var hasNoImg = false,
                        btcImg, inflImg, spmlImg,
                        btcLabel, inflLabel, spmlLabel,
                        btcDesc, inflDesc, spmlDesc,
                        hasBtcImg = true, hasInflImg = true, hasSpmlImg = true;

                    // Check if any of the menu types have no image, if one is missing use the no Image tpl
                    for (var m = 0; m < allMeals.length; m++) {
                        if (typeof allMeals[m].mealCategoryBTC !== 'undefined') {
                            btcImg = allMeals[m].mealCategoryBTC[0].mealCategoryImage;
                            btcDesc = allMeals[m].mealCategoryBTC[0].mealCategoryDescription;
                            btcLabel = allMeals[m].mealCategoryBTC[0].mealMenuCategoryName;

                            if (typeof btcImg === 'undefined' || btcImg === '') hasBtcImg = false;
                        }
                        if (typeof allMeals[m].mealCategoryInflight !== 'undefined') {
                            inflImg = allMeals[m].mealCategoryInflight[0].mealCategoryImage;
                            inflDesc = allMeals[m].mealCategoryInflight[0].mealCategoryDescription;
                            inflLabel = allMeals[m].mealCategoryInflight[0].mealMenuCategoryName;

                            if (typeof inflImg === 'undefined' || inflImg === '') hasInflImg = false;
                        }
                        if (typeof allMeals[m].mealCategorySPML !== 'undefined') {
                            spmlImg = allMeals[m].mealCategorySPML[0].mealCategoryImage;
                            spmlDesc = allMeals[m].mealCategorySPML[0].mealCategoryDescription;
                            spmlLabel = allMeals[m].mealCategorySPML[0].mealMenuCategoryName;
                            if (typeof spmlImg === 'undefined' || spmlImg === '') hasSpmlImg = false;
                        }
                    }

                    var nContentTpl = (!hasBtcImg || !hasInflImg || !hasSpmlImg) ? p.accordionContentTplNoImage : p.accordionContentTpl;

                    // Change content based on cabin class
                    if(p.isPey) nContentTpl = p.accordionContentTplPey;
                    if(p.isEco) nContentTpl = p.accordionContentTplEco;

                    // Create accordion content
                    var contentTpl = p.curPaxObj.paxType.toLowerCase() == 'infant' ? p.accordionInfantContentTpl : nContentTpl;

                    // check if meal is not available for order
                    mealsSelectionAvail = p.curPaxObj.paxType.toLowerCase() != 'infant' ? checkMealsSelAvailable(mealService, k) :
                        { 'mealsLabel': '', 'viewOnly': '' };

                    var checked = '';
                    var notEatingMsg = 'hidden';
                    if (!curPaxData.isEating) {
                        checked = 'checked';
                        notEatingMsg = '';
                    }
                    
                    var accContent = $(_.template(contentTpl, {
                        'mealNotReady': mealAvailData.mealNotReady,
                        'mealNotReadyMsg': mealAvailData.mealNotReadyMsg,
                        'id': p.accordionTabIndex,
                        'viewOnly': mealsSelectionAvail.viewOnly,
                        'mealsSelectionAvailable': mealsSelectionAvail.mealsLabel,
                        'mealType': mealService[k].mealServiceName.toLowerCase(),
                        'flightSegment': i,
                        'flightSector': j,
                        'curPax': p.curPaxIndex,
                        'service': k,
                        'targetTrigger': p.accordionTabIndex,
                        'segmentId': i,
                        'sectorId': j,
                        'paxId': p.curPaxIndex,
                        'mealCategoryCode': mealCategoryCode,
                        'mealServiceCode': mealServiceCode,
                        'checked': checked,
                        'notEatingMsg': notEatingMsg,
                        'mealServiceCode': mealService[k].mealServiceCode,
                        'btcImg': btcImg,
                        'inflImg': inflImg,
                        'spmlImg': spmlImg,
                        'btcLabel': btcLabel,
                        'inflLabel': inflLabel,
                        'spmlLabel': spmlLabel,
                        'btcDesc': btcDesc,
                        'inflDesc': inflDesc,
                        'spmlDesc': spmlDesc
                    }));

                    if (p.serviceType == 'mealServicesInfant') {
                        var infantMealCat = getMealCategory('mealCategoryInfant', mealService[k].allMeals);
                        var infm = new SIA.InfantMeal(infantMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
                        infm.init();
                        p.infm.push(infm);
                    } else {
                        // Init Inflight Menu
                        var inflightMealCat = getMealCategory('mealCategoryInflight', mealService[k].allMeals);
                        var ifm = new SIA.InflightMenu(inflightMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
                        ifm.init();
                        p.ifm.push(ifm);

                        // Init Special meals
                        var specialMealCat = getMealCategory('mealCategorySPML', mealService[k].allMeals);
                        var spm = new SIA.SpecialMeal(specialMealCat, p.data, accContent, i, j, k, p.curPaxIndex, mealServiceCode);
                        spm.init();

                        p.spm.push(spm);

                        // pain book the cook menu content
                        var btcm = new SIA.BookTheCook();
                        btcm.init(
                            getMealCategory('mealCategoryBTC', mealService[k].allMeals),
                            p.inflightDishIcons,
                            i, j, p.curPaxIndex,
                            mealService[k].mealServiceCode,
                            p.data,
                            accContent,
                            'pre-slide-in-left hidden');

                        p.btc.push(btcm);
                    }

                    // if meal is not available for order, disabled clickable event.
                    $('.meal-not-ready a').removeAttr('href');

                    // add the trigger and content in the wrapper
                    accItemWrap.append(accTrigger);
                    accItemWrap.append(accContent);

                    // Add accordion item wrapper to accordion group wrapper
                    accordionContentWrap.append(accItemWrap);

                    p.accordionTabIndex++;

                    defaultSelection(accItemWrap);
                }
            }

            // Add accordion-wrapper block
            flSegment.append(accordionWrap);

            // Add flight segment to accordion wrapper
            p.accordionWrapper.append(flSegment);

            // add accordion wrapper to mealSelection block
            $('#mealSelection .block-inner').prepend(p.accordionWrapper);
        }

        // Add checkbox listeners
        addCheckboxEvents();

        // add events to all meal menu types
        addMealsItemEvents();

        // Initialise accordion plugin
        waitForAccordion();
    };
    var defaultSelection = function(wrapper) {
        if (p.isEco || p.isPey) {
            var itemContainer = wrapper.find(".item-container");
            if (!itemContainer.length) return;
            var paxId = itemContainer.data("curpax");
            var segmentId = itemContainer.data("segment");
            var mealServiceCode = itemContainer.data("meal-service-code");
            if (!p.data.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode].isEating) {
                return;
            }
        }
      var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
      var inflightBadge = wrapper.find(".inflight-badge");
      if (!wrapper.find(".fm-inf-menu.selected").length) {
          inflightBadge.removeClass("hidden");
          inflightBadge.addClass("badge-active");
          wrapperInflightMenu.addClass("selected-border");
          wrapperInflightMenu.find(".select-label").addClass("hidden");
      }
    };

    var addCheckboxEvents = function () {
        // get all checkbox for not eating then add change listener
        var checkBox = $('[data-checktype="not-eating"]');
        checkBox.each(function () {
            var cb = $(this);
            var el = cb.closest("[data-accordion-content]").find(".inflight-menu");
            cb.off().on({
                'change': function () {
                    var t = $(this);
                    var trigger = t.parent().parent().parent().parent().prev().find('.meal-sub-title-black');
                    if (t.prop('checked')) {
                        t.next().next().removeClass('hidden');

                        var msg = p.serviceType === 'mealServicesInfant' ? 'I don’t need an infant meal' : 'I\'m not having ' + t.attr('data-meal-type');
                        trigger.text(msg);

                        // add checkbox style for IE 8
                        $(this).next().addClass('checkbox_color');

                        resetMeals(t, ['ifm', 'infm', 'spm', 'alcm', 'btcm']);

                        if (p.isPey || p.isEco) {
                            unChooseInflightMenu(el);
                        }
                    } else {
                        t.next().next().addClass('hidden');
                        trigger.html('<span class="title-selected">'+ trigger.attr('data-label') + '</span>');


                        if (p.isPey || p.isEco) {
                            chooseInflightMenu(el, true);
                        }
                        // remove checkbox style for IE 8
                        $(this).next().removeClass('checkbox_color');
                    }

                    updateData(t);
                }
            }).ready(function() {
                var t = cb;
                if (p.isPey || p.isEco) {
                    t.closest("[data-accordion-content]").off()
                        .on("click.checkBoxSelect", function(e) {
                            var targetEl = $(e.target);
                            var clickables = [
                                targetEl.hasClass("inflight-inner-container"),
                                targetEl.hasClass("select-label"),
                                targetEl.hasClass("ml-inflight-container")
                            ];
                            if (clickables.indexOf(true) != -1) {
                                if (t.is(":checked")) {
                                    t.trigger("click");
                                }
                            }
                        })
                        .on("specialMealSelect", function(e) {
                            if (t.is(":checked")) {
                                t.trigger("click");
                            }
                        });
                }
            });
        });
    };

    var animateMealMenu = function (el, className, currAllMeals) {
        var curSubMenu = el.find(className);
        curSubMenu.removeClass('hidden');
        currAllMeals.addClass('sub-menu-on');

        setTimeout(function(){
            curSubMenu.removeClass('pre-slide-in-left');
        }, 20);
    };

    var addMealsItemEvents = function() {
        var mealMenuList = $('.meals-menu-item');
        for (var i = 0; i < mealMenuList.length; i++) {
            var mealMenuItemEl =  $(mealMenuList[i]);

            // check if meal is available
            if (! mealMenuItemEl.parent().hasClass('meal-not-ready')) {
                mealMenuItemEl.on('click', function(e) {
                    e.preventDefault();

                    // get the current accordion tab wrapper
                    var currAccordionTabWrapper = $(this).parent().parent().parent().parent().parent();

                     // get the current accordion tab index
                    var accordionIndex = currAccordionTabWrapper.attr('data-accordion-index');

                    // get current accordion content wrapper
                    var currAccordionContentWrapper = $(this).parent().parent().parent().parent();

                    // get the current accordion content block
                    var currAllMeals = $(this).parent().parent().parent();

                    // hide the current all meals content
                    currAllMeals.find('.meals-menu-item').attr('tabindex', '-1');
                    currAllMeals.parent().find('[data-checktype="not-eating"]').attr('tabindex', '-1');


                    // check what is the selected meal menu type
                    if ($(this).hasClass('book-the-cook-menu')) {
                        animateMealMenu(currAccordionContentWrapper, '.book-the-cook', currAllMeals);
                    } else if ($(this).hasClass('inflight-menu')) {
                        if(!p.isEco && !p.isPey) {
                            animateMealMenu(currAccordionContentWrapper, '.inflight-menu-wrap', currAllMeals);
                        }else {
                            chooseInflightMenu($(this));
                        }
                    } else {
                        animateMealMenu(currAccordionContentWrapper, '.special-meal-wrap', currAllMeals);
                    }

                    setTimeout(function() {
                        currAccordionContentWrapper.find('[data-first-focus="true"]').focus();
                    }, 500);
                });
            }
        }
    };

    var chooseInflightMenu = function(el, disableFlightAlert) {
        var inflightBadge = el.find(".inflight-badge");
        if (!inflightBadge.hasClass("badge-active")) {
            inflightBadge.addClass("badge-active");
            inflightBadge.removeClass("hidden");
            p.inflightAlertPrompt.insertAfter(el);
            el.find(".ml-inflight-container").addClass("selected-border");
            el.find(".ml-inflight-container").find(".select-label").addClass("hidden");
            removeAllSpecialMenu(el);
            if (!disableFlightAlert && undoMeal) {
                p.inflightAlertPrompt.insertAfter(el);
            }
            removeInputData(el.closest(".accordion-box"));
            undoButtonListener();
        }
    };

    var unChooseInflightMenu = function(el) {
        var inflightBadge = el.find(".inflight-badge");
        inflightBadge.removeClass("badge-active");
        inflightBadge.addClass("hidden");
        el.find(".ml-inflight-container").removeClass("selected-border");
        el.find(".ml-inflight-container").find(".select-label").removeClass("hidden");
        el.parent().find(".inflight-confirmation-prompt").remove();
    };

    var removeInputData = function(parent) {
        var input = $("#mealSelection").find("[type='hidden']");
        var btnElem = $(parent.find(".fm-select-btn").get(0));
        var paxId = btnElem.data("paxid");
        var segment = btnElem.data("segment");
        var mealCode = btnElem.data("meal-servicecode");

        var pax = JSON.parse(input.val());
        pax.paxList[paxId].selectedMeals[segment].selectedMeal[mealCode] = {
            "isEating": true,
            "isALaCarte": false,
            "mealCategoryCode": null,
            "mealCode": null,
            "mealMenuName": null,
            "aLaCarteData": null
        }
        input.val(JSON.stringify(pax));
        p.data.paxList = pax.paxList;
    };

    var undoButtonListener = function() {
        $(".inflight-link-undo").on("click", function(evt) {
            var parent = $(this).closest("[data-accordion-content]");

            var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

            var mealText = selectedLabel + undoMeal.data("meal-menuname") + " (" + mealCode[undoMeal.data("meal-category")] + ")";
            selectMeal(undoMeal);
            updateData(undoMeal);
            var mealSubTitle = parent.siblings("[data-accordion-trigger]").find(".meal-sub-title-black");
            mealSubTitle.html(mealText);
            if(p.isEco || p.isPey) mealSubTitle.append($(p.removeButton));
            removeButtonListener();
            parent.find(".ml-inflight-container").removeClass("selected-border");
            parent.find(".ml-inflight-container").find(".select-label").removeClass("hidden");
            undoMeal = null;
            
            evt.preventDefault();
            evt.stopPropagation();
        });

        $(document).off().on('click.promptUndoCheck', function (e) {
            var t = $(e.target);
            if (t.hasClass('passenger-info__text')
                || t.hasClass('meal-tab-item')
                || t.hasClass('accordion__control')
                || t.hasClass('meal-sub-title-black')
                || t.hasClass('ico-point-d')
                || t.hasClass('fm-ico-text')
                || t.hasClass('meal-service-name')
                || t.hasClass('flight-ind')
                || t.hasClass('btn-back')
                || t.is('#not-eating-delectables-1')
            ) {
                $(".inflight-link-undo").parent().parent().remove();
                $(document).off('click.promptUndoCheck');
            }
        });
    };
    
    var removeAllSpecialMenu = function(el) {
        var parent = el.closest("[data-accordion-content]");
        parent.find(".fm-inf-menu.selected").each(function() {
            var self = $(this);
            var mealButton = self.find(".fm-select-btn");
            unSelectMeal(mealButton);
            undoMeal = mealButton;
        });
        parent.siblings("[data-accordion-trigger]").find(".meal-sub-title-black").html('<span class="title-selected">Inflight Menu</span> ');
    };

    var selectMeal = function (meal) {
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        // Add selected class to the input and to the container
        parentEl.addClass('selected');
        meal.addClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.removeClass('hidden');

        meal.attr('value', 'Remove');
        removeDefaultSelection(meal);
    };

    var unSelectMeal = function (meal) {
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // Add selected class to the input and to the container
        parentEl.removeClass('selected');
        meal.removeClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.addClass('hidden');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        meal.attr('value', 'Select');
    };

    var removeDefaultSelection = function(mealButton) {
        var parent = mealButton.closest("[data-accordion-content]");
        var badge = parent.find(".inflight-badge.badge-active");
        var alertPrompt = parent.find(".inflight-confirmation-prompt");
        badge.removeClass("badge-active");
        badge.addClass("hidden");
        alertPrompt.remove();
    };


    var resetAccordionLabels = function(obj){
        for (var i = 0; i < p.accordTabArr.length; i++) {
            p.accordTabArr[i].find('.fm-ico-text').text(p.accordionTabOpenTxt);
        }

        if(!obj.hasClass('active')) obj.find('.fm-ico-text').text(p.accordionTabCloseTxt);
    };

    var paintPaxNav = function () {
        // cache paxlist
        var paxList = p.json.paxList;

        // Get dynamic pax width
        p.paxWidth = p.win.width() < 1024 ? p.paxListWrapper.parent().width() * 0.5 : 320;

        // Loop through pax list and add to DOM
        for (var i = 0; i < paxList.length; i++) {
            var pax;
            if (paxList[i].paxType.toLowerCase() === 'infant') {
                pax = $('<li class="pax-nav"><a href="#" data-id="' + i + '"><span>' + (i + 1) + '. ' + paxList[i].paxName + ' (Infant)</span></a></li>');
            } else {
                pax = $('<li class="pax-nav"><a href="#" data-id="' + i + '"><span>' + (i + 1) + '. ' + paxList[i].paxName + '</span></a></li>');
            }

            // Add active class to current pax
            if (i == p.curPaxIndex) { pax.addClass('active'); }

            // add pax-start class if first pax is active pax
            if (p.curPaxIndex == 0) p.paxListWrapper.parent().addClass('pax-start');

            p.paxListWrapper.append(pax);
            p.paxArr.push(pax);

            if (p.win.width() < 1024 && paxList.length > 2) {
                pax.css('width', p.paxWidth);
            }
        }

        // Add conditionals for different pax counts
        if (paxList.length === 1) {
            p.paxListWrapper.parent().addClass('hidden');
            $('#main-inner').find('.main-heading').text('Meals selection for ' + paxList[0].paxName);

        } else if (paxList.length === 2) {
            p.paxListWrapper.parent().addClass('double-pax');
            // addPaxNavListeners();
            initPaxNav();
        } else if (paxList.length === 3 && p.win.width() >= 1024) {
            p.paxListWrapper.parent().addClass('three-pax');
            setTimeout(function () { initPaxNav(); }, 700);
        } else {
            // initialise pax nav event listeners
            initPaxNav();
        }
    };

    var initPaxNav = function() {
        // Get reference to the buttons
        p.paxLt = p.paxListWrapper.prev();
        p.paxRt = p.paxListWrapper.next();

        // reset pax nav tab index
        for (var i = 0; i < p.paxArr.length; i++) {

            // check if pax is not visible, then hide from tab navigation


            if(p.paxArr[i].offset().left > p.paxListWrapper.parent().width() || p.paxArr[i].offset().left < 0) {
                p.paxArr[i].find('a').attr('tabindex', '-1');
            }else {
                p.paxArr[i].find('a').removeAttr('tabindex');
            }
        }

        // Add listeners to the next and prev buttons
        p.paxLt.off().on({
            'click': function(){
                movePax(false);
            }
        });

        p.paxRt.off().on({
            'click': function(){
                movePax(true);
            }
        });

        // add listeners on each pax element
        for (var i = 0; i < p.paxArr.length; i++) {
            p.paxArr[i].find('a').on({
                'click': function(e){
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    if(!$(this).parent().hasClass('active')) {
                        p.curPaxIndex = parseInt($(this).attr('data-id'));

                        // if active pax menu is clicked again, then dont allow to re-paint.
                        var lastClicked = $(this).parent().parent().data('lastClicked');

                        if (lastClicked == p.curPaxIndex) {
                            return;
                        }

                        $(this).parent().parent().data('lastClicked', p.curPaxIndex);

                        // Reset other pax nav and enable the clicked pax
                        resetAllPax($(this));

                        // scroll the page up to focus on the new menu
                        // after scroll show the next accordion wrapper
                        var top = p.paxListWrapper.parent().offset().top;
                        var bodyTop = p.htmlBody.prop('scrollTop');

                        // index the current accordion wrapper
                        p.exitWrapper = null;
                        p.exitWrapper = p.accordionWrapper;

                        // After adding the exit wrapper reference, paint the next wrapper for the next pax
                        paintMealsMenu('pre-slide-in-right');

                        if(bodyTop >= top) {
                            showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
                        }else {
                            p.htmlBody.stop().animate({
                                scrollTop: top
                            }, 400);

                            setTimeout(function(){
                                showNextPaxAccordion(p.exitWrapper, p.accordionWrapper);
                            },450);
                        }
                    }
                }
            });
        }
    };

    var resetAllPax = function(obj){
        for (var i = 0; i < p.paxArr.length; i++) {
            p.paxArr[i].removeClass('active');
        }

        obj.parent().addClass('active');
    };

    var movePax = function(dir){
        // Save reference for pax list container and wrapper
        var c = p.paxListWrapper.parent().width();
        var w = p.paxWidth * p.json.paxList.length;

        // Check which direction was clicked
        if(dir) {
            var amt = (p.paxNavCount+1) * p.paxWidth;

            // Check if its the last pax
            if((w-amt) > c) {
                p.paxListWrapper.css({
                    'left': (-amt) + 'px',
                    'width': w+64
                });
                p.paxListWrapper.parent().removeClass('pax-end');
            }else {
                p.paxListWrapper.css({
                    // Add 1 px per pax minus the default 3
                    'left': (c-w-32-(p.json.paxList.length-3)) + 'px',
                    'width': w+64
                });
                p.paxListWrapper.parent().addClass('pax-end');
            }

            p.paxNavCount++;
            p.paxListWrapper.parent().removeClass('pax-start');
        } else {
            var amt = (p.paxNavCount-1) * p.paxWidth;

            // Check if its the first pax
            if(amt > 0) {
                p.paxListWrapper.css({
                    'left': (-amt) +'px',
                    'width': w+32
                });
                p.paxListWrapper.parent().removeClass('pax-start');
            }else {
                p.paxListWrapper.css({
                    'left': '0',
                    'width': w+32
                });
                p.paxListWrapper.parent().addClass('pax-start');
            }

            p.paxNavCount--;
            p.paxListWrapper.parent().removeClass('pax-end');
        }
    };

    /* slide out the accordion wrapper
     */
    var showNextPaxAccordion = function(curObj, nextObj) {

        // remove current object exitwrapper
        curObj.remove();

        setTimeout(function(){
            // Add class to exit wrapper to start animating out
            curObj.addClass('slide-out-left');
            // Add class to current accordion wrapper to start animating in
            nextObj.removeClass('pre-slide-in-right');
        }, 10);
    };

    /* To set the accordion wrapper default position
     */
    var waitForAccordion = function() {
       setTimeout(function() {
           if (typeof SIA.accordion.initAccordion == 'undefined') {
               waitForAccordion();
           } else {
                SIA.accordion.initAccordion();

                // check if second segment meal selection is not available
                // then remove any events for accordion tab
                disableSecSegAvailable();
           }
       }, 60);
    };

    /* check if second segment available for meal selection
     */
    var secSegmentAvailable = function (flSegment, index) {
        var mealSelWaitListedData = p.json.flightSegment[index];
        var secSegMealSelectionStatus = mealSelWaitListedData.isSegmentAvailableForMealSelection;
        var secSegMealSelectionMsg = mealSelWaitListedData.segmentMealSelectionStatusMessage;

        if (secSegMealSelectionStatus == 'false') {
            var checkinAlert = $(_.template(p.checkinAlert, { checknMsg: secSegMealSelectionMsg }));

            // add checkin alert to second segment block
            flSegment.append(checkinAlert);

            // change accordion arrow text && icon to gray to disable
            flSegment.addClass('disable-accordion');
        }
    };

    /* disable events for all second segment meals selection
     */
    var disableSecSegAvailable = function() {
       $('.disable-accordion [data-accordion-trigger="1"]').unbind();

       // remove href attrib to disable clickable
       $('.disable-accordion a').removeAttr('href');
    };

    /* Check all meals availability attribute only shows up in inflight meal
     */
    var mealsAvailable = function(mealService, index) {
        var mealNotReady = '';
        var mealNotReadyMsg = '';

        if (((p.serviceType == 'mealServicesAdult') ||
            (p.serviceType == 'mealServicesChild'))) {
            var mealServiceData = mealService[index].allMeals[1].mealCategoryInflight[0];
            var isViewMenuEligible = mealServiceData.isViewMenuEligible;
            var isMealSelAvail = mealServiceData.isMealSelectionAvailable;

            mealNotReady = ((isViewMenuEligible == 'false') && (isMealSelAvail == 'false')) ? 'meal-not-ready' : '';
            mealNotReadyMsg = mealServiceData.isViewMenuDate;
        }

        return {
            mealNotReady: mealNotReady,
            mealNotReadyMsg: mealNotReadyMsg
        };
    };

    /* check if meal is not available for order
     */
    var checkMealsSelAvailable = function(mealService, index) {
        var mealsAvailData = mealService[index].allMeals[1].mealCategoryInflight[0];
        var isViewMenuEligible = mealsAvailData.isViewMenuEligible;
        var isMealSelAvail = mealsAvailData.isMealSelectionAvailable;

        if ((isViewMenuEligible == 'true') && (isMealSelAvail == 'false')) {
            return {
                'mealsLabel': p.mealsLabelTpl,
                'viewOnly': 'view-only'
            };
        } else {
            return {
                'mealsLabel': '',
                'viewOnly': ''
            };
        }
    };

    /* get current pax type if adult, child or infant
     */
    var getServiceType = function(paxIndex) {
        p.curPaxObj = p.json.paxList[paxIndex];
        var serviceType = '';
        switch (p.curPaxObj.paxType.toLowerCase()) {
            case 'adult':
                serviceType = 'mealServicesAdult';
                break;
            case 'child':
                serviceType = 'mealServicesChild';
                break;
            case 'infant':
                serviceType = 'mealServicesInfant';
                break;
            default:
                serviceType = 'mealServicesAdult';
        }

        return serviceType;
    };

    var getMealCategory = function(type, arr) {
        for (var i = 0; i < arr.length; i++) {
            if(typeof arr[i][type] != 'undefined') {
                return arr[i][type];
            }
        }
    };

    var resetMeals = function(el, meals) {
      var mealList = {
          'btcm': '.book-the-cook .selected .fm-select-btn',
          'ifm': '.inflight-menu-wrap .selected .fm-select-btn',
          'infm': '.selected .fm-select-btn',
          'spm': '.special-meal-wrap .selected .fm-select-btn',
          'alcm': '.a-la-carte [style="display: block;"] .selected .fm-select-btn'
      };

      $.each(meals, function (i, meal) {
          var inputBtn = el.closest('.all-meals-list').find(mealList[meal]);
          inputBtn.closest('.fm-inf-menu').removeClass('selected');
          inputBtn.closest('.fm-inf-menu').removeClass('int-prompt');
          inputBtn.attr('value', 'Select');

          inputBtn.parent().parent().find('.confirmation-prompt').addClass('hidden');

          // for a-la-carte only to clear its accordion tab.
          if (meal == 'alcm') { inputBtn.closest('.fm-inf-menu').parent().prev().find('.alc-summary').html(''); }
      });
    };

    var resetNotEating = function(el) {
        var notEatingEl = el.closest('.all-meals-list').find('.not-eating-supper');
        notEatingEl.find('input').prop('checked', false);
        notEatingEl.find('.not-eating-msg').addClass('hidden');
        notEatingEl.find('.fm-cbox').removeClass('checkbox_color');
    };

    var checkCabinClass = function(curCC, classArr){
      var isMatch = false;
      for (var i = 0; i < classArr.length; i++) {
          var cc = classArr[i];
          if(curCC === cc) {
              isMatch = true;
              break;
          }
      }

      return isMatch;
    };

    var resizeEnd = function(){
        // Stretch the wrapper to full width
        if(p.json.paxList.length > 2) {
            paintPaxNav();
        }

        p.curWinWidth = p.win.width();
    };

    var oPublic = {
        init: init,
        getServiceType: getServiceType,
        updateData: updateData,
        resetMeals: resetMeals,
        resetNotEating: resetNotEating,
        removeInputData: removeInputData,
        public: p
    };
    return oPublic;
}();

SIA.InflightMenu = function(jsonData,
    paxData,
    accContent,
    segmentId,
    sectorId,
    serviceId,
    paxId,
    mealServiceCode) {
    var p = {};

    // Declare templates for inflight menu
    p.h2TxtTpl = '<div class="alert-block info-box<%= viewOnlyAlert %>">\
          <div class="inner">\
            <div class="alert__icon"><em class="ico-tooltips"></em></div>\
            <div class="alert__message"><%= viewMenuData %></div>\
          </div>\
        </div>\
        <h2 class="fm-title-blue"><%= h2Txt %></h2>';
    p.descTxtTpl = '<p class="fm-inf-menu-desc">Choose a delicious main course from our inflight menu to complete your inflight dining experience.</p>\
        <h3 class="fm-title2-black">Your choice of main course</h3>';
    p.inflightMealMenuWrapperTpl = '<div class="fm-inf-menu <%= selectedClass %><%= viewOnlyWrapper %>">\
        <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
        <h4 class="fm-inf-menu-title"><%= title %></h4>\
        <p class="fm-inf-menu-desc"><%= desc %></p>\
        <div class="fm-footnote-txt">\
          <span class="food-icon-set"><%= foodIconSet %></span>\
          <input type="button" name="select" value="<%= inputVal %>" data-inflightMenuselect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-isALC="<%= isALC %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
          <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
            <span class="btn-small">VIEW ONLY</span>\
            <span class="onboard"> Order on board</span>\
          </div>\
        </div>\
      </div>';
    p.inflightEthinicMealMenuWrapperTpl = '<div class="fm-inf-menu <%= selectedClass %><%= viewOnlyWrapper %>">\
            <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
            <span class="fm-ysh-choice <%= badgeState %>"><%= badgeName %></span>\
            <h4 class="fm-inf-menu-title"><%= menuTitle %></h4>\
            <p class="fm-inf-menu-desc"><%= menuDesc %></p>\
            <%= promotionsConditionTpl %>\
            <div class="fm-footnote-txt">\
                <span class="food-icon-set"><%= foodIconSet %></span>\
                <input type="button" name="" value="<%= inputVal %>" data-inflightMenuselect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-isALC="<%= isALC %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
                <div class="btn-wrap-view-only<%= viewOnlyClass %>">\
                  <span class="btn-small">VIEW ONLY</span>\
                  <span class="onboard"> Order on board</span>\
                </div>\
            </div>\
        </div>';
    p.inflightEthinicMealMenuTpl1 = '<div class="promotion-conditions" />';
    p.inflightEthinicMealMenuTpl2 = '<div class="promotion-conditions-text" />';
    p.inflightEthinicMealMenuTplDataTpl = '<dl><dt><%= foodName %></dt></dl>';
    p.inflightEthinicMealMenuFoodDescTpl = '<dd><span class="title-5--blue"><%= foodDesc %></span></dd>';

    // A la carte / noodle bar description
    p.noodleBsaaarDescTxtTpl = '<p class="fm-inf-menu-desc">\
       You can choose to pre-order one option from each meal service now,\
       but there isn&#8217t a fixed time where they&#8217ll be served the meal(s).\
       Pre-ordering will help you guarantee that you&#8217ll get some things that you would like to eat.\
       You can also choose to order more from the various meal services when you are on the flight.</p>\
    <h3 class="fm-title2-black">Mid-flight noodle bar</h3>';
    p.alcarteDescTxtTpl = '<p class="fm-inf-menu-desc">\
     You can choose to pre-order one option from each meal service now,\
     but there isn&#8217t a fixed time where they&#8217ll be served the meal(s).\
     Pre-ordering will help you guarantee that you&#8217ll get some things that you would like to eat.\
     You can also choose to order more from the various meal services when you are on the flight.</p>';

    // A la carte
    p.alcAccMainWrap = '<div data-accordion-wrapper="1" class="accordion-component m-accordion-wrapper">\
      <div data-accordion-wrapper-content="1" class="accordion-wrapper-content">\
      </div></div>';
    p.alcAccWrapper = '<div data-accordion="1" data-accordion-index="<%= id %>" data-mealsubcat-code="<%= mealSubCategoryCode %>" data-mealsubcat-name="<%= mealServiceName %>" class="block-2 accordion accordion-box alc-accordion">\
            <a href="#" data-accordion-trigger="1" class="accordion__control<%= isActive %>" aria-expanded="true" tabindex="0">\
                <div class="fm-accordion-label">\
                    <ul class="meal-menu">\
                        <li class="meal-service-name"><%= mealServiceName %></li>\
                    </ul>\
                    <span class="fm-ico-text selected alc-summary"></span>\
                    <em class="ico-point-d"></em>\
                </div>\
            </a>\
            <div data-accordion-content="1" class="accordion__content alc-acc-content"></div>\
        </div>';

    // applied special meal prompt
    p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
        <em class="ico-alert"></em>\
        <p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
      </div>');

    p.selectedALC = [];
    p.isALC = false;

    var init = function() {
        p.mealCatName = jsonData[0].mealMenuCategoryName;

        p.mealWrap = accContent.find('.inflight-menu-wrap .menu-container');

        backToMainMenu();
        paintHeading();
        paintMealChoices();
    };

    var paintHeading = function(){
        var viewOnlyAlert = ' hidden';
        var viewOnlyMsg = jsonData[0].isViewMenuDate;
        var inflMenuViewOnly = true;

        if(jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
            viewOnlyAlert = '';
            viewOnlyMsg = jsonData[0].isViewMenuDate;
            inflMenuViewOnly = true;
        }

        p.mealMenuCatName = $(_.template(p.h2TxtTpl, {
            'h2Txt':  p.mealCatName,
            'viewOnlyAlert': viewOnlyAlert,
            'viewMenuData': viewOnlyMsg
        }));

        p.mealWrap.append(p.mealMenuCatName);

        // add the meal menu category name description
        if(mealServiceCode == 'ACL_110') {
          p.mealWrap.append(p.alcarteDescTxtTpl);
        } else if (mealServiceCode == 'MNB_110') {
          p.mealWrap.append(p.noodleBsaaarDescTxtTpl);
        } else {
          p.mealWrap.append(p.descTxtTpl);
        }
    };

    var paintMealChoices = function(){
        var viewOnlyAlert = ' hidden';
        var viewOnlyMsg = jsonData[0].isViewMenuDate;
        var inflMenuViewOnly = false;

        if(jsonData[0].isMealSelectionAvailable == "false" && jsonData[0].isViewMenuEligible == "true") {
            viewOnlyAlert = '';
            viewOnlyMsg = jsonData[0].isViewMenuDate;
            inflMenuViewOnly = true;
        }

        p.mealCardList = [];
        var mealCatCode = jsonData[0].mealCategoryCode;

        p.curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

        if(jsonData[0].isALaCarte == 'true') {
          p.isALC = true;
          p.alcInput = $('<input type="button" class="hidden selected" name="A La Carte Selection" value="A La Carte Selection" data-isEating="true" data-inflightMenuselect-id="" data-segment="" data-sector="" data-service-id="" data-isalc="true" data-paxid="" data-meal-category="" data-meal-servicecode="" data-meal-menuname="" data-meal-mealcode="" />');

          p.mealWrap.append(p.alcInput);

          // Setup a la carte data
          p.alcData = {};

          var alcAccMainWrap = $(p.alcAccMainWrap);
          for (var j = 0; j < jsonData[0].mealSubcategory.length; j++) {
                var curData = jsonData[0].mealSubcategory[j];

                var alcAccTitle = '';
                // Check if any ALC meals are selected
                if (p.curPaxData.isALaCarte === 'true') {
                    p.alcData[curData.mealSubCategoryCode] = p.curPaxData.aLaCarteData[curData.mealSubCategoryCode];
                    if (p.alcData[curData.mealSubCategoryCode].mealMenuName) alcAccTitle = 'Selected: ' + p.alcData[curData.mealSubCategoryCode].mealMenuName + ' (Inflight Menu)';
                } else {
                    p.alcData[curData.mealSubCategoryCode] = {};
                    p.alcData[curData.mealSubCategoryCode].mealCategoryCode = null;
                    p.alcData[curData.mealSubCategoryCode].mealCode = null;
                    p.alcData[curData.mealSubCategoryCode].mealMenuName = null;
                    p.alcData[curData.mealSubCategoryCode].mealServiceCode = null;
                    p.alcData[curData.mealSubCategoryCode].mealServiceName = null;
                }

                var isActive = '';
                if (j == 0) isActive = ' active';

                var alcAccordionWrap = $(_.template(p.alcAccWrapper, {
                    'mealServiceName': jsonData[0].mealSubcategory[j].mealSubCategoryName,
                    'mealSubCategoryCode': jsonData[0].mealSubcategory[j].mealSubCategoryCode,
                    'id': j,
                    'isActive': isActive
                }));

                alcAccMainWrap.find('.accordion-wrapper-content').append(alcAccordionWrap);
                p.mealWrap.addClass('a-la-carte');

                // Add individual alc title
                alcAccordionWrap.find('.alc-summary').html(alcAccTitle);

                var mealCardData = jsonData[0].mealSubcategory[j].Meals;
                addInflightMeals(mealCardData, mealCatCode, p.curPaxData, alcAccordionWrap.find('.alc-acc-content'), inflMenuViewOnly, jsonData[0].mealSubcategory[j].mealSubCategoryCode);
            }

          p.mealWrap.append(alcAccMainWrap);
        }
        else {
            var mealCardData = jsonData[0].mealSubcategory["0"].Meals;
            addInflightMeals(mealCardData, mealCatCode, p.curPaxData, p.mealWrap, inflMenuViewOnly);
        }
    };

    var addInflightMeals = function(mealCardData, mealCatCode, curPaxData, mealWrapper, inflMenuViewOnly, alcCat){
        for (var i = 0; i < mealCardData.length; i++) {
            // indentify which food icon to use
            var foodIcon = '';
            if(typeof mealCardData[i].specialityInfo !== 'undefined') {
                foodIcon = SIA.MealsSelection.public.inflightDishIcons[mealCardData[i].specialityInfo.dishIconId];
            }

            // check for any previous selected item
            if(curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == mealCardData[i].mealCode){
                selectedClass =  'selected';
                inputVal = 'Remove';
                isSelected = '';
                p.lastTick = p.mealCardList.length;
            }else {
                selectedClass =  '';
                inputVal = 'Select';
                isSelected = 'hidden';
            }

            var mealCard;

            // if its ethnic meal
            if (mealCardData[i].isEthnicMeal === 'true' && typeof mealCardData[i].ethinicMeallist != 'undefined') {
                mealCard = paintEthnicMeal(i, mealCardData, mealCatCode, inflMenuViewOnly);
            } else { // generate the information for meal card
                mealCard = paintMeal(i, mealCardData, mealCatCode, inflMenuViewOnly, alcCat);
            }

            // check if menu is view ONLY, if true, hide input
            if(inflMenuViewOnly) {
                mealCard.find('input').hide();
                mealCard.find('.btn-wrap-view-only').hide();
                mealCard.addClass('view-only-meal');
            }else {
                // add meal menu select button event
                mealCard.find('input').each(function(){
                    addInputListener($(this));
                });
            }

            // add the meal card
            p.mealCardList.push(mealCard);
            mealWrapper.append(mealCard);
        }
    };

    var paintEthnicMeal = function(i, mealCardData, mealCatCode, inflMenuViewOnly){
        var mealCard;
        // create two wrapper to have two columns layout in ethinic meal menu list of names
        var ethinicMealListWrapper1 = $(p.inflightEthinicMealMenuTpl2);
        var ethinicMealListWrapper2 = $(p.inflightEthinicMealMenuTpl2);
        var ethinicMealMenuData = mealCardData[i].ethinicMeallist;

        for (var k = 0; k < ethinicMealMenuData.length; k++) {
            // prepare the ethinic meal menu name item wrapper
            var ethinicMealMenuDataWrapper = $(_.template(p.inflightEthinicMealMenuTplDataTpl, {
                'foodName': ethinicMealMenuData[k].ethinicMealMenuName
            }));

            // prepare the ethinic meal menu description block
            var ethinicMealMenuDescData = ethinicMealMenuData[k].itemInfo;
            var ethinicMealMenuDesc = null;

            for (var j = 0; j < ethinicMealMenuDescData.length; j++) {
                // generate ethinic meal menu description data
                ethinicMealMenuDesc = $(_.template(p.inflightEthinicMealMenuFoodDescTpl, {
                    'foodDesc': ethinicMealMenuDescData[j].ethinicMealMenuDescription
                }));

                // add to ethinic meal menu name item wrapper
                ethinicMealMenuDataWrapper.append(ethinicMealMenuDesc);
            }

            // Add additional description
            var ethnicMealDesc = ethinicMealMenuData[k].ethnicMealMenuDescription;
            if(typeof ethnicMealDesc !== 'undefined' && ethnicMealDesc !== '') {
              ethinicMealMenuDataWrapper.append('<dd class="ethnic-meal-desc">' + ethinicMealMenuData[k].ethnicMealMenuDescription + '</dd>')
            }

            // check in which column to add then,
            // add to ethinic meal menu list column
            if(k % 2 == 0) {
                ethinicMealListWrapper1.append(ethinicMealMenuDataWrapper);
            } else {
                ethinicMealListWrapper2.append(ethinicMealMenuDataWrapper);
            }
        }

        // add to ethinic meal menu main wrapper
        var ethinicMealMenuMainWrapper = $(p.inflightEthinicMealMenuTpl1);
        ethinicMealMenuMainWrapper.append(ethinicMealListWrapper1);
        ethinicMealMenuMainWrapper.append(ethinicMealListWrapper2);

        var foodIconSet = '';
        if(typeof mealCardData[i].specialityInfo !== 'undefined') {
          for (var l = 0; l < mealCardData[i].specialityInfo.length; l++) {
            var br = l === mealCardData[i].specialityInfo.length -1 ? '' : '<br>';
            foodIconSet += '<em class="' +SIA.MealsSelection.public.inflightDishIcons[mealCardData[i].specialityInfo[l].dishIconId]+ ' fm-footnote-logo"></em>\
            <span class="fm-dish-text"> ' + mealCardData[i].specialityInfo[l].specialityFootNote + ' </span>' + br;
          }
        }

        // check if to add a badge style
        var badgeName =  null;
        var badgeState = 'hidden';
        if (mealCardData[i].isSpecialMeal === 'true') {
            badgeName = mealCardData[i].specialMealName
            badgeState = '';
        }

        var selectClass = '';
        var viewOnlyClass = ' hidden';
        var viewOnlyWrapper = '';

        if(typeof mealCardData[i].isViewOnly != 'undefined' && mealCardData[i].isViewOnly) {
            selectClass = ' hidden';
            viewOnlyClass = '';
            viewOnlyWrapper = ' view-only-meal';
        }

        // generate ethinic meal menu wrapper data
        mealCard = $(_.template(p.inflightEthinicMealMenuWrapperTpl, {
            'badgeName': badgeName,
            'badgeState': badgeState,
            'menuTitle':  mealCardData[i].mealMenuName,
            'menuDesc': mealCardData[i].mealMenuDescription,
            'menuSpecialityFootNote': mealCardData[i].specialityInfo.specialityFootNote,
            'promotionsConditionTpl': ethinicMealMenuMainWrapper["0"].outerHTML,
            'id': p.mealCardList.length,
            'selectClass': selectClass,
            'segmentId': segmentId,
            'sectorId': sectorId,
            'paxId': paxId,
            'mealServiceId': serviceId,
            'mealCategoryCode': mealCatCode,
            'mealServiceCode': mealServiceCode,
            'mealCode': mealCardData[i].mealCode,
            'mealMenuName': mealCardData[i].mealMenuName,
            'viewOnlyClass': viewOnlyClass,
            'selectedClass': selectedClass,
            'inputVal': inputVal,
            'isSelected': isSelected,
            'isALC': p.isALC,
            'viewOnlyWrapper': viewOnlyWrapper,
            'foodIconSet': foodIconSet

        }));

        // check if menu is view ONLY, if true, hide input
        if(inflMenuViewOnly) {
            mealCard.find('input').hide();
            mealCard.find('.btn-wrap-view-only').hide();
            mealCard.addClass('view-only-meal');
        } else {
            // add meal menu select button event
            mealCard.find('input').each(function(){
                addInputListener($(this));
            });
        }

        return mealCard;
    };

    var paintMeal = function(i, mealCardData, mealCatCode, inflMenuViewOnly, alcCat){
        var mealCard;

        var foodDesc = '';
        if(typeof mealCardData[i].specialityInfo != 'undefined') foodDesc = mealCardData[i].specialityInfo.specialityFootNote;

        var selectClass = '';
        var viewOnlyClass = ' hidden';
        var viewOnlyWrapper = '';

        if(typeof mealCardData[i].isViewOnly != 'undefined' && mealCardData[i].isViewOnly) {
            selectClass = ' hidden';
            viewOnlyClass = '';
            viewOnlyWrapper = ' view-only-meal';
        }

        if(p.isALC && typeof alcCat !== 'undefined' && p.curPaxData.aLaCarteData) {
            if(p.curPaxData.aLaCarteData[alcCat].mealCode === mealCardData[i].mealCode) {
                selectClass = ' selected';
                selectedClass =  'selected';
                viewOnlyClass = ' hidden';
                inputVal = 'Remove';
                isSelected = '';
                p.lastTick = i;
            }
        }

        var foodIconSet = '';
        if(typeof mealCardData[i].specialityInfo !== 'undefined') {
          for (var l = 0; l < mealCardData[i].specialityInfo.length; l++) {
            var br = l === mealCardData[i].specialityInfo.length -1 ? '' : '<br>';
            foodIconSet += '<em class="' +SIA.MealsSelection.public.inflightDishIcons[mealCardData[i].specialityInfo[l].dishIconId]+ ' fm-footnote-logo"></em>\
            <span class="fm-dish-text"> ' + mealCardData[i].specialityInfo[l].specialityFootNote + ' </span>' + br;
          }
        }

        mealCard = $(_.template(p.inflightMealMenuWrapperTpl, {
            'title': mealCardData[i].mealMenuName,
            'desc': mealCardData[i].mealMenuDescription,
            'foodDesc': foodDesc,
            'id': p.mealCardList.length,
            'triggerId': (p.accordionTabIndex + 1),
            'selectClass': selectClass,
            'segmentId': segmentId,
            'sectorId': sectorId,
            'paxId': paxId,
            'mealServiceId': serviceId,
            'mealCategoryCode': mealCatCode,
            'mealServiceCode': mealServiceCode,
            'mealCode': mealCardData[i].mealCode,
            'mealMenuName': mealCardData[i].mealMenuName,
            'viewOnlyClass': viewOnlyClass,
            'selectedClass': selectedClass,
            'inputVal': inputVal,
            'isSelected' : isSelected,
            'isALC': p.isALC,
            'viewOnlyWrapper': viewOnlyWrapper,
            'foodIconSet': foodIconSet
        }));

        if (i == 0) {
          mealCard.find('.fm-select-btn').attr('data-first-focus', 'true');
        }

        return mealCard;
    };

    var addInputListener = function(input) {
        input.parent().parent().off().on({
            'click': function(e){
                e.preventDefault();
                e.stopImmediatePropagation();


                var t = $(this).find('.fm-select-btn');
                var parentEl = t.parent().parent();
                var selectedId = t.attr('data-inflightMenuselect-id');

                if(t.hasClass('hidden')) return;

                var accordionContent = t.closest('.accordion__content');
                var accordionTabMealTitle, alcParentAccWrap, alcParentAccWrapTrigger;

                if(p.isALC) {
                    accordionTabMealTitle = accordionContent.prev().find('.alc-summary');
                    alcParentAccWrap = accordionContent.closest('.alc-accordion');
                    alcParentAccWrapTriggerSummary = alcParentAccWrap.closest('.all-meals-list').prev().find('li:nth-child(3)');
                }else {
                    accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
                }
                var parentElList = parentEl.parent();

                // search and remove any previous selected meal
                var mealCardList = accordionContent.find('.fm-select-btn');
                for (var j = 0; j < mealCardList.length; j++) {
                    var el = $(mealCardList[j]);

                    // add hidden class to hide the selected badge
                    el.closest('.fm-inf-menu').find('.fm-inf-menu-select-badge').addClass('hidden');

                    // remove selected class
                    el.closest('.fm-inf-menu').removeClass('selected');

                    // reset the select button value to it's default text('Select').
                    el.attr('value','Select');

                    // change accordion tab previously selected meal to its default text('inflight menu')
                    var inflLbl = 'Inflight Menu';
                    if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                    accordionTabMealTitle.html(p.isALC ? '' : inflLbl);
                }

                // check if current meal card is selected again, then remove it
                var badgeEl = $(parentEl).children();

                // Assign A La Carte initial data
                if(p.isALC){
                    var curMealSubCatCode = alcParentAccWrap.data('mealsubcat-code');
                    var curMealSubCatName = alcParentAccWrap.data('mealsubcat-name');
                }

                if (p.lastTick == selectedId) {
                    // Add selected class to the input and to the container
                    parentEl.removeClass('selected');
                    t.removeClass('selected');

                    // remove hidden class to show the selected badge
                    $(badgeEl["0"]).addClass('hidden');

                    t.attr('value','Select');

                    p.lastTick = '';

                    // change accordion tab previously selected mean to its default text('inflight menu')
                    var inflLbl = 'Inflight Menu';
                    if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                    accordionTabMealTitle.html(p.isALC ? '' : inflLbl);

                    if(p.isALC) {
                        p.alcData[curMealSubCatCode].mealCategoryCode = null;
                        p.alcData[curMealSubCatCode].mealCode = null;
                        p.alcData[curMealSubCatCode].mealMenuName = null;
                        p.alcData[curMealSubCatCode].mealServiceCode = null;
                        p.alcData[curMealSubCatCode].mealServiceName = null;
                    }

                    // Check if there are selected special meals then select same meal for this service code
                    for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
                      var spml = SIA.MealsSelection.public.spm[i];
                      if(spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
                        spml.selectSegmentMeal();
                        p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
                        parentEl.after(p.spmlAlertPrompt);

                        $(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck',function(e) {
                            var t = $(e.target);
                            if(t.hasClass('passenger-info__text')
                              || t.hasClass('meal-tab-item')
                              || t.hasClass('accordion__control')
                              || t.hasClass('ico-point-d')
                              || t.hasClass('fm-ico-text')
                              || t.hasClass('meal-service-name')
                              || t.hasClass('flight-ind')
                              || t.hasClass('btn-back')
                              || t.hasClass('fm-inf-menu')
                              || t.hasClass('search-btn')
                            ) {
                              p.spmlAlertPrompt.detach();

                              $(document).off('click.spmlPromptCheck');
                            }
                        });

                        break;
                      }
                    }

                } else {
                    // Add selected class to the input and to the container
                    parentEl.addClass('selected');
                    t.addClass('selected');

                    // remove hidden class to show the selected badge
                    $(badgeEl["0"]).removeClass('hidden');

                    t.attr('value','Remove');

                    p.lastTick = selectedId;

                    // get the selected mean name
                    var selMealName = p.isALC ? 'Selected: '+ parentEl.find('.fm-inf-menu-title').text(): parentEl.find('.fm-inf-menu-title').text();

                    // change accordion tab default text('inflight menu') to selected meal
                    accordionTabMealTitle.text(selMealName + ' (Inflight Menu)');

                    SIA.MealsSelection.resetNotEating(t);

                    if(p.isALC){
                        p.alcData[curMealSubCatCode].mealCategoryCode = t.data().mealCategory;
                        p.alcData[curMealSubCatCode].mealCode = t.data().mealMealcode;
                        p.alcData[curMealSubCatCode].mealMenuName = t.data().mealMenuname;
                        p.alcData[curMealSubCatCode].mealServiceCode = t.data().mealServicecode;
                        p.alcData[curMealSubCatCode].mealServiceName = curMealSubCatName;
                    }
                }

                if(p.isALC) {
                    var parentAccMealSummary = '';
                    var mealMenunames = '';
                    var mealMealcodes = '';

                    _.each(p.alcData, function(v, k, l){
                        if(v.mealMenuName != null) {
                            // set the title info
                            parentAccMealSummary = parentAccMealSummary + '<br>' + v.mealServiceName + ' - ' + v.mealMenuName + ' (Inflight Menu)';

                            mealMenunames = mealMenunames + ',' + v.mealMenuName;
                            mealMealcodes = mealMealcodes + ',' + v.mealCode;
                        }

                    });

                    p.alcInput.attr('data-inflightMenuselect-id', t.data().inflightmenuselectId);
                    p.alcInput.attr('data-paxid', t.data('paxid'));
                    p.alcInput.attr('data-sector', t.data().sector);
                    p.alcInput.attr('data-segment', t.data().segment);
                    p.alcInput.attr('data-service-id', t.data().serviceId);
                    p.alcInput.attr('data-meal-category', t.data().mealCategory);
                    p.alcInput.attr('data-meal-servicecode', t.data().mealServicecode);
                    p.alcInput.attr('data-alc-json', JSON.stringify(p.alcData));

                    alcParentAccWrapTriggerSummary.html(parentAccMealSummary.substring(4));

                    // Update the user data here
                    SIA.MealsSelection.updateData(p.alcInput);
                }else {
                    SIA.MealsSelection.updateData(t);
                }

            }
        });
    };

    var backToMainMenu = function(){
        // add back to all meals button event
        p.mealWrap.find('.fm-backtomeals-btn .btn-back').off().on('click', function(e) {
            e.preventDefault();
            var subMenuWrapper = $(this).parent().parent();

            subMenuWrapper.addClass('hidden');

            // reset position of main menu wrapper
            subMenuWrapper.parent().find('.all-meals-menu').removeClass('sub-menu-on');

            // hide inflight menu wrapper
            subMenuWrapper.addClass('pre-slide-in-left');

            // show all menu wrapper
            var prevAllMeal = $(this).parent().parent().parent();
            //prevAllMeal.children('div:nth-child(1)').removeClass('hidden');
            // prevAllMeal.find('.inflight-menu-wrap').removeClass('hidden');

            p.mealCardList = null;
        });
    };

    var oPublic = {
        init: init,
        p: p
    };

    return oPublic;
};

SIA.BookTheCook = function() {
  var p = {};

  // declare templates
  p.back2AllMealsLinkTpl = '<hr><div class="fm-backtomeals-btn"><a href="#" class="btn-back"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back</a></div>';
  p.back2AllMealsLinkTpl2 = '<div class="fm-backtomeals-btn floater">\
            <a href="#" class="btn-back" tabindex="0"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back</a>\
        </div>';
  p.BTCTitleTpl = '<h2 class="fm-title-blue"><%= txt %></h2>';
  p.BTCDescTpl = '<p class="fm-inf-menu-desc">Choose a Book the Cook main course at least 24 hours before departure to enjoy an exquisite meal crafted by our International Culinary Panel of chefs. </p>';
  p.BTCSearch = '<p class="search-heading"> Have a meal in mind? </p><div class="static-content"> <div class="static-details"> <div class="btc-search"> <label for="input-search-1" class="hidden"></label><em class="ico-search"></em><span class="input-1"><input type="text" name="input-search-1" value="" placeholder="Search all our available Book the Cook main courses"/></span> <button type="submit" name="btn-search" class="btn-1 search-btn">Search</button> </div><a href="#" class="clear-search hidden"><em class="ico-point-r"></em>Clear search result</a></div></div>';

  p.BTCCuisineCatMainWrapperTpl = '<div class="tab-wrapper"> <div class="tab-content active"> <p class="search-result-msg hidden"></p></div><div class="form-seatmap responsive btc-meals"> <%=tabList %> <div data-customselect="true" class="btc-meal-select custom-select custom-select--2 custom-select--3 multi-select tab-select" data-select-tab="true"> <label for="calcTabSelect" class="select__label">&nbsp;</label><span class="select__text">Economy</span><span class="ico-dropdown"></span> <select name="calcTabSelect"></select> </div><%=contentList %> </div></div>';
  p.selOptionItemTpl = '<option value="<%= tabMenu %>"><%= menuName %></option>';
  p.BTCCruisineCatListTpl = '<div data-fixed="true" class="sidebar"><div class="inner" role="application" style="left: auto;"><ul class="booking-nav btc-meal-nav" role="tablist"><%= catListItems %><li class="more-item"><a href="#"><span>More</span><em class="ico-dropdown"></em></a></li></ul></div></div>';
  p.BTCCruisineCatListItemTpl = '<li class="booking-nav__item tab-item <%= isActive %>" role="tab" data-tab-menu="<%= tabMenu %>"><a href="#" role="tab"><span class="passenger-info"><span class="passenger-info__text"><%= menuName %></span></span></a></li>';
  // p.BTCMealItem = '<div class="fm-inf-menu <%= isSel %>"><span class="fm-inf-menu-select-badge">Selected</span><span class="special-badge <%= isSpecial %>">SPECIAL</span><h4 class="fm-inf-menu-title"><%= mealMenuName %></h4><p class="fm-inf-menu-desc"><%= mealMenuDesc %></p><div class="fm-footnote-txt"><em class="<%= footIcon %> fm-footnote-logo <%= footLogo %>"></em><span class="fm-dish-text"><%= specialityFootNote %></span><input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>"></div></div>';
  p.BTCMealItem = '<div class="fm-inf-menu <%= isSel %>">\
      <span class="fm-inf-menu-select-badge">Selected</span>\
      <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
      <h4 class="fm-inf-menu-title" id="ariaLbl<%= index %>"><%= mealMenuName %></h4>\
      <p class="fm-inf-menu-desc" id="ariaDesc<%= index %>"><%= mealMenuDesc %></p>\
      <div class="fm-footnote-txt">\
          <span class="food-icon-set"><%= foodIconSet %></span>\
          <input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" aria-labelledby="ariaLbl<%= index %>" aria-describedby="ariaDesc<%= index %>">\
      </div>\
  </div>';

  p.BTCMealItemWithImage = '<div class="fm-inf-menu with-image <%= isSel %>">\
      <span class="fm-inf-menu-select-badge">Selected</span>\
     <span class="special-badge <%= isSpecial %>"><%= btcSpecialMealName %></span>\
      <div class="meal-selection-info">\
          <h4 class="fm-inf-menu-title" id="ariaLbl<%= index %>"><%= mealMenuName %></h4>\
          <p class="fm-inf-menu-desc" id="ariaDesc<%= index %>"><%= mealMenuDesc %></p>\
          <div class="fm-footnote-txt">\
              <span class="food-icon-set"><%= foodIconSet %></span>\
          </div>\
          <input type="button" name="select" value="<%= selected %>" class="fm-select-btn right" data-mealCatType="btc-meal-list" data-btn-index="<%= index %>" data-meal-tab-index="<%= tabIndex %>" data-segment="<%= segment %>" data-sector="<%= sector %>" data-paxid="<%= paxid %>" data-meal-category="<%= mealCatCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealName %>" aria-labelledby="ariaLbl<%= index %>" aria-describedby="ariaDesc<%= index %>">\
      </div>\
      <img src="<%= btcImg %>" />\
  </div>';

  p.BTCLoadMoreBtn = '<div class="load-more"><input type="button" name="select" value="Load more" class="load-more-btn"></div>';
  p.searchMealTitleTpl = '<p class="title-4--blue"><%= mealName %></p>';

  // applied special meal prompt
  p.spmlAlertPrompt = $('<div class="spml-confirmation-prompt confirmation-prompt">\
      <em class="ico-alert"></em>\
      <p class="meal-sub-title-black">Please note that your meal choice has been changed to  <span class="spml-name"></span>. You may continue to make your own selection if you prefer another meal.\
    </div>');

  var mealSelEvent = null;
  var bckAllMealsEvent = null;
  
  p.removeButton = "<a class='meal-menu-remove-button' data-remove-trigger='1' href='#'>Remove</a>";

  var init = function(
    data,
    mealIcons,
    segment,
    sector,
    paxid,
    mealServiceCode,
    jsonData,
    accContent,
    animateClss) {
    p.BTCJson = data;
    p.mealIcons = mealIcons;
    p.segment = segment;
    p.sector = sector;
    p.paxid = paxid;
    p.mealServiceCode = mealServiceCode;
    p.btcMealCatCode = data[0].mealCategoryCode;
    p.data = jsonData;

    var btcMeal = paintBTCMeal(animateClss);
    backAllMealsEvent(btcMeal);
    mealSelectionEvent(btcMeal);

    accContent.append(btcMeal);
  };

  var paintBTCMeal = function(animateClss) {
    var animateClss = (animateClss != '') ? ' pre-slide-in-left hidden' : '';
    var bookTheCookEl = $('<div class="meal-sub-menu-wrap book-the-cook' + animateClss + '" />');

    // add back the all meals link
    bookTheCookEl.append(p.back2AllMealsLinkTpl);

    // add meal menu category name
    var mealMenuCatName = $(_.template(p.BTCTitleTpl, {
      txt: p.BTCJson[0].mealMenuCategoryName
    }));
    bookTheCookEl.append(mealMenuCatName);

    // add description
    bookTheCookEl.append(p.BTCDescTpl);

    // add search control
    bookTheCookEl.append(p.BTCSearch);

    var mealSubcategoryJson = p.BTCJson[0].mealSubcategory;
    var cruisineCatListItems = '';
    var allMealListing = $('<div class="seatmap btc-meal-list" role="application"></div>');
    var mealListingMaxNum = 5;
    var otherTabs = '';
    var totalTabs = mealSubcategoryJson.length;
    for (var i = 0; i < totalTabs; i++) {
      // create the Cuisine Category list
      // add default active to first
      var isActive = '';
      if (i == 0) {
        isActive = 'active';
      }

      var el = $(_.template(p.BTCCruisineCatListItemTpl, {
        'isActive': isActive,
        'menuName': mealSubcategoryJson[i].mealSubCategoryName,
        'tabMenu': i
      }));

      cruisineCatListItems += el[0].outerHTML;

      if (totalTabs > 4 && i > 2) {
        var opt = _.template(p.selOptionItemTpl, {
          'tabMenu': i,
          'menuName': mealSubcategoryJson[i].mealSubCategoryName
        });

        otherTabs += opt;
      }

      // create the Cuisine Category - Meal listing
      var mealListingWrapper = (i == 0) ?
        $('<div data-tab-content="' + i + '"></div>') :
        $('<div class="hidden" data-tab-content="' + i + '"></div>');
      var cruisineCatMealListingJson = mealSubcategoryJson[i].Meals;
      for (var j = 0; j < cruisineCatMealListingJson.length; j++) {
        if (j < mealListingMaxNum) {
          mealListingWrapper.append(
            paintBTCEachMealItem(cruisineCatMealListingJson[j], j, i)
          );
        }
      }

      // check if there is more than 5 items, then add a load more button
      if (cruisineCatMealListingJson.length > mealListingMaxNum) {
        mealListingWrapper.append(p.BTCLoadMoreBtn);
      }

      allMealListing.append(mealListingWrapper);
    }

    // add to Cuisine Category list
    var cruisineCatListEl = $(_.template(p.BTCCruisineCatListTpl, {
      'catListItems': cruisineCatListItems
    }));

    // add to cruisine category main wrapper
    var cruisineCatMainWrapper = $(_.template(p.BTCCuisineCatMainWrapperTpl, {
      'tabList': cruisineCatListEl[0].outerHTML,
      'contentList': allMealListing[0].outerHTML
    }));

    cruisineCatMainWrapper.find('[name="calcTabSelect"]').append(otherTabs);

    // add to book the cook menu main wrapper
    bookTheCookEl.append(cruisineCatMainWrapper);

    // attach events
    loadMoreBtnEvent(cruisineCatMainWrapper, mealSubcategoryJson, mealListingMaxNum);
    searchBtnEvent(bookTheCookEl);

    var simpleTab = new SIA.VerticalTab(
      bookTheCookEl.find('.btc-meal-nav'),
      bookTheCookEl.find('.btc-meal-list'),
      bookTheCookEl.find('btc-meal-select select'),
      p.BTCJson[0].mealSubcategory.length);
    simpleTab.init();

    var moreTab = new SIA.MoreTab(cruisineCatMainWrapper.find('.booking-nav__item'), cruisineCatMainWrapper.find('[name="calcTabSelect"]'));
    moreTab.init();

    return bookTheCookEl;
  };

  var paintBTCEachMealItem = function(data, btnIndex, tabIndex) {
    // check if it's special meal
    var isSpecialAvailable = (data.isSpecialMeal === 'true') ? '' : 'hidden';

    var btcSpecialMealName;
    if (data.isSpecialMeal === 'true' && typeof data.specialMealName !== 'undefined' ) {
      btcSpecialMealName = data.specialMealName;
    } else {
      btcSpecialMealName = 'SPECIAL';
    }


    var footLogo = 'hidden';
    var footIcon = '';
    var specialityFootNote = '';
    if (typeof data.specialityInfo !== 'undefined') {
      // footLogo = '';
      var foodIconSet = '';
      for (var l = 0; l < data.specialityInfo.length; l++) {
        var br = l === data.specialityInfo.length -1 ? '' : '<br>';
        foodIconSet += '<em class="' +SIA.MealsSelection.public.inflightDishIcons[data.specialityInfo[l].dishIconId]+ ' fm-footnote-logo"></em>\
        <span class="fm-dish-text"> ' + data.specialityInfo[l].specialityFootNote + ' </span>' + br;
      }

      // specialityFootNote = data.specialityInfo.specialityFootNote;
      // footIcon = p.mealIcons[data.specialityInfo.dishIconId];
    }

    // check for selected menu in data
    var selMealsel = p.data.paxList[parseInt(p.paxid)].selectedMeals[parseInt(p.segment)].selectedMeal;
    selMeal = selMealsel[p.mealServiceCode];
    var isSel = '';
    var selected = 'Select';
    if (typeof selMeal != 'undefined' && selMeal.mealMenuName == data.mealMenuName) {
      isSel = 'selected';
      selected = 'Remove';
    }

    var tpl;
    if(typeof data.mealImage !== 'undefined' && data.mealImage !== '') {
      tpl = p.BTCMealItemWithImage;
    }else {
      tpl = p.BTCMealItem;
    }

    return $(_.template(tpl, {
      'isSel': isSel,
      'selected': selected,
      'isSpecial': isSpecialAvailable,
      'mealMenuName': data.mealMenuName,
      'mealMenuDesc': data.mealMenuDescription,
      // 'footLogo': footLogo,
      // 'footIcon': footIcon,
      // 'specialityFootNote': specialityFootNote,
      'foodIconSet': foodIconSet,
      'index': btnIndex,
      'tabIndex': tabIndex,
      'mealName': data.mealMenuName,
      'mealCode': data.mealCode,
      'segment': p.segment,
      'sector': p.sector,
      'paxid': p.paxid,
      'mealServiceCode': p.mealServiceCode,
      'mealCatCode': p.btcMealCatCode,
      'btcImg': data.mealImage,
      'btcSpecialMealName': btcSpecialMealName
    }));
  }

  var loadMoreBtnEvent = function(el, data, maxPerList) {
    var loadMoreBtns = el.find('.load-more-btn');
    var content = '';

    for (var i = 0; i < loadMoreBtns.length; i++) {
      $(loadMoreBtns[i]).on('click', function() {
        var t = $(this);

        // get parent tab content
        var tabContentIndex = t.parent().parent().attr('data-tab-content');

        // get parent load more button
        var loadMoreBtnMain = t.parent();

        // get current index data
        var currentData = data[tabContentIndex].Meals;

        // compute if there is more than 5 meals left
        var leftMealsLength = currentData.length - maxPerList;
        var limitResult = (leftMealsLength >= maxPerList) ? true : false;
        for (var j = maxPerList, k = 0; j < currentData.length; j++, k++) {
          if (k == (maxPerList - 1) && limitResult) {
            break;
          }

          var el = paintBTCEachMealItem(currentData[j], j, tabContentIndex);
          mealSelectionEvent(el);
          el.insertBefore(loadMoreBtnMain);

          if (k === 0) el.find('.fm-select-btn').focus();
        }

        // check if there is more left data, then hide the load more button
        // get total list length on tab content
        var tabContentMain = t.parent().parent();
        var tabContentLength = tabContentMain.find('.fm-inf-menu').length;
        if (tabContentLength == currentData.length) {
          loadMoreBtnMain.addClass('hidden');
        }
      });
    }
  };

  var searchJSON = function(data, val) {
    var result = [];
    $.each(data, function(i, value) {
      $.each(data[i].Meals, function(j, value) {
        var mealMenuName = value.mealMenuName.toLowerCase();
        var mealMenuDescription = value.mealMenuDescription.toLowerCase();

        val = val.toLowerCase().trim();
        if ((mealMenuName.indexOf(val) >= 0) || (mealMenuDescription.indexOf(val) >= 0)) {
          result.push({
            'cat': i,
            'item': j
          });
        }
      });
    });

    return result;
  };

  var updateAttrDatas = function(el) {
    p.segment = el.attr('data-segment');
    p.sector = el.attr('data-sector');
    p.paxid = el.attr('data-curpax');
    p.mealServiceCode = el.attr('data-meal-service-code');
  };

  var searchBtnEvent = function(el) {
    var searchBtns = el.find('.search-btn');
    var clrSearchBtns = el.find('.clear-search');
    var tabContent = el.find('.tab-content');
    var searchMsgEl = el.find('.search-result-msg');
    var searchListClss = 'btc-meal-list';

    $.each(searchBtns, function() {
      searchBtns.on('click', function(e) {
        e.preventDefault();

        var t = $(this);

        updateAttrDatas(t.closest('.book-the-cook').parent().find('.all-meals-menu .item-container'));

        t.closest('.static-details').parent().next().find('.btc-meal-list').remove();

        // remove the btc meal list
        el.find('.btc-meals').remove();
        t.closest('.btc-search').next().removeClass('hidden');

        var val = t.closest('.static-content').find('[name="input-search-1"]').val();
        var mealSubCat = p.BTCJson['0'].mealSubcategory;
        var result = searchJSON(mealSubCat, val);

        var searchMsg = '';
        if (result.length >= 1) {
          searchMsg = result.length + ' search results for\'' + val + '\'';

          // generate meals blocks
          tabContent.append('<div class="' + searchListClss + '"></div>');
          var searchList = tabContent.find('.' + searchListClss);
          var resCatIds = [];
          $.each(result, function(i, meal) {
            var res = result[i];
            if (resCatIds.indexOf(res.cat) <= -1) {
              resCatIds.push(res.cat);
              searchList.append(_.template(p.searchMealTitleTpl, { 'mealName': mealSubCat[res.cat].mealSubCategoryName }));
            }

            searchList.append(paintBTCEachMealItem(mealSubCat[res.cat].Meals[res.item], res.item, 0));
          });

          mealSelectionEvent(tabContent.find('.' + searchListClss));
        } else {
          // if no search result found
          searchMsg = 'There are no meals matching\'' + val + '\'. There are no results. Amend your search criteria to try again.';
          searchMsgEl.parent().addClass('no-result');
        }

        // generate search result message
        searchMsgEl.html(searchMsg);
        searchMsgEl.removeClass('hidden');
      });
    });

    $.each(clrSearchBtns, function() {
      clrSearchBtns.on('click', function(e) {
        e.preventDefault();

        var t = $(this);

        // get selected meal data index
        var selItem = t.closest('.static-content').next().find('.btc-meal-list .fm-inf-menu');
        $.each(selItem, function(i, value) {
          var selInput = $(selItem[i]).find('.fm-select-btn');
          if (selInput.attr('value') == 'Remove') {
            p.doSearch = true;
            p.selIndex = selInput.attr('data-btn-index');
          }
        });

        updateAttrDatas(t.closest('.book-the-cook').parent().find('.all-meals-menu .item-container'));

        // repaint the btc meal list
        var accordionContent = t.closest('.accordion__content');
        accordionContent.find('.book-the-cook').remove();
        accordionContent.append(paintBTCMeal(''));

        // attached event
        mealSelectionEvent(accordionContent.find('.btc-meal-list'));
        backAllMealsEvent(accordionContent.find('.book-the-cook'));
      });
    });
  };

  var backAllMealsEvent = function(el) {
    var allBackMealsBtn = el.find('.btn-back');
    for (var i = 0; i < allBackMealsBtn.length; i++) {
        $(allBackMealsBtn[i]).off().on('click', function(e) {
            e.preventDefault();

            var t = $(this);

            // get the accordion tab main content
            var accordionTabMainContent = t.parent().parent().parent();

            // get the current tab index content parent
            var currentIndexTabContent = t.parent().parent();

            currentIndexTabContent.addClass('hidden');

            // show the all meals content
            accordionTabMainContent.find('.all-meals-menu').removeClass('sub-menu-on');

            // hide book the cook
            currentIndexTabContent.addClass('pre-slide-in-left');

            currentIndexTabContent.removeClass('hidden');
        });
    }
  };

  var mealSelectionEvent = function(el) {
      var allSelectionBtn = el.find('.fm-select-btn');

      for (var i = 0; i < allSelectionBtn.length; i++) {
          $(allSelectionBtn[i]).parent().parent().on('click', function(e) {
              e.preventDefault();

              var t = $(this).find('.fm-select-btn');
              var parentEl = t.parent().parent();
              var parentElList = parentEl.parent();
              var accordionContent = t.closest('.accordion__content');
              var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
              var parentMealsList = t.closest('.' + t.attr('data-mealCatType'));
              var currMealTabIndex = t.attr('data-meal-tab-index');
              var badgeEl = $(parentEl).children();
              var btnIndex = t.attr('data-btn-index');

               // search and remove any previous selected meal
              SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

              // check if there is selected meal in inflight
              if (p.lastTick != null) {
                  p.lastTick = null;
                  parentMealsList.data('mealTabLast', '');
              }

              // add default selected
              if (i == 0) {
                  parentMealsList.data('mealLastSelect', 0);
                  parentMealsList.data('mealTabLast', 0);
              }

              // check if different tab index
              if (parentMealsList.data('mealTabLast') != currMealTabIndex) {
                  parentMealsList.data('mealLastSelect', '');
                  parentMealsList.data('mealTabLast', currMealTabIndex);
              }

              // clear if do a search in btc meals
              if (p.doSearch) {
                  parentMealsList.data('mealLastSelect', p.selIndex);
                  p.selIndex = false;
              }

              // check if current meal card is selected again, then remove it
              if (parentMealsList.data('mealLastSelect') === btnIndex) {
                  // Add selected class to the input and to the container
                  parentEl.removeClass('selected');
                  t.removeClass('selected');

                  // remove hidden class to show the selected badge
                  $(badgeEl["0"]).addClass('hidden');

                  t.attr('value','Select');

                  parentMealsList.data('mealLastSelect', '');

                  // change accordion tab previously selected mean to its default text('inflight menu')
                  var inflLbl = 'Inflight Menu';
                  if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                  accordionTabMealTitle.html(inflLbl);

                  defaultSelection(t);

                  SIA.MealsSelection.updateData(t);

                  // Check if there are selected special meals then select same meal for this service code
                  for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
                    var spml = SIA.MealsSelection.public.spm[i];
                    if(spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
                      spml.selectSegmentMeal();
                      p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
                      parentEl.after(p.spmlAlertPrompt);

                      $(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function(e) {
                          var t = $(e.target);
                          if(t.hasClass('passenger-info__text')
                            || t.hasClass('meal-tab-item')
                            || t.hasClass('accordion__control')
                            || t.hasClass('ico-point-d')
                            || t.hasClass('fm-ico-text')
                            || t.hasClass('meal-service-name')
                            || t.hasClass('flight-ind')
                            || t.hasClass('btn-back')
                            || t.hasClass('fm-inf-menu')
                            || t.hasClass('search-btn')
                          ) {
                            p.spmlAlertPrompt.detach();

                            $(document).off('click.spmlPromptCheck');
                          }
                      });

                      break;
                    }

                  }
              } else {
                  // Add selected class to the input and to the container
                  parentEl.addClass('selected');
                  t.addClass('selected');

                  // remove hidden class to show the selected badge
                  $(badgeEl["0"]).removeClass('hidden');

                  t.attr('value','Remove');

                  parentMealsList.data('mealLastSelect', btnIndex);

                  // get the selected mean name
                  var selMealName = parentEl.find('.fm-inf-menu-title').text();

                  // change accordion tab default text('inflight menu') to selected meal
                  var selectedLabel = '';
            if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

                    accordionTabMealTitle.html(selectedLabel + selMealName + ' (Book the Cook)');
                    if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));

                    SIA.MealsSelection.resetNotEating(t);

                    removeDefaultSelection(t);
                    removeButtonListener();

                    SIA.MealsSelection.updateData(t);
              }
          });
      }
  };

     var defaultSelection = function(el) {
        var wrapper = el.closest("[data-accordion-content]");
        var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
        var inflightBadge = wrapper.find(".inflight-badge");
        if (!wrapper.find(".fm-inf-menu.selected").length) {
            inflightBadge.removeClass("hidden");
            inflightBadge.addClass("badge-active");
            wrapperInflightMenu.addClass("selected-border");
            wrapperInflightMenu.find(".select-label").addClass("hidden");
        }
    }

    var removeDefaultSelection = function(mealButton) {
        var parent = mealButton.closest("[data-accordion-content]");
        var badge = parent.find(".inflight-badge.badge-active");
        var borderHighlight = parent.find(".ml-inflight-container");
        var alertPrompt = parent.find(".inflight-confirmation-prompt");
        badge.removeClass("badge-active");
        badge.addClass("hidden");
        borderHighlight.removeClass("selected-border");
        borderHighlight.find(".select-label").removeClass("hidden");
        alertPrompt.remove();
    };

    var removeButtonListener = function() {
        setTimeout(function() {
            $("[data-accordion-trigger]").find("[data-remove-trigger]").off()
                .on("click" ,function(evt) {
                    var self = $(this);
                    var wrapper = self.closest(".accordion-box");
                    var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
                    var inflightBadge = wrapper.find(".inflight-badge");
                    
                    inflightBadge.removeClass("hidden");
                    inflightBadge.addClass("badge-active");
                    wrapperInflightMenu.addClass("selected-border");

                    wrapper.find(".fm-inf-menu.selected").each(function() {
                        var self = $(this);
                        var mealButton = self.find(".fm-select-btn");
                        unSelectMeal(mealButton);
                        undoMeal = mealButton;
                    });

                    wrapper.find(".meal-sub-title-black").text("Inflight Menu");
                    SIA.MealsSelection.removeInputData(wrapper);
                    evt.preventDefault();
                    evt.stopPropagation();
                });
        }, 500);
    };

    var unSelectMeal = function(t){
        var parentEl = t.parent().parent();
        var parentElList = parentEl.parent();
        var accordionContent = t.closest('.accordion__content');
        var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
        var parentMealsList = t.closest('.' + t.attr('data-mealCatType'));
        var currMealTabIndex = t.attr('data-meal-tab-index');
        var badgeEl = $(parentEl).children();
        var btnIndex = t.attr('data-btn-index');

        parentEl.removeClass('selected');
        t.removeClass('selected');

        // remove hidden class to show the selected badge
        $(badgeEl["0"]).addClass('hidden');

        t.attr('value', 'Select');

        parentMealsList.data('mealLastSelect', '');

        // change accordion tab previously selected mean to its default text('inflight menu')
        var inflLbl = 'Inflight Menu';
        if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
        accordionTabMealTitle.html(inflLbl);

        defaultSelection(t);

        SIA.MealsSelection.updateData(t);

        // Check if there are selected special meals then select same meal for this service code
        for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
            var spml = SIA.MealsSelection.public.spm[i];
            if (spml.p.mealServiceCode === t.attr('data-meal-servicecode') && parseInt(spml.p.segmentId) === parseInt(t.attr('data-segment')) && spml.getSelectedSpml() !== '') {
                spml.selectSegmentMeal();
                p.spmlAlertPrompt.find('.spml-name').text(spml.p.curSMLName);
                parentEl.after(p.spmlAlertPrompt);

                $(document).off('click.spmlPromptCheck').on('click.spmlPromptCheck', function (e) {
                    var t = $(e.target);
                    if (t.hasClass('passenger-info__text')
                        || t.hasClass('meal-tab-item')
                        || t.hasClass('accordion__control')
                        || t.hasClass('ico-point-d')
                        || t.hasClass('fm-ico-text')
                        || t.hasClass('meal-service-name')
                        || t.hasClass('flight-ind')
                        || t.hasClass('btn-back')
                        || t.hasClass('fm-inf-menu')
                        || t.hasClass('search-btn')
                    ) {
                        p.spmlAlertPrompt.detach();

                        $(document).off('click.spmlPromptCheck');
                    }
                });

                break;
            }

        }
    };

  var oPublic = {
    init: init,
    paintBTCMeal: paintBTCMeal,
    public: p
  };

  return oPublic;
};

SIA.SpecialMeal = function(jsonData,
    paxData,
    accContent,
    segmentId,
    sectorId,
    serviceId,
    paxId,
    mealServiceCode) {
    var p = {};

    p.vTab = '<div data-fixed="true" class="sidebar">\
          <div class="inner" role="application">\
              <ul class="booking-nav special-meal-nav" role="tablist"></ul>\
          </div>\
          <div data-customselect="true" class="custom-select custom-select--2 custom-select--3 multi-select tab-select special-meal-select" data-select-tab="true">\
              <label for="calcTabSelect<%= menuId %>" class="select__label">&nbsp;</label>\
              <span class="select__text"><%= mealCat %></span><span class="ico-dropdown"></span>\
              <select id="smTabSelect<%= menuId %>" name="smTabSelect<%= menuId %>">\
              </select>\
          </div>\
      </div>\
      <div class="seatmap special-meal-list" role="application"></div>';
    p.tabItem = '<li class="booking-nav__item tab-item<%= active %>" role="tab"  data-tab-menu="<%= tabId %>">\
          <a href="#" role="tab" class="meal-tab-item">\
              <span class="passenger-info">\
                  <span class="passenger-info__text"><%= label %></span>\
              </span>\
          </a>\
      </li>';

    p.menuItem = '<div class="fm-inf-menu <%= selectedClass %>">\
          <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
          <h4 class="fm-inf-menu-title"><%= menuName %></h4>\
          <p class="fm-inf-menu-desc"><%= menuDescription %></p>\
          <div class="fm-footnote-txt">\
              <input type="button" name="" value="<%= inputVal %>" data-mealcattype="speacial-meal-list" data-specialMealSelect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
          </div>\
          <div data-prompt="with-selection" class="confirmation-prompt confirmation-prompt--blue hidden">\
            <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change your other special meals selection from <%= origin %> to <%= dest %> to <span><%= menuName %></span> as well. </p>\
            <div class="button-group-1">\
              <input type="button" value="Proceed" class="btn-1 btn-proceed">\
              <a href="#" class="btn-2 btn-cancel">Cancel</a>\
            </div>\
          </div>\
          <div data-prompt="initial" class="confirmation-prompt hidden">\
           <em class="ico-alert"></em>\
            <p class="meal-sub-title-black">For your convenience, all your meal choices from <%= origin %> to <%= dest %> have been changed to <span><%= menuName %></span> as well. If you&#8217d prefer other meals, make your selections below. </p>\
          </div>\
      </div>';

          // <div data-prompt="initial" class="confirmation-prompt hidden">\
          //     <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change all your meal selections from <%= origin %> to <%= dest %> to <span><%= menuName %></span>. </p>\
          //     <div class="button-group-1">\
          //       <input type="button" value="Proceed" class="btn-1 btn-proceed">\
          //       <a href="#" class="btn-2 btn-cancel">Cancel</a>\
          //     </div>\
          //   </div>\

    // Array to store all meals selection
    p.mealsArr = [];

      p.curPrompt = null;

      p.curSMLName = '';

    p.removeButton = "<a class='meal-menu-remove-button' data-remove-trigger='1' href='#'>Remove</a>";

    var init = function() {
        p.data = paxData;
        p.jsonData = jsonData;

        p.curPaxId = paxId;
        p.segmentId = segmentId;
        p.sectorId = sectorId;
        p.serviceId = serviceId;
        p.mealCatCode = jsonData[0].mealCategoryCode;
        p.mealCatName = jsonData[0].mealMenuCategoryName;
        p.totalTabs = jsonData[0].mealSubcategory.length;
        p.mealServiceCode = mealServiceCode;

        p.specialMealWrap = accContent.find('.special-meal-wrap');
        p.contentWrap = p.specialMealWrap.find('.responsive.special-meals');

        p.specialVTab = $(_.template(p.vTab, {
            'menuId':  ''+segmentId+sectorId+serviceId,
            'mealCat': p.mealCatName
        }));
        p.contentWrap.append(p.specialVTab);

        paintTabMenu();
        paintTabContent();
        backToMainMenu();
        
        // var firstInput = $(p.menuList.find('.fm-select-btn')[0]);
        // firstInput.attr('data-first-focus','true');

        p.simpleTab = new SIA.VerticalTab(p.tabMenu, p.menuList, p.tabSelect, p.totalTabs);
        p.simpleTab.init();

    };

    var paintTabMenu = function(){
        p.tabMenu = p.specialVTab.find('.special-meal-nav');
        p.tabSelect = p.specialVTab.find('.special-meal-select select');

        for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
            var mealCatArr = p.jsonData[0].mealSubcategory[i];
            var active = '';
            var selected = '';

            if(i==0) {
                active = ' active';
                selected = ' selected="true"';
            }

            var tabItem = $(_.template(p.tabItem, {
                'tabId':  i,
                'label': mealCatArr.mealSubCategoryName,
                'active': active
            }));

            // Since on tablet portrait you can fit 4 tabs, only add options greater than 3rd index
            if(i > 2 && p.totalTabs > 4) {
                var tabOption = $('<option data-id="'+i+'" value="'+mealCatArr.mealSubCategoryName+'"'+selected+' />');

                p.tabSelect.append(tabOption);
            }

            p.tabMenu.append(tabItem);
        }
    };

    var paintTabContent = function(){
        p.menuList = p.contentWrap.find('.special-meal-list');
        var curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

        for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
            var mealCatArr = p.jsonData[0].mealSubcategory[i];
            // Class for the content tab wrapper
            var hidden = '';
            if(i > 0) hidden = ' class="hidden"';

            var menuItemsWrap = $('<div data-tab-content="'+i+'"'+hidden+' />');
            for (var j = 0; j < mealCatArr.Meals.length; j++) {
                var menu = mealCatArr.Meals[j];

                var selectClass = '';
                var viewOnlyClass = ' hidden';
                var mealCatCode = p.jsonData[0].mealCategoryCode;

                // check for any previous selected item
                if(curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == menu.mealCode){
                    selectedClass =  'selected';
                    inputVal = 'Remove';
                    isSelected = '';

                    p.lastTick = p.mealsArr.length;
                }else {
                    selectedClass =  '';
                    inputVal = 'Select';
                    isSelected = 'hidden';
                }

                if(typeof menu.isViewOnly != 'undefined' && menu.isViewOnly) {
                    selectClass = ' hidden';
                    viewOnlyClass = '';
                }

                var origin = SIA.MealsSelection.public.json.flightSegment[p.segmentId].departureAirportCode;
                var dest = SIA.MealsSelection.public.json.flightSegment[p.segmentId].arrivalAirportCode;

                var menuItem = $(_.template(p.menuItem, {
                    'id': p.mealsArr.length,
                    'menuId':  i+'-'+j,
                    'menuName': menu.mealMenuName,
                    'menuDescription': menu.mealMenuDescription,
                    'segmentId': p.segmentId,
                    'sectorId': p.sectorId,
                    'paxId': p.curPaxId,
                    'mealCategoryCode': p.mealCatCode,
                    'mealServiceCode': mealServiceCode,
                    'selectClass': selectClass,
                    'mealServiceId': serviceId,
                    'mealCode': menu.mealCode,
                    'mealMenuName': menu.mealMenuName,
                    'origin': origin,
                    'dest': dest
                }));

                menuItemsWrap.append(menuItem);

                p.mealsArr.push(menuItem);

                menuItem.find('input.fm-select-btn').each(function(){
                    addInputListener($(this));
                });
            }

            p.menuList.append(menuItemsWrap);
        }
    };

    var backToMainMenu = function(){
        // add back to all meals button event
        p.specialMealWrap.find('.fm-backtomeals-btn .btn-back').off().on('click', function(e) {
            e.preventDefault();
            var subMenuWrapper = $(this).parent().parent();
            subMenuWrapper.addClass('hidden');

            // reset position of main menu wrapper
            subMenuWrapper.parent().find('.all-meals-menu').removeClass('sub-menu-on');

            // hide inflight menu wrapper
            subMenuWrapper.addClass('pre-slide-in-left');

            // show all menu wrapper
            var prevAllMeal = $(this).parent().parent().parent();
            p.mealCardList = null;
        });
    };

    var addInputListener = function(input) {
        input.parent().parent().off().on({
            'click': function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var t = $(this).find('.fm-select-btn');
                var parentEl = t.parent().parent();
                var accordionContent = t.closest('.accordion__content');
                var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
                var parentElList = parentEl.parent();

                var selectMealCode = t.data('meal-mealcode');
                var hasOtherSpecials = getOtherSpecials(t);
                var haveItemsToUpdate = getMoreToSelect(t);

                var curAccordWrapper = SIA.MealsSelection.public.mealSelectionForm.find('.flightmeal-control')[p.segmentId];
                //var similarInput = $(curAccordWrapper).find('input[data-meal-category="SPML"].selected');

                // check if current meal card is selected again, then remove it
                var badgeEl = $(parentEl).children();

                if (t.val() === 'Remove') {
                    unSelectMeal(t);

                    // change accordion tab previously selected mean to its default text('inflight menu')
                    var inflLbl = 'Inflight Menu';
                    if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                    accordionTabMealTitle.html(inflLbl);

                    SIA.MealsSelection.updateData(t);
                } else {
                    if(haveItemsToUpdate) {
                        if(hasOtherSpecials.length > 0) {
                            var prompt = t.parent().parent().find('[data-prompt="with-selection"]');
                        }else {
                            var prompt = t.parent().parent().find('[data-prompt="initial"]');
                        }

                        // Remove any selected prompt
                        if(p.curPrompt !== null) {
                            p.curPrompt.prev().find('.fm-select-btn').val('Select');
                            p.curPrompt.addClass('hidden');
                            p.curPrompt.parent().removeClass('int-prompt');
                            p.curPrompt.find('.btn-cancel').off();
                            p.curPrompt.find('.btn-proceed').off();
                        }

                        prompt.removeClass('hidden');

                        if(hasOtherSpecials.length > 0) {
                          t.val('Selected');
                          prompt.parent().addClass('int-prompt');
                          prompt.find('.btn-cancel').off().on({
                              'click': function(e){
                                  e.preventDefault();
                                  e.stopImmediatePropagation();

                                  t.val('Select');
                                  prompt.addClass('hidden');
                                  prompt.parent().removeClass('int-prompt');

                                  p.curPrompt = null;
                              }
                          });

                          prompt.find('.btn-proceed').off().on({
                              'click': function(e){
                                  e.preventDefault();
                                  e.stopImmediatePropagation();

                                  // remove listener to prevent double calls on otherSM
                                  $(this).off();

                                  // reset other meals before assigning selected class to current item
                                  SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

                                  selectMeal(t);

                                  // apply selection to other special meals in same leg
                                  selectSameMeal(t, hasOtherSpecials);

                                  // assign data
                                  var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

                                  accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
                                  if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
                                  SIA.MealsSelection.updateData(t);
                              }
                          });
                        }else {
                          // reset other meals before assigning selected class to current item
                          SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

                          var sameSpecialsSelected = getSameSpecials(t);

                          selectMeal(t);

                          // apply selection to other special meals in same leg
                          selectSameMeal(t, hasOtherSpecials);

                          // assign data
                          var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

                          accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
                          if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
                          removeButtonListener();
                          
                          SIA.MealsSelection.updateData(t);

                          if(sameSpecialsSelected.items.length < sameSpecialsSelected.length-1) {
                            prompt.removeClass('hidden');
                            $(document).off().on('click.promptCheck',function(e) {
                                var t = $(e.target);
                                if(t.hasClass('passenger-info__text')
                                  || t.hasClass('meal-tab-item')
                                  || t.hasClass('accordion__control')
                                  || t.hasClass('meal-sub-title-black')
                                  || t.hasClass('ico-point-d')
                                  || t.hasClass('fm-ico-text')
                                  || t.hasClass('meal-service-name')
                                  || t.hasClass('flight-ind')
                                  || t.hasClass('btn-back')
                                ) {
                                  p.curPrompt.addClass('hidden');
                                  p.curPrompt.parent().removeClass('int-prompt');

                                  $(document).off('click.promptCheck');
                                }
                            });
                          }
                        }

                        p.curPrompt = prompt;
                    }else {
                        // reset other meals before assigning selected class to current item
                        SIA.MealsSelection.resetMeals(t, ['ifm', 'btcm', 'spm']);

                        selectMeal(t);

                        // assign data
                        var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

                        accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
                        if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
                        removeButtonListener();

                        SIA.MealsSelection.updateData(t);
                    }
                }
            }
        });
    };

    var getOtherSpecials = function (meal) {
        var otherSelectedSM = [];
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];
        _.each(curSegment.selectedMeal, function (v, k, l) {
            if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode !== meal.attr('data-meal-mealcode')) {
                otherSelectedSM.push({ 'k': k, 'val': v });
            }
        });

        return otherSelectedSM;
    };

    var getSameSpecials = function (meal) {
        var sameSelectedSM = {};
        sameSelectedSM['length'] = 0;
        sameSelectedSM['items'] = [];
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];
        _.each(curSegment.selectedMeal, function (v, k, l) {
            if (k !== mealServiceCode && v.mealCategoryCode === 'SPML' && v.mealCode === meal.attr('data-meal-mealcode')) {
                sameSelectedSM.items.push({ 'k': k, 'val': v });
            }

            sameSelectedSM.length++;
        });

        return sameSelectedSM;
    };

    var getMoreToSelect = function (meal) {
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];

        var hasSPMLToChange = false;
        _.each(curSegment.selectedMeal, function (v, k, l) {
            if (k !== meal.attr('data-meal-servicecode') && (v.mealCategoryCode === 'SPML' || v.mealCategoryCode === null) && v.isEating) {
                hasSPMLToChange = true;
            }
        });

        return hasSPMLToChange;
    };

    var selectSameMeal = function (meal) {
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];

        // loop through each special meal in MealSelection module
        for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
            var spml = SIA.MealsSelection.public.spm[i];

            if (p.curPaxId !== spml.p.curPaxId) continue;

            spml.p.specialMealWrap.find('.fm-select-btn').each(function () {
                var t = $(this);

                var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];
                if (typeof curCatCode !== 'undefined'
                    && curCatCode.isEating
                    && t.attr('data-meal-category') === 'SPML'
                    && (curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {

                    // Unselect other selected special meals in same sector
                    if (t.attr('data-segment') === meal.attr('data-segment')) {
                        unSelectMeal(t);
                    }

                    // Find the selected special meal in current segment and select it
                    if (t.attr('data-segment') === meal.attr('data-segment')
                        && t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode')) {
                        selectMeal(t);
                        
                        var parentEl = t.parent().parent();
                        var accordionContent = t.closest('.accordion__content');
                        var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');

                        // SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
                        var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';
            
                        accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals)');
                        if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
                        SIA.MealsSelection.updateData(t);
                    }
                }
            });
        }
    };

    var getSelectedSpml = function () {
        var selectedSPML = '';
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];
        _.each(curSegment.selectedMeal, function (v, k, l) {
            if (v.mealCategoryCode === 'SPML' && v.mealCode !== null) {
                selectedSPML = v.mealCode;
            }
        });

        return selectedSPML;
    };

    // var selectSameMeal = function(meal, otherSM){
    //   var pax = SIA.MealsSelection.public.data.paxList[paxId];
    //   var curSegment = pax.selectedMeals[segmentId];

    //   // loop through each special meal in MealSelection module
    //   for (var i = 0; i < SIA.MealsSelection.public.spm.length; i++) {
    //     var spml = SIA.MealsSelection.public.spm[i];

    //     if(p.curPaxId !== spml.p.curPaxId) continue;

    //     spml.p.specialMealWrap.find('.fm-select-btn').each(function(){
    //       var t = $(this);

    //       var curCatCode = curSegment.selectedMeal[t.attr('data-meal-servicecode')];

    //       if(typeof curCatCode !== 'undefined' && curCatCode.isEating && t.attr('data-meal-category') === 'SPML' && (curCatCode.mealCategoryCode === 'SPML' || curCatCode.mealCategoryCode === null)) {
    //         // Unselect other selected special meals in same sector
    //         if( t.attr('data-segment') === meal.attr('data-segment') ) {
    //           unSelectMeal(t);
    //         }

    //         // Find the selected special meal in current segment and select it
    //         if( t.attr('data-segment') === meal.attr('data-segment')
    //         && t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode') ) {
    //           selectMeal(t);

    //           var parentEl = t.parent().parent();
    //           var accordionContent = t.closest('.accordion__content');
    //           var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');

    //           // SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
    //           accordionTabMealTitle.text(parentEl.find('.fm-inf-menu-title').text() + ' (Special Meal)');
    //           SIA.MealsSelection.updateData(t);
    //         }
    //       }

    //     });
    //   }
    // };

    // Used to select special meals when a meal is unselected from BTC or INFLM
    var selectSegmentMeal = function(mealServiceCode, segmentId){
        var selectedMealCode = getSelectedSpml();

        for (var i = 0; i < p.mealsArr.length; i++) {
          var meal = p.mealsArr[i].find('.fm-select-btn');
          if(meal.attr('data-meal-mealcode') === selectedMealCode) {

            var parentEl = meal.parent().parent();
            var accordionContent = meal.closest('.accordion__content');
            var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');

            selectMeal(meal);

            p.curSMLName = meal.attr('data-meal-menuname');

            var selectedLabel = '';
            if(p.isPey || p.isEco) selectedLabel = '<span class="title-selected">Selected:</span> ';

            accordionTabMealTitle.html(selectedLabel + parentEl.find('.fm-inf-menu-title').text() + ' (Special Meals) ');
            if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) accordionTabMealTitle.append($(p.removeButton));
            // assign data
            SIA.MealsSelection.updateData(meal);

            break;
          }
        }
    };

    var selectMeal = function(meal){
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        // Add selected class to the input and to the container
        parentEl.addClass('selected');
        meal.addClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.removeClass('hidden');

        meal.attr('value', 'Remove');
        removeDefaultSelection(meal);
        meal.closest("[data-accordion-content]").trigger("specialMealSelect");
        removeButtonListener();
    };

    var unSelectMeal = function(meal){
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // Add selected class to the input and to the container
        parentEl.removeClass('selected');
        meal.removeClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.addClass('hidden');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        meal.attr('value', 'Select');
        defaultSelection(meal);
    };

    var defaultSelection = function(el) {
        var wrapper = el.closest("[data-accordion-content]");
        var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
        var inflightBadge = wrapper.find(".inflight-badge");
        if (!wrapper.find(".fm-inf-menu.selected").length) {
            inflightBadge.removeClass("hidden");
            inflightBadge.addClass("badge-active");
            wrapperInflightMenu.addClass("selected-border");
            wrapperInflightMenu.find(".select-label").addClass("hidden");
        }
    }

    var removeDefaultSelection = function(mealButton) {
        var parent = mealButton.closest("[data-accordion-content]");
        var badge = parent.find(".inflight-badge.badge-active");
        var borderHighlight = parent.find(".ml-inflight-container");
        var alertPrompt = parent.find(".inflight-confirmation-prompt");
        badge.removeClass("badge-active");
        badge.addClass("hidden");
        borderHighlight.removeClass("selected-border");
        borderHighlight.find(".select-label").removeClass("hidden");
        alertPrompt.remove();
    };

    var removeButtonListener = function() {
        setTimeout(function() {
            $("[data-accordion-trigger]").find("[data-remove-trigger]").off()
                .on("click" ,function(evt) {
                    var self = $(this);
                    var wrapper = self.closest(".accordion-box");
                    var wrapperInflightMenu = wrapper.find(".ml-inflight-container");
                    var inflightBadge = wrapper.find(".inflight-badge");
                    
                    inflightBadge.removeClass("hidden");
                    inflightBadge.addClass("badge-active");
                    wrapperInflightMenu.addClass("selected-border");

                    wrapper.find(".fm-inf-menu.selected").each(function() {
                        var self = $(this);
                        var mealButton = self.find(".fm-select-btn");
                        unSelectMeal(mealButton);
                        undoMeal = mealButton;
                    });

                    wrapper.find(".meal-sub-title-black").text("Inflight Menu");
                    SIA.MealsSelection.removeInputData(wrapper);
                    evt.preventDefault();
                    evt.stopPropagation();
                });
        }, 500);
    };

    var oPublic = {
          init: init,
          selectSegmentMeal: selectSegmentMeal,
          getSelectedSpml: getSelectedSpml,
          p: p
      };

      return oPublic;
    };

SIA.InfantMeal = function(jsonData,
    paxData,
    accContent,
    segmentId,
    sectorId,
    serviceId,
    paxId,
    mealServiceCode) {
    var p = {};

    p.menuItem = '<div class="fm-inf-menu <%= selectedClass %>">\
            <span class="fm-inf-menu-select-badge <%= isSelected %>">Selected</span>\
            <h4 class="fm-inf-menu-title"><%= menuName %></h4>\
            <p class="fm-inf-menu-desc"><%= menuDescription %></p>\
            <div class="fm-footnote-txt">\
                <input type="button" name="" value="<%= inputVal %>" data-mealcattype="speacial-meal-list" data-infantMealSelect-id="<%= id %>" class="fm-select-btn right<%= selectClass %>" data-segment="<%= segmentId %>" data-sector="<%= sectorId %>" data-paxid="<%= paxId %>" data-meal-category="<%= mealCategoryCode %>" data-meal-servicecode="<%= mealServiceCode %>" data-service-id="<%= mealServiceId %>" data-meal-mealcode="<%= mealCode %>" data-meal-menuname="<%= mealMenuName %>">\
            </div>\
            <div data-prompt="with-selection" class="confirmation-prompt hidden">\
              <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change your other infant meals selection from <%= origin %> to <%= dest %> to <span><%= menuName %></span> as well. </p>\
              <div class="button-group-1">\
                <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
                <a href="#" class="btn-2 btn-cancel">Cancel</a>\
              </div>\
            </div>\
            <div data-prompt="initial" class="confirmation-prompt hidden">\
              <p class="meal-sub-title-blue"> Selecting <span><%= menuName %></span> will change all your meal selections from <%= origin %> to <%= dest %> to <span><%= menuName %></span>.</p>\
              <div class="button-group-1">\
                <input type="submit" value="Proceed" class="btn-1 btn-proceed">\
                <a href="#" class="btn-2 btn-cancel">Cancel</a>\
              </div>\
            </div>\
        </div>';

    // Array to store all meals selection
    p.mealsArr = [];
    p.curPrompt = null;
    p.currentMeal = null;

    var init = function() {
        p.data = paxData;
        p.jsonData = jsonData;

        p.curPaxId = paxId;
        p.segmentId = segmentId;
        p.sectorId = sectorId;
        p.serviceId = serviceId;
        p.mealCatCode = jsonData[0].mealCategoryCode;
        p.mealCatName = jsonData[0].mealMenuCategoryName;
        p.totalTabs = jsonData[0].mealSubcategory.length;
        p.totalTabs = jsonData[0].mealSubcategory.length;
        p.accContent = accContent;
        p.infantlMealWrap = accContent.find('.infant-meals');

        paintInfantMenu();

        var firstInput = $(p.infantlMealWrap.find('.fm-select-btn')[0]);
        firstInput.attr('data-first-focus','true');

        p.infmlChkBox = p.accContent.find('[data-checktype="not-eating"]');
        setTimeout(function(){
            p.infmlChkBox.on({
                'change.infm': function() {
                    var t = $(this);
                    if(t.prop('checked') && p.currentMeal !== null) {
                        unSelectMeal(p.currentMeal);
                        var accordionContent = t.closest('.accordion__content');
                        var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
                        accordionTabMealTitle.text('I don’t need an infant meal');
                    }
                    SIA.MealsSelection.updateData(t);
                }
            });
        });
    };

    var paintInfantMenu = function(){
        var curPaxData = paxData.paxList[paxId].selectedMeals[segmentId].selectedMeal[mealServiceCode];

        for (var i = 0; i < p.jsonData[0].mealSubcategory.length; i++) {
            var mealCatArr = p.jsonData[0].mealSubcategory[i];
            // Class for the content tab wrapper
            var hidden = '';
            if(i > 0) hidden = ' class="hidden"';

            if(typeof mealCatArr.Meals != 'undefined') {
                for (var j = 0; j < mealCatArr.Meals.length; j++) {
                    var menu = mealCatArr.Meals[j];

                    var selectClass = '';
                    var viewOnlyClass = ' hidden';
                    var mealCatCode = p.jsonData[0].mealCategoryCode;

                    // check for any previous selected item
                    if(curPaxData.mealCategoryCode == mealCatCode && curPaxData.mealCode == menu.mealCode){
                        selectedClass =  'selected';
                        inputVal = 'Remove';
                        isSelected = '';

                        p.lastTick = p.mealsArr.length;
                    }else {
                        selectedClass =  '';
                        inputVal = 'Select';
                        isSelected = 'hidden';
                    }

                    if(typeof menu.isViewOnly != 'undefined' && menu.isViewOnly) {
                        selectClass = ' hidden';
                        viewOnlyClass = '';
                    }

                    var origin = SIA.MealsSelection.public.json.flightSegment[p.segmentId].departureAirportCode;
                    var dest = SIA.MealsSelection.public.json.flightSegment[p.segmentId].arrivalAirportCode;

                    var menuItem = $(_.template(p.menuItem, {
                        'id': p.mealsArr.length,
                        'menuId':  i+'-'+j,
                        'menuName': menu.mealMenuName,
                        'menuDescription': menu.mealMenuDescription,
                        'segmentId': p.segmentId,
                        'sectorId': p.sectorId,
                        'paxId': p.curPaxId,
                        'mealCategoryCode': p.mealCatCode,
                        'mealServiceCode': mealServiceCode,
                        'selectClass': selectClass,
                        'mealServiceId': serviceId,
                        'mealCode': menu.mealCode,
                        'mealMenuName': menu.mealMenuName,
                        'origin': origin,
                        'dest': dest
                    }));

                    p.infantlMealWrap.append(menuItem);
                    p.mealsArr.push(menuItem);

                    menuItem.find('input.fm-select-btn').each(function(){
                        addInputListener($(this));
                    });
                }
            }
        }
    };

    var addInputListener = function(input) {
        input.parent().parent().off().on({
            'click': function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var t = $(this).find('.fm-select-btn');
                var parentEl = t.parent().parent();
                var accordionContent = t.closest('.accordion__content');
                var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');
                var parentElList = parentEl.parent();

                var selectMealCode = t.data('meal-mealcode');

                var curAccordWrapper = SIA.MealsSelection.public.mealSelectionForm.find('.flightmeal-control')[p.segmentId];
                var similarInput = $(curAccordWrapper).find('input[data-meal-category="INF"].selected');

                // check if current meal card is selected again, then remove it
                var badgeEl = $(parentEl).children();

                if (t.val() === 'Remove') {
                    unSelectMeal(t);

                    t.attr('value','Select');

                    p.lastTick = '';

                    // change accordion tab previously selected mean to its default text('inflight menu')
                    var inflLbl = 'Inflight Menu';
                    if(SIA.MealsSelection.public.isPey || SIA.MealsSelection.public.isEco) inflLbl = '<span class="title-selected">Inflight Menu</span>';
                    accordionTabMealTitle.html(inflLbl);

                    SIA.MealsSelection.updateData(t);

                    p.currentMeal = null;
                } else {
                    var hasOtherINFM = getOtherINFM(t);
                    if(hasOtherINFM.length > 0) {
                        var prompt = t.parent().parent().find('[data-prompt="with-selection"]');
                    }else {
                        var prompt = t.parent().parent().find('[data-prompt="initial"]');
                    }

                    t.val('Selected');

                    // Remove any selected prompt
                    if(p.curPrompt !== null) {
                        p.curPrompt.parent().find('.fm-select-btn').val('Select');
                        p.curPrompt.addClass('hidden');
                        p.curPrompt.parent().removeClass('int-prompt');
                        p.curPrompt.find('.btn-cancel').off();
                        p.curPrompt.find('.btn-proceed').off();
                    }

                    prompt.removeClass('hidden');
                    prompt.parent().addClass('int-prompt');

                    prompt.find('.btn-cancel').off().on({
                        'click': function(e){
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            t.val('Select');
                            prompt.addClass('hidden');
                            prompt.parent().removeClass('int-prompt');

                            p.curPrompt = null;
                        }
                    });

                    prompt.find('.btn-proceed').off().on({
                        'click': function(e){
                            e.preventDefault();
                            e.stopImmediatePropagation();

                            // remove listener to prevent double calls on otherSM
                            $(this).off();

                            // search and remove any previous selected meal
                            SIA.MealsSelection.resetMeals(t, ['infm']);

                            selectMeal(t);
                            p.currentMeal = t;

                            // apply selection to other special meals in same leg
                            selectSameMeal(t);

                            // assign data
                            accordionTabMealTitle.text(parentEl.find('.fm-inf-menu-title').text() + ' (Infant Meal)');
                            SIA.MealsSelection.updateData(t);

                            unselectChkBox();
                        }
                    });

                    p.curPrompt = prompt;

                }
            }
        });
    };

    var getOtherINFM = function(meal){
        var otherSelectedSM = [];
        var pax = SIA.MealsSelection.public.data.paxList[paxId];
        var curSegment = pax.selectedMeals[segmentId];

        _.each(curSegment.selectedMeal, function(v, k, l){
            if(k !== mealServiceCode && v.mealCategoryCode === 'INF' && v.mealCode !== meal.attr('data-meal-mealcode')){
                          otherSelectedSM.push({'k': k, 'val': v });
                  }
        });

        return otherSelectedSM;
    };

    var selectSameMeal = function(meal) {
        // loop through each special meal in MealSelection module
        for (var i = 0; i < SIA.MealsSelection.public.infm.length; i++) {
            var infml = SIA.MealsSelection.public.infm[i];

            infml.p.accContent.find('.fm-select-btn').each(function(){
                var t = $(this);

                  // Unselect other selected special meals in same sector
                  if( t.attr('data-segment') === meal.attr('data-segment') ) {
                      unSelectMeal(t);
                  }

                  // Find the selected special meal in current segment and select it
                  if( t.attr('data-segment') === meal.attr('data-segment')
                  && t.attr('data-meal-mealcode') === meal.attr('data-meal-mealcode') ) {
                      selectMeal(t);

                      var parentEl = t.parent().parent();
                      var accordionContent = t.closest('.accordion__content');
                      var accordionTabMealTitle = accordionContent.prev().find('li:nth-child(3)');

                      // SIA.MealsSelection.setMainMenuLbl(t, t.attr('data-meal-menuname'));
                      accordionTabMealTitle.text(parentEl.find('.fm-inf-menu-title').text() + ' (Infant Meal)');
                      SIA.MealsSelection.updateData(t);
                  }
            });
        }
    };

    var selectMeal = function(meal){
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        // Add selected class to the input and to the container
        parentEl.addClass('selected');
        meal.addClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.removeClass('hidden');

        meal.attr('value', 'Remove');
    };
    var unSelectMeal = function(meal){
        var parentEl = meal.parent().parent();
        var badgeEl = $(parentEl).find('.fm-inf-menu-select-badge');

        // Add selected class to the input and to the container
        parentEl.removeClass('selected');
        meal.removeClass('selected');

        // remove hidden class to show the selected badge
        badgeEl.addClass('hidden');

        // reset prompt
        parentEl.removeClass('int-prompt');
        parentEl.find('.confirmation-prompt').addClass('hidden');

        meal.attr('value', 'Select');
    };

    var unselectChkBox = function(){
          p.infmlChkBox.prop("checked", false);
          p.infmlChkBox.parent().find('.not-eating-msg').addClass('hidden');
      };

    var destroy = function(){
        accContent.remove();
    };

    var oPublic = {
        init: init,
        p: p,
        destroy: destroy
    };

    return oPublic;
};

SIA.MoreTab = function(mainTabs, moreTabs) {
  var p = {};

  p.mainTabs = mainTabs;
  p.moreTabs = moreTabs;

  var init = function() {
    p.moreTabs.on('change', function(e) {
      e.preventDefault();

      var t = $(this);
      var tabIndex = t.val();
      var tabContentEl = p.moreTabs.parent().next();

      // hide all tabs
      $.each(tabContentEl.find('[data-tab-content]'), function(i, value) {
        $(value).addClass('hidden');
      });

      // show the current selected tab index content
      tabContentEl.find('[data-tab-content]').eq(tabIndex).removeClass('hidden');

      // swap the text from the other tab to the main tab
      var activeMainTab = p.moreTabs.parent().prev().find('.active .passenger-info__text');
      var moreTabs = p.moreTabs.find('[value=' + tabIndex + ']');
      var moreTabsTxt = moreTabs.html();
      var activeMainTxt = activeMainTab.html();

      activeMainTab.html(moreTabsTxt);
      moreTabs.html(activeMainTxt);

      // swap index number from the other tab to the main tab
      var mainTabActive = activeMainTab.closest('.active');
      var mainTabId = mainTabActive.attr('data-tab-menu');
      moreTabs.attr('value', mainTabId);
      mainTabActive.attr('data-tab-menu', tabIndex);

      // update custom scroll list
      var customScroll = p.moreTabs.next().attr('id');
      var csId = customScroll.split('-');
      var csEl = $('.custom-scroll').eq(parseInt(csId[1]));
      var csElAtt = '[data-value="' + tabIndex + '"]';

      csEl.find(csElAtt).html(activeMainTxt);
      csEl.find(csElAtt).attr('data-value', mainTabId);
    });
  };

  var oPublic = {
      init: init,
      p: p
  };

  return oPublic;
};

SIA.VerticalTab = function(tabControls, tabContent, tabSelect, totalTabs) {
    var p = {};
    p.tabControls = tabControls;
    p.tabContent = tabContent;
    p.tabSelect = tabSelect;
    p.totalTabs = totalTabs;

    var init = function(){
        p.tabControls.find('.tab-item').on({
            'click': function(e){
                e.preventDefault();
                var t = $(this);

                if(!t.hasClass('active')) {
                    var id = parseInt(t.data('tab-menu'));
                    showContent(id);

                    p.tabControls.find('.tab-item').removeClass('active');
                    t.addClass('active');

                    if(id < 3 && p.totalTabs > 4) {
                        p.tabSelect.find('options').removeAttr('selected');
                    }
                }

            }
        });

        if(p.totalTabs < 5) {
            p.tabSelect.parent().addClass('hidden');
        }

        arrangeTabs();
        resizeTabs();
    };

    var arrangeTabs = function() {
      if ($(window).width() <= 768 && totalTabs > 4) {
        tabControls.find('.tab-item:gt(2)').addClass('hidden');
      } else {
        tabControls.find('.tab-item').removeClass('hidden');
      }
    };

    var resizeTabs = function() {
      $(window).resize(function() {
        arrangeTabs();
      });
    };

    var showContent = function(id){
        p.tabContent.children('[data-tab-content]').addClass('hidden');
        p.tabContent.children('[data-tab-content="'+id+'"]').removeClass('hidden');
    };

    var oPublic = {
        init: init,
        p: p
    };

    return oPublic;
};

// check if underscore is loaded
var waitForUnderscore = function() {
   setTimeout(function() {
       if (typeof _ == 'undefined') {
           waitForUnderscore();
       } else {
            SIA.MealsSelection.init();
       }
   }, 100);
};

$(function() {
    typeof _ == 'undefined' ? waitForUnderscore() : SIA.MealsSelection.init();
});
