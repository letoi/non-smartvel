/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */
SIA.desEntry = function(){
	var btnSeeMore = $('.country-button [data-see-more]'),
		config = SIA.global.config,
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startDeskNum = 0,
		defaultDeskNum = 9,
		seeMoreDeskNum = 9,
		seeMoreCount = 0,
		isShuffleActive = false;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var initShuffle = function() {
		var countryItem = container.find('.static-item.col-mb-3').eq(0);
		container.shuffle({
			speed: 0,
			itemSelector: '.static-item',
			sizer: countryItem
		});
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region-categories');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var changeRegion = function(regionVal) {
		initCity(res.cities, (regionVal != 1 ? regionVal : null));
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg == region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region-categories') == region || region == "1") &&
					(!city || self.data('city').toLowerCase() == city.toLowerCase());
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);
		startDeskNum = 0;
		fillContent();
	};

	var generateTemplate = function(tpl) {
		if (!isShuffleActive) {
			initShuffle();
			isShuffleActive = true;
		}
		else {
			container.shuffle('appended', tpl);
			var searchKey = $("#search-city").val();
			if (!searchKey) {
				_.each(tpl, function(el) {
					el = $(el);
					if (!el.hasClass("col-mb-3")) {
						el.addClass("col-mb-3");
					}
				});
				var canLoadMore = container.find(".static-item:not(.shuffle-item)").length;
				if (!canLoadMore) {
					SIA.desEntryFilter.canLoadMoreFn(false);
				}
				SIA.desEntryFilter.arrangeTpl();
			} else {
				SIA.desEntryFilter.resetFilters();
			}
		}
		preventClick = false;
		changeSeemoreText();
	};

	var changeSeemoreText = function() {
		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.addClass('hidden');
			$('.country-button-border').addClass('hidden');
		}
		else {
			btnSeeMore.text(seeMoreCount === 2 ?
				L10n.kfSeemore.seeAll : L10n.kfSeemore.loadMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx) {
		var tpl = tpl = $(".static-item");
		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');
				if (!img.data('loaded')) {
					var newImg = new Image();

					newImg.onload = function() {
						self.removeClass('hidden');
						img.data('loaded', true);

						if (!tpl.filter('.hidden').length) {
							generateTemplate(tpl);
						}
						img.parent().css({
							'background-image': 'url(' + this.src + ')'
						});
					};

					newImg.src = img.data('img-src');
					img.attr('src', config.imgSrc.transparent);
				}
				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(tpl);
					}
				}
			});
		}
	};

	var fillContent = function() {
		if (staticItems.length) {
			var endIdx = 0;
			var resLen = staticItems.length;
			endIdx = !startDeskNum ?
				(seeMoreCount > 2 ? resLen : startDeskNum + defaultDeskNum) :
				(seeMoreCount > 2 ? resLen : startDeskNum + seeMoreDeskNum);
			startDeskNum = endIdx;
			getTemplate(staticItems, endIdx, resLen);
		}
	};

	var resetClass = function(elm, rClass, aClass) {
		elm
			.removeClass(rClass)
			.addClass(aClass);
	};

	var generateClass = function(res) {
		var i = -6, temp = 0;
		resetClass(res.removeAttr('style'), 'static-item--large col-mb-6', 'hidden col-mb-3');
		do {
			if (temp <= 2) {
				i += 6;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 3;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		SIA.desEntryFilter.resetFilters();
	});

	var canClick = function(status) {
		preventClick = false;
	};

	var initModule = function(){
		var public = {
			renderTemplate: renderTemplate,
			changeRegion: changeRegion,
			canClick: canClick
		};

		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);
		SIA.desEntryFilter.init(public);
	};

	initModule();
};

SIA.desEntryFilter = function() {
	var p, widthEl = 0, heightEl = 0, canLoadMore = true, desktopLimit = 9, desktopSeeMore = 9, step = 1;

	var addInspireMeListeners = function() {
		$(".all-checkbox").find("input").off().on("change", function() {
			var $this = $(this);
			var parentContainer = $this.closest(".custom--inspire");

			var isChecked = parentContainer.find("input").is(":checked");
			$this.closest(".custom--inspire")[isChecked ? "addClass" : "removeClass"]("cbox-checked");

			$(".inspire-filter").find("input").off();
			setTimeout(function() {
				addInspireFiltersListeners();
			}, 500);
			if (isChecked) toggleCheckbox(isChecked);
		});

		step = 1;
		addInspireFiltersListeners();
	};

	var addInspireFiltersListeners = function() {
		$(".inspire-filter").find("input").off().on("change", function(evt) {
			if ($(".all-checkbox").find("input").is(":checked")) {
				$(".all-checkbox").find("input").prop("checked", false);
				$(".all-checkbox").removeClass("cbox-checked");
			}
			var $this = $(this);
			var isChecked = $this.is(":checked");
			$this.closest(".custom--inspire")[isChecked ? "addClass" : "removeClass"]("cbox-checked");

			step = 1;
			filterTriggers();
			regionTriggers();
		});

		step = 1;
		filterTriggers();
		regionTriggers();
	};

	var addRegionListeners = function() {
		$(".region-radio-filter").find("input").off().on("click", function(evt) {
			removeRadioActiveClass();
			$(this).closest("li").addClass("rd-active");
			step = 1;
			filterTriggers();
			regionTriggers();
		});
		$($(".region-radio-filter").find("li")[0]).addClass("rd-active");
	};

	var removeRadioActiveClass = function() {
		$(".region-radio-filter").find("li.rd-active").each(function() {
			$(this).removeClass("rd-active");
		});
	};

	var regionTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var el = $(".region-radio-filter").find("input:checked");
		var filteredElements = getAllFilterBy([el.val()], "region-categories");
		var showElLimit = showLoadMore(filteredElements);
		if (showElLimit.hidden.length) {
			removeAllHidden($(".country-button"));
		} else {
			$(".country-button").addClass("hidden");
		}
		toggleElements(showElLimit.display);
		removeSearchText();
	};

	var removeSearchText = function() {
		$("#search-city").val("");
	};

	var showLoadMore = function(filteredElements) {
		var desktopLoad = 1, stepValue = 0;
		if (step == 1) {
			desktopLoad = desktopLimit;
			stepValue = desktopLoad * step;
		} else {
			desktopLoad = desktopSeeMore;
			stepValue = desktopLimit + (desktopLoad * (step - 1));
		}
		var displayEl = filteredElements.splice(0, stepValue);
		_.each(filteredElements, function(el) {
			el = $(el);
			el.addClass("hidden");
		});

		return {
			display: displayEl,
			hidden: filteredElements
		};
	};

	var removeAllHidden = function(el) {
		el.removeClass("hidden");
		el.find(".hidden").each(function() {
			var $this = $(this);
			$this.removeClass("hidden");
		});
	};

	var filterTriggers = function() {
		widthEl = $(".static-item:visible").width();
		heightEl = $(".static-item:visible").height();
		var allFilters = getAllClicked();
		var filteredElements;

		if (allFilters.length) {
			filteredElements = getAllFilterBy(allFilters, "inspire-categories");
			toggleElements(filteredElements);
		} else {
			filteredElements = getAllFilterBy(["1"], "inspire-categories");
			toggleElements(filteredElements);
		}
		p.canClick(false);
	};

	var toggleElements = function(elementList) {
		removeStatic();
		var step = 0;
		var stepValue = heightEl;
		for (var i = 0 ; i < elementList.length ; i += 3) {
			var firstEl = $(elementList[i]);
			var secondEl = $(elementList[i + 1]);
			var thirdEl = $(elementList[i + 2]);
			var topValue = (stepValue * step);
			if (firstEl) {
				firstEl.css("left", "0px");
				firstEl.css("top", topValue + "px");
				if (i == 0) {
					firstEl.addClass("static-item--large col-mb-6");
				}
				firstEl.removeClass("hidden");
				firstEl.css("transform", "");
				if (!firstEl.hasClass("col-mb-3")) {
					firstEl.addClass("col-mb-3");
				}
			} 
			if (secondEl) {
				secondEl.css("left", widthEl + "px");
				secondEl.css("top", topValue + "px");
				secondEl.removeClass("hidden");
				secondEl.css("transform", "");
				if (!secondEl.hasClass("col-mb-3")) {
					secondEl.addClass("col-mb-3");
				}
			}
			if (thirdEl) {
				thirdEl.css("left", (widthEl * 2) + "px");
				thirdEl.css("top", topValue + "px");
				thirdEl.removeClass("hidden");
				thirdEl.css("transform", "");
				if (!thirdEl.hasClass("col-mb-3")) {
					thirdEl.addClass("col-mb-3");
				}
			}
			step ++;
		}
		if (step == 0)   {
			step ++;
		}
		$(".static-block--item").css("height", (stepValue * step) + "px");
	};

	var removeStatic = function() {
		var staticEL = $(".static-block--item").find(".static-item--large");
		staticEL.removeClass("static-item--large col-mb-6");
		staticEL.addClass("hidden");
	}

	var getAllFilterBy = function(filters, dataAttr) {
		var regexString = _.map(filters, function(i) {
			return "(" + i + ")";
		}).join("|");
		var regex = new RegExp(regexString, "i");
		var allVisibleElements = $("[data-" + dataAttr + "]:visible");
		var hiddenElements = $("[data-" + dataAttr + "].hidden");
		if (dataAttr != "region-categories") {
			allVisibleElements = $.merge(allVisibleElements, hiddenElements);
		}
		var filteredElements = _.filter(allVisibleElements, function(el) {
			el = $(el);
			
			var containsValue = regex.test(el.data(dataAttr));
			if (containsValue || filters[0] == "1") {
				return el;
			} else {
				el.addClass("hidden");
			}
		});

		return filteredElements;
	};

	var getAllClicked = function() {
		var allCustomInspire = $(".custom--inspire");
		var clickedFilters = [];
		_.each(allCustomInspire, function(el, idx) {
			if (idx == 0) return;
			el = $(el);
			
			if (el.find("input").is(":checked")) {
				clickedFilters.push(el.data("custom-inspire"));
			}
		});

		return clickedFilters;
	};

	var toggleCheckbox = function(status) {
		var allCustomInspire = $(".custom--inspire");
		_.each(allCustomInspire, function(el, idx) {
			if (idx == 0) return;
			el = $(el);

			if (el.find("input").is(":checked") == status) {
				el[!status ? "addClass" : "removeClass"]("cbox-checked");
				el.find("label").trigger("click");
			}
		});
		$(allCustomInspire[0]).find("input").focus();
	};

	var arrangeTpl = function() {
		filterTriggers();
		regionTriggers();
	};

	var canLoadMoreFn = function(status) {
		canLoadMore = status;
	};

	var resetFilters = function() {
		$(".all-checkbox").find("input").prop("checked", true);
		$(".all-checkbox").addClass("cbox-checked");
		$(".custom--inspire:not(.all-checkbox)").each(function() {
			var $this = $(this);
			$(this).removeClass("cbox-checked");
			$this.find("input").prop("checked", false);
		});
		removeRadioActiveClass();
		$(".region-radio-filter").find("input[value='1']").prop("checked", true);
		$(".region-radio-filter").find("input[value='1']").closest("li").addClass("rd-active");
		displaySearchItem();
	};

	var displaySearchItem = function() {
		var city = $("#search-city").val();
		var showSearch = [];
		$(".static-item[data-city]").each(function() {
			var $this = $(this);
			if ($this.data("city").toLowerCase() == city.toLowerCase()) {
				showSearch.push($this);
			} else {
				$this.addClass("hidden");
			}
		});
		toggleElements(showSearch);
		$(".country-button").addClass("hidden");
	};

	var loadMoreButton = function() {
		$("[data-see-more]").off().on("click", function(evt) {
			evt.preventDefault();
			evt.stopPropagation();
			step ++;
			filterTriggers();
			regionTriggers();
		});
	};

	var init = function(public) {
		p = public;
		addInspireMeListeners();
		addRegionListeners();
		loadMoreButton();
		p.renderTemplate();
	};

	return {
		init: init,
		arrangeTpl: arrangeTpl,
		canLoadMoreFn: canLoadMoreFn,
		resetFilters: resetFilters,
		displaySearchItem: displaySearchItem
	}
}();
