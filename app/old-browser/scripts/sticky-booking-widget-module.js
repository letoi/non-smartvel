var SIA = SIA || {};

SIA.StickyBookingWidget = function () {
	var p = {};

	var init = function () {
        p.stickyWidget = $('[data-sticky-bookingwdiget]');
        p.stickyWidgetTarget = $('[data-sticky-bookingwdiget-target]');

        p.openWidgetBtn = p.stickyWidget.find('[data-sticky-bookingwdiget-open]');
        p.closeWidgetBtn = p.stickyWidget.find('[data-sticky-bookingwdiget-close]');
        p.bookingWidgetBlock = p.stickyWidget.find('.booking-widget-block');

        p.openWidgetBtn.off('click.openSticky').on('click.openSticky', function () {
            p.bookingWidgetBlock.slideDown('fast').removeClass('hidden');
            p.openWidgetBtn.addClass('hidden');
            p.closeWidgetBtn.removeClass('hidden');
            p.stickyWidget.focus();
        });
        p.closeWidgetBtn.off('click.closeSticky').on('click.closeSticky', function () {
            $.when(p.bookingWidgetBlock.slideUp('fast')).done(function () {
                p.openWidgetBtn.removeClass('hidden');
            });
            p.closeWidgetBtn.addClass('hidden');
        });

        initScrollListener();
    };
    
    var initScrollListener = function(){
        $(window).off('scroll.bookingwidget').on('scroll.bookingwidget', function () {
            p.elPos = p.stickyWidgetTarget.offset().top + p.stickyWidgetTarget.height() - 20
            if ($(window).scrollTop() >= p.elPos) {
                p.stickyWidget.addClass('sticky-bookingwidget--show');
            } else {
                p.stickyWidget.removeClass('sticky-bookingwidget--show');
            }
        });

        $(window).trigger('scroll.bookingwidget');
    };

	var pub = {
		init: init
	};

	return pub;
}();

SIA.StickyBookingWidget.init();