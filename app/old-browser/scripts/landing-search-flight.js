/**
 * @name SIA
 * @description Define global landingSearchFlight functions
 * @version 1.0
 */

SIA.landingSearchFlight = function() {
	var global = SIA.global;
	var body = $('body');
	var config = global.config;
	var searchBookFlightsForm = $('#form-book-travel');
	var searchRedeemFlightsForm = $('#form-book-travel-1');
	var searchFlightsForm = $('#form-book-travel-2');
	var searchByRouteForm = $('#form-flight-status');
	var searchByNumberForm = $('#form-flight-status-1');
	var mbBookingRefForm = $('#form-manage-booking');
	var mbETicketForm = $('#form-manage-booking-1');
	var checkInBookingRefForm = $('#form-check-in');
	var checkInETicketForm = $('#form-check-in-1');
	var landingScheduleForm = $('#form-landing-flight-schedules');

	var initPromo = function(){
		var popupPromoMember = $('.popup--promo-code-kf-member');
		var triggerProCode = $('[data-promo-code-popup]');
		var flyingFocus = $('#flying-focus');
		var popupPromo = global.vars.popupPromo;

		if(globalJson.loggedUser){
			popupPromo = popupPromoMember;
			global.vars.popupPromo = popupPromoMember;
		}

		popupPromo.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			triggerCloseModal: '.popup__close'
		});
		triggerProCode.off('click.showPromo').on('click.showPromo', function(e){
			e.preventDefault();
			popupPromo.Popup('show');
		});
	};

	var validateForms = function() {
		searchBookFlightsForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		searchRedeemFlightsForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		searchFlightsForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		searchByRouteForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		searchByNumberForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		mbBookingRefForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		mbETicketForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		checkInBookingRefForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		checkInETicketForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});

		landingScheduleForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: function(label, element) {
				global.vars.validateSuccess(label, element);
			}
		});
	};

	var tabOptionHandler = function() {
		var options = $('[data-option]');
		if (!options.length) {
			return;
		}

		var radioEls = options.find('input');
		var labelEls = options.find('label');

		var flightStatusTabHandler = function(checkedEl) {
			if (checkedEl.is('#depart-route-2')) {
				$('#selectDeparting').removeClass('hidden');
				$('#selectArriving').addClass('hidden');
			} else if (checkedEl.is('#arrive-route-2')) {
				$('#selectDeparting').addClass('hidden');
				$('#selectArriving').removeClass('hidden');
			}
		};

		var flightScheduleTabHandler = function(checkedEl) {
			if (checkedEl.is('#city-radio-4')) {
				$('#city-one-way-1').removeClass('hidden');
				$('#city-one-way-2').addClass('hidden');
			} else if (checkedEl.is('#city-radio-5')) {
				$('#city-one-way-1').addClass('hidden');
				$('#city-one-way-2').removeClass('hidden');
			}
		};

		var searchFlightsHandler = function(checkedEl){
			var currentForm = checkedEl.parents('form');
			var returnFlightEl = currentForm.find('[data-return-flight]');
			var oneWayFlightEl = returnFlightEl.next();

			if (checkedEl.is('#city-radio-5') || checkedEl.is('#city1-radio-5') || checkedEl.is('#city2-radio-5')) {
				returnFlightEl.addClass('hidden');
				oneWayFlightEl.removeClass('hidden');
			}
			if (checkedEl.is('#city-radio-4') || checkedEl.is('#city1-radio-4') || checkedEl.is('#city2-radio-4')) {
				returnFlightEl.removeClass('hidden');
				oneWayFlightEl.addClass('hidden');
			}
		};

		radioEls.off('change.flightStatus').on('change.flightStatus', function() {
			var checkedEl = $(this).parents('form').find('input[type="radio"]').filter(':checked');
			if (body.hasClass('landing-fl-status-page')) {
				flightStatusTabHandler(checkedEl);
			} else if (body.hasClass('landing-flight-schedules-page')) {
				flightScheduleTabHandler(checkedEl);
			} else if (body.hasClass('landing-search-flights-page')) {
				searchFlightsHandler(checkedEl);
			}
		});

		labelEls.off('click.flightStatus').on('click.flightStatus', function() {
			radioEls.trigger('change.flightStatus');
		});
	};

	var repositionLayout = function(){
		var loginBtn = $('[data-trigger-popup]');
		if(!loginBtn.length){
			return;
		}
		var langToolbar = $('.toolbar--language');
		var header = $('.header');
		var popup = $(loginBtn.data('trigger-popup'));
		popup.off('afterShow.landingPages').on('afterShow.landingPages', function(){
			var height = langToolbar.outerHeight() + header.outerHeight() + 6;
			body.css({ 'padding-top': height});
		}).off('afterHide.landingPages').on('afterHide.landingPages', function(){
			body.removeAttr('style');
		});
	};

	var focusElementForm = function() {
		var classCustomEl = $('[data-class]'),
			adultCustomEl = $('[data-adult]'),
			childCustomEl = $('[data-child]'),
			dataFocusExpand = $('[data-focus-expand]'),
			timeMinuteEl = $('.time-minute select'),
			timeSecondEl = $('.time-second select');

			$(classCustomEl).off('change.saveClassData').on('change.saveClassData',function(){
				var AdultInputEl = $(this).closest('form').find('[data-adult]'),
					timeOut = null;
				timeOut && clearTimeout(timeOut);
				if(AdultInputEl.length) {
				  timeOut = setTimeout(function(){
				    AdultInputEl.trigger('click');
				  }, 100);
				}
			});
			$(adultCustomEl).off('change.saveAdultData').on('change.saveAdultData',function(event, flag){
				var ChildInputEl = $(this).closest('form').find('[data-child]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(ChildInputEl.length) {
					  timeOut = setTimeout(function(){
					    ChildInputEl.trigger('click');
					  }, 100);
					}
			});
			$(childCustomEl).off('change.saveChildData').on('change.saveChildData',function(event, flag){
				var InfantInputEl = $(this).closest('form').find('[data-infant]'),
					timeOut = null;

					timeOut && clearTimeout(timeOut);
					if(InfantInputEl.length) {
					  timeOut = setTimeout(function(){
					    InfantInputEl.trigger('click');
					  }, 100);
					}
			});	
			$(dataFocusExpand).off('change.savedataFocusExpand').on('change.savedataFocusExpand',function(event, flag){
				if($('body').hasClass('landing-agoda-page')){
					var adultInputEl1 = $(this).closest('form').find('[data-adult]'),
						timeOut = null;

						timeOut && clearTimeout(timeOut);
						if(adultInputEl1.length) {
						  timeOut = setTimeout(function(){
						    adultInputEl1.trigger('click');
						  }, 100);
						}
					
				}
			});
			timeMinuteEl.each(function(){
				$(this).off('change.focusNext').on('change.focusNext',function(event, flag){
				  if($('body').hasClass('landing-rental-car-page')){
					  var SecondInputEl = $(this).closest('.grid-col').find('.time-second'),
						  timeOut = null;

					  timeOut && clearTimeout(timeOut);
					  if(SecondInputEl.length) {
					    timeOut = setTimeout(function(){
					      SecondInputEl.trigger('click');
					    }, 100);
					  }
				  }

				});
			});
			timeSecondEl.each(function(){
				$(this).off('change.focusNext').on('change.focusNext',function(event, flag){
				   if($('body').hasClass('landing-rental-car-page')){
					  var timeOut = null,
					  	  customEl = $(this).closest('[data-customselect]'),
					  	  driverAgeEl = $(this).closest('form').find('[data-checked-show="#drive-age-1"]');
					  timeOut && clearTimeout(timeOut);
					  $(this).closest('[data-customselect]').removeClass('focus');
					  if(customEl.is('.pickup-time')) {
					  	$('#car-dropoff').datepicker('show');
					  } else {
					  	if($('#drive-age-1 input').length && !driverAgeEl.is(':checked')) {
					  	  timeOut = setTimeout(function(){
					      	$('#drive-age-1 input').focus();
					      }, 100);
					  	}
					  }
					
				   }
				});
			});				
		};

	var init = function() {
		validateForms();
		initPromo();
		tabOptionHandler();
		repositionLayout();
		focusElementForm();
	};

	init();
};
