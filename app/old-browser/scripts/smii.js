SIA.FormValidator = function() {
	var p = {};

	p.errElClss = '.gigya-error-msg';

	var init = function() {

	};

	var attachEvnt = function(obj) {
		var evnts = ['keypress', 'blur', 'click', 'focus'];

		if (typeof obj != 'undefined' && typeof obj.el != 'undefined') {
			for (var i = 0, iLen = evnts.length; i < iLen; i++) {
				var evntType = evnts[i];
				var evntFunc = obj[evntType];

				if (typeof evntFunc != 'undefined') {
					obj.el.on(evntType, evntFunc);
				}
			}
		}
	};

	var validateEmail = function(val) {
		var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;

		return {
			state: emailReg.test(val),
			msg: 'Please enter a valid email address.'
		};
	};

	var validateEmpty = function(val) {
		return {
			state: (val != '') ? true : false,
			msg: 'This field is required'
		};
	};

	var isNotNumeric = function(val) {
		var value = val;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			var str = value[i];
			if (isNaN(str)) {
				return true;
			}
		}

		return false;
	};

	var isValRepetitive = function(val) {
		var value = val;
		var repetitiveTotal = parseInt(value[0]) * value.length;
		var valueTotal = 0;

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == repetitiveTotal) ? true : false;
	};

	var isValSequential = function(val) {
		var value = val;
		var sequentialTotal = 0;
		var valueTotal = 0;

		for (var j = parseInt(value[0]), jLen = parseInt(value[value.length - 1]); j <= jLen; j++) {
			sequentialTotal += j;
		}

		for (var i = 0, iLen = value.length; i < iLen; i++) {
			valueTotal += parseInt(value[i]);
		}

		return (valueTotal == sequentialTotal) ? true : false;
	};

	var computeBirthDate = function(val) {
		var date = parseDate(val);

		// first month starts at 0
		var birthDate = new Date(date.year, date.month - 1, date.day);
		var nowDate = new Date();
		var age =  nowDate.getFullYear() - birthDate.getFullYear();
		var month = nowDate.getMonth() - birthDate.getMonth();

		// compute the exact birthdate of the user
		if(month < 0 || (month === 0 && nowDate.getDate() < birthDate.getDate())) { age--; }

		return age;
	};

	var validatePassword = function(val) {
		if (val.length < 6) {
			return {
				state: false,
				msg: 'Please enter a 6 digit PIN.'
			};
		} else if (isValRepetitive(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that has different digits.'
			};
		} else if (isValSequential(val)) {
			return {
				state: false,
				msg: 'Please enter a PIN that is not formed by sequential digits.'
			};
		} else if (isNotNumeric(val)) {
			return {
				state: false,
				msg: 'Enter numbers only'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validatePassportExpDate = function(val, arrivalDate){
		var dateFormatValid = validateDateFormat(val);

		if (! dateFormatValid.state) {
			return dateFormatValid;
		} else {
			var date = parseDate(val);

			// convert passport exp. date to numerical
			var passportExpMonth = new Date(Date.parse(date.month +" 1, 2012")).getMonth() + 1;
			var nowDate = new Date();

			var month = arrivalDate - passportExpMonth;
			if ( (month > 0 && month < 6) || ( (month < 0 || month >= 6) && date.year < parseInt(nowDate.getFullYear())) ) {
				return {
					state: false,
					msg: 'Your passport must be valid for at least six months from your arrival date.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		}
	};

	var validateGuardianAge = function(val) {
		var guardianMinAge = 2;
		var guardianMaxAge = 12;
		var age = computeBirthDate(val);

		if (age < 2) {
			return {
				state: false,
				msg: 'A KrisFlyer member must be at least 2 years old.',
				needsGuardian: false
			};
		} else if (age >= guardianMinAge && age <= guardianMaxAge) {
			return {
				state: true,
				msg: '',
				needsGuardian: true
			};
		} else {
			return validateLegalAge(val);
		}
	};

	var validateLegalAge = function(val) {
		var dateFormat = validateDateFormat(val);

		if (dateFormat.state) {
			// compute the exact birthdate of the user
			var age = computeBirthDate(val);

			if (age < 16) {
				return {
					state: false,
					msg: 'You must be at least 16 years old to be a basic account holder.'
				};
			} else {
				return {
					state: true,
					msg: ''
				};
			}
		} else {
			return dateFormat;
		}
	};

	var parseDate = function(val) {
		var value = val;
		var fSeparator = value.indexOf('/');
		var lSeparator = value.lastIndexOf('/');

		return {
			day: value.slice(0, fSeparator).trim(),
			month: value.slice(fSeparator + 1, lSeparator).trim(),
			year: value.slice(lSeparator + 1).trim()
		};
	};

	var validateDateFormat = function(val) {
		var date = parseDate(val);

		if (date.day == '' && date.month == '' && date.year == '') {
			return {
				state: false,
				msg: 'This date is required'
			};
		} else if (parseInt(date.day) < 1 || parseInt(date.day) > 31) {
			return {
				state: false,
				msg: 'Please enter a valid day'
			};
		} else if (parseInt(date.month) < 0 || parseInt(date.month) == 0 || parseInt(date.month) > 12) {
			return {
				state: false,
				msg: 'Please enter a valid month'
			};
		} else if (isNaN(date.day) || isNaN(date.month) || isNaN(date.year)) {
			return {
				state: false,
				msg: 'Please enter digits from [0-9]'
			};
		} else if (date.year.length < 4) {
			 return {
				state: false,
				msg: 'Please enter a valid year'
			};
		} else {
			return {
				state: true,
				msg: ''
			};
		}
	};

	var validateAlphaSpecialChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaSpecialChars = /^[A-Za-z\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, and if required, any of these symbols /\'@()\”\-'
			};
		}
	};

	var validateAlphaNumSpecChars = function(el) {
		var t = el;
		var val = t.val();
		var alphaNumSpecialChars = /^[A-Za-z0-9\/\\'\@\(\)\- "]+$/;

		if (val.match(alphaNumSpecialChars)) {
			return {
				state: true,
				msg: ''
			};
		} else {
			return {
				state: false,
				msg: 'Enter letters of the English alphabet, the numbers 0 - 9, and if required, any of these symbols /\'@()”-'
			};
		}
	}

	var addErrState = function(el, inputclss, msgClss, errMsg) {
		el.addClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.addClass(msgClss);
		msgEl.html(errMsg);
	};

	var addValidState = function(el, inputclss, msgClss) {
		el.addClass(inputclss);
	};

	var removeInputState = function(el, inputclss, msgClss) {
		el.removeClass(inputclss);

		var msgEl = el.parent().find(p.errElClss);
		msgEl.removeClass(msgClss);
		msgEl.html('');
	};

	var inputValidator = function(el, result, errClss, validClss) {
		if (!result.state) {
			addErrState(el, errClss.input, errClss.msg, result.msg);
		} else {
			removeInputState(el, errClss.input, errClss.msg);
			addValidState(el, validClss);
		}
	};

	return {
		init: init,
		attachEvnt : attachEvnt,
		validateEmail : validateEmail,
		validateEmpty : validateEmpty,
		validatePassword : validatePassword,
		addErrState : addErrState,
		addValidState : addValidState,
		removeInputState : removeInputState,
		inputValidator : inputValidator,
		validateDateFormat: validateDateFormat,
		validateLegalAge: validateLegalAge,
		validateGuardianAge: validateGuardianAge,
		validateAlphaSpecialChars: validateAlphaSpecialChars,
		validateAlphaNumSpecChars: validateAlphaNumSpecChars,
		validatePassportExpDate: validatePassportExpDate,
		parseDate: parseDate
	};
}();

SIA.TogglePassword = function(appendEl, input) {
	var p = {};

	p.appendEl = appendEl;
	p.input = input;
	p.iconShow  = 'images/show-eye.png';
 	p.iconHide = 'images/hidden-eye.png';
 	p.icon = '<img data-togglePass class="pin-eye-icon" src="" width="auto" heigh="15">';

	var init = function() {
		p.appendEl.css('position', 'relative');

		var icon = $(p.icon);
		p.appendEl.append(icon);
		icon.attr('src', p.iconShow);

		// add default setting to hide password mask
		p.input.attr('type', 'password');

		evnt(p.appendEl.find('[data-togglePass]'), p.input);
	};

	var evnt = function(el, input) {
		el.on('click', function() {
			var inputType = input.attr('type');
			var iconEl = input.parent().find('[data-togglePass]');

			if (inputType == 'password') {
				input.attr('type', 'text');
				iconEl.addClass('show-pincode');
				iconEl.attr('src', p.iconHide);
			} else if (inputType == 'text') {
				input.attr('type', 'password');
				iconEl.removeClass('show-pincode');
				iconEl.attr('src', p.iconShow);
			}
		});
	};

	return { init: init };
};

SIA.RadioTab = function() {
    var p = {};

    p.gigyaEl = $('#ruLoginContainer');
    p.gigyaTabs = 'data-radiotab';
    p.gigyaContents = 'data-radiotab-content';

    p.ruErrorCont = $('#ruLoginError');

    var init = function() {
        // show default checked radio
        showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + p.gigyaEl.find('[checked="checked"]').attr(p.gigyaTabs) + '"]'));

        evnt(p.gigyaEl.find('[' + p.gigyaTabs + ']'));
    };

    var evnt = function(el) {
        el.on('click', function() {
            var t = $(this);

            // hide all contents
            p.gigyaEl.find('[' + p.gigyaContents + ']').addClass('hidden');

            // show current select tab content
            showContent(p.gigyaEl.find('[' + p.gigyaContents + '="' + t.attr(p.gigyaTabs) + '"]'));

            t.attr('data-radiotab') === '2' ? p.ruErrorCont.removeClass('hidden') : p.ruErrorCont.addClass('hidden')

        });
    };

    var showContent = function(el) {
        el.removeClass('hidden');
    };

    return {
        init: init,
        p: p
    };
}();

SIA.DatePicker = function(){
	var p = {};

	p.datePlaceholder = '<div data-datepicker style="position:absolute;top: 28px;left: 5px;height: 40px;width: 100%;">\
		<input type="text" placeholder="DD" data-day style="padding-left: 8px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="MM" data-month style="padding-left: 5px;background-color:transparent;width: 31px;border: none;text-align: left;height:100%;color: black;">\
		<span style="color: #999999;">/</span>\
		<input type="text" placeholder="YYYY *" data-year style="padding-left: 5px;background-color:transparent;width: 52px;border: none;text-align: left;height:100%;color: black;">\
	</div>';

	p.inputDay = '[data-day]';
	p.inputMonth = '[data-month]';
	p.inputYear = '[data-year]';

	var init = function(elParent, el) {
		p.dateParentEl = elParent;
		p.dateEl = el;

		customize(p.dateParentEl, p.dateEl);
		evnt(p.dateParentEl);
	};

	var customize = function(elParent, el) {
		elParent.css('position', 'relative');
		elParent.find(el).after($(p.datePlaceholder));
	};

	var transferVal = function() {
		var day = p.dateParentEl.find(p.inputDay).val();
		var month = p.dateParentEl.find(p.inputMonth).val();
		var year = p.dateParentEl.find(p.inputYear).val();
		var separator = '  /  ';

		p.dateEl.val(day + separator + month + separator + year);
	};

	var isInputEmpty = function(el) {
		return el.val().length == 0 ? true : false;
	};

	var removeErrorState = function(el) {
		var gigyaErrorMsgEl = el.find('.gigya-error-msg');
		gigyaErrorMsgEl.html('');
		gigyaErrorMsgEl.removeClass('gigya-error-msg-active');

		el.find('.gigya-error').removeClass('gigya-error');
	};

	var moveInputFocus = function(el) {
		el.on('keyup.moveInputFocus', p.inputDay + ', ' + p.inputMonth  + ', ' + p.inputYear, function() {
			var t = $(this);
			var nxtFocus = t.next().next();

			if ((t.val().length == 2) &&
				nxtFocus.length) {

				nxtFocus.focus();
				removeErrorState(p.dateParentEl);

				if (! isInputEmpty(nxtFocus)) {
					nxtFocus.select();
				}
			}
		});
	};

	var moveKeysFocus = function(el) {
		el.on('keydown', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function(event) {
			var t = $(this);
			var inputFocus = null;
			var inputVal = null;
			var backspaceKey = 8;
			var tabkey = 9;

			// tab key
			if (event.keyCode == tabkey) {
				inputFocus = t.next().next();

				p.dateParentEl.off('keyup.moveInputFocus');

				if (inputFocus.length) {
					event.preventDefault();

					if (isInputEmpty(inputFocus)) {
						inputFocus.focus();
						removeErrorState(p.dateParentEl);

						setTimeout(function() {
							removeErrorState(p.dateParentEl);
						}, 100);
					} else {
						inputFocus.select();
					}
				}
			} else {
				moveInputFocus(p.dateParentEl);
			}

			// backspace key
			if (event.keyCode == backspaceKey && t.val().length == 0) {
				t.prev().prev().focus()
				removeErrorState(p.dateParentEl);
			}
		});
	};

	var evnt = function(elParent) {
		elParent.on('blur', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			// transfer to actual input field
			transferVal();
		});

		elParent.on('click', p.inputDay + ', ' + p.inputMonth + ', ' + p.inputYear, function() {
			var t = $(this);

			// check if DD || MM || YYYY field is empty, then focus to DD field.
			var fieldParent = t.parent();
			var dayField = fieldParent.find(p.inputDay);
			if ( isInputEmpty(dayField) &&
				isInputEmpty(fieldParent.find(p.inputMonth)) &&
				isInputEmpty(fieldParent.find(p.inputYear)) ) {
					dayField.focus();

					// remove error msg / styles,
					// because it causes an on blur event when changing the focus to DD field
					removeErrorState(elParent);

					return;
			}
		});

		moveInputFocus(elParent);
		moveKeysFocus(elParent);
	};

	return {
		init: init
	};
};

SIA.RULogin = function() {
    var p = {};

    p.formValidator = SIA.FormValidator;

    p.gigyaErrClss = {
        input: 'gigya-error',
        msg: 'gigya-error-msg-active'
    };
    p.gigyaValidClss = 'gigya-valid';

    p.containerName = 'loginContainer';
    p.containerId = $('#' + p.containerName);

    var init = function() {
        //  find email field attached by gigya, save html and remove
        p.emailInput = p.containerId.find('.gigya-login-form .user-email-address input');

        // remove clear button
        p.emailInput.parent().find('.add-clear-text').remove();

         p.formValidator.attachEvnt({
            el: p.emailInput,
            keypress: function() {
                var t = $(this);

                p.formValidator.inputValidator(
                    t,
                    p.formValidator.validateEmail(t.val()),
                    p.gigyaErrClss,
                    p.gigyaValidClss);
            },
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmail(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
         });

        p.passInput = p.containerId.find('.gigya-login-form .user-password input');

     		// add max characters length
			p.passInput.attr('maxlength', '6');

        p.formValidator.attachEvnt({
            el: p.passInput,
            blur: function() {
                var t = $(this);

                if (t.val() == '') {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validateEmpty(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                } else {
                    p.formValidator.inputValidator(
                        t,
                        p.formValidator.validatePassword(t.val()),
                        p.gigyaErrClss,
                        p.gigyaValidClss);
                }
            }
        });

        p.ruLoginContainer = $('#ruLoginContainer');
    };

    var onError = function(e){
     	$('#gigya-login-screen .gigya-error-display').prepend('<em class="ico-close-round-fill"></em>');
    };

    var onFieldChanged = function(e){
    	if(e.field === 'loginID') {
    		p.ruLoginError.html('');
    	}
    };

    return {
        init: init,
        p: p,
        onError: onError,
        onFieldChanged: onFieldChanged
    };
}();

SIA.RURegistration = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function(containerID) {
		p.containerName = containerID;
		p.containerId = $('#' + p.containerName);

		p.registrationForm = p.containerId.find('.gigya-register-form');

		// attach show/hide mask password
	 	p.passInput = p.registrationForm.find('.gigya-input-password');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '6');

		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		validateEmptyFields();

		// firstname, lastname
		addMaxLen();

		// date
		p.dateBirthInput = p.registrationForm.find('[data-gigya-name="data.birthDate"]');

		setTimeout(function() {
			p.dateBirthInput.css('color', 'transparent');
		}, 1000);

		disableDate();

		// customize placeholder
		var dateBirth = new SIA.DatePicker();
		dateBirth.init(p.dateBirthInput.parent(), p.dateBirthInput);

		validateDateBirth();

		initTabs();
		disableFirstName();
	};

	var addMaxLen = function() {
		p.registrationForm.find('[data-gigya-name="profile.firstName"]').attr('maxlength', '50');
		p.registrationForm.find('[data-gigya-name="profile.lastName"]').attr('maxlength', '30');
	};

	var disableFirstName = function() {
		var firstName = p.registrationForm.find('[data-gigya-name="profile.firstName"]');
		var noFirstName = p.registrationForm.find('[data-gigya-name="data.noFirstName"]');
		var asteriskLabel = firstName.prev().find('label');

		noFirstName.on('click', function() {
			if (noFirstName.prop('checked')) {
				// disable
				firstName.attr('disabled', true);

				firstName.addClass('firstname-disabled');

				// remove evnt
				firstName.off();

				firstName.val('');

				setTimeout(function() {
					firstName.attr('placeholder', '');
				}, 100);

				// hide asterisk on label
				asteriskLabel.addClass('hidden-asterisk');

				// just in case there is an error msg, remove it
				var errorEl = firstName.parent().find('.gigya-error-msg');
				errorEl.removeClass('gigya-error-msg-active');
				errorEl.html('');
			} else {
				// enable
				firstName.attr('disabled', false);
				firstName.attr('placeholder', 'First Name *');

				firstName.removeClass('firstname-disabled');

				// show asterisk on label
				asteriskLabel.removeClass('hidden-asterisk');

				setTimeout(function() {
					asteriskLabel.removeClass('gigya-hidden');
				},100);

				// attach validator
				firstNameValidator();
			}
		});
	};

	var firstNameValidator = function() {
		p.formValidator.attachEvnt({
			el: p.registrationForm.find('[data-gigya-name="profile.firstName"]'),
			blur: function() {
			var t = $(this);

 			if(t.val() == '') {
 				p.formValidator.inputValidator(
					t,
					p.formValidator.validateEmpty(t.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
 				}
			}
		});
	};

	var initTabs = function(){
		p.tabContainer = $('#ruTabs');
		p.tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					p.tabContainer.find('.tab-item').removeClass('active');
					p.tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					p.tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	};

	// remove placeholder
	var disableDate = function() {
		p.dateBirthInput.attr('placeholder', '');
		p.dateBirthInput.attr('readonly', true);
		p.dateBirthInput.css('color', 'white');
	};

	// firstname, lastname, password, country & email
	var validateEmptyFields = function() {
		var inputs = [
			p.registrationForm.find('[data-gigya-name="profile.firstName"]'),
			p.registrationForm.find('[data-gigya-name="profile.lastName"]'),
			p.registrationForm.find('[data-gigya-name="email"]'),
			p.registrationForm.find('[data-gigya-name="profile.country"]'),
			p.passInput
		];

		for (var i = 0, iLen = inputs.length; i < iLen; i++) {
			p.formValidator.attachEvnt({
				el: inputs[i],
				blur: function() {
				var t = $(this);

	 			if(t.val() == '') {
	 				p.formValidator.inputValidator(
						t,
						p.formValidator.validateEmpty(t.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
	 				}
				}
			});
		}
	};

	// date
	var validateDateBirth = function() {
		var dateBirthFields = [
			p.registrationForm.find('[data-day]'),
			p.registrationForm.find('[data-month]'),
			p.registrationForm.find('[data-year]')
		];

		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					p.formValidator.inputValidator(
						p.dateBirthInput,
						p.formValidator.validateLegalAge(p.dateBirthInput.val()),
						p.gigyaErrClss,
						p.gigyaValidClss);
				}
			});

			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}
		}

		p.formValidator.attachEvnt({
			el: p.dateBirthInput,
			focus: function() {
				p.formValidator.inputValidator(
					p.dateBirthInput,
					p.formValidator.validateLegalAge(p.dateBirthInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});
	};

	return {
		init: init
	};
}();

SIA.RURegIntermediate = function() {
	var p = {};

	p.formValidator = SIA.FormValidator;

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var init = function () {

	};

	var onAfterScreenLoad = function() {
		p.container = $('#gigyaRegistrationIntermediate');

		// move container position to top
		p.container.closest('.ru-reg-intermediate .popup__inner').css('top', '89px');

		// email
		p.formValidator.attachEvnt({
         el: p.container.find('[data-gigya-name="profile.email"]'),
         keypress: function() {
       		var t = $(this);

          	p.formValidator.inputValidator(
           		t,
           		p.formValidator.validateEmail(t.val()),
           		p.gigyaErrClss,
              	p.gigyaValidClss);
         },
         blur: function() {
             var t = $(this);

             if (t.val() == '') {
                 	p.formValidator.inputValidator(
                     t,
                     p.formValidator.validateEmpty(t.val()),
                     p.gigyaErrClss,
                     p.gigyaValidClss);
             } else {
              	p.formValidator.inputValidator(
                  t,
                  p.formValidator.validateEmail(t.val()),
                  p.gigyaErrClss,
                  p.gigyaValidClss);
             }
         }
   	});

		// attach show/hide mask password
	 	p.passInput = p.container.find('[data-gigya-name="password"]');
		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);
		togglePass.init();

		// add max characters length
		p.passInput.attr('maxlength', '6');

		// password
		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});

		// add max characters length
		p.passInput.attr('maxlength', '6');
	};

	var onBeforeSubmit = function () {
		var passInput = $('.gigya-profile-form').find('[data-gigya-name="password"]');
		var passwordValid = p.formValidator.validatePassword($(passInput[2]).val());

		return passwordValid.state;
	};

	var onError = function() {
		var errorMsgEl = p.container.find('.gigya-error-display');
		p.container.find('.gigya-error-display').remove();

		errorMsgEl.prepend('<em class="ico-close-round-fill"></em>');
		p.container.find('.gigya-profile-form h2').after(errorMsgEl);

		p.container.find('[type="submit"]').on('click', function() {
			p.container.find('.gigya-error-display .ico-close-round-fill').remove();

			p.container.find('form').trigger('submit');
		});
	};

	return {
		init: init,
		onAfterScreenLoad: onAfterScreenLoad,
		onError: onError,
		onBeforeSubmit: onBeforeSubmit
	};
}();

SIA.ProfileUpdate = function() {
	var p = {};

	p.gigyaErrClss = {
		input: 'gigya-error',
		msg: 'gigya-error-msg-active'
	};
	p.gigyaValidClss = 'gigya-valid';

	p.formValidator = SIA.FormValidator;

	var afterScreenLoad = function() {
		p.container = $('#changePassword');

		p.passInput = p.container.find('[data-screenset-element-id="gigya-password-passwordRetype"]');

		var togglePass = new SIA.TogglePassword(
			p.passInput.parent(),
			p.passInput);

		togglePass.init();

		p.passInput.attr('maxlength', '6');

		p.formValidator.attachEvnt({
			el: p.passInput,
			blur: function() {
				p.formValidator.inputValidator(
					p.passInput,
					p.formValidator.validatePassword(p.passInput.val()),
					p.gigyaErrClss,
					p.gigyaValidClss);
			}
		});
	};

	var error = function() {
		var formError = p.container.find('.gigya-composite-control-form-error');
		formError.find('.ico-close-round-fill').remove();
		formError.prepend('<em class="ico-close-round-fill"></em>');
	};

	var beforeSubmit = function() {
		var passwordValid = p.formValidator.validatePassword($(p.passInput).val());

		return passwordValid.state;
	};

	return {
		afterScreenLoad : afterScreenLoad,
		error: error,
		beforeSubmit: beforeSubmit
	};
}();

SIA.JoinKrisFlyer = function() {
	var p = {};

	p.container = $('#joinKrisflyerTab');
	p.formValidator = SIA.FormValidator;
	p.gigyaErrClss = {
     	input: 'gigya-error',
      msg: 'gigya-error-msg-active'
   };
   p.gigyaValidClss = 'gigya-valid';

	var init = function() {
		p.firstName = p.container.find('#personal-infor-first-name');
		p.lastName = p.container.find('#lastName');
		p.preferredName = p.container.find('#preferredName');
		p.noFirstName = p.container.find('#personal-infor-first-name-cb');
		p.birthdate = p.container.find('#dateOfBirth');
		p.postCode = p.container.find('#postCode');
		p.promotionCode = p.container.find('#promotionCode');
		p.address = p.container.find('#addressLine');

		updateKrisFlyerCard([
				p.firstName,
				p.lastName
			],
			p.preferredName
		);

		noFirstName(p.noFirstName, p.firstName, p.lastName, p.preferredName);
		dateOfBirth(p.birthdate);

		alphaNumericInput([
			p.postCode,
			p.promotionCode
		]);

		alphaSpecialCharsInput();

		inputAddressAdder(p.address);

		setTimeout(function() {
			p.birthdate.parent().find('.add-clear-text').remove()
		}, 1000);
	};

	var updateKrisFlyerCard = function(input, preferredName) {
		for (var i = 0, Ilen = input.length; i < Ilen; i++) {
			$(input[i]).on('blur', function() {
				updatePreferredName($(input[0]).val(), $(input[1]).val(), preferredName);
			});
		}
	};

	var updatePreferredName = function(firstName, lastName, preferredName) {
		preferredName.val(lastName + ((firstName.length === 0) ? '' : (' ' + firstName)) );
	};

	var noFirstName = function(input, firstName, lastName, preferredName) {
		input.on('click', function() {
			// get first name label
			var firstNameLabel = firstName.closest('.grid-inner').find('[for="personal-infor-first-name"]');
			var val = firstNameLabel.html();
			var len =  val.length;

			if (input.prop('checked')) {
				// remove asterisk(*) as required
				firstNameLabel.html(val.slice(0, len - 1));

				firstName.attr('disabled', true);
				firstName.val('');
				firstName.parent().addClass('firstname-disabled');
				removeErrorInput(firstName);

				updatePreferredName('', lastName.val(), preferredName);
			} else {
				// add asterisk(*) to set required
				firstNameLabel.html(val + '*');

				firstName.attr('disabled', false);
				firstName.parent().removeClass('firstname-disabled');
			}
		});
	};

	var dateOfBirth = function(birthdateInput) {
		var dateParent = birthdateInput.parent();
		var dateBirth = new SIA.DatePicker();
		dateBirth.init(dateParent, birthdateInput);

		var dateBirthFields = [
			dateParent.find('[data-day]'),
			dateParent.find('[data-month]'),
			dateParent.find('[data-year]')
		];

		for (var j = 0, jLen = dateBirthFields.length; j < jLen; j++) {
			var el = dateBirthFields[j];
			p.formValidator.attachEvnt({
				el: el,
				blur: function() {
					var ageResult = p.formValidator.validateGuardianAge(birthdateInput.val());
					p.formValidator.inputValidator(
						birthdateInput,
						ageResult,
						p.gigyaErrClss,
						p.gigyaValidClss);

					showGuardian(ageResult);
					addErrClss(birthdateInput, ageResult);
				}
			});

			if (typeof $(el).attr('data-year') == 'undefined') {
				el.attr('maxlength', '2');
			} else {
				el.attr('maxlength', '4');
			}
		}

		p.formValidator.attachEvnt({
			el: birthdateInput,
			focus: function() {
				var ageResult = p.formValidator.validateGuardianAge(birthdateInput.val());
				p.formValidator.inputValidator(
					birthdateInput,
					ageResult,
					p.gigyaErrClss,
					p.gigyaValidClss);

				showGuardian(ageResult);
				addErrClss(birthdateInput, ageResult);
			}
		});

		birthdateInput.css('color', 'transparent');

		// adjust position top
		dateParent.find('[data-datepicker]').css('top', '0');
	};

	var addErrClss = function(input, age) {
		var input = input.parent();
		if (!age.state) {
			input.addClass('input-error');
		} else {
			input.removeClass('input-error');
		}
	};

	var showGuardian = function(val) {
		var guardianSection = p.container.find('.guardian-section');
		if (val.needsGuardian) {
			guardianSection.removeClass('hidden');
		} else {
			guardianSection.addClass('hidden');
		}
	};

	var generateList = function(from, to) {
		var list = [];
		for (var i = from; i <= to; i++) {
			list.push(i);
		}

		return list;
	};

 	var alphaNumericInput = function(inputs) {
		p.keyCodeList = $.merge($.merge(generateList(48, 57), generateList(65, 90)), generateList(97, 122));

		for (var i = 0, iLen = inputs.length; i < iLen; i++) {
			$(inputs[i]).on('keypress', function(e) {
				// english letters / numbers only
				// http://www.theasciicode.com.ar/ascii-printable-characters/capital-letter-z-uppercase-ascii-code-90.html
				if(p.keyCodeList.indexOf(e.which) <= -1) {
					return false;
				}
			});
		}
	};

	var alphaSpecialCharsInput = function() {
		p.firstName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.lastName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});

		p.preferredName.on('blur', function(){
			validateAlphaSpecialChars($(this));
		});
	};

	var validateAlphaSpecialChars = function(el) {
		var t = el;
		var val = t.val();

		var isEmpty = p.formValidator.validateEmpty(val);
		var validVal  = p.formValidator.validateAlphaSpecialChars(el);
		if (!isEmpty.state && t.attr('id') == 'preferredName') {
			return;
		}

		if (!isEmpty.state){
			addErrorInput(el, isEmpty.msg);
			return;
		}

		if(!validVal.state) {
			addErrorInput(el, validVal.msg);
			return;
		} else {
			removeErrorInput(el);
			return;
		}
	};

	var addErrorInput = function(el, msg) {
		el.parent().addClass('input-error');
		el.closest('.grid-inner').find('.gigya-error-msg').html(msg);
	};

	var removeErrorInput = function(el) {
		el.parent().removeClass('input-error');
		el.closest('.grid-inner').find('.gigya-error-msg').html('');
	};

	var inputAddressAdder = function(el) {
		var btn = el.find('#addAddressLine');

		btn.on('click', function(e) {
			e.preventDefault();

			var t = $(this);
			var max = parseInt(el.attr('data-addressline-max'));
			var currentAddressLine = el.find('[data-addressline-default]').length;

			if (currentAddressLine <= max) {
				var newIndex = currentAddressLine + 1;
				var addressLine = $(el.find('.additional-address-cont')[0]).clone();
				var addressLineEl = addressLine.find('input');

				var placeholder = addressLineEl.attr('placeholder');
				addressLineEl.attr('placeholder', placeholder.slice(0, -1) + newIndex);

				var idname = addressLineEl.attr('id');
				var newIdName = idname.slice(0, -1) + newIndex;
				addressLineEl.attr('id', newIdName);
				addressLineEl.attr('name', newIdName);

				var ariaLabel = addressLineEl.attr('aria-labelledby');
				var currentIndexPos = ariaLabel.indexOf('2');
				var newAriaLabel = ariaLabel.slice(0, currentIndexPos) + newIndex + ariaLabel.slice(currentIndexPos + 1);
				addressLineEl.attr('aria-labelledby', newAriaLabel);

				addressLineEl.val('');

				el.find('.additonal-address-line').before(addressLine);

				if (newIndex == max) {
					btn.closest('.additonal-address-line').addClass('hidden');
				}
			}
		});
	};

	return {
		init: init
	};
}();

// SIA.DataProfileEdit = function() {
// 	var p = {};

// 	p.gigyaErrClss = {
// 		input: 'gigya-error',
// 		msg: 'gigya-error-msg-active'
// 	};
// 	p.gigyaValidClss = 'gigya-valid';

// 	p.formValidator = SIA.FormValidator;
// 	var init = function(){
// 		p.containerId = $('#profileEditContainer');
// 		p.inputs = []
// 		p.formContainer = p.containerId.find(".gigya-profile-form");
// 		p.inputs.push({el: p.formContainer.find('[name="profile.firstName"]'), errMsg:"Enter first/given name"});
// 		p.inputs.push({el: p.formContainer.find('[name="profile.lastName"]'), errMsg:"Enter last/family name"});
// 		p.inputs.push({el: p.formContainer.find('[name="data.birthDate"]'), errMsg:"Enter date of birth"});
		
// 		// console.log(p.inputs);
		
// 		p.saveBtn = p.formContainer.find("input.gigya-input-submit");

// 		p.formValidator.attachEvnt({
// 			el: p.saveBtn,
// 			click: function(evt){
// 				evt.preventDefault();
// 				_.each(p.inputs, function(inputData){
// 					// console.log("hello world");
// 					if(inputData.el.val() == '') {
// 						p.formValidator.inputValidator(
// 							inputData.el,
// 							emptyInputValidator(inputData),
// 							p.gigyaErrClss,
// 							p.gigyaValidClss
// 						);
// 					}
// 				});
// 			}
// 		});
		
		
// 		_.each(p.inputs,function(inputData){
// 			// console.log(inputData);
// 			p.formValidator.attachEvnt({
// 				el: inputData.el,
// 				blur: function(){
// 					eventFunction($(this),inputData)
// 				}
// 			});
// 		});
// 	}

// 	var eventFunction = function(el,inputData){
// 		var element = el;

// 		if(element.val() == '') {
// 			p.formValidator.inputValidator(
// 				element,
// 				emptyInputValidator(inputData),
// 				p.gigyaErrClss,
// 				p.gigyaValidClss);
// 			}
// 	}

// 	var emptyInputValidator = function(inputData){
// 		validatorResult = p.formValidator.validateEmpty(inputData.el.val());
// 		validatorResult.msg = inputData.errMsg;
// 		return validatorResult;
// 	}
	
	
// 	return {
// 		init: init
// 	}
// }();

$(function() {
	var body = $('body');

	if (body.hasClass('gigya-login')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'loginContainer',
	        startScreen: "gigya-login-screen",
	        onAfterScreenLoad: SIA.RULogin.init,
	        onError: SIA.RULogin.onError
	    });

	    SIA.RadioTab.init();
	}

	if (body.hasClass('gigya-registration')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTab',
	  		startScreen: "gigya-register-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTab');

	  			SIA.JoinKrisFlyer.init();
	  		}
	   });
	}
	
	if (body.hasClass('gigya-reg-intermediate')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'gigyaRegistrationIntermediate',
	  		startScreen: "gigya-register-intermediate-screen",
	  		width: "100%",
	  		onAfterScreenLoad: SIA.RURegIntermediate.onAfterScreenLoad,
  			onError: SIA.RURegIntermediate.onError,
  			onBeforeSubmit: SIA.RURegIntermediate.onBeforeSubmit
	   });
	}

	if (body.hasClass('gigya-reg-completion')) {
		gigya.accounts.showScreenSet({
	  		screenSet:'RU-RegistrationLogin',
	  		containerID: 'createAccountTabCompletion',
	  		startScreen: "gigya-complete-registration-screen",
	  		width: "100%",
	  		onAfterScreenLoad: function() {
	  			SIA.RURegistration.init('createAccountTabCompletion');

	  			SIA.JoinKrisFlyer.init();
	  		}
	   });
	}

	if (body.hasClass('profile-update')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-ProfileUpdate',
	        containerID: 'changePassword',
	        startScreen: "gigya-change-password-screen",
	        onAfterScreenLoad: SIA.ProfileUpdate.afterScreenLoad,
	        onError: SIA.ProfileUpdate.error,
	        onBeforeSubmit: SIA.ProfileUpdate.beforeSubmit
	    });
	}

	if (body.hasClass('password-change-success')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-ProfileUpdate',
	        containerID: 'resetPasswordSuccess',
	        startScreen: "Success"
	    });
	}

	if (body.hasClass('link-accounts')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-LinkAccounts',
	        containerID: 'forgotPasswordContainer',
	        startScreen: "gigya-forgot-password-screen"
	    });
	}

	if (body.hasClass('link-accounts')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-LinkAccounts',
	        containerID: 'forgotPasswordSuccessContainer',
	        startScreen: "gigya-forgot-password-success-screen"
	    });
	}

	if (body.hasClass('login-social-media')) {
		gigya.accounts.showScreenSet({
        	screenSet: 'RU-LinkAccounts',
        	containerID: 'loginSocialMedia',
        	startScreen: "gigya-link-account-screen",
        	onAfterScreenLoad: function() {
				if ($('#loginSocialMedia').attr('data-modal-onload') === "true") {
					$('.ru-login').Popup('show');
				}
        	}
    	});
	}

	if (body.hasClass('gigya-login')) {
	    gigya.accounts.showScreenSet({
	        screenSet: 'RU-RegistrationLogin',
	        containerID: 'verificationPending',
	        startScreen: "gigya-verification-pending-screen",
	        onAfterScreenLoad: function() {
				if ($('#verificationPending').attr('data-modal-onload') === "true") {
					$('.ru-login').Popup('show');
				}
        	}
	    });
	}


	var tabContainer = $('#ruTabs');
	if(tabContainer.length) {
		tabContainer.find('.tab-item a').each(function(){
			var t = $(this);
			t.on({
				'click': function(e){
					e.preventDefault();
					var tab = $(this);
					var target = tab.attr('data-target');

					tabContainer.find('.tab-item').removeClass('active');
					tabContainer.find('.tab-content').removeClass('active');

					tab.parent().addClass('active');
					tabContainer.find('[data-tab-content="'+target+'"]').addClass('active');
				}
			});
		});
	}

	if($('[ data-profile-edit="true"]').length) {
		// gigya.accounts.showScreenSet({
	    //     screenSet: 'RU-RegistrationLogin',
	    //     containerID: 'profileEditContainer',
	    //     startScreen: "gigya-profile-edit-screen",
	    //     onAfterScreenLoad:SIA.DataProfileEdit.init
	    // });
	}
});
