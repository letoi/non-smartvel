/**
 * @name SIA
 * @description Define global autocomplete search functions
 * @version 1.0
 */
SIA.autocompleteSearch = function(){
	var vars = SIA.global.vars;
	var win = vars.win;
	var doc = vars.doc;
	var searchEl = $('[data-autocomplete-search]');
	var term = '';
	var timerResize = null;
	var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var timerAutocompleteLangOpen = null;

	// Init autocomplele search
	var initAutocompleteSearch = function() {
		searchEl.each(function(index, el) {
			var field = $(el);
			var sourceUrl = field.data('autocomplete-source') || 'ajax/autocomplete-search.json';
			var searchAutoComplete = field.autocomplete({
				open: function() {
					var self = $(this);
					self.autocomplete('widget').hide();
					clearTimeout(timerAutocompleteLangOpen);
					timerAutocompleteLangOpen = setTimeout(function(){
						self.autocomplete('widget').show().css({
							'left': self.parent().offset().left,
							'top': self.parent().offset().top + self.parent().outerHeight(true)
						});
						self.autocomplete('widget')
							.jScrollPane({
								scrollPagePercent: 10
							}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
								e.preventDefault();
								e.stopPropagation();
							});
					}, 100);
				},
				source: function(request, response) {
					term = request.term;
					$.get(sourceUrl + '?term=' + term, function(data) {
						response(data.searchSuggests);
					});
				},
				search: function() {
					var self = $(this);
					self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				},
				close: function() {
					var self = $(this);
					self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				},
				select: function(evt, ui) {
					$(this).val(ui.item.value).closest('form').trigger('submit');
				}
			}).data('ui-autocomplete');

			searchAutoComplete._renderItem = function(ul, item) {
				if (!ul.hasClass('auto-suggest')) {
					ul.addClass('auto-suggest');
				}
				return $('<li class="autocomplete-item" data-value="' + (item.value ? item.value : item.label) + '"><a href="javascript:void(0);">' + (item.label ? item.label : '') + '</a></li>')
					.appendTo(ul);
			};

			searchAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(field.parent().outerWidth(true));
			};

			searchAutoComplete._move = function( direction ) {
				var item, previousItem,
					last = false,
					li = $(),
					minus = null,
					api = this.menu.element.data('jsp');

				if (!api) {
					if (!this.menu.element.is(':visible')) {
						this.search(null, event);
						return;
					}
					if (this.menu.isFirstItem() && /^previous/.test(direction) ||
							this.menu.isLastItem() && /^next/.test(direction) ) {
						this._value( this.term );
						this.menu.blur();
						return;
					}
					this.menu[direction](event);
				}
				else {
					var currentPosition = api.getContentPositionY();
					switch(direction){
						case 'next':
							if(this.element.val() === ''){
								api.scrollToY(0);
								li = this.menu.element.find('li:first');
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.next();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
							}
							if(!item){
								last = true;
								li = this.menu.element.find('li').removeClass('active').first();
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(0);
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								minus = li.position().top + li.innerHeight();
								if(minus - this.menu.element.height() > currentPosition){
									api.scrollToY(Math.max(0, minus - this.menu.element.height()));
								}
							}
							break;
						case 'previous':
							if(this.element.val() === ''){
								last = true;
								item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.prev();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
							}
							if(!item){
								last = true;
								item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(this.menu.element.find('.jspPane').height());
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								if(li.position().top <= currentPosition){
									api.scrollToY(li.position().top);
								}
							}
							break;
					}
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');

			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				// var self = $(this);

				doc.off('mousedown.hideAutocompleteSearch').on('mousedown.hideAutocompleteSearch', function(e){
					if(!$(e.target).closest('.ui-autocomplete').length){
						field.autocomplete('close');
					}
				});
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll').on('scroll.preventScroll mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});

			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});

			// field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
			// 	if(e.which === 13){
			// 		e.preventDefault();
			// 		if(field.autocomplete('widget').find('li').length === 1){
			// 			field.autocomplete('widget').find('li').trigger('click');
			// 			return;
			// 		}
			// 		field.autocomplete('widget').find('li.active').trigger('click');
			// 	}
			// });
		});
	};

	win.off('resize.blurSearch').on('resize.blurSearch', function() {
		clearTimeout(timerResize);
		timerResize = setTimeout(function() {
			var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (winW !== width) {
				winW = width;
				var currentEl = searchEl.not(':hidden');
				if (currentEl.length) {
					var currentMenu = currentEl.autocomplete('widget');
					if (currentMenu.length && currentMenu.is(':visible')) {
						currentEl.autocomplete('close');
					}
				}
			}
		}, 50);
	});

	var initModule = function() {
		initAutocompleteSearch();
	};

	initModule();
};
