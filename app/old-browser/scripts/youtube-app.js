/**
 * @name SIA
 * @description Define global youtubeApp functions
 * @version 1.0
 */
SIA.youtubeApp = function(){
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var flyingFocus = $();
	var dataVideo = globalJson.dataVideo;

	// video template for ie7
	var templateIE7 = '<iframe width="{2}" height="{1}" src="https://www.youtube.com/embed/{0}?autoplay={3}" frameborder="0" allowfullscreen></iframe>';

	// if(dataVideo){
	// 	dataVideo = {
	// 		url: ['NUOcs1nZ35s', 'asPKzZ7X79w'],
	// 		image: [{
	// 			url: 'images/img-1.jpg',
	// 			description: 'Lorem ipsum dolor sit dipiscing elit.'
	// 		},{
	// 			url:'images/img-2.jpg',
	// 			description: 'Curabitur congue tortor vitae era.'
	// 		}]
	// 	};
	// }

	// init youtube function
	var initYoutube = function(){
		var player;
		var url = dataVideo.url[0];

		// apply youtube video
		var applyYoutube = function(temp){
			var videoCollect = $('[data-youtube-url]');

			var videoLightbox = $(temp).appendTo(body);
			var thumbnail = videoLightbox.find('.info-watch');

			// close youtube video
			var closeYoutubeVideo = function(videoBlock, callback){
				videoBlock.html('<div id="youtube-player"></div>');
				if(callback){
					callback();
				}
			};

			// init video popup
			videoLightbox.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',
				afterShow: function(){ // after showing popup
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				},
				afterHide: function(popup){ // after hiding popup
					player = null;
					closeYoutubeVideo($(popup).find('.wrap-video'));
				}
			});

			// init youtube api plugin or load video url if already inited
			thumbnail.each(function(idx){
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e){
					e.preventDefault();
					url = dataVideo.url[idx];
					if(!player){
						window.onYouTubeIframeAPIReady();
					}
					else{
						if(global.vars.isIE() && global.vars.isIE() > 7){
							player.loadVideoById(url);
						}
						else {
							player = $('#youtube-player').html(templateIE7.format(url, 390, 640, 1));
						}
					}
				});
			});

			// show popup video
			videoCollect.each(function(){
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e){
					e.preventDefault();
					url = dataVideo.url[self.data('youtube-url')];
					if(!player){
						window.onYouTubeIframeAPIReady();
					}
					// else{
					// 	player.loadVideoById(url);
					// }
					videoLightbox.Popup('show');
				});
			});

			window.onYouTubeIframeAPIReady = function () {
				if(global.vars.isIE() && global.vars.isIE() > 7){
					player = new window.YT.Player('youtube-player', {
						height: 390,
						width: 640,
						videoId: url//,
						// playerVars: {
					//     listType:'playlist',
					//     list: 'UUPW9TMt0le6orPKdDwLR93w'
					//   },
						// events: {
							// 'onReady': onPlayerReady,
							// 'onStateChange': onPlayerStateChange
							// 'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
							// 'onError': onPlayerError
						// }
					});
				}
				else{
					player = $('#youtube-player').html(templateIE7.format(url, 390, 640, 0));
				}
				// var done = false;
			};
		};

		// draw template
		$.get(config.url.videoLightbox.youtube, function (temp) {
			var template = window._.template(temp, {
				'data': dataVideo
			});
			applyYoutube(template);
		}, 'html');

	};
	var initModule = function(){
		initYoutube();
	};
	initModule();
};
