

SIA.MpIceCheckIn = function() {
    var templateData = {
        bookReference: "",
        departureCity: "",
        arrivalCity: "",
        flightSegments: [],
        flightCabinClass: "",
        delayedFlightNotice: ""
    };
    var days = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
    var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    var mainContainer = '<div class="main-container-old">\
        <h2 class="heading-1">Check-in</h2>\
        <div class="flight-alert <%- (!delayedFlightNotice) ? "hidden" : "" %>">\
            <em class="ico-alert"></em>\
            <p class="text"><%- delayedFlightNotice %></p>\
        </div>\
        <div class="booking-reference">\
            <span class="booking-text">Booking reference</span>\
            <span class="booking-number"><%- bookReference %></span>\
        </div>\
        <div class="flight-details-wrapper">\
            <div class="flight-header">\
                <p class="flight-sector">1. <%- departureCity %> to <%- arrivalCity %></p>\
                <span class="flight-type"><%- flightCabinClass.toUpperCase() %> (N)\
                <em class="ico-info-round-fill"></em>\
                </span>\
            </div>\
            <div class="flight-content">\
            </div>\
        </div>\
    </div>';
    var flightSegmentLeg = '<div><% _.each(flightSegments, function(flights, idx) { %>\
        <div class="flight-station--inner <%- (idx > 0) ? "flight-wrap-info" : "" %>">\
            <div class="flight-info-wrap">\
                <div class="flight-station-info">\
                    <div class="station-stop" data-timeflight="08h 10m">\
                        <span class="station-stop-detail">\
                            <em class="ico-airplane-2"></em>\
                            <span class="time"><%- flights.travelTime %></span>\
                        </span>\
                    </div>\
                    <div class="flights-station__info--detail">\
                        <span class="hour"><%- flights.origin.airportCode %> <%- timeParser(flights.scheduledDepartureDateTime) %></span>\
                        <span class="country-name"><%- flights.origin.cityName %></span>\
                        <span class="date">\
                            <span class="flight-date"><%- dateParser(flights.scheduledDepartureDateTime) %></span>\
                            <br> <%- flights.origin.airportName %> Terminal <%- flights.origin.terminalNumber %> </span>\
                    </div>\
                    <div class="flights-station__info--detail return-flight">\
                        <span class="hour"><%- flights.destination.airportCode %> <%- timeParser(flights.scheduledArrivalDateTime) %></span>\
                        <span class="country-name"><%- flights.destination.cityName %></span>\
                        <span class="date">\
                            <span class="flight-date"><%- dateParser(flights.scheduledArrivalDateTime) %></span>\
                            <br> <%- flights.destination.airportName %> Terminal <%- flights.destination.terminalNumber %>\
                        </span>\
                    </div>\
                </div>\
                <div class="airline-info multistop-arlineinfo">\
                    <div class="inner-info multistops-airline-info">\
                        <span class="airline-detail">\
                            <img src="images/svg/sq.png" alt="sq Logo" longdesc="img-desc.html">\
                            <strong><%- flights.operatingAirline.airlineName %>\
                                <span> •</span>\
                            </strong>\
                            <%- flights.operatingAirline.airlineCode %> <%- flights.operatingAirline.flightNumber %>\
                        </span>\
                        <span class="name-plane"><%- flights.aircraftType %></span>\
                        <span class="cabin-color economy"><%- flights.cabinClass %></span>\
                        <span data-col-cabinclass="Premium Economy" class="cabin-color hidden"></span>\
                    </div>\
                </div>\
            </div>\
            <div class="booking-passenger">\
                <a href="#" class="accordion__control booking-passenger__control <%- (idx == 0) ? "active" : "" %>">Select passengers\
                    <em class="ico-point-d">\
                        <span class="ui-helper-hidden-accessible"></span>\
                    </em>\
                </a>\
                <div class="booking-passenger-content">\
                    <div class="booking-passenger-heading">\
                        <div class="custom-checkbox custom-checkbox--1">\
                            <input class="select-all" name="booking-checkbox-all-<%- (idx + 1) %>" id="booking-checkbox-all-<%- (idx + 1) %>" aria-labelledby="booking-checkbox-all-<%- (idx + 1) %>-error" type="checkbox">\
                            <label for="booking-checkbox-all-<%- (idx + 1) %>">Select all</label>\
                        </div>\
                    </div>\
                    <ul class="booking-passenger-list">\
                        <%_.each(flights.passengers, function(passenger, passengerIndex) {%>\
                        <li>\
                            <div class="custom-checkbox custom-checkbox--1">\
                                <input class="passenger-checkbox" name="booking-checkbox-<%- (idx + 1) %>-<%- (passengerIndex + 1) %>" id="booking-checkbox-<%- (idx + 1) %>-<%- (passengerIndex + 1) %>" aria-labelledby="booking-checkbox-<%- (idx + 1) %>-<%- (passengerIndex + 1 ) %>-error" type="checkbox">\
                                <label for="booking-checkbox-<%- (idx + 1) %>-<%- (passengerIndex + 1) %>">\
                                    <span>\
                                        <%- passenger.firstName %> <%- passenger.lastName %>\
                                    </span>\
                                    <% if (passenger.infant) { %>\
                                        <span class="pax-infant-name">\
                                            <%- passenger.infant.firstName %> <%- passenger.infant.lastName %>\
                                        </span>\
                                    <% } %>\
                                </label>\
                            </div>\
                        </li>\
                        <% }) %>\
                    </ul>\
                </div>\
            </div>\
        </div>\
    <% }) %></div>';

    var init = function() {
        $("ice-checkin").remove();
        onMount();
    };

    var onMount = function() {
        $.get("ajax/JSONS/ICE/ICE_PaxDetailsJson.json", function(response) {
            var data = response.paxRecordVO;
            var flights = data.flights;
            templateData.bookReference = data.recordLocator;
            templateData.departureCity = flights[0].origin.cityName;
            templateData.arrivalCity = flights[flights.length - 1].destination.cityName;
            templateData.flightSegments = _.map(flights, function(segment) {
                segment.passengers = deepClone(data.passengers);
                return segment;
            });
            templateData.flightCabinClass = (flights[0] || {}).cabinClass;
            templateData.delayedFlightNotice = (flights[0] || {}).delayedFlightNotice;
            
            renderMainContainer();
            renderFlightComponent();
            toggleAccordion();
            selectAllCheckBox();
            passengerCheckBox();
            setAllCheckBoxesStatus();
            setTimeout(function() {
                $("[v-cloak]").removeAttr("v-cloak");
            }, 600);
        });
    };

    var renderMainContainer = function() {
        var tpl = $(_.template(mainContainer, templateData));
        appendTo(".main-inner", tpl);
    };

    var renderFlightComponent = function() {
        var tplData = deepClone(templateData);
        tplData.dateParser = dateParser;
        tplData.timeParser = timeParser;
        var tpl = $(_.template(flightSegmentLeg, tplData));
        appendTo(".flight-content", tpl);
    };

    var toggleAccordion = function() {
        $(".accordion__control")
            .off()
            .on("click", function(e) {
                e.preventDefault();
                var $this = $(this);
                var toggleValue = $this.hasClass("active");
                var passengerList = $this.closest(".booking-passenger").find(".booking-passenger-content");
                passengerList.slideToggle(500);
                $this[(!toggleValue) ? "addClass" : "removeClass"]("active");
            }).each(function() {
                var $this = $(this);
                var toggleValue = $this.hasClass("active");
                var passengerList = $this.closest(".booking-passenger").find(".booking-passenger-content");
                passengerList[toggleValue ? "slideDown" : "slideUp"](500);
            });
    };

    var selectAllCheckBox = function() {
        $(".select-all")
            .off()
            .on("change", _.debounce(function() {
                var $this = $(this);
                var allCheckboxes = $this.closest(".booking-passenger").find("ul").find("[type='checkbox']:not([disabled])");
                allCheckboxes.each(function() {
                    $(this).prop("checked", $this.is(":checked"));
                });
                setAllCheckBoxesStatus();
                setTimeout(function() {
                    checkIfSelectedAll();
                }, 100);
            }, 100));
    };

    var passengerCheckBox = function() {
        $(".passenger-checkbox")
            .off()
            .on("change", _.debounce(function() {
                setAllCheckBoxesStatus();
                setTimeout(function() {
                    checkIfSelectedAll();
                }, 100);
            }, 100));
    };

    var checkIfSelectedAll =  function() {
        $(".select-all").each(function() {
            var $this = $(this);
            var allCheckBoxes = $this.closest(".booking-passenger").find("ul").find("[type='checkbox']:not([disabled])");
            var isSelectedAll = getAllCheckBoxStatus(allCheckBoxes, "and");
            $this.prop("checked", isSelectedAll);
        });
    };

    var getAllCheckBoxStatus = function(checkboxes, operator) {
        if (!checkboxes.length) {
            return false;
        }

        return _.chain(checkboxes)
            .map(function(each) {
                each = $(each);
                return each.is(":checked");
            })
            .reduce(function(memory, current) {
                if (operator == "and") {
                    return memory && current;
                }
                return memory || current;
            }, (operator ? true : false))
            .value();
    };

    var setAllCheckBoxesStatus = function() {
        var canContinue = true;
        var list = $(".booking-passenger-content");
        for (var idx = 0; idx < list.length; idx ++) {
            passenger = $(list[idx]);
            if (idx > 0) {
                var lastLeg = $(list[idx - 1]);
                var checkBoxSet = _.map(lastLeg.find(".custom-checkbox"), function(el) {
                    var $this = $(el);
                    return $this.find("[type='checkbox']").is(":checked");
                });

                _.each(passenger.find(".custom-checkbox"), function(el, idx) {
                    var $this = $(el);
                    if (idx == 0) {
                        var canSelectAll = (checkBoxSet.indexOf(true) != -1) ? true : false;
                        toggleCheckBox(canSelectAll, $this);
                    } else {
                        toggleCheckBox(checkBoxSet[idx], $this);
                    }
                });
            }
        }
    };

    var toggleCheckBox = function(state, $this) {
        $this[state ? "removeClass" : "addClass"]("disabled");
        if (state) {
            $this.find("[type='checkbox']").removeAttr("disabled");
        } else {
            $this.find("[type='checkbox']").prop("checked", false);
            $this.find("[type='checkbox']").attr("disabled", "true");
        }
    };

    var dateParser = function(date) {
        var dateTime = dateTimeSplit(date);
        var date = new Date(dateTime.year, dateTime.month, dateTime.day, dateTime.hours, dateTime.minutes);
        var dayLabel = days[date.getDay()];
        var dateLabel = (date.getDate().toString().length == 1) ? "0" + date.getDate() : date.getDate();
        var monthLabel = months[date.getMonth() - 1];
        return "(" + dayLabel + ") " + dateLabel + " " + monthLabel;
    };

    var timeParser = function(date) {
        var dateTime = dateTimeSplit(date);
        var date = new Date(dateTime.year, dateTime.month, dateTime.day, dateTime.hours, dateTime.minutes);
        var hourLabel = (date.getHours().toString().length == 1) ? "0" + date.getHours() : date.getHours();
        var minuteLabel = (date.getMinutes().toString().length == 1) ? "0" + date.getMinutes() : date.getMinutes();

        return hourLabel + ":" + minuteLabel;
    };

    var dateTimeSplit = function(date) {
        var dateTime = date.split("T");
        var dateArray = _.map(dateTime[0].split("-"), function(num) {
            return parseInt(num, 10);
        });
        var timeArray = _.map(dateTime[1].split(":"), function(num) {
            return parseInt(num, 10);
        });

        return {
            year: dateArray[0],
            month: dateArray[1],
            day: dateArray[2],
            hours: timeArray[0],
            minutes: timeArray[1]
        }
    };

    var deepClone = function(list) {
        return JSON.parse(JSON.stringify(list));
    };

    var appendTo = function(parent, el) {
        $(parent).append(el);
    };

    return {
        init: init
    };
}();

var waitForLibraries = function() {
    setTimeout(function() {
        if (typeof _ == "undefined") {
            waitForLibraries();
        } else {
            SIA.MpIceCheckIn.init();
        }
    }, 100);
};

$(function() {
    waitForLibraries();
});