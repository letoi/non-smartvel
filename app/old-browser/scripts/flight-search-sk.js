/**
 * @name SIA
 * @description Define global flight economy functions
 * @version 1.0
 */

SIA.flightEconomy = function () {
    var global = SIA.global,
        config = global.config,
        body = $('body'),
        container = $('#container'),
        isChooseDate = null,
        priceDateOutbound,
        clickedBtn,
        miniFareData;

    var p = {};

    var saar5 = {
		l4: {
			sk: {
				chooseflight: {
                    points: "points",
                    miles: "miles",
                    carriersortingswitch: "true",
                    carriersorting: "SQ 1000-SQ 1099,SQ 1189-SQ 1199,SQ 1240-SQ 1297,SQ 1298-SQ 1449,SQ 1450-SQ 1579,SQ 1600-SQ 1649,SQ 1800-SQ 1850,SQ 1870-SQ 1899,SQ 1900-SQ 1999,SQ 2000-SQ 2299,SQ 2300-SQ 2399,SQ 2400-SQ 2429,SQ 2430-SQ 2439,SQ 2470-SQ 2499,SQ 2500-SQ 2599,SQ 2600-SQ 2769,SQ 2770-SQ 2799,SQ 2800-SQ 2849,SQ 2850-SQ 2899,SQ 2900-SQ 2999,SQ 5550-SQ 5569,SQ 5600-SQ 5699,SQ 5700-SQ 5799,SQ 5800-SQ 5839,SQ 5840-SQ 5869,SQ 5870-SQ 5879,SQ 5890-SQ 5899,SQ 5900-SQ 5999,SQ 4700-SQ 4939,SQ 6031-SQ 6049,SQ 6050-SQ 6055,SQ 6066-SQ 6099,SQ 6120-SQ 6139,SQ 6100-SQ 6119,SQ 6200-SQ 6349,SQ 4200-SQ 4599,SQ 6350-SQ 6999",
                    totalToBePaid: "Total to be paid",
                    totalToBePaidNow: "Total to be paid now",
                    adults: "Adults",
                    children: "Children",
                    infants: "Infants  ",
                    tabKey: "Please use the tab key to move through this application",
                    lowestFare: "Lowest available fare",
                    outbound: "Outbound fares",
                    inbound: "Inbound fares",
                    calendar: "calendar",
                    see: "See",
                    seven: "7",
                    three: "3",
                    five: "5",
                    dayFares: "-day fares",
                    toolTipMsg: "Fly to more places around the world with the Singapore Airlines Group, which includes Singapore Airlines, SilkAir and Scoot.",
                    logo: "Singapore Airlines Group",
                    showFilter: "Show filters",
                    filters: "Filters",
                    hideFilter: "Hide filters",
                    stopOver: "Stopover",
                    oneStop: "1-stop",
                    twoStop: "2-stop",
                    carrier: "Operating carrier",
                    partner: "Partner / codeshare airlines",
                    depTime: "Departure time",
                    arrTime: "Arrival time",
                    reset: "Reset filter",
                    noResult: "We are unable to find recommendations for your search. Please change your search criteria and resubmit the search.",
                    fareMsg1: "You are now on a fare table. To navigate through the fare selection, press tab. To quickly move to the other available flights, use the up or down arrow keys. To select a fare, press enter. To go directly to the Booking Summary Panel, press control Y. ",
                    recFlight: "Recommended flight for you",
                    depFlight: "Selected departure flight",
                    five: "5",
                    ten: "10",
                    flights: "flights",
                    load: "Load more flights",
                    show: "Show all flights",
                    summary: "Here's a summary of fare conditions",
                    fareMsg2: "Each of the flight segments you've selected comes with its own fare conditions. When you mix fare types, whether within the same cabin class or across different cabin classes, fare conditions for cancellation, booking change and no show will follow the more restrictive fare type.  ",
                    fareMsg44: "Each of the flight segments you've selected comes with its own fare conditions. When you mix fare types, whether within the same cabin class or across cabin classes, fare conditions for cancellation, booking change and no show will follow the more restrictive fare type.",
                    forMulticityBookings: "For multi-city bookings, all flights in the booking will share the same fare type. The fare type will be determined by the first flight segment. If you'd like to change the fare type for the entire booking, you'll need to reslect your first flight segment. For more details on Travel Itinerary Sequence and No Show, please refer to the full fare conditions.",
                    forMoreDetailsOn: "For more details on Travel Itinerary Sequence and No Show, please refer to the full fare conditions.",
                    conditions: "Fare conditions",
                    baggage: "Baggage",
                    seat: "Seat selection at booking",
                    earnMiles: "Earn KrisFlyer miles",
                    upgrade: "Upgrade with miles",
                    cancellation: "Cancellation",
                    booking: "Booking change",
                    noShow: "No show",
                    fullFare: "Full fare conditions",
                    secureOne: "Fare secure is not applicable to one ",
                    secureTwo: "or more of your selected flights",
                    secureFare: "Secure Fare",
                    CancellationMessage: "If your ticket is cancelled or unused, taxes and airport fees will be refunded to you.",
                    fareConditionMsg_1: "All payments must be made with a credit card. If your ticket is cancelled or unused, taxes and airport fees will be refunded to you.",
                    bookingSummaryMessage: "If your ticket is cancelled or unused, taxes and airport fees will be refunded to you.",
                    bookingSummaryMessageTooltip: "Log in to Manage Booking or get in touch with your local Singapore Airlines office in case you need to cancel your booking. Depending on the conditions of your selected fare type, cancellation fees may apply. Refer to your ticket for the fees.",
                    hold: "Hold this fare for",
                    economy: "Economy",
                    premiumEconomy: "Premium Economy",
                    flightMsg1: "Flight",
                    flightMsg2: ", Departing from",
                    flightMsg3: "on ",
                    flightMsg4: "at",
                    flightMsg5: ". Arriving in ",
                    flightMsg6: "Terminal",
                    flightMsg7: ", with layover time: ",
                    flightMsg8: ", total flight duration: ",
                    seatLeft: "seats left",
                    non_Stop: "Non-stop",
                    one_Stop: "One-stop",
                    two_Stop: "Two-stop",
                    sq: "SQ",
                    si: "SI",
                    tr: "TR",
                    moreInfo: "More info",
                    moreDetails: "More details",
                    lessDetails: "Fewer details",
                    sc: "SC",
                    layOver: "Layover time:",
                    business: "Business",
                    selected: "Selected",
                    first: "First",
                    scoot: "Part of your itinerary is operated by Scoot, the low-cost carrier of the Singapore Airlines Group. On your flight, you'll enjoy: ",
                    baggageAllowance: "Check-in baggage allowance",
                    hotMeal: "Complimentary hot meal on board",
                    scootBenefits: "About scoot benefits",
                    snooze: "Snooze Kit",
                    blanket: "A warm woven fleece blanket",
                    scootConditions: "You'll also enjoy the same fare conditions on Scoot that you've selected for your Singapore Airlines flight. Other services on Scoot are not included in your fare, and may be chargeable. When you fly on Scoot, their Conditions of Carriage apply.",
                    find: "Find out more",
                    notAllowed: "Not allowed",
                    complimentary: "Complimentary",
                    disabled: "disabled",
                    enabled: "enabled",
                    privileges: "View PPS Club / KrisFlyer privileges",
                    fareCondtns: "fare conditions",
                    upgradeTo: "Upgrade to",
                    close: "Close",
                    economyGood: "Economy good",
                    additional: "for an additional",
                    enjoy: ", and you'll enjoy:",
                    inflight: "Your Premium Economy Class inflight experience",
                    comfort: "Greater comfort",
                    choice: "Greater choices",
                    priority: "Priority treatment",
                    sit: "Sit back. All the way back, with a generous 38' seat pitch and 19.5' seat width.",
                    cook: "Savour more meal choices from our Premium Economy Book the Cook menu.",
                    checkin: "Enjoy priority check-in, boarding and baggage handling.",
                    kfMember: "PPS Club / KrisFlyer member privileges on Economy Class",
                    ppsCondition: "As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each Economy Class fare family.",
                    eliteGold: "KrisFlyer Elite Gold",
                    eliteSilver: "KrisFlyer Elite Silver",
                    krisflyer: "KrisFlyer",
                    more: "More",
                    benefits: "PPS Club member benefits",
                    fareCondition: "Fare condition",
                    economySuperSaver: "Economy Super Saver",
                    lite: "Lite",
                    standard: "Standard",
                    flexi: "Flexi",
                    additional30: "Additional 30kg",
                    standardSeat: "Standard Seat selection",
                    complimentary: "Complimentary",
                    forwardZone: "Forward Zone selection",
                    preferredSeat: "Extra Legroom Seat selection",
                    additional25: "Additional 25%",
                    allowed50: "Allowed (50%)",
                    exceptEcoLite: "(Except for Economy Lite)",
                    memeberOnly: "(member only)",
                    memeberCompanion: "(member and companion)",
                    ppsSwitchEnabled: "true",
                    termsAndConditions: "Terms and conditions",
                    termsAndConditionsOne: "Complimentary seat selection privileges for expired memberships",
                    termsAndConditionsTwo: "If your PPS Club / Krisflyer membership expires on or before the date of seat selection during booking,your complimentary seat selection privileges will remain unchanged.",
                    termsAndConditionsThree: "Complimentary additional baggage privileges for expired memberships",
                    termsAndConditionsFour: "If your PPS Club or Krisflyer Elite Gold membership expires on or before the counter check-in date, you and your travelling party will no longer enjoy that expired tier's complimentary baggage allowance.",
                    eliteGoldBenefits: "KrisFlyer Elite Gold member benefits",
                    additional20: "Additional 20kg",
                    eliteSilverBenefits: "KrisFlyer Elite Silver member benefits",
                    krisflyerBenefits: "KrisFlyer member benefits",
                    kfBusiness: "PPS Club / KrisFlyer member privileges on Business Class",
                    businessCondition: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy privileges across our fare types on Business Class, according to your membership tier.",
                    businessLite: "Business Lite",
                    businessStandard: "Business Standard",
                    businessFlexi: "Business Flexi",
                    kfFirst: "PPS Club / KrisFlyer member privileges on First Class / Suites",
                    firstFlexi: "First Flexi",
                    firstclassSuite: "First Class / Suites",
                    kfPremium: "PPS Club / KrisFlyer member privileges on Premium Economy Class",
                    additional35: "Additional 35kg",
                    firstCondition: "As a valued PPS Club or KrisFlyer member, you enjoy many privileges across each First Class fare family.",
                    asCharged: "As charged",
                    ppsClub: "PPS Club",
                    upgradePremium: "Upgrade to Premium Economy and enjoy:",
                    moreComfort: "More comfort, with a greater seat width and more legroom",
                    moreChoice: "More choices, as you can pre-order from our",
                    book: "Book",
                    cookOne: "Cook",
                    menu: "menu",
                    handling: "More privileges, with priority check-in, boarding and baggage handling",
                    adtnl: "Additional",
                    perPassenger: "Per passenger",
                    upgrde: "Upgrade",
                    baggageOne: "Baggage^",
                    seatOne: "Seat selection at booking^",
                    earnMilesOne: "Earn KrisFlyer miles^",
                    upgradeOne: "Upgrade with miles^",
                    fareMsg3: "^ These fare conditions are only applicable to Singapore Airlines and SilkAir flights. Refer to the full fare rules and conditions for more information when flying with partner airlines.",
                    fareMsg4: "Fares are not guaranteed until payment is completed",
                    fareMsg5: "View fare conditions when flying with our partner airlines",
                    fareMsg6: "Find out more about your Scoot flight",
                    fareMsg7: "SQ 2627 is a flight operated by Scandinavian Airlines. Fare conditions for baggage allowance, seat selection, earning of miles and upgrading with miles indicated below are only for Singapore Airlines and SilkAir flights.",
                    fareMsg8: "View conditions for flights operated by partner airlines",
                    moreFlights: "more flights",
                    adlt: "Adult",
                    chld: "Child",
                    infnt: "Infant",
                    editSearch: "Edit search",
                    oneWay: "One way",
                    departing: "Departing",
                    returning: "Returning",
                    totalFare: "Total fare",
                    fareMsg9: "Total fare includes discounts, taxes and surcharges",
                    flightMsg9: "Flights",
                    oldItinerary: "Old Itinerary",
                    fare: "Fare",
                    withTaxes: "(with taxes and surcharges)",
                    subTotal: "Subtotal",
                    previouslyPaid: "Previously paid",
                    miles: "miles",
                    newItinerary: "New Itinerary",
                    airportTax: "Airport/Government taxes",
                    carrierSurcharge: "Carrier surcharges",
                    rebookingFee: "Rebooking fee",
                    offset: "Offset by previously paid amount",
                    costBreakdown: "Cost breakdown by passengers",
                    fullFareCondition: "Full fare rules and conditions",
                    bagggeAllwnce: "Baggage allowance",
                    flyng: "Flying",
                    terminal: "Terminal",
                    totalTravel: "Total travel time:",
                    duration: "Travel duration",
                    showing: "Showing",
                    discounted: "Discounted",
                    rooms: "Rooms and rates",
                    international: "Int'l",
                    economyp1: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy seat selection and baggage allowance privileges across all our fare types on Economy Class, according to your membership tier.",
                    forallodpairs: "For all OD pairs",
                    travellingtofromusa: "Travelling to/from the USA",
                    kfbenefitseconomy: "Economy",
                    additional1to23: "Additional 1 piece (up to 23kg each)",
                    additional2to23: "Additional 2 pieces (up to 23kg each)",
                    additional2to32: "Additional 2 pieces (up to 32kg each)",
                    additional1to32: "Additional 1 piece (up to 32kg each)",
                    extralegroomseat: "Extra Legroom Seat selection",
                    frwdzoneseatselection: "Forward Zone Seat selection",
                    stdseatselection: "Standard Seat selection",
                    economyp2: "Complimentary advance seat selection privileges ",
                    economyp3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    economyp4: "Complimentary additional checked-in baggage allowance privileges",
                    economyp5: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    chrgble: "Chargeable",
                    economyp6: "Complimentary advance seat selection privileges",
                    economyp7: "Seat selection privileges apply to the principal member only. The selected seat will be yours even if your Elite Gold membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    economyp8: "Complimentary additional checked-in baggage allowance privileges",
                    economyp9: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    economypps3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member as well as his/her companions on the same booking only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    economypps3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member as well as his/her companions on the same booking only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    lte: "Lite",
                    std: "Standard",
                    fxi: "Flexi",
                    economyp10: "Complimentary advance seat selection privileges",
                    economyp11: "Seat selection privileges apply to the principal member only. The selected seat will be yours even if your Elite Silver membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    travellingtofromother: "Travelling to/from any other destination",
                    add40kg: "Additional 40kg",
                    economyp12: "Complimentary additional checked-in baggage allowance privileges",
                    economyp13: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    kfgold: "KRISFLYER GOLD",
                    kfgoldbenefits: "KrisFlyer Elite Gold member benefits",
                    kfsilverbenefits: "KrisFlyer Elite Silver member benefits",
                    add25kg: "Additional 25kg",
                    economyp14: "Complimentary additional checked-in baggage allowance privileges",
                    economyp15: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    kfsilver: "KRISFLYER SILVER",
                    peyp1: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy seat selection and baggage allowance privileges across our fare types on Premium Economy Class, according to your membership tier. ",
                    peyp2: "Complimentary advance seat selection privileges",
                    peyp3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    peyp4: "Complimentary additional checked-in baggage allowance privileges",
                    peyp5: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    peyp6: "Complimentary additional checked-in baggage allowance privileges",
                    peyp7: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    forallodpairs: "For all OD pairs",
                    peyp8: "As a valued KrisFlyer Elite or PPS Club member, you’ll enjoy privileges on First Class / Suites, according to your membership tier.",
                    travellingtofromanyotherdest: "Travelling to/from any other destination",
                    add50kg: "Additional 50kg",
                    peyp9: "Complimentary additional checked-in baggage allowance privileges",
                    peyp10: "Complimentary extra check-in baggage allowance is offered to you and your immediate family members based on your PPS Club membership at the point of check-in at the airport.You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    peyp11: "Complimentary additional checked-in baggage allowance privileges",
                    peyp12: "Complimentary extra check-in baggage allowance is offered to you based on your Elite Gold membership at the point of check-in at the airport. You will enjoy complimentary additional check-in baggage allowance only if your membership tier is valid on the day of departure.",
                    checkedinbaggageallowance: "Checked-in baggage allowance",
                    peypps3: "Seat selection privileges apply to the principal (or supplementary) PPS Club member as well as his/her companions on the same booking only. The selected seat will be yours even if your membership expires before the day of departure, as long as you do not make any changes to your seat or booking.",
                    upgradeAward: "???jslocalisation.saar5.l.sk.chooseflight.upgradeAward???",
                    milesRequired: "???jslocalisation.saar5.l.sk.chooseflight.milesRequired???",
                    promo: "???jslocalisation.saar5.l.sk.chooseflight.promo???",
                    kilos: "???jslocalisation.saar5.l.sk.chooseflight.kilos???",
                    sgd: "???jslocalisation.saar5.l.sk.chooseflight.sgd???",
                    fullFareRules: "Full fare rules and conditions",
                    operatedBy: "Operated By",
                    suites: "/Suites",
                    oneAdult: "1 adult",
                    fiveKg: "???jslocalisation.saar5.l.sk.chooseflight.fiveKg???",
                    fiftyPcnt: "???jslocalisation.saar5.l.sk.chooseflight.fiftyPcnt???",
                    flexibility: "Flexibility",
                    seats: "Seats",
                    additionalBaggagePerPax: "Additional baggage per passenger",
                    earnMoreMiles: "Earn more KrisFlyer miles with this flight",
                    saveBooking: "???jslocalisation.saar5.l.sk.chooseflight.saveBooking???",
                    paySeat: "???jslocalisation.saar5.l.sk.chooseflight.paySeat???",
                    earnFiftyMiles: "???jslocalisation.saar5.l.sk.chooseflight.earnFiftyMiles???",
                    saveUsd: "???jslocalisation.saar5.l.sk.chooseflight.saveUsd???",
                    paySeatSelection: "Pay for seat selection at booking",
                    isNotOffered: "???jslocalisation.saar5.l.sk.chooseflight.isNotOffered???",
                    superSaver: "???jslocalisation.saar5.l.sk.chooseflight.superSaver???",
                    sgd305: "???jslocalisation.saar5.l.sk.chooseflight.sgd305???",
                    saverUpgradeFrom: "Saver upgrade from",
                    to: "to",
                    from: "From",
                    selectInbound: "Select your inbound flight",
                    selectOutbound: "Select your outbound flight",
                    change: "Change",
                    s: "s",
                    notAvailable: "Not available",
                    proceed: "Proceed",
                    milesRequiredMsg: "miles required ",
                    advantageUpgradeFrom: "Advantage upgrade from",
                    complimentarySeatMsg1: "Your travel party enjoy complimentary selection of Standard Seats as you're travelling with a child and infant",
                    complimentarySeatMsg2: "Your travel party enjoy complimentary selection of Standard Seats as you're travelling with a child",
                    complimentarySeatMsg3: "Your travel party enjoy complimentary selection of Standard Seats as you're travelling with an infant",
                    travelpartyppsmessage: "Your travel party enjoys complimentary selection of Forward Zone and Standard Seats as you're travelling with a PPS Club member",
                    isaflightoperatedby: "???jslocalisation.saar5.l.sk.chooseflight.isaflightoperatedby???",
                    fareconditionforoal: "???jslocalisation.saar5.l.sk.chooseflight.fareconditionforoal???",
                    scootMsg: "Fare conditions for seat selection. earning of miles and upgrading with miles indicated below are only for Singapore Airlines and SilkAir flights.",
                    milesUpgrade: "We are unable to process your request. Please contact your local Singapore Airlines office to find out the number of miles required for your upgrade.",
                    lblSave: "Save",
                    onBookingChange: "on booking changes",
                    keepMySelection: "Keep my selection",
                    your: "Your",
                    classInflightExperience: "Class inflight experience",
                    save: "Save",
                    bookingChange: "on booking changes",
                    freeSeatSelection: "Free seat selection at booking",
                    selectedReturn: "Selected return flight",
                    mixedcabin: "Mixed Cabin",
                    inflightPey: "Your Premium Economy Class inflight experience",
                    inflightBiz: "Your Business Class inflight experience",
                    inflightFirst: "Your First Class/Suite inflight experience",
                    newSeatPosition: "New seat position",
                    fullyFlatBed: "Fully-flat bed",
                    storageSpace: "Storage space",
                    privateSpace: "Private space",
                    extraWideSeat: "Extra wide seat",
                    moodLighting: "Mood lighting",
                    oaldisplaymsg: "These fare conditions are only applicable to Singapore Airlines and SilkAir flights. Refer to the full fare rules and conditions for more information when flying with partner airlines.",
                    mixedcabindisplaymsg: "Each of the flight segments you've selected comes with its own fare conditions. When you mix fare types, whether within the same cabin class or across cabin classes, fare conditions for cancellation, booking change and no show will follow the more restrictive fare type.",
                    seatselection: "Fees shown here for seat selection are indicative. As the original fees are based in US dollars, the actual fees may vary due to exchange rate fluctuations. The original fees can be found on the ticket of a confirmed booking.",
                    cancellationNoShow: "Fees shown here for change, cancellation and no show are indicative. As the original fees are based in US dollars, the actual fees may vary due to exchange rate fluctuations. The original fees can be found on the ticket of a confirmed booking.",
                    seatselectionCancellationNoShow: "Fees shown here for seat selection, change, cancellation and no show are indicative. As the original fees are based in US dollars, the actual fees may vary due to exchange rate fluctuations. The original fees can be found on the ticket of a confirmed booking.",
                    currencyconvertmsg: "Fees shown here are indicative. As the original fees are based in US dollars, the actual fees may vary due to exchange rate fluctuations. The original fees can be found on the ticket of a confirmed booking.",
                    fourDigitFareFamilyCodes: "FC01,FC21,FC31,FC41,FC51,FF13,FF14"
                }
			}
		}
    };
    
    var flightTableData = {};

    if (!Array.prototype.filter) {
        Array.prototype.filter = function (fun /*, thisp */) {
            "use strict";

            if (this === void 0 || this === null)
                throw new TypeError();

            var t = Object(this);
            var len = t.length >>> 0;
            if (typeof fun !== "function")
                throw new TypeError();

            var res = [];
            var thisp = arguments[1];
            for (var i = 0; i < len; i++) {
                if (i in t) {
                    var val = t[i]; // in case fun mutates this
                    if (fun.call(thisp, val, i, t))
                        res.push(val);
                }
            }

            return res;
        };
    }

    var slideshowpremiumeconomy = function () {
        if ($('body').hasClass('fs-economy')) {
            var totalDesktopSlide = 1,
                totalLandscapeSlide = 1;
            $('[data-slideshow-premium-economy]').each(function () {
                var slider = $(this);
                slider.find('img').each(function () {
                    var self = $(this),
                        newImg = new Image();
                    newImg.onload = function () {
                        slider.css('visibility', 'visible');
                        slider.find('.slides').slick({
                            siaCustomisations: true,
                            dots: false,
                            speed: 300,
                            draggable: true,
                            slidesToShow: totalDesktopSlide,
                            slidesToScroll: totalLandscapeSlide,
                            accessibility: false,
                            arrows: true
                        });
                    };
                    newImg.src = self.attr('src');
                });
            });
        }
    };
    var initSlider = function (sliderEl, btnNext, btnPrev) {
        var wrapperSlides = $(sliderEl).find('.slides');

        if (wrapperSlides.is('.slick-initialized')) {
            wrapperSlides.slick('unslick');
        }

        wrapperSlides.slick({
            dots: false,
            draggable: false,
            infinite: false,
            speed: 300,
            slidesToShow: 7,
            slidesToScroll: 7,
            prevArrow: btnPrev,
            nextArrow: btnNext
        })

        selectItemSlider(wrapperSlides);

        if (!sliderEl.data('slider-outbound')) {
            sliderEl.find('.slide-item').each(function () {
                var selfPrice = parseFloat($(this).find('.large-price').text()),
                    priceAfterSelect = selfPrice - priceDateOutbound;

                $(this).find('.large-price').text('+ ' + (priceAfterSelect > 0 ? priceAfterSelect : 0));
            })
        }
    }

    var selectItemSlider = function (wrapperSlides) {
        var slideItem = wrapperSlides.find('.slick-slide');
        slideItem.each(function () {
            if (isChooseDate) {
                $(this).data('date') === isChooseDate && $(this).addClass('selected');
            }
            $(this).off('click.selectSlideItem').on('click.selectSlideItem', function (e) {
                e.preventDefault();
                var isOutBound = $(this).closest('[data-fs-slider]').data('slider-outbound');
                isOutBound && $('[data-fs-slider]').attr('data-selected-date', $(this).data('date'));
                slideItem.removeClass('selected');
                $(this).addClass('selected');
                isChooseDate = $(this).data('date');

                if (isOutBound) {
                    priceDateOutbound = parseFloat($(this).find('.large-price').text());
                }
            });
        })
    }

    var renderPopupBenefit = function (popupEl, callback) {
        $.get(global.config.url.fsEconomyBenefitTemplate, function (data) {
            var content = popupEl.find('.popup__content');
            var template = window._.template(data, {
                data: data
            });
            $(template).prependTo(content.empty());
            // reinit js
            SIA.initTabMenu();
            SIA.multiTabsWithLongText()

            if (callback) {
                callback();
            }
        });
    }

    var renderPopupPremiumBenefit = function (popupEl, callback) {
        $.get(global.config.url.fsEconomyPremiumBenefitTemplate, function (data) {
            var content = popupEl.find('.popup__content');
            var template = window._.template(data, {
                data: data
            });
            $(template).prependTo(content.empty());
            // reinit js
            SIA.initTabMenu();
            SIA.multiTabsWithLongText()

            if (callback) {
                callback();
            }
        });
    }

    var initPopup = function (data) {
        var triggerPopup = $('[data-trigger-popup]');

        triggerPopup.each(function () {
            var self = $(this);

            if (typeof self.data('trigger-popup') === 'boolean') {
                return;
            }

            var popup = $(self.data('trigger-popup'));

            if (!popup.data('Popup')) {
                popup.Popup({
                    overlayBGTemplate: config.template.overlay,
                    modalShowClass: '',
                    triggerCloseModal: '.popup__close, [data-close], .cancel',

                    afterHide: function () {
                        container.css('padding-right', '');
                        body.css('overflow', '');
                        var ua = navigator.userAgent.toLowerCase();
                        if (ua.indexOf('safari') != -1) {
                            if (ua.indexOf('chrome') > -1) {
                            }
                            else {
                                body.attr('style', function (i, s) {
                                    return s.replace('overflow:hidden !important;', '');
                                });
                            }
                        }
                    }
                });
            }
            self.off('click.showPopup').on('click.showPopup', function (e) {
                e.preventDefault();
                var jsonURL = self.data('flight-json-url');
                if (!self.hasClass('disabled')) {
                    // render popup benefit detail
                    if (self.data('trigger-popup') === '.popup-view-benefit--krisflyer') {
                        renderPopupBenefit(popup, function () {
                            popup.Popup('show');
                            $(window).trigger('resize');
                        });
                    } else if (self.data('trigger-popup') === '.popup-view-partner-airlines') {
                        popup.Popup('show');
                    } else if (self.data('trigger-popup') === '.popup-view-premium-benefit--krisflyer') {
                        renderPopupPremiumBenefit(popup, function () {
                            popup.Popup('show');
                            $(window).trigger('resize');
                        });
                    }
                }
            });

        });
    };

    var showHideFilters = function () {
        var fsFilters = $('.flight-search-filter-economy');

        fsFilters.each(function () {
            var linkShow = $(this).find('.link-show'),
                linkHide = $(this).find('.link-hide'),
                content = $(this).find('.content');

            linkShow.off('click').on('click', function (e) {
                e.preventDefault();
                if (!$(this).parent().is('.active')) {
                    $(this).parent().addClass('active')
                    linkShow.hide();
                    content.fadeIn();
                }
            });
            linkHide.off('click', 'a').on('click', 'a', function (e) {
                e.preventDefault();
                // e.stopPropagation();
                content.hide();
                linkShow.fadeIn();
                $(this).closest('.flight-search-filter-economy').removeClass('active');
            });
        })
    };

    var showMoreLessDetails = function () {
        var btnMore = $('[data-more-details-table]'),
            btnLess = $('[data-less-details-table]'),
            triggerAnimation = $('[data-trigger-animation]'),
            wrapFlights = $('[data-wrap-flight]'),
            fareBlocks = $('[data-hidden-recommended]');

        // set data-height for wrap flight
        wrapFlights.each(function () {
            var flightLegItem = $(this).find('.flight-result-leg'),
                wrapFlightHeight = 0;

            flightLegItem.each(function () {
                wrapFlightHeight += $(this).outerHeight();
            });

            $(this).attr('data-wrap-flight-height', wrapFlightHeight);
        });

        btnMore.each(function () {
            $(this).off('click.showMore').on('click.showMore', function (e) {
                e.preventDefault();
                var flightItem = $(this).closest('[data-flight-item]'),
                    controlFlight = flightItem.find('.control-flight-station'),
                    wrapFlight = flightItem.find('[data-wrap-flight]');
                    controlFlightHeight = wrapFlight.siblings('.control-flight-station').outerHeight() || 84;
                    wrapFlightHeight = wrapFlight.data('wrap-flight-height');

                if (wrapFlight) {
                    if ($('html').hasClass('ie8')) {
                        flightItem.find('[data-first-wrap-flight]').addClass('hidden');
                    }

                    var pad = window.innerWidth < 988 ? 40 : 0;

                    flightItem.addClass('active');
                    if ($('html').hasClass('ie8')) {
                        wrapFlight.css({
                            'height': wrapFlightHeight - controlFlightHeight + controlFlightHeight + pad + 'px',
                            '-webkit-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-moz-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-ms-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-o-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            'transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)'
                        })
                    } else {
                        wrapFlight.css({
                            'height': wrapFlightHeight - controlFlightHeight + 'px',
                            '-webkit-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-moz-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-ms-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            '-o-transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)',
                            'transform': 'translate3d(0px, -' + controlFlightHeight + 'px, 0)'
                        })
                    }
                }
            });
        });

        btnLess.each(function () {
            $(this).off('click.showLess').on('click.showLess', function (e) {
                e.preventDefault();
                var flightItem = $(this).closest('[data-flight-item]'),
                    controlFlight = flightItem.find('.control-flight-station'),
                    wrapFlight = flightItem.find('[data-wrap-flight]');

                if (wrapFlight) {
                    if ($('html').hasClass('ie8')) {
                        flightItem.find('[data-first-wrap-flight]').removeClass('hidden');
                    }
                    flightItem.removeClass('active');
                    wrapFlight.css({
                        'height': 0,
                        '-webkit-transform': 'translate3d(0)',
                        '-moz-transform': 'translate3d(0)',
                        '-ms-transform': 'translate3d(0)',
                        '-o-transform': 'translate3d(0)',
                        'transform': 'translate3d(0)'
                    })
                }
            })
        });
    }

    var selectFlightAnimation = function () {
        var triggerAnimation = $('[data-trigger-animation]');
        triggerAnimation.removeAttr('data-trigger-popup');
        triggerAnimation.each(function () {
            $(this).off('click.toggleAnimation, keydown.toggleAnimation').on('click.toggleAnimation, keydown.toggleAnimation', function (e, isOneAnimation) {
                var parentRecommended = $(this).parent(),
                    flightItem = parentRecommended.find('[data-flight-item]'),
                    wrapFlight = flightItem.find('[data-wrap-flight]'),
                    btnMore = flightItem.find('[data-more-details-table]'),
                    flightStation = flightItem.find('.flight-station'),
                    hiddenRecommended = parentRecommended.siblings('[data-hidden-recommended]'),
                    hiddenRecommended1 = parentRecommended.siblings('[data-hidden-recommended-1]'),
                    isOpen = $(this).is('.active'),
                    hiddenRecommendedHeight = hiddenRecommended.data('fare-block-height'),
                    colIdx = $(this).data('trigger-animation'),
                    selfTable = $('[data-col-index="' + colIdx + '"]'),
                    code = e.keyCode || e.which;

                if ($('body').hasClass('sk-ut-workflow') && $(this).hasClass('economy-flight--pey')) {
                    return false;
                }

                triggerAnimation.not($(this)).each(function () {
                    if ($(this).is('.active')) {
                        $(this).trigger('click', true);
                    }
                })

                // reset animation
                if (wrapFlight) {
                    flightItem.removeClass('active');
                    wrapFlight.css({
                        'height': '10px',
                        '-webkit-transform': 'translate3d(0)',
                        '-moz-transform': 'translate3d(0)',
                        '-ms-transform': 'translate3d(0)',
                        '-o-transform': 'translate3d(0)',
                        'transform': 'translate3d(0)'
                    })
                }
                $('[data-col-index]').find('[data-tooltip], a, input').attr('tabindex', -1);
                if (!$(this).next().is('.not-available')) {
                    $(this).next().attr('tabindex', 0);
                }
                parentRecommended.find('[data-trigger-animation]').removeClass('active');
                hiddenRecommended.removeClass('active economy-flight--green , business-flight--blue');
                hiddenRecommended1.removeClass('active economy-flight--pey , business-flight--red');
                var recommendedFlight = $(this).closest('.flight-list-item').find('.flight-station-item').find('.inner-info');
                recommendedFlight.find('.cabin-color').addClass('hidden');
                recommendedFlight.find('.cabin-color.cabin-class-selected').removeClass('hidden');

                // start animation
                if (!isOpen) {
                    if (!$(this).hasClass('not-available')) {
                        selfTable.find('[data-tooltip], input').attr('tabindex', 0);
                        selfTable.find('a').each(function () {
                            var hasDisabled = $(this).closest('.has-disabled').length;

                            !hasDisabled && $(this).attr('tabindex', 0);
                        })
                        $(this).next().attr('tabindex', -1);
                        btnMore.trigger('click.showMore');
                        $(this).addClass('active');

                        var selectedCabinClass = $(this).parent().parent().find('.bgd-white [data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]');
                        if ($(this).hasClass('column-trigger-animation')) {
                            var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
                            if (!$(body).hasClass('fs-business')) {
                                hiddenRecommended.addClass('active economy-flight--green');
                            } else {
                                hiddenRecommended.addClass('active business-flight--blue');
                            }
                            recommendedFlightItem.find('[data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]').removeClass('hidden');
                            recommendedFlightItem.find('[data-col-cabinclass="' + $(this).prev().data('target-cabinclass') + '"]').addClass('hidden');

                            selectedCabinClass.addClass('cabin-class-selected');
                            selectedCabinClass.next().removeClass('cabin-class-selected').addClass('hidden');
                        }
                        if ($(this).hasClass('column-trigger-animation-1')) {
                            var recommendedFlightItem = $(this).closest('.flight-list-item').find('.flight-station-item');
                            if (!$(body).hasClass('fs-business')) {
                                hiddenRecommended1.addClass('active economy-flight--pey');
                            } else {
                                if ($(body).hasClass('fs-economy-rbd')) {
                                    hiddenRecommended1.addClass('active economy-flight--pey');
                                } else {
                                    hiddenRecommended1.addClass('active business-flight--red');
                                }
                            }

                            recommendedFlightItem.find('[data-col-cabinclass="' + $(this).data('target-cabinclass') + '"]').removeClass('hidden');
                            recommendedFlightItem.find('[data-col-cabinclass="' + $(this).next().data('target-cabinclass') + '"]').addClass('hidden');

                            selectedCabinClass.addClass('cabin-class-selected');
                            selectedCabinClass.prev().removeClass('cabin-class-selected').addClass('hidden');
                        }
                    }
                }
            });
        });
    }

    var getTemplateCarousel = function (sliderEl, btnNext, btnPrev, daysData, callback) {
        $.get(global.config.url.fsSevenDayFareTemplate, function (data) {
            var slides = sliderEl.find('.slides');
            var template = window._.template(data, {
                data: daysData
            });
            slides.empty().append($(template));
            initSlider(sliderEl, btnNext, btnPrev);

            if (callback) {
                callback();
            }
        });
    }

    var loadCarouselSevenDay = function (sliderEl, btnNext, btnPrev, isNext) {
        $.get(global.config.url.fsSevenDayFareJson, function (data) {
            var initDays = data.response.byDay,
                currDate = sliderEl.data('current-date'),
                currDateIdx = 0,
                slideToIdx = 0,
                currDateIdx1 = 7;

            _.map(initDays, function (day, idx) {
                if (day.month === currDate) {
                    currDateIdx = idx;
                }
                return false;
            })
            if (typeof isNext !== 'undefined') {
                currDateIdx = isNext ? currDateIdx + 7 : currDateIdx - 7;
            }

            currDateIdx = currDateIdx < 0 ? 0 : currDateIdx;

            currDateIdx1 = currDateIdx;

            if (initDays.length - currDateIdx < 7) {
                currDateIdx = currDateIdx - (7 - (initDays.length - currDateIdx));
            }

            days = initDays.slice(currDateIdx, currDateIdx + 7);
            getTemplateCarousel(sliderEl, btnNext, btnPrev, days, function () {
                if (typeof isNext !== 'undefined') {
                    sliderEl.data('current-date', initDays[currDateIdx].month);
                } else {
                    $('.slick-slide.slick-active').each(function () {
                        if ($(this).data('date') === sliderEl.attr('data-selected-date') && !$(this).is('.selected')) {
                            $(this).trigger('click.selectSlideItem');
                        }
                    })
                }
                btnNext && initDays.length - currDateIdx1 < 7 && currDateIdx !== 0 ? btnNext.attr('disabled', true) : btnNext.attr('disabled', false);
                btnPrev && currDateIdx === 0 ? btnPrev.attr('disabled', true) : btnPrev.attr('disabled', false);
            });
        })
    }

    var handleActionSlider = function () {
        var slider = $('[data-fs-slider]');
        // init
        slider.each(function () {
            var self = $(this);
            btnPrev = $(this).find('.btn-prev'),
                btnNext = $(this).find('.btn-next');

            loadCarouselSevenDay($(this), btnNext, btnPrev);

            btnPrev.off('click.slidePrev').on('click.slidePrev', function () {
                loadCarouselSevenDay(self, $(this).siblings('.btn-next'), $(this), false);
            });

            btnNext.off('click.slideNext').on('click.slideNext', function () {
                loadCarouselSevenDay(self, $(this), $(this).siblings('.btn-prev'), true);
            });

        });

    }

    var formatTimeToHour = function (seconds) {
        return parseFloat(seconds / 3600).toFixed(2) + 'hr';
    }

    var formatTimeToDate = function (seconds) {
        var dateObj = new Date(seconds),
            date = dateObj.getDate(),
            month = dateObj.getMonth() + 1,
            hour = dateObj.getHours(),
            minute = dateObj.getMinutes();

        return hour + ':' + minute;
    }

    var sliderRange = function () {
        var rangeSlider = $('[data-range-slider]');

        rangeSlider.each(function () {
            var min = $(this).data('min'),
                max = $(this).data('max'),
                step = $(this).data('step'),
                unit = $(this).data('unit'),
                type = $(this).data('range-slider');
            labelFrom = '<span class="slider-from ' + type + '"></span',
                labelTo = '<span class="slider-to ' + type + '"></span',

                $(this).slider({
                    range: true,
                    min: min,
                    max: max,
                    step: step,
                    values: [min, max],
                    create: function () {
                        var slider = $(this),
                            leftLabel,
                            rightLabel;

                        switch (type) {
                            case "tripDuration":
                            case "layover":
                                leftLabel = $(labelFrom).text(formatTimeToHour(min));
                                rightLabel = $(labelTo).text(formatTimeToHour(max));
                                break;
                            case "departure":
                            case "arrival":
                                leftLabel = $(labelFrom).text(formatTimeToDate(min));
                                rightLabel = $(labelTo).text(formatTimeToDate(max));
                                break;
                            default:
                                break;
                        }

                        $(this).append(leftLabel);
                        $(this).append(rightLabel);
                    },
                    slide: function (event, ui) {
                        var Label;

                        switch (type) {
                            case "tripDuration":
                            case "layover":
                                $(this).find('.slider-from').text(formatTimeToHour(ui.values[0]));
                                $(this).find('.slider-to').text(formatTimeToHour(ui.values[1]));
                                break;
                            case "departure":
                            case "arrival":
                                $(this).find('.slider-from').text(formatTimeToDate(ui.values[0]));
                                $(this).find('.slider-to').text(formatTimeToDate(ui.values[1]));
                                break;
                            default:
                                break;
                        }
                    }
                })

            if (type === 'tripDuration') {
                $(this).find('.ui-slider-handle').eq(0).remove();
                $(this).find('.slider-from').remove();
            }

        });
    }

    var countLayover = function (segment) {
        var countLayover = 0;

        _.map(segment.legs, function (leg) {
            leg.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;

            leg.stops.length && _.map(leg.stops, function (stop) {
                stop.layoverDuration > 0 ? countLayover += 1 : countLayover = countLayover;
            })
        })

        return countLayover;
    }

    var getFilterData = function (flightsData) {
        var arr = [];

        _.map(flightsData, function (flight, flightIdx) {
            var obj = {
                "nonStop": false,
                "oneStop": false,
                "twoStop": false,
                "codeShare": false
            };


            var tripDuration = _.sortBy(flight.segments, function (segment) {
                return segment.tripDuration;
            });

            var departure = _.sortBy(flight.segments, function (segment) {
                return new Date(segment.departureDateTime.replace(/-/g, '/')).getTime();
            });

            var arrival = _.sortBy(flight.segments, function (segment) {
                return new Date(segment.arrivalDateTime.replace(/-/g, '/')).getTime();
            });

            var layover = _.sortBy(flight.segments, function (segment) {
                var totalLayover = 0;

                _.map(segment.legs, function (leg, legIdx) {
                    totalLayover += leg.layoverDuration;
                })
                segment['totalLayover'] = totalLayover
                return totalLayover
            });

            _.map(flight.segments, function (segment) {
                var count = countLayover(segment);

                !segment.legs.length && (obj.nonStop = true);
                switch (count) {
                    case 0:
                        !obj.nonStop && (obj.nonStop = true);
                        break;
                    case 1:
                        !obj.oneStop && (obj.oneStop = true);
                        break;
                    case 2:
                        !obj.twoStop && (obj.twoStop = true);
                        break;
                    default:
                        break;
                }

                segment.legs.length && _.map(segment.legs, function (leg) {
                    if (typeof leg.codeShareFlight === "boolean") {
                        leg.codeShareFlight && (obj.codeShare = true);
                    }
                })
            })

            obj['minTripDuration'] = tripDuration[0].tripDuration;
            obj['maxTripDuration'] = tripDuration[tripDuration.length - 1].tripDuration;
            obj['minDeparture'] = new Date(departure[0].departureDateTime.replace(/-/g, '/')).getTime();
            obj['maxDeparture'] = new Date(departure[departure.length - 1].departureDateTime.replace(/-/g, '/')).getTime();
            obj['minArrival'] = new Date(arrival[0].arrivalDateTime.replace(/-/g, '/')).getTime();
            obj['maxArrival'] = new Date(arrival[arrival.length - 1].arrivalDateTime.replace(/-/g, '/')).getTime();
            obj['minLayover'] = layover[0].totalLayover;
            obj['maxLayover'] = layover[layover.length - 1].totalLayover;

            arr.push(obj);
        })

        return arr;
    }

    var filterFlights = function (data) {
        var filterBlock = $('[data-flight-filter]'),
            flightsArr = data.flights;

        filterBlock.each(function () {
            var flightsBlock = $(this).siblings('.recommended-flight-block'),
                flightIdx = $(this).data('flightFilter'),
                nonStopCheckbox = $('input[name="non-stop-' + flightIdx + '"]'),
                oneStopCheckbox = $('input[name="one-stop-' + flightIdx + '"]'),
                twoStopCheckbox = $('input[name="two-stop-' + flightIdx + '"]'),
                codeShareCheckbox = $('input[name="codeshare-' + flightIdx + '"]'),
                saGroupCheckbox = $('input[name="sa-group-' + flightIdx + '"]'),
                sliderTripDuration = $(this).find('[data-range-slider="tripDuration"]'),
                sliderDeparture = $(this).find('[data-range-slider="departure"]'),
                sliderArrival = $(this).find('[data-range-slider="arrival"]'),
                filterObj = {
                    "stopover": {
                        "nonstopVal": true,
                        "onestopVal": true,
                        "twostopVal": true
                    },
                    "operating": {
                        "codeshareVal": true
                    },
                    "tripduration": sliderTripDuration.slider("values"),
                    "departure": sliderDeparture.slider("values"),
                    "arrival": sliderArrival.slider("values")
                };

            // get value from checkbox

            nonStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.nonstopVal = $(this).is(':checked');
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            oneStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.onestopVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            twoStopCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.stopover.twostopVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            codeShareCheckbox.off('change.getValue').on('change.getValue', function () {
                filterObj.operating.codeshareVal = $(this).is(':checked')
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            saGroupCheckbox.off('change.resetFilter').on('change.resetFilter', function () {
                filterObj.stopover.nonstopVal = false;
                filterObj.stopover.onestopVal = false;
                filterObj.stopover.twostopVal = false;
                filterObj.operating.codeshareVal = false;
                filterObj.tripduration = [sliderTripDuration.data('min'), sliderTripDuration.data('max')];
                filterObj.departure = [sliderDeparture.data('min'), sliderDeparture.data('max')];
                filterObj.arrival = [sliderArrival.data('min'), sliderArrival.data('max')];
                handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
            });

            // get value from range slider

            sliderTripDuration.slider({
                stop: function (event, ui) {
                    filterObj.tripduration = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            sliderDeparture.slider({
                stop: function (event, ui) {
                    filterObj.departure = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            sliderArrival.slider({
                stop: function (event, ui) {
                    filterObj.arrival = ui.values;
                    handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);
                }
            });

            // handleFilterFlights(flightsBlock, data, flightsArr[flightIdx].segments, filterObj, flightIdx);

        });
    }

    var handleFilterFlights = function (el, data, flightData, filterObj, flightIdx) {
        var selfData = [],
            filterDataStopover = [],
            filterDataOperating = [],
            obj = {};

        _.map(filterObj.stopover, function (value, key) {
            selfData = [];

            switch (key) {
                case 'nonstopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return segment.legs.length === 0 || count === 0;
                    }));
                    break;
                case 'onestopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return count === 1;
                    }));
                    break;
                case 'twostopVal':
                    value && (selfData = flightData.filter(function (segment) {
                        var count = countLayover(segment);

                        return count === 2;
                    }));
                    break;
                default:
                    break;
            }

            filterDataStopover = $.unique([].concat.apply([], [filterDataStopover, selfData]));
        });

        filterDataStopover.length && (flightData = filterDataStopover);

        _.map(filterObj.operating, function (value, key) {
            selfData = [];

            _.map(flightData, function (flight) {
                flight.legs.length && _.map(flight.legs, function (leg) {
                    if (key === 'codeshareVal') {
                        if (value) {
                            typeof leg.codeShareFlight !== 'undefined' && selfData.push(flight);
                        } else {
                            typeof leg.codeShareFlight === 'undefined' && selfData.push(flight);
                        }
                        return false;
                    }
                })
                return false;
            })

            filterDataOperating = $.unique([].concat.apply([], [filterDataOperating, selfData]));
        });

        filterDataOperating.length && (flightData = filterDataOperating);

        var tripDuration = filterObj.tripduration;

        selfData = flightData.filter(function (flight) {
            return flight.tripDuration >= tripDuration[0] && flight.tripDuration <= tripDuration[1];
        });

        flightData = selfData;

        var departureDateTime = filterObj.departure;

        selfData = flightData.filter(function (flight) {
            return new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() >= departureDateTime[0] && new Date(flight.departureDateTime.replace(/-/g, '/')).getTime() <= departureDateTime[1];
        });

        flightData = selfData;

        var arrivalDateTime = filterObj.arrival;

        selfData = flightData.filter(function (flight) {
            return new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() >= arrivalDateTime[0] && new Date(flight.arrivalDateTime.replace(/-/g, '/')).getTime() <= arrivalDateTime[1];
        });

        flightData = selfData;

        obj['segments'] = flightData;

        getTemplateFlightTable(el, data, obj, flightIdx, true);

    }

    var handleLoadmore = function (flightBlock, loadmoreBlock) {
        var flights = flightBlock.find('.flight-list-item'),
            flightsHidden = flightBlock.find('.flight-list-item.hidden');

        flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
        loadmoreBlock.find('[data-total-flight]').text(flights.length);
        loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
        loadmoreBlock.off('click.loadMore').on('click.loadMore', function (e) {
            e.preventDefault();
            flightBlock.find('.flight-list-item.hidden').slice(0, 5).removeClass('hidden');
            flightsHidden = flightBlock.find('.flight-list-item.hidden');
            loadmoreBlock.find('[data-loaded-flight]').text(flights.length - flightsHidden.length);
            flightsHidden.length ? loadmoreBlock.removeClass('hidden') : loadmoreBlock.addClass('hidden');
            selectFlightAnimation();
            showMoreLessDetails();
        });
    }

    var labelStatusCheapest = function () {
        var arrPriceCheapest = [];
        var blockWrapFirst = $('.wrap-content-fs').first();
        var colInfoLeft = blockWrapFirst.find('.col-info-left');
        colInfoLeft.each(function (idexLabel) {
            arrPriceCheapest.push($(this).find('.flight-price').find('.price-cheapest-colum').text());
            arrPriceCheapest.sort(function (a, b) {
                return parseFloat(a) - parseFloat(b);
            });
            if ($(this).find('.flight-price').find('.price-cheapest-colum').text() === arrPriceCheapest[0]) {
                $(this).find('.head-col').append('<span class="label-status anim-all"></span');
            }
        });
    }

    var tableLoadPage = function (isRenderTemplate) {
        !isRenderTemplate && setTimeout(function () {
            if ($('.main-inner').find('.wrap-content-fs').length == 2) {
                $('.wrap-content-fs').last().addClass('hidden');
            }
            ;
        }, 500);

        var flightBlockItem = $('.recommended-flight-block').find('.flight-list-item');

        flightBlockItem.each(function (idx) {
            var rowSelect = $(this).find('[data-hidden-recommended]').find('.multy-column').find('.row-head-select');
            var rowSelect1 = $(this).find('[data-hidden-recommended-1]').find('.multy-column').find('.row-head-select');
            var rowSelectOneColumn = $(this).find('[data-hidden-recommended]').find('.one-column').find('.row-head-select');
            var rowSelectOneColumn1 = $(this).find('[data-hidden-recommended-1]').find('.one-column').find('.row-head-select');
            colSelectColor = rowSelect.find('.col-select');
            colSelectColor1 = rowSelect1.find('.col-select');
            colSelectColorOneColumn = rowSelectOneColumn.find('.col-select');
            colSelectColorOneColumn1 = rowSelectOneColumn1.find('.col-select');
            $(this).find('.row-head-select').find('.col-select').removeClass('economy-fs--green-1 , economy-fs--green-2 , economy-fs--green-3 , economy-fs--pey-1');
            if (flightTableData.fareFamilies[0].cabinClassName.toLowerCase() === 'business') {
                flightBlockItem.removeClass('economy-flight-bgd').addClass('business-flight-bgd');
                flightBlockItem.find('.column-trigger-animation').addClass('business-flight--blue');

                var cbType = flightBlockItem.find('.column-trigger-animation-1').find('.text-head').html();
                if (cbType == 'Premium Economy') {
                    flightBlockItem.addClass('economy-flight-bgd');
                    flightBlockItem.find('.column-trigger-animation-1').addClass('economy-flight--pey');
                } else {
                    flightBlockItem.find('.column-trigger-animation-1').addClass('business-flight--red');
                }

                colSelectColor.each(function (idx) {
                    $(this).addClass('business-fs--blue-' + idx);
                });
                colSelectColor1.each(function (idx) {
                    if (cbType == 'Premium Economy') {
                        $(this).addClass('economy-fs--pey-' + idx);
                    } else {
                        $(this).addClass('business-fs--red-' + idx);
                    }
                });

                colSelectColorOneColumn.each(function (idx) {
                    $(this).addClass('business-fs--blue-1');
                });
                colSelectColorOneColumn1.each(function (idx) {
                    if (cbType == 'Premium Economy') {
                        $(this).addClass('economy-fs--pey-1');
                    } else {
                        $(this).addClass('business-fs--red-1');
                    }
                });

            } else {
                colSelectColor.each(function (idx1) {
                    $(this).addClass('economy-fs--green-' + idx1);
                });
                colSelectColor1.each(function (idx1) {
                    $(this).addClass('economy-fs--pey-' + idx1);
                });

                colSelectColorOneColumn.each(function (idx1) {
                    $(this).addClass('economy-fs--green-1');
                });
                colSelectColorOneColumn1.each(function (idx1) {
                    $(this).addClass('economy-fs--pey-1');
                });
            }

            var blk = $(this);
            blk.find('[data-target-cabinclass]').each(function(){
                var trigger = $(this);
                if(trigger.attr('data-target-cabinclass') === 'Business') {
                    trigger.addClass('business-flight--blue');
                    blk.addClass('business-flight-bgd');
                    blk.find('[data-hidden-recommended]').addClass('business-flight--blue');
                    blk.find('[data-hidden-recommended] .row-head-select .col-select').each(function(){
                        $(this).addClass('business-fs--blue-'+ ($(this).index()+1)).removeClass('economy-fs--green-1 , economy-fs--green-2 , economy-fs--green-3 , economy-fs--pey-1');
                    });
                }
            });
        });

        var dataRecommended = $('[data-hidden-recommended]');
        var dataRecommended1 = $('[data-hidden-recommended-1]');
        dataRecommended.each(function (idxData) {
            var colSelectItem = $(this).find('.select-fare-table.hidden-mb-small').find('.col-select');
            var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
            var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
            var btnPrice = colSelectItem.find('.btn-price');
            if (btnPrice.length > 1 && !$('body').hasClass('fs-business')) {
                var buttonFirst = btnPrice.first();
                btnPrice.first().addClass('btn-price-cheapest-select');
                buttonFirst.closest('.col-select').next().find('.btn-price').addClass('btn-price-cheapest-select');

                var buttonLast = btnPrice.last();
                buttonLast.removeClass('btn-price-cheapest-select');
            }
            if (lengthColSelectItem == 2) {
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
            }
        });

        dataRecommended1.each(function (idxData) {
            var rowHeadSelect = $(this).find('.select-fare-table.hidden-mb-small').find('.row-head-select')
            var lengthColSelectItem = rowHeadSelect.find('.col-select').length;
            if (lengthColSelectItem == 2) {
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').addClass('hidden');
                rowHeadSelect.closest('.select-fare-table.hidden-mb-small').next().removeClass('hidden');
            }
        });

        labelStatusCheapest();
    }

    var wrapContent, parentFlightList, el, blockParentWrap,
        blockParentColSelect, wrapContents, wrapContentList,
        nameHead, baggage, seatSelection, earnKrisFlyer, upgrade,
        cancellation1, cancellation2, bookingChange1, bookingChange2, noShow1, noShow2, priceCurrentSelected, idx1,
        idx2, hasCodeShare = false;

    var hiddenBlockClick = function () {
        var selfHeight = el.closest('.flight-list-item').find('.recommended-table [data-wrap-flight-height]').data('wrap-flight-height');
        if (el.closest('.wrap-content-fs').index() === (p.wrapContents.length - 1)) {
            el = $('.btn-price.active');
            var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();

            var flightListItem = el.last().closest('.flight-list-item');
            var listRow = el.last().closest('.select-fare-table').find('.row-select, .row-head-select');

            var codeOrigin = flightListItem.find('.code-origin-airport').text().substring(0, 3);
            var codeDestination = flightListItem.find('.code-destination-airport').text().substring(0, 3);

            var thisCol = el.last().closest('.col-select');
            var idx = thisCol.index();
            var listContents, colRight, colLeft;
            var noteFare = $('.has-note-fare');
            listRow.each(function () {
                $(this).find('.col-select').eq(idx).addClass('col-selected');
            });
            var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
            var colSelected = listRow.find('.col-select.col-selected');
            var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
            idx2 = colSelected.find('.index-of').text();
            nameHead = el.last().attr('data-header-class');
            baggage = colSelected.find('.baggage').text();
            seatSelection = colSelected.find('.seat-selection').text();
            earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
            upgrade = colSelected.find('.upgrade').text();
            cancellation2 = colSelected.find('.cancellation').text();
            bookingChange2 = colSelected.find('.booking-change').text();
            noShow2 = colSelected.find('.no-show').text();
            listContents = summaryGroup.find('.row-select .column-left span, .row-select .column-right span');
            colLeft = summaryGroup.find('.col-select.column-left');
            colRight = summaryGroup.find('.col-select.column-right');
            summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').attr('class', className);
            summaryGroup.find('.column-right').find('.code-flight').closest('.col-select').addClass('border-fare-family');
            summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            summaryGroup.find('.col-select').find('.column-right').find('.name-header').text(nameHead);
            summaryGroup.find('.col-select').find('.column-right').parent().attr('data-class-selected', nameHead.toLowerCase());
            summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
            summaryGroup.find('.col-select').find('.column-right').find('.ttt').attr('value', eligibleRecommendationIds);
            colRight.find('.baggage').text(baggage);
            colRight.find('.seat-selection').text(seatSelection);

            // colRight.find('.seat-selection').siblings('.complimentary-note').remove();
            // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
            //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
            //   if(!colRight.find('.seat-selection').siblings('.complimentary-note').length) {
            //     colRight.find('.seat-selection').parent().append(seatSelectionExtra.clone());
            //   }
            // }

            colRight.find('.earn-krisFlyer').text(earnKrisFlyer);

            var upgradeTextSummary = colRight.find('.upgrade');
            upgradeTextSummary.text(upgrade);
            if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
                var upgradeText = colSelected.find('.upgrade');
                var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
                if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
                    upgradeTextSummary.parent().append(upgradeTooltip.clone());
                    upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
                }
            } else {
                upgradeTextSummary.siblings('em').remove();
            }

            // --- Compare 2 fare conditions
            noteFare.show();
            $('.note-fare').removeClass('hidden');
            if (idx1 < idx2) {
                colLeft.find('.cancellation').text(cancellation1);
                colLeft.find('.booking-change').text(bookingChange1);
                colLeft.find('.no-show').text(noShow1);
                // colRight.find('.cancellation').text(cancellation1 + ' *');
                // colRight.find('.booking-change').text(bookingChange1 + ' *');
                // colRight.find('.no-show').text(noShow1 + ' *');
                colRight.find('.cancellation').text(cancellation1);
                colRight.find('.booking-change').text(bookingChange1);
                colRight.find('.no-show').text(noShow1);
            } else if (idx1 > idx2) {
                // colLeft.find('.cancellation').text(cancellation2 + ' *');
                // colLeft.find('.booking-change').text(bookingChange2 + ' *');
                // colLeft.find('.no-show').text(noShow2 + ' *');
                colLeft.find('.cancellation').text(cancellation2);
                colLeft.find('.booking-change').text(bookingChange2);
                colLeft.find('.no-show').text(noShow2);
                colRight.find('.cancellation').text(cancellation2);
                colRight.find('.booking-change').text(bookingChange2);
                colRight.find('.no-show').text(noShow2);
            } else {
                noteFare.hide();
                if(!hasCodeShare) $('.note-fare').addClass('hidden');
                colLeft.find('.cancellation').text(cancellation1);
                colLeft.find('.booking-change').text(bookingChange1);
                colLeft.find('.no-show').text(noShow1);
                colRight.find('.cancellation').text(cancellation2);
                colRight.find('.booking-change').text(bookingChange2);
                colRight.find('.no-show').text(noShow2);
            }
            if ($(".wrap-content-fs").length == 1) {
                summaryGroup.find(".column-left").closest(".col-select").remove();
                colLeft.remove();
            }
            // --- End

            var eachColSelect = summaryGroup.find('.row-head-select').find('.col-select');
            eachColSelect.each(function () {
                var valuettt = eachColSelect.find('.ttt').val();
                if (valuettt === "0") {
                    summaryGroup.find('.button-group-1').find('.not-tootip').removeClass('hidden')
                    summaryGroup.find('.button-group-1').find('.text').removeClass('hidden');
                    summaryGroup.find('.button-group-1').find('.has-tootip').addClass('hidden');
                } else {
                    summaryGroup.find('.button-group-1').find('.not-tootip').addClass('hidden')
                    summaryGroup.find('.button-group-1').find('.text').addClass('hidden');
                    summaryGroup.find('.button-group-1').find('.has-tootip').removeClass('hidden');
                }
            });

            // --- Render color accordingly
            if (!String.prototype.includes) {
                String.prototype.includes = function (search, start) {
                    'use strict';
                    if (typeof start !== 'number') {
                        start = 0;
                    }

                    if (start + search.length > this.length) {
                        return false;
                    } else {
                        return this.indexOf(search, start) !== -1;
                    }
                };
            }

            listContents.each(function () {
                var _this = $(this),
                    thisText = _this.text();
                _this.removeClass('complimentary not-allowed fare-price');
                if (thisText.includes('Complimentary')) {
                    _this.addClass('complimentary');
                } else if (thisText.includes('Not allowed')) {
                    _this.addClass('not-allowed');
                } else {
                    _this.addClass('fare-price');
                }
            });
            // --- End

            var rcmidCorresponding = el.find('.rcmid-corresponding').text();
            var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
            var flightId = el.closest('.flight-list-item').find('.segment-id').text();
            summaryGroup.find('#recommendation-id').val(rcmidCorresponding);
            summaryGroup.find('#fare-family-inbound').val(fareFamilyId);
            summaryGroup.find('#flight-inbound').val(flightId.trim());

            var nameTtem2 = summaryGroup.find('.col-select').find('.column-right').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            $('.flight-search-summary-conditions').find('.tab-item-2').text(nameTtem2.text());
            $('.flight-search-summary-conditions').find('.tab-right').removeClass('has-disabled');

            if(p.wrapContents.length === 1) {
                $('.flight-search-summary-conditions').find('.tab-item-1').text(nameTtem2.text());
                $('.flight-search-summary-conditions').find('.tab-item-2').parent().addClass('hidden');
                $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');
            }

            showCorrelativeTab();

            summaryGroup.removeClass('hidden');
            slideshowpremiumeconomy();
            listRow.each(function () {
                $(this).find('.col-select').eq(idx).removeClass('col-selected');
            });

            // scrollTop func
            setTimeout(function () {
                if($('.wrap-content-fs:eq(1)').length) {
                    $('html, body').animate({
                        scrollTop: $('.wrap-content-fs:eq(1)').offset().top - $('[data-booking-summary-panel]').innerHeight() - 20
                    }, 0);
                }
            }, 750);
        } else {
            el = $('.btn-price.active');
            var eligibleRecommendationIds = el.find('.eligible-oc-recommendation-ids').text();
            var codeOrigin = el.closest('.flight-list-item').find('.code-origin-airport').text();
            var codeDestination = el.closest('.flight-list-item').find('.code-destination-airport').text();
            var listRow1 = el.closest('.select-fare-table').find('.row-select, .row-head-select');
            var thisCol = el.closest('.col-select');
            var idx = thisCol.index();
            listRow1.each(function () {
                $(this).find('.col-select').eq(idx).addClass('col-selected');
            });
            var summaryGroup = el.closest('.wrap-content-fs').closest('.wrap-content-list').next('.fare-summary-group');
            var colSelected = listRow1.find('.col-select.col-selected');
            var className = colSelected.find('.name-header-family').closest('.col-select').attr('class');
            idx1 = colSelected.find('.index-of').text();
            nameHead = el.attr('data-header-class');
            baggage = colSelected.find('.baggage').text();
            seatSelection = colSelected.find('.seat-selection').text();
            earnKrisFlyer = colSelected.find('.earn-krisFlyer').text();
            upgrade = colSelected.find('.upgrade').text();
            cancellation1 = colSelected.find('.cancellation').text();
            bookingChange1 = colSelected.find('.booking-change').text();
            noShow1 = colSelected.find('.no-show').text();
            colLeft = summaryGroup.find('.col-select.column-left');
            summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').attr('class', className);
            summaryGroup.find('.column-left').find('.code-flight').closest('.col-select').addClass('border-fare-family');

            summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            summaryGroup.find('.col-select').find('.column-left').find('.name-header').text(nameHead);
            summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('name', "ttt-" + eligibleRecommendationIds);
            summaryGroup.find('.col-select').find('.column-left').find('.ttt').attr('value', eligibleRecommendationIds);
            colLeft.find('.baggage').text(baggage);
            colLeft.find('.seat-selection').text(seatSelection);

            // colLeft.find('.seat-selection').siblings('.complimentary-note').remove();
            // if(colSelected.find('.seat-selection').siblings('.complimentary-note').length) {
            //   var seatSelectionExtra = colSelected.find('.seat-selection').siblings('.complimentary-note');
            //   if(!colLeft.find('.seat-selection').siblings('.complimentary-note').length) {
            //     colLeft.find('.seat-selection').parent().append(seatSelectionExtra.clone());
            //   }
            // }

            colLeft.find('.earn-krisFlyer').text(earnKrisFlyer);

            var upgradeTextSummary = colLeft.find('.upgrade');
            upgradeTextSummary.text(upgrade);
            if (colSelected.find('.upgrade').siblings('[data-tooltip]').length) {
                var upgradeText = colSelected.find('.upgrade');
                var upgradeTooltip = upgradeText.siblings('[data-tooltip]');
                if (!upgradeTextSummary.siblings('[data-tooltip]').length) {
                    upgradeTextSummary.parent().append(upgradeTooltip.clone());
                    upgradeTextSummary.siblings('[data-tooltip]').kTooltip();
                }
            } else {
                upgradeTextSummary.siblings('em').remove();
            }

            var fareFamilyId = el.closest('.col-select').find('.fare-family-id').text();
            var flightId = el.closest('.flight-list-item').find('.segment-id').text();
            summaryGroup.find('#fare-family-outbound').val(fareFamilyId);
            summaryGroup.find('#flight-outbound').val(flightId.trim());
            summaryGroup.addClass('hidden');
            listRow1.each(function () {
                $(this).find('.col-select').eq(idx).removeClass('col-selected');
            });

            if ($('html').hasClass('ie8')) {
                var nameTtem1 = summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text('SIN' + " - " + 'SFO');
            } else {
                var nameTtem1 = summaryGroup.find('.col-select').find('.column-left').find('.code-flight').text(codeOrigin + " - " + codeDestination);
            }
            $('.flight-search-summary-conditions').find('.tab-item-1').text(nameTtem1.text());
            $('.flight-search-summary-conditions').find('.tab-left').removeClass('has-disabled');

            // scrollTop func
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $('.wrap-content-fs:eq(0)').offset().top - $('[data-booking-summary-panel]').innerHeight() - 20
                }, 0);
            }, 750);
        }

        wrapContent = el.closest('.wrap-content-fs');
        el.closest('.upsell').addClass('hidden');
        parentFlightList = el.closest('.flight-list-item');
        // parentFlightList.find('.col-info-left').get(0).click();
        parentFlightList.find('.change-flight-item.bgd-white').find('[data-wrap-flight-height]').data('wrap-flight-height', selfHeight);
        if ($('html').hasClass('ie8')) {
            parentFlightList.children('.change-flight-item.bgd-white').removeClass('hidden');
        } else {
            parentFlightList.find('.change-flight-item.bgd-white').removeClass('hidden');
        }

        wrapContent.find('.economy-slider , .fs-status-list , .sub-logo , .monthly-view , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').addClass('hidden');
        wrapContent.find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
        wrapContent.find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
        wrapContent.find('.col-info-select').removeClass('active');
        parentFlightList.find('[data-less-details-table]').removeClass('hidden').removeClass('premium-economy');

        if (blockParentColSelect.find('.btn-price').hasClass('active') && $('.main-inner').find('.wrap-content-fs').length == 2) {
            blockParentWrap.next().removeClass('hidden');

            var dataTriggercolumn = blockParentWrap.next().find('[data-trigger-animation]');
            if (dataTriggercolumn.hasClass('has-disabled')) {
                dataTriggercolumn.removeClass('active');
                dataTriggercolumn.closest('.recommended-table').find('.airline-info').find('.less-detail').get(0).click();
                dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active , economy-flight--green , business-flight--blue');
                dataTriggercolumn.closest('.flight-list-item').find('.select-fare-block').removeClass('active, economy-flight--pey , business-flight--red');
            }

        } else {
            blockParentWrap.next().addClass('hidden');
        }
        handleLoadmore(blockParentWrap.next().find('.recommended-flight-block'), blockParentWrap.next().find('[data-loadmore]'));

        // Reset spacing
        blockParentWrap.next().find('.recommended-flight-block').removeClass('collapsed');

        handleActionSlider();
        showMoreLessDetails();
    }

    var upgradeAdditional = function (el) {
        var nextBtn = el.closest('.col-select').next().find('.btn-price');
        var nameHeaderClick = nextBtn.data('header-class');
        var priceClick = el.find('.btn-price-cheapest-colum').text();
        var priceUpgrade = nextBtn.find('.btn-price-cheapest-colum').text();
        el.closest('[data-hidden-recommended]').find('.upsell').find('.name-family').text(nameHeaderClick);
        var newPrice;
        if(isNaN(parseFloat(priceClick))) {
          newPrice = parseFloat(priceUpgrade).toLocaleString();
        }
        if(isNaN(parseFloat(priceUpgrade))) {
          newPrice = parseFloat(priceClick).toLocaleString();
        }
        if(!isNaN(parseFloat(priceUpgrade)) && !isNaN(parseFloat(priceClick))) {
          newPrice = (parseFloat(priceUpgrade) - parseFloat(priceClick)).toLocaleString();
        }

        el.closest('[data-hidden-recommended]').find('.upsell').find('.price-sgd').text(' ' + Number(newPrice).toFixed(2));
        // el.closest('[data-hidden-recommended]').find('.upsell').find('.price-sgd').text(' ' + (parseFloat(priceUpgrade) - parseFloat(priceClick)).toLocaleString(undefined, {minimumFractionDigits: 2}));
    }

    var cacheElements = function () {
        p.changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
        p.wrapContentList = $('.wrap-content-list');
        p.wrapContents = p.wrapContentList.find('.wrap-content-fs');
        p.dataTriggerAnimation = $('.wrap-content-fs').find('[data-flight-item]');
        p.wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');

        p.listItems = p.wrapContentNotFirst.find('.flight-list-item');
        p.listCol = p.listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
        p.listPrice = p.listItems.find('.btn-price').find('.list-price');
        p.oneCol = p.listItems.find('.one-column');

        p.allBtnPrice = $('.col-select').find('.btn-price');
        p.allUpsell = $('.upsell');

        p.listRcmID = p.listItems.find('.btn-price').find('.rcmid');

        p.listItems.each(function () {
            var that = $(this);
            var thisFareBlock = that.find('.select-fare-block');
            var listColInfoSel = that.find('.col-info-select');
            thisFareBlock.each(function () {
                var listRowHeadSel = $(this).find('.select-fare-table.multy-column.hidden-mb-small .row-head-select');
                var listColSelLength = listRowHeadSel.find('.col-select:not(:eq(0))').length,
                    listColSelDisabledLength = listRowHeadSel.find('.col-select.has-disabled').length;
                if (listColSelLength === listColSelDisabledLength) {
                    var idx = $(this).data('col-index');
                    listColInfoSel.each(function () {
                        if ($(this).data('trigger-animation') === idx) {
                            $(this).addClass('has-disabled');
                        }
                    });
                }
            });
        });

        p.flightColumnTrigger = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation');
        p.flightColumnTrigger1 = $('.wrap-content-fs').last().find('.flight-list-item').find('.column-trigger-animation-1');

        p.blockWrapFirst = $('.wrap-content-fs').last();
        p.colInfoLeft = p.blockWrapFirst.find('.col-info-left');
    };

    var btnPriceEvnt = function (wcl) {
        wcl.on('click.calculatePriceInbound', '.btn-price', function (e) {
            // e.stopImmediatePropagation();
            e.preventDefault();

            if (!$(this).hasClass('btn-price-cheapest-select')) {
                var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
                $(this).parents('.recommended-flight-block').addClass('collapsed');
                changeFlightItem.each(function () {
                    $(this).addClass('has-choose');
                });
            }

            var arrayPrice1, arrayPrice2;
            // wrapContentList = $('.wrap-content-list');
            // wrapContents = wrapContentList.find('.wrap-content-fs');
            // var wrapContentNotFirst = $('.wrap-content-fs:not(:eq(0))');
            var clickedBtn = $(this);

            el = $(this);
            blockParentWrap = el.closest('.wrap-content-fs');

            if ($('body').data("disable-upgrade") || $('body').hasClass('fs-business')) $(this).parents('.recommended-flight-block').addClass('collapsed');

            // var dataTriggerAnimation =  $('.wrap-content-fs').find('[data-flight-item]');
            if (!$(this).hasClass('btn-price-cheapest-select')) {
                for (var i = 0, iLen = p.dataTriggerAnimation.length; i < iLen; i++) {
                    if ($(this).hasClass('active')) {
                        $(this).closest('.flight-list-item').find('.col-info').removeClass('selected-item')
                        $(this).find('.less-detail').get(0).click();
                    }
                }
            }

            blockParentWrap.removeClass('selected-item');

            var selectedFareTypeBlock = blockParentWrap.find('.selected-fare-type');
            var selectedFareType = $(this).data('cabin-class');
            var selectedFareClass = '';
            if (selectedFareType == 'Business') {
                selectedFareClass = 'label-bus';
            } else if (selectedFareType == 'Premium Economy') {
                selectedFareClass = 'label-pey';
            } else if (selectedFareType == 'First') {
                selectedFareClass = 'label-first';
            } else {
                selectedFareClass = 'label-economy';
            }
            var span = selectedFareTypeBlock.find('span').text($(this).data('header-class')).removeClass().addClass(selectedFareClass);

            // Reset for each interaction
            $('.has-note-fare-oal').addClass('hidden');

            // Check for codeshare attribute
            var codeShare = $(this).data('codeshare');
            if(codeShare) hasCodeShare = true;

            if (hasCodeShare) {
                hasCodeShare = true;
                $('.summary-label').each(function(){
                    $(this).text($(this).text().replace(/\^/g,'') + '^');
                });
                $('.has-note-fare-oal').removeClass('hidden');
            }else {
              $('.summary-label').each(function(){
                  var label = $(this).text();
                  $(this).text(label.replace('^', ''));
              });
            }

            if (blockParentWrap.index() === 0) {
                p.wrapContentNotFirst.find('.change-flight-item.bgd-white').addClass('hidden');
                p.wrapContentNotFirst.find('.economy-slider , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
                p.wrapContentNotFirst.find('.col-select').find('.btn-price').removeClass('active');
            }

            // var listItems = wrapContentNotFirst.find('.flight-list-item');
            // var listCol = listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
            // var listPrice = listItems.find('.btn-price').find('.list-price');
            // var oneCol = listItems.find('.one-column');
            // var contentListPrice = $(this).find('.list-price').text();

            // el.closest('.select-fare-block').addClass('selected-item');
            if (el.hasClass('column-left')) {
                el.closest('.flight-list-item').find('.column-trigger-animation').addClass('selected-item');
            } else if (el.hasClass('column-right')) {
                el.closest('.flight-list-item').find('.column-trigger-animation-1').addClass('selected-item');
            }

            upgradeAdditional(el);
            // blockParentWrap = el.closest('.wrap-content-fs');
            blockParentColSelect = blockParentWrap.find('.col-select');
            blockParentColSelect.find('.btn-price').removeClass('active').find('.ie-copy').text('Select');

            p.allBtnPrice.removeClass('active');
            p.allUpsell.addClass('hidden');
            $(this).addClass('active');
            $(this).find('.ie-copy').text('Selected');
            if ($(this).hasClass('btn-price-cheapest-select')) {
                if ($("body").data("disable-upgrade")) {
                    hiddenBlockClick();
                } else {
                    var upsell = $(this).closest('[data-hidden-recommended]').find('.upsell');
                    if (blockParentWrap.index() === 0) {
                        upsell.removeClass('hidden');
                    } else {
                        hiddenBlockClick();
                    }
                    setTimeout(function () {
                        upsell.find('input[name="btn-keep-selection"]').focus();
                    }, 100);
                }
            } else {
                hiddenBlockClick();
            }

            if (blockParentWrap.index() !== 0) {
                return;
            }

            var listItems = p.wrapContentNotFirst.find('.flight-list-item');
            var listCol = listItems.find('.row-select, .row-head-select').find('.col-select:not(:eq(0))');
            var oneCol = listItems.find('.one-column');

            listCol.addClass('has-disabled');
            oneCol.each(function () {
                if (!$(this).hasClass('hidden')) {
                    var thisIdx = $(this).closest('.select-fare-block').data('col-index');
                    var thisColSelect = $(this).closest('.flight-list-item').find('[data-trigger-animation]');

                    thisColSelect.each(function () {
                        if ($(this).data('trigger-animation') === thisIdx) {
                            $(this).addClass('has-disabled');
                        }
                    });
                }
            });

            p.listItems.each(function () {
                var that = $(this);
                var thisFareBlock = $(this).find('.select-fare-block');
                var listColInfoSel = $(this).find('.col-info-select');
                thisFareBlock.each(function () {
                    var listRowHeadSel = $(this).find('.select-fare-table.multy-column.hidden-mb-small .row-head-select');
                    var listColSelLength = listRowHeadSel.find('.col-select:not(:eq(0))').length,
                        listColSelDisabledLength = listRowHeadSel.find('.col-select.has-disabled').length;
                    if (listColSelLength === listColSelDisabledLength) {
                        var idx = $(this).data('col-index');
                        listColInfoSel.each(function () {
                            if ($(this).data('trigger-animation') === idx) {
                                $(this).addClass('has-disabled');
                            }
                        });
                    }
                });
            });

            var blockWrapLast =  $('.wrap-content-fs').last();
            var flightColumnTrigger = blockWrapLast.find('.flight-list-item').find('.column-trigger-animation');
            var flightColumnTrigger1 = blockWrapLast.find('.flight-list-item').find('.column-trigger-animation-1');
            
            if (blockParentWrap.index() === 0 && $('.wrap-content-fs').length > 1) {
                var arrPriceCheapest = [];
                
                var colInfoLeft = blockWrapLast.find('.col-info-left');
                colInfoLeft.find('.head-col').find('.label-status').remove();

                var priceCurrentSelected = el.find('.btn-price-cheapest-colum').text();
                var orgPriceCurrentSelected = el.attr('data-originalprice');

                flightColumnTrigger.each(function (idx) {
                    $(this).find('.price-cheapest-outbound').text(priceCurrentSelected);
                    $(this).closest('.flight-list-item').find('[data-hidden-recommended]').find('.price-cheapest-colum').text(priceCurrentSelected);
                    $(this).addClass('has-disabled');
                });

                colInfoLeft.each(function (idexLabel) {
                    arrPriceCheapest.push($(this).find('.price').find('.compare-number').eq(0).text());
                });

                arrPriceCheapest.sort(function (a, b) {
                    return parseFloat(a) - parseFloat(b);
                });

                colInfoLeft.each(function () {
                    if (parseFloat($(this).find('.flight-price').find('.compare-number').eq(0).text()) === parseFloat(arrPriceCheapest[0])) {
                        $(this).find('.head-col').append('<span class="label-status anim-all"></span');
                    }
                })

                flightColumnTrigger1.each(function (idx1) {
                    $(this).find('.price-cheapest-outbound-1').text(priceCurrentSelected);
                    $(this).closest('.flight-list-item').find('[data-hidden-recommended-1]').find('.price-cheapest-colum').text(priceCurrentSelected);
                    $(this).addClass('has-disabled');
                });

                var colSelect = blockWrapLast.find('[data-hidden-recommended]').find('.col-select').find('.btn-price');
                var colSelect1 = blockWrapLast.find('[data-hidden-recommended-1]').find('.col-select').find('.btn-price');

                colSelect.each(function (idxColSelect) {
                    if(typeof $(this).attr('data-rcmid-out') !== 'undefined') {
                      var arrayList = $(this).attr('data-rcmid-out').split(' ');
                      var arrayPriceList = $(this).attr('data-rcmid-pricelist').split(' ');
                      var curArrayList = el.attr('data-rcmid-out').split(' ');

                      for (var i = 0; i < curArrayList.length; i++) {
                        for (var j = 0; j < arrayList.length; j++) {
                          if(arrayList[j] === curArrayList[i]) {
                            var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
                            var thisCol = $(this).closest('.col-select');
                            var idx = thisCol.index();
                            listRow.each(function() {
                              $(this).find('.col-select').eq(idx).removeClass('has-disabled');
                            });

                            var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation].column-trigger-animation');

                            thisColSelectOneCol.removeClass('has-disabled');

                            $(this).find('.btn-price-cheapest-colum').text(arrayPriceList[j]);
                            $(this).find('.rcmid-corresponding').text(curArrayList[i]);
                            $(this).closest('.btn-price').attr('data-outbound-totalamt', el.attr('data-totalamt'));

                            if(typeof $(this).closest('.btn-price').attr('data-totalamt-obj') !== 'undefined') {
                              var totalAmtObj = JSON.parse($(this).closest('.btn-price').attr('data-totalamt-obj'));
                              var combinableTotalAmt = totalAmtObj[curArrayList[i]+''];

                              $(this).closest('.btn-price').attr('data-totalamt', combinableTotalAmt);
                              $(this).closest('.btn-price').attr('data-cmbnble-rcmid', curArrayList[i]);
                            }
                          }
                        }
                      }
                    }

                    var priceCheapestColumn = $(this).closest('[data-hidden-recommended]').find('.price-cheapest-colum').text();
                    var priceCheapestColumnCabin = $(this).find('.btn-price-cheapest-colum').text();

                    var totalPriceBtnColumn = parseFloat(priceCheapestColumnCabin) - parseFloat(priceCheapestColumn);
                    var toFixedPrice = totalPriceBtnColumn.toFixed(2);

                    var sym = '+ ';
                    if (totalPriceBtnColumn < 0) {
                        // totalPriceBtnColumn = 0;
                        sym = '- ';
                    }

                    var orgSym = '+ ';
                    var orgPriceBtn, totalOrgPriceBtnColumn;
                    if(orgPriceCurrentSelected.length > 0) {
                        var orgPriceBtn = $(this).prev();
                        var thisOrgPrice = $(this).attr('data-originalprice');
                        totalOrgPriceBtnColumn = parseFloat(orgPriceCurrentSelected.replace(/,/g, '')) - parseFloat(thisOrgPrice.replace(/,/g, ''));
                        if (totalOrgPriceBtnColumn < 0) {
                            orgSym = '- ';
                        }
                    }

                    var priceStyleUnitSmall = toFixedPrice.indexOf(".");
                    if (priceCheapestColumnCabin !== '') {
                      $(this).find('.btn-price-cheapest').text('');
                      $(this).find('.unit-small').text('');

                      var prc = totalPriceBtnColumn.toFixed(2);
                      var decPt = prc.indexOf(".");
                      var dec = prc.slice(decPt);
                      var curPrice = Math.abs(prc.slice(0, decPt)).toLocaleString() + dec;

                        var colPrice;
                        if(orgPriceCurrentSelected.length > 0) {
                            colPrice = $(this).prev().prev();

                            var oPrc = totalOrgPriceBtnColumn.toFixed(2);
                            var oDecPt = oPrc.indexOf(".");
                            var oDec = oPrc.slice(oDecPt);
                            var oCurPrice = Math.abs(oPrc.slice(0, oDecPt)).toLocaleString() + oDec;

                            orgPriceBtn.text(orgSym + $(this).attr('data-currency') + ' ' + oCurPrice);
                        }else {
                            colPrice = $(this).prev();
                        }
                        colPrice.text(sym + $(this).attr('data-currency') + ' ' + curPrice);

                      $(this).attr('data-price-segment-after', totalPriceBtnColumn.toFixed(2).slice(0, priceStyleUnitSmall));
                      $(this).attr('data-computed-totalamt', ( parseFloat($(this).attr('data-totalamt')) - parseFloat($(this).attr('data-outbound-totalamt')) ));
                    }
                });

                colSelect1.each(function (idxColSelect1) {
                    if(typeof $(this).attr('data-rcmid-out') !== 'undefined') {
                      var arrayList = $(this).attr('data-rcmid-out').split(' ');
                      var arrayPriceList = $(this).attr('data-rcmid-pricelist').split(' ');
                      var curArrayList = el.attr('data-rcmid-out').split(' ');

                      for (var i = 0; i < curArrayList.length; i++) {
                        for (var j = 0; j < arrayList.length; j++) {
                          if(arrayList[j] === curArrayList[i]) {
                            var listRow = $(this).closest('.select-fare-table').find('.row-select, .row-head-select');
                            var thisCol = $(this).closest('.col-select');
                            var idx = thisCol.index();
                            listRow.each(function() {
                              $(this).find('.col-select').eq(idx).removeClass('has-disabled');
                            });

                            var thisColSelectOneCol = $(this).closest('.flight-list-item').find('[data-trigger-animation].column-trigger-animation-1');

                            thisColSelectOneCol.removeClass('has-disabled');

                            $(this).find('.btn-price-cheapest-colum').text(arrayPriceList[j]);
                            $(this).find('.rcmid-corresponding').text(curArrayList[i]);
                            $(this).closest('.btn-price').attr('data-outbound-totalamt', el.attr('data-totalamt'));

                            if(typeof $(this).closest('.btn-price').attr('data-totalamt-obj') !== 'undefined') {
                              var totalAmtObj = JSON.parse($(this).closest('.btn-price').attr('data-totalamt-obj'));
                              var combinableTotalAmt = totalAmtObj[curArrayList[i]+''];

                              $(this).closest('.btn-price').attr('data-totalamt', combinableTotalAmt);
                              $(this).closest('.btn-price').attr('data-cmbnble-rcmid', curArrayList[i]);
                            }
                          }
                        }
                      }
                    }

                    var priceCheapestColumn1 = $(this).closest('[data-hidden-recommended-1]').find('.price-cheapest-colum').text();
                    var priceCheapestColumnCabin1 = $(this).find('.btn-price-cheapest-colum').text();

                    var totalPriceBtnColumn1 = parseFloat(priceCheapestColumnCabin1) - parseFloat(priceCheapestColumn1);
                    var toFixedPrice1 = totalPriceBtnColumn1.toFixed(2);

                    var sym = '+ ';
                    if (totalPriceBtnColumn1 < 0) {
                        // totalPriceBtnColumn1 = 0;
                        sym = '- ';
                    }

                    var orgSym = '+ ';
                    var orgPriceBtn, totalOrgPriceBtnColumn;
                    if(orgPriceCurrentSelected.length > 0) {
                        var orgPriceBtn = $(this).prev();
                        var thisOrgPrice = $(this).attr('data-originalprice');
                        totalOrgPriceBtnColumn = parseFloat(orgPriceCurrentSelected.replace(/,/g, '')) - parseFloat(thisOrgPrice.replace(/,/g, ''));
                        if (totalOrgPriceBtnColumn < 0) {
                            orgSym = '- ';
                        }
                    }

                    var priceStyleUnitSmall1 = toFixedPrice1.indexOf(".");
                    if (priceCheapestColumnCabin1 !== '') {
                      $(this).find('.btn-price-cheapest-1').text('');
                      $(this).find('.unit-small').text('');

                      var prc = totalPriceBtnColumn1.toFixed(2);
                      var decPt = prc.indexOf(".");
                      var dec = prc.slice(decPt);
                      var curPrice = Math.abs(prc.slice(0, decPt)).toLocaleString() + dec;

                      var colPrice;
                        if(orgPriceCurrentSelected.length > 0) {
                            colPrice = $(this).prev().prev();

                            var oPrc = totalOrgPriceBtnColumn.toFixed(2);
                            var oDecPt = oPrc.indexOf(".");
                            var oDec = oPrc.slice(oDecPt);
                            var oCurPrice = Math.abs(oPrc.slice(0, oDecPt)).toLocaleString() + oDec;

                            orgPriceBtn.text(orgSym + $(this).attr('data-currency') + ' ' + oCurPrice);
                        }else {
                            colPrice = $(this).prev();
                        }
                        colPrice.text(sym + $(this).attr('data-currency') + ' ' + curPrice);

                      $(this).attr('data-price-segment-after', totalPriceBtnColumn1.toFixed(2).slice(0, priceStyleUnitSmall1));
                      $(this).attr('data-computed-totalamt', ( parseFloat($(this).attr('data-totalamt')) - parseFloat($(this).attr('data-outbound-totalamt')) ));
                    }
                });

                flightColumnTrigger.each(function (idx) {
                    // var priceCheapest = $(this).find('.price-cheapest-colum').text();
                    var priceSelectedOutBound = $(this).find('.price-cheapest-outbound').text();
                    var cheapestPriceBtn = $(this).parent().parent().find('[data-hidden-recommended]').find('.row-select.row-footer-select .col-select').find('.btn-price .btn-price-cheapest-colum');
                    var cheapestPrice = [];

                    cheapestPriceBtn.each(function(){
                      if(!$(this).parent().parent().hasClass('has-disabled')) cheapestPrice.push($(this).text());
                    });

                    cheapestPrice.sort(function (a, b) {
                        return parseFloat(a) - parseFloat(b);
                    });

                    var priceCheapest = cheapestPrice[0];

                    if (priceCheapest && priceSelectedOutBound) {
                        var totalPriceCabin = parseFloat(priceCheapest) - parseFloat(priceSelectedOutBound);

                        var sym = '+ ';

                        if (totalPriceCabin < 0) {
                            // totalPriceCabin = 0;
                            sym = '- ';
                        }
                        var triggerAnimation = $(this);
                        var priceStyleSmall = totalPriceCabin.toFixed(2).indexOf(".");

                        triggerAnimation.find('.price').text(sym + Math.abs(parseFloat(totalPriceCabin.toFixed(2).slice(0, priceStyleSmall))).toLocaleString());
                        triggerAnimation.find('.price').append("<small></small>");
                        triggerAnimation.find('.price').find('small').text(totalPriceCabin.toFixed(2).slice(priceStyleSmall));

                        triggerAnimation.find('.price').append('<span class="compare-number hidden">' + totalPriceCabin + '</span>');
                    }
                });

                flightColumnTrigger1.each(function (idx1) {
                    // var priceCheapest1 = $(this).find('.price-cheapest-colum').text();
                    var priceSelectedOutBound1 = priceCurrentSelected;
                    var cheapestPriceBtn = $(this).parent().parent().find('[data-hidden-recommended-1]').find('.row-select.row-footer-select .col-select').find('.btn-price .btn-price-cheapest-colum');
                    var cheapestPrice = [];

                    cheapestPriceBtn.each(function(){
                      if(!$(this).parent().parent().hasClass('has-disabled')) cheapestPrice.push($(this).text());
                    });

                    cheapestPrice.sort(function (a, b) {
                        return parseFloat(a) - parseFloat(b);
                    });

                    var priceCheapest1;

                    // Make sure disabled triggers are computed
                    if(cheapestPrice.length === 0) {
                       priceCheapest1 = cheapestPriceBtn.eq(0).text();
                    }else {
                       priceCheapest1 = cheapestPrice[0];
                    }

                    if (priceCheapest1 && priceSelectedOutBound1) {
                        var totalPriceCabin1 = parseFloat(priceCheapest1) - parseFloat(priceSelectedOutBound1);

                        var sym = '+ ';
                        if (totalPriceCabin1 < 0) {
                            // totalPriceCabin1 = 0;
                            sym = '- ';
                        }

                        var triggerAnimation1 = $(this);
                        var priceStyleSmall1 = totalPriceCabin1.toFixed(2).indexOf(".");
                        triggerAnimation1.find('.price').text(sym + Math.abs(parseFloat(totalPriceCabin1.toFixed(2).slice(0, priceStyleSmall1))).toLocaleString());
                        triggerAnimation1.find('.price').append("<small></small>");
                        triggerAnimation1.find('.price').find('small').text(totalPriceCabin1.toFixed(2).slice(priceStyleSmall1));
                    }
                    // }
                });
            }

        });

    };

    $(document).on('click', '.group-btn > #btn-keep-selection', function (e) {
        e.preventDefault();
        el = $(this);

        $(this).parents('.recommended-flight-block').addClass('collapsed');

        if (!$(this).hasClass('btn-price-cheapest-select')) {
            var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
            changeFlightItem.each(function () {
                $(this).addClass('has-choose');
            });
        }
        hiddenBlockClick();
    });

    $(document).on('click', '.group-btn > #btn-upgrade', function (e) {
        e.preventDefault();
        var btnPriceActive = $(this).closest('.flight-list-item').find('.col-select').find('.btn-price.active');
        var parentBtn = btnPriceActive.closest('.col-select');
        var prevUpsell = $(this).closest('.upsell').prev().find('.col-select');
        var btnEqActive = prevUpsell.find('.btn-price');
        // btnEqActive.eq(1).addClass('item-next-active');
        // var btnPriceNext = parentBtn.next().find('.btn-price.item-next-active');
        var btnPriceNext = parentBtn.next().find('.btn-price');
        if (btnPriceNext.length) {
            btnPriceActive.removeClass('active');
            btnPriceActive.find('.ie-copy').text('Select');
            btnPriceNext.addClass('active');
            btnPriceNext.find('.ie-copy').text('Selected');

            btnPriceNext.trigger('click');
            var priceActive = $(this).closest('[data-hidden-recommended]').find('.btn-price.active');
            var textPriceActive = priceActive.find('.header-family').text();
            var priceNext = priceActive.find('.btn-price-cheapest-colum').text();
            var priceCurrent = $(this).closest('.upsell').find('.price-selected').text();
            var minus = parseFloat(priceNext) - parseFloat(priceCurrent);
            $(this).closest('.upsell').find('.name-family').text(textPriceActive);
            if (btnPriceNext.length) {
                $(this).closest('.upsell').find('.price-sgd').text(" SGD " + minus.toLocaleString());
            }
            $(this).closest('.upsell').find('.price-selected ').text(priceNext);
            hiddenBlockClick();
        }
    });

    $(document).on('click', '.button-group-1 > .button-change', function (e) {
        var wrapContentBlock = $(this).closest('.wrap-content-fs');

        var buttonSelected = $(this).closest('.recommended-flight-block').find('[data-trigger-animation].selected-item');
        if (buttonSelected.length) {
            buttonSelected.parent().parent().find('.btn-price').removeClass('active').find('.ie-copy').text('Select');
            buttonSelected.get(0).click();
        }

        setTimeout(function () {
            $(window).scrollTop(wrapContentBlock.offset().top - $('[data-booking-summary-panel]').innerHeight() - 20);
        }, 500);

        $(this).closest('.main-inner').find('.fare-summary-group').addClass('hidden');
        wrapContentBlock.find('.select-fare-block.selected-item').addClass('active');
        if ($('.main-inner').find('.wrap-content-fs').length) {
            wrapContentBlock.find('.change-flight-item.bgd-white').addClass('hidden');
            wrapContentBlock.find('.economy-slider , .fs-status-list , .monthly-view , .sub-logo , .flight-search-filter-economy , .loadmore-block , .recommended-table , .head-recommended').removeClass('hidden');
            wrapContentBlock.find('.col-select').find('.btn-price').removeClass('active');

            if (wrapContentBlock.index() === 0) {
                wrapContentBlock.next().addClass('hidden');
                wrapContentBlock.next().find('.col-select').find('.btn-price').removeClass('active');

                // reset codeshare
                hasCodeShare = false;
            } else {
                $('.flight-search-summary-conditions').find('.tab-right').addClass('has-disabled');
                $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
            }
            handleLoadmore(wrapContentBlock.find('.recommended-flight-block'), wrapContentBlock.find('[data-loadmore]'));
            showMoreLessDetails();
            handleActionSlider();
        }
        $('form.fare-summary-group').addClass('hidden');

        var changeFlightItem = $('.wrap-content-fs').find('.change-flight-item.bgd-white');
        changeFlightItem.each(function () {
            if (!changeFlightItem.is(':visible')) {
                $(this).removeClass('has-choose');
            } else {
                $(this).addClass('has-choose');
                $(this).parents('.recommended-flight-block').addClass('collapsed');
            }
        });

        if($(this).parents('.recommended-flight-block').hasClass('collapsed')) $(this).parents('.recommended-flight-block').removeClass('collapsed');

    });

    // show 2nd tab when click link right
    function showCorrelativeTab() {
        $('.table-fare-summary.hidden-mb-small').on('click', '.link-left', function () {
            $('.flight-search-summary-conditions').find('.tab-left').trigger('click');
        });
        $('.table-fare-summary.hidden-mb-small').on('click', '.link-right', function () {
            $('.flight-search-summary-conditions').find('.tab-right').trigger('click');
        });
    }

    var workflowTable = function (isRenderTemplate) {
        tableLoadPage(isRenderTemplate);
        var priceTable = $('.recommended-table').find('.price');
        var flightListItem = $('.flight-list-item');
        var flightList = $('.wrap-content-fs').first().find('.flight-list-item');
        priceTable.each(function (idx) {
            var contentInner = $(this).text();
            if ($.trim(contentInner) === "") {
                var parentPrice = $(this).closest('.col-info-select');
                parentPrice.addClass('not-available');
                parentPrice.find('.flight-price').remove();
                parentPrice.find('span.not-available').empty().text('Not available');
            }
        });
        flightListItem.each(function (idx) {
            var flightStationItem = $(this).find('.flight-station-item');
            flightStationItem.each(function (idx) {
                if (!$(this).is(':last-child')) {
                    $(this).find('.less-detail').remove();
                }
            });
        });
    }
    var getTemplateFlightTable = function (el, data1, flightData, index, isFilter) {
        var loadmoreBlock = el.siblings('[data-loadmore]');
        var miniUrl = 'ajax/minifare-conditions.json';
        if ($('body').hasClass('fs-sk-mixed-rbd-biz')) {
            miniUrl = 'ajax/sk-minifare-conditions.json';
        }

        if ($('body').hasClass('fs-sk-mixed-rbd')) {
            miniUrl = 'ajax/sk-minifare-conditions-pey-biz-syd-maa-new.json';
        }

        if ($('body').hasClass('fs-economy-rbd')) {
            miniUrl = 'ajax/sk-minifare-conditions-pey-biz-syd-maa-new.json';
        }

        if ($('body').hasClass('lcf-json')) {
            miniUrl = 'ajax/292-fareconditions.json';
        }

        if ($('body').hasClass('ccc-json')) {
            miniUrl = 'ajax/cheapest-cross-cabin-fare-conditions.json';
        }

        if ($('body').hasClass('mpxbbl-json')) {
            miniUrl = 'ajax/fs-sk-economy-multiple-pax-ff-conditions.json';
        }

        if(SIA.URLParamParser && SIA.URLParamParser.getURLParams('miniUrl')) miniUrl = 'ajax/'+ SIA.URLParamParser.getURLParams('miniUrl') + '.json';

        if (miniFareData) {
            $.get(global.config.url.fsEconomyFlightTable, function (data) {
                var template = window._.template(data, {
                    data: data1,
                    familyData: miniFareData,
                    flight: flightData ? flightData : data1.flights[index],
                    flightIdx: index,
                    labels: saar5.l4.sk.chooseflight
                });

                el.find('.flight-list-item').remove();
                el.append($(template));
                el.find('.flight-list-item:gt(4)').addClass('hidden');

                if (isFilter && index === 1) {
                    $(clickedBtn).trigger('click.calculatePriceInbound');
                }

                workflowTable(isFilter);
                selectFlightAnimation();
                showMoreLessDetails();
                initPopup();
                // init tooltip
                // var BlockLogoChange = $('.flight-station--inner').find('.airline-detail');
                // if ($('html').hasClass('ie8')) {
                //     BlockLogoChange.each(function () {
                //         $(this).find('img').prop('src', 'images/transparent.png');
                //     });
                // }
                if ($('[data-tooltip]')) {
                    $('[data-tooltip]').kTooltip();
                }
                handleLoadmore(el, loadmoreBlock);

                var segmentsLength = el.find('.flight-list-item').length;

                if (!segmentsLength) {
                    el.addClass('hidden');
                    el.siblings('.no-result-filter').removeClass('hidden');
                } else {
                    el.siblings('.no-result-filter').addClass('hidden');
                    el.removeClass('hidden');
                }

                if(segmentsLength === 1) {
                    el.addClass('single-result');
                }

            })
        } else {
            $.ajax({
                url: miniUrl,
                type: SIA.global.config.ajaxMethod,
                dataType: 'json',
                success: function (response) {
                    miniFareData = response;
                    $.get(global.config.url.fsEconomyFlightTable, function (data) {
                        var template = window._.template(data, {
                            data: data1,
                            familyData: response,
                            flight: flightData ? flightData : data1.flights[index],
                            flightIdx: index,
                            labels: saar5.l4.sk.chooseflight
                        });

                        el.find('.flight-list-item').remove();
                        el.append($(template));
                        el.find('.flight-list-item:gt(4)').addClass('hidden');

                        if (isFilter && index === 1) {
                            $(clickedBtn).trigger('click.calculatePriceInbound');
                        }

                        workflowTable(isFilter);
                        selectFlightAnimation();
                        showMoreLessDetails();
                        initPopup();
                        // init tooltip
                        if ($('[data-tooltip]')) {
                            $('[data-tooltip]').kTooltip();
                        }
                        handleLoadmore(el, loadmoreBlock);

                        var segmentsLength = el.find('.flight-list-item').length;

                        if (!segmentsLength) {
                            el.addClass('hidden');
                            el.siblings('.no-result-filter').removeClass('hidden');
                        } else {
                            el.siblings('.no-result-filter').addClass('hidden');
                            el.removeClass('hidden');
                        }

                        if(segmentsLength === 1) {
							el.addClass('single-result');
						}

                    })
                }
            });
        }
    }

    var renderFlightTable = function (data1) {
        var flightBlock = $('.recommended-flight-block');

        flightBlock.each(function (index) {
            var self = $(this);
            if (index === 0) {
                getTemplateFlightTable(self, data1, null, index);
            } else {
                setTimeout(function () {
                    getTemplateFlightTable(self, data1, null, index);
                }, 2000);
            }
        })
    }

    var resetFilter = function () {
        var btnReset = $('[data-reset-filter]');

        btnReset.each(function () {
            $(this).off('click.resetFilter').on('click.resetFilter', function (e) {
                e.preventDefault();
                var filterBlock = $(this).closest('.no-result-filter').siblings('[data-flight-filter]'),
                    listCheckbox = filterBlock.find('input[type="checkbox"]').not("[disabled]"),
                    listRangeSlider = filterBlock.find('[data-range-slider]'),
                    checkboxReset = filterBlock.find('input[type="checkbox"]:disabled');

                listCheckbox.each(function () {
                    $(this).prop('checked', false);
                })

                listRangeSlider.each(function () {
                    var min = $(this).data('min'),
                        max = $(this).data('max');
                    $(this).slider("option", "values", [min, max]);
                })

                checkboxReset.prop('checked', true).trigger('change.resetFilter');

            })
        })
    }

    var renderCombinationsJson = function () {
        var templateBookingPayment;
        var appendDiv;
        if (!$('body').hasClass('fs-economy')) {
            appendDiv = $('.combinations-json');
        } else {
            appendDiv = $('.fs-economy').find('.top-main-inner');
        }

        p.appendDiv = appendDiv;

        var combinationsJson = function (data1) {
            if (!$('body').hasClass('fs-economy')) {
                $.get(global.config.url.combinationsJsonTpl, function (data) {
                    var template = window._.template(data, {
                        data: data1
                    });
                    templateBookingPayment = $(template);
                    appendDiv.append(templateBookingPayment);
                });
            } else {
                $.get(global.config.url.fsEconomy, function (data) {
                    var filterArr = getFilterData(data1.flights);
                    var template = window._.template(data, {
                        data: data1,
                        filterArr: filterArr
                    });
                    templateBookingPayment = $(template);
                    appendDiv.append(templateBookingPayment);
                    renderFlightTable(data1);
                    showHideFilters();
                    handleActionSlider();
                    sliderRange();
                    filterFlights(data1);
                    resetFilter(data1.flights);
                    SIA.flightEconomySort();

                    cacheElements();

                    var urlPage;
                    if ($('body').hasClass('sk-ut-flight-search-a')) {
                        urlPage = 'sk-ut-passenger-details-a.html';
                    } else if ($('body').hasClass('sk-ut-flight-search-b')) {
                        urlPage = 'sk-ut-passenger-details-b.html';
                    }

                    if (urlPage) {
                        $('form[name="flight-search-summary"]').attr('action', urlPage);
                    }

                });
            }
        };

        var url;
        if ($('body').hasClass('fs-economy-response-page')) {
            url = "ajax/flightsearch-response.json";
        }
        if ($('body').hasClass('fs-economy-response-new-page') || $('body').hasClass('fs-economy')) {
            url = "ajax/sin-sfo-30 JUNE-2017-Most-updated.json";
        }
        if ($('body').hasClass('fs-business')) {
            url = "ajax/sin-sfo-business-first.json";
        }
        if ($('body').hasClass('fs-economy-scoot')) {
            url = "ajax/sin-sfo-economy-scoot.json";
        }
        if ($('body').hasClass('fs-economy-two-column-premium-economy')) {
            url = "ajax/sin-sfo-tow-column-premium-economy.json";
        }
        if ($('body').hasClass('fs-economy-four-column')) {
            url = "ajax/sin-sfo-economy-four-column.json";
        }
        if ($('body').hasClass('fs-economy-sin-maa-roundtrip-page')) {
            url = "ajax/flight-search-SIN-MAA-Roundtrip-2A1C1I.json";
        }

        if ($('body').hasClass('fs-sk-mixed-rbd-biz')) {
            url = "ajax/sk-bus-first-syd-maa.json";
        }
        if ($('body').hasClass('fs-economy-rbd')) {
            url = "ajax/sk-pey-biz-syd-maa-new.json";
        }

        if ($('body').hasClass('lcf-json')) {
            url = "ajax/292.json";
        }

        if ($('body').hasClass('ccc-json')) {
            url = "ajax/cheapest-cross-cabin.json";
        }

        if ($('body').hasClass('mpxbbl-json')) {
            url = "ajax/fs-sk-economy-multiple-pax-results.json";
        }

        if(SIA.URLParamParser && SIA.URLParamParser.getURLParams('url')) url = 'ajax/'+ SIA.URLParamParser.getURLParams('url') + '.json';

        $.ajax({
            url: url,
            type: SIA.global.config.ajaxMethod,
            dataType: 'json',
            success: function (response) {
                flightTableData = response.response;
                var data1 = response.response;
                if (flightTableData.discountApplied) {
                    renderDiscountedPrompt(flightTableData.discountApplied);
                }
                if (checkIfCabinClass(flightTableData.fareFamilies, "business")) {
                    $("body").addClass("fs-business");
                }
                combinationsJson(data1);
            }
        });
    };

    var checkIfCabinClass = function(fareFamilies, cabinType) {
        var cabinClassBusiness = [
            "F",
            "J"
        ];
        switch(cabinType) {
            case "business":
                return cabinClassBusiness.indexOf(fareFamilies[0].cabinClass) != -1;
            default:
                return false;
        }
    };

    var renderDiscountedPrompt = function(discountedMessage) {
        var alertPrompt = $(".checkin-alert");
        alertPrompt.removeClass("hidden");
        alertPrompt.find(".alert__message").html(discountedMessage);
    };

    var popup1 = $('.popup--flights-details-sf');
    var popup2 = $('.flight-search-summary-conditions');
    $(document).on('click', '.flights-details-sf', function (e) {
        e.preventDefault();
        $(this).trigger('dblclick');
        popup1.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('dblclick', '.flights-details-sf', function () {
        popup1.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.trigger-summary-of-fare-condition', function (e) {
        e.preventDefault();
        $(this).trigger('dblclick');
        if ($(this).hasClass('link-left')) {
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').addClass('active');
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').removeClass('active');
        } else {
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-left').removeClass('active');
            $('.flight-search-summary-conditions').find('.tab').find('.tab-item.tab-right').addClass('active');
        }
        popup2.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('dblclick', '.trigger-summary-of-fare-condition', function () {
        popup2.Popup('show');
        $(this).closest('body').find('.fadeInOverlay').addClass('overlay');
    });
    $(document).on('click', '.popup__close', function (event) {
        popup1.Popup('hide');
        popup2.Popup('hide');
        event.preventDefault();
    });

    if ($('body').hasClass('fs-economy-page')) {
        renderCombinationsJson();

        setTimeout(function () {
            btnPriceEvnt(p.appendDiv.find('.wrap-content-list'));
        }, 2000);
    }
};

SIA.flightEconomySort = function() {
    jQuery.fn.shift = [].shift;
    jQuery.fn.unshift = [].unshift;
    
    var global = SIA.global;
    var listEl =  [];

    var sortFunctionListener = function() {
        var flightSearchFilterEl = $(".flight-search-filter-economy");
        flightSearchFilterEl.each(function() {
            var self = $(this);
            var sortTriggers = self.find(".sort-filter").find("a");
            sortTriggers.on("click.sortTrigger", function(e) {
                e.preventDefault();
                changeActive(sortTriggers, $(this));
            });
        });
    };

    var cloneArray = function(clone) {
        return JSON.parse(JSON.stringify(clone));
    }

    var changeActive = function(triggers, triggered) {
        if (triggered.hasClass("sort-active")) {
            return;
        }
        var values = getListElements(triggered);
        var sortBySelected = triggered.text().toLowerCase();
        
        if (sortBySelected == "price") {
            values = sortPrice(values);
            values = sortDurationFn(values, ["price"])
            values = sortDepartureFn(values, ["price", "duration"]);
            values = sortArrivalFn(values, ["price", "duration", "departTimeString"]);
        } else if (sortBySelected == "travel duration") {
            values = sortDuration(values);
            values = sortPriceFn(values, ["duration"]);
            values = sortArrivalFn(values, ["duration", "price"]);
            values = sortDepartureFn(values, ["duration", "price", "arrivalTimeString"]);
        } else if (sortBySelected == "arrival time") {
            values = sortDepartureArrival(values, "arrivalTime", true);
            values = sortDurationFn(values, ["arrivalTimeString"]);
            values = sortPriceFn(values, ["arrivalTimeString", "duration"]);
            values = sortDepartureFn(values, ["arrivalTimeString", "duration", "departTime"]);
        } else if (sortBySelected == "departure time") {
            values = sortDepartureArrival(values, "departTime");
            values = sortDurationFn(values, ["departTimeString"]);
            values = sortPriceFn(values, ["departTimeString", "duration"]);
            values = sortArrivalFn(values, ["departTimeString", "duration", "price"]);
        }
        
        reArrangeElements(triggered, values);
        _.each(triggers, function(trigger) {
            trigger = $(trigger);
            if (trigger.is(triggered)) {
                trigger.addClass("sort-active");
            } else {
                if (trigger.hasClass("sort-active")) {
                    trigger.removeClass("sort-active");
                }
            }
        });
    };

    var findSameNext = function(list, criteria) {
        var idx = 0;
        var same = [list[idx]];
        while(list[++idx]) {
            var conditions = [];

            _.each(criteria, function(by) {
                conditions.push(list[idx][by] == same[0][by]);
            });

            if (conditions.indexOf(false) == -1) {
                same.push(list[idx]);
            } else {
                break;
            }
        }

        return same;
    };

    var checkIfElementHasHidden = function(elementList) {
        var hiddenCount = 0;
        elementList = $(elementList);
        elementList.each(function() {
            var $this = $(this);
            if ($this.hasClass("hidden")) {
                hiddenCount ++;
                $this.removeClass("hidden");
            }
        });
        return {
            count: hiddenCount
        }
    };

    var reArrangeElements = function(el, values) {
        var newElementArrangement = [];
        var parentWrapper = el.closest(".wrap-content-fs");
        var renderAfterChange = parentWrapper.find(".render-after");
        _.each(values, function(value) {
            newElementArrangement.push(listEl[value.$id]);
        });
        var result = checkIfElementHasHidden(newElementArrangement);
        newElementArrangement = $(newElementArrangement);
        for (var i = 1 ; i <= result.count ; i ++ ) {
            var idx = (newElementArrangement.length - i);
            $(newElementArrangement[idx]).addClass("hidden");
        }
        newElementArrangement.insertAfter(renderAfterChange);
    };

    var getListElements = function(el) {
        var parentWrapper = el.closest(".wrap-content-fs");
        listEl = parentWrapper.find(".flight-list-item");
        var data = [];
        listEl.each(function(idx) {
            var elData = scrapeValues($(this));
            elData.$id = idx;
            data.push(elData);
        });

        return data;
    };

    var scrapeValues = function(el) {
        var data = {
            prices: []
        };
        data.duration = el.find("[data-sort-duration]").data("sort-duration");
        el.find("[data-sort-price]").each(function() {
            var $this = $(this);
            var key = $this.data("sort-price").toLowerCase().replace(" ", "_");
            var priceValue = $this.text().trim().replace(",", "").replace(" ", "");
            data.prices.push(parseFloat(priceValue));
        });
        var legData = el.find("[data-sort-leg]");
        data.departTime = getDepartureArrivalTime(legData, "sort-departure");
        data.arrivalTime = getDepartureArrivalTime(legData, "sort-arrival");

        return data;
    };

    var sortPriceFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            _.each(sortPrice(batch), function(batchItem) {
                multiSort.push(batchItem);
            });
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortDurationFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            _.each(sortDuration(batch), function(batchItem) {
                multiSort.push(batchItem);
            });
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortDepartureFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            _.each(sortDepartureArrival(batch, "departTime"), function(batchItem) {
                multiSort.push(batchItem);
            });
            values.splice(0, batch.length);
        }

        return multiSort;
    };

    var sortArrivalFn = function(list, by) {
        var multiSort = [];
        var values = cloneArray(list);
        while(values.length) {
            var batch = findSameNext(values, by);
            _.each(sortDepartureArrival(batch, "arrivalTime", true), function(batchItem) {
                multiSort.push(batchItem);
            });
            values.splice(0, batch.length);
        }

        return multiSort;
    } ;

    var sortPrice = function(list) {
        return _.chain(list)
                .map(function(item) {
                    item.price = _.sortBy(item.prices)[0];
                    return item;
                })
                .sortBy("price")
                .value();
    };

    var sortDuration = function(list) {
        return _.sortBy(list, "duration");
    };

    var sortDepartureArrival = function(list, by, isArrival) {
        var newKey = by + "String";
        
        return _.chain(list)
                .map(function(item) {
                    var timeHolder = item[by];
                    var index = isArrival ? timeHolder.length - 1 : 0;
                    // item[newKey] = [
                    //     ((timeHolder[index].hours <= 9) ? "0" : "") + timeHolder[index].hours, 
                    //     ((timeHolder[index].minutes <= 9) ? "0" : "") + timeHolder[index].minutes, 
                    //     ((timeHolder[index].seconds <= 9) ? "0" : "") + timeHolder[index].seconds
                    // ].join(":");
                    item[newKey] = timeHolder[index];
                    return item
                })
                .sortBy(newKey)
                .value();
    };

    var getDepartureArrivalTime = function(el, by) {
        var timeArray = [];
        el.each(function() {
            var $this = $(this);
            var dateTime = $this.data(by);
            // var dateTime = $this.data(by).split(" ");
            // var dateElement = _.map(dateTime[0].split("-"), function(i) { return parseInt(i) });
            // var timeElement = _.map(dateTime[1].split(":"), function(i) { return parseInt(i) });
            // var date = new Date(
            //     dateElement[0],
            //     dateElement[1],
            //     dateElement[2],
            //     timeElement[0],
            //     timeElement[1],
            //     timeElement[2]
            // );
            // timeArray.push({
            //     hours: date.getHours(), 
            //     minutes: date.getMinutes(), 
            //     seconds: date.getSeconds()
            // });
            timeArray.push(dateTime);
        });
        return timeArray;
    };

    function parseISO8601(dateStringInRange) {
        var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d).*Z\s*$/,
            date = new Date(NaN), month,
            parts = isoExp.exec(dateStringInRange);
        if (parts) {
            month = +parts[2];
            date.setUTCFullYear(parts[1], month - 1, parts[3]);
            date.setUTCHours(parts[4]);
            date.setUTCMinutes(parts[5]);
            date.setUTCSeconds(parts[6]);
            if(month != date.getMonth() + 1) {
                date.setTime(NaN);
            }
        }
        return date;
    }

    var init = function() {
        sortFunctionListener();
    };

    init();
};

// Only modify if toLocaleString adds decimal places
if (/\D/.test((1).toLocaleString())) {
  Number.prototype.toLocaleString = (function() {
    // Store built-in toLocaleString
    var _toLocale = Number.prototype.toLocaleString;
    // Work out the decimal separator
    var _sep = /\./.test((1.1).toLocaleString())? '.' : ',';
    // Regular expression to trim decimal places
    var re = new RegExp( '\\' + _sep + '\\d+$');
    return function() {
      // If number is an integer, call built–in function and trim decimal places
      // if they're added
      if (parseInt(this) == this) {
        return _toLocale.call(this).replace(re,'');
      }
      // Otherwise, just convert to locale
      return _toLocale.call(this);
    }
  }());
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}