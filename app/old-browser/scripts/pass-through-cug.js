var SIA = SIA || {};

if (typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

SIA.PassThroughAAM = function () {
    var global = SIA.global;
    var promotionObject = {};
    promotionObject.json = globalJson.promotionCugData;
    promotionObject.passThroughPromotionDetailTpl = '<div class="promotion-item__inner">\
    <div class="promotion-item__content">\
     <h4>Promotion details</h4>\
     <table class="promotion-details-table">\
      <tbody>\
       <tr>\
        <td data-th="">Travel duration</td>\
        <td data-th=""><%- travelDurationStart %> — <%- travelDurationEnd %></td>\
       </tr>\
       <tr>\
        <td data-th="">Book by</td>\
        <td data-th=""><%- bookBy %></td>\
       </tr>\
       <tr class="promo-detail border-bottom table-row-3">\
        <td data-th="">Advance purchase</td>\
        <td data-th="" class="promo-detail-advance-purchase"></td>\
       </tr>\
       <tr class="promo-detail table-row-4">\
        <td data-th="">Minimum stay</td>\
        <td data-th=""><%- minStay %> <%- minimumStayRadio %></td>\
       </tr>\
       <tr class="promo-detail border-bottom table-row-5">\
        <td data-th="">Maximum stay</td>\
        <td data-th=""><%- maxStay %> <%- maximumStayRadio %></td>\
       </tr>\
       <tr class="promo-detail table-row-6">\
        <td data-th="">Minimum passengers</td>\
        <td data-th=""><%- minPaxNo %> passengers</td>\
       </tr>\
       <tr class="view-full-fare-condition">\
        <td>\
         <a href="#accordion--fare-conditions" class="link-4 accordion--fare-conditions-trigger">\
          <em class="ico-point-r"></em>View full fare conditions</a>\
        </td>\
       </tr>\
      </tbody>\
     </table>\
    </div>\
   </div>';

    promotionObject.passThroughFareDealBannerTpl = '<div data-tablet-bg="30% top" class="full-banner--img fare-deal-banner-img"><img class="imgBanner" src="" alt="Faredeal Banner" longdesc="img-desc.html"/>\
   </div>';

    promotionObject.passThroughFareConditionsListTpl = '<li class="fare-condition-list"><div class="list-title">Valid flights</div>\
   <ul class="fare-conditions-unordered-list">Availability of this fare deal is based on flight departure times.\
     <li>\
       <span>Valid outbound flights:</span> <%- validOutboundFlights %></li>\
     <li>\
       <span>Valid inbound flights:</span> <%- validInboundFlights %></li>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Travel period</div>\
   <ul class="fare-conditions-unordered-list">\
     <li>\
       <span>Outbound travel period:</span> <%- startDate %> to <%- endDate %></li>\
     <li>\
       <span>Travel completion date:</span> <%- travelCompletionDate %></li>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">KrisFlyer</div>\
   <ul>\
    <% _.each(mileAccruablefactor, function(item, idx) { %>\
      <li><%- item %></li>\
    <% }) %>\
   </ul>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Itinerary change</div>\
   <p><%- itineraryChangeDesc %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Cancellation / Refund</div>\
   <p><%- cancellationOrRefundDesc %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Other conditions</div>\
  <p class="other-conditions"><%- otherconditions %></p>\
  </li>\
  <li class="fare-condition-list"><div class="list-title">Special conditions</div>\
   <p><%- specialConditions %></p>\
  </li>';

    var init = function () {
        renderTPLPromoDetailFareDealBanner();
        renderTPLFareConditions();
        accordionAnchorTrigger();
    };

    var renderTPLPromoDetailFareDealBanner = function () {
        var contentBodyPassThroughPromoDetail = $(".pass-through-promotion-detail-container");
        var contentBodyPassThroughFareDealBanner = $(".fare-deal-banner-container");
        var fareDeals = promotionObject.json.fareDeals;
        var promoDestinationArray = [];
        // var promoData;
        // _.each(promotionObject.json, function (promo) {
        // promoData = promo;
        _.each(fareDeals.origin, function (originName) {
            promoDestinationArray.push(_.map(originName.destination, function (destinationNameParam) {
                return destinationNameParam.translatedDestinationName;
            }));
        });
        // });
        var destinationNames = promoDestinationArray.toString().split(",").join(", ");
        var passThroughPromotionDetailTplRendered = $(_.template(promotionObject.passThroughPromotionDetailTpl, {
            'travelDurationStart': fareDeals.travelDurationStart,
            'travelDurationEnd': fareDeals.travelDurationEnd,
            'bookBy': fareDeals.bookBy,
            'advancePurchase': fareDeals.advancePurchase,
            'minStay': fareDeals.minimumStay,
            'minimumStayRadio': fareDeals.minimumStaytype,
            'maxStay': fareDeals.maximumStay,
            'maximumStayRadio': fareDeals.maximumStayType,
            'minPaxNo': fareDeals.minPax,
            'cabinClass': fareDeals.cabinClass,
            'eligibility': fareDeals.eligibility,
            'translatedDestinationName': destinationNames
        }));
        contentBodyPassThroughPromoDetail.append(passThroughPromotionDetailTplRendered);

        var passThroughFareDealBannerTplRendered = $(_.template(promotionObject.passThroughFareDealBannerTpl, {
            'AAMbannerImage': fareDeals.promotionBannerImg
            // 'promoCode': fareDeals.promoCode,
            // 'AAMbannerTitle': fareDeals.AAMbannerTitle,
            // 'AAMbannerdesc': fareDeals.AAMbannerdesc
        }));
        contentBodyPassThroughFareDealBanner.append(passThroughFareDealBannerTplRendered);
        $(".from-destination-price").text("FROM " + fareDeals.currency + " " + fareDeals.amount);
        $(".airport-code-from").text(fareDeals.origin.saleOriginCode);
        $(".airport-code-to").text(fareDeals.destination.saleDestinationCode);
        $(".fare-deals-flight-price").text(fareDeals.currency + " " + fareDeals.amount);
        $(".outobund-inbound-banner-flight").text(fareDeals.origin.translatedOriginName + " to " + fareDeals.destination.translatedDestinationName);
        $(".booking-widget-search-flights-from-to").text("Search flights from " + fareDeals.origin.saleOriginName + " to " + fareDeals.destination.saleDestinationName);
        $("img.imgBanner").attr('src',fareDeals.promotionBannerImg);
        $("td.trip-cabin").text(fareDeals.cabinClass+" "+fareDeals.tripType);
    };

    var renderTPLFareConditions = function () {
        var contentBodyPassThroughFareConditionsList = $('.fare-condition-ordered-list-main');
        var fareDeals = promotionObject.json.fareDeals;
        var outbound = fareDeals.outboundContainer;
        var inbound = fareDeals.inboundContainer;
        var fareDealsConditions = fareDeals.conditions;
        var isUpgradable = (fareDealsConditions.mileageUpgradable == "yes") ? true : false;
        var mileAccruablefactor = _.map(fareDealsConditions.mileAccruablefactor.split(","), function(i) { return i.trim() });
        var outboundDates = outbound.outboundDates[0];

        if (!isUpgradable) {
          mileAccruablefactor = [mileAccruablefactor.shift()];
        }
        
        var passThroughFareConditionsListTplTplRendered = $(_.template(promotionObject.passThroughFareConditionsListTpl, {
            'validOutboundFlights': outbound.outboundFlights,
            'validInboundFlights': inbound.inboundFlights,
            'startDate': outboundDates.startDate,
            'endDate': outboundDates.endDate,
            'travelCompletionDate': fareDeals.travelCompletionDate,
            'mileAccruablefactor': mileAccruablefactor,
            'cancellationOrRefundDesc': fareDealsConditions.cancellation,
            'specialConditions': fareDealsConditions.specialConditions,
            'itineraryChangeDesc': fareDealsConditions.rebooking,
            'otherconditions': fareDealsConditions.otherConditions
        }));
        contentBodyPassThroughFareConditionsList.append(passThroughFareConditionsListTplTplRendered);
        $('p.other-conditions').html(fareDealsConditions.otherConditions.replace('Note:','<span class="bold-text">Note:</span>'));
        $('.promo-detail-advance-purchase').text(fareDeals.advancePurchase + " days");
    };

    var accordionAnchorTrigger = function() {
        $("a.accordion--fare-conditions-trigger").off().on("click", function(evt) {
            var accordion = $("#accordion--fare-conditions").find("[data-accordion-trigger]");
            if (!accordion.hasClass("active")) {
                evt.preventDefault();
                accordion.trigger("click");
            }
        });
    };

    var pub = {
        init: init
    };

    return pub;
}();

var waitForUnderscore = function () {
    setTimeout(function () {
        if (typeof _ == 'undefined') {
            waitForUnderscore();
        } else {
            SIA.PassThroughAAM.init();
        }
    }, 100);
};

$(function () {
    typeof _ == 'undefined' ? waitForUnderscore() : SIA.PassThroughAAM.init();
});
