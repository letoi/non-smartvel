/**
 * @name SIA
 * @description Define global CIB Booking Summary functions
 * @version 1.0
 */
SIA.CIBBookingSummary = function(){
	var global = SIA.global;
	var config = global.config;
	var vars = global.vars;
	var body = vars.body;
	var win = $(window);
	var container = $('#container');
	var flightSearch = $('.flights__searchs');
	var flightUpgrades = $('.flights-upgrade');
	var bookingSummaryWidget = $('.booking-summary');
	var bookingSummaryControl = bookingSummaryWidget.find('.booking-summary__heading .booking-summary__control');
	var bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');
	var bookingSummaryOffset = bookingSummaryWidget.offset();
	var bookingGroupContent = bookingSummaryContent.find('[data-flight-cost]');

	// var fare = 0, taxes = 0, carrier = 0;
	var addonSubtotal = 0;
	var grandTotal = 0;
	var addons = [];
	var kkMiles = 0;
	var kkMilesRest = 0;
  var agodaTotal = 0;
	var infoFare = bookingSummaryWidget.find('[data-fare] span').last();
	var infoTaxes = bookingSummaryWidget.find('[data-taxes] span').last();
	var infoCarrier = bookingSummaryWidget.find('[data-carrier] span').last();
	var infoFlightSubtotal = bookingSummaryWidget.find('[data-subtotal] span').last();
	var infoGrandTotal = bookingSummaryWidget.find('[data-grandtotal]');
	var infoAddonList = bookingSummaryWidget.find('[data-addons]');
	var infoKKMilesRest = bookingSummaryWidget.find('[data-krisflyer-miles-rest]');
	// var formAddon = $('.form--addons');
	// var tableBaggages = $('.table-baggage');
	var tooltipPopup = $('.add-ons-booking-tooltip');
	var footer = $('footer.footer');
	var mainInner = $('.main-inner');
	// var passengerDetailsPage = $('.passenger-details-page');
	// var seatsmapPage = $('.seatsmap-page');
	var passengerCount = bookingSummaryWidget.find('.number-passengers');
	var infoPayableWithMiles = bookingSummaryWidget.find('[data-krisflyer-miles]');
	var totalToBePaid = $('[data-headtotal], [data-tobepaid]');
  var infoAgodaTotal = bookingSummaryWidget.find('[data-agoda-total]');

	if(!bookingSummaryWidget.length){
		if(!$('.mp-add-ons-page').length){
			return;
		}
	}

	// This function uses remove format Number
	var unformatNumber = function(number) {
		number = window.accounting.unformat(number);
		return parseFloat(number);
	};

	// This function uses format Number
	var formatNumber = function(number, fraction) {
		return window.accounting.formatNumber(number, (typeof(fraction) !== 'undefined') ? fraction : 2, ',', '.');
	};

	// Set the number of people booking
	var setPassengerCount = function() {
		if(globalJson.bookingSummary) {
			var bsinfo = globalJson.bookingSummary.bookingSummary;
			var html = '';
			if(bsinfo.adultCount) {
				html += bsinfo.adultCount + ' Adult' + (bsinfo.adultCount > 1 ? 's' : '');
			}
			if(bsinfo.childCount) {
				html += (html.length ? ', ' : '') + bsinfo.childCount + (bsinfo.childCount > 1 ? ' Children' : ' Child');
			}
			if(bsinfo.infantCount) {
				html += (html.length ? ', ' : '') + bsinfo.infantCount + ' Infant' + (bsinfo.infantCount > 1 ? 's' : '');
			}
			passengerCount.html(html);
		}
	};

	// Set state for radio button
	var preselectFlights = function() {
		if(globalJson.bookingSummary) {
			var flightInfo = globalJson.bookingSummary.fareAvailablityVO;
			if(flightInfo.dafaults) {
				for (var i = flightInfo.dafaults.length - 1; i >= 0; i--) {
					if(flightInfo.dafaults[i] !== null) {
						flightSearch.filter('[data-flight="' + (i + 1) + '"]').find('input[value="' + flightInfo.dafaults[i] + '"]').prop('checked', true).trigger('change.select-flight');
						$('[name="selectedFlightIdDetails[' + i + ']"]').val(flightInfo.dafaults[i]);
					}
				}
			}

			var radioEls = flightSearch.find('input:radio');

			//Check for enable flights
			if(body.is('.flight-select-interim-page')) {
        var url = $('.form-flight-search').find('fieldset').data('table-flight');
        flightInfo.messages = {};
        $.ajax({
          url: url,
          type: SIA.global.config.ajaxMethod,
          dataType: 'json',
          success: function(reponse) {
            var data = reponse.response,
                recommendations = data.recommendations;
            flightInfo.messages = {};
            for(var i = 0; i < recommendations.length; i++) {
              for(var j = 0; j < recommendations[i].segmentBounds.length; j++) {
                for(var k = 0; k < recommendations[i].segmentBounds[j].segments.length; k++) {
                  flightInfo.messages['O' + recommendations[i].segmentBounds[j].segments[k].segmentID + recommendations[i].fareFamily] = "";
                }
              }
            }
            radioEls.each(function() {
              if(!$.isEmptyObject(flightInfo.messages)) {
                var flightId = $(this).val();
                if(typeof(flightInfo.messages[flightId]) === 'undefined') {
                  $(this).prop('disabled', true);
                  $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled').attr('tabindex', '-1');
                  $(this).parents('td').attr('data-package-disabled', 'true');
                }
                else {
                  $(this).prop('disabled', false);
                  $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
                  $(this).parents('td').attr('data-package-disabled', 'false');
                }
              }
              else {
                $(this).prop('disabled', false);
                $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
                $(this).parents('td').attr('data-package-disabled', 'false');
              }
            });

            if($.isEmptyObject(flightInfo.messages)) {
              var firstFare = flightSearch.eq(0).find('input:checked:first');
              var isWaitlisted = firstFare.data('waitlisted');

              flightSearch.not(':first').find(isWaitlisted ? 'input[data-waitlisted="true"]' : 'input[data-waitlisted="false"]').each(function() {
                $(this).prop('disabled', true);
                $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
                $(this).parents('td').attr('data-package-disabled', 'true');
              });
            }
          }
        });
      } else {
        radioEls.each(function() {
          if(!$.isEmptyObject(flightInfo.messages)) {
            var flightId = $(this).val();
            if(typeof(flightInfo.messages[flightId]) === 'undefined') {
              $(this).prop('disabled', true);
              $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled').attr('tabindex', '-1');
              $(this).parents('td').attr('data-package-disabled', 'true');
            }
            else {
              $(this).prop('disabled', false);
              $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
              $(this).parents('td').attr('data-package-disabled', 'false');
            }
          }
          else {
            $(this).prop('disabled', false);
            $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
            $(this).parents('td').attr('data-package-disabled', 'false');
          }
        });

        if($.isEmptyObject(flightInfo.messages)) {
          var firstFare = flightSearch.eq(0).find('input:checked:first');
          var isWaitlisted = firstFare.data('waitlisted');

          flightSearch.not(':first').find(isWaitlisted ? 'input[data-waitlisted="true"]' : 'input[data-waitlisted="false"]').each(function() {
            $(this).prop('disabled', true);
            $(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
            $(this).parents('td').attr('data-package-disabled', 'true');
          });
        }
      }

			radioEls.trigger('change.flightTableBorder');
		}
	};

	// Set booking sumary flight infomation
	var setBookingSummaryFlightInfo = function() {
		var bsinfo = globalJson.bookingSummary.bookingSummary;
		var flightsInfo = bookingSummaryWidget.find('[data-flight-info]');
		var secureFareInfo = globalJson.bookingSummary.tttVO;

		flightsInfo.empty();
		var flightsInfoHtml = '';
		for (var i = 0; i < bsinfo.flight.length; i++) {
			flightsInfoHtml += '<div class="flights-info">';
			flightsInfoHtml += '	<div class="flights-info-heading">';
			flightsInfoHtml += '		<h4>Flight ' + (i + 1) + '</h4>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.date + ' - ' + bsinfo.flight[i].flightSegments[0].deparure.time + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '	<div class="flights-info__country">';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.airportCode + '</span>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[bsinfo.flight[i].flightSegments.length - 1].arrival.airportCode + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '</div>';
		}
		flightsInfo.html(flightsInfoHtml);

		infoFare.text(formatNumber(bsinfo.fareTotal));
		infoTaxes.text(formatNumber(bsinfo.taxTotal));
		infoCarrier.text(formatNumber(bsinfo.surchargeTotal));
		infoFlightSubtotal.text('Sgd ' + formatNumber(bsinfo.fareSubTotal));

		infoAddonList.empty();
		for (var i = 0; i < addons.length; i++) {
			var html = '';
			html += '<li class="addon--item"><span>' + addons[i].title + '</span>';
			html += '<span class="price">' + formatNumber(addons[i].price) + '</span>';
			// html += '<a href="javascript:void(0);" class="delete-btn"><em class="ico-close-round-fill"></em></a>';
			html += '</li>';
			var li = $(html);
			// li.find('.delete-btn').data('element', addons[i].element);
			infoAddonList.append(li);
		}

		var htmlEnd = '<li class="sub-total"><span>Sub-total</span><span class="price">Sgd ' + formatNumber(addonSubtotal) + '</span></li>';
		var liTotal = $(htmlEnd);
		var stringTotalPaid = '';
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		var payWithKfMiles = true;

		var totalPaidWithConvert = '';
		var convertPriceChb = $('.payment-currency').find('[data-toggler] input');
		var isConvertPriceChecked = convertPriceChb.length && convertPriceChb.is(':checked');
		var isPaymentPage = body.hasClass('payments-page');

		infoAddonList.append(liTotal);
    infoAgodaTotal.text('Sgd ' + formatNumber(agodaTotal));
		infoGrandTotal.text('Sgd ' + formatNumber(grandTotal));
		infoPayableWithMiles.text('Sgd ' + formatNumber(kkMiles));
		infoKKMilesRest.text('Sgd ' + formatNumber(kkMilesRest));

		if (kkMilesRest > 0) {
			stringTotalPaid += '<span class="unit">Sgd ' + formatNumber(kkMilesRest) + '</span>';

			if (isPaymentPage) {
				totalPaidWithConvert += '<span class="unit">Sgd ' + formatNumber(kkMilesRest) +
					'</span><span class="payment-currency-text' +
					(isConvertPriceChecked ? '' : ' hidden') + '">' +
					L10n.payment.convertText.format(formatNumber(kkMilesRest)) + '</span>';
			}
		}

		if(payWithKfMilesCheckbox.length && !payWithKfMilesCheckbox.is(':checked') &&
			!globalJson.bookingSummary.bookingSummary.milesAllocated) {
			payWithKfMiles = false;
		}

		if(kkMiles > 0 && payWithKfMiles) {
			// stringTotalPaid += '&nbsp;<small>+</small></span>';
			// stringTotalPaid += '<span class="miles">' + formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';

			if (!isPaymentPage) {
				stringTotalPaid += '<span class="miles">' +
					(kkMilesRest > 0 ? '<small>+</small>&nbsp;' : '') +
					formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';
			}
			else {
				totalPaidWithConvert += '<span class="miles">' +
					(kkMilesRest > 0 ? '<small>+</small>&nbsp;' : '') +
					formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';
			}
		}

		// totalToBePaid.html(stringTotalPaid);
		totalToBePaid.eq(0).html(stringTotalPaid);
		totalToBePaid.eq(1).html(isPaymentPage ? totalPaidWithConvert : stringTotalPaid);

		//secure fare
    var secureFare = '';
    var totalSecureFare = '';
    if(secureFareInfo.tttFare) {
      secureFare += '<div class="flights-cost" data-tabindex="true">' +
                    '<h4 class="flights-cost-title">' +
                      '<span class="text-left">SECURE FARE</span>' +
                      '<span class="text-right">&nbsp;</span>' +
                    '</h4>';

      secureFare += '<ul class="flights-cost__details">' +
                      '<li data-hold-fare="true">' +
                        '<span>Hold this fare</span>' +
                        '<span class="values">' +
                          formatNumber(secureFareInfo.tttFare, 2) +
                        '</span>' +
                      '</li>' +
                    '</ul></div>';

      $(secureFare).appendTo(bookingGroupContent);

      totalSecureFare += '<div class="grand-total">' +
                            '<p class="total-title" data-tabindex="true" data-aria-text="Secure this fare at">SECURE THIS FARE AT</p>' +
                            '<p class="total-info" data-tabindex="true" >' +
                              '<span class="unit">' + secureFareInfo.tttCurrency + ' ' + formatNumber(secureFareInfo.tttFare, 2) + '</span>' +
                            '</p>' +
                            '<p class="info-charge" data-tabindex="true">Includes taxes and surcharges</p' +
                          '</div>';

      $(totalSecureFare).appendTo(bookingGroupContent);
    }

		if (body.hasClass('payments-page') && isConvertPriceChecked) {
			convertPriceChb.trigger('change.exchange');
		}
	};

	// Attach a onchange to checkbox
	var payWithKfMilesChange = function() {
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		payWithKfMilesCheckbox.off('change.pay-with-miles').on('change.pay-with-miles', function() {
			setBookingSummaryFlightInfo();
		});
	};

	payWithKfMilesChange();

	// Caculate flight prices
	var calculateFlightPrices = function() {
		var bsinfo = globalJson.bookingSummary.bookingSummary;

		// if($('.add-ons-page').length) {
		// 	addonSubtotal = 0;
		// 	for (var i = 0; i < addons.length; i++) {
		// 		addonSubtotal += addons[i].price;
		// 	}
		// 	grandTotal = bsinfo.fareSubTotal + addonSubtotal;
		// 	kkMilesRest = grandTotal;
		// }
		// else {
		addonSubtotal = bsinfo.addonSubTotal;
		grandTotal = bsinfo.grandTotal;
		kkMilesRest = bsinfo.costPayableByCash;
		// }

		kkMiles = bsinfo.costPayableByMiles;
    var agodaTotal1, agodaTotal2;
    _.forEach(bsinfo.commonAddons, function(value, i){
      if(value.type === "Hotel") {
        if(value.hotelTaxAmount){
          agodaTotal1 = value.amount + value.hotelTaxAmount;
        }else{
          agodaTotal1 = value.amount;
        }
      }
      if(value.type === "Car Rental") {
        if(value.hotelTaxAmount){
          agodaTotal2 = value.amount + value.hotelTaxAmount;
        }else{
          agodaTotal2 = value.amount;
        }
      }
    });
    agodaTotal = agodaTotal1 + agodaTotal2;
	};

	// Print summary of fare conditions
	var printFareCondition = function(res) {
		if($('.flight-select-page').length) {
			var fareCondition = $('.summary-fare__conditions');
			var html = '';
			for(var i = 0; i < res.fareFamilyCondition.length; i++) {
				html += '<li>';
				if(res.fareFamilyCondition[i].isAllowed) {
					html += '<em class="ico-check-thick"></em>';
				}
				else {
					html += '<em class="ico-close"></em>';
				}
				html += res.fareFamilyCondition[i].description;
				html += '</li>';
			}
			fareCondition.html(html);
		}
	};

	// Print addons
	var printAddons = function(res) {
		if(res.bookingSummary.commonAddons) {
			for(var i = 0; i < res.bookingSummary.commonAddons.length; i++) {
				if(!checkExistAddon(res.bookingSummary.commonAddons[i].type)) {
					addons.push({
						title: res.bookingSummary.commonAddons[i].type,
						price: res.bookingSummary.commonAddons[i].amount,
						element: $()
					});
				}
			}
		}
	};

	// Render popup details
	var renderPopupDetails = function(res) {
		var isAddonPage = body.is('.add-ons-payment-page'),
        templateUrl = '';

    templateUrl = isAddonPage === true ? config.url.cibBookingSummaryDetailsPopupTemplateAddOn : config.url.cibBookingSummaryDetailsPopupTemplate;
    $.get(templateUrl, function(data) {
			if(!$('.add-ons-page, .payments-page').length) {
				res.bookingSummary.commonAddons = [];
			}
			data = data.replace(/td>\s+<td/g,'td><td');
			var template = window._.template(data, {
				data: res,
				confirmationPage: $('.cib-confirmation-page').length
			});
			var popupContent = $('.popup--flights-details .popup__content');
			popupContent.children(':not(.popup__close)').remove();
			popupContent.append(template);
			popupContent.find('[data-need-format]').each(function() {
				var number = unformatNumber($(this).text());
				$(this).text(formatNumber(number, $(this).data('need-format')));
			});
		});
	};

	var renderPopupRefund = function(res, callback) {

		$.get(config.url.cibBookingSummaryRefundPopup, function(data) {
			var template = window._.template(data, {
				data: res,
			});
			var popupContent = $('.popup--flights-details .popup__content');
			popupContent.children(':not(.popup__close)').remove();
			popupContent.append(template);

			if(typeof(callback) === 'function') {
				callback();
			}
		});
	};

	var renderSecurePopupDetails = function(res) {
    $.get(config.url.cibBookingSummarySfDetailsPopupTemplate, function(data) {
      if(!$('.add-ons-page, .payments-page').length) {
        res.bookingSummary.commonAddons = [];
      }
      var template = _.template(data, {
        data: res,
        confirmationPage: $('.cib-confirmation-page').length
      });
      var popupContent = $('.popup--flights-details-sf .popup__content');
      popupContent.children(':not(.popup__close)').remove();
      popupContent.append(template);
      popupContent.find('[data-need-format]').each(function() {
        var number = unformatNumber($(this).text());
        $(this).text(formatNumber(number, $(this).data('need-format')));
      });

      popupContent
        .find('.flights--detail span')
        .off('click.getFlightInfo')
        .on('click.getFlightInfo', function() {
          var self = $(this);
          var details = self.siblings('.details');
          if (details.is(':empty')) {
            $.ajax({
              url: config.url.flightSearchFareFlightInfoJSON,
              type: config.ajaxMethod,
              dataType: 'json',
              data: {
                flightNumber: self.parent().data('flight-number'),
                carrierCode: self.parent().data('carrier-code'),
                date: self.parent().data('date'),
                origin: self.parent().data('origin')
              },
              success: function(res) {
                self.children('em').toggleClass('ico-point-d ico-point-u');
                details.toggleClass('hidden');
                var html = '';
                html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
                for (var ft in res.flyingTimes) {
                  html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
                }
                details.html(html);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                if (textStatus !== 'abort') {
                  window.alert(errorThrown);
                }
              },
              beforeSend: function() {
                self.children('em').addClass('hidden');
                self.children('.loading').removeClass('hidden');
              },
              complete: function() {
                self.children('.loading').addClass('hidden');
                self.children('em').removeClass('hidden');
              }
            });
          } else {
            self.children('em').toggleClass('ico-point-d ico-point-u');
            if (details.is('.hidden')) {
              details.hide().removeClass('hidden').slideDown(400);
            }
            else {
              details.slideUp(400, function() {
                details.addClass('hidden');
              });
            }
          }
        });
    });
  };

	var BSPAjax;
	// Call Ajax for Booking sumary
	var callBSPAjax = function(onchange, extData, callback, radioEl) {
		var data = {
		};
		$.extend(data, extData);
		if(onchange) {
			flightSearch.each(function(i, it) {
				var selectedFlightId = $(it).find('input:radio:checked').first().val();
				if(selectedFlightId) {
					selectedFlightId = i + '|' + selectedFlightId.substring(2);
					data['selectedFlightIdDetails[' + i + ']'] = selectedFlightId;
				}
			});
		}
		if(BSPAjax) {
			BSPAjax.abort();
		}
		BSPAjax = $.ajax({
			url: onchange ? config.url.cibFlightSelectOnChange : config.url.cibFlightSelect,
			type: config.ajaxMethod,
			data: data,
			dataType: 'json',
			success: function(res) {
				globalJson.bookingSummary = res;

				if($('.payments-page').length) {
					var triggerCostPayableByCash = new jQuery.Event('change.costPayableByCash');
					triggerCostPayableByCash.cash = res.bookingSummary.costPayableByCash;
					bookingSummaryWidget.trigger(triggerCostPayableByCash);
				}

				printAddons(res);
				setPassengerCount();
				preselectFlights();
				calculateFlightPrices();
				setBookingSummaryFlightInfo();
				printFareCondition(res);
				if(!$('[data-booking-summary-panel]').hasClass('bsp-booking-summary')){
					renderPopupDetails(res);
				}
				renderSecurePopupDetails(res);

				if ($('.ssh-additional-page').length && SIA.sshAdditional &&
					SIA.sshAdditional.setAdditional) {
					SIA.sshAdditional.setAdditional();
				}

				$(window).trigger('finishLoadingBS.initSelection');

				if(typeof(callback) === 'function') {
					callback();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				if(textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			},
			beforeSend: function() {
				!body.hasClass('ssh-review-cancel-page') &&
				passengerCount
				.add(totalToBePaid)
				.add(infoFare)
				.add(infoTaxes)
				.add(infoCarrier)
				.add(infoFlightSubtotal)
				.add(infoGrandTotal)
				.add(infoKKMilesRest)
				.add(infoPayableWithMiles)
				.text('...');
				infoAddonList.empty();

				if(body.hasClass('flight-select-page') && onchange && radioEl) {
					var bspInfo = bookingSummaryWidget.find('.booking-summary__info');
					var radioEls = flightSearch.find('input:radio').not(radioEl).not(':checked');

					bspInfo.find('.total-cost').addClass('hidden');
					bspInfo.find('.fare-notice').addClass('hidden');
					bspInfo.siblings('.loading--medium-2').removeClass('hidden');

					// add css to fix issue: change label color on old-browser
					radioEls.addClass('disabled').prop('disabled', true).siblings('label').css('color', '#999');
				}
				else {
					SIA.preloader.show();
				}
			},
			complete: function() {
				if (body.hasClass('flight-select-page') && onchange) {
					var bspInfo = bookingSummaryWidget.find('.booking-summary__info');
					// flightSearch.find('label').css('color', '');
					var radioEls = flightSearch.find('input:radio').not(radioEl).not(':checked');
					radioEls.removeClass('disabled').prop('disabled', false).siblings('label').css('color', '');
					bspInfo.find('.total-cost').removeClass('hidden');
					bspInfo.find('.fare-notice').removeClass('hidden');
					bspInfo.siblings('.loading--medium-2').addClass('hidden');
          SIA.preloader.hide();
				}
				else {
					SIA.preloader.hide();
				}
			}
		});
	};

	callBSPAjax(false, {}, function() {
		$('[data-flight]').each(function() {
			$(this).find('input:radio:checked').eq(0).trigger('change.select-flight');
		});
	});

	var paymentKFMiles = function() {
		bookingSummaryWidget.off('change.KfMiles').on('change.KfMiles', function(e) {
			callBSPAjax(true, {
				selectedMiles: e.miles
			}, e.callback);
		});
	};

	paymentKFMiles();

	// Check exist Addon
	var checkExistAddon = function(elm) {
		var exists = false;
		for (var i = addons.length - 1; i >= 0; i--) {
			if(typeof(elm) === 'object' && addons[i].element.is(elm)) {
				exists = true;
				return exists;
			}
			else if(typeof(elm) === 'string' && addons[i].title === elm) {
				exists = true;
				return exists;
			}
		}
		return exists;
	};

	var setEventRadio = function() {
		flightSearch.off('change.customRadio click.toggleTooltip').on('change.customRadio click.toggleTooltip', 'input[type="radio"]',function(e) {
			if((e.originalEvent && $(this).is(':checked')) || ('click' === e.type && 'toggleTooltip' === e.namespace)) {
				toogleTooltip(tooltipPopup, L10n.bookingSummary.fare, $(this).next('label').find('strong.package--price-number').text());
			}
		});
	};
	setEventRadio();

	// Fill data to table flight search
	var fillData = function() {
		flightSearch
		.off('change.fillData')
		.on('change.fillData', 'input[type="radio"]', function(e) {
			var radio = $(this);
			var tableIndex = flightSearch.index($(this).closest('.flights__searchs'));
			$('[name="selectedFlightIdDetails[' + tableIndex + ']"]').val($(this).val());
			callBSPAjax(true, {
				tripType: $(this).val()[0]
			}, function() {
				if(e.originalEvent) {
					toogleTooltip(tooltipPopup, L10n.bookingSummary.fare, radio.next('label').find('strong.package--price-number').text());
				}
			}, radio);
		});

		flightUpgrades
		.off('change.fillData')
		.on('change.fillData', 'input[type="checkbox"]', function() {
			var checkbox = $(this);
			var isChecked = $(this).is(':checked');
			if(isChecked) {
				if(!checkExistAddon(checkbox)) {
					addons.push({
						title : checkbox.data('upgrade-title'),
						price : unformatNumber(checkbox.data('upgrade-price')),
						element : checkbox
					});
				}
			}
			else {
				for (var i = addons.length - 1; i >= 0; i--) {
					if(addons[i].element.is(checkbox)) {
						addons.splice(i, 1);
					}
				}
			}

			calculateFlightPrices();
			setBookingSummaryFlightInfo();
			if (isChecked) {
				toogleTooltip(tooltipPopup, checkbox.data('upgrade-title'), checkbox.data('upgrade-price'));
			}
		});

		flightUpgrades.off('clearAddOn').on('clearAddOn', function() {
			var radio = $(this).find('input[type="checkbox"]');
			for (var i = addons.length - 1; i >= 0; i--) {
				if(addons[i].element.is(radio)) {
					addons.splice(i, 1);
				}
			}

			calculateFlightPrices();
			setBookingSummaryFlightInfo();
		});

		// bookingSummaryWidget
		// .off('click.deleteAddon')
		// .on('click.deleteAddon', '.delete-btn', function(e) {
		// 	e.preventDefault();
		// 	var btn = $(this);
		// 	var elm = $(btn.data('element'));
		// 	if(elm.is('[type="radio"]') || elm.is('[type="checkbox"]')) {
		// 		elm.prop('checked', false).parent().removeClass('checked');
		// 		if(elm.closest('.baggage-1').length) {
		// 			elm.closest('.baggage-1').siblings('.baggage-3').find(':checkbox').prop({
		// 				'checked': false,
		// 				'disabled': true
		// 			}).parent().removeClass('checked').addClass('disabled');
		// 			elm.trigger('change');
		// 		}
		// 	}
		// 	$(this).closest('li').remove();

		// 	for (var i = addons.length - 1; i >= 0; i--) {
		// 		if(addons[i].element.is(elm)) {
		// 			addons.splice(i, 1);
		// 		}
		// 	}

		// 	calculateFlightPrices();
		// 	setBookingSummaryFlightInfo();
		// });
	};

	fillData();

	// Scroll popup
	var popupScroll = function() {
		var trigger = bookingSummaryWidget.find('[data-popup-anchor]');
		trigger.off('click.setAnchor').on('click.setAnchor', function() {
			var anchor = $(this).data('popup-anchor');
			var popup = $($(this).data('trigger-popup'));
			popup.data('anchor', anchor);
		});

		trigger.each(function() {
			var popup = $($(this).data('trigger-popup'));
			if(!popup.data('boundScroll')) {
				popup.data('boundScroll', true);
				popup.off('afterShow.scrollToAnchor').on('afterShow.scrollToAnchor', function() {
					var pop = $(this);
					window.setTimeout(function() {
						/*var content = pop.find('.popup__content');
						var paddingTop = parseInt(content.css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						content.scrollTop(anchorElement.position().top + content.scrollTop() - paddingTop);*/
						var paddingTop = parseInt(popup.find('.popup__content').css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						if(anchorElement.length) {
							popup.scrollTop(anchorElement.position().top /*+ popup.scrollTop()*/ - paddingTop);
						}
					}, 1);
				});
			}
		});
	};

	popupScroll();

	var stickyWidget = function() {
		var fixedClassName = 'booking-summary--fixed';

		// Set position for sticky
		var setStickyPosition = function() {
			if($('.passenger-details-page, .seatsmap-page, .cib-confirmation-page').length) {
				return;
			}
			if(win.scrollTop() >= bookingSummaryOffset.top) {
				var right = win.width() - mainInner.width();
				if (win.width() < mainInner.width()) {
					right += win.scrollLeft();
				}
				else {
					right = right / 2;
				}
				bookingSummaryWidget.addClass(fixedClassName).css('right', right);
				//vars
				var winHeight = win.height(),
				winTop = win.scrollTop(),
				bmHeight = bookingSummaryWidget.height(),
				// bmTop = bookingSummaryWidget.offset().top,
				footTop = footer.offset().top;
				if(bmHeight <= winHeight) {
					//if widget's height is smaller than window's height
					if(winTop + bmHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});
					}
					else {
						bookingSummaryWidget.css({
							top: '',
							bottom: ''
						});
					}
				}
				else {
					//widget's height is larger than window's height
					// if(winTop + bmHeight - (winTop - bmTop) > footTop || winTop + winHeight >= footTop) {
					if(winTop + winHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});
					}
					else {
						bookingSummaryWidget.css({
							top: '',
							bottom: ''
						});
					}
					// else if(bmTop + bmHeight > winTop + winHeight) {
					// 	bookingSummaryWidget.css({
					// 		top: bookingSummaryOffset.top - winTop,
					// 		bottom: ''
					// 	});
					// 	if(bookingSummaryWidget.offset().top + bmHeight < winTop + winHeight) {
					// 		bookingSummaryWidget.css({
					// 			top: 'auto',
					// 			bottom: 0
					// 		});
					// 	}
					// }
					// else {
					// 	bookingSummaryWidget.css({
					// 		top: 'auto',
					// 		bottom: 0
					// 	});
					// }
				}
			}
			else {
				bookingSummaryWidget.removeClass(fixedClassName).css({
					right: '',
					top: '',
					bottom: ''
				});
			}
		};
		var originalWidth = win.width();
		var clearTimeResize = null;
		var clearTimeScroll = null;
		win.off('resize.sticky-booking-summary').on('resize.sticky-booking-summary', function() {
			clearTimeout(clearTimeResize);
			clearTimeResize = setTimeout(function(){
				if(win.width() !== originalWidth) {
					setStickyPosition();
					originalWidth = win.width();
				}
			}, 50);
		}).on('scroll.sticky-booking-summary', function() {
			clearTimeout(clearTimeScroll);
			clearTimeScroll = setTimeout(function(){
				setStickyPosition();
			}, 50);
		}).trigger('scroll.sticky-booking-summary');

		return {
			setStickyPosition: setStickyPosition
		};
	};

	if(bookingSummaryWidget.length){
		var sticky = stickyWidget();
	}

	// Toggle Booking sumary
	var toggleBookingSummary = function() {
		bookingSummaryControl.off('click.openBS').on('click.openBS', function(e){
			e.preventDefault();
			if($('.orb-flight-select-page').length) {
				if($('[data-flight] input[type=radio]:checked').length === 0) {
					return;
				}
			}

			bookingSummaryWidget.toggleClass('active');
			// if(bookingSummaryWidget.hasClass('active')){
			// 	bookingSummaryWidget.removeClass('active');
			// 	// $('em',bookingSummaryControl).removeClass('ico-point-u').addClass('ico-point-d');
			// } else {
			// 	bookingSummaryWidget.addClass('active');
			// 	// $('em',bookingSummaryControl).removeClass('ico-point-d').addClass('ico-point-u');
			// }

			bookingSummaryContent.toggle(0, function(){
				if(bookingSummaryWidget.hasClass('active') &&
					bookingSummaryWidget.closest('body').hasClass('add-ons-page')) {
					bookingSummaryWidget.closest('.main-inner').css('min-height',bookingSummaryWidget.outerHeight(true) + 'px');
				} else {
					bookingSummaryWidget.closest('.main-inner').css('min-height','');
				}
				//$(window).trigger('scroll.sticky-booking-summary');
			});
			sticky.setStickyPosition();
		});

		if($('.payments-page').length) {
			bookingSummaryControl.trigger('click.openBS');
		}
	};

	if(bookingSummaryWidget.length){
		toggleBookingSummary();
	}

	// var formAddonBS = function() {
	// 	formAddon.off('click.addon').on('click.addon', '[data-add-booking-summary]', function(e) {
	// 		var btn = $(this);
	// 		var pkg = {
	// 			title: btn.data('addon-title') ? btn.data('addon-title') : '...',
	// 			price: btn.data('addon-price') ? btn.data('addon-price') : 0,
	// 			element: btn
	// 		};

	// 		if(!checkExistAddon(btn)) {
	// 			addons.push(pkg);
	// 			calculateFlightPrices();
	// 			setBookingSummaryFlightInfo();
	// 			if(e.originalEvent) {
	// 				toogleTooltip(tooltipPopup, pkg.title, pkg.price);
	// 			}
	// 		}
	// 	});
	// };

	// formAddonBS();

	// var tableBaggageAddon = function() {
	// 	tableBaggages
	// 	.off('change.addBaggage')
	// 	.on('change.addBaggage', '.baggage-1 input[type="checkbox"]', function() {
	// 		var chb = $(this);
	// 		if(chb.is(':checked')) {
	// 			var add = {
	// 				title: chb.data('title') ? chb.data('title') : '...',
	// 				price: chb.data('price') ? unformatNumber(chb.data('price')) : 0,
	// 				element: chb
	// 			};

	// 			if(!checkExistAddon(chb)) {
	// 				addons.push(add);
	// 				calculateFlightPrices();
	// 				setBookingSummaryFlightInfo();
	// 			}
	// 		}
	// 		else {
	// 			for (var i = addons.length - 1; i >= 0; i--) {
	// 				if(addons[i].element.is(chb)) {
	// 					addons.splice(i, 1);
	// 				}
	// 			}
	// 			calculateFlightPrices();
	// 			setBookingSummaryFlightInfo();
	// 		}
	// 	})
	// 	.off('change.addBaggageOversize')
	// 	.on('change.addBaggageOversize', '.baggage-3 input[type="checkbox"]', function() {
	// 		var chbOversize = $(this);
	// 		var chb = $(this).closest('tr').find('.baggage-1 input[type="checkbox"]');
	// 		var oversizePrice = chbOversize.data('price') ? unformatNumber(chbOversize.data('price')) : 0;
	// 		for (var i = addons.length - 1; i >= 0; i--) {
	// 			if(addons[i].element.is(chb)) {
	// 				if(chbOversize.is(':checked')) {
	// 					if(!addons[i].addedBonus) {
	// 						addons[i].price += oversizePrice;
	// 						addons[i].addedBonus = true;
	// 					}
	// 				}
	// 				else {
	// 					if(addons[i].addedBonus) {
	// 						addons[i].price -= oversizePrice;
	// 						addons[i].addedBonus = false;
	// 					}
	// 				}
	// 			}
	// 		}

	// 		calculateFlightPrices();
	// 		setBookingSummaryFlightInfo();
	// 	});
	// };

	// tableBaggageAddon();

	// Toggle tooltip
	var toogleTooltip = function(tooltipElement, upperText, priceAdded) {
		if(!tooltipElement.is(':visible') && !bookingSummaryWidget.hasClass('active')) {
			var overwriteTxtTooltip = tooltipElement.find('.tooltip__content');
			var position = bookingSummaryWidget.offset(),
					posTop = Math.max(position.top - $(window).scrollTop(), 0),
					posRight = $(window).innerWidth() - position.left + 10;

			var currency = $('#booking-summary').is('[data-currency]') ? $('#booking-summary').data('currency') : 'SGD';

      overwriteTxtTooltip.html(
        upperText +
        '<span class="text-1">+' + currency + ' ' + formatNumber(priceAdded) + '</span>'
      );

			if($(window).innerWidth() < SIA.global.config.tablet) {
				posTop = 15;
				posRight = 0;
			}
			else if($(window).innerWidth() === SIA.global.config.tablet){
				posRight += 15;
			}

			tooltipElement.addClass('active').stop().css({
				position: 'fixed',
				top: posTop,
				right: posRight
			}).show().delay(2000).hide(0, function() {
				$(this).removeClass('active');
			});
		}

		win.on('scroll.sticky-booking-summary', function() {
			if(bookingSummaryWidget.hasClass('booking-summary--fixed')) {
				tooltipElement.css('top', '5px');
			} else {
				tooltipElement.css('top', bookingSummaryWidget.offset().top);
			}
		});
	};

  // add img card to customSelect
  function appendImgToCustomSelect() {
		var parentUlBlock = $('li.select-card-empty').closest('ul'),
			listcustomSelect = parentUlBlock.find('li');
		listcustomSelect.each(function(index) {
			var _this = $(this),
					thisDataImg = _this.data('value'),
					imgTemp = '<img src="images/' + thisDataImg + '" alt="credit card" data-format-card="master">';
			if(index === 0) { return; }
			_this.prepend(imgTemp);
			_this.find('img').css({
				'margin-top': '-5px',
    		'margin-right': '10px'
			});
		});
	};

  // Function show/ hide content credit card
  var blockContent = $('.block-content-credit-debit'),
      contentForAnotherCard = blockContent.find('.for-another'),
      contentForSelectCard = blockContent.find('.for-select');

  function forAnotherCard() {
    contentForAnotherCard.removeClass('hidden');
    contentForSelectCard.addClass('hidden');
    blockContent.removeClass('for-select');
    blockContent.find('.number-cvv, .list-cards').removeClass('for-select');

    var blockCreditDebit = $('#credit-debit-card-rd').closest('.grid-col').next('.grid-col');
		var srcImgCreditDebit = blockCreditDebit.find('.type-card').not('.grey-out').attr('src');
		$('.block-save-your-credit').find('.group-save-card').find('img').attr('src', '');
		$('.block-save-your-credit').find('.group-save-card').find('img').attr('src', srcImgCreditDebit);
		$('.get-value-card').find('.img-card-selected').removeClass('hidden');

  };
  function forSelectCard() {
    contentForSelectCard.removeClass('hidden');
    contentForAnotherCard.addClass('hidden');
    blockContent.addClass('for-select');
    if($('.mp-1-payments').length){
      blockContent.find('.number-cvv').addClass('for-select');
    } else {
      blockContent.find('.number-cvv, .list-cards').addClass('for-select');
    }
  };

	var renderBlockCreditSaved = function(){
    var templateCreditSaved;
    var appendDiv = $('.block-selected-card');
    appendDiv.addClass('append-block');
    var yourSavedCreditTpl = function(data1){
      $.get(global.config.url.yourSavedCredit, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateCreditSaved = $(template);
        appendDiv.append(templateCreditSaved);
        SIA.initCustomSelect();
        var lengthOption = $('.save-credit-debit').find('option').length;
        if(lengthOption > 5 && $('body').hasClass('payments-page-logged') || lengthOption > 5 && $('body').hasClass('mp-add-ons-page')){
        	$('.block-save-your-credit').find('.group-save-card').addClass('has-disabled');
        	$('.block-save-your-credit').find('.text-error').removeClass('hidden');
        } else{
        	$('.block-save-your-credit').find('.group-save-card').removeClass('has-disabled');
        	$('.block-save-your-credit').find('.text-error').addClass('hidden');
        }

        var parentUlBlock = $('li.select-card-empty').closest('ul'),
						listcustomSelect = parentUlBlock.find('li');

				parentUlBlock.find('.disabled-card').attr('disabled','true').removeClass('has-disabled');

        listcustomSelect.on('click', function() {
        	var dataMonth, dataYear, dataName, dataCard,
        	elmCurrentClick = $(this).index();
        	if(elmCurrentClick !== 0){
        		inputCreditDebit.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        	inputCreditDebit.val("");
	        	inputNumberCard.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        	inputNumberCard.val("");
	        	inputSelectMonth.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        	inputSelectMonth.val("");
	        	inputSelectYear.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        	inputSelectYear.val("");
	        	inputSelectCvv.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        	inputSelectCvv.val("");
        		blockContent.removeClass('hidden');
          	var valueOption = $('.save-credit-debit').find('option'),
							imageSrc = $(this).find('img').attr('src'),
							widthImage = $(this).find('img').width();
						$('.payments-detail__form').find('.block-selected-card').find('.select__text').css('padding-left', widthImage + 22);
						$('.get-value-card').find('.img-card-selected').attr('src', imageSrc);
						$('.get-value-card').find('.img-card-selected').removeClass('hidden');
						$('.get-value-card').find('.img-card-selected').removeClass('grey-out');
        		valueOption.each(function() {
        			var elmValueOption = $(this);
        			var indexElmValueOption = elmValueOption.index();
							if(elmCurrentClick === indexElmValueOption){
								dataMonth = elmValueOption.data('month');
								dataYear = elmValueOption.data('year');
								dataName = elmValueOption.data('name');
								dataCard = elmValueOption.data('card');
							} else {}
				  	});
				  	blockContent.find('.name-card').text(dataName);
				  	blockContent.find('.date-year').text(dataYear);
				  	blockContent.find('.date-month').text(dataMonth);
				  	blockContent.find('.card-number').html('&bull;&bull;&bull;&bull;'+dataCard);
				  	$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
        	} else {
        		blockContent.find('.name-card').text('');
				  	blockContent.find('.date-year').text('');
				  	blockContent.find('.date-month').text('');
				  	blockContent.find('.card-number').html('');
				  	blockContent.addClass('hidden');
				  	$('.payments-detail__form').find('.block-selected-card').find('.select__text').css('padding-left', '13px');
				  	$('.get-value-card').find('.img-card-selected').attr('src','');
				  	$('.get-value-card').find('.img-card-selected').addClass('hidden');

				  	$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
        	}
				});

      	var firstClick = true;
        $('.block-selected-card .custom-select').on('click', function() {
        	if(firstClick) {
        		appendImgToCustomSelect();
        	}
        	firstClick = false;
				});

				$('.block-selected-card .pay-another-card').on('click', function(e){
					e.preventDefault();
					inputSelectCvv.closest('.grid-col').removeClass('error').find('.text-error').remove();
	        inputSelectCvv.val("");
					$('#customSelect-4-listbox').find('li').removeClass('active').attr('aria-selected', 'false');
					$('#customSelect-4-listbox').find('li').eq(0).trigger('click');
					if(blockContent.hasClass('hidden')) {
						blockContent.removeClass('hidden');
					}
					forAnotherCard();
				});
				$('.block-selected-card .save-credit-debit').on('change', function(e){
					e.preventDefault();
					if(blockContent.hasClass('hidden')) {
						// blockContent.removeClass('hidden');
					}

					forSelectCard();
				});

				$('.has-disabled.disabled-card').on('click', function(event){
					event.preventDefault();
				});

      });
    };
    var urlKfDashboard;
    if($('body').hasClass('with-4-cards')){
    	urlKfDashboard = "ajax/kf-wallet-4-cards.json";
    } else{
    	urlKfDashboard = global.config.url.jsonKfDashboard;
    }
    $.ajax({
      url: urlKfDashboard,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data1 = reponse.digitalWalletVO;
        yourSavedCreditTpl(data1);
      }
    });
  };

  var inputCreditDebit = $('.block-content-credit-debit').find('#credit-debit'),
  	inputNumberCard = $('.block-content-credit-debit').find('#credit-debit-number'),
  	inputSelectMonth = $('.block-content-credit-debit').find('#input-month'),
  	inputSelectYear = $('.block-content-credit-debit').find('#input-year'),
  	inputSelectCvv = $('.block-content-credit-debit').find('#input-cvv');

  var showBlockSaveCard = function(){
  	if(inputCreditDebit.val() !== '' && inputCreditDebit.closest('.grid-inner').next('.text-error').length === 0){
  		if(inputNumberCard.val() !== '' && inputNumberCard.closest('.grid-inner').next('.text-error').length === 0){
  			if(inputSelectCvv.val() !== ''){
  				if(inputSelectYear.val() !== ''){
  					if(inputSelectMonth.val() !== ''){
  						$('.block-content-credit-debit').find('.block-save-your-credit').removeClass('hidden');
  					} else {
				  		$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
				  	}
  				} else {
			  		$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
			  	}
  			} else {
		  		$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
		  	}
  		}else {
	  		$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
	  	}
  	} else {
  		$('.block-content-credit-debit').find('.block-save-your-credit').addClass('hidden');
  	}
  };

  var initPopup = function(data) {
    var triggerPopup = $('[data-trigger-popup]');

    triggerPopup.each(function() {
      var self = $(this);

      if (typeof self.data('trigger-popup') === 'boolean') {
        return;
      }

      var popup = $(self.data('trigger-popup'));

      if (!popup.data('Popup')) {
        popup.Popup({
          overlayBGTemplate: config.template.overlay,
          modalShowClass: '',
          triggerCloseModal: '.popup__close, [data-close], .cancel',

          afterHide: function(){
            container.css('padding-right', '');
            body.css('overflow', '');
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
              if (ua.indexOf('chrome') > -1) { }
              else {
                body.attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
              }
            }
          }
        });
      }
      self.off('click.showPopup').on('click.showPopup', function(e) {
        e.preventDefault();
        if (self.data('trigger-popup') === '.popup--flights-details') {
        	renderPopupRefund(data, function(){
        		popup.Popup('show');
        		$(window).trigger('resize');
        	});
        }
      });

    });
  };

  var renderBSPCancel = function(){
  	$.ajax({
  		url: config.url.sshCancelBspJson,
  		method: 'GET',
  		dataType: 'json',
  		success: function(data1){
  			var bookingSummary = $('#booking-summary');
  			$.get(config.url.sshCancelBspTemplate, function(data) {
  				var template = window._.template(data, {
  					data: data1.refundSummary
  				});
  				
  				bookingSummary.empty().append($(template));

  				bookingSummaryControl = bookingSummaryWidget.find('.booking-summary__heading .booking-summary__control');
  				bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');

  				toggleBookingSummary();
  				initPopup(data1.refundSummary);

  			});
  		}
  	})
  }

  if($('body').hasClass('payments-page-logged') || $('body').hasClass('payments-page-not-logged') || $('body').hasClass('mp-add-ons-page') ){
		showBlockSaveCard();
		inputCreditDebit.off('keyup').on('keyup',function(){
			showBlockSaveCard();
	  });

	  inputNumberCard.off('focusout').on('focusout',function(){
	  	setTimeout(function(){ showBlockSaveCard(); }, 300);
	  });

	  inputSelectMonth.on('change',function(){
	  	showBlockSaveCard();
	  });

	  inputSelectYear.on('change',function(){
	  	showBlockSaveCard();
	  });

	  inputSelectCvv.off('keyup').on('keyup',function(){
	  	if(!$('.block-content-credit-debit').hasClass('for-select')){
	  	showBlockSaveCard();
	  	}
	  });
  }

  if($('body').hasClass('payments-page-logged') || $('body').hasClass('mp-add-ons-page')){
  	if(!$('.block-selected-card').hasClass('append-block')){
			renderBlockCreditSaved();
  	}
  	var paymentsDetailForm = $('#payments-detail__form');
  	var krisflyerAccount = paymentsDetailForm.find('#krisflyer-account-cb');
	  krisflyerAccount.trigger('click');
  }
  if($('body').hasClass('payments-page-not-logged')){
  	var paymentsDetailForm = $('#payments-detail__form');
  	var blockSelectedCard = paymentsDetailForm.find('.block-selected-card');
  	var blockCreditDebit = paymentsDetailForm.find('.block-content-credit-debit');
  	blockSelectedCard.addClass('hidden');
  	blockCreditDebit.removeClass('hidden');
  	var krisflyerAccount = paymentsDetailForm.find('#krisflyer-account-cb');
	  krisflyerAccount.trigger('click');
  }

  if($('body').is('.ssh-review-cancel-page')) {
  	renderBSPCancel();
  }
  
};
