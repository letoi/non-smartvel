var SIA = SIA || {};

SIA.skChangeManagement = function() {
    var p = {};

    p.overlayChangeManagementEl = $('.overlay-change-management');
    p.skChangeManagementSliderEl = $('.sk-change-management-slider');
    p.bodySkChangeManagementEl = $('.sk-change-management');
    
    var init = function () {
        if (showOverlay()) {
            // remove scrolling on background
            setBackgroundScrolling('position', 'fixed');

            // show the overlay of change management
            p.overlayChangeManagementEl.removeClass('hidden');

            loadCloseBtnEvent();
            loadSlider();
        }
    }; 

    var showOverlay = function() {
        var cookie = document.cookie.replace(/(?:(?:^|.*;\s*)showCoachMarks\s*\=\s*([^;]*).*$)|^.*$/, "$1");
        var val = false;

        if (cookie === '') {
            val = true;
        }

        return val;
    };

    var disableOverlay = function () {
        // don't show this overlay again
        document.cookie = 'showCoachMarks=false';
    };

    var setBackgroundScrolling = function (properties, values) {
        p.bodySkChangeManagementEl.css(properties, values);
    };

    var loadCloseBtnEvent = function() {
        p.overlayChangeManagementEl.find('.close-btn').on('click', function(e) {
            e.preventDefault();
            p.overlayChangeManagementEl.addClass('hidden');

            // add scrolling on background
            setBackgroundScrolling('position', '');

            // don't show this overlay again
            disableOverlay();
        });
    };

    var loadSlider = function() {
        p.nextBtnEl = $('.next-btn');

        p.skChangeManagementSliderEl.slick({
            dots: true,
            speed: 600,
            prevArrow: '',
            nextArrow: '',
            draggable: false,
            useCSS: false,
            useTransform: false,
            waitForAnimate: false
        });

        function changeElemetStyle(currentSlide, nextSlide, el, className) {
            el.removeClass(className + currentSlide);
            el.addClass(className + nextSlide);
        }

        function loadNextBtnEvents() {
            p.nextBtnEl.on('click', function() {
                p.skChangeManagementSliderEl.slick('slickNext');
            }); 
        }

        function hideSlickDots() {
            p.skChangeManagementSliderEl.find('.slick-dots').addClass('hidden');
        }

        // add default style
        var firstSlide = p.skChangeManagementSliderEl.slick('slickCurrentSlide');
        if (firstSlide == 0) {
            // dots styles
            changeElemetStyle('', 0, $('.slick-dots'), 'slick-dots-');
        }

        // add on before change slider event
        p.skChangeManagementSliderEl.on('beforeChange', function(event, slick, currentSlide, nextSlide) {

            var currentSlideDot = p.skChangeManagementSliderEl.find('.slick-dots li');

            if (!currentSlideDot.hasClass('slick-active')) {
                hideSlickDots();
            }
            // change dots style
            changeElemetStyle(currentSlide, nextSlide, $('.slick-dots'), 'slick-dots-');

            // add last button "start booking"
            if (nextSlide == 4) {
                $('.start-booking').off().on('click', function (e) {
                    e.preventDefault();
                    p.overlayChangeManagementEl.addClass('hidden');

                    // add scrolling on background
                    setBackgroundScrolling('position', '');

                    // don't show this overlay again
                    disableOverlay();
                });
            }
        });

        p.skChangeManagementSliderEl.on('afterChange', function(event, slick, currentSlide) {
            setTimeout(function() {
                // show the slick dots
                p.skChangeManagementSliderEl.find('.slick-dots').removeClass('hidden');
            }, 10);
        });

        $(window).on('touchmove', function() {
            hideSlickDots();
        });

        // add the next button event
        loadNextBtnEvents();
    };

    var oPublic = {
        init: init,
        public: p
    };

    return oPublic;
}();

$(function() {
    // wait for overlay loading to be finish
    var waitForOverlayLoading = function() {
       setTimeout(function() {
           if (! $('.overlay-loading').hasClass('hidden')) {
               waitForOverlayLoading();
           } else {
                SIA.skChangeManagement.init();
           }
       }, 100);
    };

    $('.overlay-loading').hasClass('hidden') ? SIA.skChangeManagement.init() : waitForOverlayLoading();
});
