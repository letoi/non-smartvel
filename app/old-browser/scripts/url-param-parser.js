var SIA = SIA || {};

SIA.URLParamParser = function() {
    function getURLParams(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    
    var pub = {
        getURLParams: getURLParams
	};
    
	return pub;
}();