/**
 * @name SIA
 * @description Define global initAutocompleteCity functions
 * @version 1.0
 */
SIA.initAutocompleteCityNew = function() {
	var global = SIA.global;
	var vars = global.vars;
	// var doc = global.vars.doc;
	var win = vars.win;
	var config = global.config;
	var body = vars.body;
	// var isIETouch = vars.isIETouch();
	var originSelected;
	var destinationSelected;
	var countryOriginSelected;
	var countryDestinationSelected;
	var countryGroups = [];
	var selectionIdList = '#book-redem, #book-flight,' +
		' #singaporeAirlines, #australiaAirlines, #form-book-travel, #form-book-travel-1, #form-search-flight-1';
	var excludeRecentSearch = [
		'form-book-travel-1', 'book-redem'
	];

	var railCoachList = {};
	var listRailsCoach = true;

	var destinationAirports = [];
	var originAirports = [];

	var listLimitDisplay = 300;
	var localizedCountries = [
		"DE"
	];
	var isLocalized = false;

	var getFormattedDate = function(date) {
        date = date.split("/");
        return date[0] + " " + getMonthName(date[1]);
	};
	
	var getMonthName = function (monthIdx) {
        var months = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        return months[parseInt(monthIdx) - 1];
	};
	
	var extractCode = function(value) {
        return value.match(/ \w+$/i).pop().trim();
    };

	var formatRecentSearch = function(searchObject) {
        var formatedDate = getFormattedDate(searchObject.depart) +  ( searchObject["return"] ? (" - " + getFormattedDate(searchObject["return"])) : '');
        var formatedSearch = extractCode(searchObject.from) + " - " + extractCode(searchObject.to);

        return {
			formatedDate: formatedDate,
			formatedSearch: formatedSearch
		}
    };

	var _autoComplete = function(opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			// containerAutocomplete : '',
			autocompleteFields: '',
			autoCompleteAppendTo: '',
			airportData: [],
			open: function() {},
			change: function() {},
			select: function() {},
			close: function() {},
			search: function() {},
			response: function() {},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);
		// that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.autocompleteFields = that.options.autocompleteFields;
		that.airportData = that.options.airportData;
		that.cityData = that.options.cityData;
		that.timer = null;
		that.timerResize = null;
		that.winW = win.width();

		that.autocompleteFields.each(function(index, value) {
			var field = $(value);
			var wp = field.closest('[data-autocomplete-od]');
			var inputEl = wp.find('input');
			var optionEl = wp.find('select option');
			var isEmptySearch = false;

			if (wp.data('origin') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						countryOriginSelected = $(this).data('text');
						originSelected = $(this).data('country-exclusion');
					}
				});
			}

			if (wp.data('destination') && inputEl.val() !== '') {
				optionEl.each(function() {
					if ($(this).data('text') === inputEl.val()) {
						countryDestinationSelected = $(this).data('text');
						destinationSelected = $(this).data('country-exclusion');
					}
				});
			}
			var minLength = 1;
			if (body.hasClass("recent-search") && field.data("recent-search") == "from") {
				minLength = 0;
			}

			var bookingAutoComplete = field.autocomplete({
				minLength: minLength,
				open: that.options.open,
				change: that.options.change,
				select: that.options.select,
				close: that.options.close,
				search: that.options.search,
				response: that.options.response,
				// source: that.airportData,
				source: function(request, response) {
					var listData = [],
						listCodeCheck = [];

					if (!request.term) {
						listData = checkAvailableFlights(listCodeCheck, that.airportData);
						listData = checkGroupNoItem(listData);
						if (body.hasClass("recent-search")) {
							var formName = field.closest("form").attr("id");
							if (excludeRecentSearch.indexOf(formName) != -1) {
								return;
							}
							
							var fieldValue = field.val();
							var recentSearchResult = getLocalStorage("fs_history");
							recentSearchResult = recentSearchResult.reverse();
							for (var i = 0; i < recentSearchResult.length; i ++) {
								var e = recentSearchResult[i];
								recentSearchResult[i] = {
									value: e.from,
									id: e.id,
									data: e,
									idx: i,
									label: formatRecentSearch(e)
								};
							}
							if (!fieldValue) {
								$("[data-recent-search-trigger]").trigger("clearTo");
							}
							if (!recentSearchResult.length) isEmptySearch = true;
							return response(recentSearchResult);
						}
						if (wp.closest(selectionIdList)) {
							response(listData.slice(0, 100));
						} else {
							response(listData);
						}
						return;
					}

					try {
						that.group = '';
						var matcher = new RegExp('(^' + $.ui.autocomplete.escapeRegex(request.term) + ')|(\\s' + $.ui.autocomplete.escapeRegex(request.term) + ')|([(]' + $.ui.autocomplete.escapeRegex(request.term) + ')|('+$.ui.autocomplete.escapeRegex(request.term)+')', 'i');
						var match1 = [];
						if (that.cityData.length) {
							match1 = $.grep(that.cityData, function(airport) {

								var label = airport.label;
								var valueOption = airport.valueOption;
								return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
							});
						}


						var match = $.grep(that.airportData, function(airport) {
							var label = airport.label;
							var valueOption = airport.valueOption;
							var value = airport.countrySelectedcity;
							return (matcher.test(label) || matcher.test(value) || matcher.test(valueOption));
						});

						listData = checkAvailableFlights(listCodeCheck, match);
						listData = checkGroupNoItem(listData);

						if (match1.length) {
							match1 = checkAvailableFlights(listCodeCheck, match1);
							response(match1.concat(listData));
						} else {
							if (wp.closest(selectionIdList)) {
								response(listData);
							} else {
								response(listData);
							}
						}

					} catch (err) {
						response(that.airportData);
					}
				},
				position: that.options.position,
				appendTo: that.options.autoCompleteAppendTo
			}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function(ul, item) {
				if (!isEmptySearch) {
					ul.removeClass("hidden");
					if (item.city != "No results found.") {
						if (item.group) {
							that.group = item.group;
							return $('<li class="group-item">' + item.group + '</li>')
								.appendTo(ul);
						} else {
							var link = item.link ? ' data-link="' + item.link + '"' : '';
							// since all cities are under a country/group, if item.parent does not belong to that.options.group, you can just prepend to ul
							if (!item.city) {
								var showRecentSearchLabel = '<div class="recent-title-main-container"><span class="recent-title">Recent Searches</span></div>';
								return $('<li class="autocomplete-item">')
									.attr('data-value', item.id)
									.append((item.idx == 0 ? showRecentSearchLabel : '') 
										+ '<a class="autocomplete-link">' 
										+ '<span class="date-format">'
										+ item.label.formatedDate
										+ '</span>'
										+ '<span class="search-format">'
										+ item.label.formatedSearch
										+ '</span>' 
										+ ' <em class="remove-recent ico-delete pull-right"></em></a>')
									.appendTo(ul);
							}

							if (item.parent === that.group) {
								return $('<li class="autocomplete-item"' + link + '>')
									.attr('data-value', item.city)
									.append('<a class="autocomplete-link">' + item.city + '</a>')
									.appendTo(ul);
							} else if (item.parent) {
								return $('<li class="autocomplete-item redundancy"' + link + '>')
									.attr('data-value', item.city)
									.append('<a class="autocomplete-link">' + item.city + '</a>')
									.prependTo(ul);
							}

							if(item.railCoachLabel) {
								var labelText = getRailCoachLabel(item);
								var listItem = '<li class="ui-menu-item autocomplete-item autocomplete-railcoach-label">' + labelText + ' ' + item.cityName + '</li>';
								return $(listItem).appendTo(ul);
							}

							if(item.isCoachPointStation) {
								var listItem = '<li class="autocomplete-item" data-value="' + item.city + '"' + link + '><a class="autocomplete-link autocomplete-railcoach-link"><em class="ico-5-bus ico-railcoach"></em>' + item.city + '</a></li>';
								return $(listItem).appendTo(ul);
							}

							if(item.isRailPointStation) {
								var listItem = '<li class="autocomplete-item" data-value="' + item.city + '"' + link + '><a class="autocomplete-link autocomplete-railcoach-link"><em class="ico-5-rail ico-railcoach"></em>' + item.city + '</a></li>';
								return $(listItem).appendTo(ul);
							}
							
							var listItem =  '<li class="autocomplete-item" data-value="' + item.city + '"' + link + '><a class="autocomplete-link">' + item.city + '</a></li>';
							
							return $(listItem).appendTo(ul);
						}
					} else if (item.city == "No results found.") {
						return $('<li class="autocomplete-item" data-value="No results found.">').append('<p style="padding-left: 10px;">No results found.</p>').appendTo(ul);
					}
				} else {
					isEmptySearch = false;
					ul.addClass("hidden");
					return $('<li class="autocomplete-item" data-value="No results found.">').appendTo(ul);
				}
			};

			bookingAutoComplete._resizeMenu = function() {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function() {
				var self = $(this);

				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				
				win.off('resize.repositionAutocompleteCity').on('resize.repositionAutocompleteCity', function() {
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function() {
						if (that.winW !== win.width()) {
							field.trigger('blur.highlight');
							that.winW = win.width();
						}
					}, 100);
				});
			});

			field.off('blur.highlight').on('blur.highlight', function() {
				that.timer = setTimeout(function() {
					field.closest('.custom-select').removeClass('focus');
					field.autocomplete('close');
				}, 200);
				//field.autocomplete('close');
				/*if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
				 win.off('resize.reposition');
				 }*/
				win.off('resize.repositionAutocompleteCity');
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e) {
				e.stopPropagation();
			});

			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function() {
				clearTimeout(that.timer);
			});

			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e) {
				if (e.which === 13) {
					field.autocomplete('widget').find('li.active').trigger('click');
					e.preventDefault();
				}
			});

			if (field.closest(selectionIdList).length >= 1) {
				field.next().remove();
			}

			field.on('keyup', function(e) {
				if (field.closest(selectionIdList).length >= 1) {
					if (field.val().length == 0) {
						field.closest('[data-autocomplete-od="true"]').find('.optim-close').addClass('hidden');
						field.autocomplete('close');
					} else {
						clearCloseBtn(field);
					}
				}
			});

			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e) {
				e.preventDefault();
				clearTimeout(that.timer);
				if (field.closest('.custom-select').hasClass('focus')) {
					field.trigger('blur.highlight');
				} else {
					field.trigger('focus.highlight');
				}
			});
		});
		resetAvailableFlight();
	};

	var createGroupList = function(data, type) {
		var str = '';

		if (type == 'list') {
			str += '<li class="group-item">' + data + '</li>';
		} else {
			str += '<optgroup label="' + data + '"></optgroup>';
		}

		return $(str);
	};

	var clearCloseBtn = function(el) {
		var closeIconEl = $('<a href="#clear" class="ico-cancel-thin optim-close add-clear-text" tabindex="-1"><span class="ui-helper-hidden-accessible">clear</span></a>');
		var parent = el.closest('[data-autocomplete-od="true"]');

		var closeIcon = parent.find('.optim-close');
		if (closeIcon.length >= 1) {
			closeIcon.removeClass('hidden');
		} else {
			closeIconEl.off().on('click', function(e) {
				e.preventDefault();

				el.val('');

				// reset selected values
                // reset selected values
                if(parent.attr('data-origin') === 'true'){
                    originSelected = '';
                    countryOriginSelected = '';
                }

                if(parent.attr('data-destination') === 'true'){
                    destinationSelected = '';
                    countryDestinationSelected = '';
                }

				$(this).addClass('hidden');
			});

			parent.find('.select__text').after(closeIconEl);
		}
	};

	$("[data-recent-search-trigger]")
		.on("createClearCloseBtn", function(evt, el) {
			clearCloseBtn($(el));
		});

	var createList = function(data, type) {
		var str = '';
		var exCntry = [];
		if(data.excludeCountries !== '' && data.excludeCountries.length > 0) {
			for (var i = 0; i < data.excludeCountries.length; i++) {
				exCntry.push(data.excludeCountries[i].excludedCountryCode);
			}
		}

		var railCoach = typeof data.displayRailCoachFlypoints === 'object' ? data.displayRailCoachFlypoints.toString() : '';
		
		if (type === 'list') {
			str = '<li class="autocomplete-item ui-menu-item" data-value="' + (data.airportName + ' - ' + data.airportCode) + '" data-airportcode="' + data.airportCode + '" data-cityname="' + data.cityName + '" data-country="' + data.countryCode + '" data-country-exclusion="' + exCntry.toString() + '" data-railcoach="' + railCoach + '" role="presentation"><a class="autocomplete-link ui-corner-all" tabindex="-1">' + (data.cityName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.airportCode + ')') + '</a></li>';
		} else {
			str = '<option value="' + (data.airportName + ' - ' + data.airportCode) + '" data-text="' + (data.cityName + ' - ' + data.airportCode) + '" data-airportcode="' + data.airportCode + '" data-cityname="' + data.cityName + '" data-country="' + data.countryCode + '" data-country-exclusion="' + exCntry.toString() + '" data-railcoach="' + railCoach + '">' + (data.cityName + ', ' + data.countryName + ' (' + data.airportName + ' - ' + data.airportCode + ')') + '</option>';
		}

		return $(str);
	};

	var getAirportListInfo = function(formName) {
		var redeemId = [
			"book-redem",
			"form-book-travel-1"
		];
		if (redeemId.indexOf(formName) !== -1) {
			return globalJson.countriesListRedeem;
		} else {
			return globalJson.countriesList;
		}
	};

	var resetForms = function() {
		$("[data-global-form-validate]").resetForm();
	};

	var initAutocompleteCity = function() {
		// init autocomplete
		var autocompleteEl = body.find('[data-autocomplete-od]');
		var autocompleteJSON = body.find('[data-autocomplete-json]');
		var autocompleteJSONButton = body.find('[data-autocomplete-json-selection]');
		
		//var countriesList = globalJson.countriesList.airportList;
		var form, airportList;

		if (!autocompleteJSON.length) {
			autocompleteEl.each(function(c) {
				autoCompleteInit($(this), c);
			});
		} else {
			autocompleteJSONButton.find(".custom-radio").find("[type='radio']").on("change", 
				_.debounce(function() {
					form = autocompleteHasActive('[data-autocomplete-json]');
					airportList = getAirportListInfo(form.formName);
					getAJAXFlights(airportList, form);
				}, 100));

			form = autocompleteHasActive('[data-autocomplete-json]');
			airportList = getAirportListInfo(form.formName);
			getAJAXFlights(airportList, form);
		}

		win.off('touchmove.closeAutocomplete').on('touchmove.closeAutocomplete', function() {
			body.find('.focus[data-autocomplete-od] input').trigger('blur.highlight');
		});
	};

	var getAllAutocompleteByForm = function(formName) {
		$("#" + formName).find("[data-autocomplete-od]").each(function(c) {
			autoCompleteInit($(this), c);
		});
	};

	var getAJAXFlights = function(airportList, form) {
		var redeemId = [
			"book-redem",
			"form-book-travel-1"
		];
		if (_.isEmpty(airportList)) {
			getJSON(airportList, form.formJSON, function(response) {
				if (redeemId.indexOf(form.formName) !== -1) {
					globalJson.countriesListRedeem = response;
				} else {
					globalJson.countriesList = response;
				}
				getAllAutocompleteByForm(form.formName);
			});
		}
	};

	var getJSON = function($global, formJSON, cb) {
		var en = location.href.match(/country=(\w+)&*/);
		var bookingJSON = {
			"bookFlight": {
				"default": "Book_Flights.json",
				"jp": "Book_Flight_JP.json",
				"cn": "Book_Flight_CN.json"
			},
			"bookRedeem": {
				"default": "Redeem_Flights.json",
				"jp": "Book_Flight_JP.json",
				"cn": "Book_Flight_CN.json"
			}
		};
		$.get("ajax/" + bookingJSON[formJSON][en ? en[1].toLowerCase() : "default"], function(response) {
			if (!isLocalized) {
				isLocalized = true;
				setLocalizedCountries(response.airportListInfo);
			}
			cb(response);
		});
	};

	var setLocalizedCountries = function(countriesListJSON) {
		localizedCountries = _.map(localizedCountries, function(countryCode) {
			var countryFind = _.findWhere(countriesListJSON, {"countryCode": countryCode});
			return countryFind.countryName;
		});
	};

	var autocompleteHasActive = function(selector) {
		return {
			formName: $(selector + ".active").attr("id"),
			formJSON: $(selector + ".active").data("autocomplete-json")
		};
	};

	var autoCompleteInit = function($this, c) {
		var countriesList, countriesListJSON;
		var timerAutocomplete = null;
		var self = $this,
			inputEl = self.find('input:text');
		var formName = self.closest("form").attr("id");
		countriesListJSON = getAirportListInfo(formName);
		countriesList = getAirportListInfo(formName);
		resetForms();
		
		processAirportList(countriesList.airportListInfo);
		// a script is removing the input field so we need to re-add
		if(inputEl.length < 1) {
			inputEl = $('<input type="text" autocomplete="new-password" id="city'+c+'" placeholder="City" value="" aria-labelledby="city'+c+'-error" class="destination-city" data-rule-required="true" data-msg-required="Select a destination." data-msg-notequalto="Select another origin or destination."/>');
			self.find('span.select__text').prev().attr('for', 'city'+c);
			self.find('span.select__text').append(inputEl);
		}

		if (self.data('init-automcomplete')) {
			return;
		}

		if (self.find('input:text').is('[readonly]')) {
			self.find('input:text').off('focus').off('click');
			return;
		}
		var select = self.find('select');
		//var options = select.children();
		var data = [];
		var arrDataValue = [];
		var clearHideaddClear = null;
		var def = [];

		// Check if origin or destination field
		if(typeof self.attr('data-destination') !== 'undefined'
		&& self.attr('data-destination') === 'true') {
			countriesList = destinationAirports;
		}

		if(typeof self.attr('data-origin') !== 'undefined'
		&& self.attr('data-origin') === 'true') {
			countriesList = originAirports;

			// if defaultSelectedAirport is defined
			if(typeof countriesListJSON.defaultSelectedAirport !== 'undefined' && countriesListJSON.defaultSelectedAirport !== '') {
				def = countriesList.filter(function(val) {
					return val.airportCode === countriesListJSON.defaultSelectedAirport;
				});

				if(def.length > 0) {
					inputEl.val(def[0].cityName + ' - ' + def[0].airportCode);
					clearCloseBtn(inputEl);
				}
			}
		}

		self.data('minLength', 2);
		self.data('init-automcomplete', true);
		self.find('input:text').focus(function() {});

		function addData(els) {
			els.each(function() {
				var indexOfAirport = $(this).text().indexOf('(');
				var valueOption = $(this).text().slice(indexOfAirport);
				data.push({
					city: $(this).text(),
					label: $(this).text(),
					value: $(this).data('text'),
					valueOption: valueOption,
					parent: $(this).parent().attr('label'),
					country: $(this).data('country'),
					link: $(this).data('link') || '',
					countryExclusion: $(this).data('country-exclusion')
				});
				arrDataValue.push({
					city: $(this).text(),
					label: $(this).data('text'),
					value: $(this).data('text'),
					valueOption: valueOption,
					country: $(this).data('country'),
					link: $(this).data('link') || '',
					countryExclusion: $(this).data('country-exclusion')
				});
			});
		};

		function optionsAddData(select, options) {
			if (select.data('city-group')) {		
				options.each(function(i, val) {
					var self = $(this);
					var countryItem = self.children();
					if (self.attr('label')) {
						data.push({
							city: self.attr('label'),
							label: self.attr('label'),
							group: self.attr('label'),
							link: self.data('link') || '',
							value: self.attr('label'),
							country: $(this).data('country'),
							countryExclusion: $(this).data('country-exclusion'),
							railcoach: self.attr('data-railcoach'),
							cityName: self.attr('data-cityname'),
							airportCode: self.attr('data-airportcode')
						});
					} else {
						data.push({
							city: self.text(),
							label: self.text(),
							value: self.data('text'),
							country: $(this).data('country'),
							countryExclusion: $(this).data('country-exclusion'),
							link: $(this).data('link') || '',
							railcoach: self.attr('data-railcoach'),
							cityName: self.attr('data-cityname'),
							airportCode: self.attr('data-airportcode')
						});
					}

					if (countryItem.length) {
						addData(countryItem);
					}
				});
			} else {
				options.each(function() {
					data.push({
						city: $(this).text(),
						label: $(this).text(),
						value: $(this).data('text'),
						link: $(this).data('link') || ''
					});
				});
			}
		};

		if (select.closest(selectionIdList).length) {
			var selectEl = select;
			var cll = countriesList.length;
			var isBookFlights = select.closest('[data-search-flights-form]').attr('data-search-flights-form') === 'book';
			
			for (var i = 0; i < cll; i++) {
				var curCity = countriesList[i];
				selectEl.append(createList(curCity, ''));
				
				// check if booking flights then prepare data for rail-fly
				if(isBookFlights && listRailsCoach) {
					if(typeof curCity.displayRailCoachFlypoints === 'object') {
						var rcl = curCity.displayRailCoachFlypoints.length;

						// start looping through current city rail fly items
						for (var j = 0; j < rcl; j++) {
							var curAirportCode = curCity.displayRailCoachFlypoints[j];
							if(typeof railCoachList[curAirportCode] === 'undefined') {
								railCoachList[curAirportCode] = getCityByAirportCode(curAirportCode, countriesListJSON);
							}
						}
					}
				}
			}
			
			if(listRailsCoach && railCoachList.length !== 0) listRailsCoach = false;
			optionsAddData(select, selectEl.children());
		} else {
			optionsAddData(select, select.children());
		}

		select.off('change.selectCity').on('change.selectCity', function() {
			select.closest('[data-autocomplete-od]').find('input').val(select.find(':selected').data('text'));
		});

		var isFixed = false;
		self.parents().each(function() {
			isFixed = $(this).css('position') === 'fixed';
			return !isFixed;
		});
		var isClicked = false;

		self.on("isDeleteClicked", function() {
			isClicked = true;
		});

		_autoComplete({
			autocompleteFields: self.find('input:text'),
			autoCompleteAppendTo: body,
			airportData: data,
			cityData: arrDataValue,
			position: isFixed ? {
				collision: 'flip'
			} : {
				my: 'left top',
				at: 'left bottom',
				collision: 'none'
			},
			open: function() {
				var self = $(this);
				self.autocomplete('widget').find('.redundancy').remove();
				clearTimeout(clearHideaddClear);
				// self.addClear('hide');
				// self.autocomplete('widget').hide();
				if (isFixed) {
					self.autocomplete('widget').css({
						position: 'fixed'
					});
				}

				var selectTag = self.closest('[data-autocomplete-od]').find('[data-city-group]');

				// remove close icon in book-redem
				if (selectTag.closest(selectionIdList).length) {
					self.next().remove();
				}
			},
			response: function(event, ui) {
				var resultL = ui.content.length;
				
				if (resultL > listLimitDisplay) {
					ui.content.splice(listLimitDisplay, Math.abs(resultL - listLimitDisplay));
				}

				if (resultL === 1 && event.target.value !== '') {
					// $(this).val(ui.content[0].value);
					// $(this).select();
				}
				if (!resultL) {
					ui.content.push({
						city: L10n.globalSearch.noMatches,
						label: L10n.globalSearch.noMatches,
						value: null
					});
				}
				
				var bookATripForm = $("[data-bookatrip], .static-details").find('[name="search-type"]:checked').data("search-flights");

				if (
					((bookATripForm || "").toLowerCase() == "book" || $("body").hasClass("flight-search")) && 
					resultL > 0 && 
					ui.content[0].city !== 'No results found.' &&
					ui.content[0].city &&
					!$this.data("disable-rail-coach")
				) {
					var newUIContent = [];
					var specialCaseCountries = [];
					var sorted = _.sortBy(ui.content, "label");
					var isCity = true, isRailCoach = false;
					// check if search is special case
					var isSpecialCase = getSpecialCaseCountries(event.target.value);
					
					rearrangeUI(ui.content, sorted);
					var haveList = ifSearchedListHaveRailOrCoach(sorted);
					_.each(sorted, function(item, i) {
						var result = findRailCoach(item, countriesListJSON);
						// if special case, remove results as it will be piped on the bottom
						result = (isSpecialCase) ? [] : result;
						isRailCoach = checkRailCoachFly(item.airportCode, sorted).length ? true : false;
						item = getRailCoach(item.airportCode, countriesListJSON);
						isCity = checkIfCity(item, event.target.value);

						// Check if country search and if item is rail/coach with exception on special case as it will be piped on the las part
						if (haveList &&
							(item.isRailPointStation || item.isCoachPointStation) &&
							!isSpecialCase
						) {
							return;
						}

						// rail/coach special case label
						if (isSpecialCase && isRailCoach && isSpecialCaseCountryCode(item.country)) {
							if (!specialCaseCountries.length) {
								specialCaseCountries.push({
									city: ui.content[i].city,
									cityName: getRailCoachCityLabel(ui.content[i].country),
									country: ui.content[i].country,
									value: ui.content[i].value,
									railCoachLabel: true
								});
							}

							return specialCaseCountries.push(getRailCoach(item.airportCode, countriesListJSON));
						// rail/coach label
						} else if (result.length) {
							result = _.sortBy(result, "city");
							result.unshift({
								city: ui.content[i].city,
								cityName: ui.content[i].cityName,
								country: ui.content[i].country,
								value: ui.content[i].value,
								railCoachLabel: true
							});
						}
						result.unshift(getRailCoach(item.airportCode, countriesListJSON));
						newUIContent = $.merge(newUIContent, result);
					});

					// pipes special case
					if (isSpecialCase) {
						newUIContent = $.merge(newUIContent, specialCaseCountries);
					}
					
					rearrangeUI(ui.content, newUIContent);
				} else {
					var sorted = _.sortBy(ui.content, "label");
					rearrangeUI(ui.content, sorted);
				}

			},
			search: function() {},
			close: function() {
				var self = $(this);
				
				if (body.hasClass("recent-search") && isClicked) {
					self.autocomplete("widget");
					self.focus();
					isClicked = false;
					return false;
				}
				
				self.closest('[data-autocomplete-od]').removeClass('focus');

				if (!self.val() || self.val() === self.attr('placeholder')) {
					self.closest('[data-autocomplete-od]').addClass('default');
				}

				// Fix bug for iPad
				if (/iPad/i.test(window.navigator.userAgent)) {
					setTimeout(function() {
						$(document).scrollTop(win.scrollTop() + 1);
					}, 100);
				}

				if (!self.closest(selectionIdList).length >= 1) {
					clearHideaddClear = setTimeout(function() {
						self.blur();
						self.addClear('hide');
					}, 200);
				}


				// add close icon for book redeem block
				if (self.closest(selectionIdList).length >= 1 && self.val().length >= 1) {
					clearCloseBtn(self);
				} else if (self.closest(selectionIdList).length >= 1 && self.val().length == 0) {
					self.closest('[data-autocomplete-od="true"]').find('.optim-close').addClass('hidden');
				}

				if (self.data('autopopulate')) {
					$(self.data('autopopulate')).val(self.val()).closest('[data-autocomplete-od]').removeClass('default');
					if (self.parents('.grid-col').hasClass('error')) {
						$(self.data('autopopulate')).valid();
					}
				}

				if (self.data('autopopulateholder')) {
					self.val(self.val() || self.data('autopopulateholder')).closest('[data-autocomplete-od]').removeClass('default');
					self.valid();
				}
			},
			select: function(event, ui) {
				// $(this).addClear('hide');
				var self = $(this),
					selfAutoComplete = self.parents('[data-autocomplete-od]'),
					searchBox = $('input', selfAutoComplete);
				if (ui.item.link) {
					searchBox.data('link', ui.item.link);
				}

				
				if (ui.item.data) {
					$("[data-recent-search-trigger]").trigger("selectAutoSearch", ui.item.data);
					return;
				}

				if (!ui.item.value) {
					window.setTimeout(function() {
						self.trigger('blur.triggerByGroupValidate');
					}, 400);
					return;
				} else {
					if (self.closest('#travel-widget').data('widget-v1') || self.closest('#travel-widget').data('widget-v2')) {
						$('#travel-widget .from-select, #travel-widget .to-select').not(selfAutoComplete).removeClass('default');
						if (selfAutoComplete.is('.from-select')) {
							$('#travel-widget .from-select').not(selfAutoComplete).find('input').val(ui.item.value);
						} else if (selfAutoComplete.is('.to-select')) {
							$('#travel-widget .to-select').not(selfAutoComplete).find('input').val(ui.item.value);
						}
					};
				}

				if (ui.item.value) {
					ui.item.value = ui.item.cityName + " - " + ui.item.airportCode;
				}

				self.closest('[data-autocomplete-od]').removeClass('default');
				
				if (self.parents('.from-select').length) {
					originSelected = ui.item.countryExclusion;
					countryOriginSelected = ui.item.value;
					self.closest('.form-group').data('change', true);
					if (self.parents('.from-to-container').find('.to-select').length) {
						if (window.navigator.msMaxTouchPoints) {
							// fix lumina 820 can not open autocomplete of To
							self.blur();
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
							}, 500);
						} else {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
							}, 100);
						}
					} else {
						if (window.navigator.msMaxTouchPoints) {
							// fix lumina 820 can not open autocomplete of To
							self.blur();
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
								self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
							}, 500);
						} else {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								//self.blur();
								// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
								self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
							}, 100);
						}
					}
				}
				if (self.parents('.to-select').length) {
					destinationSelected = ui.item.countryExclusion;
					countryDestinationSelected = ui.item.value;
					if ($('#travel-radio-4').length && $('#travel-radio-5').length && ($('#travel-radio-4').is(':visible') || $('#travel-radio-5').is(':visible'))) {
						if (self.is('#city-2')) {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								if ($('#travel-radio-4').is(':checked')) {
									self.blur();
									setTimeout(function() {
										// $('#travel-start-day').closest('.input-3').trigger('click.showDatepicker');
										$('#travel-start-day').focus();
									}, 300);
								} else if ($('#travel-radio-5').is(':checked')) {
									self.blur();
									setTimeout(function() {
										// self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').focus();
									}, 300);
								}
								// if(self.closest('form').data('validator')){
								// 	self.closest('.form-group').find('input').valid();
								// }
							}, 100);
						}
					} else if (self.closest('form').find('[data-return-flight]').length) {
						if (window.navigator.msMaxTouchPoints) {
							// fix lumina 820 can not open autocomplete of To
							self.blur();
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
								if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
									self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
								} else {
									self.closest('form').find('.form-group').find('[data-oneway]').focus();
								}
							}, 500);
						} else {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.blur();
								setTimeout(function() {
									// self.parents('.from-to-container').siblings('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
									if (self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')) {
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									} else {
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									}
								}, 300);
							}, 100);
						}
					} else if (self.closest('form').find('.form-group').length) {
						if (window.navigator.msMaxTouchPoints) {
							// fix lumina 820 can not open autocomplete of To
							self.blur();
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
								self.closest('form').find('.form-group').find('[data-oneway]').focus();
							}, 500);
						} else {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.blur();
								setTimeout(function() {
									// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
									if (!$('body').hasClass('multi-city-page')) {
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									} else {
										self.closest('.form-group').find('[data-oneway]').focus();
									}
								}, 300);
							}, 100);
						}
					} else if (self.parents('.from-to-container').children('[data-trigger-date]').length) {
						if (window.navigator.msMaxTouchPoints) {
							// fix lumina 820 can not open autocomplete of To
							self.blur();
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
							}, 500);
						} else {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
							}, 100);
						}
					} else if (self.closest('[data-flight-schedule]').length) {
						var dtfs = self.closest('[data-flight-schedule]');
						if (dtfs.find('[data-return-flight]').length) {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								dtfs.find('[data-start-date]').focus();
							}, 500);
						} else if (dtfs.find('[data-oneway]').length) {
							clearTimeout(timerAutocomplete);
							timerAutocomplete = setTimeout(function() {
								dtfs.find('[data-oneway]').focus();
							}, 500);
						}
					}
				}
				setTimeout(function() {
					if (self.closest('form').data('validator')) {
						self.valid();
					}
					/*if(initLoading){
					 loadingStatus.hide();
					 }*/
					self.closest('[data-autocomplete-od]').siblings('.mobile').find('.select__text').text(ui.city).siblings('select').val(ui.value);
					self.trigger('change.programCode');
					self.blur();
				}, 200);
			},
			setWidth: 0
		});

		if (inputEl.val() === '' || inputEl.val() === inputEl.attr('placeholder')) {
			self.closest('[data-autocomplete-od]').addClass('default');
		} else {
			self.closest('[data-autocomplete-od]').removeClass('default');
		}
	};

	var ifSearchedListHaveRailOrCoach = function(sorted) {
		var searchItems = [
			"LHR",
			"DUS",
			"FRA",
			"MUC"
		];

		for(var i = 0; i < searchItems.length; i ++) {
			var airportCode = searchItems[i];
			var find = _.findWhere(sorted, {"airportCode": airportCode});
			if (find) {
				return true;
			}
		}

		return false;
	};

	var getSpecialCaseCountries = function(countryName) {
		var countryList = localizedCountries;
		var regExp = new RegExp($.ui.autocomplete.escapeRegex(countryName), "i");
		return _.chain(countryList)
				.map(function(matchWord) {
					return matchWord.toLowerCase() == countryName.toLowerCase();
				})
				.reduce(function(memory, isMatch) {
					return memory || isMatch;
				}, false)
				.value();
	};

	var isSpecialCaseCountryCode = function(code) {
		var codes = [
			"DE"
		];

		return codes.indexOf(code.toUpperCase()) != -1;
	};

	var getRailCoachLabel = function(item) {
		var countryCode = item.country.toUpperCase();
		var labelOne = [
			"DE"
		];
		var labelOneText = "Book Rail-Fly and catch the train to";
		var defaultText = "Book Rail-Fly or Coach-Fly to connect to";

		if (labelOne.indexOf(countryCode) != -1) {
			return labelOneText;
		}  else {
			return defaultText;
		}
	};

	var getRailCoachCityLabel = function(countryCode) {
		switch(countryCode) {
			case "DE":
				return "Dusseldorf, Frankfurt or Munich";
			default: 
				return "London";
		}
	};

	var checkRailCoachFly = function(code, json) {
		var countriedWithRailCoach = _.filter(json, function(list) {
			if (list.railcoach) {
				if (list.railcoach.split(",").indexOf(code) != -1)
					return list;
			}
		});

		return countriedWithRailCoach;
	};

	var checkIfCity = function(data, value) {
		var regExp  = RegExp($.ui.autocomplete.escapeRegex(value), "i");
		return regExp.test(data.cityName) || regExp.test(data.airportCode) || regExp.test(data.airportName);
	};

	var findRailCoach = function(item, list) {
		var railCoachData = [];
		if (item.railcoach) {
			_.each(item.railcoach.split(","), function(code) {
				railCoachData.push(getRailCoach(code, list));
			});
		}
		return railCoachData;
	};

	var getRailCoach = function(code, list) {
		var data = _.findWhere(list.airportListInfo, { airportCode: code });
		if (!data) return;
		var city = data.cityName + ", " + data.countryName;
		var airport = data.airportName + " - " + data.airportCode;
		var label = city + " (" + airport + ")";
		var newData = {
			airportCode: data.airportCode,
			airportName: data.airportName,
			cityCode: data.cityCode,
			city: label,
			cityName: data.cityName,
			country: data.countryCode,
			countryExclusion: _.map((checkExcludeCountries(data.excludeCountries) || []), function(exclude) {
				return exclude.excludedCountryCode;
			}).join(","),
			label: label,
			link: "",
			value: airport
		};
		newData.isCoachPointStation = data.isCoachPointStation;
		newData.isRailPointStation = data.isRailPointStation;
		return newData;
	};

	var checkExcludeCountries = function(excludeCountries) {
		if (typeof excludeCountries == "string") {
			return _.map(excludeCountries.split(","), function(country) {
				return {
					excludedCountryCode: country
				}
			});
		} else {
			return excludeCountries;
		}
	};

	var rearrangeUI = function(uiContent, newUIContent) {
		uiContent.splice(0, uiContent.length);
		_.each(newUIContent, function(UIContent) {
			uiContent.push(UIContent);
		});
	};

	var checkAvailableFlights = function(listCode, match) {
		var listData = match;
		var filterDataOrigin = [];

		_.each(listCode, function(code, idx) {
			filterDataOrigin = $.grep(listData, function(item) {
				if (idx !== listCode.length - 1) {
					if (item.country) {
						return item.country !== code;
					}
				} else {
					if (item.value) {
						return item.value !== code;
					}
				}
				return item;
			});
			listData = filterDataOrigin;
		});

		return listData;
	};

	var checkGroupNoItem = function(listData) {
		var isCity = true;

		_.each(listData, function(data) {
			isCity = data.value ? true : false;
		})
		if (!isCity) listData = [];

		var newArr = [];
		_.each(listData, function(data) {
			!data.value ? newArr.push(data.label) : '';
			// data.value === data.label ? newArr.push(data.label) : '';
		})

		_.each(newArr, function(country) {
			var hasCity = false;
			for (var i = 0; i < listData.length; i++) {
				if (listData[i].parent === country) {
					hasCity = true;
					break;
				}
			}
			if (!hasCity) {
				_.each(listData, function(data, i) {
					if (data.label === country) {
						listData.splice(i, 1);
					}
				});
			}
		});

		return listData;

	}

	var resetAvailableFlight = function() {
		$('[data-autocomplete-od]').each(function() {
			var _self = $(this),
				inputEl = _self.find('input'),
				clearTextEl = _self.find('.add-clear-text'),
				timeout;

			if (timeout) {
				clearTimeout(timeout);
			}

			timeout = setTimeout(function() {
				if (_self.data('origin')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							originSelected = '';
							countryOriginSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						originSelected = '';
						countryOriginSelected = '';
					});
				}
				if (_self.data('destination')) {
					inputEl.off('keyup.resetAvailableFlight').on('keyup.resetAvailableFlight', function(e) {
						if (e.keyCode === 8) {
							destinationSelected = '';
							countryDestinationSelected = '';
						}
					});
					clearTextEl.off('click.resetAvailableFlight').on('click.resetAvailableFlight', function() {
						destinationSelected = '';
						countryDestinationSelected = '';
					});
				}
			}, 1000);

		});
	};

	var processAirportList = function(data) {
		originAirports = data.filter(isOrigin);
		destinationAirports = data.filter(isDestination);
	};

	var isDestination = function(val){
		return val.isDestinationAirport === 'true';
	};

	var isOrigin = function(val){
		return val.isOriginAirport === 'true';
	};

	function setLocalStorage(key, data) {
        window.localStorage.setItem(key, JSON.stringify(data));
    }

    function getLocalStorage(key) {
        var items = JSON.parse(window.localStorage.getItem(key));
        return items ? items : [];
	}
	
	var getCityByAirportCode = function(ac, list){
		var countriesList = list.airportListInfo;
		// loop through main city list again
		var cll = countriesList.length;
		for (var i = 0; i < cll; i++) {
			var city = countriesList[i];
			if(city.airportCode === ac) {
				return city;
			}
		}
	};
	// init
	initAutocompleteCity();
};
// Array filter implementation
if (!Array.prototype.filter){
  Array.prototype.filter = function(fun /*, thisp */)
  {
    "use strict";

    if (this === void 0 || this === null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function")
      throw new TypeError();

    var res = [];
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in t)
      {
        var val = t[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, t))
          res.push(val);
      }
    }

    return res;
  };
}

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(elt /*, from*/) {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
